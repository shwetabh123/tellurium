﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace Common
{
   public class Rawdata
    {

        public static bool compareresults(DataSet ActualSP, DataSet rawdata)
        {
            bool RSbbflag = true;
            DataTable ActualSPTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[0]);
            ActualDv.Sort = "SurveywaveParticipantID ASC";
            ActualSPTable = ActualDv.ToTable();



            DataTable CompRawdataTable;
            DataViewManager CompGrpdvm = rawdata.DefaultViewManager;
            DataView Compdv = CompGrpdvm.CreateDataView(rawdata.Tables[1]);
            Compdv.Sort = "Respondentid ASC";
            CompRawdataTable = Compdv.ToTable();

            var ACTParicipantID = ActualSPTable.Columns["SurveywaveParticipantID"];
            var TrgParticipantID = CompRawdataTable.Columns["Respondentid"];

            foreach (DataRow RowTbl1 in ActualSPTable.Rows)
            {
                foreach (DataRow RowTbl2 in CompRawdataTable.Rows)
                {
                    if (RowTbl1[ACTParicipantID].ToString() == RowTbl2[TrgParticipantID].ToString())
                    {

                        RSbbflag = false;

                        break;
                    }

                }
            }
            return RSbbflag;
        }






    }
}
