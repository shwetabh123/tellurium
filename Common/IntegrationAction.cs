﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class IntegrationAction
    {

        public static void NavigatetoSettings(IWebDriver driver)
        {
            var navbar = driver.FindElements(By.CssSelector(".sidebar-menu a span"));
            navbar.FirstOrDefault(e => e.Text == "Settings").Click();

          
        }

        public static void OpenIntegrationtab(IWebDriver driver)
        {
         
            UIActions.clickwithXapth(driver, "(//*[@id='menu-integration-features'])");
        }
     
    }
}
