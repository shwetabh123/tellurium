﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

namespace Common
{
    public class DataPopulationForReportDataValidation
    {
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";

        #region Stg_InsertQuery
        public static string Stg_InsertQuery = @"
DELETE FROM UserAnalyzeDisplay WHERE UserID BETWEEN 9900 AND 12000 AND SurveyFormID = 6876

DELETE SF
FROM UserAnalyzeFilter UAF INNER JOIN SurveyFilter SF
ON SF.UserAnalyzeFilterID = UAF.UserAnalyzeFilterID
AND UAF.UserID BETWEEN 9900 AND 12000 AND UAF.SurveyFormID = 6876

DELETE DRF
FROM UserAnalyzeFilter UAF INNER JOIN DemoRangeFilter DRF
ON DRF.UserAnalyzeFilterID = UAF.UserAnalyzeFilterID
AND UAF.UserID BETWEEN 9900 AND 12000 AND UAF.SurveyFormID = 6876

DELETE DF
FROM UserAnalyzeFilter UAF INNER JOIN DemoFilter DF
ON DF.UserAnalyzeFilterID = UAF.UserAnalyzeFilterID
AND UAF.UserID BETWEEN 9900 AND 12000 AND UAF.SurveyFormID = 6876

DELETE FROM UserAnalyzeFilter WHERE UserID BETWEEN 9900 AND 12000 AND SurveyFormID = 6876

drop table if exists UserAnalyzeDisplay_Stg
create table UserAnalyzeDisplay_Stg
(
ID int
,DataCalculation int
,FavOrMean int
)

insert into UserAnalyzeDisplay_Stg
select 1, 1, 1
UNION
select 2, 1, 2
UNION
select 3, 2, 1
UNION
select 4, 2, 2


drop table if exists tblScenariouserAnalyzeFilterXref
create table tblScenariouserAnalyzeFilterXref
(
	UserAnalyzeFilterId bigint
	,ScenarioId int
)




declare @sno int = 0
declare @id int = 0
declare @target int
declare @comp1 int
declare @comp2 int
declare @comp3 int
declare @Tidentity int
declare @tdemo nvarchar(max) = null
declare @tdateasdemo nvarchar(max) = null
declare @ttimeperiod nvarchar(max) = null
declare @tbenchmarkid int
declare @C1identity int
declare @c1demo nvarchar(max) = null
declare @c1dateasdemo nvarchar(max) = null
declare @c1timeperiod nvarchar(max) = null
declare @c1benchmarkid int
declare @C2identity int
declare @c2demo nvarchar(max) = null
declare @c2dateasdemo nvarchar(max) = null
declare @c2timeperiod nvarchar(max) = null
declare @c2benchmarkid int
declare @C3identity int
declare @c3demo nvarchar(max) = null
declare @c3dateasdemo nvarchar(max) = null
declare @c3timeperiod nvarchar(max) = null
declare @c3benchmarkid int
declare @userid int = 9900
declare @clientid int = 29


	WHILE (1 = 1)
	BEGIN
	SET @id = 0
		SELECT DISTINCT TOP 1 @sno = Sno
		FROM ActualSPWithComparison_Stg
		WHERE 
			Sno > @sno
		ORDER BY Sno ASC
		
		IF @@ROWCOUNT = 0
			BREAK;
		
		WHILE (1 = 1)
		BEGIN
			SELECT DISTINCT TOP 1 @id = ID
			FROM UserAnalyzeDisplay_Stg
			WHERE
				ID > @id
				ORDER BY ID ASC

			IF @@ROWCOUNT = 0
			BREAK;

			declare @datacalculation int
			declare @favormean int

			SELECT @datacalculation = DataCalculation FROM UserAnalyzeDisplay_Stg WHERE ID = @id
			SELECT @favormean = FavOrMean FROM UserAnalyzeDisplay_Stg WHERE ID = @id

			

			select @target = CONVERT(INT,[Target]) from ActualSPWithComparison_Stg where Sno = @sno
			select @comp1 = CONVERT(INT,Comp1) from ActualSPWithComparison_Stg where Sno = @sno
			select @comp2 = CONVERT(INT,Comp2) from ActualSPWithComparison_Stg where Sno = @sno
			select @comp3 = CONVERT(INT,Comp3) from ActualSPWithComparison_Stg where Sno = @sno
			select top 1 @tdemo = Demovalue from TestSPScenario_Stg where Sno = @target
			select top 1 @tdateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @target
			select top 1 @ttimeperiod = Timeperiod from TestSPScenario_Stg where Sno = @target
			select top 1 @tbenchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @target
			
			select top 1 @c1demo = Demovalue from TestSPScenario_Stg where Sno = @comp1
			select top 1 @c1dateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @comp1
			select top 1 @c1timeperiod = Timeperiod from TestSPScenario_Stg where Sno = @comp1
			select top 1 @c1benchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @comp1

			select top 1 @c2demo = Demovalue from TestSPScenario_Stg where Sno = @comp2
			select top 1 @c2dateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @comp2
			select top 1 @c2timeperiod = Timeperiod from TestSPScenario_Stg where Sno = @comp2
			select top 1 @c2benchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @comp2

			select top 1 @c3demo = Demovalue from TestSPScenario_Stg where Sno = @comp3
			select top 1 @c3dateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @comp3
			select top 1 @c3timeperiod = Timeperiod from TestSPScenario_Stg where Sno = @comp3
			select top 1 @c3benchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @comp3

		

			/* Target Group */
			
			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,1,0,'RED'
			,CASE WHEN @tbenchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@tbenchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @target
			
			SET @Tidentity = SCOPE_IDENTITY()

			if(@tdemo is not null)
			begin
				insert into DemoFilter
				Select distinct @Tidentity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @target
			end

			if(@tdateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @Tidentity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @target
			end

			insert into SurveyFilter
			select @Tidentity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @target

			insert into tblScenariouserAnalyzeFilterXref
			select @Tidentity, @target

			/* Comparison group 1 */

			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,2,1,'Tangerine'
			,CASE WHEN @c1benchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@c1benchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @comp1
			
			SET @C1identity = SCOPE_IDENTITY()

			if(@c1demo is not null)
			begin
				insert into DemoFilter
				Select distinct @C1identity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @comp1
			end

			if(@c1dateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @C1identity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @comp1
			end

			insert into SurveyFilter
			select @C1identity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @comp1

			insert into tblScenariouserAnalyzeFilterXref
			select @C1identity, @comp1

			/* Comparison group 2 */

			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,2,2,'Sky'
			,CASE WHEN @c2benchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@c2benchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @comp2
			
			SET @C2identity = SCOPE_IDENTITY()

			if(@c2demo is not null)
			begin
				insert into DemoFilter
				Select distinct @C2identity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @comp2
			end

			if(@c2dateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @C2identity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @comp2
			end

			insert into SurveyFilter
			select @C2identity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @comp2

			insert into tblScenariouserAnalyzeFilterXref
			select @C2identity, @comp2

			/* Comparison group 3 */

			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,2,3,'Lemon'
			,CASE WHEN @c3benchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@c3benchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @comp3
			
			SET @C3identity = SCOPE_IDENTITY()

			if(@c3demo is not null)
			begin
				insert into DemoFilter
				Select distinct @C3identity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @comp3
			end

			if(@c3dateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @C3identity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @comp3
			end

			insert into SurveyFilter
			select @C3identity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @comp3

			insert into tblScenariouserAnalyzeFilterXref
			select @C3identity, @comp3

			INSERT INTO UserAnalyzeDisplay
			SELECT TOP 1 @userid, @clientid, SurveyFormID, @datacalculation, @favormean, NULL, NULL, NULL FROM TestSPScenario_Stg
			WHERE Sno = @target

		

			set @userid = @userid + 1
		END
		
	END
";
        #endregion

        #region QA_InsertQuery
        public static string QA_InsertQuery = @"
DELETE FROM UserAnalyzeDisplay WHERE UserID BETWEEN 9900 AND 12000 AND SurveyFormID = 5881

DELETE SF
FROM UserAnalyzeFilter UAF INNER JOIN SurveyFilter SF
ON SF.UserAnalyzeFilterID = UAF.UserAnalyzeFilterID
AND UAF.UserID BETWEEN 9900 AND 12000 AND UAF.SurveyFormID = 5881

DELETE DRF
FROM UserAnalyzeFilter UAF INNER JOIN DemoRangeFilter DRF
ON DRF.UserAnalyzeFilterID = UAF.UserAnalyzeFilterID
AND UAF.UserID BETWEEN 9900 AND 12000 AND UAF.SurveyFormID = 5881

DELETE DF
FROM UserAnalyzeFilter UAF INNER JOIN DemoFilter DF
ON DF.UserAnalyzeFilterID = UAF.UserAnalyzeFilterID
AND UAF.UserID BETWEEN 9900 AND 12000 AND UAF.SurveyFormID = 5881

DELETE FROM UserAnalyzeFilter WHERE UserID BETWEEN 9900 AND 12000 AND SurveyFormID = 5881

drop table if exists UserAnalyzeDisplay_Stg
create table UserAnalyzeDisplay_Stg
(
ID int
,DataCalculation int
,FavOrMean int
)

insert into UserAnalyzeDisplay_Stg
select 1, 1, 1
UNION
select 2, 1, 2
UNION
select 3, 2, 1
UNION
select 4, 2, 2


drop table if exists tblScenariouserAnalyzeFilterXref
create table tblScenariouserAnalyzeFilterXref
(
	UserAnalyzeFilterId bigint
	,ScenarioId int
)




declare @sno int = 0
declare @id int = 0
declare @target int
declare @comp1 int
declare @comp2 int
declare @comp3 int
declare @Tidentity int
declare @tdemo nvarchar(max) = null
declare @tdateasdemo nvarchar(max) = null
declare @ttimeperiod nvarchar(max) = null
declare @tbenchmarkid int
declare @C1identity int
declare @c1demo nvarchar(max) = null
declare @c1dateasdemo nvarchar(max) = null
declare @c1timeperiod nvarchar(max) = null
declare @c1benchmarkid int
declare @C2identity int
declare @c2demo nvarchar(max) = null
declare @c2dateasdemo nvarchar(max) = null
declare @c2timeperiod nvarchar(max) = null
declare @c2benchmarkid int
declare @C3identity int
declare @c3demo nvarchar(max) = null
declare @c3dateasdemo nvarchar(max) = null
declare @c3timeperiod nvarchar(max) = null
declare @c3benchmarkid int
declare @userid int = 9900
declare @clientid int = 34767


	WHILE (1 = 1)
	BEGIN
	SET @id = 0
		SELECT DISTINCT TOP 1 @sno = Sno
		FROM ActualSPWithComparison_Stg
		WHERE 
			Sno > @sno
		ORDER BY Sno ASC
		
		IF @@ROWCOUNT = 0
			BREAK;
		
		WHILE (1 = 1)
		BEGIN
			SELECT DISTINCT TOP 1 @id = ID
			FROM UserAnalyzeDisplay_Stg
			WHERE
				ID > @id
				ORDER BY ID ASC

			IF @@ROWCOUNT = 0
			BREAK;

			declare @datacalculation int
			declare @favormean int

			SELECT @datacalculation = DataCalculation FROM UserAnalyzeDisplay_Stg WHERE ID = @id
			SELECT @favormean = FavOrMean FROM UserAnalyzeDisplay_Stg WHERE ID = @id

			

			select @target = CONVERT(INT,[Target]) from ActualSPWithComparison_Stg where Sno = @sno
			select @comp1 = CONVERT(INT,Comp1) from ActualSPWithComparison_Stg where Sno = @sno
			select @comp2 = CONVERT(INT,Comp2) from ActualSPWithComparison_Stg where Sno = @sno
			select @comp3 = CONVERT(INT,Comp3) from ActualSPWithComparison_Stg where Sno = @sno
			select top 1 @tdemo = Demovalue from TestSPScenario_Stg where Sno = @target
			select top 1 @tdateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @target
			select top 1 @ttimeperiod = Timeperiod from TestSPScenario_Stg where Sno = @target
			select top 1 @tbenchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @target
			
			select top 1 @c1demo = Demovalue from TestSPScenario_Stg where Sno = @comp1
			select top 1 @c1dateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @comp1
			select top 1 @c1timeperiod = Timeperiod from TestSPScenario_Stg where Sno = @comp1
			select top 1 @c1benchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @comp1

			select top 1 @c2demo = Demovalue from TestSPScenario_Stg where Sno = @comp2
			select top 1 @c2dateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @comp2
			select top 1 @c2timeperiod = Timeperiod from TestSPScenario_Stg where Sno = @comp2
			select top 1 @c2benchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @comp2

			select top 1 @c3demo = Demovalue from TestSPScenario_Stg where Sno = @comp3
			select top 1 @c3dateasdemo = [Date as Demo] from TestSPScenario_Stg where Sno = @comp3
			select top 1 @c3timeperiod = Timeperiod from TestSPScenario_Stg where Sno = @comp3
			select top 1 @c3benchmarkid = [Benchmark] from TestSPScenario_Stg where Sno = @comp3

		

			/* Target Group */
			
			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,1,0,'RED'
			,CASE WHEN @tbenchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@tbenchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @target
			
			SET @Tidentity = SCOPE_IDENTITY()

			if(@tdemo is not null)
			begin
				insert into DemoFilter
				Select distinct @Tidentity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @target
			end

			if(@tdateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @Tidentity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @target
			end

			insert into SurveyFilter
			select @Tidentity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @target

			insert into tblScenariouserAnalyzeFilterXref
			select @Tidentity, @target

			/* Comparison group 1 */

			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,2,1,'Tangerine'
			,CASE WHEN @c1benchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@c1benchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @comp1
			
			SET @C1identity = SCOPE_IDENTITY()

			if(@c1demo is not null)
			begin
				insert into DemoFilter
				Select distinct @C1identity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @comp1
			end

			if(@c1dateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @C1identity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @comp1
			end

			insert into SurveyFilter
			select @C1identity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @comp1

			insert into tblScenariouserAnalyzeFilterXref
			select @C1identity, @comp1

			/* Comparison group 2 */

			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,2,2,'Sky'
			,CASE WHEN @c2benchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@c2benchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @comp2
			
			SET @C2identity = SCOPE_IDENTITY()

			if(@c2demo is not null)
			begin
				insert into DemoFilter
				Select distinct @C2identity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @comp2
			end

			if(@c2dateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @C2identity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @comp2
			end

			insert into SurveyFilter
			select @C2identity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @comp2

			insert into tblScenariouserAnalyzeFilterXref
			select @C2identity, @comp2

			/* Comparison group 3 */

			insert into UserAnalyzeFilter
			select top 1 @userid,@clientid,SurveyFormID,2,3,'Lemon'
			,CASE WHEN @c3benchmarkid is null THEN 2
			ELSE 1
			END
			,1,0,NULL
			,@c3benchmarkid
			,1,1,GETUTCDATE(),GETUTCDATE(),1,
			CASE WHEN	Timeperiod = '1' THEN 1
			ELSE 5
			END
			,[Start],[End],
			CASE WHEN Timeperiod IN ('1','Survey Response Date') THEN NULL
			ELSE Timeperiod
			END
			,0
			from TestSPScenario_Stg where Sno = @comp3
			
			SET @C3identity = SCOPE_IDENTITY()

			if(@c3demo is not null)
			begin
				insert into DemoFilter
				Select distinct @C3identity, Demolabel, Demovalue,0 from TestSPScenario_Stg where Sno = @comp3
			end

			if(@c3dateasdemo is not null)
			begin
				insert into DemoRangeFilter
				Select distinct @C3identity, StartDate, EndDate, 1, [Date as Demo],0 from TestSPScenario_Stg where Sno = @comp3
			end

			insert into SurveyFilter
			select @C3identity,SurveyFormID,[Distribution] from TestSPScenario_Stg where Sno = @comp3

			insert into tblScenariouserAnalyzeFilterXref
			select @C3identity, @comp3

			INSERT INTO UserAnalyzeDisplay
			SELECT TOP 1 @userid, @clientid, SurveyFormID, @datacalculation, @favormean, NULL, NULL, NULL FROM TestSPScenario_Stg
			WHERE Sno = @target

		

			set @userid = @userid + 1
		END
		
	END
	";
        #endregion

        #region Stg_tblTestScenario
        public static string Stg_tblTestScenario = @"
DROP TABLE IF EXISTS [dbo].[tblTestScenario]
CREATE TABLE [dbo].[tblTestScenario](
	[ScenarioID] [int] NULL,
	[ScenarioDescription] [nvarchar](max) NULL,
	[CompletedTheSurvey] [int] NULL,
	[AtleastOneQuestion] [int] NULL
) 

INSERT INTO tblTestScenario
SELECT 1, N'Default Target Launch', 5350, 5975
UNION
SELECT 2, N'Target+ DemoFilter (Which of the following best describes your marital status? - Single (including separated,divorced and widowed)How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have? (None) ', 665, 735
UNION
SELECT 3, N'Target+2Distribution -  Q5 and Q7Distribution', 889, 984
UNION
SELECT 4, N'Target+TimePeriod - Time Period (12/31/2016 - 12/31/2017)', 545, 599
UNION
SELECT 5, N'Target+ Date Demo (left Side Chip) -Date demo: transfer date (01/03/2016 - 12/31/2016)', 1715, 1915
UNION
SELECT 6, N'Target + Date Demo (Timeperiod )- Date demo: transfer date (01/03/2016 - 12/31/2016)', 1715, 1915
UNION
SELECT 7, N'Target+Demo + Timeperiod - Demo:domestic vs international: Domestic , Time Period (12/31/2016 - 12/31/2017)', 288, 310
UNION
SELECT 8, N'Target+Distribution+Timeperiod -Distribution Q5 and Q7 Distribution,Time Period (12/31/2016 - 12/31/2017)', 489, 531
UNION
SELECT 9, N'Target+Demo+DateDemo(LeftsideChip) - Demo: domestic vs international: Domestic , Date demo: transfer date (01/03/2016 - 12/31/2016)', 960, 1085
UNION
SELECT 10, N'Target+Demo+Datedemo(TimePeriod)- Demo :Periodic activities : Animal Charades, TimePeriod (12/31/2016 - 12/31/2017)', 58, 64
UNION
SELECT 11, N'Target+Distribution+DateDemo(LeftSideChip) - Date demo: transfer date (01/03/2016 - 12/31/2016), Distribution Q5 and Q7.', 1426, 1598
UNION
SELECT 12, N'Target+Distribution+DateDemo(HireDate)  - (01/01/2016 -12/12/2017)', 284, 319
UNION
SELECT 13, N'Target+ Distribution+ Demo  -  How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have? (None), More than one , Distribution Q5 and Q7 Distribution.', 429, 483
UNION
SELECT 14, N'Target+3 Distribution  Q5, Q7 and Q6', 889, 984
UNION
SELECT 15, N'Target+TimePeriod+DateDemo(hireDate)  - Date Demo: HireDate (01/01/2016 -12/12/2017) ,  TimePeriod ((12/31/2016 - 12/31/2017)', 162, 184
UNION
SELECT 16, N'Benchmark Technology-USA', 1, 1
";
        #endregion

        #region QA_tblTestScenario
        public static string QA_tblTestScenario = @"
DROP TABLE IF EXISTS [dbo].[tblTestScenario]
CREATE TABLE [dbo].[tblTestScenario](
	[ScenarioID] [int] NULL,
	[ScenarioDescription] [nvarchar](max) NULL,
	[CompletedTheSurvey] [int] NULL,
	[AtleastOneQuestion] [int] NULL
) 

INSERT INTO tblTestScenario
SELECT 1, N'Default Target Launch', 5350, 5975
UNION
SELECT 2, N'Target+ DemoFilter (Which of the following best describes your marital status? - Single (including separated,divorced and widowed)How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have? (None) ', 665, 735
UNION
SELECT 3, N'Target+2Distribution -  Q5 and Q7Distribution', 889, 984
UNION
SELECT 4, N'Target+TimePeriod - Time Period (12/31/2016 - 12/31/2017)', 545, 599
UNION
SELECT 5, N'Target+ Date Demo (left Side Chip) -Date demo: transfer date (01/03/2016 - 12/31/2016)', 1715, 1915
UNION
SELECT 6, N'Target + Date Demo (Timeperiod )- Date demo: transfer date (01/03/2016 - 12/31/2016)', 1715, 1915
UNION
SELECT 7, N'Target+Demo + Timeperiod - Demo:domestic vs international: Domestic , Time Period (12/31/2016 - 12/31/2017)', 288, 310
UNION
SELECT 8, N'Target+Distribution+Timeperiod -Distribution Q5 and Q7 Distribution,Time Period (12/31/2016 - 12/31/2017)', 489, 531
UNION
SELECT 9, N'Target+Demo+DateDemo(LeftsideChip) - Demo: domestic vs international: Domestic , Date demo: transfer date (01/03/2016 - 12/31/2016)', 960, 1085
UNION
SELECT 10, N'Target+Demo+Datedemo(TimePeriod)- Demo :Periodic activities : Animal Charades, TimePeriod (12/31/2016 - 12/31/2017)', 58, 64
UNION
SELECT 11, N'Target+Distribution+DateDemo(LeftSideChip) - Date demo: transfer date (01/03/2016 - 12/31/2016), Distribution Q5 and Q7.', 1426, 1598
UNION
SELECT 12, N'Target+Distribution+DateDemo(HireDate)  - (01/01/2016 -12/12/2017)', 284, 319
UNION
SELECT 13, N'Target+ Distribution+ Demo  -  How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have? (None), More than one , Distribution Q5 and Q7 Distribution.', 429, 483
UNION
SELECT 14, N'Target+3 Distribution  Q5, Q7 and Q6', 889, 984
UNION
SELECT 15, N'Target+TimePeriod+DateDemo(hireDate)  - Date Demo: HireDate (01/01/2016 -12/12/2017) ,  TimePeriod ((12/31/2016 - 12/31/2017)', 162, 184
UNION
SELECT 16, N'Benchmark Technology-USA', 1, 1
";
        #endregion

        #region QA_TestSPScenario_Stg
        public static string QA_TestSPScenario_Stg = @"
drop table if exists TestSPScenario_Stg
create table TestSPScenario_Stg
(
Sno	int null
,[Description] NVARCHAR(MAX) null
,SurveyFormID	int null
,Demolabel NVARCHAR(MAX) null
,Demovalue	NVARCHAR(MAX) null
,[Date as Demo]	NVARCHAR(MAX) null
,StartDate NVARCHAR(MAX) null
,EndDate NVARCHAR(MAX) null
,Timeperiod  	NVARCHAR(MAX) null
,[Start] NVARCHAR(MAX) null
,[End] NVARCHAR(MAX) null
,Benchmark	int null
,[Distribution] int null
)

Insert into TestSPScenario_Stg
Select 1,	'Default  Target Launch',5881,	Null,	Null,	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16822
UNION
Select 1,	'Default  Target Launch',5881,	Null,	Null,	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16792
UNION
Select 2,	'Target+ DemoFilter',	5881,	'Which of the following best describes your marital status?', 'Single (including separated, divorced and widowed)',	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16822
UNION
Select 2,	'Target+ DemoFilter', 	5881,	'Which of the following best describes your marital status?', 'Single (including separated, divorced and widowed)',	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16792
UNION
Select  2,	'Target+ DemoFilter',	5881,	'How Many Dependents (Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16822
UNION
Select  2,	'Target+ DemoFilter', 	5881,	'How Many Dependents (Not Including yourself or Your Spouse/Domestic partner) do you have?', 'None',	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16792
UNION
Select 3,	'Target+2Distribution',	5881,	Null,	Null,	Null,	Null,	Null,	'1',Null,	Null,	Null,	16792
UNION
Select 4,	'Target+TimePeriod',	5881,	Null,	Null,	Null,	Null,	Null,	'Survey Response Date',	'1/1/2016',	'1/1/2017',	Null,	16822
UNION
Select 4,	'Target+TimePeriod',	5881,	Null,	Null,	Null,	Null,	Null,	'Survey Response Date',	'1/1/2016',	'1/1/2017',	Null,	16792
UNION
Select 5,	'Target+ Date Demo (left Side  Chip)',	5881,	Null,	Null,	 'transfer date', 	'1/1/2016',	'1/1/2017',	'1'	,Null,	Null,	Null,	16822
UNION
Select 5,	'Target+ Date Demo (left Side  Chip)',	5881,	Null,	Null,	 'transfer date', 	'1/1/2016',	'1/1/2017',	'1'	,Null,	Null,	Null,	16792
UNION
select 6,	'Target + Date Demo (Timeperiod )',	5881,	Null,	Null,	Null,	Null,	Null,	'transfer date',	'1/1/2016',	'1/1/2017',	Null,	16822
UNION
select 6,	'Target + Date Demo (Timeperiod )',	5881,	Null,	Null,	Null,	Null,	Null,	'transfer date',	'1/1/2016',	'1/1/2017',	Null,	16792
UNION
Select 7,	'Target+Demo + Timeperiod',	5881,	'domestic vs international',	'Domestic', 	Null,	Null,	Null,	'Survey Response Date',	'1/1/2016',	  '1/1/2017',	Null	,16822
UNION
Select 7,	'Target+Demo + Timeperiod',	5881,	'domestic vs international',	'Domestic', 	Null,	Null,	Null,	'Survey Response Date',	'1/1/2016',	
'1/1/2017',	Null,	16822
UNION
Select 8,	'Target+Distribution+Timeperiod',	5881,	Null	,Null,	Null,	Null,	Null,	'Survey Response Date',	'1/1/2016',	'1/1/2017',	Null,16822
UNION
Select  9,	'Target+Demo+DateDemo(LeftsideChip)',	5881,	'domestic vs international',	'Domestic', 	 'transfer date',	'1/1/2016',	'1/1/2017',	'1',	Null,	Null,	Null,	16822
UNION
Select  9,	'Target+Demo+DateDemo(LeftsideChip)',	5881,	'domestic vs international',	'Domestic', 	 'transfer date',	'1/1/2016',	'1/1/2017',	'1',	Null,	Null,	Null,	16792
UNION
Select 10,	'Target+Demo+DateDemo(Timeperiod)',	5881,	'Periodic activities', 	'Animal Charades',	Null,	Null,	Null,	'Survey Response Date',	'1/1/2016',	'1/1/2017',	Null,	16822
UNION
Select 10,	'Target+Demo+DateDemo(Timeperiod)',	5881,	'Periodic activities', 	'Animal Charades',	Null,	Null,	Null,	'Survey Response Date',	'1/1/2016',	'1/1/2017',	Null,	16822
UNION
Select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	5881,	Null,	Null,	 'transfer date', 	'1/1/2016',	'1/1/2017',	'1',	Null,	Null,	Null,16792
UNION
Select 12,	'Target+Distribution+DateDemo(HireDate)',	5881,	Null,	Null,	Null,	Null,	Null,	 'transfer date', 	'1/1/2016',	'1/1/2017',	Null,	16792
UNION
Select 13,	'Target+ Distribution+ Demo', 	5881,	'How Many Dependents (Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'More than two',	Null,	Null,Null,	'1',	Null,	Null,	Null,	16792
UNION
Select 13,	'Target+ Distribution+ Demo', 	5881,	'How Many Dependents (Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16792
UNION
Select 14,	'Target+1 distribution', 	5881,	Null,	Null,	Null,	Null,	Null,	'1',	Null,	Null,	Null,	16792
UNION
Select 15,	'Target+TimePeriod+DateDemo(hireDate)',	5881	,Null	,Null,	 'transfer date', 	'1/1/2016',	'1/1/2017',	'Survey Response Date','1/1/2016',	'1/1/2017',	Null,16822	
UNION
Select 15,	'Target+TimePeriod+DateDemo(hireDate)',	5881	,Null	,Null,	 'transfer date', 	'1/1/2016',	'1/1/2017',	'Survey Response Date','1/1/2016',	'1/1/2017',	Null,16792
UNION
Select 16,	'Benchmark- Black Pearl Manufacturing',	5881,	Null,	Null,	Null,	Null,	Null,	'1',	Null,	Null,	102666	,16822
UNION
Select 16,	'Benchmark- Black Pearl Manufacturing',	5881,	Null,	Null,	Null,	Null,	Null,	'1',	Null,	Null,	102666	,16792
";
        #endregion

        #region Stg_TestSPScenario_Stg
        public static string Stg_TestSPScenario_Stg = @"
drop table if exists TestSPScenario_Stg
create table TestSPScenario_Stg
(
Sno	int null
,[Description] NVARCHAR(MAX) null
,SurveyFormID	int null
,Demolabel NVARCHAR(MAX) null
,Demovalue	NVARCHAR(MAX) null
,[Date as Demo]	NVARCHAR(MAX) null
,StartDate NVARCHAR(MAX) null
,EndDate NVARCHAR(MAX) null
,Timeperiod  	NVARCHAR(MAX) null
,[Start] NVARCHAR(MAX) null
,[End] NVARCHAR(MAX) null
,Benchmark	int null
,[Distribution] int null
)

drop table if exists TestSPScenario_Stg
create table TestSPScenario_Stg
(
Sno	int null
,[Description] NVARCHAR(MAX) null
,SurveyFormID	int null
,Demolabel NVARCHAR(MAX) null
,Demovalue	NVARCHAR(MAX) null
,[Date as Demo]	NVARCHAR(MAX) null
,StartDate NVARCHAR(MAX) null
,EndDate NVARCHAR(MAX) null
,Timeperiod  	NVARCHAR(MAX) null
,[Start] NVARCHAR(MAX) null
,[End] NVARCHAR(MAX) null
,Benchmark	int null
,[Distribution] int null
)

insert into TestSPScenario_Stg
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21414
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21415
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22165
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22234
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23485
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23487
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	28058
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	29444
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30654
UNION
select 1,	'Default  Target Launch',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30670
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21414
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21415
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22165
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22234
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23485
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23487
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	28058
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	29444
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30654
UNION
select 2,	'Target+ DemoFilter', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30670
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21414
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21415
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22165
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22234
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23485
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23487
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	28058
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	29444
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30654
UNION
select 2,	'Target+ DemoFilter', 	6876,	'Which of the following best describes your marital status?',		'Single (including separated,divorced and widowed)',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30670
UNION
select 3,	'Target+2Distribution',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30654
UNION
select 3,	'Target+2Distribution',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23485
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21414
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21415
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22165
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22234
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23485
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23487
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	28058
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	29444
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30654
UNION
select 4,	'Target+TimePeriod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30670
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	21414
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	21415
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	22165
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	22234
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	23485
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	23487
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	28058
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	29444
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	30654
UNION
select 5,	'Target+ Date Demo (left Side  Chip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	30670
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	21414
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	21415
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	22165
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	22234
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	23485
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	23487
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	28058
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	29444
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	30654
UNION
select 6,	'Target + Date Demo (Timeperiod )',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'transfer date', 	'1/3/2016',	'12/31/2016',	NULL,	30670
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21414
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21415
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22165
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22234
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23485
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23487
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	28058
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	29444
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30654
UNION
select 7,	'Target+Demo + Timeperiod',	6876,	'domestic vs international',	'Domestic', 	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30670
UNION
select 8,	'Target+Distribution+Timeperiod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30654
UNION
select 8,	'Target+Distribution+Timeperiod',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23485
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	21414
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	21415
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	22165
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	22234
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	23485
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	23487
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	28058
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	29444
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	30654
UNION
select 9,	'Target+Demo+DateDemo(LeftsideChip)',	6876,	'domestic vs international',	'Domestic', 	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	30670
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21414
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21415
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22165
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22234
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23485
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23487
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	28058
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	29444
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30654
UNION
select 10,	'Target+Demo+DateDemo(Timeperiod)',	6876,	'Periodic activities', 	'Animal Charades',	NULL,	NULL,	NULL,	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30670
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	21414
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	21415
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	22165
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	22234
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	23485
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	23487
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	28058
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	29444
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	30654
UNION
select 11,	'Target+Distribution+DateDemo(LeftSideChip)',	6876,	NULL,	NULL,	'transfer date',	'1/3/2016',	'12/31/2016',	'1',	NULL,	NULL,	NULL,	30670
UNION
select 12,	'Target+Distribution+DateDemo(HireDate)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Hire Date',	'1/1/2016',	'12/12/2017',	NULL,	30654
UNION
select 12,	'Target+Distribution+DateDemo(HireDate)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'Hire Date',	'1/1/2016',	'12/12/2017',	NULL,	23485
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21414
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	21415
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22165
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	22234
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23485
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23487
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	28058
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	29444
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30654
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?',	'None',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30670
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'More than one',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30654
UNION
select 13,	'Target+ Distribution+ Demo', 	6876,	'How Many #Dependents##(Not Including yourself or Your Spouse/Domestic partner) do you have?', 	'More than one',	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23485
UNION
select 14,	'Target+3 distribution', 	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	30654
UNION
select 14,	'Target+3 distribution', 	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	23485
UNION
select 14,	'Target+3 distribution', 	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	29444
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21414
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	21415
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22165
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	22234
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23485
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	23487
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	28058
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	29444
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30654
UNION
select 15,	'Target+TimePeriod+DateDemo(hireDate',	6876,	NULL,	NULL,	'HireDate',	'1/1/2016',	'12/12/2017',	'Survey Response Date',	'12/31/2016',	'12/31/2017',	NULL,	30670
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	21414
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	21415
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	22165
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	22234
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	23485
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	23487
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	28058
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	29444
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	30654
UNION
select 16,	'Benchmark: Technology-USA (275)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	275,	30670
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	21414
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	21415
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	22165
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	22234
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	23485
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	23487
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	28058
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	29444
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	30654
UNION
select 17,	'Benchmark: HealthCare-India (340)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	340,	30670
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	21414
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	21415
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	22165
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	22234
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	23485
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	23487
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	28058
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	29444
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	30654
UNION
select 18,	'Benchmark: Government-Other(341)',	6876,	NULL,	NULL,	NULL,	NULL,	NULL,	'1',	NULL,	NULL,	341,	30670
";
        #endregion

        #region Stg_ActualSPWithComparison_Stg
        public static string Stg_ActualSPWithComparison_Stg = @"
DROP TABLE IF EXISTS ActualSPWithComparison_Stg
CREATE TABLE ActualSPWithComparison_Stg
(
Sno 	INT				  NULL 	
,Target	NVARCHAR(MAX) 	  NULL
,Comp1	NVARCHAR(MAX)	  NULL
,comp2	NVARCHAR(MAX)	  NULL
,Comp3	NVARCHAR(MAX)	  NULL
)


insert into ActualSPWithComparison_Stg
select 18	,'1',	'16',	'7',	'8'
UNION
select 31	,'1',	'4',	'16',	'2'
UNION
select 32	,'1',	'4',	'16',	'7'
UNION
select 33	,'1',	'4',	'16',	'3'
UNION
select 34	,'1',	'4',	'16',	'8'
UNION
select 47	,'1',	'6',	'16',	'2'
UNION
select 48	,'1',	'6',	'16',	'7'
UNION
select 49	,'1',	'6',	'16',	'3'
UNION
select 50	,'1',	'6',	'16',	'8'
UNION
select 51	,'1',	'2',	'2',	'16'
UNION
select 52	,'1',	'2',	'7',	'16'
UNION
select 53	,'1',	'2',	'3',	'16'
UNION
select 54	,'1',	'2',	'8',	'16'
UNION
select 67	,'1',	'7',	'2',	'16'
UNION
select 68	,'1',	'7',	'7',	'16'
UNION
select 69	,'1',	'7',	'3',	'16'
UNION
select 70	,'1',	'7',	'8',	'16'
UNION
select 83	,'1',	'8',	'2',	'16'
UNION
select 84	,'1',	'8',	'7',	'16'
UNION
select 85	,'1',	'8',	'3',	'16'
UNION
select 86	,'1',	'8',	'8',	'16'
UNION
select 99	,'1',	'9',	'2',	'16'
UNION
select 100	,'1',	'9',	'7',	'16'
UNION
select 119	,'1',	'1',	'3',	'3'
UNION
select 120	,'1',	'2',	'10',	'11'
UNION
select 121	,'1',	'4',	'10',	'11'
UNION
select 122	,'1',	'10',	'11',	'12'
UNION
select 123	,'1',	'1',	'13',	'8'
UNION
select 125	,'1',	'3',	'14',	'8'
UNION
select 126	,'1',	'9',	'7'	,'10'
UNION
select 127	,'1',	'6',	'10',	'15'
UNION
select 128	,'2',	'2',	'2'	,'2'
UNION
select 129	,'3',	'3',	'7'	,'13'
UNION
select 130	,'4',	'4',	'7'	,'10'
UNION
select 131	,'5',	'8',	'3'	,'15'
UNION
select 132	,'6',	'16',	'18',	'9'
UNION
select 133	,'7',	'2',	'13',	'6'
UNION
select 134	,'8',	'3',	'14',	'17'
UNION
select 135	,'9',	'10',	'12',	'13'
UNION
select 136	,'10',	'5',	'6'	,'7'
UNION
select 137	,'11',	'11',	'11',	'11'
UNION
select 138	,'12',	'13',	'14',	'3'
UNION
select 139	,'13',	'2',	'7'	,'9'
UNION
select 140	,'14',	'3',	'1'	,'11'
UNION
select 141	,'15',	'16',	'17',	'18'
UNION
select 142	,'4',	NULL,NULL,NULL		
UNION
select 143	,'3',	'3',NULL,NULL		
UNION
select 144	,'9',	'1',	'12',NULL	
UNION
select 145	,'1',	'17',NULL,NULL		
UNION
select 146	,'1',	'16',	'17',NULL	
";
        #endregion

        #region QA_ActualSPWithComparison_Stg
        public static string QA_ActualSPWithComparison_Stg = @"
DROP TABLE IF EXISTS ActualSPWithComparison_Stg
CREATE TABLE ActualSPWithComparison_Stg
(
Sno 	INT				  NULL 	
,Target	NVARCHAR(MAX) 	  NULL
,Comp1	NVARCHAR(MAX)	  NULL
,comp2	NVARCHAR(MAX)	  NULL
,Comp3	NVARCHAR(MAX)	  NULL
)

Insert into ActualSPWithComparison_Stg

Select 16	,'1',	'16',	'7', '8'  		
UNION	
Select 31	,'1',	'4',	'16','2'
UNION	
Select 32	,'1',	'4',	'16','7'
UNION	
Select 33	,'1',	'4',	'16','3'
UNION	
Select 34	,'1',	'4',	'16','8'
UNION	
Select 47	,'1',	'6',	'16','2'
UNION	
Select 48	,'1',	'6',	'16','7'
UNION	
Select 49	,'1',	'6',	'16','3'
UNION	
Select 50	,'1',	'6',	'16','8'
UNION	
Select 51	,'1',	'2',	'2','16'
UNION	
Select 52	,'1',	'2',	'7','16'
UNION	
Select 53	,'1',	'2',	'3','16'
UNION	
Select 54	,'1',	'2',	'8','16'
UNION	
Select 67	,'1',	'7',	'2','16'
UNION	
Select 68	,'1',	'7',	'7','16'
UNION	
Select 69	,'1',	'7',	'3','16'
UNION	
Select 70	,'1',	'7',	'8','16'
UNION	
Select 83	,'1',	'8',	'2','16'
UNION	
Select 84	,'1',	'8',	'7','16'
UNION	
Select 85	,'1',	'8',	'3','16'
UNION	
Select 86	,'1',	'8',	'8','16'
UNION	
Select 99	,'1',	'9',	'2','16'
UNION	
Select 100	,'1',	'9',	'7','16'
UNION	
Select 119	,'1',	'1',	'3','3'
UNION	
Select 120	,'1',	'2',	'10','11'
UNION	
Select 121	,'1',	'4',	'10','11'
UNION	
Select 122	,'1',	'10',	'11','12'
UNION	
Select 123	,'1',	'1',	'13','8'
UNION	
Select 125	,'1',	'3',	'14','8'
UNION	
Select 126	,'1',	'9',	'7','10'
UNION	
Select 127	,'1',	'6'	,	'10','15'
UNION	
Select 128	,'2',	'2',	'2','2'
UNION	
Select 129	,'3',	'3',	'7','13'
UNION	
Select 130	,'4',	'4',	'7','10'
UNION	
Select 131	,'5',	'8',	'3','15'
UNION	
Select 133	,'7',	'2',	'13','6'
UNION	
Select 134	,'8',	'3',	'14','16'
UNION	
Select 135	,'9','10',	'12','13'
UNION	
Select 136	,'10',	'5',	'6','7'
UNION	
Select 137	,'11',	'11',	'11','11'
UNION	
Select 138	,'12',	'13',	'14','3'
UNION	
Select 139	,'13',	'2',	'7','9'
UNION	
Select 140	,'14',	'3',	'1','11'
UNION	
Select 142	,'4', NULL,NULL, NULL
UNION	
Select 143	,'3',	'3', NULL, NULL
UNION	
Select 144	,'9',	'1','12', NULL
UNION	
Select 145	,'1',	'16', NULL, NULL
";
        #endregion

        #region QA_V11Tables
        public static string QA_V11Tables = @"


DROP TABLE IF EXISTS Dimitem_V11
DROP TABLE IF EXISTS Dimparticipant_V11
DROP TABLE IF EXISTS FactResponse_V11
DROP TABLE IF EXISTS FactResponseComment_V11

SELECT * INTO Dimitem_V11 FROM  DimItem WHERE ClientId = 34767

Alter table Dimitem_V11
Add  SurveyFormID int

UPDATE DI
SET DI.SurveyFormID= Ds.SurveyFormID
FROM Dimitem_V11 As DI
INNER JOIN DimSurveySetting Ds
ON Ds.SurveyID= DI.SurveyId


SELECT * INTO Dimparticipant_V11 FROM  Dimparticipant WHERE ClientId = 34767

ALTER TABLE Dimparticipant_V11
ADD SurveyFormID int


UPDATE DP
SET DP.SurveyFormID =Ds.SurveyFormID
FROM DimParticipant_V11 DP
INNER JOIN  DimSurveySetting Ds
ON Ds.SurveyID= DP.SurveyId



SELECT * INTO FactResponse_V11  FROM FactResponse WHERE ClientId = 34767

ALTER TABLE FactResponse_V11
ADD SurveyFormID int


UPDATE FR
SET FR.SurveyFormID = Ds.SurveyFormID
FROM FactResponse_V11  FR
INNER JOIN DimSurveySetting Ds
ON Ds.SurveyID= FR.SurveyId




SELECT * INTO  FactResponseComment_V11 FROM FactResponseComment WHERE ClientId = 34767

ALTER TABLE  FactResponseComment_V11
ADD SurveyFormID int


UPDATE FR
SET FR.SurveyFormID = Ds.SurveyFormID
FROM FactResponseComment_V11  FR
INNER JOIN DimSurveySetting Ds
ON Ds.SurveyID= FR.SurveyId";
        #endregion

        #region Stg_V11Tables
        public static string Stg_V11Tables = @"


DROP TABLE IF EXISTS Dimitem_V11
DROP TABLE IF EXISTS Dimparticipant_V11
DROP TABLE IF EXISTS FactResponse_V11
DROP TABLE IF EXISTS FactResponseComment_V11

SELECT * INTO Dimitem_V11 FROM  DimItem WHERE ClientId = 29

Alter table Dimitem_V11
Add  SurveyFormID int

UPDATE DI
SET DI.SurveyFormID= Ds.SurveyFormID
FROM Dimitem_V11 As DI
INNER JOIN DimSurveySetting Ds
ON Ds.SurveyID= DI.SurveyId


SELECT * INTO Dimparticipant_V11 FROM  Dimparticipant WHERE ClientId = 29

ALTER TABLE Dimparticipant_V11
ADD SurveyFormID int


UPDATE DP
SET DP.SurveyFormID =Ds.SurveyFormID
FROM DimParticipant_V11 DP
INNER JOIN  DimSurveySetting Ds
ON Ds.SurveyID= DP.SurveyId



SELECT * INTO FactResponse_V11  FROM FactResponse WHERE ClientId = 29

ALTER TABLE FactResponse_V11
ADD SurveyFormID int


UPDATE FR
SET FR.SurveyFormID = Ds.SurveyFormID
FROM FactResponse_V11  FR
INNER JOIN DimSurveySetting Ds
ON Ds.SurveyID= FR.SurveyId




SELECT * INTO  FactResponseComment_V11 FROM FactResponseComment WHERE ClientId = 29

ALTER TABLE  FactResponseComment_V11
ADD SurveyFormID int


UPDATE FR
SET FR.SurveyFormID = Ds.SurveyFormID
FROM FactResponseComment_V11  FR
INNER JOIN DimSurveySetting Ds
ON Ds.SurveyID= FR.SurveyId";
        #endregion

        #region QAPROC_DemographicReportCalcQuestions_V11
        public static string QAPROC_DemographicReportCalcQuestions_V11 = @"
CREATE OR ALTER       PROCEDURE [dbo].[QAPROC_DemographicReportCalcQuestions_V11] (
	@SurveyFormID INT
	,@SurveyIDQuery VARCHAR(1000)
	,@DemoQuery NVARCHAR(MAX)
	,@TimePeriodQuery NVARCHAR(MAX)
	,@DemoColumn VARCHAR(1000)
	,@DemoValue VARCHAR(1000)
	,@SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
	--,@ItemID BIGINT
	)
AS
BEGIN
	SET NOCOUNT ON;


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--DECLARE @DemoID VARCHAR(100)
DECLARE @SQL NVARCHAR(MAX)
DECLARE @QuestionAvg NUMERIC(10,2)
DECLARE @QuestionFav NUMERIC(10,0)
DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
DECLARE @compRuleSettingType INT
DECLARE @FavCnt5 BIGINT
DECLARE @FavCnt05 BIGINT
DECLARE @FavCnt7 BIGINT
DECLARE @TotCnt5 BIGINT
DECLARE @TotCnt05 BIGINT
DECLARE @TotCnt7 BIGINT

	SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

	IF @SurveyFormItemIdQuery IS NULL
	BEGIN
		SET @SurveyFormItemIdQuery = '1 = 1'
	END

IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
	ItemID BIGINT
	,ParticipantID BIGINT
	,Response INT
	)

--INSERT INTO #FactResponse

SET @SQL = 'SELECT SurveyFormItemID
	,SurveyWaveParticipantID
	,ScaleOptionID
FROM FactResponse_V11 FR WITH (NOLOCK)
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND FR.' + @SurveyIDQuery + '
AND '+@TimePeriodQuery+' 
AND '+@SurveyFormItemIdQuery+'
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND ' + @SurveyIDQuery + '
AND '+@DemoColumn+' = ' + ''''+@DemoValue+'''' + '
AND '+@DemoQuery+' 
AND '+@CompletionRuleQuery+' 
AND [Status]=1)'



PRINT @SQL

INSERT INTO #FactResponse
EXEC(@SQL)

--SELECT * FROM #FactResponse

UPDATE fr
SET fr.response = di.scaleoptionvalue
FROM #FactResponse fr
INNER JOIN dimitem_V11 di ON fr.response = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID

IF OBJECT_ID('tempdb..#MeanCalculation7') IS NOT NULL
    DROP TABLE #MeanCalculation7
CREATE TABLE #MeanCalculation7 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,ResponseOption6 INT
	,ResponseOption7 INT
	,NA INT
	)

	
INSERT INTO #MeanCalculation7
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[6]
	,[7]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 7)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[6]
			,[7]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

IF OBJECT_ID('tempdb..#MeanCalculation5') IS NOT NULL
    DROP TABLE #MeanCalculation5
CREATE TABLE #MeanCalculation5 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation5
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) != 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

	
IF OBJECT_ID('tempdb..#MeanCalculation05') IS NOT NULL
    DROP TABLE #MeanCalculation05
CREATE TABLE #MeanCalculation05 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption0 INT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation05
SELECT itemid
	,participantid
	,[0]
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) = 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[0]
			,[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

	
;WITH FavResults
(
	[ObjectID]
	,[Favorable]
)
AS
(
	SELECT mc.ItemID [ObjectID],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),((SUM(mc.ResponseOption4) + SUM(mc.ResponseOption5))  * 100 )) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1, CONVERT(NUMERIC(10, 2),(SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Favorable]
	
	FROM #MeanCalculation5 mc
	GROUP BY mc.ItemID
	UNION
	SELECT mc.ItemID [ObjectID],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),((SUM(mc.ResponseOption4) + SUM(mc.ResponseOption5))  * 100 )) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1, CONVERT(NUMERIC(10, 2),(SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Favorable]
	
	FROM #MeanCalculation05 mc
	GROUP BY mc.ItemID
	UNION
	SELECT mc.ItemID [ObjectID],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),((SUM(mc.ResponseOption5) + SUM(mc.ResponseOption6) + SUM(mc.ResponseOption7))  * 100 )) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))) = 0,1, CONVERT(NUMERIC(10, 2),(SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))))) [Favorable]
	
	FROM #MeanCalculation7 mc
	GROUP BY mc.ItemID
),
AveResults
(
	[ObjectID]
	,[Mean]
)
AS
(
	SELECT  itemid [ObjectID]
		,CONVERT(NUMERIC(10, 2),CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5)) 
		/  IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) 
		[Mean]
	FROM #MeanCalculation5
	GROUP BY itemid
	UNION
	SELECT  itemid [ObjectID]
		,CONVERT(NUMERIC(10, 2),CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) * 0 + SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5)) 
		/  IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) 
		[Mean]
	FROM #MeanCalculation05
	GROUP BY itemid
	UNION
	SELECT  itemid [ObjectID]
		,CONVERT(NUMERIC(10, 2),CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5 + SUM(ResponseOption6) * 6 + SUM(ResponseOption7) * 7)) 
		/  IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))))) 
		[Mean]
	FROM #MeanCalculation7
	GROUP BY itemid
)
Insert into #DemoQuestionScores
SELECT F.ObjectID, @DemoValue [Demo Value], F.Favorable, A.Mean FROM FavResults F INNER JOIN AveResults A
ON F.ObjectID = A.ObjectID
ORDER BY F.ObjectID ASC
END";
        #endregion

        #region QAPROC_ReportCalculationNonRatingScale_V11
        public static string QAPROC_ReportCalculationNonRatingScale_V11 = @"
CREATE OR ALTER  PROCEDURE [dbo].[QAPROC_ReportCalculationNonRatingScale_V11] (
	@SurveyFormID BIGINT,
	@SurveyIDs VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX)
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
	DECLARE @compRuleSettingType INT
	DECLARE @ResponseCount INT
	DECLARE @InsertQuery NVARCHAR(MAX)
    DECLARE @GroupMinimumNSize NVARCHAR(MAX)

	
SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')
SELECT @GroupMinimumNSize = GroupMinimumSize FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID

DECLARE @SurveyIDQuery NVARCHAR(MAX)

		IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END


IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
	ItemID BIGINT
	,ParticipantID BIGINT
	,Response NVARCHAR(MAX)
	,ResponseID BIGINT
	)
IF OBJECT_ID('tempdb..#NonMultiselectResponse') IS NOT NULL
    DROP TABLE #NonMultiselectResponse
CREATE TABLE #NonMultiselectResponse (
	SurveyformItemID BIGINT
	,ScaleOptionId BIGINT
	,[Response Rate]BIGINT
	,ScaleResponses BIGINT
	,[Total Responses] INT
	,RespondentCount BIGINT
	,IsSuppressed TINYINT 
	)

IF OBJECT_ID('tempdb..#MultiselectResponse') IS NOT NULL
    DROP TABLE #MultiselectResponse
CREATE TABLE #MultiselectResponse (
	SurveyformItemID BIGINT
	,ScaleOptionId BIGINT
	,[Response Rate] BIGINT
	,ScaleResponses BIGINT
	,[Total Responses] INT
	,RespondentCount BIGINT
	,IsSuppressed TINYINT
	)

SET @InsertQuery = 'SELECT SurveyFormItemID  
 ,SurveyWaveParticipantID  
 ,''''
 ,ScaleOptionID  
FROM FactResponse_V11 FR WITH (NOLOCK)  
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
AND FR.' + @SurveyIDQuery + '  
AND '+@TimePeriodQuery+'   
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
AND ' + @SurveyIDQuery + '  
AND '+@DemoQuery+'   
AND '+@CompletionRuleQuery+'   
AND [Status]=1)'

INSERT INTO #FactResponse
EXEC(@InsertQuery)

UPDATE fr
SET fr.response = di.ScaleOptionText
FROM #FactResponse fr
INNER JOIN DimItem_V11 di ON fr.ResponseID = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID
and di.categoryText Not in('Demographic')


SELECT @ResponseCount = COUNT(DISTINCT ParticipantID) FROM #FactResponse


;with cte as
(
SELECT fr.ItemID, COUNT(*) [RespondentCount] FROM #FactResponse fr 
GROUP BY fr.ItemID
)
,
cte2 as
(
SELECT fr.ItemID,fr.ResponseID,fr.Response,COUNT(*) [ScaleResponses] FROM #FactResponse fr
INNER JOIN cte ON cte.ItemID = fr.ItemID
Where fr.ResponseID in (Select ScaleOptionId from Dimitem_V11 where  itemstyleName not in ('MultiSelect')  and GroupName is NULL and Iscomment=0)
GROUP BY fr.ItemID,fr.ResponseID,fr.Response
)

Insert into #NonMultiselectResponse
select cte.ItemID [SurveyFormItemID], cte2.ResponseID [ScaleOptionID], Convert(Int,Convert(Numeric(5,0),(cte2.ScaleResponses * 1.00 * 100/cte.RespondentCount * 1.00))) [Response Rate], cte2.ScaleResponses [ScaleResponses], @ResponseCount [Total Responses],cte.RespondentCount,
CASE 
WHEN @ResponseCount >= @GroupMinimumNSize THEN 0 
ELSE 1
END [IsSuppressed]
from cte inner join cte2 on cte.ItemID = cte2.ItemID
ORDER BY cte.ItemID ASC


;with cte3 as
(
SELECT fr.ItemID, COUNT(*) [RespondentCount] FROM #FactResponse fr 
GROUP BY fr.ItemID
),
cte5 as
(
SELECT fr.ItemID, COUNT(distinct participantID)[ResponseCount] FROM #FactResponse fr 
GROUP BY fr.ItemID
)

,Cte4 as
(
SELECT fr.ItemID,fr.ResponseID,fr.Response,COUNT(*) [ScaleResponses] FROM #FactResponse fr
INNER JOIN cte3 ON cte3.ItemID = fr.ItemID
Where fr.ResponseID in (Select ScaleOptionId from Dimitem_V11 where  itemstyleName in ('MultiSelect') and GroupName is NULL and Iscomment=0)
GROUP BY fr.ItemID,fr.ResponseID,fr.Response
)
Insert into  #MultiselectResponse
select cte3.ItemID [SurveyFormItemID], cte4.ResponseID [ScaleOptionID], Convert(Int,Convert(Numeric(5,0),(Cte4.ScaleResponses * 1.00 * 100/ResponseCount *1.00))) [Response Rate], cte4.ScaleResponses [ScaleResponses],ResponseCount[Total Responses],cte3.RespondentCount,
CASE 
WHEN @ResponseCount >= @GroupMinimumNSize THEN 0 
ELSE 1
END [IsSuppressed]
from cte3 inner join cte4 on cte3.ItemID = cte4.ItemID
inner join Cte5 on Cte5.itemID= Cte3.itemID
ORDER BY cte3.ItemID ASC

Select * from  #NonMultiselectResponse
UNION ALL
Select * from  #MultiselectResponse

END";
        #endregion

        #region QAPROC_TrendReport_Rating_V11_New
        public static string QAPROC_TrendReport_Rating_V11_New = @"
CREATE OR ALTER   PROCEDURE [dbo].[QAPROC_TrendReport_Rating_V11_New] (
	-- Add the parameters for the stored procedure here
	@SurveyFormID bigint, 
	@SurveyIDQuery VARCHAR(1000),
	@StartTime NVARCHAR(MAX),
	@EndTime NVARCHAR(MAX),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX),
	@ItemOrCategory NVARCHAR(MAX),
	@IsTimePeriodDateDemoEnabled INT,
	@SurveyID VARCHAR(100)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Insert statements for procedure here

	DECLARE @Group NVARCHAR(200) = ''
	DECLARE @ShadeStatus INT
	DECLARE @ResponseCount INT
	DECLARE @OverFav INT
	DECLARE @OverMean NUMERIC(10,2)
	DECLARE @Shading NVARCHAR(100) = ''
	DECLARE @InsertQuery NVARCHAR(MAX)
	DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
	DECLARE @compRuleSettingType INT
	DECLARE @FavCnt5 BIGINT
	DECLARE @FavCnt05 BIGINT
	DECLARE @FavCnt7 BIGINT
	DECLARE @TotCnt5 BIGINT
	DECLARE @TotCnt05 BIGINT
	DECLARE @TotCnt7 BIGINT
	DECLARE @SQLQuery5 NVARCHAR(MAX)
	DECLARE @SQLQuery05 NVARCHAR(MAX)
	DECLARE @SQLQuery7 NVARCHAR(MAX)
    DECLARE @GroupMinimumNSize INT
	DECLARE @NoOfItemsInCategory INT
	DECLARE @CountSQL NVARCHAR(1000)

	IF DATEDIFF (DAY, @StartTime, @EndTime) < 31
	SET @Group = SUBSTRING(DATENAME(MONTH, @StartTime),1,3) + ' ' + CONVERT(VARCHAR(100),DATEPART(YEAR, @StartTime))
ELSE IF DATEDIFF (DAY, @StartTime, @EndTime) BETWEEN 88 AND 92
	BEGIN
		IF DATEPART(MONTH,@StartTime) = 1
			SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime))
		ELSE IF DATEPART(MONTH,@StartTime) = 4
			SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime))
		ELSE IF DATEPART(MONTH,@StartTime) = 7
			SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime))
		ELSE IF DATEPART(MONTH,@StartTime) = 10
			SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime))
	END
ELSE IF DATEDIFF (DAY, @StartTime, @EndTime) BETWEEN 364 AND 367
	SET @Group = DATEPART(YEAR,@StartTime)

	SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

    SELECT @GroupMinimumNSize = GroupMinimumSize FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CountSQL = 'SELECT @Count = COUNT(DISTINCT SurveyFormItemID) FROM Dimitem_V11 WHERE SurveyFormID = ' + CONVERT(VARCHAR(10),@SurveyFormID) + ' AND ' + @ItemOrCategory +' AND GroupName = ''RatingScale'''
	EXEC sp_executesql @CountSQL , N'@Count INT OUT', @NoOfItemsInCategory OUT
	
IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
	ItemID BIGINT
	,ParticipantID BIGINT
	,Response INT
	)

--INSERT INTO #FactResponse
IF(@IsTimePeriodDateDemoEnabled != 1)
BEGIN
	IF @SurveyID = ''
	BEGIN
	SELECT @InsertQuery = 
		'SELECT SurveyFormItemID
			,SurveyWaveParticipantID
			,ScaleOptionID
		FROM FactResponse_V11 FR WITH (NOLOCK)
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
		AND FR.' + @SurveyIDQuery + '
		AND '+@TimePeriodQuery+' 
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
		AND ' + @SurveyIDQuery + '
		AND '+@DemoQuery+' 
		AND '+@CompletionRuleQuery+' 
		AND [Status]=1)
		AND FR.UpdateTimestampGMT >= CONVERT(DATETIME,'''+@StartTime+''') AND FR.UpdateTimestampGMT <= CONVERT(DATETIME,'''+@EndTime+''')'
	END
	ELSE
	BEGIN
	SELECT @InsertQuery = 
		'SELECT SurveyFormItemID
			,SurveyWaveParticipantID
			,ScaleOptionID
		FROM FactResponse_V11 FR WITH (NOLOCK)
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
		AND FR.' + @SurveyIDQuery + '
		AND '+@TimePeriodQuery+' 
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyID = '+CONVERT(VARCHAR(10),@SurveyID)+'
		AND ' + @SurveyIDQuery + '
		AND '+@DemoQuery+' 
		AND '+@CompletionRuleQuery+' 
		AND [Status]=1)
		AND FR.UpdateTimestampGMT >= CONVERT(DATETIME,'''+@StartTime+''') AND FR.UpdateTimestampGMT <= CONVERT(DATETIME,'''+@EndTime+''')'
	END
END
ELSE
BEGIN
	DECLARE @DemoLabelSQL NVARCHAR(1000)
	DECLARE @DemoTimePeriodQuery NVARCHAR(MAX) = ''
	DECLARE @MaxSQL NVARCHAR(MAX) = ''
	SELECT @DemoLabelSQL = SUBSTRING(@DemoQuery, 1, CHARINDEX('BETWEEN', @DemoQuery)-2)
	SET @DemoTimePeriodQuery = @DemoLabelSQL + ' BETWEEN CONVERT(DATETIME,'''+@StartTime+''') AND CONVERT(DATETIME,'''+@EndTime+''')'
	PRINT @DemoTimePeriodQuery
	IF @SurveyID = ''
	BEGIN
	SELECT @InsertQuery = 
		'SELECT SurveyFormItemID
			,SurveyWaveParticipantID
			,ScaleOptionID
		FROM FactResponse_V11 FR WITH (NOLOCK)
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
		AND FR.' + @SurveyIDQuery + '
		AND '+@TimePeriodQuery+' 
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
		AND ' + @SurveyIDQuery + '
		AND '+@DemoQuery+' 
		AND '+@CompletionRuleQuery+' 
		AND '+@DemoTimePeriodQuery+'
		AND [Status]=1)
		'
	END
	ELSE
	BEGIN
	SELECT @InsertQuery = 
		'SELECT SurveyFormItemID
			,SurveyWaveParticipantID
			,ScaleOptionID
		FROM FactResponse_V11 FR WITH (NOLOCK)
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
		AND FR.' + @SurveyIDQuery + '
		AND '+@TimePeriodQuery+' 
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyID = '+CONVERT(VARCHAR(10),@SurveyID)+'
		AND ' + @SurveyIDQuery + '
		AND '+@DemoQuery+' 
		AND '+@CompletionRuleQuery+' 
		AND '+@DemoTimePeriodQuery+'
		AND [Status]=1)
		'
	END
END

INSERT INTO #FactResponse
EXEC(@InsertQuery)

UPDATE fr
SET fr.response = di.scaleoptionvalue
FROM #FactResponse fr
INNER JOIN dimitem_V11 di ON fr.response = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID

IF OBJECT_ID('tempdb..#MeanCalculation7') IS NOT NULL
    DROP TABLE #MeanCalculation7
CREATE TABLE #MeanCalculation7 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,ResponseOption6 INT
	,ResponseOption7 INT
	,NA INT
	)

	

SET @SQLQuery7 = 
'SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[6]
	,[7]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID) + ' AND ' + @ItemOrCategory + ' AND GroupName = ''RatingScale'' GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 7)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[6]
			,[7]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;'

INSERT INTO #MeanCalculation7
EXEC(@SQLQuery7)

IF OBJECT_ID('tempdb..#MeanCalculation5') IS NOT NULL
    DROP TABLE #MeanCalculation5
CREATE TABLE #MeanCalculation5 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)


SET @SQLQuery5 = 
'SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID) + ' AND ' + @ItemOrCategory + ' AND GroupName = ''RatingScale'' GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) != 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;'

INSERT INTO #MeanCalculation5
EXEC(@SQLQuery5)

IF OBJECT_ID('tempdb..#MeanCalculation05') IS NOT NULL
    DROP TABLE #MeanCalculation05
CREATE TABLE #MeanCalculation05 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption0 INT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)


SET @SQLQuery05 = 
'SELECT itemid
	,participantid
	,[0]
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID) + ' AND ' + @ItemOrCategory + ' AND GroupName = ''RatingScale'' GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) = 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[0]
			,[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;'

INSERT INTO #MeanCalculation05
EXEC(@SQLQuery05)


-- Overall Percentage
SELECT @FavCnt5 = (SUM(ResponseOption4) + SUM(ResponseOption5))  FROM #MeanCalculation5
SELECT @FavCnt05 = (SUM(ResponseOption4) + SUM(ResponseOption5)) FROM #MeanCalculation05
SELECT @FavCnt7 = (SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)) FROM #MeanCalculation7

SELECT @TotCnt5 = IIF ((SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)) = 0, 1, (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)))  
FROM #MeanCalculation5
SELECT @TotCnt05 = IIF ((SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)) = 0, 1, (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)))  
FROM #MeanCalculation05
SELECT @TotCnt7 = IIF ((SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)) = 0, 1, (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)))  
FROM #MeanCalculation7

SELECT @OverFav =   CONVERT(NUMERIC(10,0),(ISNULL(@FavCnt5,0) + ISNULL(@FavCnt05,0) + ISNULL(@FavCnt7,0)) * 100.00 / IIF((ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0))  = 0, 1,(ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0)) * 1.00))


-- Overall Average
;with ResultTable AS
(
SELECT ResponseOption0 [Scale0], ResponseOption1 [Scale1], ResponseOption2 [Scale2], ResponseOption3 [Scale3], ResponseOption4 [Scale4], ResponseOption5 [Scale5], 0 [Scale6], 0 [Scale7] FROM #MeanCalculation05
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,0,0 FROM #MeanCalculation5
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,ResponseOption6,ResponseOption7 FROM #MeanCalculation7
)
SELECT @OverMean = CONVERT(NUMERIC(10,2),(SUM(Scale0) * 0.00 + SUM(Scale1) * 1.00 + SUM(Scale2) * 2.00 + SUM(Scale3) * 3.00 + SUM(Scale4) * 4.00 + SUM(Scale5) * 5.00 + SUM(Scale6) * 6.00 + SUM(Scale7) * 7.00) /
IIF ((SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00  = 0, 1, (SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00 )  )
FROM ResultTable

DECLARE @TotalResponses INT

DECLARE @TotCntWithNA5 BIGINT
DECLARE @TotCntWithNA05 BIGINT
DECLARE @TotCntWithNA7 BIGINT

SELECT @TotCntWithNA5 =  SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA) FROM #MeanCalculation5
SELECT @TotCntWithNA05 =  SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA) FROM #MeanCalculation05
SELECT @TotCntWithNA7 = SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7) + SUM(NA) FROM #MeanCalculation7
SELECT @TotalResponses = ISNULL(@TotCntWithNA5,0) + ISNULL(@TotCntWithNA05,0) + ISNULL(@TotCntWithNA7,0)

;WITH tblResponses
(
SurveyWaveParticipantID
)
AS
(
SELECT ParticipantID FROM #MeanCalculation5
UNION
SELECT ParticipantID FROM #MeanCalculation05
UNION
SELECT ParticipantID FROM #MeanCalculation7
)
SELECT @ResponseCount = COUNT(DISTINCT SurveyWaveParticipantID) FROM tblResponses


INSERT INTO #TrendResult
SELECT IIF(@Group = '',CONVERT(NVARCHAR(100),@SurveyID),@Group), @OverFav, @OverMean, @ResponseCount, @TotalResponses,
CASE 
WHEN CONVERT(NUMERIC(10,0),(@ResponseCount * 1.00/@NoOfItemsInCategory)) >= @GroupMinimumNSize THEN 1 
ELSE 0
END [GroupMinimumSizeMet]

END";
        #endregion

        #region TrendSPExecution_Year
        public static string TrendSPExecution_Year = @"
CREATE OR ALTER   PROCEDURE [dbo].[TrendSPExecution_Year] (
	@SurveyFormID INT,
	@DemoQuery NVARCHAR(MAX),
	@TImePeriodQuery NVARCHAR(MAX),
	@ItemOrCategory NVARCHAR(MAX),
	@IsTimePeriodDateDemoEnabled INT,
	@SurveyIDs NVARCHAR(MAX)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @StartDate NVARCHAR(MAX)  
	DECLARE @EndDate NVARCHAR(MAX)  

	
IF OBJECT_ID('tempdb..#YearDateList') IS NOT NULL
	DROP TABLE #YearDateList

IF OBJECT_ID('tempdb..#TrendResult') IS NULL
CREATE TABLE #TrendResult (
	[GroupBy] NVARCHAR(100)
	,[Overall Favorability] NUMERIC(10,0)
	,[Overall Mean] NUMERIC(10,2)
	,Responses INT
    ,TotalResponses INT
    ,[GroupMinimumSizeMet] INT 
	)

CREATE TABLE #YearDateList (
	ID INT IDENTITY(1,1)
	,StartDate DATETIME
	,EndDate DATETIME
	)

	DECLARE @SurveyIDQuery NVARCHAR(MAX)

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END

	IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END


SELECT @StartDate = DATEADD(MONTH,-DATEPART(MONTH,@StartDate)+1,@StartDate) 


;WITH cte AS (
SELECT CASE WHEN DATEPART(Day,@StartDate) = 1 THEN @StartDate 
            ELSE DATEADD(DAY,-DATEPART(Day,@StartDate)+1,@StartDate) END AS myDate
UNION ALL
SELECT DATEADD(Month,12,myDate)
FROM cte
WHERE DATEADD(Month,12,myDate) <=  @EndDate
)
INSERT INTO #YearDateList
SELECT myDate, EOMONTH(DATEADD(MONTH,11,myDate))
FROM cte
OPTION (MAXRECURSION 0)

UPDATE #YearDateList
SET EndDate = DATEADD(SECOND,86399,EndDate)

DECLARE @StartTime NVARCHAR(MAX) 
DECLARE @EndTime NVARCHAR(MAX) 
DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #YearDateList 
	WHERE ID > @id ORDER BY ID 
	IF @@ROWCOUNT = 0 BREAK;  
	SELECT @StartTime = StartDate, @EndTime = EndDate FROM #YearDateList WHERE ID = @id
	EXEC QAPROC_TrendReport_Rating_V11_New @SurveyFormID,@SurveyIDQuery, @StartTime, @EndTime, @DemoQuery, @TImePeriodQuery, @ItemOrCategory, @IsTimePeriodDateDemoEnabled, ''
END

SELECT DISTINCT [GroupBy], [Overall Favorability], [Overall Mean], Responses, TotalResponses, GroupMinimumSizeMet FROM #TrendResult
WHERE [Overall Favorability] IS NOT NULL AND [Overall Mean] IS NOT NULL
	    
END	   ";
        #endregion

        #region TrendSPExecution_Quarter
        public static string TrendSPExecution_Quarter = @"
CREATE OR ALTER   PROCEDURE [dbo].[TrendSPExecution_Quarter] (
	@SurveyFormID INT,
	@DemoQuery NVARCHAR(MAX),
	@TImePeriodQuery NVARCHAR(MAX),
	@ItemOrCategory NVARCHAR(MAX),
	@IsTimePeriodDateDemoEnabled INT,
	@SurveyIDS NVARCHAR(MAX)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @StartDate NVARCHAR(MAX)  
	DECLARE @EndDate NVARCHAR(MAX)  

	
IF OBJECT_ID('tempdb..#QuarterDateList') IS NOT NULL
	DROP TABLE #QuarterDateList

IF OBJECT_ID('tempdb..#TrendResult') IS NULL
CREATE TABLE #TrendResult (
	[GroupBy] NVARCHAR(100)
	,[Overall Favorability] NUMERIC(10,0)
	,[Overall Mean] NUMERIC(10,2)
	,Responses INT
    ,TotalResponses INT
    ,[GroupMinimumSizeMet] INT 
	)

CREATE TABLE #QuarterDateList (
	ID INT IDENTITY(1,1)
	,StartDate DATETIME
	,EndDate DATETIME
	)

	DECLARE @SurveyIDQuery NVARCHAR(MAX)

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END

IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END


IF DATEPART(MONTH,@StartDate) NOT IN (1,4,7,10)
BEGIN
	IF DATEPART(MONTH,@StartDate) IN (2,5,8,11)
		SELECT @StartDate = DATEADD(MONTH,-1,@StartDate)
	ELSE IF DATEPART(MONTH,@StartDate) IN (3,6,9,12)
		SELECT @StartDate = DATEADD(MONTH,-2,@StartDate)
END


;WITH cte AS (
SELECT CASE WHEN DATEPART(Day,@StartDate) = 1 THEN @StartDate 
            ELSE DATEADD(DAY,-DATEPART(Day,@StartDate)+1,@StartDate) END AS myDate
UNION ALL
SELECT DATEADD(Month,3,myDate)
FROM cte
WHERE DATEADD(Month,3,myDate) <=  @EndDate
)
INSERT INTO #QuarterDateList
SELECT myDate, EOMONTH(DATEADD(MONTH,2,myDate))
FROM cte
OPTION (MAXRECURSION 0)

UPDATE #QuarterDateList
SET EndDate = DATEADD(SECOND,86399,EndDate)


DECLARE @StartTime NVARCHAR(MAX) 
DECLARE @EndTime NVARCHAR(MAX) 
DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #QuarterDateList 
	WHERE ID > @id ORDER BY ID 
	IF @@ROWCOUNT = 0 BREAK;  
	SELECT @StartTime = StartDate, @EndTime = EndDate FROM #QuarterDateList WHERE ID = @id
	EXEC QAPROC_TrendReport_Rating_V11_New @SurveyFormID,@SurveyIDQuery, @StartTime, @EndTime, @DemoQuery, @TImePeriodQuery, @ItemOrCategory, @IsTimePeriodDateDemoEnabled, ''
END

SELECT DISTINCT [GroupBy], [Overall Favorability], [Overall Mean], Responses, TotalResponses, GroupMinimumSizeMet FROM #TrendResult
WHERE [Overall Favorability] IS NOT NULL AND [Overall Mean] IS NOT NULL
	    
END	   ";
        #endregion

        #region TrendSPExecution_Month
        public static string TrendSPExecution_Month = @"
CREATE OR ALTER   PROCEDURE [dbo].[TrendSPExecution_Month] (
	@SurveyFormID INT,
	@DemoQuery NVARCHAR(MAX),
	@TImePeriodQuery NVARCHAR(MAX),
	@ItemOrCategory NVARCHAR(MAX),
	@IsTimePeriodDateDemoEnabled INT,
	@SurveyIDs NVARCHAR(MAX)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @StartDate NVARCHAR(MAX)  
	DECLARE @EndDate NVARCHAR(MAX)  

	
IF OBJECT_ID('tempdb..#DateList') IS NOT NULL
	DROP TABLE #DateList

IF OBJECT_ID('tempdb..#TrendResult') IS NULL
CREATE TABLE #TrendResult (
	[GroupBy] NVARCHAR(100)
	,[Overall Favorability] NUMERIC(10,0)
	,[Overall Mean] NUMERIC(10,2)
	,Responses INT
    ,TotalResponses INT
    ,[GroupMinimumSizeMet] INT 
	)

CREATE TABLE #DateList (
	ID INT IDENTITY(1,1)
	,StartDate DATETIME
	,EndDate DATETIME
	)


	DECLARE @SurveyIDQuery NVARCHAR(MAX)

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END

	IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END

;WITH cte AS (
SELECT CASE WHEN DATEPART(Day,@StartDate) = 1 THEN @StartDate 
            ELSE DATEADD(DAY,-DATEPART(Day,@StartDate)+1,@StartDate) END AS myDate
UNION ALL
SELECT DATEADD(Month,1,myDate)
FROM cte
WHERE DATEADD(Month,1,myDate) <=  @EndDate
)
INSERT INTO #DateList
SELECT myDate, EOMONTH(myDate)
FROM cte
OPTION (MAXRECURSION 0)

UPDATE #DateList
SET EndDate = DATEADD(SECOND,86399,EndDate)


DECLARE @StartTime NVARCHAR(MAX) 
DECLARE @EndTime NVARCHAR(MAX) 
DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #DateList 
	WHERE ID > @id ORDER BY ID 
	IF @@ROWCOUNT = 0 BREAK;  
	SELECT @StartTime = StartDate, @EndTime = EndDate FROM #DateList WHERE ID = @id
	EXEC QAPROC_TrendReport_Rating_V11_New @SurveyFormID,@SurveyIDQuery, @StartTime, @EndTime, @DemoQuery, @TImePeriodQuery, @ItemOrCategory, @IsTimePeriodDateDemoEnabled, ''
END

SELECT DISTINCT [GroupBy], [Overall Favorability], [Overall Mean], Responses, TotalResponses, GroupMinimumSizeMet FROM #TrendResult
WHERE [Overall Favorability] IS NOT NULL AND [Overall Mean] IS NOT NULL
	    
END	   
";
        #endregion

        #region TrendSPExecution_Distribution
        public static string TrendSPExecution_Distribution = @"
CREATE OR ALTER   PROCEDURE [dbo].[TrendSPExecution_Distribution] (
	@SurveyFormID INT,
	@DemoQuery NVARCHAR(MAX),
	@TImePeriodQuery NVARCHAR(MAX),
	@ItemOrCategory NVARCHAR(MAX),
	@IsTimePeriodDateDemoEnabled INT,
	@SurveyIDs NVARCHAR(MAX)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @StartDate NVARCHAR(MAX)  
	DECLARE @EndDate NVARCHAR(MAX)  


	
IF OBJECT_ID('tempdb..#SurveyLists') IS NOT NULL
	DROP TABLE #SurveyLists

IF OBJECT_ID('tempdb..#TrendResult') IS NULL
CREATE TABLE #TrendResult (
	[GroupBy] NVARCHAR(100)
	,[Overall Favorability] NUMERIC(10,0)
	,[Overall Mean] NUMERIC(10,2)
	,Responses INT
    ,TotalResponses INT
    ,[GroupMinimumSizeMet] INT 
	)

CREATE TABLE #SurveyLists (
	ID INT IDENTITY(1,1)
	,SurveyID INT
	)

	
	DECLARE @SurveyIDQuery NVARCHAR(MAX)

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END

	IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END


Declare @SurveyId Bigint;
INSERT INTO #SurveyLists
SELECT SurveyID FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID AND SurveyWaveStatus = 1

DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #SurveyLists 
	WHERE ID > @id ORDER BY ID 
	IF @@ROWCOUNT = 0 BREAK;	
	SELECT @SurveyId = SurveyID FROM #SurveyLists WHERE ID = @id

	EXEC QAPROC_TrendReport_Rating_V11_New @SurveyFormID,@SurveyIDQuery, @StartDate, @EndDate, @DemoQuery, @TImePeriodQuery,@ItemOrCategory, @IsTimePeriodDateDemoEnabled, @SurveyID
END

SELECT DISTINCT [GroupBy], [Overall Favorability], [Overall Mean], Responses, TotalResponses, GroupMinimumSizeMet FROM #TrendResult
WHERE [Overall Favorability] IS NOT NULL AND [Overall Mean] IS NOT NULL
	    
END	   
";
        #endregion

        #region QAPROC_GetTestSPIDFromScenarios_V11
        public static string QAPROC_GetTestSPIDFromScenarios_V11 = @"
CREATE OR ALTER   PROCEDURE [dbo].[QAPROC_GetTestSPIDFromScenarios_V11] (
	@UserID BIGINT,
	@ClientID BIGINT,
	@SurveyFormID BIGINT,
	@CompletedtheSurvey nvarchar(max)
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @insertquery NVARCHAR(max)
	 print @CompletedtheSurvey
	
	SELECT @insertquery =
	'SELECT uaf.UserAnalyzeFilterID, uaf.GroupTypeID, uaf.GroupOrder, xref.ScenarioID, s.'+@CompletedTheSurvey+' FROM UserAnalyzeFilter uaf 
	INNER JOIN tblScenarioUserAnalyzeFilterXref xref ON uaf.UserAnalyzeFilterID = xref.UserAnalyzeFilterID
	INNER JOIN tblTestScenario s ON xref.ScenarioId = s.ScenarioID
	WHERE uaf.UserID ='+Convert(VARCHAR(10),@UserID)+' AND uaf.ClientID ='+Convert(VARCHAR(10),@ClientID)+' AND uaf.SurveyFormID ='+Convert(VARCHAR(10),@SurveyformID)+
	'ORDER BY uaf.GroupOrder ASC'

    print @insertquery
	Exec (@insertquery)

END


";
        #endregion

        #region Parent_SP_QAPROC_ResultsReport_V11
        public static string Parent_SP_QAPROC_ResultsReport_V11 = @"
CREATE OR ALTER     PROCEDURE [dbo].[Parent_SP_QAPROC_ResultsReport_V11] (
	@SurveyFormID BIGINT,
	@SurveyIDs VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	EXEC [dbo].[QAPROC_ReportCalculationQuestions_V11] @SurveyFormID, @SurveyIDs, @DemoQuery, @TimePeriodQuery
	EXEC [dbo].[QAPROC_ReportCalculationNonRatingScale_V11] @SurveyFormID, @SurveyIDs, @DemoQuery, @TimePeriodQuery
    Exec [dbo].[QAPROCResultWordCloud] @SurveyformID,@SurveyIDs,@DemoQuery,@TimePeriodQuery

END";
        #endregion

        #region Parent_SP_QAPROC_ReportCalculationCategory_V11
        public static string Parent_SP_QAPROC_ReportCalculationCategory_V11 = @"
CREATE OR ALTER PROCEDURE [dbo].[Parent_SP_QAPROC_ReportCalculationCategory_V11] (
	@SurveyFormID BIGINT,
	@SurveyIDs VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX),
    @CategoryQuery NVARCHAR(MAX) = NULL,
	@SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
IF OBJECT_ID(N'tempdb..#DrillDownCategoryScores',N'U') IS NULL
Create table #DrillDownCategoryScores
	(
		ObjectID bigint
		,CategoryText varchar(200)
		,Favorable numeric (10,0)
		,Neutral numeric (10,0)
		,Unfavorable numeric (10,0)
		,Mean numeric (10,2)
		,[Total Responses] BIGINT
        ,[CategoryTotalResponses]BIGINT
        ,[IsSuppressed] INT
	)

	CREATE TABLE #Temp_DimItem
	(
		ID INT IDENTITY(1,1)
		,CategoryText NVARCHAR(200)
	)

IF @CategoryQuery IS NULL
BEGIN
	INSERT INTO #Temp_DimItem
	SELECT DISTINCT	CategoryText FROM Dimitem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' AND CategoryText NOT IN ('Unassigned')
END
ELSE
BEGIN
	DECLARE @CNTR INT	
	SET @CNTR = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #Temp_DimItem (CategoryText )		
		SELECT	CategoryText = LTRIM(RTRIM(SUBSTRING(@CategoryQuery,1,CHARINDEX(',',@CategoryQuery)-1)))		
		SET @CategoryQuery = SUBSTRING(@CategoryQuery,CHARINDEX(',',@CategoryQuery)+1,LEN(@CategoryQuery))		
		SET @CNTR = @CNTR + 1	
	END		
	INSERT INTO #Temp_DimItem (CategoryText )	
	SELECT CategoryText = LTRIM(RTRIM(@CategoryQuery))	
END

DECLARE @categorytext VARCHAR(200) = ''
DECLARE @SurveyIDQuery NVARCHAR(MAX)

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END

WHILE (1 = 1)
BEGIN
	SELECT DISTINCT TOP 1 @categorytext = categorytext
	FROM #Temp_DimItem
	WHERE 
	--SurveyFormId = @SurveyFormID
	--	AND GroupName = 'ratingscale'
	--	AND CategoryText NOT IN ('Unassigned')
	--	AND 
		CategoryText > @categorytext
	ORDER BY categorytext ASC

	IF @@ROWCOUNT = 0
		BREAK;

	EXEC QAPROC_ReportCalculationCategory_V11 @SurveyFormID, @SurveyIDQuery, @DemoQuery, @TimePeriodQuery
		,@categorytext, @SurveyFormItemIdQuery
END

SELECT *
FROM #DrillDownCategoryScores order by ObjectID ASC

END";
        #endregion

        #region QAPROC_ReportCalculationQuestions_V11
        public static string QAPROC_ReportCalculationQuestions_V11 = @"
CREATE OR ALTER PROCEDURE [dbo].[QAPROC_ReportCalculationQuestions_V11] (
	-- Add the parameters for the stored procedure here
	@SurveyFormID BIGINT, 
	@SurveyIDs VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX),
    @SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Insert statements for procedure here

	DECLARE @OverFav INT
	DECLARE @OverMean NUMERIC(10,2)
	DECLARE @InsertQuery NVARCHAR(MAX)
	DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
	DECLARE @compRuleSettingType INT
	DECLARE @SurveyIDQuery NVARCHAR(MAX)
    DECLARE @GroupMinimumNSize INT

	SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')
	
    SELECT @GroupMinimumNSize = GroupMinimumSize FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID

    IF @SurveyFormItemIdQuery IS NULL
	BEGIN
		SET @SurveyFormItemIdQuery = '1 = 1'
	END

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END

	
IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
	ItemID BIGINT
	,ParticipantID BIGINT
	,Response INT
	)


SELECT @InsertQuery = 
'SELECT SurveyFormItemID
	,SurveyWaveParticipantID
	,ScaleOptionID
FROM FactResponse_V11 FR WITH (NOLOCK)
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND FR.' + @SurveyIDQuery + '
AND '+@TimePeriodQuery+' 
AND '+@SurveyFormItemIdQuery+'
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND ' + @SurveyIDQuery + '
AND '+@DemoQuery+' 
AND '+@CompletionRuleQuery+' 
AND [Status]=1)'

PRINT @InsertQuery

INSERT INTO #FactResponse
EXEC(@InsertQuery)

UPDATE fr
SET fr.response = di.scaleoptionvalue
FROM #FactResponse fr
INNER JOIN dimitem_V11 di ON fr.response = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID

IF OBJECT_ID('tempdb..#MeanCalculation7') IS NOT NULL
    DROP TABLE #MeanCalculation7
CREATE TABLE #MeanCalculation7 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,ResponseOption6 INT
	,ResponseOption7 INT
	,NA INT
	)

	
INSERT INTO #MeanCalculation7
SELECT itemid
	,ParticipantID
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[6]
	,[7]
	,[-1]
FROM (
	SELECT ItemID
	,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 7)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[6]
			,[7]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

IF OBJECT_ID('tempdb..#MeanCalculation5') IS NOT NULL
    DROP TABLE #MeanCalculation5
CREATE TABLE #MeanCalculation5 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation5
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) != 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;


IF OBJECT_ID('tempdb..#MeanCalculation05') IS NOT NULL
    DROP TABLE #MeanCalculation05
CREATE TABLE #MeanCalculation05 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption0 INT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation05
SELECT itemid
	,participantid
	,[0]
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) = 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[0]
			,[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;
  

;WITH FavResults
(
	[ObjectID]
	,[Favorable]
	,[Neutral]
	,[Unfavorable]
)
AS
(
	SELECT mc.ItemID [ObjectID],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),((SUM(mc.ResponseOption4) + SUM(mc.ResponseOption5))  * 100 )) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1, CONVERT(NUMERIC(10, 2),(SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Favorable],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),(SUM(mc.ResponseOption3)  * 100 ))/ IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2),(SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Neutral],
	CONVERT(NUMERIC(10, 0), CONVERT(NUMERIC(10, 2), ((SUM(mc.ResponseOption1) + SUM(mc.ResponseOption2)) * 100 ))/ IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Unfavorable]
	FROM #MeanCalculation5 mc
	GROUP BY mc.ItemID
	UNION
	SELECT mc.ItemID [ObjectID],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),((SUM(mc.ResponseOption4) + SUM(mc.ResponseOption5))  * 100 )) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1, CONVERT(NUMERIC(10, 2),(SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Favorable],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),((SUM(mc.ResponseOption2) + SUM(mc.ResponseOption3))  * 100 ))/ IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2),(SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Neutral],
	CONVERT(NUMERIC(10, 0), CONVERT(NUMERIC(10, 2), ((SUM(mc.ResponseOption0) + SUM(mc.ResponseOption1)) * 100 ))/ IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Unfavorable]
	FROM #MeanCalculation05 mc
	GROUP BY mc.ItemID
	UNION
	SELECT mc.ItemID [ObjectID],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),((SUM(mc.ResponseOption5) + SUM(mc.ResponseOption6) + SUM(mc.ResponseOption7))  * 100 )) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))) = 0,1, CONVERT(NUMERIC(10, 2),(SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))))) [Favorable],
	CONVERT(NUMERIC(10, 0),CONVERT(NUMERIC(10, 2),(SUM(mc.ResponseOption4)  * 100 ))/ IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))) = 0,1,CONVERT(NUMERIC(10, 2),(SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))))) [Neutral],
	CONVERT(NUMERIC(10, 0), CONVERT(NUMERIC(10, 2), ((SUM(mc.ResponseOption1) + SUM(mc.ResponseOption2) + SUM(mc.ResponseOption3)) * 100 ))/ IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))))) [Unfavorable]
	FROM #MeanCalculation7 mc
	GROUP BY mc.ItemID
),
AveResults
(
	[ObjectID]
	,[Mean]
)
AS
(
	SELECT  itemid [ObjectID]
		,CONVERT(NUMERIC(10, 2),CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5)) 
		/  IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) 
		[Mean]
	FROM #MeanCalculation5
	GROUP BY itemid
	UNION
	SELECT  itemid [ObjectID]
		,CONVERT(NUMERIC(10, 2),CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) * 0 + SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5)) 
		/  IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) 
		[Mean]
	FROM #MeanCalculation05
	GROUP BY itemid
	UNION
	SELECT  itemid [ObjectID]
		,CONVERT(NUMERIC(10, 2),CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5 + SUM(ResponseOption6) * 6 + SUM(ResponseOption7) * 7)) 
		/  IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7))))) 
		[Mean]
	FROM #MeanCalculation7
	GROUP BY itemid
),
Responses
(
	[ObjectID]
	,[Total Responses]
)
AS
(
	SELECT ItemID [ObjectID], SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA)  [Total Responses] FROM #MeanCalculation5
	GROUP BY ItemID
	UNION
	SELECT ItemID [ObjectID], SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA)  [Total Responses] FROM #MeanCalculation05
	GROUP BY ItemID
	UNION
	SELECT ItemID [ObjectID], SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7) + SUM(NA)  [Total Responses] FROM #MeanCalculation7
	GROUP BY ItemID
)
SELECT F.ObjectID, F.Favorable, F.Neutral, F.Unfavorable, A.Mean, R.[Total Responses], 
CASE 
WHEN R.[Total Responses] >= @GroupMinimumNSize THEN 0 
ELSE 1
END [IsSuppressed]
FROM FavResults F INNER JOIN AveResults A
ON F.ObjectID = A.ObjectID INNER JOIN Responses R ON F.ObjectID = R.ObjectID
ORDER BY F.ObjectID ASC



-- Response Count
SELECT [ObjectID], [Total Responses]
FROM
(
SELECT ItemID [ObjectID], SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA)  [Total Responses] FROM #MeanCalculation5
GROUP BY ItemID
UNION
SELECT ItemID [ObjectID], SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA)  [Total Responses] FROM #MeanCalculation05
GROUP BY ItemID
UNION
SELECT ItemID [ObjectID], SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7) + SUM(NA)  [Total Responses] FROM #MeanCalculation7
GROUP BY ItemID
) ResponseCntSet
ORDER BY ResponseCntSet.ObjectID ASC

-- Sentiment Count
SELECT [ObjectID], [Favorable Count], [Neutral Count], [Unfavorable Count], [NA Count]
FROM
(
SELECT mc.ItemID [ObjectID], SUM(mc.ResponseOption4) + SUM(mc.ResponseOption5) [Favorable Count],
SUM(mc.ResponseOption3) [Neutral Count],
SUM(mc.ResponseOption1) + SUM(mc.ResponseOption2) [Unfavorable Count],
SUM(mc.NA) [NA Count]
FROM #MeanCalculation5 mc
GROUP BY mc.ItemID
UNION
SELECT mc.ItemID [ObjectID], SUM(mc.ResponseOption4) + SUM(mc.ResponseOption5) [Favorable Count],
SUM(mc.ResponseOption3) + SUM(mc.ResponseOption3) [Neutral Count],
SUM(mc.ResponseOption0) + SUM(mc.ResponseOption1) [Unfavorable Count],
SUM(mc.NA) [NA Count]
FROM #MeanCalculation05 mc
GROUP BY mc.ItemID
UNION
SELECT mc.ItemID [ObjectID], SUM(mc.ResponseOption5) + SUM(mc.ResponseOption6)+ SUM(mc.ResponseOption7) [Favorable Count],
SUM(mc.ResponseOption4) [Neutral Count],
SUM(mc.ResponseOption1) + SUM(mc.ResponseOption2) + SUM(mc.ResponseOption3) [Unfavorable Count],
SUM(mc.NA) [NA Count]
FROM #MeanCalculation7 mc
GROUP BY mc.ItemID
) ResultSetCount
ORDER BY ResultSetCount.ObjectID ASC  

END";
        #endregion

        #region QAPROC_CommentReportCalc_V11
        public static string QAPROC_CommentReportCalc_V11 = @"
CREATE OR ALTER   PROCEDURE [dbo].[QAPROC_CommentReportCalc_V11] (
	@SurveyFormID INT
	,@SurveyIds NVARCHAR(MAX)
	,@DemoQuery NVARCHAR(MAX)
	,@TimePeriodQuery NVARCHAR(MAX)
	,@CategoryText VARCHAR(1000)
	,@SurveyFormItemId BIGINT
	)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @CategoryAvg NUMERIC(10, 2)
		DECLARE @InsertQuery NVARCHAR(MAX)
	DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
	DECLARE @compRuleSettingType INT
	

	SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

	IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
		DROP TABLE #FactResponse

	CREATE TABLE #FactResponse (
		ItemID BIGINT
		,ParticipantID BIGINT
		,Response INT
		)

	IF OBJECT_ID('tempdb..#MeanCalculation7') IS NOT NULL
		DROP TABLE #MeanCalculation7

	CREATE TABLE #MeanCalculation7 (
		ItemID BIGINT
		,ParticipantID BIGINT
		,ResponseOption1 INT
		,ResponseOption2 INT
		,ResponseOption3 INT
		,ResponseOption4 INT
		,ResponseOption5 INT
		,ResponseOption6 INT
		,ResponseOption7 INT
		,NA INT
		)

	IF OBJECT_ID('tempdb..#MeanCalculation5') IS NOT NULL
		DROP TABLE #MeanCalculation5

	CREATE TABLE #MeanCalculation5 (
		ItemID BIGINT
		,ParticipantID BIGINT
		,ResponseOption1 INT
		,ResponseOption2 INT
		,ResponseOption3 INT
		,ResponseOption4 INT
		,ResponseOption5 INT
		,NA INT
		)

		IF OBJECT_ID('tempdb..#MeanCalculation05') IS NOT NULL
		DROP TABLE #MeanCalculation05

	CREATE TABLE #MeanCalculation05 (
		ItemID BIGINT
		,ParticipantID BIGINT
	,ResponseOption0 INT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
		)

	IF OBJECT_ID('tempdb..#ParticipantRank') IS NOT NULL
		DROP TABLE #ParticipantRank

	CREATE TABLE #ParticipantRank (
		ParticipantID BIGINT
		,Mean NUMERIC(10, 2)
		,[Rank] INT
		,CommentGrouping INT
		)

		DECLARE @SurveyIDQuery NVARCHAR(MAX)

		IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END


SELECT @InsertQuery = 
'SELECT SurveyFormItemID
	,SurveyWaveParticipantID
	,ScaleOptionID
FROM FactResponse_V11 FR WITH (NOLOCK)
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND FR.' + @SurveyIDQuery + '
AND '+@TimePeriodQuery+' 
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND ' + @SurveyIDQuery + '
AND '+@DemoQuery+' 
AND '+@CompletionRuleQuery+' 
AND [Status]=1)'

--PRINT @InsertQuery
INSERT INTO #FactResponse
EXEC(@InsertQuery)

UPDATE fr
SET fr.response = di.scaleoptionvalue
FROM #FactResponse fr
INNER JOIN dimitem_V11 di ON fr.response = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID


	INSERT INTO #MeanCalculation7
	SELECT itemid
		,participantid
		,[1]
		,[2]
		,[3]
		,[4]
		,[5]
		,[6]
		,[7]
		,[-1]
	FROM (
		SELECT ItemID
			,ParticipantID
			,Response
		FROM #Factresponse
		WHERE ItemID IN (
				SELECT SurveyFormItemID
				FROM DimItem_V11
				WHERE SurveyFormID = @SurveyFormID
					AND GroupName = 'RatingScale'
					AND CategoryText = @CategoryText
				GROUP BY SurveyFormItemID
				HAVING MAX(ScaleOptionValue) = 7
				)
		) fr
	PIVOT(Count(response) FOR response IN (
				[1]
				,[2]
				,[3]
				,[4]
				,[5]
				,[6]
				,[7]
				,[-1]
				)) AS pivottable
	ORDER BY itemid
		,participantid ASC;

	INSERT INTO #MeanCalculation5
	SELECT itemid
		,participantid
		,[1]
		,[2]
		,[3]
		,[4]
		,[5]
		,[-1]
	FROM (
		SELECT ItemID
			,ParticipantID
			,Response
		FROM #Factresponse
		WHERE ItemID IN (
				SELECT SurveyFormItemID
				FROM DimItem_V11
				WHERE SurveyFormID = @SurveyFormID
					AND GroupName = 'RatingScale'
					AND CategoryText = @CategoryText
				GROUP BY SurveyFormItemID
				HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) != 0
				)
		) fr
	PIVOT(Count(response) FOR response IN (
				[1]
				,[2]
				,[3]
				,[4]
				,[5]
				,[-1]
				)) AS pivottable
	ORDER BY itemid
		,participantid ASC;

INSERT INTO #MeanCalculation05
	SELECT itemid
		,participantid
		,[0]
		,[1]
		,[2]
		,[3]
		,[4]
		,[5]
		,[-1]
	FROM (
		SELECT ItemID
			,ParticipantID
			,Response
		FROM #Factresponse
		WHERE ItemID IN (
				SELECT SurveyFormItemID
				FROM DimItem_V11
				WHERE SurveyFormID = @SurveyFormID
					AND GroupName = 'RatingScale'
					AND CategoryText = @CategoryText
				GROUP BY SurveyFormItemID
				HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) = 0
				)
		) fr
	PIVOT(Count(response) FOR response IN (
				[0]
				,[1]
				,[2]
				,[3]
				,[4]
				,[5]
				,[-1]
				)) AS pivottable
	ORDER BY itemid
		,participantid ASC;

	
;with ResultTable AS
(
SELECT ResponseOption0 [Scale0], ResponseOption1 [Scale1], ResponseOption2 [Scale2], ResponseOption3 [Scale3], ResponseOption4 [Scale4], ResponseOption5 [Scale5], 0 [Scale6], 0 [Scale7] FROM #MeanCalculation05
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,0,0 FROM #MeanCalculation5
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,ResponseOption6,ResponseOption7 FROM #MeanCalculation7
)
SELECT @CategoryAvg = CONVERT(NUMERIC(10,2),(SUM(Scale0) * 0.00 + SUM(Scale1) * 1.00 + SUM(Scale2) * 2.00 + SUM(Scale3) * 3.00 + SUM(Scale4) * 4.00 + SUM(Scale5) * 5.00 + SUM(Scale6) * 6.00 + SUM(Scale7) * 7.00) /
IIF ((SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00  = 0, 1, (SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00 )  )
FROM ResultTable


	INSERT INTO #ParticipantRank
	SELECT [ParticipantID]
		,[Mean]
		,RANK() OVER (
			ORDER BY ResultSet.Mean DESC
			)
		,0
	FROM (
		SELECT ParticipantID [ParticipantID]
			,CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5)) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Mean]
		FROM #MeanCalculation5
		GROUP BY ParticipantID 
		
		UNION
		
		SELECT ParticipantID [ParticipantID]
			,CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5 + SUM(ResponseOption6) * 6 + SUM(ResponseOption7) * 7)) / CONVERT(NUMERIC(10, 2), (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)))) [Mean]
		FROM #MeanCalculation7
		GROUP BY ParticipantID
		
		UNION
		
		SELECT ParticipantID [ParticipantID]
			,CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) * 0 + SUM(ResponseOption1) * 1 + SUM(ResponseOption2) * 2 + SUM(ResponseOption3) * 3 + SUM(ResponseOption4) * 4 + SUM(ResponseOption5) * 5)) / IIF(CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))) = 0,1,CONVERT(NUMERIC(10, 2), (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5))))) [Mean]
		FROM #MeanCalculation05
		GROUP BY ParticipantID  
		) ResultSet
	ORDER BY ResultSet.Mean DESC
		,ResultSet.ParticipantID ASC

	--select * from #ParticipantRank
	UPDATE #ParticipantRank
	SET CommentGrouping = 1
	WHERE Mean >= @CategoryAvg

	SELECT TOP 20 di.ReportItemText
		,frc.Comments
		,[RANK]
		,CharCount
	FROM #ParticipantRank pr
	INNER JOIN FactResponseComment_V11 frc WITH (NOLOCK) ON pr.ParticipantID = frc.SurveyWaveParticipantID
	INNER JOIN DimItem_V11 di ON frc.SurveyID = di.SurveyId
		AND frc.SurveyFormItemID = di.SurveyFormItemID
	WHERE frc.SurveyFormID = @SurveyFormID
		AND di.IsComment = 1
		AND di.CategoryText = @CategoryText
		AND pr.CommentGrouping = 1
		AND frc.SurveyFormItemID = @SurveyFormItemId
	ORDER BY frc.SurveyFormItemID ASC
		,pr.[Rank] ASC
		,CharCount DESC

	SELECT TOP 20 di.ReportItemText
		,frc.Comments
		,[RANK]
		,CharCount
	FROM #ParticipantRank pr
	INNER JOIN FactResponseComment_V11 frc WITH (NOLOCK) ON pr.ParticipantID = frc.SurveyWaveParticipantID
	INNER JOIN DimItem_V11 di WITH (NOLOCK) ON frc.SurveyID = di.SurveyId
		AND frc.SurveyFormItemID = di.SurveyFormItemID
	WHERE frc.SurveyFormID = @SurveyFormID
		AND di.IsComment = 1
		AND di.CategoryText = @CategoryText
		AND pr.CommentGrouping = 0
		AND frc.SurveyFormItemID = @SurveyFormItemId
	ORDER BY frc.SurveyFormItemID ASC
		,pr.[Rank] ASC
		,CharCount DESC
END";
        #endregion

        #region QAPROC_CommentReportCalc_UnScored_V11
        public static string QAPROC_CommentReportCalc_UnScored_V11 = @"
CREATE OR ALTER   PROCEDURE [dbo].[QAPROC_CommentReportCalc_UnScored_V11] (
	@SurveyFormID INT
	,@SurveyIds NVARCHAR(MAX)
	,@DemoQuery NVARCHAR(MAX)
	,@TimePeriodQuery NVARCHAR(MAX)
	,@CategoryText VARCHAR(1000)
	,@SurveyFormItemId BIGINT
	)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @CategoryAvg NUMERIC(10, 2)
	DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
	DECLARE @compRuleSettingType INT
	DECLARE @InsertQuery NVARCHAR(MAX)

DROP TABLE IF EXISTS #Participant1
CREATE TABLE #Participant1
(
SurveyWaveParticipantID bigint
)
	
SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

		DECLARE @SurveyIDQuery NVARCHAR(MAX)

		IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END


SELECT @InsertQuery = 
'SELECT SurveyWaveParticipantID
FROM FactResponseComment_V11 FR WITH (NOLOCK)
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND FR.' + @SurveyIDQuery + '
AND '+@TimePeriodQuery+' 
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND ' + @SurveyIDQuery + '
AND '+@DemoQuery+' 
AND '+@CompletionRuleQuery+' 
AND [Status]=1)'
INSERT INTO #Participant1
EXEC(@InsertQuery)


	SELECT  TOP 20 di.ReportItemText, frc.Comments , CharCount
	FROM DimParticipant_V11 dp
	INNER JOIN FactResponseComment_V11 frc WITH (NOLOCK) ON 
	dp.SurveyWaveParticipantID = frc.SurveyWaveParticipantID AND dp.SurveyID = frc.SurveyID
	INNER JOIN DimItem_V11 di ON frc.SurveyID = di.SurveyId
		AND frc.SurveyFormItemID = di.SurveyFormItemID
	WHERE frc.surveyFormid = @SurveyFormID
		AND di.IsComment = 1
		AND di.CategoryText = @CategoryText
		AND frc.SurveyFormItemID = @SurveyFormItemId
		AND dp.SurveyWaveParticipantID in (SELECT distinct  SurveyWaveParticipantID FROM #Participant1)
	ORDER BY frc.SurveyFormItemID ASC
		,CharCount DESC


END";
        #endregion

        #region Parent_SP_QAPROC_DemographicReportCalcCategory_V11
        public static string Parent_SP_QAPROC_DemographicReportCalcCategory_V11 = @"
CREATE OR ALTER PROCEDURE [dbo].[Parent_SP_QAPROC_DemographicReportCalcCategory_V11] (
	@SurveyFormID BIGINT,
	@SurveyIDs VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX),
	@CategoryQuery NVARCHAR(MAX) = NULL,
	@SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @categorytext VARCHAR(200) = ''
	DECLARE @SurveyIDQuery VARCHAR(1000)

IF OBJECT_ID(N'tempdb..#DemoCategoryScores',N'U') IS NULL
Create table #DemoCategoryScores
	(
		CategoryID bigint
		,CategoryText varchar(200)
		,[Demo Value] nvarchar(100)
		,Favorable numeric (10,0)
		,Mean numeric (10,2)
	)


	IF OBJECT_ID(N'tempdb..#DemoValueResponses',N'U') IS NULL
	Create table #DemoValueResponses
	(
		[Demo Value] nvarchar(100)
		,Responses bigint
	)

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	CREATE TABLE #Temp_DimItem
	(
		ID INT IDENTITY(1,1)
		,CategoryText NVARCHAR(200)
	)

IF @CategoryQuery IS NULL
BEGIN
	INSERT INTO #Temp_DimItem
	SELECT DISTINCT	CategoryText FROM Dimitem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' AND CategoryText NOT IN ('Unassigned')
END
ELSE
BEGIN
	DECLARE @CNTR INT	
	SET @CNTR = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #Temp_DimItem (CategoryText )		
		SELECT	CategoryText = LTRIM(RTRIM(SUBSTRING(@CategoryQuery,1,CHARINDEX(',',@CategoryQuery)-1)))		
		SET @CategoryQuery = SUBSTRING(@CategoryQuery,CHARINDEX(',',@CategoryQuery)+1,LEN(@CategoryQuery))		
		SET @CNTR = @CNTR + 1	
	END		
	INSERT INTO #Temp_DimItem (CategoryText )	
	SELECT CategoryText = LTRIM(RTRIM(@CategoryQuery))	
END

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END
		



WHILE (1 = 1)
BEGIN
	SELECT DISTINCT TOP 1 @categorytext = CategoryText
	FROM #Temp_DimItem
	WHERE 
	--SurveyFormID = @SurveyFormID
	--	AND GroupName = 'ratingscale'
	--	AND CategoryText NOT IN ('Unassigned')
		--AND 
		CategoryText > @categorytext
	ORDER BY categorytext ASC

	IF @@ROWCOUNT = 0
		BREAK;

	DECLARE @demo VARCHAR(200) = ''

	WHILE (1 = 1)
	BEGIN
		SELECT DISTINCT TOP 1 @demo = Demo18
		FROM DimParticipant_V11
		WHERE SurveyFormID = @SurveyFormID
			AND Demo18 > @demo
		ORDER BY Demo18

		IF @@ROWCOUNT = 0
			BREAK;

		EXEC QAPROC_DemographicReportCalcCategory_V11 @SurveyFormID, @SurveyIDQuery, @DemoQuery, @TimePeriodQuery
			,'Demo18'
			,@demo
			,@categorytext
			,@SurveyFormItemIdQuery
	END
END


SET @demo = ''

WHILE (1 = 1)
BEGIN
	SELECT DISTINCT TOP 1 @demo = Demo18
	FROM DimParticipant_V11
	WHERE SurveyFormID = @SurveyFormID
		AND Demo18 > @demo
	ORDER BY Demo18

	IF @@ROWCOUNT = 0
		BREAK;
	
	EXEC QAPROC_DemographicReportResponseCalc_V11 @SurveyFormID, @SurveyIDQuery, @DemoQuery, @TimePeriodQuery
		,'Demo18'
		,@demo
		,@SurveyFormItemIdQuery
END

SELECT *
FROM #DemoCategoryScores WITH (NOLOCK) order by CategoryID ASC, [Demo Value] ASC

SELECT * FROM #DemoValueResponses WITH (NOLOCK) ORDER BY [Demo Value] ASC


END
";
        #endregion

        #region Parent_SP_QAPROC_DemographicReportCalcQuestions_V11
        public static string Parent_SP_QAPROC_DemographicReportCalcQuestions_V11 = @"
CREATE OR ALTER PROCEDURE [dbo].[Parent_SP_QAPROC_DemographicReportCalcQuestions_V11] (
	@SurveyFormID BIGINT,
	@SurveyIDs VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX),
	@SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @itemid BIGINT = 0
	DECLARE @SurveyIDQuery VARCHAR(1000)
	
IF OBJECT_ID(N'tempdb..#DemoQuestionScores',N'U') IS NULL
Create table #DemoQuestionScores
	(
		QuestionID bigint
		,[Demo Value] nvarchar(100)
		,Favorable numeric (10,0)
		,Mean numeric (10,2)
	)

IF OBJECT_ID(N'tempdb..#DemoValueResponses',N'U') IS NULL
Create table #DemoValueResponses
	(
		[Demo Value] nvarchar(100)
		,Responses bigint
	)

	IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
		DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END
		
	DECLARE @demo VARCHAR(200) = ''

	WHILE (1 = 1)
	BEGIN
		SELECT DISTINCT TOP 1 @demo = Demo18
		FROM DimParticipant_V11
		WHERE SurveyFormID = @SurveyFormID
			AND Demo18 > @demo
		ORDER BY Demo18

		IF @@ROWCOUNT = 0
			BREAK;

		EXEC QAPROC_DemographicReportCalcQuestions_V11 @SurveyFormID, @SurveyIDQuery, @DemoQuery, @TimePeriodQuery
			,'Demo18'
			,@demo
			,@SurveyFormItemIdQuery
			-- ,@itemid

	END

SET @demo = ''

WHILE (1 = 1)
BEGIN
	SELECT DISTINCT TOP 1 @demo = Demo18
	FROM DimParticipant_V11
	WHERE SurveyFormID = @SurveyFormID
		AND Demo18 > @demo
	ORDER BY Demo18

	IF @@ROWCOUNT = 0
		BREAK;
	
	EXEC QAPROC_DemographicReportResponseCalc_V11 @SurveyFormID, @SurveyIDQuery, @DemoQuery, @TimePeriodQuery
		,'Demo18'
		,@demo
        ,@SurveyFormItemIdQuery
END

SELECT *
FROM #DemoQuestionScores WITH (NOLOCK) ORDER BY QuestionID ASC, [Demo Value] ASC

SELECT * FROM #DemoValueResponses WITH (NOLOCK) ORDER BY [Demo Value] ASC

END";
        #endregion

        #region QAPROC_DemographicReportResponseCalc_V11
        public static string QAPROC_DemographicReportResponseCalc_V11 = @"
CREATE OR ALTER       PROCEDURE [dbo].[QAPROC_DemographicReportResponseCalc_V11] (
	@SurveyFormID INT
	,@SurveyIDQuery VARCHAR(1000)
	,@DemoQuery NVARCHAR(MAX)
	,@TimePeriodQuery NVARCHAR(MAX)
	,@DemoColumn VARCHAR(1000)
	,@DemoValue VARCHAR(1000)
    ,@SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
	)
AS
BEGIN
	SET NOCOUNT ON;


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--DECLARE @DemoID VARCHAR(100)
DECLARE @SQL NVARCHAR(MAX)
DECLARE @CategoryID BIGINT
DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
DECLARE @compRuleSettingType INT
DECLARE @TotCnt5 BIGINT
DECLARE @TotCnt05 BIGINT
DECLARE @TotCnt7 BIGINT
DECLARE @TotalCnt BIGINT

	SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

	IF @SurveyFormItemIdQuery IS NULL
	BEGIN
		SET @SurveyFormItemIdQuery = '1 = 1'
	END

IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
	ItemID BIGINT
	,ParticipantID BIGINT
	,Response INT
	)

--INSERT INTO #FactResponse

SET @SQL = 'SELECT SurveyFormItemID
	,SurveyWaveParticipantID
	,ScaleOptionID
FROM FactResponse_V11 FR WITH (NOLOCK)
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND FR.' + @SurveyIDQuery + '
AND '+@TimePeriodQuery+' 
AND '+@SurveyFormItemIdQuery+'
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND ' + @SurveyIDQuery + '
AND '+@DemoColumn+' = ' + ''''+@DemoValue+'''' + '
AND '+@DemoQuery+' 
AND '+@CompletionRuleQuery+' 
AND [Status]=1)'



PRINT @SQL

INSERT INTO #FactResponse
EXEC(@SQL)

--SELECT * FROM #FactResponse

UPDATE fr
SET fr.response = di.scaleoptionvalue
FROM #FactResponse fr
INNER JOIN dimitem_V11 di ON fr.response = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID

IF OBJECT_ID('tempdb..#MeanCalculation7') IS NOT NULL
    DROP TABLE #MeanCalculation7
CREATE TABLE #MeanCalculation7 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,ResponseOption6 INT
	,ResponseOption7 INT
	,NA INT
	)

	
INSERT INTO #MeanCalculation7
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[6]
	,[7]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 7)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[6]
			,[7]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

IF OBJECT_ID('tempdb..#MeanCalculation5') IS NOT NULL
    DROP TABLE #MeanCalculation5
CREATE TABLE #MeanCalculation5 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation5
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) != 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;


IF OBJECT_ID('tempdb..#MeanCalculation05') IS NOT NULL
    DROP TABLE #MeanCalculation05
CREATE TABLE #MeanCalculation05 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption0 INT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation05
SELECT itemid
	,participantid
	,[0]
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) = 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[0]
			,[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

SELECT @TotCnt5 = COUNT(DISTINCT ParticipantID)
FROM #MeanCalculation5 

SELECT @TotCnt05 = COUNT(DISTINCT ParticipantID)
FROM #MeanCalculation05

SELECT @TotCnt7 = COUNT(DISTINCT ParticipantID)
FROM #MeanCalculation7

SELECT @TotalCnt = ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0)

Insert into #DemoValueResponses
SELECT  @DemoValue [Demo Value], @TotalCnt [Responses]


END";
        #endregion

        #region QAPROC_GetTestScenarios_V11
        public static string QAPROC_GetTestScenarios_V11 = @"
CREATE OR ALTER PROCEDURE [dbo].[QAPROC_GetTestScenarios_V11] 
(
	@ItemorCategory INT
	,@DataCalculation INT
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	;WITH cteslno
	AS
	(
	SELECT ScenarioID,UserAnalyzeFilterID,
	slno = ROW_NUMBER() OVER (PARTITION BY ScenarioID ORDER BY ScenarioID)
	FROM tblScenarioUserAnalyzeFilterXref 
	)
	SELECT CT.UserAnalyzeFilterID,TS.ScenarioDescription FROM cteslno CT
    INNER JOIN tblTestScenario TS
	ON TS.ScenarioID= CT.ScenarioID
	WHERE CT.slno = 1 ORDER BY CT.ScenarioID ASC

	SELECT DISTINCT uaf.UserID, uaf.ClientID, uaf.SurveyFormID FROM UserAnalyzeFilter uaf INNER JOIN tblScenarioUserAnalyzeFilterXref xref  
	ON uaf.UserAnalyzeFilterID = xref.UserAnalyzeFilterID  
	INNER JOIN  UserAnalyzeDisplay UAD
	ON Uaf.userID = UAD.userID
	WHERE UAD.ResultDisplay = @ItemorCategory
	AND UAD.DataCalculation = @DataCalculation
	ORDER BY uaf.UserID ASC
	
END
";
        #endregion

        #region QAPROC_TrendReport_NR_V11
        public static string QAPROC_TrendReport_NR_V11 = @"
CREATE OR ALTER PROCEDURE [dbo].[QAPROC_TrendReport_NR_V11] (
       -- Add the parameters for the stored procedure here
       @SurveyFormID INT,  
       @SurveyIDQuery VARCHAR(1000),
       @SurveyFormItemID BIGINT,
       @StartTime NVARCHAR(MAX),
       @EndTime NVARCHAR(MAX),
       @DemoQuery NVARCHAR(MAX),
       @TImePeriodQuery NVARCHAR(MAX),
	   @IsTimePeriodDateDemoEnabled INT,
       @SurveyID NVARCHAR(MAX)
       )
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Insert statements for procedure here

       DECLARE @Group NVARCHAR(200) = ''
       DECLARE @ShadeStatus INT
       DECLARE @ResponseCount INT
       DECLARE @OverFav INT
       DECLARE @OverMean NUMERIC(10,2)
       DECLARE @Shading NVARCHAR(100) = ''
       DECLARE @InsertQuery NVARCHAR(MAX)
       DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
       DECLARE @compRuleSettingType INT
       DECLARE @GroupMinimumNSize INT

       IF DATEDIFF (DAY, @StartTime, @EndTime) < 31
       SET @Group = SUBSTRING(DATENAME(MONTH, @StartTime),1,3) + ' ' + CONVERT(VARCHAR(100),DATEPART(YEAR, @StartTime))
ELSE IF DATEDIFF (DAY, @StartTime, @EndTime) BETWEEN 88 AND 92
       BEGIN
              IF DATEPART(MONTH,@StartTime) = 1
                     SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime)) +' 12:00AM'
              ELSE IF DATEPART(MONTH,@StartTime) = 4
                     SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime)) +' 12:00AM'
              ELSE IF DATEPART(MONTH,@StartTime) = 7
                     SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime)) +' 12:00AM'
              ELSE IF DATEPART(MONTH,@StartTime) = 10
                     SET @Group = CONVERT(VARCHAR(3),DATENAME(MONTH, @StartTime)) + '  1 ' + CONVERT(VARCHAR(100),DATEPART(YEAR,@StartTime)) +' 12:00AM'
       END
ELSE IF DATEDIFF (DAY, @StartTime, @EndTime) BETWEEN 364 AND 367
       SET @Group = DATEPART(YEAR,@StartTime)
       
       SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
       SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

        SELECT @GroupMinimumNSize = GroupMinimumSize FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID

IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
       ItemID BIGINT
       ,ParticipantID BIGINT
       ,Response NVARCHAR(MAX)
       ,ResponseID BIGINT
       )
IF OBJECT_ID('tempdb..#NonMultiselectResponse') IS NOT NULL
    DROP TABLE #NonMultiselectResponse
CREATE TABLE #NonMultiselectResponse (
     GroupBy NVARCHAR(MAX)
       ,SurveyformItemID BIGINT
       ,ScaleOptionId BIGINT
       ,[Response Rate]BIGINT
       ,ScaleResponses BIGINT
       ,ResponseCount BIGINT
       ,TotalResponses BIGINT
       ,[IsSuppressed] INT
       )

IF OBJECT_ID('tempdb..#MultiselectResponse') IS NOT NULL
    DROP TABLE #MultiselectResponse
CREATE TABLE #MultiselectResponse (
     GroupBy NVARCHAR(MAX)
       ,SurveyformItemID BIGINT
       ,ScaleOptionId BIGINT
       ,[Response Rate] BIGINT
       ,ScaleResponses BIGINT
       ,ResponseCount BIGINT
       ,TotalResponses BIGINT
       ,[IsSuppressed] INT
       )


IF OBJECT_ID('tempdb..#ParticipantList') IS NOT NULL
    DROP TABLE #ParticipantList
CREATE TABLE #ParticipantList (
       ParticipantID BIGINT
       )

IF(@IsTimePeriodDateDemoEnabled != 1)
BEGIN
	IF(@SurveyID ='')
	BEGIN
		SET @InsertQuery = 'SELECT SurveyFormItemID  
		 ,SurveyWaveParticipantID  
		 ,''''
		,ScaleOptionID  
		FROM FactResponse_V11 FR WITH (NOLOCK)  
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
		AND FR.SurveyFormItemID = '+CONVERT(VARCHAR(10),@SurveyFormItemID)+' 
		AND FR.' + @SurveyIDQuery + '  
		AND '+@TimePeriodQuery+'   
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
		AND ' + @SurveyIDQuery + '  
		AND '+@DemoQuery+'   
		AND '+@CompletionRuleQuery+'   
		AND [Status]=1)
		AND FR.UpdateTimestampGMT >= CONVERT(DATETIME,'''+@StartTime+''') AND FR.UpdateTimestampGMT <= CONVERT(DATETIME,'''+@EndTime+''')'
	END
	ELSE 
	BEGIN
		SET @InsertQuery = 'SELECT SurveyFormItemID  
		 ,SurveyWaveParticipantID  
		 ,''''
		,ScaleOptionID  
		FROM FactResponse_V11 FR WITH (NOLOCK)  
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
		AND FR.SurveyFormItemID = '+CONVERT(VARCHAR(10),@SurveyFormItemID)+' 
		AND FR.' + @SurveyIDQuery + '  
		AND '+@TimePeriodQuery+'   
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyID = '+CONVERT(VARCHAR(10),@SurveyID)+'   
		AND ' + @SurveyIDQuery + '  
		AND '+@DemoQuery+'   
		AND '+@CompletionRuleQuery+'   
		AND [Status]=1)
		AND FR.UpdateTimestampGMT >= CONVERT(DATETIME,'''+@StartTime+''') AND FR.UpdateTimestampGMT <= CONVERT(DATETIME,'''+@EndTime+''')'
	END
END
ELSE
BEGIN
	DECLARE @DemoLabelSQL NVARCHAR(1000)
	DECLARE @DemoTimePeriodQuery NVARCHAR(MAX) = ''
	DECLARE @MaxSQL NVARCHAR(MAX) = ''
	SELECT @DemoLabelSQL = SUBSTRING(@DemoQuery, 1, CHARINDEX('BETWEEN', @DemoQuery)-2)
	SET @DemoTimePeriodQuery = @DemoLabelSQL + ' BETWEEN CONVERT(DATETIME,'''+@StartTime+''') AND CONVERT(DATETIME,'''+@EndTime+''')'
	PRINT @DemoTimePeriodQuery
	IF(@SurveyID ='')
	BEGIN
		SET @InsertQuery = 'SELECT SurveyFormItemID  
		 ,SurveyWaveParticipantID  
		 ,''''
		,ScaleOptionID  
		FROM FactResponse_V11 FR WITH (NOLOCK)  
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
		AND FR.SurveyFormItemID = '+CONVERT(VARCHAR(10),@SurveyFormItemID)+' 
		AND FR.' + @SurveyIDQuery + '  
		AND '+@TimePeriodQuery+'   
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
		AND ' + @SurveyIDQuery + '  
		AND '+@DemoQuery+'   
		AND '+@CompletionRuleQuery+'   
		AND '+@DemoTimePeriodQuery+'
		AND [Status]=1)
		'
	END
	ELSE 
	BEGIN
		SET @InsertQuery = 'SELECT SurveyFormItemID  
		 ,SurveyWaveParticipantID  
		 ,''''
		,ScaleOptionID  
		FROM FactResponse_V11 FR WITH (NOLOCK)  
		WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'   
		AND FR.SurveyFormItemID = '+CONVERT(VARCHAR(10),@SurveyFormItemID)+' 
		AND FR.' + @SurveyIDQuery + '  
		AND '+@TimePeriodQuery+'   
		AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyID = '+CONVERT(VARCHAR(10),@SurveyID)+'   
		AND ' + @SurveyIDQuery + '  
		AND '+@DemoQuery+'   
		AND '+@CompletionRuleQuery+'   
		AND '+@DemoTimePeriodQuery+'
		AND [Status]=1)
		'
	END
END

PRINT @InsertQuery

INSERT INTO #FactResponse
EXEC(@InsertQuery)

UPDATE fr
SET fr.response = di.ScaleOptionText
FROM #FactResponse fr
INNER JOIN DimItem_V11 di ON fr.ResponseID = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID



SELECT @ResponseCount = COUNT(DISTINCT ParticipantID) FROM #FactResponse

;with cte as
(
SELECT fr.ItemID, COUNT(*) [TotalResponse] FROM #FactResponse fr 
GROUP BY fr.ItemID
)
,
cte2 as
(
SELECT fr.ItemID,fr.ResponseID,fr.Response,COUNT(*) [ScaleResponses] FROM #FactResponse fr
INNER JOIN cte ON cte.ItemID = fr.ItemID
Where fr.ResponseID in (Select ScaleOptionId from Dimitem_V11 where  itemstyleName not in ('MultiSelect'))
GROUP BY fr.ItemID,fr.ResponseID,fr.Response
)
--INSERT INTO #TrendResult_NR
Insert into #NonMultiselectResponse
select IIF(@Group = '',CONVERT(NVARCHAR(100),@SurveyID),@Group),  cte.ItemID [SurveyFormItemID], cte2.ResponseID [ScaleOptionID], Convert(Int,Convert(Numeric(5,0),(cte2.ScaleResponses * 1.00 * 100/cte.TotalResponse * 1.00))) [Response Rate], cte2.ScaleResponses [ScaleResponses], @ResponseCount [Responses],cte.totalResponse,
CASE 
WHEN @ResponseCount >= @GroupMinimumNSize THEN 0 
ELSE 1
END [IsSuppressed]
from cte inner join cte2 on cte.ItemID = cte2.ItemID
ORDER BY cte.ItemID ASC

;with cte3 as
(
SELECT fr.ItemID, COUNT(*) [TotalResponse] FROM #FactResponse fr 
GROUP BY fr.ItemID
),
cte5 as
(
SELECT fr.ItemID, COUNT(distinct participantID)[ResponseCount] FROM #FactResponse fr 
GROUP BY fr.ItemID
)

,Cte4 as
(
SELECT fr.ItemID,fr.ResponseID,fr.Response,COUNT(*) [ScaleResponses] FROM #FactResponse fr
INNER JOIN cte3 ON cte3.ItemID = fr.ItemID
Where fr.ResponseID in (Select ScaleOptionId from Dimitem_V11 where  itemstyleName in ('MultiSelect'))
GROUP BY fr.ItemID,fr.ResponseID,fr.Response
)
Insert into  #MultiselectResponse
select IIF(@Group = '',CONVERT(NVARCHAR(100),@SurveyID),@Group),  cte3.ItemID [SurveyFormItemID], cte4.ResponseID [ScaleOptionID], Convert(Int,Convert(Numeric(5,0),(Cte4.ScaleResponses * 1.00 * 100/ResponseCount *1.00))) [Response Rate], cte4.ScaleResponses [ScaleResponses],ResponseCount,cte3.totalResponse,
CASE 
WHEN ResponseCount >= @GroupMinimumNSize THEN 0 
ELSE 1
END [IsSuppressed]
from cte3 inner join cte4 on cte3.ItemID = cte4.ItemID
inner join Cte5 on Cte5.itemID= Cte3.itemID
ORDER BY cte3.ItemID ASC
INsert into  #TrendResult_NR
Select * from  #NonMultiselectResponse
UNION ALL
Select * from  #MultiselectResponse

END";
        #endregion

        #region TrendSPExecution_NR_Year
        public static string TrendSPExecution_NR_Year = @"
CREATE OR ALTER PROCEDURE [dbo].[TrendSPExecution_NR_Year] (
       @SurveyFormID INT,
       @SurveyFormItemID BIGINT,
       @DemoQuery NVARCHAR(MAX),
       @TImePeriodQuery NVARCHAR(MAX),
	   @IsTimePeriodDateDemoEnabled INT,
	   @SurveyIDs VARCHAR(1000)
)
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

       DECLARE @StartDate NVARCHAR(MAX)  
       DECLARE @EndDate NVARCHAR(MAX)  

       
IF OBJECT_ID('tempdb..#YearDateList') IS NOT NULL
       DROP TABLE #YearDateList

IF OBJECT_ID('tempdb..#TrendResult_NR') IS NULL
       CREATE TABLE #TrendResult_NR (
              [GroupBy] NVARCHAR(200)
              ,[SurveyFormItemID] BIGINT
              ,[ScaleOptionID] BIGINT
              ,[Response Rate] NUMERIC (10,2)
              ,[ScaleResponses] INT
              ,Responses INT
              ,TotalResponseCount INT 
              ,[IsSuppressed] INT
              )

CREATE TABLE #YearDateList (
       ID INT IDENTITY(1,1)
       ,StartDate DATETIME
       ,EndDate DATETIME
       )

       DECLARE @SurveyIDQuery NVARCHAR(MAX)

       IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
              DROP TABLE #SurveyList

       CREATE TABLE #SurveyList (
       ID INT IDENTITY(1,1)
       ,SurveyID VARCHAR(1000)
       )

       DECLARE @CNT INT     
       SET @CNT = 1  
       WHILE (CHARINDEX(',',@SurveyIDs)>0)
       BEGIN         
              INSERT INTO #SurveyList (SurveyID )             
              SELECT SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))              
              SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))        
              SET @CNT = @CNT + 1  
       END           
       INSERT INTO #SurveyList (SurveyID )      
       SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))      

              IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
              BEGIN
                     SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
                     SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
              END
              ELSE
              BEGIN
                     SET @SurveyIDQuery = 'SurveyID > 0'
              END

	IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END


SELECT @StartDate = DATEADD(MONTH,-DATEPART(MONTH,@StartDate)+1,@StartDate) 


;WITH cte AS (
SELECT CASE WHEN DATEPART(Day,@StartDate) = 1 THEN @StartDate 
            ELSE DATEADD(DAY,-DATEPART(Day,@StartDate)+1,@StartDate) END AS myDate
UNION ALL
SELECT DATEADD(Month,12,myDate)
FROM cte
WHERE DATEADD(Month,12,myDate) <=  @EndDate
)
INSERT INTO #YearDateList
SELECT myDate, EOMONTH(DATEADD(MONTH,11,myDate))
FROM cte
OPTION (MAXRECURSION 0)

UPDATE #YearDateList
SET EndDate = DATEADD(SECOND,86399,EndDate)

--SELECT * FROM #YearDateList

DECLARE @StartTime NVARCHAR(MAX) 
DECLARE @EndTime NVARCHAR(MAX) 
DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #YearDateList 
       WHERE ID > @id ORDER BY ID 
       IF @@ROWCOUNT = 0 BREAK;  
       SELECT @StartTime = StartDate, @EndTime = EndDate FROM #YearDateList WHERE ID = @id
       EXEC QAPROC_TrendReport_NR_V11 @SurveyformID, @SurveyIDQuery, @SurveyFormItemID, @StartTime, @EndTime, @DemoQuery, @TImePeriodQuery, @IsTimePeriodDateDemoEnabled, ''
END

SELECT [GroupBy], [SurveyFormItemID], [ScaleOptionID], [Response Rate], [ScaleResponses], Responses, TotalResponseCount, IsSuppressed FROM #TrendResult_NR
ORDER BY [GroupBy] ASC, [ScaleOptionID] ASC         
END ";
        #endregion

        #region TrendSPExecution_NR_Quarter
        public static string TrendSPExecution_NR_Quarter = @"
CREATE OR ALTER PROCEDURE [dbo].[TrendSPExecution_NR_Quarter] (
       @SurveyFormID INT,
       @SurveyFormItemID BIGINT,
       @DemoQuery NVARCHAR(MAX),
       @TImePeriodQuery NVARCHAR(MAX),
	   @IsTimePeriodDateDemoEnabled INT,
       @SurveyIDs VARCHAR(1000)
)
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

       DECLARE @StartDate NVARCHAR(MAX)  
       DECLARE @EndDate NVARCHAR(MAX)  

       
IF OBJECT_ID('tempdb..#QuarterDateList') IS NOT NULL
       DROP TABLE #QuarterDateList

IF OBJECT_ID('tempdb..#TrendResult_NR') IS NULL
       CREATE TABLE #TrendResult_NR (
              [GroupBy] NVARCHAR(200)
              ,[SurveyFormItemID] BIGINT
              ,[ScaleOptionID] BIGINT
              ,[Response Rate] NUMERIC (10,2)
              ,[ScaleResponses] INT
              ,Responses INT
              ,TotalResponseCount INT 
              ,[IsSuppressed] INT
              )

CREATE TABLE #QuarterDateList (
       ID INT IDENTITY(1,1)
       ,StartDate DATETIME
       ,EndDate DATETIME
       )

DECLARE @SurveyIDQuery NVARCHAR(MAX)

       IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
              DROP TABLE #SurveyList

       CREATE TABLE #SurveyList (
       ID INT IDENTITY(1,1)
       ,SurveyID VARCHAR(1000)
       )

       DECLARE @CNT INT     
       SET @CNT = 1  
       WHILE (CHARINDEX(',',@SurveyIDs)>0)
       BEGIN         
              INSERT INTO #SurveyList (SurveyID )             
              SELECT SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))              
              SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))        
              SET @CNT = @CNT + 1  
       END           
       INSERT INTO #SurveyList (SurveyID )      
       SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))      

              IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
              BEGIN
                     SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
                     SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
              END
              ELSE
              BEGIN
                     SET @SurveyIDQuery = 'SurveyID > 0'
              END

	IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END
	
IF DATEPART(MONTH,@StartDate) NOT IN (1,4,7,10)
BEGIN
       IF DATEPART(MONTH,@StartDate) IN (2,5,8,11)
              SELECT @StartDate = DATEADD(MONTH,-1,@StartDate)
       ELSE IF DATEPART(MONTH,@StartDate) IN (3,6,9,12)
              SELECT @StartDate = DATEADD(MONTH,-2,@StartDate)
END


;WITH cte AS (
SELECT CASE WHEN DATEPART(Day,@StartDate) = 1 THEN @StartDate 
            ELSE DATEADD(DAY,-DATEPART(Day,@StartDate)+1,@StartDate) END AS myDate
UNION ALL
SELECT DATEADD(Month,3,myDate)
FROM cte
WHERE DATEADD(Month,3,myDate) <=  @EndDate
)
INSERT INTO #QuarterDateList
SELECT myDate, EOMONTH(DATEADD(MONTH,2,myDate))
FROM cte
OPTION (MAXRECURSION 0)

UPDATE #QuarterDateList
SET EndDate = DATEADD(SECOND,86399,EndDate)

--SELECT * FROM #QuarterDateList

DECLARE @StartTime NVARCHAR(MAX) 
DECLARE @EndTime NVARCHAR(MAX) 
DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #QuarterDateList 
       WHERE ID > @id ORDER BY ID 
       IF @@ROWCOUNT = 0 BREAK;  
       SELECT @StartTime = StartDate, @EndTime = EndDate FROM #QuarterDateList WHERE ID = @id
       EXEC QAPROC_TrendReport_NR_V11 @SurveyformID,@SurveyIDQuery, @SurveyFormItemID, @StartTime, @EndTime, @DemoQuery, @TImePeriodQuery, @IsTimePeriodDateDemoEnabled, ''
END

SELECT [GroupBy], [SurveyFormItemID], [ScaleOptionID], [Response Rate], [ScaleResponses], Responses, TotalResponseCount, IsSuppressed FROM #TrendResult_NR
ORDER BY [GroupBy] ASC, [ScaleOptionID] ASC         
END    ";
        #endregion

        #region TrendSPExecution_NR_Month
        public static string TrendSPExecution_NR_Month = @"
CREATE OR ALTER PROCEDURE [dbo].[TrendSPExecution_NR_Month] (
       @SurveyFormID INT,
       @SurveyFormItemID BIGINT,
       @DemoQuery NVARCHAR(MAX),
       @TImePeriodQuery NVARCHAR(MAX),
	   @IsTimePeriodDateDemoEnabled INT,
       @SurveyIDs NVARCHAR(MAX)
)
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

       DECLARE @StartDate NVARCHAR(MAX)  
       DECLARE @EndDate NVARCHAR(MAX)  

       
IF OBJECT_ID('tempdb..#DateList') IS NOT NULL
       DROP TABLE #DateList

IF OBJECT_ID('tempdb..#TrendResult_NR') IS NULL
       CREATE TABLE #TrendResult_NR (
              [GroupBy] NVARCHAR(200)
              ,[SurveyFormItemID] BIGINT
              ,[ScaleOptionID] BIGINT
              ,[Response Rate] NUMERIC (10,2)
              ,[ScaleResponses] INT
              ,Responses INT
              ,TotalResponseCount INT
              ,[IsSuppressed] INT
              )

CREATE TABLE #DateList (
       ID INT IDENTITY(1,1)
       ,StartDate DATETIME
       ,EndDate DATETIME
       )

       DECLARE @SurveyIDQuery NVARCHAR(MAX)

       IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
              DROP TABLE #SurveyList

       CREATE TABLE #SurveyList (
       ID INT IDENTITY(1,1)
       ,SurveyID VARCHAR(1000)
       )

       DECLARE @CNT INT     
       SET @CNT = 1  
       WHILE (CHARINDEX(',',@SurveyIDs)>0)
       BEGIN         
              INSERT INTO #SurveyList (SurveyID )             
              SELECT SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))              
              SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))        
              SET @CNT = @CNT + 1  
       END           
       INSERT INTO #SurveyList (SurveyID )      
       SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))      

              IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
              BEGIN
                     SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
                     SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
              END
              ELSE
              BEGIN
                     SET @SurveyIDQuery = 'SurveyID > 0'
              END


	IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END
	
;WITH cte AS (
SELECT CASE WHEN DATEPART(Day,@StartDate) = 1 THEN @StartDate 
            ELSE DATEADD(DAY,-DATEPART(Day,@StartDate)+1,@StartDate) END AS myDate
UNION ALL
SELECT DATEADD(Month,1,myDate)
FROM cte
WHERE DATEADD(Month,1,myDate) <=  @EndDate
)
INSERT INTO #DateList
SELECT myDate, EOMONTH(myDate)
FROM cte
OPTION (MAXRECURSION 0)

UPDATE #DateList
SET EndDate = DATEADD(SECOND,86399,EndDate)

--SELECT * FROM #DateList

DECLARE @StartTime NVARCHAR(MAX) 
DECLARE @EndTime NVARCHAR(MAX) 
DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #DateList 
       WHERE ID > @id ORDER BY ID 
       IF @@ROWCOUNT = 0 BREAK;  
       SELECT @StartTime = StartDate, @EndTime = EndDate FROM #DateList WHERE ID = @id
       EXEC QAPROC_TrendReport_NR_V11 @SurveyFormID, @SurveyIDQuery, @SurveyFormItemID, @StartTime, @EndTime, @DemoQuery, @TImePeriodQuery, @IsTimePeriodDateDemoEnabled, ''
END

SELECT [GroupBy], [SurveyFormItemID], [ScaleOptionID], [Response Rate], [ScaleResponses], Responses, TotalResponseCount, IsSuppressed FROM #TrendResult_NR
ORDER BY [GroupBy] ASC, [ScaleOptionID] ASC         
END      ";
        #endregion

        #region TrendSPExecution_NR_Distribution
        public static string TrendSPExecution_NR_Distribution = @"
CREATE OR ALTER PROCEDURE [dbo].[TrendSPExecution_NR_Distribution] (
       @SurveyFormID INT,
       @SurveyFormItemID BIGINT,
       @DemoQuery NVARCHAR(MAX),
       @TImePeriodQuery NVARCHAR(MAX),
	   @IsTimePeriodDateDemoEnabled INT,
       @SurveyIDs NVARCHAR(MAX)

)
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

       DECLARE @StartDate NVARCHAR(MAX)  
       DECLARE @EndDate NVARCHAR(MAX)  


       
IF OBJECT_ID('tempdb..#SurveyLists') IS NOT NULL
       DROP TABLE #SurveyLists

IF OBJECT_ID('tempdb..#TrendResult_NR') IS NULL
       CREATE TABLE #TrendResult_NR (
              [GroupBy] NVARCHAR(200)
              ,[SurveyFormItemID] BIGINT
              ,[ScaleOptionID] BIGINT
              ,[Response Rate] NUMERIC (10,2)
              ,[ScaleResponses] INT
              ,Responses INT
              ,TotalResponseCount INT
              ,[IsSuppressed] INT
              )

CREATE TABLE #SurveyLists (
       ID INT IDENTITY(1,1)
       ,SurveyID INT
       )


       DECLARE @SurveyIDQuery NVARCHAR(MAX)

       IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
              DROP TABLE #SurveyList

       CREATE TABLE #SurveyList (
       ID INT IDENTITY(1,1)
       ,SurveyID VARCHAR(1000)
       )

       DECLARE @CNT INT     
       SET @CNT = 1  
       WHILE (CHARINDEX(',',@SurveyIDs)>0)
       BEGIN         
              INSERT INTO #SurveyList (SurveyID )             
              SELECT SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))              
              SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))        
              SET @CNT = @CNT + 1  
       END           
       INSERT INTO #SurveyList (SurveyID )      
       SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))      

              IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
              BEGIN
                     SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
                     SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
              END
              ELSE
              BEGIN
                     SET @SurveyIDQuery = 'SurveyID > 0'
              END


	IF (@IsTimePeriodDateDemoEnabled = 1)
	BEGIN
		DECLARE @DemoLabelSQL NVARCHAR(1000)
		DECLARE @MinSQL NVARCHAR(MAX) = ''
		DECLARE @MaxSQL NVARCHAR(MAX) = ''
		SELECT @DemoLabelSQL = LTRIM(RTRIM(SUBSTRING(@DemoQuery, CHARINDEX('BETWEEN', @DemoQuery)-8, CHARINDEX('BETWEEN', @DemoQuery)-(CHARINDEX('BETWEEN', @DemoQuery)-8))))
		SET @MinSQL = 'SELECT @mindate = MIN(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		SET @MaxSQL = 'SELECT @maxdate = MAX(CONVERT(DATETIME,' + @DemoLabelSQL + ')) FROM Dimparticipant_V11 WITH (NOLOCK) WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		EXEC sp_executesql @MinSQL, N'@mindate DATETIME OUT', @StartDate OUT
		EXEC sp_executesql @MaxSQL, N'@maxdate DATETIME OUT', @EndDate OUT
	END
	ELSE
	BEGIN
		SELECT @StartDate = MIN(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
		SELECT @EndDate = MAX(UpdateTimestampGMT) FROM FactResponse_V11 WITH (NOLOCK) WHERE SurveyFormID = @SurveyFormID
	END
	

INSERT INTO #SurveyLists
SELECT SurveyID FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID AND SurveyWaveStatus = 1


Declare @SurveyId Bigint;

DECLARE @id INT = 0
WHILE (1=1) 
BEGIN
    SELECT DISTINCT top 1 @id = ID FROM #SurveyLists 
       WHERE ID > @id ORDER BY ID 
       IF @@ROWCOUNT = 0 BREAK;   
       SELECT @SurveyId = SurveyID FROM #SurveyLists WHERE ID = @id

       EXEC QAPROC_TrendReport_NR_V11 @SurveyFormID,@SurveyIDQuery, @SurveyFormItemID, @StartDate, @EndDate, @DemoQuery, @TImePeriodQuery, @IsTimePeriodDateDemoEnabled, @SurveyID
END

SELECT [GroupBy], [SurveyFormItemID], [ScaleOptionID], [Response Rate], [ScaleResponses], Responses, TotalResponseCount, IsSuppressed FROM #TrendResult_NR
ORDER BY [GroupBy] ASC, [ScaleOptionID] ASC         
END       ";
        #endregion

        #region QAPROC_ReportCalculationCategory_V11
        public static string QAPROC_ReportCalculationCategory_V11 = @"
CREATE OR ALTER PROCEDURE [dbo].[QAPROC_ReportCalculationCategory_V11] (
	-- Add the parameters for the stored procedure here
	@SurveyFormID BIGINT, 
	@SurveyIDQuery VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX),
	@CategoryText VARCHAR(1000),
    @SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Insert statements for procedure here

	DECLARE @CategoryAvg NUMERIC(10,2)
	DECLARE @CategoryFav NUMERIC(10,0)
	DECLARE @CategoryNeu NUMERIC(10,0)
	DECLARE @CategoryUnfav NUMERIC(10,0)
	DECLARE @CategoryID BIGINT
	DECLARE @InsertQuery NVARCHAR(MAX)
	DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
	DECLARE @compRuleSettingType INT
	DECLARE @FavCnt5 BIGINT
	DECLARE @FavCnt05 BIGINT
	DECLARE @FavCnt7 BIGINT
	DECLARE @NeuCnt5 BIGINT
	DECLARE @NeuCnt05 BIGINT
	DECLARE @NeuCnt7 BIGINT
	DECLARE @UnFavCnt5 BIGINT
	DECLARE @UnFavCnt05 BIGINT
	DECLARE @UnFavCnt7 BIGINT
	DECLARE @TotCnt5 BIGINT
	DECLARE @TotCnt05 BIGINT
	DECLARE @TotCnt7 BIGINT
    DECLARE @GroupMinimumNSize INT
    DECLARE @NoOfItemsInCategory INT

	
	IF @SurveyFormItemIdQuery IS NULL
	BEGIN
		SELECT @NoOfItemsInCategory = COUNT(DISTINCT SurveyFormItemID) FROM Dimitem_V11 WHERE SurveyFormID = @SurveyFormID AND CategoryText = @CategoryText AND GroupName = 'RatingScale'
	END
	ELSE
	BEGIN
		DECLARE @Query NVARCHAR(MAX) = ''
		SET @Query = 'SELECT @count = COUNT(DISTINCT SurveyFormItemID) FROM Dimitem_V11 WHERE SurveyFormID = ' + CONVERT(NVARCHAR(100),@SurveyFormID)
		 + 'AND CategoryText =' + @CategoryText + 'GroupName = ''RatingScale'' AND ' + @SurveyFormItemIdQuery 
		EXEC sp_executesql @Query, N'@count INT OUT', @NoOfItemsInCategory OUT	
	END
	

	SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

    SELECT @GroupMinimumNSize = GroupMinimumSize FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
    --SELECT @NoOfItemsInCategory = COUNT(DISTINCT SurveyFormItemID) FROM Dimitem_V11 WHERE SurveyFormID = @SurveyFormID AND CategoryText = @CategoryText AND GroupName = 'RatingScale'

	IF @SurveyFormItemIdQuery IS NULL
	BEGIN
		SET @SurveyFormItemIdQuery = '1 = 1'
	END
	
IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
	ItemID BIGINT
	,ParticipantID BIGINT
	,Response INT
	)

--INSERT INTO #FactResponse

SELECT @InsertQuery = 
'SELECT SurveyFormItemID
	,SurveyWaveParticipantID
	,ScaleOptionID
FROM FactResponse_V11 FR WITH (NOLOCK)
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND FR.' + @SurveyIDQuery + '
AND '+@TimePeriodQuery+' 
AND '+@SurveyFormItemIdQuery+'
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND ' + @SurveyIDQuery + '
AND '+@DemoQuery+' 
AND '+@CompletionRuleQuery+' 
AND [Status]=1)'

PRINT @InsertQuery

INSERT INTO #FactResponse
EXEC(@InsertQuery)

UPDATE fr
SET fr.response = di.scaleoptionvalue
FROM #FactResponse fr
INNER JOIN dimitem_V11 di ON fr.response = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID

IF OBJECT_ID('tempdb..#MeanCalculation7') IS NOT NULL
    DROP TABLE #MeanCalculation7
CREATE TABLE #MeanCalculation7 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,ResponseOption6 INT
	,ResponseOption7 INT
	,NA INT
	)

	
INSERT INTO #MeanCalculation7
SELECT itemid
	,ParticipantID
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[6]
	,[7]
	,[-1]
FROM (
	SELECT ItemID
	,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' AND CategoryText = @CategoryText GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 7)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[6]
			,[7]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

IF OBJECT_ID('tempdb..#MeanCalculation5') IS NOT NULL
    DROP TABLE #MeanCalculation5
CREATE TABLE #MeanCalculation5 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation5
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' AND CategoryText = @CategoryText GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) != 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

IF OBJECT_ID('tempdb..#MeanCalculation05') IS NOT NULL
    DROP TABLE #MeanCalculation05
CREATE TABLE #MeanCalculation05 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption0 INT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation05
SELECT itemid
	,participantid
	,[0]
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' AND CategoryText = @CategoryText GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) = 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[0]
			,[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;


SELECT @CategoryID = SurveyCategoryID FROM DimItem_V11 WHERE SurveyFormId = @SurveyFormID AND CategoryText = @CategoryText

-- Category Favorable Scores
SELECT @FavCnt5 = (SUM(ResponseOption4) + SUM(ResponseOption5))  FROM #MeanCalculation5
SELECT @FavCnt05 = (SUM(ResponseOption4) + SUM(ResponseOption5)) FROM #MeanCalculation05
SELECT @FavCnt7 = (SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)) FROM #MeanCalculation7

SELECT @NeuCnt5 = SUM(ResponseOption3)  FROM #MeanCalculation5
SELECT @NeuCnt05 = (SUM(ResponseOption2) + SUM(ResponseOption3)) FROM #MeanCalculation05
SELECT @NeuCnt7 = SUM(ResponseOption4) FROM #MeanCalculation7

SELECT @UnFavCnt5 = (SUM(ResponseOption1) + SUM(ResponseOption2))  FROM #MeanCalculation5
SELECT @UnFavCnt05 = (SUM(ResponseOption0) + SUM(ResponseOption1)) FROM #MeanCalculation05
SELECT @UnFavCnt7 = (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3)) FROM #MeanCalculation7

SELECT @TotCnt5 = IIF ((SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)) = 0, 1, (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)))  
FROM #MeanCalculation5
SELECT @TotCnt05 = IIF ((SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)) = 0, 1, (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)))  
FROM #MeanCalculation05
SELECT @TotCnt7 = IIF ((SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)) = 0, 1, (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)))  
FROM #MeanCalculation7

SELECT @CategoryFav  =   CONVERT(NUMERIC(10,0),(ISNULL(@FavCnt5,0) + ISNULL(@FavCnt05,0) + ISNULL(@FavCnt7,0)) * 100.00 / IIF((ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0))  = 0, 1,(ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0)) * 1.00))
SELECT @CategoryNeu  =   CONVERT(NUMERIC(10,0),(ISNULL(@NeuCnt5,0) + ISNULL(@NeuCnt05,0) + ISNULL(@NeuCnt7,0)) * 100.00 / IIF((ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0))  = 0, 1,(ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0)) * 1.00))
SELECT @CategoryUnfav  =   CONVERT(NUMERIC(10,0),(ISNULL(@UnFavCnt5,0) + ISNULL(@UnFavCnt05,0) + ISNULL(@UnFavCnt7,0)) * 100.00 / IIF((ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0))  = 0, 1,(ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0)) * 1.00))


-- Category Average
;with ResultTable AS
(
SELECT ResponseOption0 [Scale0], ResponseOption1 [Scale1], ResponseOption2 [Scale2], ResponseOption3 [Scale3], ResponseOption4 [Scale4], ResponseOption5 [Scale5], 0 [Scale6], 0 [Scale7] FROM #MeanCalculation05
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,0,0 FROM #MeanCalculation5
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,ResponseOption6,ResponseOption7 FROM #MeanCalculation7
)
SELECT @CategoryAvg = CONVERT(NUMERIC(10,2),(SUM(Scale0) * 0.00 + SUM(Scale1) * 1.00 + SUM(Scale2) * 2.00 + SUM(Scale3) * 3.00 + SUM(Scale4) * 4.00 + SUM(Scale5) * 5.00 + SUM(Scale6) * 6.00 + SUM(Scale7) * 7.00) /
IIF ((SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00  = 0, 1, (SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00 )  )
FROM ResultTable


--Category Responses Count
DECLARE @TotCntWithNA5 BIGINT
DECLARE @TotCntWithNA05 BIGINT
DECLARE @TotCntWithNA7 BIGINT
DECLARE @TotCntWithNA BIGINT

SELECT @TotCntWithNA5 =  SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA) FROM #MeanCalculation5
SELECT @TotCntWithNA05 =  SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(NA) FROM #MeanCalculation05
SELECT @TotCntWithNA7 = SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7) + SUM(NA) FROM #MeanCalculation7
SELECT @TotCntWithNA = ISNULL(@TotCntWithNA5,0) + ISNULL(@TotCntWithNA05,0) + ISNULL(@TotCntWithNA7,0)

Insert into #DrillDownCategoryScores
SELECT @CategoryID, @CategoryText, @CategoryFav [Favorable], @CategoryNeu, @CategoryUnfav, @CategoryAvg [Mean],CONVERT(NUMERIC(10,0),(@TotCntWithNA * 1.00/@NoOfItemsInCategory))[TotalResponses],@TotCntWithNA[CategoryTotalResponse],
CASE 
WHEN CONVERT(NUMERIC(10,0),(@TotCntWithNA * 1.00/@NoOfItemsInCategory)) >= @GroupMinimumNSize THEN 0 
ELSE 1
END [IsSuppressed]

END";
        #endregion

        #region QAPROC_DemographicReportCalcCategory_V11
        public static string QAPROC_DemographicReportCalcCategory_V11 = @"
CREATE OR ALTER       PROCEDURE [dbo].[QAPROC_DemographicReportCalcCategory_V11] (
	@SurveyFormID INT
	,@SurveyIDQuery VARCHAR(1000)
	,@DemoQuery NVARCHAR(MAX)
	,@TimePeriodQuery NVARCHAR(MAX)
	,@DemoColumn VARCHAR(1000)
	,@DemoValue VARCHAR(1000)
	,@CategoryText VARCHAR(200)
	,@SurveyFormItemIdQuery NVARCHAR(MAX) = NULL
	)
AS
BEGIN
	SET NOCOUNT ON;


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--DECLARE @DemoID VARCHAR(100)
DECLARE @SQL NVARCHAR(MAX)
DECLARE @CategoryAvg NUMERIC(10,2)
DECLARE @CategoryFav NUMERIC(10,0)
DECLARE @CategoryID BIGINT
DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
DECLARE @compRuleSettingType INT
DECLARE @FavCnt5 BIGINT
DECLARE @FavCnt05 BIGINT
DECLARE @FavCnt7 BIGINT
DECLARE @TotCnt5 BIGINT
DECLARE @TotCnt05 BIGINT
DECLARE @TotCnt7 BIGINT

	SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID
	SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')

	IF @SurveyFormItemIdQuery IS NULL
	BEGIN
		SET @SurveyFormItemIdQuery = '1 = 1'
	END

IF OBJECT_ID('tempdb..#FactResponse') IS NOT NULL
    DROP TABLE #FactResponse
CREATE TABLE #FactResponse (
	ItemID BIGINT
	,ParticipantID BIGINT
	,Response INT
	)

--INSERT INTO #FactResponse

SET @SQL = 'SELECT SurveyFormItemID
	,SurveyWaveParticipantID
	,ScaleOptionID
FROM FactResponse_V11 FR WITH (NOLOCK)
WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND FR.' + @SurveyIDQuery + '
AND '+@TimePeriodQuery+' 
AND '+@SurveyFormItemIdQuery+'
AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID  FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
AND ' + @SurveyIDQuery + '
AND '+@DemoColumn+' = ' + ''''+@DemoValue+'''' + '
AND '+@DemoQuery+' 
AND '+@CompletionRuleQuery+' 
AND [Status]=1)'



PRINT @SQL

INSERT INTO #FactResponse
EXEC(@SQL)


UPDATE fr
SET fr.response = di.scaleoptionvalue
FROM #FactResponse fr
INNER JOIN dimitem_V11 di ON fr.response = di.ScaleOptionID
WHERE di.SurveyFormID = @SurveyFormID

IF OBJECT_ID('tempdb..#MeanCalculation7') IS NOT NULL
    DROP TABLE #MeanCalculation7
CREATE TABLE #MeanCalculation7 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,ResponseOption6 INT
	,ResponseOption7 INT
	,NA INT
	)

	
INSERT INTO #MeanCalculation7
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[6]
	,[7]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	AND CategoryText = @CategoryText GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 7)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[6]
			,[7]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

IF OBJECT_ID('tempdb..#MeanCalculation5') IS NOT NULL
    DROP TABLE #MeanCalculation5
CREATE TABLE #MeanCalculation5 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation5
SELECT itemid
	,participantid
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	AND CategoryText = @CategoryText GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) != 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;


IF OBJECT_ID('tempdb..#MeanCalculation05') IS NOT NULL
    DROP TABLE #MeanCalculation05
CREATE TABLE #MeanCalculation05 (
	ItemID BIGINT
	,ParticipantID BIGINT
	,ResponseOption0 INT
	,ResponseOption1 INT
	,ResponseOption2 INT
	,ResponseOption3 INT
	,ResponseOption4 INT
	,ResponseOption5 INT
	,NA INT
	)

INSERT INTO #MeanCalculation05
SELECT itemid
	,participantid
	,[0]
	,[1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[-1]
FROM (
	SELECT ItemID
		,ParticipantID
		,Response
	FROM #Factresponse where ItemID in (SELECT SurveyFormItemID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND GroupName = 'RatingScale' 
	AND CategoryText = @CategoryText GROUP BY SurveyFormItemID HAVING MAX(ScaleOptionValue) = 5 AND MIN(ScaleOptionValue) = 0)
	) fr
PIVOT(Count(response) FOR response IN (
			[0]
			,[1]
			,[2]
			,[3]
			,[4]
			,[5]
			,[-1]
			)) AS pivottable
ORDER BY itemid
	,participantid ASC;

-- Category Favorable Scores
SELECT @FavCnt5 = (SUM(ResponseOption4) + SUM(ResponseOption5))  FROM #MeanCalculation5
SELECT @FavCnt05 = (SUM(ResponseOption4) + SUM(ResponseOption5)) FROM #MeanCalculation05
SELECT @FavCnt7 = (SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)) FROM #MeanCalculation7

SELECT @TotCnt5 = IIF ((SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)) = 0, 1, (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)))  
FROM #MeanCalculation5
SELECT @TotCnt05 = IIF ((SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)) = 0, 1, (SUM(ResponseOption0) + SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5)))  
FROM #MeanCalculation05
SELECT @TotCnt7 = IIF ((SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)) = 0, 1, (SUM(ResponseOption1) + SUM(ResponseOption2) + SUM(ResponseOption3) + SUM(ResponseOption4) + SUM(ResponseOption5) + SUM(ResponseOption6) + SUM(ResponseOption7)))  
FROM #MeanCalculation7

SELECT @CategoryFav  =   CONVERT(NUMERIC(10,0),(ISNULL(@FavCnt5,0) + ISNULL(@FavCnt05,0) + ISNULL(@FavCnt7,0)) * 100.00 / IIF((ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0))  = 0, 1,(ISNULL(@TotCnt5,0) + ISNULL(@TotCnt05,0) + ISNULL(@TotCnt7,0)) * 1.00))

-- Category Average
;with ResultTable AS
(
SELECT ResponseOption0 [Scale0], ResponseOption1 [Scale1], ResponseOption2 [Scale2], ResponseOption3 [Scale3], ResponseOption4 [Scale4], ResponseOption5 [Scale5], 0 [Scale6], 0 [Scale7] FROM #MeanCalculation05
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,0,0 FROM #MeanCalculation5
UNION ALL
SELECT 0, ResponseOption1, ResponseOption2, ResponseOption3, ResponseOption4, ResponseOption5,ResponseOption6,ResponseOption7 FROM #MeanCalculation7
)
SELECT @CategoryAvg = CONVERT(NUMERIC(10,2),(SUM(Scale0) * 0.00 + SUM(Scale1) * 1.00 + SUM(Scale2) * 2.00 + SUM(Scale3) * 3.00 + SUM(Scale4) * 4.00 + SUM(Scale5) * 5.00 + SUM(Scale6) * 6.00 + SUM(Scale7) * 7.00) /
IIF ((SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00  = 0, 1, (SUM(Scale0) + SUM(Scale1) + SUM(Scale2) + SUM(Scale3) + SUM(Scale4) + SUM(Scale5) + SUM(Scale6) + SUM(Scale7)) * 1.00 )  )
FROM ResultTable

SELECT @CategoryID = SurveyCategoryID FROM DimItem_V11 WHERE SurveyFormID = @SurveyFormID AND CategoryText = @CategoryText

Insert into #DemoCategoryScores
SELECT @CategoryID, @CategoryText, @DemoValue [Demo Value] , @CategoryFav [Favorable], @CategoryAvg [Mean]


END";
        #endregion

        #region QAPROC_GetFilterResults
        public static string QAPROC_GetFilterResults = @"
CREATE OR ALTER   PROCEDURE [dbo].[QAPROC_GetFilterResults] (
	@AnalyzeFilterID NVARCHAR(MAX)
)
AS
BEGIN

	SET NOCOUNT ON;
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
       
       DECLARE @UserAnalyzeFilterID INT
       DECLARE @SurveyFormID BIGINT
       DECLARE @TimePeriod INT
       DECLARE @SurveyIDs NVARCHAR(MAX)
       DECLARE @DemoFilter NVARCHAR(MAX)
       DECLARE @TimePeriodFilter NVARCHAR(MAX)
       DECLARE @StartDate NVARCHAR(MAX)
       DECLARE @EndDate NVARCHAR(MAX)
       DECLARE @TimePeriodDemoLabel NVARCHAR(MAX)
       DECLARE @DemoLabel NVARCHAR(MAX)
       DECLARE @DemoRangeLabel NVARCHAR(MAX)
       DECLARE @DemoStartDate NVARCHAR(MAX)
       DECLARE @DemoEndDate NVARCHAR(MAX)
       DECLARE @DateDemoLabel NVARCHAR(MAX)
       DECLARE @DemographicColumnName NVARCHAR(MAX)
       DECLARE @DemoTimePeriodFilter NVARCHAR(MAX)
       DECLARE @Label NVARCHAR(MAX) = ''
       DECLARE @Values NVARCHAR(MAX) = NULL
	   DECLARE @IsTimePeriodDateDemoEnabled INT = 0

       DROP TABLE IF EXISTS #DemoQueries
       CREATE TABLE #DemoQueries
       (
              ID INT IDENTITY(1,1)
              ,DemoQuery NVARCHAR(MAX)
       )

       SET @UserAnalyzeFilterID = CONVERT(INT,@AnalyzeFilterID)
       SELECT @SurveyFormID = SurveyFormID FROM UserAnalyzeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
       SELECT @TimePeriod = TimePeriod FROM UserAnalyzeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
       SELECT @TimePeriodDemoLabel = DemoLabel FROM UserAnalyzeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
       SELECT @DemoLabel = DemoLabel FROM DemoFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
       SELECT @DemoRangeLabel = DemoLabel FROM DemoRangeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
       

       SELECT @SurveyIDs = COALESCE(@SurveyIDs + ', ','') + CONVERT(NVARCHAR(1000),SurveyID) FROM SurveyFilter 
       WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID

       IF(@TimePeriod = 1)
       BEGIN
              SET @TimePeriodFilter = '1 = 1'
       END
       ELSE
       BEGIN
              SELECT @StartDate = CONVERT(NVARCHAR(MAX),StartDate) FROM UserAnalyzeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
              SELECT @EndDate = CONVERT(NVARCHAR(MAX),EndDate) FROM UserAnalyzeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
              IF(@TimePeriodDemoLabel IS NULL)
              BEGIN
                     SET @TimePeriodFilter = 'FR.InsertTimestampGMT BETWEEN CONVERT(DATETIME,''''' + @StartDate + ''''') AND CONVERT(DATETIME,''''' + @EndDate + ''''')'
              END
              ELSE
              BEGIN
              SET @TimePeriodFilter = '1 = 1'
					 SELECT @IsTimePeriodDateDemoEnabled = 1
                     SELECT @DemographicColumnName = DemographicColumnName FROM DimSurveyDemographicColumnLabel WHERE SurveyFormID = @SurveyFormID
                     AND [Label] = @TimePeriodDemoLabel
                     SET @DemoTimePeriodFilter = @DemographicColumnName + ' BETWEEN CONVERT(DATETIME,''''' + @StartDate + ''''') AND CONVERT(DATETIME,''''' + @EndDate + ''''')'
                     INSERT INTO #DemoQueries
                     SELECT @DemoTimePeriodFilter
              END
       END
       
       IF(@DemoLabel IS NULL AND @DemoRangeLabel IS NULL AND @TimePeriodDemoLabel IS NULL)
       BEGIN
              SET @DemoLabel = '1 = 1'
              INSERT INTO #DemoQueries
              SELECT @DemoLabel
       END
       IF (@DemoLabel IS NOT NULL)
       BEGIN
              WHILE (1 = 1)
              BEGIN
                     SELECT DISTINCT TOP 1 @Label = DemoLabel FROM DemoFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
                     AND DemoLabel > @Label
                     ORDER BY DemoLabel ASC
                     IF @@ROWCOUNT = 0
                           BREAK
                     SELECT @DemographicColumnName = DemographicColumnName FROM DimSurveyDemographicColumnLabel WHERE SurveyFormID = @SurveyFormID
                     AND Label = @Label
                     SELECT @Values = COALESCE(@Values + ', ', '') + '''''' + DemoValue + '''''' FROM DemoFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
                     AND DemoLabel = @Label
                     SET @Values = CONCAT(@DemographicColumnName + ' IN (',@Values,')')
                     INSERT INTO #DemoQueries
                     SELECT @Values
                     SET @Values = NULL
              END
       END
       IF (@DemoRangeLabel IS NOT NULL)
       BEGIN
              SELECT @DemoStartDate = StartDate FROM DemoRangeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
              SELECT @DemoEndDate = EndDate FROM DemoRangeFilter WHERE UserAnalyzeFilterID = @UserAnalyzeFilterID
              SELECT @DemographicColumnName = DemographicColumnName FROM DimSurveyDemographicColumnLabel WHERE SurveyFormID = @SurveyFormID
              AND [Label] = @DemoRangeLabel
              SET @DemoTimePeriodFilter = @DemographicColumnName + ' BETWEEN CONVERT(DATETIME,''''' + @DemoStartDate + ''''') AND CONVERT(DATETIME,''''' + @DemoEndDate + ''''')'
              INSERT INTO #DemoQueries
              SELECT @DemoTimePeriodFilter
       END
       SELECT @Values = COALESCE(@Values + ' AND ', '') + DemoQuery FROM #DemoQueries 

       SELECT DISTINCT USF.SurveyFormID, @SurveyIDs [SurveyIDs], @Values [DemoFilter], @TimePeriodFilter [TimePeriodFilter], 
       USD.DataCalculation, USD.ResultDisplay, USF.BenchMarkID, @IsTimePeriodDateDemoEnabled [IsTimePeriodDateDemoEnabled] FROM UserAnalyzeDisplay USD
       INNER JOIN UserAnalyzeFilter USF
       ON USF.UserID= USD.UserID
       WHERE UserAnalyzeFilterID= @UserAnalyzeFilterID
END";
        #endregion

        #region BenchMarkCalculationTrend_V11
        public static string BenchMarkCalculationTrend_V11 = @"
CREATE OR ALTER     PROCEDURE [dbo].[BenchMarkCalculationTrend_V11](
 @BenchmarkId BIGINT,
 @SurveyIds NVARCHAR(MAX),
 @SurveyFormItemID NVARCHAR(MAX),
 @Category NVARCHAR(MAX)
 )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @SurveyIDQuery NVARCHAR(MAX)
	DECLARE @Item NVARCHAR(MAX)

	   IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
   DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)
	 IF OBJECT_ID('tempdb..#BenchmarkDetailItem') IS NOT NULL
   DROP TABLE #BenchmarkDetailItem

	CREATE TABLE #BenchmarkDetailItem(
     ObjectID INT,
	 BenchmarkID INT
	 ,RespondentCount INT
				,FavPerc INT
				,UnFavPerc INT
				,NeuPerc INT
				,Mean NUMERIC(10, 2)
				,SurveyFormItemID BIGINT
				,SurveyCategoryId INT

	)
	  IF OBJECT_ID('tempdb..#ITEMCOUNT') IS NOT NULL
      DROP TABLE #ITEMCOUNT
	  CREATE TABLE #ITEMCOUNT(
	  ItemCount INT)

	 IF OBJECT_ID('tempdb..#BenchmarkItemScore') IS NOT NULL
   DROP TABLE #BenchmarkItemScore

	CREATE TABLE #BenchmarkItemScore(
    
				FavPerc INT
				,Mean NUMERIC(10, 2)
				,RespondentCount INT

	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END
		print @SurveyIDQuery

		DECLARE @BENCHMARKDEATILS NVARCHAR(MAX)
		DECLARE @ItemCount NVARCHAR(MAX)
		DECLARE @ItemCount1 NVARCHAR(MAX)
		DECLARE @BENCHMARKCOUNT INT
  IF(@SurveyFormItemID !='1=1')
  BEGIN 
		SELECT @BENCHMARKDEATILS =
		' SELECT  DISTINCT DB.ObjectID, DB.BenchmarkID, DB.RespondentCount,DB.FavPerc, DB.UnFavPerc, DB.NeuPerc,DB.Mean, DI.SurveyformItemId, DI.SurveyCategoryID FROM 
		  DimBenchmark DB
		  INNER JOIN Dimitem DI 
		  ON DB.ObjectID = DI.ParentItemID
		  WHERE DB.BenchmarkID ='+ Convert(varchar(10),@BenchmarkId)+ 
		  'AND '+@SurveyIDQuery+
		  'AND '+@SurveyFormItemID+
		  'AND DI.GroupName= ''RatingScale'''

		  INSERT INTO #BenchmarkDetailItem

		    EXEC (@BENCHMARKDEATILS)
		   
		    SELECT @ItemCount =
		    'SELECT COUNT(DISTINCT ParentItemID)
		     FROM Dimitem 
		     Where '+@SurveyFormItemID+
		     'AND  '+@SurveyIDQuery +
		     'AND GroupName= ''RatingScale'''
            INSERT Into #ITEMCOUNT
		    EXEC(@ItemCount)
		

		   Select @BENCHMARKCOUNT= count(ObjectID) from #BenchmarkDetailItem 
		   Select @ItemCount1 = ItemCount from #ITEMCOUNT
	       Print @BENCHMARKCOUNT
		   print @ItemCount1
		   IF (@BENCHMARKCOUNT = @ItemCount1)
		   BEGIN
		   Insert into #BenchmarkItemScore
		   Select Round(AVG(CONVERT(NUMERIC(10),FavPerc)),0) as BenchmarkPerc,Round(AVG(CONVERT(DECIMAL(10,2),Mean)),2), MAX(RespondentCount)
		   FROM  #BenchmarkDetailItem
		   Group By BenchmarkID
		   SELECT FavPerc As BenchmarkFavPerc,Mean, RespondentCount FROM #BenchmarkItemScore

		   END 
		   ELSE IF(@BENCHMARKCOUNT =0)
		   BEGIN
		   INSERT into #BenchmarkItemScore VALUES(Null, NUll, NULL)
		   SELECT FavPerc As BenchmarkFavPerc,Mean, RespondentCount FROM  #BenchmarkItemScore
		   END 
		   ELSE IF (@BENCHMARKCOUNT != @ItemCount1)
		   BEGIN 
		   Insert into #BenchmarkItemScore
		   SELECT Round(AVG(CONVERT(NUMERIC(10),FavPerc)),0) as BenchmarkFavPerc,Round(AVG(CONVERT(DECIMAL(10,2),Mean)),2) As Mean, MAX(RespondentCount)
		   FROM  #BenchmarkDetailItem
		   GROUP By BenchmarkID

		   UPDATE #BenchmarkItemScore
		   SET FavPerc = NULL, Mean = NULL 
		   SELECT FavPerc As BenchmarkFavPerc,Mean, RespondentCount from  #BenchmarkItemScore
		   END 
		  
END 
  ElSE IF (@Category !='1=1')
           BEGIN
		   	 IF OBJECT_ID('tempdb..#BenchmarkCategoryDetails') IS NOT NULL
             DROP TABLE #BenchmarkCategoryDetails

			 CREATE TABLE #BenchmarkCategoryDetails
			 (
			  BenchmarkID INT,
			  FavPerc INT,
			  MEAN NUMERIC(10,2),
			  RespondentCount INT,
			  SurveyformItemID INT,
			  SurveyCategoryID INT
			  )

			  IF OBJECT_ID('tempdb..#BenchmarkCategoryScore') IS NOT NULL
              DROP TABLE #BenchmarkCategoryScore
			  CREATE TABLE #BenchmarkCategoryScore
			   (
			   BenchmarkPerc INT,
			   MEAN  NUMERIC (10,2),
			   RespondentCount INT
			   )

			   
			  IF OBJECT_ID('tempdb..#CategoryToItemCount') IS NOT NULL
              DROP TABLE #CategoryToItemCount
			  CREATE TABLE #CategoryToItemCount
			   (
			     SurveycategoryID INT,
				 ITEMCOUNT INT
			   )
			     
			  IF OBJECT_ID('tempdb..#BenchmarkItemCategoryCount') IS NOT NULL
                 DROP TABLE #BenchmarkItemCategoryCount
			  CREATE TABLE #BenchmarkItemCategoryCount
			   (
			     SurveycategoryID INT,
				 BenchmarkCOUNT INT
			   )

			   DECLARE @ITEMTOCATEGORYCOUNT NVARCHAR(MAX)
			   DECLARE @BENCHMARKITEMCATEGORYCOUNT NVARCHAR(MAX)
			   DECLARE @S NVARCHAR(MAX)
			   DECLARE @BENCHMARKCATEGORYCOUNT INT
			   DECLARE @ITEMCATEGORYCOUNT INT
			    
				SELECT @S =
			 '  Select Distinct DB.BenchmarkID, DB.FavPerc,DB.Mean, DB.RespondentCount,DB.ObjectID, DI.SurveycategoryID from  
                DimBenchmark DB
                INNER JOIN Dimitem DI
                ON DB.ObjectId= DI.ParentItemID
                WHERE DB.BenchmarkId='+ CONVERT(VARCHAR(10),@BenchmarkID)+
	            'AND '+@SurveyIDQuery+
				' AND  DI.'+@Category+
	            'AND DI.GroupName=''RatingScale'''
               
				INSERT INTO #BenchmarkCategoryDetails
				EXEC (@S)
				--Select * from #BenchmarkCategoryDetails

			   SELECT @ITEMTOCATEGORYCOUNT=
			   'Select  Distinct SurveycategoryID, COUNT(DISTINCT SurveyformItemID)
			   FROM  DimItem DI
			   WHERE 
			   '+@SurveyIDQuery+
			   'AND '+ @Category+
			   'AND  DI.GroupName =''RatingScale''
			   GROUP BY DI.SurveyCategoryID'
			   INSERT INTO #CategoryToItemCount
			   EXEC (@ITEMTOCATEGORYCOUNT)

			   --SELECT * FROM #CategoryToItemCount

			   INSERT INTO #BenchmarkItemCategoryCount
			   SELECT SurveycategoryID, Count(SurveyformItemID)
			   FROM #BenchmarkCategoryDetails
			   GROUP BY SurveycategoryID

			  -- SELECT * FROM #BenchmarkItemCategoryCount

			   SELECT @BENCHMARKCATEGORYCOUNT = Count(BC.SurveycategoryID) from #BenchmarkItemCategoryCount BC
												INNER JOIN #CategoryToItemCount IC
												 ON IC.SurveycategoryID= BC.SurveycategoryID
												 WHERE BC.BenchmarkCOUNT= IC.ITEMCOUNT
               PRINT @BENCHMARKCATEGORYCOUNT
			   SELECT @ITEMCATEGORYCOUNT = Count(SurveyCategoryID) FROM #CategoryToItemCount
			   PRINT  @ITEMCATEGORYCOUNT
			   IF (@BENCHMARKCATEGORYCOUNT = @ITEMCATEGORYCOUNT)
			   BEGIN
			        INSERT INTO #BenchmarkCategoryScore
			        SELECT Round(AVG(CONVERT(NUMERIC(10),FavPerc)),0) as BenchmarkFavPerc,Round(AVG(CONVERT(DECIMAL(10,2),Mean)),2) as Mean, MAX(RespondentCount)
					FROM #BenchmarkCategoryDetails
					GROUP BY SurveyCategoryID
					SELECT Round(AVG(CONVERT(NUMERIC(10),BenchmarkPerc)),0) as BenchmarkFavPerc,Round(AVG(CONVERT(DECIMAL(10,2),Mean)),2) as Mean, MAX(RespondentCount) [RespondentCount]
				    FROM #BenchmarkCategoryScore
			   END
			   ELSE 
			   BEGIN 
			       INSERT INTO #BenchmarkCategoryScore Values(NULL, NULL, NULL)
				   SELECT BenchmarkPerc AS BenchmarkFavPerc,Mean, RespondentCount FROM  #BenchmarkCategoryScore
			  END 
	
  END
  ELSE IF (@SurveyFormItemID ='1=1' AND @Category ='1=1')
         BEGIN
		       IF OBJECT_ID('tempdb..#BenchmarkALLDetails') IS NOT NULL
             DROP TABLE #BenchmarkALLDetails

			 CREATE TABLE #BenchmarkALLDetails
			 (
			  BenchmarkID INT,
			  ObjectID INT,
			  FavPerc INT,
			  MEAN NUMERIC(10,2),
			  RespondentCount INT,
			  SurveyformItemID INT
			)

			  IF OBJECT_ID('tempdb..#BenchmarkALLValue') IS NOT NULL
             DROP TABLE #BenchmarkALLValue

			 CREATE TABLE #BenchmarkALLValue
			 (
			  BenchmarkFavPerc INT,
			  MEAN NUMERIC(10,2),
			  RespondentCount INT,
			 )
			 IF OBJECT_ID('tempdb..#ALLItemCount') IS NOT NULL
             DROP TABLE #ALLItemCount
			 CREATE TABLE #ALLItemCount
			 (
			   ItemCount INT
			 )
			 DECLARE @ALLITEMCOUNT1 NVARCHAR(MAX)
			  DECLARE @ALLITEMCOUNT NVARCHAR(MAX)
			 DECLARE @ALLBENCHMARKCOUNT NVARCHAR(MAX)
			 DECLARE @ALL NVARCHAR(MAX)

			 SELECT @ALL=
			 'Select DISTINCT DB.BenchmarkID,DB.ObjectID, DB.Favperc ,DB.Mean ,DB.Respondentcount,Di.SurveyformItemID
			 FROM Dimbenchmark DB
			 INNER JOIN DimItem DI
			 ON DI.ParentItemID = DB.ObjectID
			 WHERE BenchmarkID='+CONVERT(VARCHAR(10),@BenchmarkId)+
			 'AND '+@SurveyIDQuery+
			 'AND  DI.GroupName =''RatingScale'''
			  INSERT INTO #BenchmarkALLDetails
			  EXEC (@ALL)

			  SELECT @ALLITEMCOUNT1 =
			  'Select Count(DISTINCT SurveyformitemID)  
			  FROM DimItem
			  Where  '+ @SurveyIDQuery+ 
			  'AND GroupName =''RatingScale''
			   AND ParentItemID Is Not NULL '

			   INSERT INTO #ALLItemCount
			   Exec (@ALLITEMCOUNT1)
			   SELECT @ALLITEMCOUNT = ItemCount FROM  #ALLItemCount

			   SELECT @ALLBENCHMARKCOUNT  = Count(ObjectID) FROM #BenchmarkALLDetails
			   Print @ALLBENCHMARKCOUNT
			   IF (@ALLITEMCOUNT= @ALLBENCHMARKCOUNT)
			   BEGIN
			        INSERT INTO #BenchmarkALLValue
					SELECT Round(AVG(CONVERT(NUMERIC(10),FavPerc)),0) as BenchmarkFavPerc,Round(AVG(CONVERT(DECIMAL(10,2),Mean)),2), MAX(RespondentCount)
					FROM #BenchmarkALLDetails
					GROUP by BenchmarkID

					SELECT BenchmarkFavPerc,Mean, RespondentCount FROM #BenchmarkALLValue
				END 
				ELSE 
				BEGIN
				     INSERT INTO #BenchmarkALLValue Values(NULL, NULL, NULL)

					 SELECT BenchmarkFavPerc,Mean, RespondentCount FROM #BenchmarkALLValue
				END

		 END 

END";
        #endregion

        #region BenchMarkCalculationTrend_NR_V11
        public static string BenchMarkCalculationTrend_NR_V11 = @"
Create OR ALTER  PROCEDURE [dbo].[BenchMarkCalculationTrend_NR_V11](
@BenchmarkId BIGINT,
@SurveyIds NVARCHAR(MAX),
@SurveyFormItemID NVARCHAR(MAX),
@ScaleOptionId NVARCHAR(MAX)
)
AS
BEGIN
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
                DECLARE @SurveyIDQuery NVARCHAR(MAX)
                DECLARE @NRQuery NVARCHAR(MAX)
				DECLARE @GroupMinimumNSize NVARCHAR(MAX)
				SELECT @GroupMinimumNSize = GroupMinimumSize FROM DimSurveySetting DS
				INNER JOIN Dimitem_V11 DI
				on DI.SurveyFormID= DS.SurveyFormID
				where DI.SurveyFormItemID= @SurveyFormItemID
				

                IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
   DROP TABLE #SurveyList

                CREATE TABLE #SurveyList (
                ID INT IDENTITY(1,1)
                ,SurveyID VARCHAR(1000)
                )

                DECLARE @CNT INT        
                SET @CNT = 1    
                WHILE (CHARINDEX(',',@SurveyIDs)>0)
                BEGIN                   
                                INSERT INTO #SurveyList (SurveyID )                      
                                SELECT  SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))                   
                                SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))                   
                                SET @CNT = @CNT + 1   
                END                       
                INSERT INTO #SurveyList (SurveyID )      
                SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs)) 

                                IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
                                BEGIN
                                                SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
                                                SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
                                END
                                ELSE
                                BEGIN
                                                SET @SurveyIDQuery = 'SurveyID > 0'
                                END
                                print @SurveyIDQuery


                                Select @NRQuery = 
                'Select Distinct DI.ScaleOptionId As BenchmarkScaleOptionId,DB.RespPerc,DI.SurveyformItemID,D.RespondentCount 
                FROM DimBenchMark_NR DB
                INNER JOIN DimItem DI
                ON DB.Itemid= DI.ParentItemID and DB.ScaleOptionId= DI.ParentScaleOptionID
				INNER JOIN DimBenchmark D
				on D.BenchmarkId = DB.BenchmarkId and D.ObjectID=DB.Itemid
                Where Db.BenchmarkId='+Convert(varchar(10),@BenchmarkID)+
                'AND  '+ @SurveyIDQuery +
                'AND DI.ScaleOptionID=' +@ScaleoptionID +
				 'AND D.Favperc is NULL'

EXEC (@NRQuery)
PRINT @NRQuery



END";
        #endregion

        #region BenchMarkCalculation_V11
        public static string BenchMarkCalculation_V11 = @"


use TCESReporting
GO


CREATE OR ALTER    PROCEDURE [dbo].[BenchMarkCalculation_V11](
 @BenchmarkId BIGINT,
 @SurveyIds NVARCHAR(MAX),
 @Surveyformitemquery NVARCHAR(MAX) = NULL

 )

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SurveyIDQuery NVARCHAR(MAX)
	DECLARE @RSQuery NVARCHAR(MAX)
	DECLARE @NRQuery NVARCHAR(MAX)
	DECLARE @RSCategory NVARCHAR(MAX)
	DECLARE @CategoryWithNoBenchMark NVARCHAR(MAX)
	DECLARE @CategoryIDs NVARCHAR(MAX)
	DECLARE @ItemCount NVARCHAR(MAX)

	  IF OBJECT_ID('tempdb..#ItemCategorylist') IS NOT NULL
   DROP TABLE #itemCategorylist

   CREATE TABLE #ItemCategoryList (
   ObjectID BIGINT,
   ItemMappedtoCategoryCount int
   )

   IF OBJECT_ID('tempdb..#BenchmarkCategoryList') IS NOT NULL
   DROP TABLE #BenchmarkCategoryList

   CREATE TABLE #BenchmarkCategoryList(
   ObjectID BIGINT,
   BenchmarkFavPerc BIGINT,
   BenchmarkMean decimal(10,2),
   [Total Responses] BIGINT
   )
   IF OBJECT_ID('tempdb..#CategoryWithNoBenchMark') IS NOT NULL
   DROP TABLE #CategoryWithNoBenchMark

   CREATE TABLE #CategoryWithNoBenchMark(
   ObjectID NVARCHAR(MAX),
   )

   IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
   DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT	
	SET @CNT = 1	
	WHILE (CHARINDEX(',',@SurveyIDs)>0)
	BEGIN		
		INSERT INTO #SurveyList (SurveyID )		
		SELECT	SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1,CHARINDEX(',',@SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs,CHARINDEX(',',@SurveyIDs)+1,LEN(@SurveyIDs))		
		SET @CNT = @CNT + 1	
	END		
	INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	

		IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN
			SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (',@SurveyIDQuery,')')
		END
		ELSE
		BEGIN
			SET @SurveyIDQuery = 'SurveyID > 0'
		END
		print @SurveyIDQuery

IF @Surveyformitemquery Is NULL
BEGIN 
   SET @Surveyformitemquery='1=1';
   END 

Select @ItemCount=

 'Select  DI.SurveycategoryID, count(distinct DI.surveyformitemID)  
	FROM  DimBenchmark Db
	INNER JOIN Dimitem DI
	ON DB.ObjectId= DI.ParentItemID 
	WHERE Db.BenchmarkId='+Convert(varchar(10),@BenchmarkID) +
	'AND  '+ @SurveyIDQuery+
	'AND  '+@Surveyformitemquery+
	'AND DI.GroupName=''RatingScale''
	Group By DI.SurveyCategoryId'
	
Insert into #ItemCategoryList
Exec (@ItemCount)

Select @RSQuery = 
	'Select Distinct DI.SurveyFormItemID ,Db.FavPerc AS BenchmarkFavPerc, Db.NeuPerc,Db.UnFavPerc, Db.Mean, Db.RespondentCount [Total Responses]
	FROM  DimBenchmark Db
	INNER JOIN Dimitem DI
	ON DB.ObjectId= DI.ParentItemID 
	WHERE Db.BenchmarkId='+Convert(varchar(10),@BenchmarkID) +
	'AND  '+ @SurveyIDQuery+
	'AND  '+@Surveyformitemquery+
	'AND DI.GroupName=''RatingScale'''
EXEC (@RSQuery)
Print @RSQuery

Select @NRQuery = 
   'Select Distinct DI.ScaleOptionId As BenchmarkScaleOptionId,DB.RespPerc,DI.SurveyformItemID,D.RespondentCount [Total Responses]
    FROM DimBenchMark_NR DB
	INNER JOIN DimItem DI
	ON DB.Itemid= DI.ParentItemID and DB.ScaleOptionId= DI.ParentScaleOptionID
	INNER JOIN DimBenchmark D
    ON D.BenchmarkId = DB.BenchmarkId and D.ObjectID=DB.Itemid
	Where Db.BenchmarkId='+Convert(varchar(10),@BenchmarkID)+
	'AND  '+ @SurveyIDQuery+
	'AND  '+@Surveyformitemquery+ ''
EXEC (@NRQuery)
PRINT @NRQuery



Select @RSCategory =
 ' Select DI.SurveyCategoryId As ObjectID,Round(AVG(CONVERT(numeric(10),DB.FavPerc)),0) as BenchmarkPerc,Round(AVG(CONVERT(DECIMAL(10,2),DB.Mean)),2) as BenchmarkMean, Max(DB.RespondentCount)[Total Responses] from  
   DimBenchmark DB
   INNER JOIN Dimitem DI
   ON DB.ObjectId= DI.ParentItemID
   WHERE DB.BenchmarkId='+ CONVERT(VARCHAR(10),@BenchmarkID)+
	'AND '+@SurveyIDQuery+
	'AND '+@Surveyformitemquery+
	'AND DI.GroupName=''RatingScale''
	And DI.categoryText not in (''unassigned'')
	 Group By DI.SurveyCategoryID'

Insert into #BenchmarkCategoryList
EXEC(@RSCategory)
Print @RSCategory


Update b SET b.[Total Responses] =b.[Total Responses] /l.ItemMappedtoCategoryCount
from #BenchmarkCategoryList b 
inner join #ItemCategoryList l
on b.ObjectID = l.ObjectID
where b.ObjectID=l.ObjectID


Select @CategoryWithNoBenchMark=
   'Select DISTINCT SurveyCategoryId from  DimItem DI
   WHERE SurveyformitemID NOT IN (Select DISTINCT DI.SurveyFormItemID
	FROM  DimBenchmark Db
	INNER JOIN Dimitem DI
	ON DB.ObjectId= DI.ParentItemID 
	WHERE Db.BenchmarkId='+ Convert(varchar(10),@BenchmarkId)+
	'AND '+@SurveyIDQuery+
	'AND '+@Surveyformitemquery+
	'AND DI.GroupName=''RatingScale'') 
	 AND '+@SurveyIDQuery+
	 'AND '+@Surveyformitemquery+
	'AND ParentItemID IS NOT NULL
     AND GroupName=''RatingScale'''

INSERT INTO #CategoryWithNoBenchMark
EXEC (@CategoryWithNoBenchMark)

SELECT @CategoryIDs = COALESCE(@CategoryIDs+',','')+ ObjectID FROM #CategoryWithNoBenchMark

Print @CategoryIDs

DECLARE @s NVARCHAR(MAX)

SELECT @S=
'Update #BenchmarkCategoryList
SET 
BenchmarkFavPerc = NULL, BenchmarkMean= NULL,[Total Responses]=NULL
 where ObjectID IN('+@CategoryIDs+')'
EXEC(@S)
Print @S

SELECT * FROM #BenchmarkCategoryList

END"
;
        #endregion

        #region  QAPROCResultWordCloud
        public static string QAPROCResultWordCloud = @"
        CREATE OR ALTER PROCEDURE[dbo].[QAPROCResultWordCloud] (
	@SurveyFormID BIGINT,
    @SurveyIDs VARCHAR(1000),
	@DemoQuery NVARCHAR(MAX),
	@TimePeriodQuery NVARCHAR(MAX)
   ,@commentSize INT = 25  
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
    SET NOCOUNT ON;
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

        DECLARE @CompletionRuleQuery NVARCHAR(MAX) = ''
	DECLARE @compRuleSettingType INT
    DECLARE @InsertQuery NVARCHAR(MAX)

    DECLARE @WordCount NVARCHAR(MAX)

    DECLARE @InsertQuery1 NVARCHAR(MAX)


    SELECT @compRuleSettingType = CompletionRuleSettingType FROM DimSurveySetting WHERE SurveyFormID = @SurveyFormID

    SET @CompletionRuleQuery = IIF(@compRuleSettingType = 1, 'CompletionRuleFlag = 1', 'SurveyStatusFlag = 2')


    DECLARE @SurveyIDQuery NVARCHAR(MAX)


    IF OBJECT_ID('tempdb..#SurveyList') IS NOT NULL
        DROP TABLE #SurveyList

	CREATE TABLE #SurveyList (
	ID INT IDENTITY(1,1)
	,SurveyID VARCHAR(1000)
	)

	DECLARE @CNT INT
    SET @CNT = 1	

    WHILE(CHARINDEX(',', @SurveyIDs)>0)
	BEGIN
        INSERT INTO #SurveyList (SurveyID )		
		SELECT  SurveyID = LTRIM(RTRIM(SUBSTRING(@SurveyIDs,1, CHARINDEX(',', @SurveyIDs)-1)))		
		SET @SurveyIDs = SUBSTRING(@SurveyIDs, CHARINDEX(',', @SurveyIDs) + 1, LEN(@SurveyIDs))

        SET @CNT = @CNT + 1

    END
    INSERT INTO #SurveyList (SurveyID )	
	SELECT SurveyID = LTRIM(RTRIM(@SurveyIDs))	


        IF((SELECT count(*)  FROM #SurveyList WHERE LEN(SurveyID) > 0) > 0)
		BEGIN

            SELECT @SurveyIDQuery = COALESCE(@SurveyIDQuery + ', ', '') + SurveyID FROM #SurveyList
			SET @SurveyIDQuery = CONCAT('SurveyID IN (', @SurveyIDQuery,')')

        END
        ELSE

        BEGIN
            SET @SurveyIDQuery = 'SurveyID > 0'
		END
        print @SurveyIDQuery

IF OBJECT_ID('tempdb..#FactResponsecomment') IS NOT NULL
  DROP TABLE #FactResponseComment
   
CREATE TABLE #FactResponsecomment  (  
 ItemID BIGINT  
 ,ParticipantID BIGINT  

 )
 IF OBJECT_ID('tempdb..#FactResponsecommentWithAllComments') IS NOT NULL
  DROP TABLE #FactResponsecommentWithAllComments
   
CREATE TABLE #FactResponsecommentWithAllComments (  
 ItemID BIGINT  
 ,SurveywaveparticipantID BIGINT 

 )  

IF OBJECT_ID('tempdb..#WordCountAndLibrary') IS NOT NULL
  DROP TABLE #WordCountAndLibrary
   
CREATE TABLE #WordCountAndLibrary  (  
  WordId bigint,
  SurveyformitemID Bigint,
  WordFrequency Bigint

 )  
IF OBJECT_ID('tempdb..#WordCount') IS NOT NULL
  DROP TABLE #WordCount
   
CREATE TABLE #WordCount  (  
  countofWord bigint,
  WordID Bigint,
  SurveyformItemID Bigint,
  Word NVARCHAR(MAX)

 )  

SELECT @InsertQuery =
    'SELECT  FR.SurveyFormItemID
        , SurveyWaveParticipantID
      FROM FactResponsecomment_V11 FR WITH(NOLOCK)


    WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
	AND FR.' + @SurveyIDQuery + '

    AND '+@TimePeriodQuery+' 

    AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'

    AND ' + @SurveyIDQuery + '

    AND '+@DemoQuery+' 

    AND '+@CompletionRuleQuery+' 

    AND[Status]= 1) AND SurveyformItemID in(SELECT Distinct surveyformItemId from Dimitem_V11 where ItemStyleID in(5,7) AND SurveyformID = '+CONVERT(VARCHAR(10),@SurveyFormID)+ ')'
PRINT @InsertQuery


INSERT INTO #FactResponsecomment 
EXEC(@InsertQuery)



SELECT @WordCount =
'Select Count(*),WI. WordID,WI.SurveyFormItemID,WL.word
 FROM WordIndex WI WITH(NOLOCK)
 INNER JOIN WORDLIBRARY WL
 on WI.WordID = WL.WordID
 WHERE WI.SurveyFormItemID
 IN(Select Distinct ItemID from #FactResponsecomment) 
 AND WI.WordID NOT IN( Select WordID from StopWord)
 AND ' + @SurveyIDQuery+' AND WI.surveywaveparticipantID IN( Select Distinct ParticipantID from  #FactResponsecomment)
group by WI.WordID, WI.SurveyFormItemID, WL.Word'

Insert into #WordCount
Exec (@WordCount)

print @wordcount


--Select* from  #WordCount
; with
 CommentWordCount(wordID, SurveyformItemID, _rank, countofwords)
As
(
Select WordID, surveyformitemID ,
_rank = rank() over (partition by surveyformitemID order by countofword desc, Word ASC),countofWord from #WordCount
)
 Insert into #WordCountAndLibrary
Select CW.wordID, CW.surveyformitemID, CW.countofwords from  CommentWordCount CW
where _Rank<= 25


Select WL.WordID, WCL.SurveyformitemID, WL.Word, WCL.WordFrequency from #WordCountAndLibrary WCL
inner join  WordLibrary WL
on WCL.WordId= WL.WordId



SELECT @InsertQuery1 =
    'SELECT  FR.SurveyFormItemID
        , SurveyWaveParticipantID
      FROM FactResponsecomment_V11 FR WITH (NOLOCK)


    WHERE FR.SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+' 
	AND FR.' + @SurveyIDQuery + '

    AND '+@TimePeriodQuery+' 

    AND FR.SurveyWaveParticipantID in (SELECT Distinct SurveyWaveParticipantID FROM DimParticipant_V11 WHERE SurveyFormID = '+CONVERT(VARCHAR(10),@SurveyFormID)+'

    AND ' + @SurveyIDQuery + '

    AND '+@DemoQuery+' 

    AND '+@CompletionRuleQuery+' 

    AND[Status]= 1) AND SurveyformItemID in(SELECT Distinct surveyformItemId from Dimitem_V11 where  SurveyformID='+CONVERT(VARCHAR(10),@SurveyFormID)+ ')'
PRINT @InsertQuery1
 Insert into #FactResponsecommentWithAllComments

Exec(@InsertQuery1)

; With ItemwithRespondentCount(ObjectID, [Total Responses])
AS
(
Select ItemID, count(SurveywaveparticipantID) from #FactResponsecommentWithAllComments
group by ItemID
)

Select* from  ItemwithRespondentCount





END";
        #endregion


        public static bool ValidateIFUserDataExists(string Environment, DBConnectionStringDTO DBConnectionParameters)
        {
            try
            {
                string Query = @"SELECT * FROM sys.tables WHERE [name] LIKE '%tblScenarioUserAnalyzeFilterXref%'";
                DataSet ds = new DataSet();
                ds = DBOperations.ExecuteSPwithInputParameters(Query, DBConnectionParameters);
                if (ds.Tables[0].Rows.Count == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool ValidateIFV11TableExists(DBConnectionStringDTO DBConnectionParameters)
        {
            try
            {
                DataSet ds = new DataSet();
                string Query = "SELECT * FROM sys.tables WHERE [name] LIKE '%_V11'";
                ds = DBOperations.ExecuteSPwithInputParameters(Query, DBConnectionParameters);
                if (ds.Tables[0].Rows.Count != 4)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static void PopulateData(string Url, DBConnectionStringDTO DBConnectionParameters)
        {
            try
            {
                string Env = Url.Substring(8, 2);
                DataSet ds = new DataSet();
                bool IFUserDataExists = ValidateIFUserDataExists(Env, DBConnectionParameters);
                if (IFUserDataExists == false)
                {
                    if (Env.ToLower() == "qa")
                    {
                        ds = DBOperations.ExecuteSPwithInputParameters(QA_tblTestScenario, DBConnectionParameters);
                        ds = DBOperations.ExecuteSPwithInputParameters(QA_ActualSPWithComparison_Stg, DBConnectionParameters);
                        ds = DBOperations.ExecuteSPwithInputParameters(QA_TestSPScenario_Stg, DBConnectionParameters);
                        ds = DBOperations.ExecuteSPwithInputParameters(QA_InsertQuery, DBConnectionParameters);
                    }
                    else
                    {
                        ds = DBOperations.ExecuteSPwithInputParameters(Stg_tblTestScenario, DBConnectionParameters);
                        ds = DBOperations.ExecuteSPwithInputParameters(Stg_ActualSPWithComparison_Stg, DBConnectionParameters);
                        ds = DBOperations.ExecuteSPwithInputParameters(Stg_TestSPScenario_Stg, DBConnectionParameters);
                        ds = DBOperations.ExecuteSPwithInputParameters(Stg_InsertQuery, DBConnectionParameters);
                    }
                }
                bool IFV11TableExists = ValidateIFV11TableExists(DBConnectionParameters);
                if (IFV11TableExists == false)
                {
                    if (Env.ToLower() == "qa")
                        ds = DBOperations.ExecuteSPwithInputParameters(QA_V11Tables, DBConnectionParameters);
                    else
                        ds = DBOperations.ExecuteSPwithInputParameters(Stg_V11Tables, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_DemographicReportCalcQuestions_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_ReportCalculationNonRatingScale_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_TrendReport_Rating_V11_New, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_Year, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_Quarter, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_Month, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_Distribution, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_GetTestSPIDFromScenarios_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(Parent_SP_QAPROC_ResultsReport_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(Parent_SP_QAPROC_ReportCalculationCategory_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_ReportCalculationQuestions_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_CommentReportCalc_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_CommentReportCalc_UnScored_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(Parent_SP_QAPROC_DemographicReportCalcCategory_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(Parent_SP_QAPROC_DemographicReportCalcQuestions_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_DemographicReportResponseCalc_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_GetTestScenarios_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_TrendReport_NR_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_NR_Year, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_NR_Quarter, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_NR_Month, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(TrendSPExecution_NR_Distribution, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_ReportCalculationCategory_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_DemographicReportCalcCategory_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROC_GetFilterResults, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(BenchMarkCalculationTrend_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(BenchMarkCalculationTrend_NR_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(BenchMarkCalculation_V11, DBConnectionParameters);
                    ds = DBOperations.ExecuteSPwithInputParameters(QAPROCResultWordCloud, DBConnectionParameters);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
            }
        }
    }
}
