﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class MatchPairResponseRateCheck
    {
        public static DBConnectionStringDTO DBConnectionParameters;

        public static DataSet GetSPResultToDataSet(DBConnectionStringDTO DBConnectionParameters,DataTable dt, string scenario)
        {
            DataSet ds = new DataSet();
            foreach (DataRow drow in dt.Rows)
            {
                if((drow["Scenarios"].ToString().ToLower() == scenario.ToLower()) && (drow["InputParameter1"]).ToString() != "")
                {
                    ds = DBOperations.ExecuteSPwithInputParameters(drow["InputParameter1"].ToString(), DBConnectionParameters);
                }
            }
            return ds;
        }
        public static bool ValidateResponsesCount(DataTable dt, DataSet ds, string scenario)
        {
            string DTsurveywaveid = null;
            string DTnoofresponses = null;
            string DSnoofresponses = null;
            foreach (DataRow drow in dt.Rows)
            {
                if (drow["Scenarios"].ToString().ToLower() == scenario.ToLower())
                {
                    DTsurveywaveid = drow["SurveyWaveId"].ToString();
                    DTnoofresponses = drow["ExpectedResult1"].ToString();
                }
            }
            foreach(DataRow dsrow in ds.Tables[0].Rows)
            {
                if(dsrow["SurveyWaveId"].ToString() == DTsurveywaveid)
                {
                        DSnoofresponses = dsrow["NumberOfResponses"].ToString();
                }
            }
            return DTnoofresponses == DSnoofresponses ? true : false;
        }
        public static bool ValidateResponses_ResponseRate(DataTable dt, DataSet ds, string scenario)
        {
            foreach (DataRow drow in dt.Rows)
            {
                if (drow["Scenarios"].ToString().ToLower() == scenario.ToLower())
                {
                    if (scenario.ToLower() == "report" && drow["ExpectedResult2"].ToString() == "")
                        return drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString() ? true : false;
                    else if (scenario.ToLower() == "report" && drow["ExpectedResult2"].ToString() != "")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["ResponseRateWithoutDemo"].ToString())
                            ? true : false;

                    else if (scenario.ToLower() == "report with demo" && drow["ExpectedResult3"].ToString() == "")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            ? true : false;
                    else if (scenario.ToLower() == "report with demo" && drow["ExpectedResult3"].ToString() != "")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["ResponseRateWithDemo"].ToString())
                            && (drow["ExpectedResult3"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            && (drow["ExpectedResult4"].ToString() == ds.Tables[0].Rows[0]["ResponseRateWithoutDemo"].ToString())
                            ? true : false;

                    else if (scenario.ToLower() == "report with timeperiod")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            ? true : false;

                    else if (scenario.ToLower() == "report with demo & timeperiod")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            ? true : false;

                    else if (scenario.ToLower() == "report with demo as timeperiod")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            ? true : false;

                    else if (scenario.ToLower() == "report with date demo" && drow["ExpectedResult3"].ToString() == "")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            ? true : false;
                    else if (scenario.ToLower() == "report with date demo" && drow["ExpectedResult3"].ToString() != "")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["ResponseRateWithDemo"].ToString())
                            && (drow["ExpectedResult3"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            && (drow["ExpectedResult4"].ToString() == ds.Tables[0].Rows[0]["ResponseRateWithoutDemo"].ToString())
                            ? true : false;

                    else if (scenario.ToLower() == "report with user filter" && drow["ExpectedResult3"].ToString() == "")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            ? true : false;
                    else if (scenario.ToLower() == "report with user filter" && drow["ExpectedResult3"].ToString() != "")
                        return (drow["ExpectedResult1"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithDemo"].ToString())
                            && (drow["ExpectedResult2"].ToString() == ds.Tables[0].Rows[0]["ResponseRateWithDemo"].ToString())
                            && (drow["ExpectedResult3"].ToString() == ds.Tables[0].Rows[0]["RespondentCountWithoutDemo"].ToString())
                            && (drow["ExpectedResult4"].ToString() == ds.Tables[0].Rows[0]["ResponseRateWithoutDemo"].ToString())
                            ? true : false;
                }
            }
            return false;
        }
    }
}
