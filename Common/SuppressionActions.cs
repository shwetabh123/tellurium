﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;

using OpenQA.Selenium.Interactions;

namespace Common
{
    public class SuppressionActions
    {
        public static void NavigatetoAccountSuppression(IWebDriver driver)
        {
            //click Arrow Down
           // IWebElement Arrowdownfor_settings = UIActions.FindElementWithXpath(driver, "//*[@id='navbar-container']//ul/li/a[@href='#']");
           // Arrowdownfor_settings.Click();
           // Thread.Sleep(10000);
            //click Settings
            IWebElement SettingsClick = UIActions.FindElementWithXpath(driver, "//A[@href='/Pulse/AccountSettings']");
            SettingsClick.Click();
            Thread.Sleep(10000);
            IWebElement AccountSupression = UIActions.FindElementWithXpath(driver, "//*[@id='menu-account-suppression']");
            AccountSupression.Click();
        }
        public static void SetGroupNSizetoReportDemographics(IWebDriver driver, string GroupNSizetoReport)
        {
            IWebElement GroupNSizetoReportDemographics = UIActions.FindElementWithXpath(driver, "//input[@id='suppression-groupnsizedemo']");
            //   GroupNSizetoReportDemographics.SendKeys("");
            GroupNSizetoReportDemographics.Clear();
            Thread.Sleep(20000);
            GroupNSizetoReportDemographics.SendKeys(GroupNSizetoReport);
        }
        public static void SetDemographicValueMinimum(IWebDriver driver, string DemographicValueMinimumvalue)
        {
            IWebElement DemographicValueMinimum = UIActions.FindElementWithXpath(driver, "//*[@id='suppression-demovaluemin']");
            //      DemographicValueMinimum.SendKeys("");
            DemographicValueMinimum.Clear();
            Thread.Sleep(20000);
            DemographicValueMinimum.SendKeys(DemographicValueMinimumvalue);
        }
        public static void Saveaccountsuppression(IWebDriver driver)
        {
            IWebElement saveaccountsuppression = UIActions.FindElementWithXpath(driver, "//*[@id='suppression-save']");
            saveaccountsuppression.Click();
            Thread.Sleep(5000);
        }
        public static void SelectAllTimePeriod(IWebDriver driver, string distributionName)
        {
            IWebElement timeperiodall = UIActions.FindElementWithXpath(driver, "(//INPUT[@name='timeperiod-selection'])[1] ");
            timeperiodall.Click();
        }
        public static void SelectLastMonthTimePeriod(IWebDriver driver)
        {
            IWebElement lastmonthtimeperiod = UIActions.FindElementWithXpath(driver, "(//INPUT[@name='timeperiod-selection'])[2] ");
            lastmonthtimeperiod.Click();
        }
        public static void SelectDateRanges(IWebDriver driver, string startdatevalue, string enddatevalue)
        {
            IWebElement daterangestimeperiod = UIActions.FindElementWithXpath(driver, "(//INPUT[@name='timeperiod-selection'])[5] ");
            daterangestimeperiod.Click();

            Thread.Sleep(30000);
            IWebElement startdate = UIActions.FindElementWithXpath(driver, " //*[@id='start_date_dr']");
            startdate.SendKeys(startdatevalue);
            Thread.Sleep(30000);
            IWebElement enddate = UIActions.FindElementWithXpath(driver, " //*[@id='end_date_dr']");
            enddate.SendKeys(enddatevalue);
            Thread.Sleep(30000);
            IWebElement daterangestimeperiod1 = UIActions.FindElementWithXpath(driver, "(//INPUT[@name='timeperiod-selection'])[5] ");
            daterangestimeperiod1.Click();




        }
        public static void Expandleftsidedemo(IWebDriver driver, string demonamevalue)
        {
            try
            {

                string xpath1 = null;
                string xpath2 = null;
                string xpath3 = null;
                string xpathgenerated = null;
                xpath1 = "//*[@id='settings-heading']//strong[contains(.,'";
                xpath2 = demonamevalue;
                xpath3 = "')] ";
                xpathgenerated = xpath1 + xpath2 + xpath3;
                //xpath of Left side Demo Header
                //*[@id='settings-heading']//strong[contains(.,'How old are you?')]
                IWebElement demoname = UIActions.FindElementWithXpath(driver, xpathgenerated);
                demoname.Click();
                Thread.Sleep(30000);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void typeandSearchleftsidedemo(IWebDriver driver, string demoname, string demovalue)
        {
            //xpath of search ( type) Left side Demo Header
            //input[@class='form-control demo-value-search'][@name='city']
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpath4 = null;
            string xpathgenerated = null;
            string xpathgenerated1 = null;
            xpath1 = "//input[@class='form-control demo-value-search'][@name='";
            xpath2 = demoname;
            xpath3 = "'] ";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            //type left side demo
            IWebElement demonameselect = UIActions.FindElementWithXpath(driver, xpathgenerated);
            demonameselect.Clear();
            demonameselect.SendKeys(demovalue);
            Thread.Sleep(130000);
            //xpath of search
            //input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i
            xpath4 = "/..//i";
            xpathgenerated1 = xpathgenerated + xpath4;
            //search left side demo
            IWebElement demonameselect1 = UIActions.FindElementWithXpath(driver, xpathgenerated1);
            demonameselect1.Click();
            Thread.Sleep(30000);
        }
        public static void Selectleftsidedemo(IWebDriver driver, string demoname, string demovalue)
        {
            //xpath of search ( type) Left side Demo Header
            //input[@class='form-control demo-value-search'][@name='city']
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpath4 = null;
            string xpath5 = null;
            string xpathgenerated = null;
            string xpathgenerated1 = null;
            string xpathgenerated2 = null;
            xpath1 = "//input[@class='form-control demo-value-search'][@name='";
            xpath2 = demoname;
            xpath3 = "'] ";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            //type left side demo
            IWebElement demonameselect = UIActions.FindElementWithXpath(driver, xpathgenerated);
            demonameselect.Clear();
            demonameselect.SendKeys(demovalue);
            //xpath of search
            //input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i
            xpath4 = "/..//i";
            xpathgenerated1 = xpathgenerated + xpath4;
            //search left side demo
            IWebElement demonameselect1 = UIActions.FindElementWithXpath(driver, xpathgenerated1);
            demonameselect1.Click();
            Thread.Sleep(30000);
            //xpath of click
            //input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul
            xpath5 = "/..//following-sibling::div//ul";
            xpathgenerated2 = xpathgenerated1 + xpath5;
            //click left side demo
            IWebElement demonameselect2 = UIActions.FindElementWithXpath(driver, xpathgenerated2);
            demonameselect2.Click();
        }
        public static void ExpandAdditionalFilters(IWebDriver driver)
        {
            //click on additional filters
            IWebElement aditionalfilters = UIActions.FindElementWithXpath(driver, "//*[@id='btnAddFilter']");
            Actions action2 = new Actions(driver);
            action2.MoveToElement(aditionalfilters).Click().Perform();
            aditionalfilters.Click();


            Thread.Sleep(7000);
        }
        public static void SelectAdditionalFilters(IWebDriver driver, string demonamevalue)
        {
            //type additional filters
            IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            aditionalfiltertype.Clear();
            aditionalfiltertype.SendKeys(demonamevalue);
            Thread.Sleep(12000);
            //search additional filters
            IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            aditionalfilterssearch.Click();
            Thread.Sleep(10000);
            //select additional filters
            IWebElement aditionalfiltersselect = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']");
            aditionalfiltersselect.Click();
            Thread.Sleep(10000);
        }
        public static void ExpandDemographicDropdowninDemographicReport(IWebDriver driver)
        {
            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();

            IWebElement Demographics = UIActions.FindElementWithXpath(driver, " //a[@href='#demographic-report']");
            Demographics.Click();


            Thread.Sleep(20000);

            //Click on .. Demographic Reports

            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();


            //document.querySelector('demographic-view').shadowRoot.querySelectorAll('.paperIcon')

             IJavaScriptExecutor js = driver as IJavaScriptExecutor;
             var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();



            Thread.Sleep(20000);
            //click on DemographicDropDown


            //IWebElement ClickDemographicDropDown = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]");
            //ClickDemographicDropDown.Click();


            
            var ClickDemographicDropDown = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('.btn-group'))[0] ");

            ClickDemographicDropDown.Click();





            Thread.Sleep(20000);




        }





        public static void SelectDemographicDropdowninDemographicReport(IWebDriver driver, string demovalue)
        {
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            xpath2 = demovalue;
            xpath3 = "')]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            //xpath of demo label click
            //    (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.," How long have you worked this company?")]//input
            //click on demo label

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();




            IWebElement SelectDemographicDropdown = UIActions.FindElementWithXpath(driver, xpathgenerated);
            SelectDemographicDropdown.Click();



            Thread.Sleep(30000);
        }
        public static void ExpandIdentifierdropdowninCommentReport(IWebDriver driver)
        {
            //click comments tab
            IWebElement Comments = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Comments']");
            Comments.Click();
            Thread.Sleep(80000);
            //click on IdentifierDropDown
            IWebElement ClickIdentifierDropDown = UIActions.FindElementWithXpath(driver, "//*[@id='category-hdr']//div[contains(@class,'col-sm-3')]");
            ClickIdentifierDropDown.Click();
            Thread.Sleep(50000);
        }
        public static void ClickAnalyzeSettings(IWebDriver driver)
        {
            IWebElement clickAnalyzeSettings = UIActions.FindElementWithXpath(driver, " //*[@id='btnAnalyseSettings']");
            clickAnalyzeSettings.Click();
        }
        public static void Selectsegmentfromanalysesettings(IWebDriver driver, string segmentname, string segmentvalue)
        {
            //check segment
            IWebElement chkSegment = UIActions.FindElementWithXpath(driver, " //*[@id='chkSegment']");
            chkSegment.Click();
            Thread.Sleep(30000);
            //click segement dropdown
            IWebElement segmentdropdownclick = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='ComparableItems[2].ShowSection']//*[contains(@class,'btn-group ceb-dropdown-container')])[1]");
            segmentdropdownclick.Click();
            Thread.Sleep(30000);
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[@data-bind='ComparableItems[2].ShowSection']//*[contains(@class,'btn-group ceb-dropdown-container')])[1]//li//label[contains(.,'";
            xpath2 = segmentname;
            xpath3 = "')]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            //select segement 
            IWebElement segmentdropdown = UIActions.FindElementWithXpath(driver, xpathgenerated);
            segmentdropdown.Click();
            Thread.Sleep(30000);
            //click segementvalue dropdown
            IWebElement segmentdropdownvalueclick = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='ComparableItems[2].ShowSection']//*[contains(@class,'btn-group ceb-dropdown-container')])[2]");
            segmentdropdownvalueclick.Click();
            Thread.Sleep(30000);
            string xpath4 = null;
            string xpath5 = null;
            string xpath6 = null;
            string xpathgenerated1 = null;
            xpath4 = "(//*[@data-bind='ComparableItems[2].ShowSection']//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//li//label[contains(.,'";
            xpath5 = segmentvalue;
            xpath6 = "')]//input";
            xpathgenerated1 = xpath4 + xpath5 + xpath6;
            //select segement  value
            IWebElement segmentdropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated1);
            segmentdropdownvalue.Click();
            Thread.Sleep(30000);
        }
        public static void ClickApplyinanalysesettings(IWebDriver driver)
        {
            IWebElement apply = UIActions.FindElementWithXpath(driver, " //button[@onclick='applyAnalysisSettings()']");
            apply.Click();
        }
        public static void ClickAccountDefaults(IWebDriver driver)
        {
            IWebElement acoountDefaults = UIActions.FindElementWithXpath(driver, "//*[@id='menu-account-defaults']");
            acoountDefaults.Click();
        }
        public static void ClickAnsweredatleast1question(IWebDriver driver)
        {
            IWebElement answeredatleastonequestion = UIActions.FindElementWithXpath(driver, "//*[@id='account-defaults-form']//label[contains(.,'Answered at least 1 question')]");
            Actions action = new Actions(driver);
            action.MoveToElement(answeredatleastonequestion).Click().Perform();
            answeredatleastonequestion.Click();
        }
        public static void Clickcompletedthesurvey(IWebDriver driver)
        {


            IWebElement element = UIActions.GetElementswithXpath(driver, "//*[@id='account-defaults-form']//label[contains(.,'Completed the survey')]", 55);
            Actions action = new Actions(driver);
            action.MoveToElement(element).Click().Perform();
            element.Click();

        }


        public static void SaveAnalysisdefaults(IWebDriver driver)
        {

            IWebElement element = UIActions.GetElementswithXpath(driver, "//*[@id='defaults-save']", 45);
            Actions action = new Actions(driver);
            action.MoveToElement(element).Perform();
            element.Click();

        }

        public static void SetSupprersionNAccountSetting(IWebDriver driver, string ItemN, string commentRecevied = null, string groupNDemo = null, string demoValue = null, string treshold = null)
        {
            try
            {

                IWebElement ItemMinN = driver.FindElement(By.Id("suppression-groupminnsize"));
                ItemMinN.Clear();
                ItemMinN.SendKeys(ItemN);
                Thread.Sleep(1000);
                if (commentRecevied != null)
                {
                    IWebElement CommentN = driver.FindElement(By.Id("suppression-noofcomments"));
                    CommentN.Clear();
                    CommentN.SendKeys(commentRecevied);
                }
                Thread.Sleep(1000);
                if (groupNDemo != null)
                {
                    IWebElement GroupDemo = driver.FindElement(By.Id("suppression-groupnsizedemo"));
                    GroupDemo.Clear();
                    GroupDemo.SendKeys(groupNDemo);
                }
                Thread.Sleep(1000);
                if (demoValue != null)
                {
                    IWebElement DemoLabel = driver.FindElement(By.Id("suppression-demovaluemin"));
                    DemoLabel.Clear();
                    DemoLabel.SendKeys(demoValue);
                }
                Thread.Sleep(1000);
                if (treshold != null)
                {
                    IWebElement DemoThreshold = driver.FindElement(By.Id("suppression-demothreshold"));

                    DemoThreshold.Clear();
                    DemoThreshold.SendKeys(treshold);
                }
                Thread.Sleep(1000);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

         public static string CheckDrillDownSuppression(IWebDriver driver, string QuestionText)
        {
            try
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                IWebElement Drilldowncard = (IWebElement)js.ExecuteScript("return document.querySelector('drilldown-view-item')");
                Actions action = new Actions(driver);
                action.MoveToElement(Drilldowncard).Perform();

                IList<IWebElement> AllQuestionText = (IList<IWebElement>)js.ExecuteScript("return document.querySelector('drilldown-view-item').shadowRoot.querySelectorAll('.regular-font')");
                IList<IWebElement> SuppressedSymbol = (IList<IWebElement>)js.ExecuteScript("return document.querySelector('drilldown-view-item').shadowRoot.querySelectorAll('.text-align-center')");

                for (int i = 0; i < AllQuestionText.Count; i++)

                {
                    if (QuestionText == AllQuestionText[i].Text)
                    {
                        return SuppressedSymbol[i].Text;
                    }
                }

                return null;
            }


            catch (Exception e)
            {
                string notsuppressed = "Not Suppressed";
                Console.WriteLine(e);
                return notsuppressed;
                
            }
        }


        public static string CheckDemographicSuppression(IWebDriver driver, string QuestionText)
        {
            try
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                IWebElement Demographiccard = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view')");
                Actions action = new Actions(driver);
                action.MoveToElement(Demographiccard).Click().Perform();
                Thread.Sleep(7000);
                IList<IWebElement> SuppressedQuestion = (IList<IWebElement>)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelectorAll('.category-name')");
                IList<IWebElement> SuppressedText = (IList<IWebElement>)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelectorAll('span.pull-right')");
                for (int i = 0; i < SuppressedQuestion.Count; i++)
                {
                    if (QuestionText == SuppressedQuestion[i].Text)
                    {
                        return SuppressedText[i].Text;
                    }

                }
                string notsuppressed = "Not Suppressed";
                return notsuppressed;
            }
            catch(Exception e)
            {

                Console.WriteLine(e);

                return null;

            }
        }



        public static void ClickMultipleDepartureViewSpotlightReports(IWebDriver driver)
        {


            IWebElement element = UIActions.FindElementWithXpath(driver, "//*[@id='mulReportChk'] ");

            Actions action = new Actions(driver);
            action.MoveToElement(element).Click().Perform();
            //element.Click();

        }


        public static void ClickMultipleDepartureFirstDropdown(IWebDriver driver)
        {

            IWebElement element = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top-20 padding-bottom-20')]//*[@class='btn-group ceb-dropdown-container'])[1]");


            Actions action = new Actions(driver);
            action.MoveToElement(element).Click().Perform();
            //element.Click();

        }

        public static void ClickMultipleDepartureSecondDropdown(IWebDriver driver)
        {

            IWebElement element = UIActions.GetElementswithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top-20 padding-bottom-20')]//*[@class='btn-group ceb-dropdown-container'])[2]", 90);
            Actions action = new Actions(driver);
            action.MoveToElement(element).Click().Perform();
            //element.Click();

        }
        public static void ClickMultipleDepartureFirstdDropdownValue(IWebDriver driver, string demoname)
        {
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[1]//ul//li//label[contains(.,'";
            xpath2 = demoname;
            xpath3 = "')]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            //xpath of MultipleDepartureFirstdDropdownValue
            //      (//*[contains(@class,"row panel panel-default padding-top")]//*[contains(@class,"btn-group ceb-dropdown-container")])[1]//ul//li//label[contains(.," What is your age in years?")]//input

            IWebElement MultipleDepartureFirstdDropdownValue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            MultipleDepartureFirstdDropdownValue.Click();
        }


        public static void ClickMultipleDepartureSecondDropdownValue(IWebDriver driver, string demovalue)
        {
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'";
            xpath2 = demovalue;
            xpath3 = "')]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            //xpath of MultipleDepartureSecondDropdownValue
            //      (//*[contains(@class,"row panel panel-default padding-top")]//*[contains(@class,"btn-group ceb-dropdown-container")])[2]//ul//li//label[contains(.,"25")]//input
            IWebElement MultipleDepartureSeconddDropdownValue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            MultipleDepartureSeconddDropdownValue.Click();
        }



        public static void ClickMultipleDistributionFromleftsideFilters(IWebDriver driver, string distributionname)
        {


            //     //*[@id="surveys-filter"]//ul//li//span[contains(.,"Net")]/..//following-sibling::input
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;

            xpath1 = "//*[@id='surveys-filter']//ul//li//span[contains(.,'";
            xpath2 = distributionname;
            xpath3 = "')]/..//following-sibling::input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            IWebElement MultipleDistributionFromleftsideFilters = UIActions.FindElementWithXpath(driver, xpathgenerated);
            MultipleDistributionFromleftsideFilters.Click();

        }

        public static void ClickAnalysisSettings(IWebDriver driver)
        {
            IWebElement btnSettingAnalyze = UIActions.FindElementWithXpath(driver, "//*[@id='btnAnalyseSettings']");
            btnSettingAnalyze.Click();
        }
        public static void vssegmentresultstab(IWebDriver driver)
        {
            IWebElement Vssegmentresultstab = UIActions.FindElementWithXpath(driver, "//*[@id='chkSegmentResult']");
            Vssegmentresultstab.Click();
        }
        public static void vssegmentFavorabilitytab(IWebDriver driver)
        {
            IWebElement VssegmentFavorabilitytab = UIActions.FindElementWithXpath(driver, "//*[@id='chkSegment']");
            VssegmentFavorabilitytab.Click();
        }
        public static void segmentdropdown1(IWebDriver driver)
        {

            IWebElement Segmentdropdown1 = UIActions.FindElementWithXpath(driver, "(//*[@id='select-demographic-label']/..//BUTTON[contains(@class,'multiselect dropdown-toggle form-control ceb-dropdown align-left')])[1]/..");
            Segmentdropdown1.Click();
        }
        public static void segmentdropdown2(IWebDriver driver)
        {

            IWebElement Segmentdropdown2 = UIActions.FindElementWithXpath(driver, "(//*[@id='select-demographic-label']/..//BUTTON[contains(@class,'multiselect dropdown-toggle form-control ceb-dropdown align-left')])[2]/..");
            Segmentdropdown2.Click();
        }

        public static void segmentdropdown1value(IWebDriver driver, string segmentname)
        {
            //(//*[@id="select-demographic-label"]/..//BUTTON[contains(@class, "multiselect dropdown-toggle form-control ceb-dropdown align-left")])[1]/..//ul//li//label[contains(.,"Gender")]//input
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[@id='select-demographic-label']/..//BUTTON[contains(@class,'multiselect dropdown-toggle form-control ceb-dropdown align-left')])[1]/..//ul//li//label[contains(.,'";
            xpath2 = segmentname;
            xpath3 = "')]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            IWebElement Segmentdropdown1value = UIActions.FindElementWithXpath(driver, xpathgenerated);
            Segmentdropdown1value.Click();
        }
        public static void segmentdropdown2value(IWebDriver driver, string segmentvalue)
        {
            //(//*[@id="select-demographic-label"]/..//BUTTON[contains(@class, "multiselect dropdown-toggle form-control ceb-dropdown align-left")])[2]/..//ul//li//label[contains(.,"Gender")]//input
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[@id='select-demographic-label']/..//BUTTON[contains(@class,'multiselect dropdown-toggle form-control ceb-dropdown align-left')])[2]/..//ul//li//label[contains(.,'";
            xpath2 = segmentvalue;
            xpath3 = "')]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            IWebElement Segmentdropdown1value = UIActions.FindElementWithXpath(driver, xpathgenerated);
            Segmentdropdown1value.Click();
        }
        public static void clickapplybutton(IWebDriver driver)
        {

            IWebElement apply = UIActions.FindElementWithXpath(driver, "//*[@id='analysis-settings-modal']//button[@class='btn btn-primary']");
            apply.Click();
        }
        public static void Resultstab(IWebDriver driver)
        {
            IWebElement resultstab = UIActions.FindElementWithXpath(driver, "//*[@id='analyseTabs']/li/a[text()='Results'] ");
            resultstab.Click();
        }
        public static void ApplyDemographicSuppressionToggle(IWebDriver driver, string toggle)
        {

            driver.FindElement(By.XPath("//label[@for='groupNSizeDemoActive']")).Click();

        }


       



        public static void FavorabilityTab(IWebDriver driver)
        {
            IWebElement favorabilitytab = UIActions.FindElementWithXpath(driver, " //A[contains(@href, '#favorability')] ");
            favorabilitytab.Click();
        }

        public static void PreviousStartDate(IWebDriver driver, string PreviousStartDatevalue)
        {
            IWebElement previousStartDate = UIActions.FindElementWithXpath(driver, " //*[@id='PreviousStartDate']");
            previousStartDate.SendKeys(PreviousStartDatevalue);
        }

        public static void UncheckbenchmarkfromResultTab(IWebDriver driver)
        {
            var Unselectbenchmark = driver.FindElement(By.XPath("//*[@id='chkCompBench'][@widget-data-event='change::OnResultsComparisionChange']"));
            Unselectbenchmark.Click();
        }
        public static void UncheckAllfromResultTab(IWebDriver driver)
        {
            var UnselectAll = driver.FindElement(By.XPath("//*[@id='chkCompAll'][@widget-data-event='change::OnResultsComparisionChange']"));
            UnselectAll.Click();
        }

        public static void UncheckPreviousfromResultTab(IWebDriver driver)
        {
            var UnselectAll = driver.FindElement(By.XPath("//*[@id='chkCompAll'][@widget-data-event='change::OnResultsComparisionChange']"));
            UnselectAll.Click();
        }

      


        public static void ClickMenubarinResultTab(IWebDriver driver)
        {
            try
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var menubar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('card-menu').shadowRoot.querySelector('paper-icon-button').shadowRoot.querySelector('iron-icon') ");
                menubar.Click();
            }

            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }


         public static void SelectChartTypeDatatableInResultTab(IWebDriver driver)
        {
            try
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var ChartSelection = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('card-menu').shadowRoot.querySelector('.ceb-icon_icon_data_table')");
                ChartSelection.Click();
                
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

           


      


        public static void PreviousEndDate(IWebDriver driver, string PreviousEndDatevalue)
        {



            IWebElement previousEndDate = UIActions.FindElementWithXpath(driver, " //*[@id='PreviousEndDate']");
            previousEndDate.SendKeys(PreviousEndDatevalue);
        }

        public static void Segment1Dropdown(IWebDriver driver)
        {



            IWebElement segment1Dropdown = UIActions.FindElementWithXpath(driver, "(//*[@id='reports']//div[contains(@class,'btn-group ceb-dropdown-container')])[3]");
            segment1Dropdown.Click();
        }



        public static void Segment2Dropdown(IWebDriver driver)
        {



            IWebElement segment2Dropdown = UIActions.FindElementWithXpath(driver, "(//*[@id='reports']//div[contains(@class,'btn-group ceb-dropdown-container')])[4]");
            segment2Dropdown.Click();
        }



        public static void Segment1DropdownValue(IWebDriver driver, string segmentvalue)
        {

            //   (//*[@id="reports"]//ul/li/a/label[text()=" What is your department ?"])[2]//input


            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[@id='reports']//ul/li/a/label[text()=' ";
            xpath2 = segmentvalue;
            xpath3 = "'])[2]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            IWebElement Segmentdropdown1value = UIActions.FindElementWithXpath(driver, xpathgenerated);
            Segmentdropdown1value.Click();


        }





        public static void Segment2DropdownValue(IWebDriver driver, string segmentvalue)
        {

            //   (//*[@id="reports"]//ul/li/a/label[text()=" What is your department ?"])[3]//input


            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[@id='reports']//ul/li/a/label[text()=' ";
            xpath2 = segmentvalue;
            xpath3 = "'])[3]//input";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            IWebElement Segmentdropdown1value = UIActions.FindElementWithXpath(driver, xpathgenerated);
            Segmentdropdown1value.Click();


        }



        public static void btnSelectBenchMark(IWebDriver driver)
        {



            IWebElement BtnSelectBenchMark = UIActions.FindElementWithXpath(driver, "//*[@id='btnSelectBenchMark']");

            BtnSelectBenchMark.Click();
        }



        public static void VsBenchmarkCheckbox(IWebDriver driver)
        {


            IWebElement vsBenchmarkCheckbox = UIActions.FindElementWithXpath(driver, "//*[@id='chkBenchmark']");

            vsBenchmarkCheckbox.Click();
        }



        public static void RunReport(IWebDriver driver)
        {




            IWebElement runReport = UIActions.FindElementWithXpath(driver, "//*[@id='exportmenu']/div/button/i");

            runReport.Click();
            Thread.Sleep(10000);


            IWebElement PPTPresentation = UIActions.FindElementWithXpath(driver, "//*[@id='expradio-1']");

            PPTPresentation.Click();

            Thread.Sleep(10000);


            IWebElement Exportbutton = UIActions.FindElementWithXpath(driver, "//*[@id='exportBtn']");

            Exportbutton.Click();


            Thread.Sleep(10000);


            //IWebElement runReport = UIActions.FindElementWithXpath(driver, "//*[@id='btnRunReport'][text()='Run Report']");

            //runReport.Click();





        }

        public static bool CheckForSuppressionInResults(IWebDriver driver)
        {
            try
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var SuppressedText = js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed').innerText").ToString();
                if (SuppressedText != "Not enough responses")
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool CheckForSuppressionInIsight(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement strwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-view').shadowRoot.getElementById('strength-data')");
            string[] strvalues = strwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            IWebElement oppwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-opportunity').shadowRoot.getElementById('opp-bind-row')");
            string[] oppvalues = oppwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable strdt = new DataTable();
            strdt.Columns.Add("Col1");
            strdt.Columns.Add("Col2");
            strdt.Columns.Add("Col3");
            DataTable oppdt = new DataTable();
            oppdt.Columns.Add("Col1");
            oppdt.Columns.Add("Col2");
            oppdt.Columns.Add("Col3");
            List<string> strmyList = new List<string>();
            List<string> oppmyList = new List<string>();
            bool strflag = true;
            bool oppflag = true;
            for (int i = 0; i < strvalues.Length; i++)
            {
                //if ((Regex.Match(strvalues[i-2], @"(^[0-9\--]*$)").Success) && (strvalues[i - 2] != "Segments") 
                //    && (strvalues[i - 2] != "Most potential based on ratings & size") && (strvalues[i - 2].Length < 2 ))
                if ((Regex.Match(strvalues[i], @"(?=[^A-Za-z]*[A-Za-z])\w+").Success) && (strvalues[i].Length > 3))
                {
                    //continue;
                    DataRow dr = strdt.NewRow();
                    dr[0] = strvalues[i];
                    strdt.Rows.Add(dr);
                }
                //else
                //{
                //    DataRow dr = strdt.NewRow();
                //    dr[0] = strvalues[i-2];
                //    dr[1]= strvalues[i-1];
                //    dr[2] = strvalues[i];
                //    strdt.Rows.Add(dr);
                //}
            }
            for (int i = 0; i < oppvalues.Length; i++)
            {
                if ((Regex.Match(oppvalues[i], @"(?=[^A-Za-z]*[A-Za-z])\w+").Success) && (oppvalues[i] != "Segments")
                    && (oppvalues[i] != "Most potential based on ratings & size") && (oppvalues[i].Length > 3))
                {
                    DataRow dr = oppdt.NewRow();
                    dr[0] = oppvalues[i];
                    oppdt.Rows.Add(dr);
                }
            }
            foreach (DataRow drow in strdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()))
                {
                    strflag = false;
                    break;
                }
            }
            foreach (DataRow drow in oppdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()))
                {
                    oppflag = false;
                    break;
                }
            }
            return strflag && oppflag;
        }

        public static bool CheckForSuppressionInDrillDown(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement ddwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('drilldown-view-item').shadowRoot.getElementById('drilldownContainer')");
            string[] values = ddwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable dt = new DataTable();
            dt.Columns.Add("Col1");
            dt.Columns.Add("Col2");
            List<string> myList = new List<string>();
            bool flag = true;

            for (int i = 1; i < values.Length; i++)
            {
                //if(Regex.Match(values[i-1], @"(\S.+?[.!?])(?=\s+|$)").Success)
                if (Regex.Match(values[i - 1], @"(^[0-9\--]*$)").Success)
                {
                    continue;
                }
                else
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = values[i - 1];
                    dr[1] = values[i];
                    dt.Rows.Add(dr);
                }
            }
            foreach (DataRow drow in dt.Rows)
            {
                if (drow[1].ToString() == "--")
                {
                    myList.Add(drow[0].ToString());
                }
            }
            //foreach(string question in myList)
            //{
            //    if(!questionTexts.Contains(question))
            //    {
            //        flag = false;
            //        break;
            //    }
            //}
            foreach (string question in questionTexts)
            {
                if (!myList.Contains(question))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        public static bool CheckForSuppressionInDemographic(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement demwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.getElementById('demoContainer')");
            string[] values = demwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable dt = new DataTable();
            dt.Columns.Add("Col1");
            dt.Columns.Add("Col2");
            List<string> myList = new List<string>();
            bool flag = true;

            for (int i = 1; i < values.Length; i++)
            {
                if (Regex.Match(values[i - 1], @"(^[0-9\--]*$)").Success)
                {
                    continue;
                }
                else
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = values[i - 1];
                    dr[1] = values[i];
                    dt.Rows.Add(dr);
                }
            }
            foreach (DataRow drow in dt.Rows)
            {
                if (drow[1].ToString() == "--")
                {
                    myList.Add(drow[0].ToString());
                }
            }
            foreach (string question in questionTexts)
            {
                if (!myList.Contains(question))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        public static bool CheckForSuppressionInResultsComparisonRating(IWebDriver driver)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement SuppressedIcon = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.getElementById('svg').querySelector('.suppress-icon-back-1')");
            if (SuppressedIcon == null)
                return false;
            else
                return true;
        }
        public static bool CheckForSuppressionInResultsComparisonNonRating(IWebDriver driver)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement SuppressedIcon = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('multiple-choice-chart').shadowRoot.getElementById('svg').querySelector('.suppress-icon-back-1')");
            if (SuppressedIcon == null)
                return false;
            else
                return true;
        }

        public static bool CheckForSuppressionInIsightComparison(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement strwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-view').shadowRoot.getElementById('strength-data')");
            string[] strvalues = strwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            IWebElement oppwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-opportunity').shadowRoot.getElementById('opp-bind-row')");
            string[] oppvalues = oppwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable strdt = new DataTable();
            strdt.Columns.Add("Col1");
            strdt.Columns.Add("Col2");
            //strdt.Columns.Add("Col3");
            DataTable oppdt = new DataTable();
            oppdt.Columns.Add("Col1");
            oppdt.Columns.Add("Col2");
            //oppdt.Columns.Add("Col3");
            List<string> strmyList = new List<string>();
            List<string> oppmyList = new List<string>();
            bool strflag = true;
            bool oppflag = true;
            //for (int i = 4; i < strvalues.Length; i++)
            for (int i = 0; i < strvalues.Length; i++)
            {
                //if ((Regex.Match(strvalues[i], @"(?=[^A-Za-z]*[A-Za-z])\w+").Success) && (strvalues[i].Length > 3))
                if (questionTexts.Contains(strvalues[i]))
                {
                    DataRow dr = strdt.NewRow();
                    dr[0] = strvalues[i];
                    i = i + 10;
                    dr[1] = strvalues[i];
                    strdt.Rows.Add(dr);
                }
            }
            //for (int i = 4; i < oppvalues.Length; i++)
            //{
            //    if ((Regex.Match(oppvalues[i], @"(^[a-zA-Z][a-zA-Z0-9]*)").Success) && (oppvalues[i] != "Segments")
            //        && (oppvalues[i] != "Most potential based on ratings & size") && (oppvalues[i].Length > 3))
            //    {
            //        DataRow dr = oppdt.NewRow();
            //        dr[0] = oppvalues[i];
            //        i = i + 10;
            //        dr[1] = oppvalues[i];
            //        oppdt.Rows.Add(dr);
            //    }
            //}
            for (int i = 0; i < oppvalues.Length; i++)
            {
                if (questionTexts.Contains(oppvalues[i]))
                {
                    DataRow dr = oppdt.NewRow();
                    dr[0] = oppvalues[i];
                    dr[1] = oppvalues[i + 8];
                    oppdt.Rows.Add(dr);
                }
            }
            foreach (DataRow drow in strdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "--")
                {
                    strflag = false;
                    break;
                }
            }
            foreach (DataRow drow in oppdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "--")
                {
                    oppflag = false;
                    break;
                }
            }
            return strflag && oppflag;
        }
        public static bool CheckForSuppressionInIsightComparisonAvg(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement strwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-view').shadowRoot.getElementById('strength-data')");
            string[] strvalues = strwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            IWebElement oppwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-opportunity').shadowRoot.getElementById('opp-bind-row')");
            string[] oppvalues = oppwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable strdt = new DataTable();
            strdt.Columns.Add("Col1");
            strdt.Columns.Add("Col2");
            DataTable oppdt = new DataTable();
            oppdt.Columns.Add("Col1");
            oppdt.Columns.Add("Col2");
            List<string> strmyList = new List<string>();
            List<string> oppmyList = new List<string>();
            bool strflag = true;
            bool oppflag = true;
            for (int i = 0; i < strvalues.Length; i++)
            {
                if (questionTexts.Contains(strvalues[i]))
                {
                    DataRow dr = strdt.NewRow();
                    dr[0] = strvalues[i];
                    i = i + 5;
                    dr[1] = strvalues[i];
                    strdt.Rows.Add(dr);
                }
            }
            for (int i = 0; i < oppvalues.Length; i++)
            {
                if (questionTexts.Contains(oppvalues[i]))
                {
                    DataRow dr = oppdt.NewRow();
                    dr[0] = oppvalues[i];
                    dr[1] = oppvalues[i + 5];
                    oppdt.Rows.Add(dr);
                }
            }
            foreach (DataRow drow in strdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "--")
                {
                    strflag = false;
                    break;
                }
            }
            foreach (DataRow drow in oppdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "--")
                {
                    oppflag = false;
                    break;
                }
            }
            return strflag && oppflag;
        }
        public static bool CheckForSuppressionInDrillDownComparison(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement ddwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('drilldown-view-item').shadowRoot.getElementById('drilldownContainer')");
            string[] values = ddwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable dt = new DataTable();
            dt.Columns.Add("Col1");
            dt.Columns.Add("Col2");
            List<string> myList = new List<string>();
            bool flag = true;
            int bookmarkindex = 0;

            DataRow dr = dt.NewRow();
            for (int j = 0; j < values.Length; j++)
            {
                if (questionTexts.Contains(values[j]))
                {
                    dr[0] = values[j];
                    bookmarkindex = j;
                    break;
                }
            }
            for (int i = bookmarkindex + 1; i < values.Length; i++)
            {
                if (questionTexts.Contains(values[i]))
                {
                    dr[1] = values[i - 1];
                    dt.Rows.Add(dr);
                    dr = dt.NewRow();
                    dr[0] = values[i];
                }
            }
            dr[1] = values[values.Length - 1];
            dt.Rows.Add(dr);
            foreach (DataRow drow in dt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "--")
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        public static bool CheckForSuppressionInResultsBenchmarkRating(IWebDriver driver)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement SuppressedIcon = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.getElementById('svg').querySelector('g.bar.comparison-1-fav')");
            if (SuppressedIcon == null)
                return false;
            else
                return true;
        }
        public static bool CheckForSuppressionInResultsBenchmarkNonRating(IWebDriver driver)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement SuppressedIcon_bar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('multiple-choice-chart').shadowRoot.getElementById('svg').querySelector('rect.bar.comparison1-bar')");
            IWebElement SuppressedIcon_donut = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('multiple-choice-chart').shadowRoot.getElementById('svg').querySelector('path.comp-path-1.comparison1-donut-4')");
            if ((SuppressedIcon_bar == null) && (SuppressedIcon_donut == null))
                return true;
            else
                return false;
        }
        public static bool CheckForSuppressionInIsightBenchmark(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement strwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-view').shadowRoot.getElementById('strength-data')");
            string[] strvalues = strwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            IWebElement oppwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-opportunity').shadowRoot.getElementById('opp-bind-row')");
            string[] oppvalues = oppwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable strdt = new DataTable();
            strdt.Columns.Add("Col1");
            strdt.Columns.Add("Col2");
            DataTable oppdt = new DataTable();
            oppdt.Columns.Add("Col1");
            oppdt.Columns.Add("Col2");
            List<string> strmyList = new List<string>();
            List<string> oppmyList = new List<string>();
            bool strflag = true;
            bool oppflag = true;
            int outerindex = 0;
            for (int i = 0; i < strvalues.Length; i++)
            {
                if (questionTexts.Contains(strvalues[i]))
                {
                    DataRow dr = strdt.NewRow();
                    dr[0] = strvalues[i];
                    i = i + 10;
                    if (i < strvalues.Length)
                    {
                        if (questionTexts.Contains(strvalues[i]))
                        {
                            dr[1] = "";
                        }
                    }
                    else
                        dr[1] = "";
                    strdt.Rows.Add(dr);
                }
            }
            for (int j = outerindex; j < oppvalues.Length; j++)
            {
                DataRow dr = oppdt.NewRow();
                if (questionTexts.Contains(oppvalues[j]))
                {
                    dr[0] = oppvalues[j];
                    for (int i = j + 1; i < oppvalues.Length; i++)
                    {
                        if ((Regex.Match(oppvalues[i], @"(?=[^A-Za-z]*[A-Za-z])\w+").Success) && (oppvalues[i].Length > 3))
                        {
                            dr[1] = oppvalues[i - 1];
                            oppdt.Rows.Add(dr);
                            outerindex = i;
                            break;
                        }
                        else if (i == oppvalues.Length - 1)
                        {
                            dr[1] = oppvalues[i];
                            oppdt.Rows.Add(dr);
                            outerindex = i;
                            break;
                        }
                    }
                }
            }
            foreach (DataRow drow in strdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "")
                {
                    strflag = false;
                    break;
                }
            }
            foreach (DataRow drow in oppdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "")
                {
                    oppflag = false;
                    break;
                }
            }
            return strflag && oppflag;
        }
        public static bool CheckForSuppressionInIsightBenchmarkAvg(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement strwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-view').shadowRoot.getElementById('strength-data')");
            string[] strvalues = strwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            IWebElement oppwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-opportunity').shadowRoot.getElementById('opp-bind-row')");
            string[] oppvalues = oppwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable strdt = new DataTable();
            strdt.Columns.Add("Col1");
            strdt.Columns.Add("Col2");
            DataTable oppdt = new DataTable();
            oppdt.Columns.Add("Col1");
            oppdt.Columns.Add("Col2");
            List<string> strmyList = new List<string>();
            List<string> oppmyList = new List<string>();
            bool strflag = true;
            bool oppflag = true;
            int outerindexstr = 0;
            int outerindexopp = 0;
            for (int j = outerindexstr; j < strvalues.Length; j++)
            {
                DataRow dr = strdt.NewRow();
                if (questionTexts.Contains(strvalues[j]))
                {
                    dr[0] = strvalues[j];
                    for (int i = j + 1; i < strvalues.Length; i++)
                    {
                        if ((Regex.Match(strvalues[i], @"(?=[^A-Za-z]*[A-Za-z])\w+").Success) && (strvalues[i].Length > 3))
                        {
                            dr[1] = strvalues[i - 1];
                            strdt.Rows.Add(dr);
                            outerindexstr = i;
                            break;
                        }
                        else if (i == strvalues.Length - 1)
                        {
                            dr[1] = "";
                            strdt.Rows.Add(dr);
                            outerindexstr = i;
                            break;
                        }
                    }
                }
            }
            for (int j = outerindexopp; j < oppvalues.Length; j++)
            {
                DataRow dr = oppdt.NewRow();
                if (questionTexts.Contains(oppvalues[j]))
                {
                    dr[0] = oppvalues[j];
                    for (int i = j + 1; i < oppvalues.Length; i++)
                    {
                        if ((Regex.Match(oppvalues[i], @"(?=[^A-Za-z]*[A-Za-z])\w+").Success) && (oppvalues[i].Length > 3))
                        {
                            dr[1] = oppvalues[i - 1];
                            oppdt.Rows.Add(dr);
                            outerindexopp = i;
                            break;
                        }
                        else if (i == oppvalues.Length - 1)
                        {
                            dr[1] = oppvalues[i];
                            oppdt.Rows.Add(dr);
                            outerindexopp = i;
                            break;
                        }
                    }
                }
            }
            foreach (DataRow drow in strdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "")
                {
                    strflag = false;
                    break;
                }
            }
            foreach (DataRow drow in oppdt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "")
                {
                    oppflag = false;
                    break;
                }
            }
            return strflag && oppflag;
        }
        public static bool CheckForSuppressionInDrillDownBenchmark(IWebDriver driver, string[] questionTexts)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement ddwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('drilldown-view-item').shadowRoot.getElementById('drilldownContainer')");
            string[] values = ddwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable dt = new DataTable();
            dt.Columns.Add("Col1");
            dt.Columns.Add("Col2");
            List<string> myList = new List<string>();
            bool flag = true;
            int outerindex = 0;

            for (int j = outerindex; j < values.Length; j++)
            {
                DataRow dr = dt.NewRow();
                if (questionTexts.Contains(values[j]))
                {
                    dr[0] = values[j];
                    for (int i = j + 1; i < values.Length; i++)
                    {
                        if ((Regex.Match(values[i], @"(?=[^A-Za-z]*[A-Za-z])\w+").Success) && (values[i].Length > 3))
                        {
                            dr[1] = values[i - 1];
                            dt.Rows.Add(dr);
                            outerindex = i;
                            break;
                        }
                        else if (i == values.Length - 1)
                        {
                            dr[1] = values[i];
                            dt.Rows.Add(dr);
                            outerindex = i;
                            break;
                        }
                    }
                }
            }
            foreach (DataRow drow in dt.Rows)
            {
                if (questionTexts.Contains(drow[0].ToString()) && drow[1].ToString() != "")
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
    }
}
