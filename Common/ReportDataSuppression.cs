﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Common
{
    public class ReportDataSuppression
    {
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";
        public static DataSet CategoryItemCount;
        public static DataSet ItemCountForTrendGroups;



        public static void Log(string logMessage, TextWriter w)
        {
            //w.Write("\r\nLog Entry : ");
            w.Write("\r\n ");
            w.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.Write("\t:\t");
            w.Write("{0}", logMessage);
            //w.WriteLine("-------------------------------");
        }
        public static void UpdateGroupMinimumSize(string NSize, string SurveyFormID, DBConnectionStringDTO DBConnectionParameters, string NoofCommentrecevied = "1")
        {
            string TCESInputQuery = "UPDATE AuthoringSetting SET GroupMinimumSize = " + NSize + ",NoOfCommentsReceived="+ NoofCommentrecevied+" WHERE SurveyFormID = " + SurveyFormID;
            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET GroupMinimumSize = " + NSize + ", NoOfCommentsReceived = "+ NoofCommentrecevied+" WHERE SurveyFormID = " + SurveyFormID;
            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);
            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }



        public static void UpdateGroupMinimumSizenew(string NSize, string ClientID, string PublishSurveyFormID, string DraftSurveyFormID, DBConnectionStringDTO DBConnectionParameters)
        { 
            
            //string TCESInputQuery = "UPDATE AuthoringSetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId = " + ClientID;



            string TCESInputQuery = "UPDATE SurveyFormSuppressionSetting SET GroupMinimumSize = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + PublishSurveyFormID;


            string TCESInputQuery1 = "UPDATE[Draft].[SurveyFormSuppressionSettingDraft] SET GroupMinimumSize = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + DraftSurveyFormID;


            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId = " + ClientID;

            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);

            DataSet t_ds1 = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery1, DBConnectionParameters);

            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }




        public static void UpdateNoOfCommentsReceived(string NSize, string ClientID, string PublishSurveyFormID, string DraftSurveyFormID, DBConnectionStringDTO DBConnectionParameters)
        {

            //string TCESInputQuery = "UPDATE AuthoringSetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId = " + ClientID;



            string TCESInputQuery = "UPDATE SurveyFormSuppressionSetting SET NoOfCommentsReceived = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + PublishSurveyFormID;


            string TCESInputQuery1 = "UPDATE[Draft].[SurveyFormSuppressionSettingDraft] SET NoOfCommentsReceived = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + DraftSurveyFormID;


            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET NoOfCommentsReceived = " + NSize + " WHERE ClientId = " + ClientID;

            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);

            DataSet t_ds1 = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery1, DBConnectionParameters);

            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }





        public static void UpdateGroupNSizeToReportDemonew(string NSize, string ClientID, string PublishSurveyFormID, string DraftSurveyFormID, DBConnectionStringDTO DBConnectionParameters)
        {
            //string TCESInputQuery = "UPDATE AuthoringSetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId = " + ClientID;



            string TCESInputQuery = "UPDATE SurveyFormSuppressionSetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + PublishSurveyFormID;


            string TCESInputQuery1 = "UPDATE[Draft].[SurveyFormSuppressionSettingDraft] SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + DraftSurveyFormID;


            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId = " + ClientID;

            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);

            DataSet t_ds1 = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery1, DBConnectionParameters);

            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }



        public static void UpdateDemoValueMinimumnew(string NSize, string ClientID, string PublishSurveyFormID, string DraftSurveyFormID, DBConnectionStringDTO DBConnectionParameters)
        {
            //string TCESInputQuery = "UPDATE AuthoringSetting SET DemoValueMinimum = " + NSize + " WHERE ClientId = " + ClientID;
            //Thread.Sleep(10000);

            string TCESInputQuery = "UPDATE SurveyFormSuppressionSetting SET DemoValueMinimum = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + PublishSurveyFormID;


            string TCESInputQuery1 = "UPDATE[Draft].[SurveyFormSuppressionSettingDraft] SET DemoValueMinimum = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + DraftSurveyFormID;


            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET DemoValueMinimum = " + NSize + " WHERE ClientId = " + ClientID;



            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);

            DataSet t_ds1 = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery1, DBConnectionParameters);

            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }




        public static void UpdateDemoThresholdToPreventResultInference(String NSize, string ClientID, string PublishSurveyFormID, string DraftSurveyFormID, DBConnectionStringDTO DBConnectionParameters)
        {
            //string TCESInputQuery = "UPDATE AuthoringSetting SET DemoValueMinimum = " + NSize + " WHERE ClientId = " + ClientID;
            //Thread.Sleep(10000);

            string TCESInputQuery = "UPDATE SurveyFormSuppressionSetting SET DemoThresholdToPreventResultInference = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + PublishSurveyFormID;


            string TCESInputQuery1 = "UPDATE[Draft].[SurveyFormSuppressionSettingDraft] SET DemoThresholdToPreventResultInference = " + NSize + " WHERE ClientId =" + ClientID + " and SurveyFormID =" + DraftSurveyFormID;


            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET DemoThresholdToPreventResultInference = " + NSize + " WHERE ClientId = " + ClientID;



            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);

            DataSet t_ds1 = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery1, DBConnectionParameters);

            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }



        public static void UpdateGroupNSizeToReportDemo(string NSize, string ClientID, DBConnectionStringDTO DBConnectionParameters)
        {
            string TCESInputQuery = "UPDATE AuthoringSetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId = " + ClientID;


            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET GroupNSizeToReportDemo = " + NSize + " WHERE ClientId = " + ClientID;
     
            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);
            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }







        public static void UpdateDemoValueMinimum(string NSize, string ClientID, DBConnectionStringDTO DBConnectionParameters)
        {
            string TCESInputQuery = "UPDATE AuthoringSetting SET DemoValueMinimum = " + NSize + " WHERE ClientId = " + ClientID;
 
            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET DemoValueMinimum = " + NSize + " WHERE ClientId = " + ClientID;
     
            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);
            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }




        public static void GetCategoryItemCount(string SurveyFormID, DBConnectionStringDTO DBConnectionParameters)
        {
            string InputQuery = "SELECT SurveyCategoryId, COUNT(DISTINCT SurveyFormItemID) [ItemCount] FROM Dimitem_V11 WHERE SurveyFormID = " + SurveyFormID + " AND GroupName IS NOT NULL GROUP BY SurveyCategoryId";
            CategoryItemCount = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
        }
        public static void GetItemCountForTrendGroups(string SurveyFormID, string Show, string ItemOrCategoryIDs, DBConnectionStringDTO DBConnectionParameters)
        {
            try
            {
                string ItemOrCategory;
                if (Show.ToLower() == "category")
                {
                    ItemOrCategory = "SurveyCategoryID IN (" + ItemOrCategoryIDs + ")";

                }
                else if (Show.ToLower() == "item")
                {
                    ItemOrCategory = "SurveyFormItemID IN (" + ItemOrCategoryIDs + ")";

                }
                else
                {
                    ItemOrCategory = "1 = 1";

                }
                string InputQuery = "SELECT COUNT(DISTINCT SurveyFormItemID) [ItemCount] FROM Dimitem_V11 WHERE SurveyFormID = " + SurveyFormID + "  AND  " + ItemOrCategory + "  AND GroupName = 'RatingScale'";
                ItemCountForTrendGroups = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
            }
        }
        public static bool CompareSuppressionForInsightReport(DataSet ActualSP, DataSet TestSP, string ResultDisplay, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                bool SuppStatus = true;
                string ColumnName = "ObjectID";
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_One_FavPerc_Diff" : "Comp_One_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_One_TotalRespCount_Diff";
                        break;
                    case 2:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Two_FavPerc_Diff" : "Comp_Two_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_Two_TotalRespCount_Diff";
                        break;
                    case 3:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Three_FavPerc_Diff" : "Comp_Three_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_Three_TotalRespCount_Diff";
                        break;
                    default:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "FavPerc" : "MeanScore";
                        ActualSPCompColumnResp = "TotalRespCount";
                        break;
                }
                if (TestSP.Tables[0].Columns.Contains("BenchmarkFavPerc") == false)
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                    string ResponseCol = (ResultDisplay.ToLower() == "category") ? "CategoryTotalResponses" : "Total Responses";
                    DT_TestSP = TestSPDv.ToTable(false, "ObjectID", ResponseCol, "IsSuppressed");
                    TestSPDv = DT_TestSP.DefaultView;
                    TestSPDv.Sort = "ObjectID ASC";
                    DT_TestSP = TestSPDv.ToTable();
                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        if (ResultDisplay.ToLower() == "category")
                        {
                            DataViewManager Dvm = CategoryItemCount.DefaultViewManager;
                            DataView Dv = Dvm.CreateDataView(CategoryItemCount.Tables[0]);
                            Dv.RowFilter = "SurveyCategoryId = " + row["ObjectID"].ToString();
                            int ItemCount = Int32.Parse(Dv.ToTable(false, "ItemCount").Rows[0][0].ToString());
                            decimal Cat_Supp = Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(row[ResponseCol])), 0) / ItemCount;
                            row["IsSuppressed"] = (Cat_Supp < NSize) ? 1 : 0;
                        }
                        else
                        {
                            row["IsSuppressed"] = (Int32.Parse(CommonActions.IfNullThenZero(row[ResponseCol])) < NSize) ? 1 : 0;
                        }
                    }
                }
                else
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    if (ResultDisplay.ToLower() == "category")
                    {
                        DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[2]);
                        DT_TestSP = TestSPDv.ToTable(false, "ObjectID", "Total Responses");
                        DT_TestSP.Columns.Add("IsSuppressed");
                        TestSPDv = DT_TestSP.DefaultView;
                        TestSPDv.Sort = "ObjectID ASC";
                        DT_TestSP = TestSPDv.ToTable();
                        ColumnName = "ObjectID";
                    }
                    else
                    {
                        DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                        DT_TestSP = TestSPDv.ToTable(false, "SurveyFormItemID", "Total Responses");
                        DT_TestSP.Columns.Add("IsSuppressed");
                        TestSPDv = DT_TestSP.DefaultView;
                        TestSPDv.Sort = "SurveyFormItemID ASC";
                        DT_TestSP = TestSPDv.ToTable();
                        ColumnName = "SurveyFormItemID";
                    }
                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        if (ResultDisplay.ToLower() == "category")
                        {
                            DataViewManager Dvm = CategoryItemCount.DefaultViewManager;
                            DataView Dv = Dvm.CreateDataView(CategoryItemCount.Tables[0]);
                            Dv.RowFilter = "SurveyCategoryId = " + row["ObjectID"].ToString();
                            int ItemCount = Int32.Parse(Dv.ToTable(false, "ItemCount").Rows[0][0].ToString());
                            decimal Cat_Supp = Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(row["Total Responses"])), 0) / ItemCount;
                            row["IsSuppressed"] = (Cat_Supp < NSize) ? 1 : 0;
                        }
                        else
                        {
                            row["IsSuppressed"] = (Int32.Parse(CommonActions.IfNullThenZero(row["Total Responses"])) < NSize) ? 1 : 0;
                        }
                    }
                }

                if (GroupOrder == 0)
                {
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    DT_ActualSP = StrengthDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp);

                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    DT_ActualSP.Merge(OpportunitiesDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp));
                    DT_ActualSP.DefaultView.Sort = "objectId ASC";

                    DataView Dv = DT_TestSP.DefaultView;
                    Dv.RowFilter = "IsSuppressed = 0";
                    DataTable DT = Dv.ToTable();

                    for (int i = 0, j = 0; i < DT.Rows.Count && j < DT_ActualSP.Rows.Count; i++, j++)
                    {
                        if (DT.Rows[i]["ObjectID"].ToString() != DT_ActualSP.DefaultView.ToTable().Rows[j]["objectId"].ToString())
                            SuppStatus = false;
                    }
                }
                else
                {
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    DT_ActualSP = StrengthDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp);

                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    DT_ActualSP.Merge(OpportunitiesDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp));
                    DT_ActualSP.DefaultView.Sort = "objectId ASC";

                    DataView Dv = DT_TestSP.DefaultView;
                    Dv.RowFilter = "IsSuppressed = 0";
                    DataTable DT = Dv.ToTable();

                    foreach (DataRow test_row in DT.Rows)
                    {
                        foreach (DataRow act_row in DT_ActualSP.DefaultView.ToTable().Rows)
                        {
                            if (test_row[ColumnName].ToString() == act_row["objectId"].ToString())
                            {
                                if (act_row[ActualSPCompColumnName] == DBNull.Value)
                                    SuppStatus = false;
                            }
                        }
                    }
                }
                return SuppStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareSuppressionForDrillDownReport(DataSet ActualSP, DataSet TestSP, string ResultDisplay, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                bool SuppStatus = true;
                //string ActualSPFavColumnName;
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                string ObjectID;

                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp1DiffFavPercent" : "Comp1DiffAverageScore";
                        ActualSPCompColumnResp = "Comp_One_TotalRespCount_Diff";
                        break;
                    case 2:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp2DiffFavPercent" : "Comp2DiffAverageScore";
                        ActualSPCompColumnResp = "Comp_Two_TotalRespCount_Diff";
                        break;
                    case 3:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp3DiffFavPercent" : "Comp3DiffAverageScore";
                        ActualSPCompColumnResp = "Comp_Three_TotalRespCount_Diff";
                        break;
                    default:
                        ActualSPCompColumnName = "IsSuppressed";
                        ActualSPCompColumnResp = "";
                        break;
                }
                DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                DataView ActualSPDV = ActualSPDVM.CreateDataView(ActualSP.Tables[0]);
                if (ResultDisplay.ToLower() == "category")
                {
                    ActualSPDV.RowFilter = " CategoryText NOT IN('UnAssigned')";
                    ActualSPDV.Sort = "CategoryID Asc";
                    ObjectID = "CategoryID";
                }
                else
                {
                    ActualSPDV.Sort = "SurveyFormItemID Asc";
                    ObjectID = "SurveyFormItemID";
                }
                DT_ActualSP = ActualSPDV.ToTable();
                DataColumnCollection BN_Columns = TestSP.Tables[0].Columns;

                if (BN_Columns.Contains("BenchmarkFavPerc") == true)
                {
                    if (ResultDisplay.ToLower() == "category")
                    {
                        DataViewManager TestSPDVM_Category = TestSP.DefaultViewManager;
                        DataView Dv_Category = TestSPDVM_Category.CreateDataView(TestSP.Tables[2]);
                        Dv_Category.Sort = "ObjectID Asc";
                        DT_TestSP = Dv_Category.ToTable();
                    }
                    else
                    {
                        DataViewManager TestSPDVM_Item = TestSP.DefaultViewManager;
                        DataView Dv_Item = TestSPDVM_Item.CreateDataView(TestSP.Tables[0]);
                        Dv_Item.Sort = "SurveyformItemID Asc";
                        DT_TestSP = Dv_Item.ToTable();
                        DT_TestSP.Columns["SurveyformItemID"].ColumnName = "ObjectID";

                    }
                }
                else
                {
                    DataViewManager TestSPDVM = TestSP.DefaultViewManager;
                    DataView Dv = TestSPDVM.CreateDataView(TestSP.Tables[0]);
                    Dv.Sort = "ObjectID Asc";
                    DT_TestSP = Dv.ToTable();
                }

                if (GroupOrder == 0)
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            if (RowTbl1[ObjectID].ToString() == RowTbl2["ObjectID"].ToString())
                            {
                                int Test_IsSuppressed;
                                Test_IsSuppressed = (Int32.Parse(RowTbl2["Total Responses"].ToString()) > NSize) ? 0 : 1;
                                int Actual_IsSuppressed;
                                Actual_IsSuppressed = Convert.ToInt32(RowTbl1["IsSuppressed"]);
                                if (Actual_IsSuppressed != Test_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + ObjectID + "] = " + RowTbl1[ObjectID].ToString(), w);
                                        Log("RowTbl2[" + ObjectID + "] = " + RowTbl2["ObjectID"].ToString(), w);
                                        Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "TotalResponses" + "] = " + RowTbl2["Total Responses"].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            if (RowTbl1[ObjectID].ToString() == RowTbl2["ObjectID"].ToString())
                            {
                                int Test_IsSuppressed;
                                if (RowTbl2["Total Responses"] == DBNull.Value)
                                {
                                    Test_IsSuppressed = 1;
                                }
                                else
                                {
                                    Test_IsSuppressed = (Int32.Parse(RowTbl2["Total Responses"].ToString()) > NSize) ? 0 : 1;
                                }
                                int Actual_IsSuppressed;
                                Actual_IsSuppressed = (RowTbl1[ActualSPCompColumnName] == DBNull.Value) ? 1 : 0;
                                if (Actual_IsSuppressed != Test_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + ObjectID + "] = " + RowTbl1[ObjectID].ToString(), w);
                                        Log("RowTbl2[" + ObjectID + "] = " + RowTbl2["ObjectID"].ToString(), w);
                                        Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "TotalResponses" + "] = " + RowTbl2["Total Responses"].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                return SuppStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }


        public static bool CompareSuppressionForResultReport(DataSet ActualSP, DataSet TestSP, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {

                DataTable DT_ActualSP = null;
                DataTable DT_TestSP_RS = null;
                DataTable DT_TestSP_NR = null;
                DataTable DT_TestSP_Comments = null;
                DataTable DT_TestSP = null;
                string TestSP_ObjectID = null;


                bool SuppStatus = true;
                //string ActualSPFavColumnName;
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;

                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = "IsComp1Suppressed";
                        break;
                    case 2:
                        ActualSPCompColumnName = "IsComp2Suppressed";
                        break;
                    case 3:
                        ActualSPCompColumnName = "IsComp3Suppressed";
                        break;
                    default:
                        ActualSPCompColumnName = "IsSuppressed";
                        break;
                }
                DataViewManager ActualSPDVM_RS = ActualSP.DefaultViewManager;
                DataView ActualSPDV = ActualSPDVM_RS.CreateDataView(ActualSP.Tables[2]);
                ActualSPDV.Sort = "ItemID ASC";
                DT_ActualSP = ActualSPDV.ToTable();
                var ActualSp_ObjectID = DT_ActualSP.Columns["ItemID"].ToString();
                DataColumnCollection BN_Columns = TestSP.Tables[0].Columns;

                if (BN_Columns.Contains("BenchmarkFavPerc") == true)
                {
                    DataViewManager TestSPDVM_RS = TestSP.DefaultViewManager;
                    DataView TestSPDV_RS = TestSPDVM_RS.CreateDataView(TestSP.Tables[0]);
                    DataTable DT_TestSP_RS_Col = TestSPDV_RS.ToTable();
                    DT_TestSP_RS_Col.Columns["SurveyformitemID"].ColumnName = "ObjectID";
                    DataView dv1 = new DataView(DT_TestSP_RS_Col);
                    DT_TestSP = dv1.ToTable(false, "ObjectID", "Total Responses");

                    DataViewManager TestSPDVM_NR = TestSP.DefaultViewManager;
                    DataView TestSPDV_NR = TestSPDVM_NR.CreateDataView(TestSP.Tables[1]);
                    DataTable DT_TestSP_Col = TestSPDV_NR.ToTable();
                    DT_TestSP_Col.Columns["SurveyformitemID"].ColumnName = "ObjectID";
                    DataView dv = new DataView(DT_TestSP_Col);
                    DT_TestSP.Merge(dv.ToTable(false, "ObjectID", "Total Responses"), true, MissingSchemaAction.Ignore);
                    DT_TestSP.DefaultView.Sort = "ObjectID Asc";

                    TestSP_ObjectID = DT_TestSP.Columns["ObjectID"].ToString();
                }
                else
                {
                    DataViewManager TestSPDVM_RS = TestSP.DefaultViewManager;
                    DataView TestSPDV_RS = TestSPDVM_RS.CreateDataView(TestSP.Tables[0]);
                    DT_TestSP = TestSPDV_RS.ToTable(false, "ObjectID", "Total Responses");

                    DataViewManager TestSPDVM_NR = TestSP.DefaultViewManager;
                    DataView TestSPDV_NR = TestSPDVM_NR.CreateDataView(TestSP.Tables[3]);
                    DataTable DT_TestSP_Col = TestSPDV_NR.ToTable();
                    DT_TestSP_Col.Columns["SurveyformitemID"].ColumnName = "ObjectID";
                    DataView dv = new DataView(DT_TestSP_Col);
                    DataTable DT_NR = dv.ToTable(false, "ObjectID", "Total Responses");
                    DT_TestSP.Merge(DT_NR, false,MissingSchemaAction.Add);
                  
                    

                    DataViewManager TestSPDVM_Comments = TestSP.DefaultViewManager;
                    DataView TestSPDV_Comments = TestSPDVM_Comments.CreateDataView(TestSP.Tables[5]);
                    DataTable DT_C = TestSPDV_Comments.ToTable(false, "ObjectID", "Total Responses");
                    DT_TestSP.Merge(DT_C, false,MissingSchemaAction.Add);
                    DT_TestSP.DefaultView.Sort = "ObjectID Asc";
                  
                    TestSP_ObjectID = DT_TestSP.Columns["ObjectID"].ToString();


                }

                if (GroupOrder == 0)
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            if (RowTbl1[ActualSp_ObjectID].ToString() == RowTbl2[TestSP_ObjectID].ToString())
                            {
                                int Test_IsSuppressed;
                               
                                Test_IsSuppressed = (Int32.Parse(RowTbl2["Total Responses"].ToString()) >= NSize) ? 0 : 1;
                                int Actual_IsSuppressed;
                                Actual_IsSuppressed = Convert.ToInt32(RowTbl1["IsSuppressed"]);
                                if (Actual_IsSuppressed != Test_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + ActualSp_ObjectID + "] = " + RowTbl1[ActualSp_ObjectID].ToString(), w);
                                        Log("RowTbl2[" + TestSP_ObjectID + "] = " + RowTbl2[TestSP_ObjectID].ToString(), w);
                                        Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "Total Responses" + "] = " + RowTbl2["Total Responses"].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                }
                else
                {

                    DataView dv = new DataView(DT_ActualSP);
                    dv.RowFilter = "ItemType Not IN(6,8,9,10,11)";
                    DT_ActualSP = dv.ToTable();
                    ActualSp_ObjectID = DT_ActualSP.Columns["ItemID"].ToString();

                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            if (RowTbl1[ActualSp_ObjectID].ToString() == RowTbl2[TestSP_ObjectID].ToString())
                            {
                                int Test_IsSuppressed;
                                if (RowTbl2["Total Responses"] == DBNull.Value)
                                {
                                    Test_IsSuppressed = 1;
                                }
                                else
                                {
                                    Test_IsSuppressed = (Int32.Parse(RowTbl2["Total Responses"].ToString()) > NSize) ? 0 : 1;
                                }

                                int actual_IsSuppressed;
                                actual_IsSuppressed = Convert.ToInt32(RowTbl1[ActualSPCompColumnName]);

                                if (actual_IsSuppressed != Test_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + ActualSp_ObjectID + "] = " + RowTbl1[ActualSp_ObjectID].ToString(), w);
                                        Log("RowTbl2[" + TestSP_ObjectID + "] = " + RowTbl2[TestSP_ObjectID].ToString(), w);
                                        Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "Total Responses" + "] = " + RowTbl2["Total Responses"].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }


                return SuppStatus;

            }
            catch (Exception e)
                {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;

            }



        }

        public static bool CompareSuppressionForTrendNonRating(DataSet ActualSP, DataSet TestSP, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                bool SuppStatus = true;
                //string ActualSPFavColumnName;
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                string TestSP_GroupBy = null;
                string ActualSp_GroupBy = null;

                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = "Comp_one_Mean";
                        ActualSPCompColumnResp = "Comp_one_ItemRespondentCount";
                        break;
                    case 2:
                        ActualSPCompColumnName = "Comp_Two_mean";
                        ActualSPCompColumnResp = "Comp_Two_ItemRespondentCount";
                        break;
                    case 3:
                        ActualSPCompColumnName = "Comp_Three_Mean";
                        ActualSPCompColumnResp = "Comp_Three_ItemRespondentCount";
                        break;
                    default:
                        ActualSPCompColumnName = "GroupMinimumSizeMet";
                        ActualSPCompColumnResp = "TotalRespCount";
                        break;
                }
                DataColumnCollection BN_Columns = TestSP.Tables[0].Columns;

                if (BN_Columns.Contains("BenchmarkScaleOptionId") == true)
                  
                {
                    DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                    DataView ActualSPDV = ActualSPDVM.CreateDataView(ActualSP.Tables[0]);
                    DT_ActualSP = ActualSPDV.ToTable();
                    ActualSPCompColumnName = "Comp_Bench_Mean";
                    ActualSPCompColumnResp = "Comp_Bench_TotalRespCount";

                    DataViewManager TestSPDVM = TestSP.DefaultViewManager;
                    DataView TestSPDV = TestSPDVM.CreateDataView(TestSP.Tables[0]);
                    DT_TestSP = TestSPDV.ToTable();

                }
                else
                {
                    DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                    DataView ActualSPDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
                    ActualSPDV.Sort = "GroupBy ASC";
                    DT_ActualSP = ActualSPDV.ToTable();
                    ActualSp_GroupBy = DT_ActualSP.Columns["GroupBy"].ToString();

                    DataViewManager TestSPDVM = TestSP.DefaultViewManager;
                    DataView TestSPDV = TestSPDVM.CreateDataView(TestSP.Tables[0]);
                    TestSPDV.Sort = "GroupBy ASC";
                    DT_TestSP = TestSPDV.ToTable();
                    TestSP_GroupBy = DT_TestSP.Columns["GroupBy"].ToString();

                }


                if (GroupOrder == 0)
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            if (RowTbl1[ActualSp_GroupBy].ToString() == RowTbl2[TestSP_GroupBy].ToString())
                            {
                                int Test_IsSuppressed;
                                Test_IsSuppressed = (Int32.Parse(RowTbl2["Responses"].ToString()) >= NSize) ? 1 : 0;
                                if (Convert.ToInt32(RowTbl1["GroupMinimumSizeMet"]) != Test_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + ActualSp_GroupBy + "] = " + RowTbl1[ActualSp_GroupBy].ToString(), w);
                                        Log("RowTbl2[" + TestSP_GroupBy + "] = " + RowTbl2[TestSP_GroupBy].ToString(), w);
                                        Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                        Log("RowTbl1[" + ActualSPCompColumnResp + "] = " + RowTbl1[ActualSPCompColumnResp].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "TotalResponses" + "] = " + RowTbl2["TotalResponses"].ToString(), w);
                                    }
                                    break;
                                }
                            }

                        }
                    }
                }
                else
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {

                            if (TestSP_GroupBy == null || ActualSp_GroupBy == null)
                            {
                                int Test_IsSuppressed;
                                Test_IsSuppressed = (Int32.Parse(RowTbl2["RespondentCount"].ToString()) >= NSize) ? 0 : 1;
                                int Actual_IsSuppressed;
                                Actual_IsSuppressed = (RowTbl1[ActualSPCompColumnName] == DBNull.Value) ? 1 : 0;
                                if (Actual_IsSuppressed != Test_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {

                                        Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                        Log("RowTbl1[" + ActualSPCompColumnResp + "] = " + RowTbl1[ActualSPCompColumnResp].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "TotalResponses" + "] = " + RowTbl2["RespondentCount"].ToString(), w);
                                    }
                                    break;
                                }

                            }
                            else
                            {
                                if (RowTbl1[ActualSp_GroupBy].ToString() == RowTbl2[TestSP_GroupBy].ToString())
                                {
                                    int Test_IsSuppressed;
                                    Test_IsSuppressed = (Int32.Parse(RowTbl2["Responses"].ToString()) >= NSize) ? 0 : 1;
                                    int Actual_IsSuppressed;
                                    Actual_IsSuppressed = (RowTbl1[ActualSPCompColumnName] == DBNull.Value) ? 1 : 0;
                                    if (Actual_IsSuppressed != Test_IsSuppressed)
                                    {
                                        SuppStatus = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {

                                            Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                            Log("RowTbl1[" + ActualSPCompColumnResp + "] = " + RowTbl1[ActualSPCompColumnResp].ToString(), w);
                                            Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                            Log("RowTbl2[" + "TotalResponses" + "] = " + RowTbl2["Responses"].ToString(), w);
                                        }
                                        break;
                                    }

                                }

                            }

                        }
                    }

                }

                return SuppStatus;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;

            }
        }

        public static bool CompareSuppressionForHeapTrendNonRating(DataSet ActualSP, DataSet TestSP, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;
                string ActualSp_GroupBy = null;
                string TestSp_GroupBy = null;

                bool SuppStatus = true;
                //string ActualSPFavColumnName;
                string ActualSPCompColumnName = "Data";

                DataColumnCollection BN_Columns = TestSP.Tables[0].Columns;

                if (BN_Columns.Contains("BenchmarkScaleOptionId") == true)
                {
                    DataViewManager TestSPDVM = TestSP.DefaultViewManager;
                    DataView TestSPDV = TestSPDVM.CreateDataView(TestSP.Tables[0]);
                    DT_TestSP = TestSPDV.ToTable();


                }
                else
                {
                    DataViewManager TestSPDVM = TestSP.DefaultViewManager;
                    DataView TestSPDV = TestSPDVM.CreateDataView(TestSP.Tables[0]);
                    TestSPDV.Sort = "GroupBy Asc";
                    DT_TestSP = TestSPDV.ToTable();
                    TestSp_GroupBy = DT_TestSP.Columns["GroupBy"].ToString();

                }

                DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                DataView ActualSPDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
                ActualSPDV.RowFilter = "ObjectTypeId In(5) And GroupBy Not IN ('All')";
                ActualSPDV.Sort = "GroupBy ASC";
                DT_ActualSP = ActualSPDV.ToTable();
                ActualSp_GroupBy = DT_ActualSP.Columns["GroupBy"].ToString();

                if (GroupOrder == 0)
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            if (RowTbl1[TestSp_GroupBy].ToString() == RowTbl2[ActualSp_GroupBy].ToString())
                            {
                                int Test_IsSuppressed;
                                Test_IsSuppressed = (Int32.Parse(RowTbl2["Responses"].ToString()) >= NSize) ? 0 : 1;
                                int Actual_IsSuppressed;
                                Actual_IsSuppressed = (RowTbl1[ActualSPCompColumnName] == DBNull.Value) ? 1 : 0;
                                if (Test_IsSuppressed != Actual_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {

                                        Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "Total Responses" + "] = " + RowTbl2["Responses"].ToString(), w);
                                    }
                                    break;
                                }

                            }
                        }
                    }
                }
                else
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            if (TestSp_GroupBy == null || ActualSp_GroupBy == null)
                            {

                                int Test_IsSuppressed;
                                Test_IsSuppressed = (Int32.Parse(RowTbl2["RespondentCount"].ToString()) >= NSize) ? 0 : 1;
                                int Actual_IsSuppressed;
                                Actual_IsSuppressed = (RowTbl1["BenchmarkData"] == DBNull.Value) ? 1 : 0;
                                if (Actual_IsSuppressed != Test_IsSuppressed)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {

                                        Log("RowTbl1[" + " BenchmarkData" + "] = " + RowTbl1["BenchmarkData"].ToString(), w);
                                        Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                        Log("RowTbl2[" + "Total Responses" + "] = " + RowTbl2["RespondentCount"].ToString(), w);
                                    }
                                    break;
                                }



                            }

                        }
                    }
                }

                return SuppStatus;


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;

            }
        }

        public static bool CompareSuppressionForTrendChartRatingScaleReport(DataSet ActualSP, DataSet TestSP, string Show, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                bool SuppStatus = true;
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_One_FavPerc" : "Comp_One_Mean";
                        ActualSPCompColumnResp = "Comp_One_TotalRespCount";
                        break;
                    case 2:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Two_FavPerc" : "Comp_Two_Mean";
                        ActualSPCompColumnResp = "Comp_Two_TotalRespCount";
                        break;
                    case 3:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Three_FavPerc" : "Comp_Three_Mean";
                        ActualSPCompColumnResp = "Comp_Three_TotalRespCount";
                        break;
                    default:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "FavPerc" : "MeanScore";
                        ActualSPCompColumnResp = "TotalRespCount";
                        break;
                }
                if (TestSP.Tables[0].Columns.Contains("BenchmarkFavPerc") == false)
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                    DT_TestSP = TestSPDv.ToTable(false, "GroupBy", "TotalResponses");
                    DT_TestSP.Columns.Add("GroupMinimumSizeMet");
                    TestSPDv = DT_TestSP.DefaultView;
                    TestSPDv.Sort = "GroupBy ASC";
                    DT_TestSP = TestSPDv.ToTable();
                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        int ItemCount = Int32.Parse(ItemCountForTrendGroups.Tables[0].Rows[0]["ItemCount"].ToString());
                        decimal Grp_Supp = (row["TotalResponses"] == DBNull.Value) ? 0 : Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(row["TotalResponses"])), 0) / ItemCount;
                        row["GroupMinimumSizeMet"] = (Grp_Supp < NSize) ? "False" : "True";
                    }
                }
                else
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                    DT_TestSP = TestSPDv.ToTable(false, "BenchmarkFavPerc", "Mean", "RespondentCount");
                    DT_TestSP.Columns.Add("GroupMinimumSizeMet");
                    TestSPDv = DT_TestSP.DefaultView;
                    DT_TestSP = TestSPDv.ToTable();

                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        int ItemCount = Int32.Parse(ItemCountForTrendGroups.Tables[0].Rows[0]["ItemCount"].ToString());
                        decimal Grp_Supp = (row["RespondentCount"] == DBNull.Value) ? 0 : Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(row["RespondentCount"])), 0) / ItemCount;
                        row["GroupMinimumSizeMet"] = (Grp_Supp < NSize) ? "False" : "True";
                    }
                }

                if (GroupOrder == 0)
                {
                    DataViewManager TrendDvm = ActualSP.DefaultViewManager;
                    if (ActualSP.Tables[1].Rows.Count > 0)
                    {
                        DataView TrendDv = TrendDvm.CreateDataView(ActualSP.Tables[1]);
                        DT_ActualSP = TrendDv.ToTable(false, "GroupBy", ActualSPCompColumnName, ActualSPCompColumnResp, "GroupMinimumSizeMet");
                    }
                    DT_ActualSP.DefaultView.Sort = "GroupBy ASC";

                    foreach (DataRow testRow in DT_TestSP.Rows)
                    {
                        foreach (DataRow actualRow in DT_ActualSP.Rows)
                        {
                            if (testRow["GroupBy"].ToString() == actualRow["GroupBy"].ToString())
                            {
                                if (testRow["GroupMinimumSizeMet"].ToString() != actualRow["GroupMinimumSizeMet"].ToString())
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        ReportDataValidation.Log("\ttestRow[GroupBy]\t" + testRow["GroupBy"].ToString() + ":\tactualRow[GroupBy]\t" + actualRow["GroupBy"].ToString(), w);
                                        //ReportDataValidation.Log("\ttestRow[Responses]\t" + testRow["Responses"].ToString() + ":\tactualRow[" + ActualSPCompColumnResp + "]\t" + actualRow["ActualSPCompColumnResp"].ToString(), w);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    DataViewManager TrendBenchDvm = ActualSP.DefaultViewManager;
                    DataViewManager TrendDvm = ActualSP.DefaultViewManager;
                    DataTable TrendBenchTable = null;
                    if (ActualSP.Tables[0].Rows.Count > 0)
                    {
                        DataView TrendBenchDv = TrendBenchDvm.CreateDataView(ActualSP.Tables[0]);
                        TrendBenchTable = TrendBenchDv.ToTable(false, "BenchMarkID", "Comp_Bench_FavPerc", "Comp_Bench_Mean");
                    }
                    if (ActualSP.Tables[1].Rows.Count > 0)
                    {
                        DataView TrendDv = TrendDvm.CreateDataView(ActualSP.Tables[1]);
                        DT_ActualSP = TrendDv.ToTable(false, "GroupBy", ActualSPCompColumnName, ActualSPCompColumnResp);
                    }
                    DT_ActualSP.DefaultView.Sort = "GroupBy ASC";

                    if (TestSP.Tables[0].Columns.Contains("BenchmarkFavPerc") == false)
                    {
                        foreach (DataRow test_row in DT_TestSP.Rows)
                        {
                            foreach (DataRow act_row in DT_ActualSP.DefaultView.ToTable().Rows)
                            {
                                if (test_row["GroupBy"].ToString() == act_row["GroupBy"].ToString())
                                {
                                    if (test_row["GroupMinimumSizeMet"].ToString() == "False" && act_row[ActualSPCompColumnName] != DBNull.Value)
                                    {
                                        SuppStatus = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            ReportDataValidation.Log("\ttest_row[GroupBy]\t" + test_row["GroupBy"].ToString() + ":\tact_row[GroupBy]\t" + act_row["GroupBy"].ToString(), w);
                                            //ReportDataValidation.Log("\ttest_row[Responses]\t" + test_row["Responses"].ToString() + ":\tact_row[" + ActualSPCompColumnResp + "]\t" + act_row["ActualSPCompColumnResp"].ToString(), w);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (DataRow test_row in DT_TestSP.Rows)
                        {
                            if (test_row["GroupMinimumSizeMet"].ToString() == "False" && TrendBenchTable.Rows[0]["Comp_Bench_FavPerc"] != DBNull.Value && TrendBenchTable.Rows[0]["Comp_Bench_Mean"] != DBNull.Value)
                            {
                                SuppStatus = false;

                            }
                        }
                    }
                }
                return SuppStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareSuppressionForHeatMapRatingScaleReport(DataSet ActualSP, DataSet TestSP, string Show, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                bool SuppStatus = true;
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "BenchMarkData1" : "BenchMarkData2";
                        ActualSPCompColumnResp = "BenchMarkNSize";
                        break;
                    default:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Data1" : "Data2";
                        ActualSPCompColumnResp = "Data1";
                        break;
                }
                if (TestSP.Tables[0].Columns.Contains("BenchmarkFavPerc") == false)
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                    DT_TestSP = TestSPDv.ToTable(false, "GroupBy", "TotalResponses");
                    DT_TestSP.Columns.Add("GroupMinimumSizeMet");
                    TestSPDv = DT_TestSP.DefaultView;
                    TestSPDv.Sort = "GroupBy ASC";
                    DT_TestSP = TestSPDv.ToTable();
                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        int ItemCount = Int32.Parse(ItemCountForTrendGroups.Tables[0].Rows[0]["ItemCount"].ToString());
                        decimal Grp_Supp = (row["TotalResponses"] == DBNull.Value) ? 0 : Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(row["TotalResponses"])), 0) / ItemCount;
                        row["GroupMinimumSizeMet"] = (Grp_Supp < NSize) ? "False" : "True";
                    }
                }
                else
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                    DT_TestSP = TestSPDv.ToTable(false, "BenchmarkFavPerc", "Mean", "RespondentCount");
                    DT_TestSP.Columns.Add("GroupMinimumSizeMet");
                    TestSPDv = DT_TestSP.DefaultView;
                    DT_TestSP = TestSPDv.ToTable();

                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        int ItemCount = Int32.Parse(ItemCountForTrendGroups.Tables[0].Rows[0]["ItemCount"].ToString());
                        decimal Grp_Supp = (row["RespondentCount"] == DBNull.Value) ? 0 : Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(row["RespondentCount"])), 0) / ItemCount;
                        row["GroupMinimumSizeMet"] = (Grp_Supp < NSize) ? "False" : "True";
                    }
                }

                DataViewManager TrendDvm = ActualSP.DefaultViewManager;
                DataView TrendDv = TrendDvm.CreateDataView(ActualSP.Tables[1]);
                if (Show.ToLower() != "category")
                {
                    TrendDv.RowFilter = "ObjectTypeId IN(4) AND GroupBY NOT IN('All')";
                    TrendDv.Sort = "GroupBy ASC";
                    DT_ActualSP = TrendDv.ToTable();
                }
                else
                {
                    TrendDv.RowFilter = "ObjectTypeId IN(3) AND GroupBY NOT IN('All')";
                    TrendDv.Sort = "GroupBy ASC";
                    DT_ActualSP = TrendDv.ToTable();
                }

                if (GroupOrder == 0)
                {
                    foreach (DataRow testRow in DT_TestSP.Rows)
                    {
                        foreach (DataRow actualRow in DT_ActualSP.Rows)
                        {
                            if (testRow["GroupBy"].ToString() == actualRow["GroupBy"].ToString())
                            {
                                if (testRow["GroupMinimumSizeMet"].ToString() == "False" && actualRow[ActualSPCompColumnName] != DBNull.Value)
                                {
                                    SuppStatus = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        ReportDataValidation.Log("\ttestRow[GroupBy]\t" + testRow["GroupBy"].ToString() + "\t:\tactualRow[GroupBy]\t" + actualRow["GroupBy"].ToString(), w);
                                        //ReportDataValidation.Log("\ttestRow[Responses]\t" + testRow["Responses"].ToString() + "\t:\tactualRow[" + ActualSPCompColumnName + "]" + actualRow[ActualSPCompColumnName].ToString(), w);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    int BenchCnt = 0;
                    int ItemCount = Int32.Parse(ItemCountForTrendGroups.Tables[0].Rows[0]["ItemCount"].ToString());
                    DataTable DT = null;
                    DataViewManager DTM = ActualSP.DefaultViewManager;
                    DataView DTV = DTM.CreateDataView(ActualSP.Tables[1]);
                    DTV.RowFilter = "ObjectTypeId IN(4) AND GroupBY NOT IN('All')";
                    DTV.Sort = "GroupBy ASC";
                    DT = DTV.ToTable();
                    foreach (DataRow row in DT.Rows)
                    {
                        BenchCnt = Math.Max(Int32.Parse(BenchCnt.ToString()), Int32.Parse(row[ActualSPCompColumnResp].ToString()));
                    }
                    string GroupMinimumSizeMet = (Math.Round(BenchCnt * 1.0 / ItemCount, 0) < NSize) ? "False" : "True";
                    foreach (DataRow test_row in DT_TestSP.Rows)
                    {
                        if (test_row["GroupMinimumSizeMet"].ToString() == "False" && GroupMinimumSizeMet != "False")
                        {
                            SuppStatus = false;
                            using (StreamWriter w = File.AppendText(LogFileName))
                            {
                                ReportDataValidation.Log("\tBenchCnt\t" + BenchCnt.ToString(), w);
                                ReportDataValidation.Log("\tItemCount\t" + ItemCount.ToString(), w);
                                ReportDataValidation.Log("\tActual NSize (BenchCnt * 1.0 / ItemCount)\t" + Math.Round(BenchCnt * 1.0 / ItemCount, 0).ToString(), w);
                                ReportDataValidation.Log("\ttest_row[RespondentCount]\t" + test_row["RespondentCount"].ToString(), w);
                            }
                        }
                    }
                }
                return SuppStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
    }
}
