﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using System.Data.SqlClient;
using System.Data;
using OpenQA.Selenium.Interactions;
using System.Collections.ObjectModel;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Common
{
    public class AnalyzeAction
    {
        public Actions ac = null;
        public static void GoToSurveyAnalyzeScreen(IWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");

            navbuttons.FirstOrDefault(b => b.Text == "Analyze").Click();

        }
        public static void GoToAnalyzeReportScreen(IWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Analyze").Click();
        }
        public static void GoToHomeScreen(IWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Home").Click();

        }

        public static void OpenAnalyzeForm(IWebDriver driver, string distributionName)
        {
            UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            searchBox.Clear();
            searchBox.SendKeys(distributionName);
            UIActions.Click(driver, "#searchAnalyseResult");
            Thread.Sleep(10000);
            IWebElement distributionLink = UIActions.GetElementWithWait(driver, ".navigate-to-analyze", 40);
            if (distributionLink != null)
                UIActions.Click(driver, ".navigate-to-analyze");
            Thread.Sleep(5000);
        }



        public static void OpenAnalyzeFromAuthor(IWebDriver driver, string FormName)
        {

            IWebElement AuthorClick = UIActions.FindElementWithXpath(driver, "(//SPAN[contains(.,'Author')])[1]");
            AuthorClick.Click();
            Thread.Sleep(20000);

            IWebElement searchtype = UIActions.FindElementWithXpath(driver, "//*[@id='txtSearch']");
            searchtype.SendKeys(FormName);

            Thread.Sleep(10000);

            IWebElement searchclick = UIActions.FindElementWithXpath(driver, " //*[@id='btnSearch']");
            searchclick.Click();


            Thread.Sleep(10000);
            //IWebElement actionclick = UIActions.FindElementWithXpath(driver, "//tr//*[text()='Employee Engagement Survey']/..//following-sibling::td//i[contains(@class,'shl-icon_settings')]");
            //actionclick.Click();


            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "//tr//*[text()='";
            xpath2 = FormName;
            xpath3 = "']/..//following-sibling::td//i[contains(@class,'shl-icon_settings')]";
            xpathgenerated = xpath1 + xpath2 + xpath3;

            IWebElement actionclick = UIActions.FindElementWithXpath(driver, xpathgenerated);

            actionclick.Click();

            Thread.Sleep(10000);

            IWebElement Analyzeclick = UIActions.FindElementWithXpath(driver, "//tr//td//ul/li[contains(.,'Analyze')]/a");

            Analyzeclick.Click();

            Thread.Sleep(5000);


        }



         public static void ResultsTab(IWebDriver driver)
        {


            IWebElement ResultsTab = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'report-tab')][contains(.,'Results Summary')]");

            ResultsTab.Click();


        }


        public static void Exportas(IWebDriver driver)
        {


            IWebElement Exportas = UIActions.FindElementWithXpath(driver, "//*[@id='exportmenu']//button[contains(@class,'mdc-fab mdc-ripple-upgraded app-fab--absolute dropdown')]");

            Exportas.Click();


        }




        public static void ExportasPPTPresentation(IWebDriver driver)
        {


            IWebElement ExportasPPTPresentation = UIActions.FindElementWithXpath(driver, "//*[@id='expradio-1']");

            ExportasPPTPresentation.Click();


        }





        public static void Exportbtnnew(IWebDriver driver)
        {


            IWebElement Exportbtnnew = UIActions.FindElementWithXpath(driver, "//button[@id='exportBtn']");

            Exportbtnnew.Click();


        }



        public static void ResultsReport(IWebDriver driver)
        {


            IWebElement ResultsReport = UIActions.FindElementWithXpath(driver, " 	//a[contains(@class,'download-export-link')][contains(.,'Results Report')]");

            ResultsReport.Click();


        }







        public static void NavigateToReportsFromAnalyzeCards(IWebDriver driver, string surveyName = null)
        {
            var surveyLinks = UIActions.FindElements(driver, "#homecards .pulse-card a.card-title");
            if (!string.IsNullOrEmpty(surveyName) && surveyLinks.Any(s => s.Text == surveyName))
            {
                var surveyLink = surveyLinks.FirstOrDefault(s => s.Text == surveyName);
                surveyLink.Click();
            }
            else
            {
                var surveyLink = surveyLinks.FirstOrDefault();
                surveyLink.Click();
            }
            Thread.Sleep(20000);
        }

        public static void ClickonFiltersComparisons(IWebDriver driver)
        {


            IWebElement clickonFiltersComparisons = UIActions.FindElementWithXpath(driver, "//i[@class='ceb-icon_icon-black-menu filter-icon-grey filter-icon-sz']");

            clickonFiltersComparisons.Click();


        }

        public static void ClickAddFilter(IWebDriver driver)
        {


            IWebElement clickAddFilter = UIActions.FindElementWithXpath(driver, " //button[contains(.,'Add Filter')]");

            clickAddFilter.Click();


        }

        public static void searchsegmentcategory(IWebDriver driver, string demo)
        {


            IWebElement searchsegmentcategory = UIActions.FindElementWithXpath(driver, "//*[@id='my-text-field']");


            searchsegmentcategory.Clear();
            Thread.Sleep(5000);

            searchsegmentcategory.SendKeys(demo);


        }

        public static void searchsegement(IWebDriver driver)
        {


            IWebElement searchsegement = UIActions.FindElementWithXpath(driver, "//i/span[@class='fa fa-search']");

            searchsegement.Click();


        }



        public static void ClickChip(IWebDriver driver, string demo)
        {


            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[@class='mdc-chip mdc-chip-light']//*[text()='";
            xpath2 = demo;
            xpath3 = "'])[1]";
            xpathgenerated = xpath1 + xpath2 + xpath3;

            IWebElement ClickChip = UIActions.FindElementWithXpath(driver, xpathgenerated);

            ClickChip.Click();




        }

        public static bool FindChip(IWebDriver driver, string demo)
        {


            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpathgenerated = null;
            xpath1 = "(//*[@class='mdc-chip mdc-chip-light']//*[text()='";
            xpath2 = demo;
            xpath3 = "'])[1]";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            IWebElement ClickChip = null;
            try
            {
                 ClickChip = UIActions.FindElementWithXpath(driver, xpathgenerated);

            }
            catch (Exception ex)
            {

            }

            if (ClickChip != null)
                return true;
            else
                return false;

            




        }





        public static void Apply(IWebDriver driver)
        {


            IWebElement Apply = UIActions.FindElementWithXpath(driver, "(//*[@class='mdc-button mdc-button--dense mdc-button--raised'][contains(.,'Apply')])[2]");

            Apply.Click();




        }

        public static void ApplySave(IWebDriver driver , string data = null )
        {

            if(data == null)
            {
                IWebElement Apply = UIActions.FindElementWithXpath(driver, "(//*[@class='mdc-button mdc-button--dense mdc-button--raised'][contains(.,'Save')])[1]");

                Apply.Click();
            }
            else
            {
                IWebElement Apply = UIActions.FindElementWithXpath(driver, data);

                Apply.Click();
            }


          




        }


        public static void CustomTimePeriod(IWebDriver driver, string date)
        {


            IWebElement CustomTimePeriod = UIActions.FindElementWithXpath(driver, " //*[@id='txtCustomDateRange']");



            CustomTimePeriod.Clear();


            Thread.Sleep(10000);

            CustomTimePeriod.SendKeys(date);




        }


        public static void txtDateDemoPicker(IWebDriver driver, string date)
        {


            IWebElement txtDateDemoPicker = UIActions.FindElementWithXpath(driver, "//*[@id='txtDateDemoPicker']");



            txtDateDemoPicker.Clear();


            Thread.Sleep(10000);

            txtDateDemoPicker.SendKeys(date);




        }

        public static void PreviousStartEndDate(IWebDriver driver, string date)
        {


            IWebElement PreviousStartEndDate = UIActions.FindElementWithXpath(driver, " //*[@id='PreviousStartEndDate']");



            PreviousStartEndDate.Clear();


            Thread.Sleep(10000);

            PreviousStartEndDate.SendKeys(date);


            Thread.Sleep(10000);

            IWebElement OK = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'drp-buttons')])[last()]//*[@class='applyBtn mddtp-button'][contains(.,'OK')]");

            OK.Click();


            Thread.Sleep(10000);

        }


        //click ok


        public static void OK(IWebDriver driver)
        {


            IWebElement OK = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'daterangepicker ltr show-calendar opensright')]//*[@class='applyBtn mddtp-button'][contains(.,'OK')])[4]");

            OK.Click();




        }

        public static void SwitchToReportTab(IWebDriver driver, string tabName)
        {
            try
            {
                var tabs = driver.FindElements(By.CssSelector("#analyseTabs li"));
                var tab = tabs.FirstOrDefault(t => t.Text.ToLower() == tabName.ToLower());
                if (tab != null && tab.GetAttribute("class") != "active")
                {
                    tab.Click();
                    Thread.Sleep(15000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /* public static void OpenAnalysisSettings(IWebDriver driver)
         {
             var openButton = driver.FindElement(By.Id("btnAnalyseSettings"));
             openButton.Click();
             //Thread.Sleep(20000);
         } */
        public static void SaveUserAnalysisSettings(IWebDriver driver, string Id)
        {
            var rdoSaveSettings = driver.FindElement(By.Id(Id));
            rdoSaveSettings.Click();
        }
        public static void ExpandDemoFilterUsingDemoLabel(IWebDriver driver, string demoLabel)
        {
            var labelSelect = driver.FindElements(By.CssSelector("[data-bind='DemographicColumnName']")).
                  FirstOrDefault(e => e.Displayed && e.Text.Trim() == demoLabel);
            if (labelSelect != null)
            {
                labelSelect.Click();
            }
        }
        public static void ApplyVsComparisionResults(IWebDriver driver, string Id)
        {
            IWebElement chk = driver.FindElement(By.Id(Id));
            if (!chk.Selected)
                chk.Click();

        }
        public static bool IfComparisionApplied(IWebDriver driver, string Id)
        {
            IWebElement chk = driver.FindElement(By.Id(Id));
            return chk.Selected;
        }
        public static void ChangeBenchMarkValueInSettings(IWebDriver driver, string benchmarkName)
        {
            ApplyVsComparisionResults(driver, "chkBenchmark");
            UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark", benchmarkName);
        }
        public static void ApplyAnalyseSettings(IWebDriver driver)
        {
            IWebElement button = driver.FindElement(By.CssSelector("#analysis-settings-modal .btn-primary"));
            button.Click();
        }

        public static IWebElement GetViewAllLink(IWebDriver driver, string demoLabel)
        {
            return driver.FindElements(By.Id("ViewAllDemoValueFilter")).
                  FirstOrDefault(e => e.Displayed && e.GetAttribute("name").Trim() == demoLabel);
        }

        public static void OpenViewAllModal(IWebDriver driver, IWebElement viewAllLink)
        {
            //int elementPosition = viewAllLink.Location.Y - 200;
            //String js = "window.scrollTo(0, " + elementPosition + " )";
            //driver.ExecuteJavaScript(js);
            UIActions.MouseHoverToElementAndClick(driver, viewAllLink);
            //viewAllLink.Click();
        }

        public static void ApplyViewAllFilter(IWebDriver driver)
        {
            IWebElement applyButton = driver.FindElement(By.Id("btnApplyViewAllFilter"));
            applyButton.Click();
        }

        public static List<IWebElement> GetSelectedDemoValues(IWebDriver driver, string demoLabel)
        {
            return driver.FindElements(By.CssSelector("[labelvalue*='" + demoLabel + "_'] input:checked")).ToList();
        }

        public static void TrendActionMenuActions(IWebDriver driver, string graphType, string groupBy, string questionType, string show1, string show2 = null)
        {
            //var parentElement = UIActions.GetElement(driver, ".trend-view");
            IWebElement parentElement = null;
            var menuButton = driver.FindElement(By.CssSelector(".trend-view paper-icon-button"));
            UIActions.ScrollToElement(driver, ".trend-view paper-icon-button");
            menuButton.Click();
            var popUpElement = UIActions.GetElement(driver, ".trend-view .poly-popover-content b");
            UIActions.SelectInCEBDropdownByText(driver, "#trend-groupby-chart", graphType, 0, parentElement);
            popUpElement.Click();
            menuButton.Click();
            UIActions.SelectInCEBDropdownByText(driver, "#trend-groupby-list-card", groupBy, 0, parentElement);
            popUpElement.Click();
            menuButton.Click();
            UIActions.SelectInCEBDropdownByText(driver, "#trend-groupby-question", questionType, 0, parentElement);
            popUpElement.Click();
            menuButton.Click();
            if (questionType == "Rating Scale")
            {
                UIActions.SelectInCEBDropdownByText(driver, "#trend-show-question-option", show1, 0, parentElement);
            }
            else if (questionType == "Non-Rating Scale")
            {
                UIActions.SelectInCEBDropdownByText(driver, "#trend-show-list-card", show1, 0, parentElement);
            }
            popUpElement.Click();
            menuButton.Click();
            Thread.Sleep(1000);
            if (show1 != "Entire Survey (All Ratings)")
            {
                string[] options = show2.Split(',');
                if (!options.Contains("Select Top 5"))
                {
                    UIActions.RemoveSelectInMultiSelectCEBDropdownByText(driver, "#trend-show-answer-option", new string[] { "Select Top 5" }, 0, parentElement);
                }
                UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#trend-show-answer-option", options, 0, parentElement);
            }
            popUpElement.Click();
            menuButton.Click();
            var applyButton = driver.FindElement(By.CssSelector("#btnTrendMenuApply"));
            applyButton.Click();
        }
        public static void SearchQuestion(IWebDriver driver, string Questiontext)
        {
            try
            {
                IWebElement SearchField = driver.FindElement(By.XPath("(//*[@id='search'])[1]"));
                SearchField.Clear();
                SearchField.SendKeys(Questiontext);
                //SearchField.Click();
                Thread.Sleep(5000);
                IWebElement SearchDropDownBox = driver.FindElement(By.XPath("//*[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all']"));
                Actions action = new Actions(driver);
                action.MoveToElement(SearchDropDownBox).Click().Perform();
                Thread.Sleep(4000);
                IWebElement SearchDropDownSelect = driver.FindElement(By.XPath("//a[@class='ui-corner-all'][contains(.,'" + Questiontext + "')]"));
                action.MoveToElement(SearchDropDownSelect).Click().Perform();
                Thread.Sleep(4000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void OpenAnalysisSettings(IWebDriver driver)
        {
            try
            {
                UIActions.Click(driver, "#analysis-settings-display-dialog");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void DataCalculationSelection(IWebDriver driver, string Data)
        {
            try
            {
                if (Data.ToLower() == "percentage")
                    UIActions.Click(driver, "#radio-1");
                else
                    UIActions.Click(driver, "#radio-2");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void GroupingInAnalysisSetting(IWebDriver driver, string Grouping)
        {
            try
            {
                if (Grouping.ToLower() == "category")
                    UIActions.Click(driver, "#radio-3");
                else
                    UIActions.Click(driver, "#radio-4");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SelectResultTabComparisonGroup(IWebDriver driver, string comparisongroupName1, string ComparisonGroupName2, string previous, string Benchmark, string Demographiclabel, string DemographicValue)
        {
            try
            {
                switch (comparisongroupName1)
                {
                    case "Previous":
                        IWebElement PreviousCheckBox = driver.FindElement(By.Id("chkCompPrev"));
                        PreviousCheckBox.Click();
                        UIActions.SelectInCEBDropdownByText(driver, "#select-previous", previous);
                        break;
                    case "Benchmark":
                        IWebElement BenchMarkCheckBox = driver.FindElement(By.Id("chkCompBench"));
                        BenchMarkCheckBox.Click();
                        UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark", Benchmark);
                        break;
                    case "Segment":
                        IWebElement SegmentCheckBox = driver.FindElement(By.Id("chkSegmentResult"));
                        SegmentCheckBox.Click();
                        if (Demographiclabel != null)
                            UIActions.SelectInCEBDropdownByText(driver, "#select-demographic-label", Demographiclabel);
                        UIActions.SelectInCEBDropdownByText(driver, "#select-demographic-value", DemographicValue);
                        break;
                    case "All":
                        IWebElement All = driver.FindElement(By.Id("chkCompAll"));
                        All.Click();
                        break;
                }
                if (ComparisonGroupName2 != null)
                {
                    switch (ComparisonGroupName2)
                    {
                        case "Previous":
                            IWebElement PreviousCheckBox = driver.FindElement(By.Id("chkCompPrev"));
                            PreviousCheckBox.Click();
                            UIActions.SelectInCEBDropdownByText(driver, "#select-previous", previous);
                            break;
                        case "Benchmark":
                            IWebElement BenchMarkCheckBox = driver.FindElement(By.Id("chkCompBench"));
                            BenchMarkCheckBox.Click();
                            UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark", Benchmark);
                            break;
                        case "Segment":
                            IWebElement SegmentCheckBox = driver.FindElement(By.Id("chkSegmentResult"));
                            SegmentCheckBox.Click();
                            UIActions.SelectInCEBDropdownByText(driver, "#select-demographic-label", Demographiclabel);
                            UIActions.SelectInCEBDropdownByText(driver, "#select-demographic-value", DemographicValue);
                            break;
                        case "All":
                            IWebElement All = driver.FindElement(By.Id("chkCompAll"));
                            All.Click();
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        public static void UnSelectResultTabComparisonGroup(IWebDriver driver)
        {
            try
            {
                var ResultElements = driver.FindElements(By.CssSelector("input[widget-data-event='change::OnResultsComparisionChange']"));
                ResultElements.ToList()
                    .ForEach((element) =>
                    {
                        var DisabledElement = driver.FindElement(By.CssSelector("input[disabled='']"));
                        Thread.Sleep(1000);
                        if (DisabledElement != null)
                        {
                            if (element.GetAttribute("id") != DisabledElement.GetAttribute("id"))
                            {
                                var UnCheckElement = driver.FindElement(By.Id(element.GetAttribute("id")));
                                UnCheckElement.Click();
                            }
                        }
                        else

                        {

                        }
                    });




            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }



        // This method used to select Multiple Demo values of the single demo label
        public static void SelectLeftSideDemoValues(IWebDriver driver, string DemoLabel, string Demovalues)
        {
            try
            {
                IWebElement DemoExpand = driver.FindElement(By.XPath("//*[@id='settings-heading']//strong[contains(.,'" + DemoLabel + "')]"));
                Actions action = new Actions(driver);
                action.MoveToElement(DemoExpand).Click().Perform();
                IWebElement DemoSearch = driver.FindElement(By.XPath("//input[@class='form-control demo-value-search'][@name='" + DemoLabel + "']"));
                DemoSearch.Clear();
                DemoSearch.SendKeys(Demovalues);
                Thread.Sleep(3000);
                var DemoSearchButton = driver.FindElement(By.XPath("//input[@class='form-control demo-value-search'][@name='" + DemoLabel + "']/preceding-sibling::i"));
                DemoSearchButton.Click();
                Thread.Sleep(3000);
                var Searchvalue = driver.FindElement(By.XPath("//*[@id='valuesearch-ul']//li[contains(.,'" + Demovalues + "')]"));
                Actions action2 = new Actions(driver);
                action2.MoveToElement(Searchvalue).Click().Perform();
                Searchvalue.Click();
                Thread.Sleep(10000);


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SelectLeftSideDateDemoValues(IWebDriver driver, string DemoLabel, string startdate, string enddate)
        {
            try
            {
                IWebElement DemoExpand = driver.FindElement(By.XPath("//*[@id='settings-heading']//strong[contains(.,'" + DemoLabel + "')]"));
                Actions action = new Actions(driver);
                action.MoveToElement(DemoExpand).Click().Perform();
                IWebElement StartdateElement = driver.FindElement(By.Id("StartDate_Demo1"));
                StartdateElement.SendKeys(startdate);
                IWebElement EndDateElement = driver.FindElement(By.Id("EndDate_Demo1"));
                EndDateElement.SendKeys(enddate);
                Thread.Sleep(2000);
                Actions action1 = new Actions(driver);
                action1.MoveToElement(EndDateElement).Click().Perform();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        public static void SelectMultipleLeftSideDemoValues(IWebDriver driver, string DemoLabel, string[] Demovalues)
        {
            try
            {
                IWebElement DemoExpand = driver.FindElement(By.XPath("//*[@id='settings-heading']//strong[contains(.,'" + DemoLabel + "')]"));
                Actions action = new Actions(driver);
                action.MoveToElement(DemoExpand).Click().Perform();
                IWebElement ViewAllLink = driver.FindElement(By.XPath("//a[@id='ViewAllDemoValueFilter'][@name='" + DemoLabel + "']"));
                Actions action1 = new Actions(driver);
                action1.MoveToElement(ViewAllLink).Perform();
                ViewAllLink.Click();
                Thread.Sleep(3000);
                IWebElement ApplyFilterList = driver.FindElement(By.XPath("(//button[@class='multiselect dropdown-toggle form-control ceb-dropdown align-left'])[5]"));
                ApplyFilterList.Click();
                Demovalues.ToList()
                    .ForEach((element) =>
                    {

                        IWebElement DemovaluecheckBox = driver.FindElement(By.XPath("(//a[@class='dropdown-item']/label/input[@value='" + element + "'])[2]"));
                        DemovaluecheckBox.Click();

                    });
                UIActions.clickwithID(driver, "btnApplyViewAllFilter");

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        public static string GetCurrentAnalyzeTab(IWebDriver driver)
        {
            var currentTab = UIActions.GetElement(driver, "#analyseTabs li.active a");
            return currentTab.Text;
        }
        public static void NavigatetoFavorabilitytab(IWebDriver driver)
        {
            IWebElement FavTab = driver.FindElement(By.XPath("//a[@title='Favorability']"));
            FavTab.Click();
        }

        public static void NavigateToAnalyzeTab(IWebDriver driver, string tabName)
        {
            var tabs = driver.FindElements(By.CssSelector("#analyseTabs li a"));
            var currentTab = tabName.ToLower() != "comments"
                ? tabs.FirstOrDefault(t => t.Text.ToLower().Trim() == tabName.ToLower().Trim()) : tabs.LastOrDefault();
            if (currentTab != null)
            {
                currentTab.Click();
                Thread.Sleep(5000);
            }
        }

        public static int PollExportStatus(IWebDriver driver, out IWebElement result)
        {
            bool done = false;
            int exportStatus = 0;
            int timer = 0;
            result = null;
            while (!done && timer < 300)
            {
                Thread.Sleep(30000);
                try
                {
                    var success = UIActions.GetElement(driver,
                        ".toaster-download #mdc-js-snackbar div.mdc-snackbar__text");
                    if (success != null)
                    {
                        var download =
                            UIActions.GetElement(driver, ".toaster-download #mdc-js-snackbar div.mdc-snackbar__action-wrapper button.mdc-snackbar__action-button");
                        if (download != null)
                        {
                            exportStatus = 1;
                            done = true;
                            result = success;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                try
                {
                    var fail = UIActions.GetElement(driver, ".toaster-error #mdc-js-snackbar div.mdc-snackbar__text");
                    if (fail != null)
                    {
                        exportStatus = 2;
                        done = true;
                        result = fail;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                timer += 30;
            }

            if (!done)
                exportStatus = 3;

            return exportStatus;
        }

        public static void ApplyFavTabVsPreviousComparison(IWebDriver driver, string previous)
        {
            IWebElement chk = driver.FindElement(By.CssSelector("[data-bind='ComparableItems[1].IsIncluded']"));
            if (!chk.Selected)
                chk.Click();
            UIActions.SelectInCEBDropdownByText(driver, "#select-previous", previous);
        }
        public static void ApplyFavTabVsSegmentComparison(IWebDriver driver, string demolabel, string demovalue)
        {
            ApplyVsComparisionResults(driver, "chkSegment");
            UIActions.SelectInCEBDropdownByText(driver, "#select-demographic-label", demolabel);
            UIActions.SelectInCEBDropdownByText(driver, "#select-demographic-value", demovalue);
        }
        public static void ApplyFavTabVsAllComparison(IWebDriver driver)
        {
            IWebElement chk = driver.FindElement(By.CssSelector("[data-bind='ComparableItems[0].IsIncluded']"));
            if (!chk.Selected)
                chk.Click();
        }
        public static void SetTimePeriodAnalysisSettings(IWebDriver driver, string timeperiod)
        {
            UIActions.SelectInCEBDropdownByText(driver, "#select-datedemo", timeperiod);
        }
        public static void ApplyLeftSideTimePeriod(IWebDriver driver, string timeperiod, string startdate = null, string enddate = null)
        {
            switch (timeperiod.ToLower())
            {
                case "last month":
                    UIActions.Click(driver, "#ul-daterange-filter [value='2']");
                    break;

                case "last 3 months":
                    UIActions.Click(driver, "#ul-daterange-filter [value='3']");
                    break;

                case "last 6 months":
                    UIActions.Click(driver, "#ul-daterange-filter [value='4']");
                    break;

                case "date ranges":
                    UIActions.Click(driver, "#ul-daterange-filter [value='5']");
                    var txtstartdate = UIActions.GetElement(driver, "#start_date_dr");
                    txtstartdate.Clear();
                    txtstartdate.SendKeys(startdate);
                    var txtenddate = UIActions.GetElement(driver, "#end_date_dr");
                    txtenddate.Clear();
                    txtenddate.SendKeys(enddate);
                    Thread.Sleep(1000);
                    var Enddate = UIActions.GetElement(driver, "#end-date-btn");
                    Actions action1 = new Actions(driver);
                    action1.MoveToElement(Enddate).Click().Perform();

                    break;

                default:
                    UIActions.Click(driver, "#ul-daterange-filter [value='1']");
                    break;
            }
        }
        public static void SelectLeftSideDistributions(IWebDriver driver, string[] distributions, int index = 0)
        {
            try
            {
                foreach (string distribution in distributions)
                {
                    IWebElement distributionname = null;
                    distributionname = driver.FindElements(By.CssSelector("#surveys-filter [data-bind-iterate='Surveys']")).ElementAt(index);
                    if (!string.IsNullOrEmpty(distribution))
                    {
                        var checkboxLabelSelect = distributionname.FindElements(By.CssSelector("li")).FirstOrDefault(e => e.Displayed && e.Text.Trim() == distribution.Trim());
                        if (checkboxLabelSelect != null)
                        {
                            var checkboxSelect =
                                checkboxLabelSelect.FindElement(By.CssSelector("input"));
                            if (checkboxSelect != null && !checkboxSelect.Selected)
                                checkboxSelect.Click();
                        }
                    }
                    Thread.Sleep(25000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void UnSelectFavComparisons(IWebDriver driver, string All = null, string Previous = null, string Benchmark = null, string Segment = null)
        {
            try
            {
                if (All != null)
                {
                    IWebElement chk = driver.FindElement(By.CssSelector("[data-bind='ComparableItems[0].IsIncluded']"));
                    if (chk.Selected)
                        chk.Click();
                }
                if (Previous != null)
                {
                    IWebElement chk = driver.FindElement(By.CssSelector("[data-bind='ComparableItems[1].IsIncluded']"));
                    if (chk.Selected)
                        chk.Click();
                }
                if (Benchmark != null)
                {
                    IWebElement chk = driver.FindElement(By.CssSelector("chkBenchmark"));
                    if (chk.Selected)
                        chk.Click();
                }
                if (Segment != null)
                {
                    IWebElement chk = driver.FindElement(By.CssSelector("chkSegment"));
                    if (chk.Selected)
                        chk.Click();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void NavToAnalyze(IWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            IWebElement btn = navbuttons.FirstOrDefault(b => b.Text == "Analyze");
            if (!btn.Selected && btn != null)
            {
                btn.Click();
            }
        }

        public static void ChooseDistributionToAnalyze(IWebDriver driver, string _distributionName)
        {
            Actions ac = new Actions(driver);
            IWebElement searchField = UIActions.GetElement(driver, "#searchInputAnalyseResult");
            if (searchField.Displayed && searchField.Enabled)
            {
                searchField.SendKeys(_distributionName);
            }

            UIActions.GetElement(driver, "#searchAnalyseResult").Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            wait.Until(d =>
            {
                return d.FindElement(By.XPath("//button[@class='btn btn-primary btn-xs navigate-to-analyze']")).Displayed;
            }
            );
            IReadOnlyCollection<IWebElement> analyzeBtn = UIActions.FindElementsWithXpath(driver, "//button[@class='btn btn-primary btn-xs navigate-to-analyze']");
            analyzeBtn.FirstOrDefault(d => d.Enabled).Click();
        }

        public static void NavToResults(IWebDriver driver)
        {
            UIActions.GetElement(driver, "#searchInputAnalyseResult").SendKeys("Current Distribution");
            UIActions.GetElement(driver, "#searchAnalyseResult").Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            wait.Until<IWebElement>(d =>
            {
                return d.FindElement(By.XPath("//button[@class='btn btn-primary btn-xs navigate-to-analyze']"));
            });

            if (!driver.FindElement(By.XPath("//button[@class='btn btn-primary btn-xs navigate-to-analyze']")).Selected && (driver.FindElement(By.XPath("//button[@class='btn btn-primary btn-xs navigate-to-analyze']")) != null))
            {
                driver.FindElement(By.XPath("//button[@class='btn btn-primary btn-xs navigate-to-analyze']")).Click();
            }
        }

        public static void UserAnalysisSettings(IWebDriver driver)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            IWait<IWebDriver> wait1 = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromMinutes(1));
            wait1.Until(wd => executor.ExecuteScript("return document.readyState").Equals("complete"));


            Thread.Sleep(15000);
            string path = @"//*[@id='scrollThreshold']/div[1]/div[2]/paper-card/div[1]/dom-if[1]";
            UIActions.GetElement(driver, "#btnAnalyseSettings").Click();

        }

        public static List<string> AnalysisSettingsVsBM(IWebDriver driver)
        {
            IWebElement vsBenchmark = driver.FindElement(By.XPath("//tr[@data-bind='ComparableItems[3].ShowSection']/td[6]"));
            if (vsBenchmark.Enabled)
            {
                vsBenchmark.Click();
            }
            IReadOnlyCollection<IWebElement> bmList = driver.FindElements(By.XPath("//*[@id='analysis-settings-modal']//div[@class='btn-group ceb-dropdown-container open']/ul/li"));
            List<string> ll = bmList.Select(d => d.Text).ToList();
            Console.WriteLine(ll.FirstOrDefault());
            return ll;
        }


        public static List<string> GetListOfBenchMark()

        {
            List<string> sqlResults = new List<string>();
            //string connectionString = "data Source=USA-QIN-PLSP-01,1605;Initial Catalog=TCES;User ID=pulseqa;Password=password-1";
            string connectionString = "data Source=USA-SIN-PLSP-01,1605;Initial Catalog=TCES;Integrated Security=SSPI;";
            SqlConnection sqlConnect = new SqlConnection(connectionString);
            string query = @"select distinct bm.BenchmarkId , bm.Name from item i join BenchmarkDetail bmd on bmd.ObjectId = i.ParentItemID
                                join Benchmark bm on bm.BenchmarkId = bmd.BenchmarkId
                                join surveyformitem sfi on sfi.itemid = i.itemid
                                join BenchmarkClientMapping bcm on bcm.ClientId = sfi.ClientID
                                join SurveyFormSurveyXref xref on xref.SurveyFormID = sfi.SurveyFormID
                                where bcm.clientid = 16 and xref.surveyid = 22135";
            //string query1 = "select top 1 surveyformid from surveyform order by 1 desc";
            SqlCommand command = new SqlCommand(query, sqlConnect);
            if (command.Connection.State == ConnectionState.Closed)
            {
                sqlConnect.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    int i = 0;
                    while (reader.HasRows)
                    {
                        sqlResults.Add(reader.GetValue(i).ToString());
                        Console.WriteLine(reader.GetValue(i).ToString());
                        i++;
                    }

                }
            }
            return sqlResults;
        }

        public static List<string> dbRead(string gfeSurveyFormID)
        {
            List<string> demoLabel = new List<string>();
            string connectionString = "Data Source =" + @"USA-QIN-PLSR-01\PLSP,1605" + "; Initial Catalog="
                + @"TCESReporting" + "; User ID=" + "pulseqa" + ";Password=" + "password-1";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                // SqlCommand command = new SqlCommand();
                string queryString = @"select label from DimSurveyDemographicColumnLabel where GFESurveyFormID = @GFESurveyFormID";
                SqlCommand command = new SqlCommand(queryString, conn);
                command.Connection = conn;
                command.Parameters.AddWithValue("@GFESurveyFormID", gfeSurveyFormID);
                //SqlDataReader reader = command.ExecuteReader();

                command.CommandTimeout = 3600;
                command.CommandText = @"select label from DimSurveyDemographicColumnLabel where GFESurveyFormID = @GFESurveyFormID";
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    demoLabel = (from IDataRecord r in reader
                                 select (string)r["Label"]
                    ).ToList();
                }


                reader.Close();
                conn.Close();

            }
            return demoLabel;
        }

        #region AnalyzeReDeisgn

        public static void NavigatetoDifferentTab(IWebDriver driver, string ReportName)
        {
            driver.FindElement(By.XPath("//*[@title= '" + ReportName + "']")).Click();
            Thread.Sleep(8000);
        }
        public static void ExpandAndCollapseFilters(IWebDriver driver, string Filter_ComparisonName, int ComparisonNumber = 1)
        {
            Thread.Sleep(15000);
            driver.FindElement(By.XPath("(//*[@Class='panel-title']//a//span[contains(text(),'" + Filter_ComparisonName + "')])[" + ComparisonNumber + "]")).Click();

        }
        // Comparison Number include the count from surveyfilter -1, Comparison 1-2, Comparison 2- 3
        public static void ClearFilter(IWebDriver driver, string Filter_ComparisonName, int ComparisonNumber = 1)
        {
            driver.FindElement(By.XPath("(//*[@id='accordion']//button[@class='mdc-button mdc-button--dense mdc-text-button margin-left-5'and contains(.,'Clear Filters')])[" + ComparisonNumber + "]"));
            Thread.Sleep(7000);

        }
        public static void RemoveComparison(IWebDriver driver, string Filter_ComparisonName, int ComparisonNumber = 1)
        {
            driver.FindElement(By.XPath("(//*[@id='accordion']//button[@class='mdc-button mdc-button--dense mdc-text-button margin-left-5'and contains(.,'Remove Comparison')])[" + ComparisonNumber + "]"));
            Thread.Sleep(7000);

        }
        public static void SelectTimePeriod(IWebDriver driver, string TimePeriodType, string frequency, string timerange = null)
        {
            driver.FindElement(By.XPath("//*[@id='accordion']//div[@title='All Time Period']")).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@class='mdc-chip__text' and contains(.,'"+ TimePeriodType + "')]")).Click();
            switch (frequency.ToLower())
            {
                case "last 30 days":
                    driver.FindElement(By.XPath("//*[@class='mdc-chip__text' and contains(.,'Last 30 Days')]")).Click();
                    break;
                case "last 90 days":
                    driver.FindElement(By.XPath("//*[@class='mdc-chip__text' and contains(.,'Last 90 Days')]")).Click();
                    break;
                case "last 180 days":
                    driver.FindElement(By.XPath("//*[@class='mdc-chip__text' and contains(.,'Last 180 Days')]")).Click();
                    break;
                case "custom time period":
                    driver.FindElement(By.XPath("//*[@class='mdc-chip__text' and contains(.,'Custom Time Period')]")).Click();
                    var TimeperiodField =driver.FindElement(By.Id("txtCustomDateRange"));
                    TimeperiodField.Clear();
                    TimeperiodField.SendKeys(timerange);
                    break;
                default:
                    break;

            }
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript("window.scrollBy(100,500)");
            driver.FindElement(By.XPath("(//*[@class='applyBtn mddtp-button'])[2]")).Click();
            /*IWebElement Okbutton = driver.FindElement(By.XPath("(//*[@class='applyBtn mddtp-button'])[2]"));
            Actions action = new Actions(driver);
            action.MoveToElement(Okbutton).Click().Perform(); */
            driver.FindElement(By.XPath("//*[@class='filter-apply-button']//button[@class='mdc-button mdc-button--dense mdc-button--raised']")).Click();
            Thread.Sleep(10000);
        }

        #endregion

        public static void OpenExportMenu(IWebDriver driver)
        {
            var exportButton = UIActions.GetElementWithWait(driver, "#exportmenu button.dropdown-toggle", 20);
            exportButton.Click();
            Thread.Sleep(1000);
        }

        public static void CloseExportMenu(IWebDriver driver)
        {
            var exportButton = UIActions.GetElementWithWait(driver, "#exportmenu button.dropdown-toggle", 20);
            exportButton.Click();
        }

        public static bool CheckForQuestionVisibilityInInsights(IWebDriver driver, string StrQuestion = null, string OppQuestion = null)
        {
            bool strflag = false;
            bool oppflag = false;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement strwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-view').shadowRoot.getElementById('insightStrengthContainer')");
            string[] strvalues = strwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            IWebElement oppwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('insights-opportunity').shadowRoot.getElementById('insightStrengthContainer')");
            string[] oppvalues = oppwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable strdt = new DataTable();
            strdt.Columns.Add("Col1");
            strdt.Columns.Add("Col2");
            DataTable oppdt = new DataTable();
            oppdt.Columns.Add("Col1");
            oppdt.Columns.Add("Col2");
            List<string> strmyList = new List<string>();
            List<string> oppmyList = new List<string>();
            if (StrQuestion != null)
            {
                for (int i = 0; i < strvalues.Length; i++)
                {
                    if (StrQuestion.ToLower().Trim() == strvalues[i].ToLower().Trim())
                    {
                        strflag = true;
                        break;
                    }
                }
            }
            if (OppQuestion != null)
            {
                for (int i = 0; i < oppvalues.Length; i++)
                {
                    if (OppQuestion.ToLower().Trim() == oppvalues[i].ToLower().Trim())
                    {
                        oppflag = true;
                        break;
                    }
                }
            }
            return (strflag || oppflag);
        }

        public static bool CheckForQuestionVisibilityInDrillDown(IWebDriver driver, string Question = null)
        {
            bool flag = false;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement ddwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('drilldown-view-item').shadowRoot.getElementById('drilldownContainer')");
            string[] values = ddwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            DataTable dt = new DataTable();
            dt.Columns.Add("Col1");
            dt.Columns.Add("Col2");
            List<string> myList = new List<string>();
            if (Question != null)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    if (Question.ToLower().Trim() == values[j].ToLower().Trim())
                    {
                        flag = true;
                        break;
                    }
                }
            }
            return (flag);
        }

        public static bool CheckForQuestionVisibilityInCommentsReport(IWebDriver driver, string Question = null)
        {
            bool flag = false;
            IReadOnlyCollection<IWebElement> CommentElements = UIActions.FindElements(driver, "#comments .question-text");
            for (int i = 0; i < CommentElements.Count; i++)
            {
                if(CommentElements.ElementAt(i).Text.ToLower() == Question.Trim().ToLower())
                {
                    flag = true;
                    break;
                }
            }
            return (flag);
        }
        public static void ExapandAndCollapseFilters(IWebDriver driver, string Filter_ComparisonName, int ComparisonNumber = 1)
        {
            driver.FindElement(By.XPath("(//*[@Class='panel-title']//a//span[contains(text(),'" + Filter_ComparisonName + "')])[" + ComparisonNumber + "]")).Click();
            Thread.Sleep(7000);
        }
        public static bool CheckForQuestionVisibilityInResultSummary(IWebDriver driver,string questionText )
        {
            bool Resultflag = true;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IList<IWebElement> ResultQuestion = (IList<IWebElement>)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelectorAll('PAPER-CARD')");
            IList<IWebElement> ResultQuestion_afterscroll;
            var Itemcount = 0;
            bool IsLoop = true;
            do
            {
                 IJavaScriptExecutor js1 = driver as IJavaScriptExecutor;
                js1.ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
                Thread.Sleep(7000);
                ResultQuestion_afterscroll = (IList<IWebElement>)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelectorAll('PAPER-CARD')");
                if (Itemcount == ResultQuestion_afterscroll.Count)
                {
                    IsLoop = false;
                }
                else
                {
                    Itemcount = ResultQuestion_afterscroll.Count;
                }
            } while (IsLoop);

            for (int i = 0; i < ResultQuestion.Count; i++)
            {
                string ResultQuestiontext = (string)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelectorAll('PAPER-CARD')[" + i + "].textContent");
                if (ResultQuestiontext.Contains("Not enough responses"))
                {
                    ResultQuestiontext = ResultQuestiontext.Replace("Not enough responses", "");
                }
                if (questionText.ToLower() == ResultQuestiontext.ToLower())
                {
                    Resultflag = false;
                }
            }
          
            return Resultflag;
        }

        public static bool ChecktheVisibilityinDrillDown(IWebDriver driver, string question)
        {
            bool DrillDownflag = true;
            IJavaScriptExecutor js1 = driver as IJavaScriptExecutor;
            js1.ExecuteScript("window.scrollTo(762,948)");
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement drilldownelements = (IWebElement)js.ExecuteScript("return document.querySelector('drilldown-view-item').shadowRoot.querySelector('paper-card')");
            string[] drilldownele = drilldownelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            for ( int i=0; i< drilldownele.Length; i++)
            {
                if(drilldownele[i].ToLower().ToString() == question.ToLower())
                {
                    DrillDownflag = false;
                }
            }

            return DrillDownflag;

        }

         public static bool ChecktheVisibilityinTrendReport(IWebDriver driver, string questionText, string questiontype="rating scale")
        {
            bool trendflag = true;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement menu = (IWebElement)js.ExecuteScript("return document.querySelector('trend-view').shadowRoot.querySelector('paper-icon-button').shadowRoot.querySelector('iron-icon')");
            menu.Click();
            IJavaScriptExecutor js1 = driver as IJavaScriptExecutor;
            js1.ExecuteScript("window.scrollTo(500,742)");
            IJavaScriptExecutor js2 = driver as IJavaScriptExecutor;
            IWebElement questionlabel = (IWebElement)js2.ExecuteScript("return document.querySelector('trend-view').shadowRoot.querySelector('paper-icon-button').shadowRoot.querySelector('iron-icon')");
            string[] trendele = questionlabel.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            for ( int i=0; i<trendele.Length;i++)
            {
                if(trendele[i].ToLower().ToString() == questionText.ToLower())

                {
                    trendflag = false;

                }
            }


            return trendflag;

        }

        public static bool CheckHiddenDemoQuestionsdisplayinFilter(IWebDriver driver,string questionText)
        {
            bool Resultflag = false;
            driver.FindElement(By.XPath("//*[@id='Filter']//button[@class='mdc-button mdc-button--dense mdc-button--raised']")).Click();
            IWebElement SearchBox =driver.FindElement(By.XPath("//*[@id='my-text-field']"));
            SearchBox.Clear();
            SearchBox.SendKeys(questionText);
            string DemoQuestionText = driver.FindElement(By.XPath("//*[@class='row filter-level-1']//div[@class='mdc-chip-set']")).Text;
             if(DemoQuestionText != null && DemoQuestionText == questionText)
            {
                Resultflag = true;
            }
            return Resultflag;

        }

        public static bool CheckDateDemoQuestionHiddeninTimePeriod(IWebDriver driver, string questionText)
        {
            bool Resultflag = false;
            driver.FindElement(By.XPath("//*[@id='accordion']//div[@title='All Time Period']")).Click();
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> DateDemotext= driver.FindElements(By.XPath("//*[@id='filter-category-header' and text()='Choose type of date']/following-sibling::div"));
            List<string> DateDemoValue = new List<string>(DateDemotext.Select(x => x.Text));
            Resultflag = (DateDemoValue.Any(x=>x.Contains(questionText)) ? false : true);
            return Resultflag;
        }
        public static void RawdataExport(IWebDriver driver)
        {
            UIActions.clickwithXapth(driver, "//*[@class='mdc-fab mdc-ripple-upgraded app-fab--absolute dropdown-toggle']");

            if (!UIActions.IsElementEnabledByXpath(driver, "//*[contains(text(),'Raw Data Workbook')]"))
            {
                Thread.Sleep(60000);
            }

            UIActions.Click(driver, "#expradio-3");
            Thread.Sleep(3000);
            UIActions.Click(driver, "#exportBtn");
            var RawDataLink = driver.FindElement(By.CssSelector(".download-export-link")).GetAttribute("style");
            while (RawDataLink != "")
            {
                Thread.Sleep(5000);
               RawDataLink = driver.FindElement(By.CssSelector(".download-export-link")).GetAttribute("style");
            }
            UIActions.Click(driver, ".download-export-link");
            Thread.Sleep(5000);
        }
     
        public static void UpdateQuestionVisibilityInTables(DBConnectionStringDTO DBConnectionParameters,string ItemText, string SurveyID, string ShowOrHide)
        {
            try
            {
                string Visibility = (ShowOrHide.ToLower() == "show") ? "1" : "0";
                string UpdateQuery = "UPDATE DimItem SET ItemIsHidden = " + Visibility +
                    " WHERE SurveyId = " + SurveyID +
                    " AND ItemText = '" + ItemText + "'";
                DataSet ds = DBOperations.ExecuteSPwithInputParameters(UpdateQuery, DBConnectionParameters);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static bool CheckForQuestionVisibilityInDemographics(IWebDriver driver, string Question = null)
        {
            bool flag = false;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            IWebElement ddwebelements = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.getElementById('demoContainer')");
            string[] values = ddwebelements.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            
            if (Question != null)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    if (Question.ToLower().Trim() == values[j].ToLower().Trim())
                    {
                        flag = true;
                        break;
                    }
                }
            }
            return (flag);
        }

    }
}
