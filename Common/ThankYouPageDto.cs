﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ThankYouPageDto
    {
        public  string _successMsg { get; set; }
        public  string _previewLink { get; set; }
        public  string _formTitle { get; set; }
        public  string _surveyName { get; set; }

        public  string _editType { get; set; }
        public string _parentWindowHandle { get; set; }
    }
}
