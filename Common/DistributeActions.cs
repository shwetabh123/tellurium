﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;
using OpenQA.Selenium.Remote;
using Aspose.Cells;
using System.Data;
using System.Data.OleDb;

using System.Text;
using Common;

namespace Common
{
    public class DistributeActions
    {
        public static string clipboardText;
        public static string ParticipantEmailAddress;
        public static string DistributionDynamicName;
        public static string Demolabel = "mp";
        public static string latestfile = "";
        public static string downloadFilepath;

        public static string logFilePath;

        public static string folder = null;


        public static string filepath;


        public static string partiallogFilePath = "\\" + "Log-" + System.DateTime.Now.ToString("MM-dd-yyyy_HHmmss") +
                                                  "." + "txt";

        public static IWebElement SearchAndOpenDistribution(IWebDriver driver, string distributionName)
        {

            UIActions.GetElementWithWait(driver, "#distribute-survey-table", 30);
            IWebElement searchBox = driver.FindElement(By.Id("txtSearch"));
            searchBox.Clear();
            searchBox.SendKeys(distributionName);
            UIActions.Click(driver, "#btnSearch");
            IWebElement surveyLink = UIActions.GetElementWithWait(driver, "a[data-bind='SurveyTitle']", 20);
            if (surveyLink != null)
                //UIActions.Click(driver, "#distribute-survey-table a");
                surveyLink.Click();
            var distributepage = UIActions.GetElementWithWait(driver, "#summary-section", 45);
            Thread.Sleep(10000);

            //IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']");


            //surveyLink1.Click();


            return distributepage;
        }

        public static IWebElement SearchAndLaunchDistributionSummary(IWebDriver driver, string distributionName)
        {

            UIActions.GetElementWithWait(driver, "#distribute-survey-table", 20);
            IWebElement searchBox = driver.FindElement(By.Id("txtSearch"));
            searchBox.Clear();
            searchBox.SendKeys(distributionName);
            UIActions.Click(driver, "#btnSearch");
            IWebElement surveyLink = UIActions.GetElementWithWait(driver, "a[data-bind='SurveyTitle']", 20);
            if (surveyLink != null)
                surveyLink.Click();
            var distributepage = UIActions.GetElementWithWait(driver, "#summary-section", 20);
            Thread.Sleep(10000);
            return distributepage;
        }

        public static IWebElement SearchAndCopyLink(IWebDriver driver, string distributionName)
        {

            UIActions.GetElementWithWait(driver, "#distribute-survey-table", 30);
            IWebElement searchBox = driver.FindElement(By.Id("txtSearch"));
            searchBox.Clear();
            searchBox.SendKeys(distributionName);
            UIActions.Click(driver, "#btnSearch");
            IWebElement surveyLink = UIActions.GetElementWithWait(driver, "a[data-bind='SurveyTitle']", 20);
            if (surveyLink != null)
                //UIActions.Click(driver, "#distribute-survey-table a");
                surveyLink.Click();
            var distributepage = UIActions.GetElementWithWait(driver, "#summary-section", 45);
            Thread.Sleep(10000);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']");
            surveyLink1.Click();


            return distributepage;
        }

        public static void NavigateToManageDistrbution(IWebDriver driver)
        {
            try
            {
                var navbuttons = UIActions.FindElements(driver, ".sidebar-menu a span");
                navbuttons.FirstOrDefault(b => b.Text == "Distribute").Click();
                Thread.Sleep(15000);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static string AddParticipant(IWebDriver driver)
        {

            try
            {
                IWebElement element = UIActions.GetElementwithXpath(driver,
                    "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                Actions action = new Actions(driver);
                action.MoveToElement(element).Click().Perform();
                IWebElement editButton = UIActions.GetElementWithWait(driver,
                    "button[widget-data-event='click::SelectParticipantSectionForEdit']", 45);
                if (editButton != null)
                    editButton.Click();
                IWebElement AddParticipantButton =
                    UIActions.GetElement(driver, "[widget-data-event='click::AddParticipantButtonClick']");
                AddParticipantButton.Click();
                Thread.Sleep(1000);

                UIActions.Click(driver, "#tabImportList");
                IWebElement ParticipantList = UIActions.GetElement(driver, ".participant-list");
                ParticipantList.Clear();
                ParticipantEmailAddress = "GN" + Guid.NewGuid() + "@an_Hn.com";
                ParticipantList.SendKeys(ParticipantEmailAddress);
                Thread.Sleep(1000);
                UIActions.clickwithXapth(driver, "(//*[@class='btn btn-primary btn-save-participant-list'])[2]");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ParticipantEmailAddress;
        }

        public static void EditParticipant(IWebDriver driver, int FieldNumber, string value)
        {
            try
            {
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "[widget-data-event='click::EditParticipant']");
                Thread.Sleep(4000);
                UIActions.clickwithXapth(driver, "//*[@class='btn btn-primary edit-fields-btn']");
                IWebElement DemographicValue = UIActions.GetElementwithXpath(driver,
                    "(//*[@class='form-control editable-textbox'])[" + FieldNumber + "]", 55);
                DemographicValue.Clear();
                DemographicValue.SendKeys(value);
                UIActions.Click(driver, "[widget-data-event='click::SaveParticipantDetails']");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        // Edit with Multiple Demo fields in Distribution

        public static void EditParticipantWithCustomDemo(IWebDriver driver, string emailaddress, int FieldCount,
            int ValueCount, string[] value = null)
        {
            try
            {
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                textBox.SendKeys(emailaddress);
                Thread.Sleep(25000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                Thread.Sleep(2000);
                UIActions.Click(driver, "[widget-data-event='click::EditParticipant']");
                Thread.Sleep(4000);
                UIActions.clickwithXapth(driver, "//*[@class='btn btn-primary edit-fields-btn']");
                var DemographicValues = UIActions.FindElementsWithXpath(driver,
                    "//div[@data-bind='IsDemoDateType' and @data-bind-type='hide']//input[@class='form-control editable-textbox' and @data-bind='DemoValue']");
                int i = 0;
                DemographicValues.ToList()
                    .ForEach((element) =>
                    {
                        //use element
                        element.SendKeys(value[i]);
                        i++;
                    });
                UIActions.Click(driver, "[widget-data-event='click::SaveParticipantDetails']");



            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        //Delete Participant from participant Grid.
        public static void DeleteParticipant(IWebDriver driver, String emailaddress)
        {
            try
            {
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                textBox.SendKeys(emailaddress);
                Thread.Sleep(25000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                Thread.Sleep(2000);
                UIActions.Click(driver, ".removeParticipantButton");
                Thread.Sleep(1000);
                UIActions.clickwithID(driver, "btnConfirmYes");
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        // Gayathri-Create Match pair one time Distribution

        public static string CreateOnetimeMatchPairUploadDistribution(IWebDriver driver, String surveyName,
            string StartDate, string EndDate)
        {
            try
            {
                IWebElement DistributionLink = driver.FindElement(By.XPath("//a[contains(@href,'DistributeSurvey')]"));
                DistributionLink.Click();
                driver.FindElement(By.Id("add-distribution")).Click();
                Thread.Sleep(8000);
                driver.FindElement(By.Id("select-survey-link")).Click();
                Thread.Sleep(5000);
                IWebElement SearchField = driver.FindElement(By.Id("txtSearch"));
                SearchField.Clear();
                SearchField.SendKeys(surveyName);
                driver.FindElement(By.Id("btnSearch")).Click();
                Thread.Sleep(3000);
                UIActions.Click(driver, "[widget-data-event='click::SelectClicked']");
                Thread.Sleep(4000);
                UIActions.Click(driver, "#onetime-survey");
                Thread.Sleep(2000);
                IWebElement OpenDate = UIActions.GetElement(driver, "#open-date");
                OpenDate.Click();
                OpenDate.Clear();
                OpenDate.SendKeys(StartDate);
                IWebElement EndDateEle = UIActions.GetElement(driver, "#close-date");
                if (EndDate != null)
                {
                    EndDateEle.Clear();
                    OpenDate.SendKeys(EndDate);
                }
                else
                {
                    UIActions.Click(driver, "#chkNeverExpires");
                }
                IWebElement DistributionName = driver.FindElement(By.Id("txt-distribution-name"));
                DistributionName.Clear();
                DistributionDynamicName = "MatchPair" + Guid.NewGuid();
                DistributionName.SendKeys(DistributionDynamicName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#btn-next");
                Console.WriteLine(DistributionDynamicName);
                return DistributionDynamicName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return DistributionDynamicName;
            }


        }



        public static void CopyUniqueLink(IWebDriver driver)
        {
            try
            {
                //IWebElement editButton = UIActions.GetElement(driver, "[widget-data-event='click::SelectParticipantSectionForEdit']");
                IWebElement editBtn =
                    UIActions.FindElementWithXpath(driver, "//*[@id='participantFilter']/div[1]/div[2]/button[2]");

                Actions ac = new Actions(driver);
                ac.MoveToElement(editBtn).Build().Perform();
                (new WebDriverWait(driver, TimeSpan.FromSeconds(3000)))
                    .Until(
                        ExpectedConditions.ElementIsVisible(
                            By.XPath("//*[@id='participantFilter']/div[1]/div[2]/button[2]")));
                ac.Click().Build().Perform();
                Thread.Sleep(3000);
                // if (editButton != null)
                //     editButton.Click();
                Thread.Sleep(15000);
                var partcipantsTable = UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                UIActions.Click(driver, "#manage-participants-table th.survey-status");
                Thread.Sleep(25000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "#btn-copy-participant-url");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }


        public static void CopyUniqueLinkBySearchParticipant(IWebDriver driver, string emailaddress)
        {
            try
            {
                IWebElement element = UIActions.GetElementwithXpath(driver,
                    "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                Actions action = new Actions(driver);
                Thread.Sleep(5000);
                action.MoveToElement(element).Click().Perform();
                Thread.Sleep(15000);
                UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                textBox.SendKeys(emailaddress);
                Thread.Sleep(10000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "#btn-copy-participant-url");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        

        public static void CopyUniqueLinkBySearchParticipantInParticipantGrid(IWebDriver driver, string emailaddress)
        {
            try
            {
                Thread.Sleep(15000);
                UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                textBox.SendKeys(emailaddress);
                Thread.Sleep(10000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "#btn-copy-participant-url");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        //Gayathri Natarajamani CopyuniqueLinkbySearchparticipantForParent(PS-34064) Issue in production

        public static void CopyUniqueLinkBySearchParticipantForParent(IWebDriver driver, string emailaddress)
        {
            try
            {
                IWebElement element = UIActions.GetElementwithXpath(driver,
                    "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                Actions action = new Actions(driver);
                action.MoveToElement(element).Click().Perform();
                IWebElement editButton =
                    UIActions.GetElement(driver, "[widget-data-event='click::SelectParticipantSectionForEdit']");
                if (editButton != null)
                    editButton.Click();
                Thread.Sleep(15000);
                UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                UIActions.Click(driver, "#parent-view");
                UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                textBox.SendKeys(emailaddress);
                Thread.Sleep(10000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "#btn-copy-participant-url");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        // Gayathri Natarajamani Copy parent link by Selecting the Default first manager
        public static void CopyuniqueLinkBySelectingDefaultFirstManager(IWebDriver driver)
        {
            try
            {
                IWebElement element = UIActions.GetElementwithXpath(driver,
                    "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                Actions action = new Actions(driver);
                action.MoveToElement(element).Click().Perform();
                IWebElement editButton =
                    UIActions.GetElement(driver, "[widget-data-event='click::SelectParticipantSectionForEdit']");
                if (editButton != null)
                    editButton.Click();
                Thread.Sleep(15000);
                UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                UIActions.Click(driver, "#parent-view");
                UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                IList<IWebElement> AllRows =
                    driver.FindElements(By.XPath("//table[@id='manage-participants-table']/tbody/tr"));
                IWebElement ParticipantColumn =
                    driver.FindElement(By.XPath("(//table[@id='manage-participants-table']/tbody/tr[1]/td[1])[1]"));
                ParticipantColumn.Click();
                UIActions.Click(driver, "#btn-copy-participant-url");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void EditMatchPairParticipantForValidation(IWebDriver driver, string emailaddress,
            int FieldNumber, int dummyfieldfocusnumber, string value, bool repeatedEdits = true)
        {
            if (repeatedEdits == false)
            {
                IWebElement editButton =
                    UIActions.GetElement(driver, "[widget-data-event='click::SelectParticipantSectionForEdit']");
                if (editButton != null)
                    editButton.Click();
                Thread.Sleep(120000);
                UIActions.GetElementWithWait(driver, "#manage-participants-table", 45);
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                textBox.SendKeys(emailaddress);
                Thread.Sleep(150000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "[widget-data-event='click::EditParticipant']");
            }

            else
            {
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                Thread.Sleep(12000);
                driver.FindElement(By.ClassName("btn-edit-participant")).Click();
            }
            Thread.Sleep(12000);
            UIActions.clickwithXapth(driver, "//*[@class='btn btn-primary edit-fields-btn']");
            Thread.Sleep(1000);
            UIActions.SendValueMakeItPersist(driver, emailaddress,
                "(//*[@class='form-control editable-textbox'])[" + dummyfieldfocusnumber + "]",
                "(//*[@class='form-control editable-textbox'])[" + FieldNumber + "]", 55);
            //IWebElement DemographicValue = UIActions.GetElementwithXpath(driver, "(//*[@class='form-control editable-textbox'])[" + FieldNumber + "]", 55);
            //DemographicValue.Clear();
            //DemographicValue.SendKeys(value);

            Thread.Sleep(3000);
            IWebElement DemographicValue2 = UIActions.GetElementwithXpath(driver,
                "(//*[@class='form-control editable-textbox'])[" + dummyfieldfocusnumber + "]", 55);
            DemographicValue2.Click();
            //DemographicValue2.Clear();
            //DemographicValue2.SendKeys("25");
            // IWebElement saveButton = driver.FindElement(By.ClassName("btn-primary"));

            //saveButton.SendKeys(Keys.Space);
            UIActions.Click(driver, "[widget-data-event='click::SaveParticipantDetails']");
        }

        public static void CopyUniqueLinkFromEditParticipantPage(IWebDriver driver, string emailaddress)
        {
            try
            {
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                textBox.Clear();
                textBox.SendKeys(emailaddress);
                Thread.Sleep(10000);
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "#btn-copy-participant-url");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }


        //File upload. Gets driver, element of select button to select the file, filepath, element of the upload button
        public static void UploadFileMthd(IWebDriver driver, IWebElement element, String filePath, String uploadBtn)

        {
            try
            {

                string image_path = filePath;
                IWebElement selectBtn = element;
                selectBtn.SendKeys(image_path);
                IWebElement obj_buttonupload = driver.FindElement(By.Id(uploadBtn));
                Actions action = new Actions(driver);
                action.MoveToElement(obj_buttonupload).Build().Perform();
                obj_buttonupload.Click();
                Thread.Sleep(3000);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        // Gayathri-  One time Matchpairupload 

        public static void UploadOneTimeMatchPair(IWebDriver driver)
        {
            IWebElement element = UIActions.GetElementwithXpath(driver,
                "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
            Actions action = new Actions(driver);
            action.MoveToElement(element).Click().Perform();
            IWebElement editButton = UIActions.GetElementWithWait(driver,
                "button[widget-data-event='click::SelectParticipantSectionForEdit']", 45);
            if (editButton != null)
                editButton.Click();
            IWebElement AddParticipantButton =
                UIActions.GetElement(driver, "[widget-data-event='click::AddParticipantButtonClick']");
            AddParticipantButton.Click();
            Thread.Sleep(1000);
            IWebElement selectBtn = driver.FindElement(By.Id("participant-file"));
            DistributeActions.UploadFileMthd(driver, selectBtn,
                @"C:\\Tellurium\\DataSet\\Template_AddParticipantFile(1).xlsx", @"btn-uploadParticipants");
            Thread.Sleep(6000);
            if (IfElementExists(driver, "//*[@id='newDemo-columns-modal-label']"))
            {
                driver.FindElement(By.XPath("//*[@widget-data-event='click::UploadNewDemoParticipantFile']")).Click();
                Thread.Sleep(3000);
            }

        }

        public static void UploadParticipant(IWebDriver driver)
        {
            String pathProject = Environment.CurrentDirectory;
            String filePath = @"C:\Tellurium\Dataset\Template_AddParticipantFile.xlsx";


            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndLaunchDistributionSummary(driver, "AuthenticationLogicUn");

            /****************** Upload of file ***********************/
            IWebElement editBtn =
                UIActions.FindElementWithXpath(driver, "//*[@id='participantFilter']/div[1]/div[2]/button[2]");

            Actions ac = new Actions(driver);
            ac.MoveToElement(editBtn).Build().Perform();
            (new WebDriverWait(driver, TimeSpan.FromSeconds(3000)))
                .Until(ExpectedConditions.ElementIsVisible(
                    By.XPath("//*[@id='participantFilter']/div[1]/div[2]/button[2]")));
            ac.Click().Build().Perform();
            Thread.Sleep(3000);
            try
            {
                UIActions.FindElementWithXpath(driver, "//*[@id='participantGridButtons']/button[1]").Click();
                Thread.Sleep(3000);
                UIActions.FindElementWithXpath(driver, "//*[@class='tabpanel']//li[2]/a").Click();
                IWebElement selectBtn = driver.FindElement(By.Id("participant-file"));
                //  DistributeActions.UploadFileMthd(driver, selectBtn, @"C:\\Automation_C#\\Tellurium\\Inputdata\\Template_AddParticipantFile.xlsx", @"btn-uploadParticipants");
                DistributeActions.UploadFileMthd(driver, selectBtn, filePath, @"btn-uploadParticipants");

                if (IfElementExists(driver, "//*[@id='newDemo-columns-modal-label']"))
                {
                    driver.FindElement(By.XPath("//*[@widget-data-event='click::UploadNewDemoParticipantFile']"))
                        .Click();
                    Thread.Sleep(3000);
                }
            }


            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        //To check if an element exists
        public static bool IfElementExists(IWebDriver driver, String ele)
        {

            try
            {
                bool status = true;
                status = driver.FindElement(By.XPath(ele)).Displayed;
                //status = UIActions.FindElementWithXpath(driver, ele).Enabled ? true : false;
                return status;
            }
            catch (NoSuchElementException)
            {
                return false;
            }


        }

        /*
         * 
         * Create Distribution from Home Page
         * 
         */

        public static void CreateDistributionFromHomePage(IWebDriver driver,
            DistributionParametersDTO DistributionParameters)
        {
            try
            {
                UIActions.Click(driver, ".btn-secondary.margin-left-10");
                Thread.Sleep(10000);
                UIActions.Click(driver, "#select-survey-link");
                Thread.Sleep(5000);
                IWebElement SearchBox = UIActions.GetElement(driver, "#txtSearch");
                SearchBox.Clear();
                SearchBox.SendKeys(DistributionParameters.FormName);
                UIActions.Click(driver, "#btnSearch");
                Thread.Sleep(3000);
                UIActions.Click(driver, "button[widget-data-event='click::SelectClicked']");
                Thread.Sleep(3000);
                IWebElement OpenDate = UIActions.GetElement(driver, "#open-date");
                OpenDate.Clear();
                OpenDate.SendKeys(DistributionParameters.StartDate);
                UIActions.SelectInCEBDropdown(driver, "#open-date-timezone", DistributionParameters.StartTimeZone);
                if (DistributionParameters.EndDate != null)
                {
                    IWebElement EndDate = UIActions.GetElement(driver, "#close-date");
                    EndDate.Clear();
                    EndDate.SendKeys(DistributionParameters.EndDate);
                    UIActions.SelectInCEBDropdown(driver, "#close-date-timezone", DistributionParameters.EndTimeZone);
                }
                else
                {
                    UIActions.Click(driver, "#chkNeverExpires");
                }
                IWebElement DistributionNameTextBox = UIActions.GetElement(driver, "#txt-distribution-name");
                DistributionNameTextBox.Clear();
                DistributionNameTextBox.SendKeys(DistributionParameters.DistributionName);
                if (DistributionParameters.Method.Length > 1)
                {
                    UIActions.Click(driver, "#scheduledMethod");
                    UIActions.Click(driver, "#IsManualMethod");
                }
                else
                {
                    if (DistributionParameters.Method[0].Contains("Share Manually"))
                        UIActions.Click(driver, "#IsManualMethod");
                    else
                        UIActions.Click(driver, "#scheduledMethod");
                }
                UIActions.Click(driver, "#btn-next");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /*
        * 
        * Create Distribution from Manage Distribution Page
        * 
        */

        public static string CreateDistributionFromDistributePage(IWebDriver driver,
            DistributionParametersDTO DistributionParameters)
        {
            try
            {
                UIActions.Click(driver, "#add-distribution");
                Thread.Sleep(7000);
                UIActions.Click(driver, "#select-survey-link");
                Thread.Sleep(3000);
                IWebElement SearchBox = UIActions.GetElement(driver, "#txtSearch");
                SearchBox.Clear();
                SearchBox.SendKeys(DistributionParameters.FormName);
                UIActions.Click(driver, "#btnSearch");
                Thread.Sleep(3000);
                UIActions.Click(driver, "button[widget-data-event='click::SelectClicked']");
                Thread.Sleep(3000);
                IWebElement OpenDate = UIActions.GetElement(driver, "#open-date");
                OpenDate.Clear();
                OpenDate.SendKeys(DistributionParameters.StartDate);
                UIActions.SelectInCEBDropdown(driver, "#open-date-timezone", DistributionParameters.StartTimeZone);
                if (DistributionParameters.EndDate != null)
                {
                    IWebElement EndDate = UIActions.GetElement(driver, "#close-date");
                    EndDate.Clear();
                    EndDate.SendKeys(DistributionParameters.EndDate);
                    UIActions.SelectInCEBDropdown(driver, "#close-date-timezone", DistributionParameters.EndTimeZone);
                }
                else
                {
                    UIActions.Click(driver, "#chkNeverExpires");
                }
                IWebElement DistributionNameTextBox = UIActions.GetElement(driver, "#txt-distribution-name");
                DistributionNameTextBox.Clear();
                DistributionNameTextBox.SendKeys(DistributionParameters.DistributionName);
                if (DistributionParameters.Method.Length > 1)
                {
                    UIActions.Click(driver, "#scheduledMethod");
                    UIActions.Click(driver, "#IsManualMethod");
                }
                else
                {
                    if (DistributionParameters.Method[0].Contains("Share Manually"))
                        UIActions.Click(driver, "#IsManualMethod");
                    else
                        UIActions.Click(driver, "#scheduledMethod");
                }
                UIActions.Click(driver, "#btn-next");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return DistributionParameters.FormName;
        }

        /*
       * 
       * Upload Participants
       * 
       */

        public static void UploadParticipantsWithDemoMapping(IWebDriver driver, string FileName)
        {
            UIActions.Click(driver, "#participantGridButtons .addParticipantButton");
            Thread.Sleep(3000);
            UIActions.Click(driver, "#tabImportFile");
            Thread.Sleep(3000);
            IWebElement SelectFileButton = UIActions.GetElement(driver, "#participant-file");
            SelectFileButton.SendKeys(@"C:\Tellurium\" + FileName);
            UIActions.Click(driver, "#btn-uploadParticipants");
            Thread.Sleep(5000);
            var DataTypeMappingModal = UIActions.GetElement(driver, "#newDemo-columns-modal-label");
            if (DataTypeMappingModal.Displayed)
            {
                IReadOnlyCollection<IWebElement> Dropdowns =
                    UIActions.FindElements(driver, "[widget-data-event='change::SaveNewDemoFieldDatatype']");
                for (int i = 0; i < Dropdowns.Count; i++)
                {
                    var element = UIActions.GetElement(driver,
                        "[widget-data-event='change::SaveNewDemoFieldDatatype']");
                    SelectElement dropdown = new SelectElement(Dropdowns.ElementAt(i));
                    dropdown.SelectByText("Text");

                }
                UIActions.Click(driver, "[widget-data-event='click::UploadNewDemoParticipantFile']");
            }
            Thread.Sleep(5000);
        }

        public static void OpenManualParticipantScreen(IWebDriver driver)
        {
            UIActions.Click(driver, "#participantGridButtons .addParticipantButton");
            Thread.Sleep(3000);
            UIActions.Click(driver, "#tabImportList");
            Thread.Sleep(3000);
        }

        public static void PopulateStdFieldValueInManualParticipantScreen(IWebDriver driver, IWebElement label, string demoValue)
        {
            var txtbox = label.FindElement(By.XPath(".."))
                    .FindElement(By.XPath(".."))
                    .FindElement(By.CssSelector("div.col-sm-6 input.form-control"));
            txtbox.Clear();
            txtbox.SendKeys(demoValue);
        }
        public static void PopulateDemoFieldValueInManualParticipantScreen(IWebDriver driver, IWebElement label, string demoValue)
        {
            var txtbox = label.FindElement(By.XPath(".."))
                    .FindElement(By.XPath(".."))
                    .FindElement(By.XPath(".."))
                    .FindElements(By.CssSelector("div.col-sm-6 input.form-control")).FirstOrDefault(e => e.Displayed);
            txtbox.Clear();
            txtbox.SendKeys(demoValue);
        }

        public static void PopulateDateFieldValueInManualParticipantScreen(IWebDriver driver, IWebElement label, int date, int month, int year)
        {
            var datePickerButton = FindDatePickerInAddEditParticipant(driver, label);
            //UIActions.SelectDateInDatePicker(driver, datePickerButton, date, month, year);
        }
        public static void Addparticipantstandardlabel(IWebDriver driver, String email, String name, string empid)
        {

            var labels = driver.FindElements(By.CssSelector("#participants-manual input"));

            labels.FirstOrDefault(l => l.GetAttribute("data-bind") == "Participant.Name").SendKeys(name);
            labels.FirstOrDefault(l => l.GetAttribute("data-bind") == "Participant.EmailAddress").SendKeys(email);
            labels.FirstOrDefault(l => l.GetAttribute("data-bind") == "Participant.EmployeeId").SendKeys(empid);
        }

        public static void AddparticipantMandatoryField(IWebDriver driver)
        {
            //var mandatorySymbol = UIActions.GetAttrForElementByCssattribute(driver, "#participants-manual span.danger", "display");
            IList<IWebElement> mandatorylabel = driver.FindElements(By.CssSelector("#participants-manual .pull-right label.control-label"));

            foreach (var label in mandatorylabel)
            {
                var labels = label.Text;
                var mandatorySymbol = UIActions.GetAttrForElementByXpathCSSattribute(driver, "//*[@id='participants-manual']//label[text()='" + labels + "']/following-sibling::span", "display");

                if (mandatorySymbol.ToLower() == "inline")
                {
                    var mandatorydemovalue = "demo " + Guid.NewGuid();
                    PopulateDemoFieldValueInManualParticipantScreen(driver, label, mandatorydemovalue);

                }

            }
        }

        public static void EditDemoValueDynamic(IWebDriver driver, string label, string demoValue)
        {
            try
            {
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                UIActions.Click(driver, "[widget-data-event='click::EditParticipant']");
                Thread.Sleep(4000);
                UIActions.clickwithXapth(driver, "//*[@class='btn btn-primary edit-fields-btn']");
                IWebElement DemoValuefield = driver.FindElement(By.XPath("//*[@id='view-edit-participant-modal']//div[@widget-name='ParticipantDetailsWidget']//label[text()='" + label + "']/following::input[2]"));
                //var DemographicValue = driver.FindElement(By.XPath("//*[@class='form-control editable-textbox'])"));
                DemoValuefield.SendKeys(demoValue);
                UIActions.Click(driver, "[widget-data-event='click::SaveParticipantDetails']");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


        }
        //Shwetabh Srivastava----download sample Participant Template
        public static void downloadSampleParticipantUploadfile(IWebDriver driver, string distributionName)
        {

            DistributeActions.NavigateToManageDistrbution(driver);

            DistributeActions.SearchAndLaunchDistributionSummary(driver, distributionName);

            //edit Button 

            IWebElement editBtn =
                UIActions.FindElementWithXpath(driver, "//*[@id='participantFilter']/div[1]/div[2]/button[2]");

            OpenQA.Selenium.Interactions.Actions ac = new Actions(driver);



            //edit Button 
            ac.MoveToElement(editBtn).Build().Perform();
            (new WebDriverWait(driver, TimeSpan.FromSeconds(3000)))
                .Until(ExpectedConditions.ElementIsVisible(
                    By.XPath("//*[@id='participantFilter']/div[1]/div[2]/button[2]")));
            ac.Click().Build().Perform();



            Thread.Sleep(3000);



            //Add participant
            UIActions.FindElementWithXpath(driver, "//*[@id='participantGridButtons']/button[1]").Click();

            Thread.Sleep(10000);

            //Add Participant-Upload
            UIActions.FindElementWithXpath(driver, "//*[@class='tabpanel']//li[2]/a").Click();


            Thread.Sleep(10000);


            //click sample import template
            UIActions.FindElementWithXpath(driver, "//a[text()='sample import template']").Click();
            Thread.Sleep(10000);

            //For Firefox
            //SendKeys.SendWait("{Down}");
            //Thread.Sleep(30000);

            //SendKeys.SendWait("{Enter}");



            //Thread.Sleep(30000);




        }


        //Shwetabh Srivastava----Get Latest Downloaded file from Downloaded folder configured in Config file as per desired capabilities

        public static string getLastDownloadedFile(string folder)
        {
            
            var files = new DirectoryInfo(folder).GetFiles("*.*");

            DateTime lastupdated = DateTime.MinValue;


            foreach (FileInfo file in files)


            {

                if (file.LastWriteTime > lastupdated)

                {

                    lastupdated = file.LastWriteTime;

                    latestfile = file.Name;

                }


            }

            Console.Write("LatestFileName: " + latestfile);

            return latestfile;


        }
        //Shwetabh Srivastava----Add text to excel (parameters-filepath,filename,rownumber,columnnumber and data to write)

        public static void WriteToExcel(IWebDriver driver, String filepath, String fileName, int RowNumber,
            int ColNumber, String dataToWrite)
        {


            //************************Write using Interop****************************************************


            //**************************************************************************************************

            //int rw = 0;
            //int cl = 0;


            //StackFrame stackFrame = new StackFrame();
            //MethodBase methodBase = stackFrame.GetMethod();


            //WriteLog(driver, filepath, "Executing Method :- " + methodBase.Name);



            //Excel.Application xlApp = new Excel.Application();

            //Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filepath + "\\" + latestfile, 0, false, 5, "", "", false,
            //    Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);


            //Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            //Excel.Range xlRange = xlWorkSheet.UsedRange;


            //int rowNumber = xlRange.Rows.Count + 1;



            //rw = xlRange.Rows.Count;
            //cl = xlRange.Columns.Count;



            //xlWorkSheet.Cells[RowNumber, ColNumber] = dataToWrite;

            //WriteLog(driver, filepath, dataToWrite + "-> Data added successfully at Column Number-> " + cl + " and Row Number ->" + rw);



            //// Disable file override confirmaton message  
            //xlApp.DisplayAlerts = false;


            //xlWorkBook.SaveAs(filepath + "\\" + latestfile, Excel.XlFileFormat.xlOpenXMLWorkbook,
            //    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
            //    Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
            //    Missing.Value, Missing.Value);

            //xlWorkBook.Close();
            //xlApp.Quit();

            //Marshal.ReleaseComObject(xlWorkSheet);
            //Marshal.ReleaseComObject(xlWorkBook);
            //Marshal.ReleaseComObject(xlApp);

            //Console.BackgroundColor = ConsoleColor.DarkBlue;
            //Console.WriteLine("\nRecords Added successfully...");
            //Console.BackgroundColor = ConsoleColor.Black;



            //************************Write using Aspose.Cell****************************************************

            //WORKING NOW


            //**************************************************************************************************


            Aspose.Cells.License license = new Aspose.Cells.License();

            license.SetLicense("C:\\Automation\\Aspose.Total.lic");




            Workbook wb = new Workbook(filepath + "\\" + fileName);

            Worksheet worksheet = wb.Worksheets[0];

            Cells cells = worksheet.Cells;

            worksheet.Cells[RowNumber, ColNumber].PutValue(dataToWrite);

            WriteLog(driver, filepath,
                " Data--> (" + dataToWrite + ") added successfully at Column Number-> " + ColNumber +
                " and Row Number ->" + RowNumber);

            wb.Save(filepath + "\\" + latestfile);

        }

        public static string GetConnectionString(String filepath, String fileName)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            // XLSX - Excel 2007, 2010, 2012, 2013
            props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
            props["Extended Properties"] = "Excel 12.0 XML";
            props["Data Source"] = filepath + "\\" + fileName;

            // XLS - Excel 2003 and Older
            //props["Provider"] = "Microsoft.Jet.OLEDB.4.0";
            //props["Extended Properties"] = "Excel 8.0";
            //props["Data Source"] = "C:\\MyExcel.xls";

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }

        //Shwetabh Srivastava----Read contexts of excel file and print in log file

        public static void readExcel(IWebDriver driver, String filepath, String fileName)
        {


            //***************************************************************working code thr Interop****************


            //************************************************************


            //Excel.Application xlApp;
            //Excel.Workbook xlWorkBook;
            //Excel.Worksheet xlWorkSheet;
            //Excel.Range range;

            //string str;
            //int rCnt;
            //int cCnt;
            //int rw = 0;
            //int cl = 0;


            //StackFrame stackFrame = new StackFrame();
            //MethodBase methodBase = stackFrame.GetMethod();
            //WriteLog(driver, filepath, methodBase.Name);

            //xlApp = new Excel.Application();


            //xlWorkBook = xlApp.Workbooks.Open(filepath + "\\" + fileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            //range = xlWorkSheet.UsedRange;
            //rw = range.Rows.Count;
            //cl = range.Columns.Count;

            //WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName + "   are :-");

            //for (rCnt = 1; rCnt <= rw; rCnt++)
            //{
            //    for (cCnt = 1; cCnt <= cl; cCnt++)
            //    {
            //        str = (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;


            //        WriteLog(driver, filepath, str + "\n");

            //    }
            //}


            ////xlWorkBook.SaveAs( latestfile, Excel.XlFileFormat.xlOpenXMLWorkbook,
            ////    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
            ////    Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
            ////    Missing.Value, Missing.Value);

            ////     xlWorkBook.Close();
            //xlApp.Quit();

            //Marshal.ReleaseComObject(xlWorkSheet);
            //Marshal.ReleaseComObject(xlWorkBook);
            //Marshal.ReleaseComObject(xlApp);



            //WriteLog(driver, filepath, "Data Read Successfully");


            //***************************************************************working code with Aspose.Cell****************


            //****************************************************************************************************************





            //StackFrame stackFrame = new StackFrame();
            //MethodBase methodBase = stackFrame.GetMethod();
            //WriteLog(driver, filepath, methodBase.Name);

            ////Open your template file.
            //Workbook wb = new Workbook(filepath + "\\" + fileName);
            ////Get the first worksheet.
            //Worksheet worksheet = wb.Worksheets[0];
            ////Get the cells collection.
            //Cells cells = worksheet.Cells;

            ////Define the list.
            //List<string> myList = new List<string>();
            ////Get the AA column index. (Since "Status" is always @ AA column.
            //int col = CellsHelper.ColumnNameToIndex("A");

            ////Get the last row index in AA column.
            //int last_row = worksheet.Cells.GetLastDataRow(col);

            ////Loop through the "Status" column while start collecting values from row 9
            ////to save each value to List
            //for (int i = 8; i <= last_row; i++)
            //{
            //    myList.Add(cells[i, col].Value.ToString());
            //}

            //WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName + "   are :-");

            //foreach (string element  in myList)
            //{
            //    WriteLog(driver, filepath, element + "\n");
            //}


            //WriteLog(driver, filepath, "Data Read Successfully");





            Workbook wb = new Workbook(filepath + "\\" + fileName);

            Worksheet worksheet = wb.Worksheets[0];


            Cells cells = worksheet.Cells;

            WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName + "   are :-");


            for (int i = 0; i < cells.MaxDataRow + 1; i++)

            {

                for (int j = 0; j < cells.MaxDataColumn + 1; j++)

                {

                    string s = cells[i, j].StringValue.Trim();

                    WriteLog(driver, filepath, s + "\n");

                }

            }







            WriteLog(driver, filepath, "Data Read Successfully");









            //**********************************not working for ***********************************
            //Not working for   Read and Write Excel Documents Using OLEDB

            //**********************************************************************************************




            // DataSet ds = new DataSet();

            // string connectionString = GetConnectionString(filepath, fileName);


            // using (OleDbConnection conn = new OleDbConnection(connectionString))
            // {
            //     conn.Open();
            //     OleDbCommand cmd = new OleDbCommand();
            //     cmd.Connection = conn;

            //     // Get all Sheets in Excel File
            //     DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            //     // Loop through all Sheets to get data
            //     foreach (DataRow dr in dtSheet.Rows)
            //     {
            //         string sheetName = dr["TABLE_NAME"].ToString();

            //         if (!sheetName.EndsWith("$"))
            //             continue;

            //         // Get all rows from the Sheet
            //         cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

            //         DataTable dt = new DataTable();
            //         dt.TableName = sheetName;

            //         OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            //         da.Fill(dt);

            //         ds.Tables.Add(dt);
            //     }


            //     WriteLog(driver, filepath, "ds");

            //     cmd = null;
            //     conn.Close();
            // }

            //// return ds;





        }


        //Shwetabh Srivastava----Read specified contexts of excel file and print in log file

        public static string readExcelrowcolumnwise(IWebDriver driver, String filepath, String fileName, int sRowNumber,
            int eRowNumber, int sColNumber, int eColNumber)
        {


            //***************************************************************working code****************


            //Excel.Application xlApp;
            //Excel.Workbook xlWorkBook;
            //Excel.Worksheet xlWorkSheet;
            //Excel.Range range;


            //int rw = 0;
            //int cl = 0;


            //StackFrame stackFrame = new StackFrame();
            //MethodBase methodBase = stackFrame.GetMethod();


            //WriteLog(driver, filepath, "Executing Method :- " + methodBase.Name);



            //xlApp = new Excel.Application();


            //xlWorkBook = xlApp.Workbooks.Open(filepath + "\\" + fileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            //range = xlWorkSheet.UsedRange;


            //rw = range.Rows.Count;
            //cl = range.Columns.Count;

            //WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName);



            //string test = xlWorkSheet.Cells[RowNumber, ColNumber].Value.ToString();



            //WriteLog(driver, filepath, "Excel data for Row Number ->" + RowNumber + " and Coumn Number " + ColNumber + " is :" + test);



            ////xlWorkBook.SaveAs( latestfile, Excel.XlFileFormat.xlOpenXMLWorkbook,
            ////    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
            ////    Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
            ////    Missing.Value, Missing.Value);

            ////     xlWorkBook.Close();
            //xlApp.Quit();

            //Marshal.ReleaseComObject(xlWorkSheet);
            //Marshal.ReleaseComObject(xlWorkBook);
            //Marshal.ReleaseComObject(xlApp);



            //WriteLog(driver, filepath, "Data Read Successfully \n \n");







            //*********************************************
            //     using aspose.cells

            //********************************


            DistributeActions.WriteLog(driver, filepath, "**************************************");


            DistributeActions.WriteLog(driver, filepath, "Inside method readExcelrowcolumnwise\n \n");


            DistributeActions.WriteLog(driver, filepath, "********************************************");


            string s = null;

            Workbook wb = new Workbook(filepath + "\\" + fileName);

            Worksheet worksheet = wb.Worksheets[0];


            Cells cells = worksheet.Cells;



            WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName + "   are :-");


            for (int i = sRowNumber; i <= eRowNumber; i++)

            {

                for (int j = sColNumber; j <= eColNumber; j++)

                {

                    //       s = cells[i, j].StringValue.Trim();

                    s = Convert.ToString(cells[i, j].StringValue.Trim());
         

                    WriteLog(driver, filepath, "Excel data for Row Number ->" + i + " and Column Number " + j + " is :" + s);


                }



            }


            WriteLog(driver, filepath, "Data Read Successfully \n \n");
            return s;

        }





        //Shwetabh Srivastava----Read specified contexts of excel file and print in log file

        public static string ReaddataonmultiplesheetExcelrowcolumnwise(IWebDriver driver, String filepath, String fileName, int sRowNumber, int eRowNumber, int sColNumber, int eColNumber, string SheetName)

        {



            //*********************************************
            //     using aspose.cells

            //********************************
 
            DistributeActions.WriteLog(driver, filepath, "**************************************");


            DistributeActions.WriteLog(driver, filepath, "Inside method ReaddataonmultiplesheetExcelrowcolumnwise \n \n");


            DistributeActions.WriteLog(driver, filepath, "********************************************");


            string s = null;

            Workbook wb = new Workbook(filepath + "\\" + fileName);




            Worksheet worksheet = wb.Worksheets[SheetName];



            //    Worksheet worksheet = wb.Worksheets[0];


            Cells cells = worksheet.Cells;



            WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName + "   are :-");


            for (int i = sRowNumber; i <= eRowNumber; i++)

            {

                for (int j = sColNumber; j <= eColNumber; j++)

                {

                    //       s = cells[i, j].StringValue.Trim();

                    s = Convert.ToString(cells[i, j].StringValue.Trim());


                    WriteLog(driver, filepath, "Actual Excel data for Row Number ->" + i + " and Column Number " + j + " in Sheet  is :" + s);


              

                }



            }


            WriteLog(driver, filepath, "Data Read Successfully \n \n");
            return s;

        }

        //Shwetabh Srivastava----Read specified contexts of excel file and print in log file

        public static string ReaddataonmultiplesheetExcelrowcolumnwise1(IWebDriver driver, String filepath, String fileName, int sRowNumber, int eRowNumber, int sColNumber, int eColNumber, string SheetName)

        {



            //*********************************************
            //     using aspose.cells

            //********************************

            DistributeActions.WriteLog(driver, filepath, "**************************************");


            DistributeActions.WriteLog(driver, filepath, "Inside method ReaddataonmultiplesheetExcelrowcolumnwise \n \n");


            DistributeActions.WriteLog(driver, filepath, "********************************************");


            string s = null;

            Workbook wb = new Workbook(filepath + "\\" + fileName);




            Worksheet worksheet = wb.Worksheets[SheetName];



            //    Worksheet worksheet = wb.Worksheets[0];


            Cells cells = worksheet.Cells;



            WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName + "   are :-");


            for (int i = sRowNumber; i <= eRowNumber; i++)

            {

                for (int j = sColNumber; j <= eColNumber; j++)

                {

                    //       s = cells[i, j].StringValue.Trim();

                    s = Convert.ToString(cells[i, j].StringValue.Trim());


                    WriteLog(driver, filepath, "Actual Excel data for Row Number ->" + i + " and Column Number " + j + " in Sheet  is :" + s);




                }



            }


            WriteLog(driver, filepath, "Data Read Successfully \n \n");
            return s;

        }








        //Shwetabh Srivastava----Read specified contexts of excel file and print in log file

        public static bool verifydataonmultiplesheetExcelrowcolumnwise(IWebDriver driver, String filepath, String fileName, int sRowNumber, int eRowNumber, int sColNumber, int eColNumber, string SheetName, string texttoverify)

        {



            //*********************************************
            //     using aspose.cells

            //********************************
            bool result = false;

            DistributeActions.WriteLog(driver, filepath, "**************************************");


            DistributeActions.WriteLog(driver, filepath, "Inside method verifydataonmultiplesheetExcelrowcolumnwise \n \n");


            DistributeActions.WriteLog(driver, filepath, "********************************************");


            string s = null;

            Workbook wb = new Workbook(filepath + "\\" + fileName);




            Worksheet worksheet = wb.Worksheets[SheetName];



            //    Worksheet worksheet = wb.Worksheets[0];


            Cells cells = worksheet.Cells;



            WriteLog(driver, filepath, "Reading Excel contents of file " + filepath + "\\" + fileName + "   are :-");


            for (int i = sRowNumber; i <= eRowNumber; i++)

            {

                for (int j = sColNumber; j <= eColNumber; j++)

                {

                    //       s = cells[i, j].StringValue.Trim();

                    s = Convert.ToString(cells[i, j].StringValue.Trim());


                    WriteLog(driver, filepath, "Excel data for Row Number ->" + i + " and Column Number " + j + " is :" + s);


                    if (s.Equals(texttoverify))

                    {
                        DistributeActions.WriteLog(driver, filepath, "Actual Excel Data for Row Number ->" + i + " and Column Number -->" + j + " is :[" + s + "] &  Expected data is -->[" + texttoverify + "]. Hence ,verification is Passed ");

                        return true;


                    }
                    else
                    {
                        DistributeActions.WriteLog(driver, filepath, "Actual Excel Data for Row Number ->" + i + " and Column Number -->" + j + " is :[" + s + "] &  Expected data -->[" + texttoverify + "]. Hence ,verification is failed ");

                        throw new Exception("Actual Excel Data for Row Number ->" + i + " and Column Number -->" + j + " is :[" + s + "] &  Expected data -->[" + texttoverify + "]. Hence ,verification is failed ");
                        return false;

                    }

                }



            }


            WriteLog(driver, filepath, "Data Read Successfully \n \n");
            return result;

        }




        // Shwetabh Srivastava---Generates the log file

        public static void WriteLog(IWebDriver driver, String filepath, String strLog)
        {


            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;


            string logFilePath = filepath;


            logFilePath = logFilePath + partiallogFilePath;

            logFileInfo = new FileInfo(logFilePath);

            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);

            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();




        }


        public static void AddInvitation(RemoteWebDriver driver, String surveyName, String metaTag)
        {
            var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            EmailMethods.CreateEmailThruEditDistribution(driver,
                "//button[@widget-data-event='click::" + methodName + "']", surveyName, methodName, metaTag);
            var inviteEmailLink = driver.FindElement(By.CssSelector("span[data-bind='InvitationEmail.Name']")).Text;
        }


        public static void AddReminder(RemoteWebDriver driver, String surveyName, String metaTag)
        {
            var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            EmailMethods.CreateEmailThruEditDistribution(driver,
                "//button[@widget-data-event='click::" + methodName + "']", surveyName, methodName, metaTag);
            var inviteEmailLink = driver.FindElement(By.CssSelector("span[data-bind='bindRemainderEmailSubject']"))
                .Text;

        }

        public static void AddCustomEmail(RemoteWebDriver driver, String surveyName)
        {
            var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Actions ac = new Actions(driver);
            EmailMethods.CreateEmailThruEditDistribution(driver,
                "//button[@widget-data-event='click::" + methodName + "']", surveyName, methodName, "Survey URL");
            var customEmailLink = driver.FindElement(By.CssSelector("span[data-bind='bindCustomEmailSubject']")).Text;
            //ac.MoveToElement(driver.FindElement(By.XPath("//*[@id='emaileditSection']/div[10]//i[@class=' danger ceb-icon_delete2_knockout pull-right removeEmail']"))).Click().Build().Perform();
        }

        public static bool ReopenSurvey(IWebDriver driver)
        {
            bool flag = false;
            if (!((UIActions.IsElementVisible(driver, "//*[@id='distribute-summary-page-header']") ||
                   (UIActions.IsElementVisible(driver, "//*[@widget-data-event='click::AddParticipantButtonClick']")))))
            {
                try
                {
                    driver.Close();
                    // UIActions.SwitchToWindowWithTitle("Distribute Survey - Pulse", driver);
                    driver.Navigate().Refresh();
                    Thread.Sleep(7000);
                    IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                    js.ExecuteScript("window.scrollBy(0,500)");
                    IWebElement EditParticipant = UIActions.GetElementwithXpath(driver,
                        "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                    EditParticipant.Click();
                    Thread.Sleep(2000);
                    IReadOnlyCollection<IWebElement> WebRow = driver.FindElements(By.XPath("//tbody//tr[1]"));

                    for (int i = 1; i < WebRow.Count; i++)
                    {

                        IWebElement Column =
                            UIActions.GetElementwithXpath(driver, "(//tbody//tr[" + i + "]/td[5])[1]", 30);

                        if (Column.Text.Equals("Finished") && Column.Displayed && Column.Enabled)
                        {
                            int j = i;
                            IWebElement participant = UIActions.FindElementWithXpath(driver,
                                "(//*[@class='email']/../..//td[1])[" + j + "]");
                            participant.Click();
                            UIActions.GetElementwithXpath(driver, "//*[@class='btn btn-primary dropdown-toggle']", 10)
                                .Click();
                            IWebElement ReopenSurvey =
                                UIActions.GetElement(driver, ".btn-ReopenSurvey-participant-grid-action-button");
                            ReopenSurvey.Click();
                            UIActions.GetElement(driver, "#btn-copy-participant-url").Click();
                            String clipboardText = System.Windows.Forms.Clipboard.GetText();
                            UIActions.OpenNewTab(driver, clipboardText);
                            if (UIActions.IsElementNotVisible(driver,
                                "//*[contains(text(),'You have already completed this survey.')]"))
                            {
                                driver.Close();
                                flag = true;
                                break;
                            }
                            else
                            {
                                driver.Close();
                                flag = false;
                                break;
                            }
                            // UIActions.SwitchToWindowWithTitle("Distribute Survey - Pulse", driver);
                        }

                    }
                    return flag;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }


            }
            else
            {
                try
                {

                    IReadOnlyCollection<IWebElement> WebRow = driver.FindElements(By.XPath("//tbody//tr[1]"));
                    for (int i = 1; i < WebRow.Count; i++)
                    {
                        IWebElement Column =
                            UIActions.GetElementwithXpath(driver, "(//tbody//tr[" + i + "]/td[5])[1]", 30);

                        if (Column.Text.Equals("Finished") && Column.Displayed && Column.Enabled)
                        {
                            int j = i;
                            IWebElement participant =
                                UIActions.FindElementWithXpath(driver, "(//*[@class='email']/../..//td[1])[" + j + "]");
                            participant.Click();
                            UIActions.GetElementwithXpath(driver, "//*[@class='btn btn-primary dropdown-toggle']", 10)
                                .Click();
                            IWebElement ReopenSurvey =
                                UIActions.GetElement(driver, ".btn-ReopenSurvey-participant-grid-action-button");
                            ReopenSurvey.Click();
                            UIActions.GetElement(driver, "#btn-copy-participant-url").Click();
                            String clipboardText = System.Windows.Forms.Clipboard.GetText();
                            UIActions.OpenNewTab(driver, clipboardText);
                            if (UIActions.IsElementNotVisible(driver,
                                "//*[contains(text(),'You have already completed this survey.')]"))
                            {
                                driver.Close();
                                flag = true;
                                break;
                            }
                            else
                            {
                                driver.Close();
                                flag = false;
                                break;
                            }

                        }

                    }

                    return flag;
                }

                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
        }

        public static String resetresponses(IWebDriver driver)
        {
            string Flag = null;
            if ((UIActions.IsElementVisible(driver, "//*[@id='distribute-summary-page-header']") ||
                 (UIActions.IsElementVisible(driver, "//*[@widget-data-event='click::AddParticipantButtonClick']"))))
            {
                IReadOnlyCollection<IWebElement> WebRow = driver.FindElements(By.XPath("//tbody//tr[1]"));
                for (int i = 1; i < WebRow.Count; i++)
                {
                    IWebElement Column = UIActions.GetElementwithXpath(driver, "//tbody//tr[" + i + "]/td[5]", 30);



                    if ((Column.Text.Equals("Finished") || Column.Text.Equals("In Progress")) && Column.Displayed &&
                        Column.Enabled)
                    {
                        int j = i;
                        IWebElement participant =
                            UIActions.FindElementWithXpath(driver, "(//*[@class='email']/../..//td[1])[" + j + "]");
                        participant.Click();

                        UIActions.GetElementwithXpath(driver, "//*[@class='btn btn-primary dropdown-toggle']", 10)
                            .Click();
                        Thread.Sleep(3000);
                        IWebElement Reset =
                            UIActions.GetElement(driver, ".btn-DeleteResponses-participant-grid-action-button");
                        Reset.Click();
                        Thread.Sleep(5000);
                        IWebElement PleaseConfirmYes =
                            UIActions.FindElementWithXpath(driver, "//*[@id='btnConfirmYes']");
                        PleaseConfirmYes.Click();
                        Thread.Sleep(4000);
                        // if (UIActions.IsElementVisible(driver, "//*[contains(text(),'Responses have been deleted.')]"))
                        // Flag= "Responses have been deleted.";
                        IWebElement emailelement =
                            UIActions.FindElementWithXpath(driver, "(//td//span[@class='email'])[1]");
                        string email = emailelement.Text;
                        //CopyUniqueLinkBySearchParticipant(driver, email);
                        IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
                        textBox.Clear();
                        textBox.SendKeys(email);
                        Thread.Sleep(10000);
                        //UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                        IWebElement colstatus =
                            UIActions.GetElementwithXpath(driver, "//*[@id='[object Object]']/td[5]/span", 30);
                        if (colstatus.Text.Equals("Not Started"))
                            return colstatus.Text;
                    }
                }

                string returnvalue = "Could not reset responses";
                return (returnvalue);
            }

            else
            {
                string returnvalue2 = "Could not reset responses";
                return (returnvalue2);
            }
        }

        public static bool OpenImportResponseModal(IWebDriver driver)
        {
            UIActions.Click(driver, "#participantGridButtons .dropdown-toggle");
            Thread.Sleep(1500);
            UIActions.Click(driver, "#btnImportResponse");
            IWebElement downloadTemplateLink = driver.FindElement(By.Id("DownLoadImportResponseTemplate"));
            return (downloadTemplateLink != null);
        }

        public static bool UploadImportResponseTemplate(IWebDriver driver, string filePath)
        {
            bool isUploadSuccess = false;
            try
            {
                UploadFileMthd(driver, UIActions.GetElement(driver, "#paper-participant-file"), filePath,
                    "btn-upload-participant-response-file");
                Thread.Sleep(30000);
                while (true)
                {
                    IWebElement downloadStatusLink = driver.FindElement(By.Id("pending-file-status"));
                    if (downloadStatusLink.Text == "Upload process completed.")
                    {
                        isUploadSuccess = true;
                        break;
                    }
                    Thread.Sleep(2000);
                }
            }
            catch (Exception e)
            {
                isUploadSuccess = false;
            }

            return isUploadSuccess;
        }

        public static void CreateInviteEmail(IWebDriver driver)
        {
            try
            {
                IWebElement Element = UIActions.GetElement(driver, "[widget-data-event='click::SelectEmailSectionForEdit']");
                Actions action = new Actions(driver);
                action.MoveToElement(Element).Click().Perform();
                IWebElement EmailEditButton = UIActions.GetElement(driver, "[widget-data-event='click::SelectEmailSectionForEdit']");
                if (EmailEditButton != null)
                {
                    EmailEditButton.Click();

                    Thread.Sleep(5000);
                }

                IWebElement CreateInviteButton = UIActions.GetElement(driver, "[widget-data-event='click::AddInvitation']");
                var CreateInviteButtonisVisible = UIActions.GetAttrForElementByCss(driver, "[data-bind='InvitationEmail.EmailCreated']", "style");
                if (CreateInviteButtonisVisible == "")
                {
                    CreateInviteButton.Click();
                    Thread.Sleep(1000);
                }
                else
                {
                    IWebElement EditInvitiation = UIActions.GetElementwithXpath(driver, "(//*[@data-target='#add-email-modal'])[1]", 2);
                    EditInvitiation.Click();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }



        public static void CreateReminderEmail(IWebDriver driver)
        {
            try
            {
                IWebElement Element = UIActions.GetElement(driver, "[widget-data-event='click::SelectEmailSectionForEdit']");
                Actions action = new Actions(driver);
                action.MoveToElement(Element).Click().Perform();
                IWebElement EmailEditButton = UIActions.GetElement(driver, "[widget-data-event='click::SelectEmailSectionForEdit']");
                if (EmailEditButton != null)
                {
                    EmailEditButton.Click();

                    Thread.Sleep(5000);
                }

                IWebElement CreateReminderButton = UIActions.FindElementWithXpath(driver, "(//*[@widget-data-event='click::AddReminder'])[1]");
                Actions ac = new Actions(driver);
                ac.MoveToElement(CreateReminderButton).Build().Perform();
                var CreateReminderButtonisVisible = UIActions.GetAttrForElementByXpath(driver, "(//div[@data-bind-iterate='ReminderEmails']//div[@data-bind='EmailCreated'])[1]", "data-bind-type");
                var CreateReminderButtonstyle = UIActions.GetAttrForElementByXpath(driver, "(//div[@data-bind-iterate='ReminderEmails']//div[@data-bind='EmailCreated'])[1]", "style");

                if (CreateReminderButtonstyle == "")
                {
                    if (CreateReminderButton != null)
                    {
                        CreateReminderButton.Click();
                        Thread.Sleep(1000);
                    }
                }

                else
                {
                    IWebElement EditInvitiation = UIActions.GetElementwithXpath(driver, "(//div[@data-bind-iterate='ReminderEmails']//div[@data-bind='EmailCreated'])[2]", 2);
                    EditInvitiation.Click();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static bool CheckImportResponseStatus(IWebDriver driver, string statusMessage)
        {
            IWebElement downloadStatusLink = driver.FindElement(By.Id("pending-file-status"));
            if (downloadStatusLink != null)
            {
                downloadStatusLink.Click();
                Thread.Sleep(30000);
                var anchorElement = driver.FindElements(By.CssSelector("#import-tracker-table a"))
                    .FirstOrDefault(e => e.Displayed && e.Text.Trim() == statusMessage);
                if (anchorElement != null)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ValidateErrorReportIsDownloaded(IWebDriver driver, string statusMessage, string filePath, string sourcePath)
        {
            try
            {
                var anchorElement = driver.FindElements(By.CssSelector("#import-tracker-table a"))
                    .FirstOrDefault(e => e.Displayed && e.Text.Trim() == statusMessage);
                anchorElement?.Click();
                Thread.Sleep(2000);
                string latestfile = DistributeActions.getLastDownloadedFile(filePath);
                CommonActions.CompareExcels(sourcePath, filePath + @"\" + latestfile, filePath);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static string EditDemographicLabel(IWebDriver driver)
        {
            try
            {
                IWebElement element = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                Actions action = new Actions(driver);
                action.MoveToElement(element).Click().Perform();
                IWebElement editButton = UIActions.GetElementWithWait(driver,
                    "button[widget-data-event='click::SelectParticipantSectionForEdit']", 45);
                if (editButton != null)
                    editButton.Click();
                Thread.Sleep(20000);
                IWebElement EditDemographicButton = UIActions.GetElementwithXpath(driver, "//button[@widget-data-event='click::TriggerEditColumns']", 45);
                EditDemographicButton.Click();
                IWebElement EditDemovalue = UIActions.GetElementwithXpath(driver, "//*[@id='manage-Demographic-table']//tbody//tr//td//label[contains(.,'" + Demolabel + "')]/following-sibling::div[2]", 45);
                EditDemovalue.Click();
                IWebElement DemoField = UIActions.GetElementWithWait(driver, "//*[@id='manage-Demographic-table']//tbody//tr//td//label[contains(.,'" + Demolabel + "'))]/following-sibling::div[2]//input", 45);
                Demolabel = "Demolabel" + Guid.NewGuid();
                DemoField.SendKeys(Demolabel);
                UIActions.clickwithXapth(driver, "//*[@class='ceb-icon_tick action-demo-edit-confirm']");
                return Demolabel;
            }

            catch (Exception e)
            {
                return Demolabel;
            }
        }

        public static void CreateEmailTemplate(IWebDriver driver, string name, string SurveyName, string SubjectCKEditorvalue, string BodyCKeditorvalue)
        {
            try
            {
                Actions ac = new Actions(driver);
                IWebElement NameField = UIActions.GetElementwithXpath(driver, "//*[@class='form-control email-name-text']", 2);
                NameField.Click();
                NameField.SendKeys(name);
                IWebElement Subject = UIActions.GetElementwithXpath(driver, "(//*[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders'])[1]", 2);
                Subject.Clear();
                Subject.Click();
                Thread.Sleep(1000);
                IWebElement CkEditorElementforsubject = driver.FindElement(By.XPath("(//*[@class='cke_button_icon cke_button__metatag_icon'])[2]"));
                Thread.Sleep(2000);
                CkEditorElementforsubject.Click();
                var CKEditorInsideElement = UIActions.GetElementwithXpath(driver, "(//*[@class='metaTags ui-autocomplete-input'])[1]", 1);
                CKEditorInsideElement.Click();
                var CKEditorValues = UIActions.FindElementsWithXpath(driver, "(//*[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all'])[1]//li//a");
                var elem = CKEditorValues.ToList().Find(x => x.Text.Equals(SubjectCKEditorvalue));
                if (elem != null)
                {
                    elem.Click();
                }

                Subject.SendKeys(SurveyName);
                Thread.Sleep(10000);
                var Body = driver.FindElement(By.XPath("//*[@title='Rich Text Editor, email-body']"));
                Body.Clear();
                Body.Click();
                Body.SendKeys(SurveyName);

                IWebElement CkEditorElementForBody = driver.FindElement(By.XPath("(//*[@class='cke_button_icon cke_button__metatag_icon'])[1]"));
                CkEditorElementForBody.Click();
                Thread.Sleep(2000);
                IWebElement CKEditorinside = UIActions.GetElementwithXpath(driver, "(//*[@class='metaTags ui-autocomplete-input'])[1]", 1);
                CKEditorinside.Click();
                var CKEditorValuesforBody = driver.FindElements(By.XPath("(//*[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all'])[2]//li//a"));
                var element = CKEditorValuesforBody.ToList().Find(x => x.Text.Equals(BodyCKeditorvalue));
                if (element != null)
                {
                    element.Click();
                }
                UIActions.Click(driver, "[widget-data-event='click::SaveEmailForm']");
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static string PublishForm(RemoteWebDriver driver)
        {
            string surveyForDistribution = driver.FindElement(By.XPath("//h2[@class='primary bolder surveyTitle-wrap']")).Text;
            var publishBtn = UIActions.FindElementsWithXpath(driver, "//li/button[@type='button']");
            Thread.Sleep(2000);

            publishBtn.FirstOrDefault(d => d.Text == "Publish").Click();
            if (UIActions.GetElement(driver, "#btnConfirmPublishClicked") != null)
            {
                Thread.Sleep(6000);
                UIActions.GetElement(driver, "#btnConfirmPublishClicked").Click();
            }
            Thread.Sleep(6000);
        

            return surveyForDistribution;
        }
        public static void PublishAndCreateDistribution(IWebDriver driver, DistributionParametersDTO DistributionParameters)
        {
            if (UIActions.GetElement(driver, "#distribute-survey") != null)
            {
                Thread.Sleep(6000);
                UIActions.GetElement(driver, "#distribute-survey").Click();
            }
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8000));
            Thread.Sleep(3000);
            IWebElement OpenDate = UIActions.GetElement(driver, "#open-date");
            OpenDate.Clear();
            OpenDate.SendKeys((DistributionParameters.StartDate).ToString());
            UIActions.SelectInCEBDropdown(driver, "#open-date-timezone", DistributionParameters.StartTimeZone);
            if (DistributionParameters.EndDate != null)
            {
                IWebElement EndDate = UIActions.GetElement(driver, "#close-date");
                EndDate.Clear();
                Actions ac = new Actions(driver);
                //EndDate.Click();
                //ac.MoveToElement(EndDate).SendKeys(OpenQA.Selenium.Keys.Tab).Perform();
                Thread.Sleep(3000);
                EndDate.SendKeys((DistributionParameters.EndDate).ToString());
                ac.MoveToElement(EndDate).SendKeys(OpenQA.Selenium.Keys.Tab).Perform();
                UIActions.SelectInCEBDropdown(driver, "#close-date-timezone", DistributionParameters.EndTimeZone);
                Thread.Sleep(3000);
            }
            else
            {
                UIActions.Click(driver, "#chkNeverExpires");
                Thread.Sleep(3000);
            }
            IWebElement DistributionNameTextBox = UIActions.GetElement(driver, "#txt-distribution-name");
            Thread.Sleep(3000);
            DistributionNameTextBox.Clear();
            DistributionNameTextBox.SendKeys(DistributionParameters.DistributionName);
            if (DistributionParameters.Method.Length > 1)
            {
                UIActions.Click(driver, "#scheduledMethod");
                UIActions.Click(driver, "#IsManualMethod");
            }
            else
            {
                if (DistributionParameters.Method[0].Contains("Share Manually"))
                    UIActions.Click(driver, "#IsManualMethod");
                else
                    UIActions.Click(driver, "#scheduledMethod");
            }

            wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#btn-next")));
            UIActions.Click(driver, "#btn-next");
            Thread.Sleep(3000);
        }

        public static bool CheckIfQuotaModelOpens(RemoteWebDriver driver)
        {
            Thread.Sleep(10000);
            var quotaButton = UIActions.GetElement(driver, "#quotaConfigurationButton");
            quotaButton.Click();
            Thread.Sleep(10000);
            return (UIActions.GetElement(driver, "#Quota-Case-table") != null);
        }

        public static bool CheckIfAddNewQuotaModalOpens(RemoteWebDriver driver)
        {
            var addQuotaButton = UIActions.GetElement(driver, "#add-Quota-logic");
            addQuotaButton.Click();
            Thread.Sleep(10000);
            return (UIActions.GetElement(driver, "#AddQuotaLogicCaseButton") != null);
        }

        public static void FillCommonQuotaDetails(RemoteWebDriver driver, string quotaName, string quotaLimit, string accordingTo)
        {
            IWebElement txtQuotaName = driver.FindElement(By.Id("QuotaName"));
            txtQuotaName.Clear();
            txtQuotaName.SendKeys(quotaName);
            IWebElement txtQuotaLimit = driver.FindElement(By.Id("QuotaLimit"));
            txtQuotaLimit.Clear();
            txtQuotaLimit.SendKeys(quotaLimit);
            UIActions.SelectInCEBDropdownByText(driver, "#rawdata-select-caseJoinType", accordingTo);
        }

        public static bool ValidateIfQuotaIsSaved(RemoteWebDriver driver, string quotaName, string quotaCriteria, int startIndex)
        {
            var ResultElements = driver.FindElements(By.CssSelector("#Quota-Case-table td"));

            foreach (var elem in ResultElements)
            {
                if (elem.Text == quotaName)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ValidateIfQuestionQuotaSaved(RemoteWebDriver driver)
        {
            Thread.Sleep(3000);
            FillCommonQuotaDetails(driver, "Question based quota", "2", "Any of");
            UIActions.SelectInCEBDropdownByText(driver, "[data-bind='SourceType']", "Question");
            UIActions.SelectInCEBDropdownByText(driver, "[name='select-sourceQuestion']", "Source 1");
            UIActions.SelectInCEBDropdownByText(driver, "[name='select-condition']", "Equal To");
            UIActions.SelectInCEBDropdownByText(driver, "[name='select-logic']", "Exactly");
            UIActions.SelectInCEBDropdownByText(driver, "[name='select-Response']", "2");
            var saveQuotaLogicButton = UIActions.GetElement(driver, "#quota-save-LogicBuilder");
            saveQuotaLogicButton.Click();
            Thread.Sleep(10000);
            return ValidateIfQuotaIsSaved(driver, "Question based quota", "Q001.Source 1 = 2", 0);
        }
        public static bool ValidateIfDemoGraphicQuotaSaved(RemoteWebDriver driver)
        {
            Thread.Sleep(3000);
            if (CheckIfAddNewQuotaModalOpens(driver))
            {
                FillCommonQuotaDetails(driver, "Demo based quota", "2", "Any of");
                UIActions.SelectInCEBDropdownByText(driver, "[data-bind='SourceType']", "Demographic");
                UIActions.SelectInCEBDropdownByText(driver, "[name='select-sourceQuestion']", "Gender");
                Thread.Sleep(3000);
                UIActions.SelectInCEBDropdownByText(driver, "[name='select-condition']", "Equal To");
                UIActions.SelectInCEBDropdownByText(driver, "[name='select-logic']", "Exactly");
                UIActions.SelectInCEBDropdownByText(driver, "[name='select-Response']", "Female");
                var saveQuotaLogicButton = UIActions.GetElement(driver, "#quota-save-LogicBuilder");
                saveQuotaLogicButton.Click();
                Thread.Sleep(10000);
                return ValidateIfQuotaIsSaved(driver, "Demo based quota", "Gender = Female", 6);
            }
            return false;
        }

        public static bool ValidateIfAuthenticationTypeQuotaSaved(RemoteWebDriver driver)
        {
            Thread.Sleep(3000);
            if (CheckIfAddNewQuotaModalOpens(driver))
            {
                FillCommonQuotaDetails(driver, "Authentication Type based quota", "2", "All of");
                UIActions.SelectInCEBDropdownByText(driver, "[data-bind='SourceType']", "Authentication Type");
                UIActions.SelectInCEBDropdownByText(driver, "[name='select-logic']", "Exactly");
                UIActions.SelectInCEBDropdownByText(driver, "[name='select-Response']", "Open Link");
                var saveQuotaLogicButton = UIActions.GetElement(driver, "#quota-save-LogicBuilder");
                saveQuotaLogicButton.Click();
                Thread.Sleep(10000);
                return ValidateIfQuotaIsSaved(driver, "Authentication Type based quota", "Authentication Type = Open Link", 12);
            }
            return false;
        }

        public static bool ValidateIfOverallResponseQuotaSaved(RemoteWebDriver driver)
        {
            Thread.Sleep(3000);
            if (CheckIfAddNewQuotaModalOpens(driver))
            {
                FillCommonQuotaDetails(driver, "Overall Response based quota", "2", "All of");
                UIActions.SelectInCEBDropdownByText(driver, "[data-bind='SourceType']", "Overall Response");
                var saveQuotaLogicButton = UIActions.GetElement(driver, "#quota-save-LogicBuilder");
                saveQuotaLogicButton.Click();
                Thread.Sleep(10000);
                return ValidateIfQuotaIsSaved(driver, "Overall Response based quota", "Over All Response Count", 18);
            }
            return false;
        }

        public static bool CheckIfQuotaButtonExist(RemoteWebDriver driver)
        {
            return (UIActions.GetElement(driver, "#quotaConfigurationButton") != null);
        }

        public static IWebElement FindDatePickerInAddEditParticipant(IWebDriver driver, IWebElement label)
        {
            var datepicker = label.FindElement(By.XPath(".."))
                    .FindElement(By.XPath(".."))
                    .FindElement(By.XPath(".."))
                    .FindElement(By.CssSelector("div.col-sm-6 span.input-group-btn button.demo-date-btn"));
            return datepicker != null ? (datepicker.Displayed ? datepicker : null) : datepicker;
        }
        public static void AddParticipantsFromDirectoryTab(IWebDriver driver, int NoofParticipants)
        {
            try
            {
                Thread.Sleep(3000);
                IWebElement element = UIActions.GetElementwithXpath(driver,
                        "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                Actions action = new Actions(driver);
                Thread.Sleep(1000);
                action.MoveToElement(element).Click().Perform();
                element.Click();
                IWebElement AddParticipantButton =
                    UIActions.GetElement(driver, "[widget-data-event='click::AddParticipantButtonClick']");
                AddParticipantButton.Click();
                Thread.Sleep(1000);
                var Participants = driver.FindElements(By.XPath("(//*[@class='select-checkbox width-minimum padding-left-20 padding-right-20 sorting_1'])"));
                for (int i = 0; i < NoofParticipants; i++)
                {
                    Participants[i].Click();
                }
                IWebElement AddParticipant = UIActions.FindElement(driver, "#btn-saveDirectoryParticipants");
                Actions action1 = new Actions(driver);
                action1.MoveToElement(AddParticipant).Click().Perform();
                //AddParticipant.Click();
                Thread.Sleep(100000);
                IWebElement refreshparticipant = driver.FindElement(By.XPath("//*[@class='dotted-border-bottom btn-refresh-participant-grid']"));
                refreshparticipant.Click();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void NavigateToEditParticipantpopup(IWebDriver driver)
        {
            try
            {
                UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
                Thread.Sleep(3000);
                UIActions.Click(driver, "[widget-data-event='click::EditParticipant']");
                Thread.Sleep(4000);
                UIActions.clickwithXapth(driver, "//*[@class='btn btn-primary edit-fields-btn']");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void NavigateToParticipantGrid(IWebDriver driver)
        {
            Thread.Sleep(8000);
            UIActions.clickwithXapth(driver, "//label[contains(text(),'Share Manually')]");
            IWebElement element = UIActions.GetElementwithXpath(driver,
                    "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
            Actions action = new Actions(driver);
            Thread.Sleep(8000);
            action.MoveToElement(element).Click().Perform();
            UIActions.Click(driver, "[widget-data-event='click::SelectParticipantSectionForEdit']");
        }
        public static bool GetDemographicLabelInEditParticipantPopup(IWebDriver driver, string DemoLabel, String selector)
        {
            try
            {
                var DemographicTitles = driver.FindElements(By.CssSelector(selector));
                var content = DemographicTitles.FirstOrDefault(e => e.Displayed && e.Text.Trim() == DemoLabel);
                if (content != null)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool GetDemographicLabelInEditColumnsPopup(IWebDriver driver, string DemoLabel, String selector)
        {
            try
            {
                var DemographicTitles = driver.FindElements(By.XPath(selector));
                var content = DemographicTitles.FirstOrDefault(e => e.Displayed && e.Text.Trim() == DemoLabel);
                if (content != null)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool ValidateParticipantExport(IWebDriver driver, string statusMessage, string filePath, string sourcePath)
        {
            try
            {
                var anchorElement = driver.FindElements(By.CssSelector("#import-tracker-table a"))
                    .FirstOrDefault(e => e.Displayed && e.Text.Trim() == statusMessage);
                anchorElement?.Click();
                Thread.Sleep(2000);
                string latestfile = DistributeActions.getLastDownloadedFile(filePath);
                CommonActions.CompareExcels(sourcePath, filePath + @"\" + latestfile, filePath);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static void ClickonExport(IWebDriver driver)
        {
            UIActions.clickwithXapth(driver, "//*[@class='btn btn-secondary participant-grid-export-button btn-export-data']");
            Thread.Sleep(40000);
            if (UIActions.IsElementVisible(driver, "//*[@class='download-export-link']"))
                UIActions.clickwithXapth(driver, "//*[@class='download-export-link']");
        }
        public static bool ExcelVerificationofDemoLabel(IWebDriver driver, string s, string DemoLabel)
        {
            bool Flag = CommonActions.CheckExcelContains(@"C:\workspace\Tellurium\DataSet\" + s, DemoLabel);
            return Flag;
        }
    }
}






