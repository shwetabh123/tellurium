﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace Common
{
    public class ReportDataValidation
    {
        public static DataSet ReportFilters { get; set; }
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";

        public static void Log(string logMessage, TextWriter w)
        {
            //w.Write("\r\nLog Entry : ");
            w.Write("\r\n ");
            w.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.Write("\t:\t");
            w.Write("{0}", logMessage);
            //w.WriteLine("-------------------------------");
        }
        public static DataSet GetReportFilter(string UserAnalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {
            string InputQuery = "EXEC dbo.QAPROC_GetFilterResults " + UserAnalyzeFilterID;
            DataSet ds = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + InputQuery, w);
            }
            return ds;
        }
        public static string GetTestSPName(string ReportName, string ItemOrCategory)
        {
            string TestSPName = "";
            if (ReportName.ToLower() == "results")
            {
                TestSPName = "dbo.Parent_SP_QAPROC_ResultsReport_V11";
            }
            else if (ReportName.ToLower() == "insights" || ReportName.ToLower() == "drilldown")
            {
                if (ItemOrCategory == "2")
                    TestSPName = "dbo.QAPROC_ReportCalculationQuestions_V11";
                else
                    TestSPName = "dbo.Parent_SP_QAPROC_ReportCalculationCategory_V11";
            }
            else if (ReportName.ToLower() == "demographic")
            {
                if (ItemOrCategory == "2")
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcCategory_V11";
                else
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcQuestions_V11";
            }
            else if (ReportName.ToLower() == "comment")
            {
                if (ItemOrCategory == "2")
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcCategory_V11";
                else
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcQuestions_V11";
            }
            return TestSPName;
        }
        public static DataSet GetTestScenarios(DBConnectionStringDTO DBConnectionParameters, string CategoryOrQuestion = "Category", string DataCalculation = "Percentage")
        {
            string InputQuery;
            string ResultDisplay;
            string FavOrMean;
            if (CategoryOrQuestion.ToLower() == "category")
                ResultDisplay = "1";
            else
                ResultDisplay = "2";
            if (DataCalculation.ToLower() == "percentage")
                FavOrMean = "1";
            else
                FavOrMean = "2";
            InputQuery = "EXEC dbo.QAPROC_GetTestScenarios_V11" + " " + ResultDisplay + " ," + FavOrMean;
            DataSet ds = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ds;
        }
        public static DataSet GetTestSPIDFromScenarios(DataRow row, DBConnectionStringDTO DBConnectionParameters, string CompletionRuleFlag = "CompletedTheSurvey")
        {
            string UserID = row[0].ToString() + ", ";
            string ClientID = row[1].ToString() + ", ";
            string SurveyFormID = row[2].ToString() + ", ";
            string CompletionRule = "'" + CompletionRuleFlag + "'";

            string InputQuery = "EXEC dbo.QAPROC_GetTestSPIDFromScenarios_V11 " + UserID + ClientID + SurveyFormID + CompletionRule;
            DataSet ds = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ds;
        }

        public static DataSet GetBenchmarkData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters)
        {
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'";
            string BenchmarkId = "'" + ReportFilters.Tables[0].Rows[0][6].ToString() + "'" + ", ";
            string TestSpQuery = "Exec dbo.BenchMarkCalculation_V11 " + BenchmarkId + SurveyIDs;
            DataSet BenchmarkData = DBOperations.ExecuteSPwithInputParameters(TestSpQuery, DBConnectionParameters);
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + TestSpQuery, w);
            }
            return BenchmarkData;
        }

        public static DataSet GetBenchmarkDataForTrend_NR(DataSet ReportFilters, string itemID, string ScaleOptionID,DBConnectionStringDTO DBConnectionParameters)
        {
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'"+",";
            string BenchmarkId = "'" + ReportFilters.Tables[0].Rows[0][6].ToString() + "'" + ", ";
            string surveyformitemId = "'" + itemID + "'" + ",";
            string scaleOptionId = "'" + ScaleOptionID + "'";
            string TestSpQuery = "Exec dbo.BenchMarkCalculationTrend_NR_V11 " + BenchmarkId + SurveyIDs+ surveyformitemId+ scaleOptionId;
            DataSet BenchmarkData = DBOperations.ExecuteSPwithInputParameters(TestSpQuery, DBConnectionParameters);
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + TestSpQuery, w);
            }
            return BenchmarkData;
        }
        public static DataSet GetBenchmarkDataForTrend(DataSet ReportFilters, string ItemID, string CategoryID, DBConnectionStringDTO DBConnectionParameters)
        {
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0]["SurveyIDs"].ToString() + "'" + ", ";
            string BenchmarkId = "'" + ReportFilters.Tables[0].Rows[0]["BenchMarkID"].ToString() + "'" + ", ";
            string SurveyformItemID;
            string SurveyCategoryID;
            if (ItemID == "1=1")
                SurveyformItemID = "'" + ItemID + "'" + ", ";
            else
                SurveyformItemID = "'SurveyFormItemID IN (" + ItemID + ")'" + ", ";
            if (CategoryID == "1=1")
                SurveyCategoryID = "'" + CategoryID + "'";
            else
                SurveyCategoryID = "'SurveyCategoryID IN (" + CategoryID + ")'";
            string TestSpQuery = "Exec BenchMarkCalculationTrend_V11 " + BenchmarkId + SurveyIDs + SurveyformItemID + SurveyCategoryID;
            DataSet BenchmarkData = DBOperations.ExecuteSPwithInputParameters(TestSpQuery, DBConnectionParameters);
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + TestSpQuery, w);
            }
            return BenchmarkData;
        }
        public static DataSet GetResultReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters)
        {
            string SurveyFormID = ReportFilters.Tables[0].Rows[0][0].ToString() + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'";
            string TestSPQuery = "EXEC dbo.Parent_SP_QAPROC_ResultsReport_V11 " + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            return ResultData;
        }
        public static DataSet GetInsightReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, string ReportDisplay)
        {
            string TestSPName;
            string SurveyFormID = "'" + ReportFilters.Tables[0].Rows[0]["SurveyFormID"].ToString() + "'" + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0]["SurveyIDs"].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0]["DemoFilter"].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0]["TimePeriodFilter"].ToString() + "'";
            if (ReportDisplay.ToLower() == "category")
                TestSPName = "dbo.Parent_SP_QAPROC_ReportCalculationCategory_V11 ";
            else
                TestSPName = "dbo.QAPROC_ReportCalculationQuestions_V11 ";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + TestSPQuery, w);
            }
            return ResultData;
        }
        public static DataSet GetDemographicReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, string ReportDisplay)
        {
            string TestSPName;
            string SurveyFormID = ReportFilters.Tables[0].Rows[0][0].ToString() + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'";
            if (ReportDisplay.ToLower() == "category")
                TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcCategory_V11 ";
            else
                TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcQuestions_V11 ";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            return ResultData;
        }
        public static DataSet GetCommentReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, string CategoryText)
        {
            int SurveyFormItemId = 46717;
            string TestSPName;
            string SurveyFormID = ReportFilters.Tables[0].Rows[0][0].ToString() + ", ";
            string SurveyFormID2 = ReportFilters.Tables[0].Rows[0][0].ToString();
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'" + ", ";
            string CommentCategoryText = "'" + CategoryText + "'" + ", ";
            string query = "SELECT COUNT(*) FROM DimItem_V11 WHERE  SurveyFormID = " + SurveyFormID2 + " and CategoryText = '" + CategoryText + "' AND GroupName = 'RatingScale'  AND CategoryText != 'Unassigned'";
            DataSet rscount = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);
            if (rscount.Tables[0].Rows[0][0].ToString() == "0")
                TestSPName = "dbo.QAPROC_CommentReportCalc_UnScored_V11 ";
            else
                TestSPName = "dbo.QAPROC_CommentReportCalc_V11 ";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery + CommentCategoryText + "@SurveyFormItemId = " + "'" + SurveyFormItemId + "'";
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            return ResultData;
        }
        public static DataSet GetDrillDownReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, String ReportDisplay)
        {
            string TestSPName;
            string SurveyFormID = "'" + ReportFilters.Tables[0].Rows[0][0].ToString() + "'" + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'";
            if (ReportDisplay.ToLower() == "category")
                TestSPName = "dbo.Parent_SP_QAPROC_ReportCalculationCategory_V11";
            else
                TestSPName = "dbo.QAPROC_ReportCalculationQuestions_V11";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;

            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);

            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + TestSPQuery, w);
            }
            return ResultData;
        }
        public static DataSet GetTrendReportDataForRatingScale(DataSet ReportFilters, String GroupBy, String Show, String ItemorCategory, DBConnectionStringDTO DBConnectionParameters)
        {
            string TestSPName;
            DataSet ResultData;
            string TestSPQuery;
            string SurveyFormID = "'" + ReportFilters.Tables[0].Rows[0]["SurveyFormID"].ToString() + "'" + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0]["SurveyIDs"].ToString() + "'";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0]["DemoFilter"].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0]["TimePeriodFilter"].ToString() + "'" + ", ";
            string IsTimePeriodDateDemoEnabled = ReportFilters.Tables[0].Rows[0]["IsTimePeriodDateDemoEnabled"].ToString() + ", ";
            string ReportDisplay = ReportFilters.Tables[0].Rows[0]["ResultDisplay"].ToString();
            string ShowItemorCategory;
            if (Show.ToLower() == "item")
            {
                ShowItemorCategory = "'SurveyFormItemID IN (" + ItemorCategory + ")' , ";
            }
            else if (Show.ToLower() == "category")
            {
                ShowItemorCategory = "'SurveyCategoryID IN (" + ItemorCategory + ")' ,";
            }
            else
            {
                ShowItemorCategory = "'1 = 1', ";
            }
            switch (GroupBy.ToLower())
            {
                case "year":
                    TestSPName = "[dbo].[TrendSPExecution_Year] ";
                    break;
                case "quarter":
                    TestSPName = "[dbo].[TrendSPExecution_Quarter] ";
                    break;
                case "month":
                    TestSPName = "[dbo].[TrendSPExecution_Month] ";
                    break;
                default:
                    TestSPName = "[dbo].[TrendSPExecution_Distribution] ";
                    break;
            }
            TestSPQuery = "EXEC " + TestSPName + SurveyFormID + DemoFilterQuery + TimePeriodQuery + ShowItemorCategory + IsTimePeriodDateDemoEnabled + SurveyIDs;
            ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + TestSPQuery, w);
            }
            return ResultData;
        }
        public static DataSet GetTrendReportDataForNonRatingScale(DataSet ReportFilters, string GroupBy, string SurveyFormItemID, DBConnectionStringDTO DBConnectionParameters, string Show = null)
        {
            string TestSPName;
            DataSet ResultData;
            string TestSPQuery;
            string SurveyFormID = ReportFilters.Tables[0].Rows[0]["SurveyFormID"].ToString() + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0]["SurveyIDs"].ToString() + "'";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0]["DemoFilter"].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0]["TimePeriodFilter"].ToString() + "'" + ", ";
            string IsTimePeriodDateDemoEnabled = ReportFilters.Tables[0].Rows[0]["IsTimePeriodDateDemoEnabled"].ToString() + ", ";
            string ReportDisplay = ReportFilters.Tables[0].Rows[0]["ResultDisplay"].ToString();

            switch (GroupBy.ToLower())
            {
                case "year":
                    TestSPName = "[dbo].[TrendSPExecution_NR_Year] ";
                    break;
                case "quarter":
                    TestSPName = "[dbo].[TrendSPExecution_NR_Quarter] ";
                    break;
                case "month":
                    TestSPName = "[dbo].[TrendSPExecution_NR_Month] ";
                    break;
                default:
                    TestSPName = "[dbo].[TrendSPExecution_NR_Distribution] ";
                    break;
            }
            TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyFormItemID + ", " + DemoFilterQuery + TimePeriodQuery + IsTimePeriodDateDemoEnabled + SurveyIDs;
            ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Input Query : " + TestSPQuery, w);
            }
            return ResultData;
        }
        public static DataSet GetActualSPReportData(DataRow row, DBConnectionStringDTO DBConnectionParameters, string ReportSPName)
        {
            //string UserFilter = " ";
            string UserID = row[0].ToString() + ", ";
            string ClientID = row[1].ToString() + ", ";
            string SurveyFormID = row[2].ToString();
            string InputQuery = "EXEC dbo." + ReportSPName + " " + "@UserID=" + UserID + "@ClientID=" + ClientID + "@SurveyFormID=" + SurveyFormID;
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Report SP : " + InputQuery, w);
            }
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ResultSet;
        }
        public static DataSet GetActualSPTrendReportData(DataRow row, DBConnectionStringDTO DBConnectionParameters, string ReportSPName, string show, string ItemorCategory, string GroupBy)
        {
            string UserID = "@userID = " + row[0].ToString() + ", ";
            string ClientID = "@clientID = " + row[1].ToString() + ", ";
            string SurveyFormID = "@surveyFormID = " + row[2].ToString() + ", ";
            string ShowCategory = "";
            string ShowItem = "";
            if (show.ToLower() == "category")
            {
                ShowCategory = "@surveycategoryfilter=" + ItemorCategory + ", ";
                ShowItem = "@surveyformitemfilter='', ";
            }
            else if (show.ToLower() == "item")
            {
                ShowItem = "@surveyformitemfilter=" + ItemorCategory + ", ";
                ShowCategory = "@surveycategoryfilter='', ";
            }
            else
            {
                ShowCategory = "@surveycategoryfilter='', ";
                ShowItem = "@surveyformitemfilter='', ";
            }
           // string IsTimePeriodDateDemoEnabled = "@IsTimePeriodDateDemoEnabled = " + row["IsTimePeriodDateDemoEnabled"].ToString() + ", ";
            string GroupByFilter = "@GroupByFilter = '" + GroupBy + "'";

            string InputQuery = "EXEC dbo." + ReportSPName + " " + UserID + ClientID + SurveyFormID + ShowCategory + ShowItem + GroupByFilter;
            using (StreamWriter w = File.AppendText(LogFileName))
            {
                Log("Report SP : " + InputQuery, w);
            }
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ResultSet;
        }

        public static DataSet GetActualSPNonRatingTrendReportData(DataRow row, DBConnectionStringDTO DBConnectionParameters, string ReportSPName, string SurveyformItem, string scaleRange, string GroupBy)
        {
            string UserID = "@UserId=" + row[0].ToString() + ", ";
            string ClientID = "@ClientId=" + row[1].ToString() + ", ";
            string SurveyFormID = "@SurveyFormId=" + row[2].ToString() + ", ";
            string SurveyFormItem = "@SurveyFormItemId=" + SurveyformItem + " ,";
            string ScaleOptions = "@ScaleOptionIds=" + scaleRange + " ,";
            string GroupByFilter = "@GroupByFilter = '" + GroupBy + "'";
            string InputQuery = "EXEC dbo." + ReportSPName + " " + UserID + ClientID + SurveyFormID + SurveyFormItem + ScaleOptions + GroupByFilter;
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ResultSet;
        }


        public static void WriteTestExecutionResult(string Datacalcualtion, string reportname, int UseranalyzeFilterID, bool Status, DBConnectionStringDTO DBConnectionParameters)
        {
            string InputQuery = "Insert into WriteTestExecutionResult values (" + "'" + Datacalcualtion + "'" + "," + "'" + reportname + "'" + "," + UseranalyzeFilterID + "," + "'" + Status + "'" + ")";
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
        }
        public static void WriteFailedData(string Datacalcualtion, string reportname, int UseranalyzeFilterID, int groupType, DBConnectionStringDTO DBConnectionParameters, string ScaleOptionID = "", string CommentText = "", int CommentRowNumber = 0)
        {

            string InputQuery = "Insert into Writelog values (" + "'" + Datacalcualtion + "'" + "," + "'" + reportname + "'" + "," + UseranalyzeFilterID + "," + groupType + "," + "'" + ScaleOptionID + "'" + "," + "'" + CommentText + "'" + "," + CommentRowNumber + ")";
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);

        }


        public static void WriteFailedDatacomment(string Datacalculation, string ReportSPName, int UseranalyzeFilterID, DBConnectionStringDTO DBConnectionParameters, int groupType = 0, string ScaleOptionID = "", string CommentText = "", int CommentRowNumber = 0)
        {

            string InputQuery = "Insert into Writelog values(" + "'" + Datacalculation + "'" + "," + "'" + ReportSPName + "'" + "," + UseranalyzeFilterID + "," + groupType + "," + "'" + ScaleOptionID + "'" + "," + "'" + CommentText + "'" + "," + CommentRowNumber + ")";

            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);

        }

        public static bool CompareResultReportBenchmarkResults(string DataCalculation, DataSet ActualSP, DataSet CompGrp, string CompType, string reportname, int UseranalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {
            bool RSbbflag = true;
            bool NRbbflag = true;

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN(1,2) AND ScaleoptionID IS NULL";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrpRatingScaleTable;
            DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
            DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[0]);
            Compdv.Sort = "SurveyFormItemID ASC";
            CompGrpRatingScaleTable = Compdv.ToTable();

            var ActSurveyformItemID = ActualSPRatingScaleTable.Columns["ItemID"];
            var TrgSurveyformItemID = CompGrpRatingScaleTable.Columns["SurveyFormItemID"];
            int groupType = Convert.ToInt32(CompType.Substring(CompType.Length - 1, 1));

            if (DataCalculation.ToLower() == "average")
            {
                string benchmarkcomparison = "" + CompType + "AverageScore";
                string benchmarkTotalresponse = "" + CompType + "TotalResponse";
                var ActualSPBenchmarkAverageCol = ActualSPRatingScaleTable.Columns[benchmarkcomparison];
                var ActualSPBenchmarkCountCol = ActualSPRatingScaleTable.Columns[benchmarkTotalresponse];
                var CompGrpBenchmarkAverageCol = CompGrpRatingScaleTable.Columns["Mean"];
                var CompGrpvBenchmarkCountCol = CompGrpRatingScaleTable.Columns["Total Responses"];

                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[ActSurveyformItemID].ToString() == RowTbl2[TrgSurveyformItemID].ToString())
                        {

                            if (RowTbl1[ActualSPBenchmarkAverageCol].ToString() != RowTbl2[CompGrpBenchmarkAverageCol].ToString() || RowTbl1[ActualSPBenchmarkCountCol].ToString() != RowTbl2[CompGrpvBenchmarkCountCol].ToString())
                            {
                                string ReportNames = "RatingScaleBenchmarkValue";

                                RSbbflag = false;
                                WriteFailedData(DataCalculation, ReportNames, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                string benchmarkcomparisonFav = "" + CompType + "FavPercent";
                string benchmarkcomparisonNeu = "" + CompType + "NeutralPercent";
                string benchmarkcomparisonUnFav = "" + CompType + "UnFavPercent";
                var ActualSPBenchmarkFavCol = ActualSPRatingScaleTable.Columns[benchmarkcomparisonFav];
                var ActualSPBenchmarkNeuCol = ActualSPRatingScaleTable.Columns[benchmarkcomparisonNeu];
                var ActualSPBenchmarkUnFavCol = ActualSPRatingScaleTable.Columns[benchmarkcomparisonUnFav];


                var CompGrpBenchmarkFavCol = CompGrpRatingScaleTable.Columns["BenchmarkFavperc"];
                var CompGrpBenchmarkNeuCol = CompGrpRatingScaleTable.Columns["Neuperc"];
                var CompGrpBenchmarkUnFavCol = CompGrpRatingScaleTable.Columns["UnFavperc"];


                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[ActSurveyformItemID].ToString() == RowTbl2[TrgSurveyformItemID].ToString())
                        {
                            if (RowTbl1[ActualSPBenchmarkFavCol].ToString() != RowTbl2[CompGrpBenchmarkFavCol].ToString() || RowTbl1[ActualSPBenchmarkNeuCol].ToString() != RowTbl2[CompGrpBenchmarkNeuCol].ToString() || RowTbl1[ActualSPBenchmarkUnFavCol].ToString() != RowTbl2[CompGrpBenchmarkUnFavCol].ToString())
                            {
                                string ReportNames = "RatingScaleBenchmarkValue";
                                RSbbflag = false;
                                WriteFailedData(DataCalculation, ReportNames, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }

            }

            DataTable ActualSPNonRatingScaleTable;
            DataViewManager ActualNRdvm = ActualSP.DefaultViewManager;
            DataView ActualNRDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualNRDv.RowFilter = "ItemType IN(3,4)";
            ActualNRDv.Sort = "ItemID,ScaleOptionID ASC";
            ActualSPNonRatingScaleTable = ActualNRDv.ToTable();

            DataTable CompNonRatingScaleTable;
            DataViewManager CompNRdvm = CompGrp.DefaultViewManager;
            DataView CompNRDv = CompNRdvm.CreateDataView(CompGrp.Tables[1]);
            CompNRDv.Sort = "SurveyFormItemID, BenchmarkScaleOptionId ASC";
            CompNonRatingScaleTable = CompNRDv.ToTable();

            string benchmarkNonRatingscalecomparison = "" + CompType + "AverageScore";
            var ActualNRScaleOptionIDCol = ActualSPNonRatingScaleTable.Columns["ScaleOptionID"];
            var ActualNRBenchmarkValueCol = ActualSPNonRatingScaleTable.Columns[benchmarkNonRatingscalecomparison];

            var CompNRScaleOptionIDCol = CompNonRatingScaleTable.Columns["BenchmarkScaleOptionId"];
            var CompNRBenchmarkValueCol = CompNonRatingScaleTable.Columns["RespPerc"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
            {
                foreach (DataRow RowTbl2 in CompNonRatingScaleTable.Rows)
                {
                    if ((RowTbl1[ActualNRScaleOptionIDCol].ToString()) == (RowTbl2[CompNRScaleOptionIDCol].ToString()))
                    {
                        string TrgNonRatingScaleAverageScore = RowTbl2[CompNRBenchmarkValueCol].ToString() + ".00";
                        if (RowTbl1[ActualNRBenchmarkValueCol].ToString() != TrgNonRatingScaleAverageScore)
                        {
                            string ReportNames = "NonRatingScaleBenchmarkValue";
                            NRbbflag = false;
                            WriteFailedData(DataCalculation, ReportNames, UseranalyzeFilterID, groupType, DBConnectionParameters, RowTbl2[CompNRScaleOptionIDCol].ToString());
                            break;
                        }
                    }
                }
            }

            return RSbbflag && NRbbflag;
        }



        public static bool CompareInsightReportBenchmarkResults(string DataCalculation, DataSet ActualSP, DataSet CompGrp, int CompGrpPos)
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("Inside CompareInsightReportBenchmarkResults method", w);
                }
                bool Strbbflag = true;
                bool Oppbbflag = true;

                DataTable ActualSPRatingScaleStrTable;
                DataViewManager ActualStrdvm = ActualSP.DefaultViewManager;
                DataView ActualStrDv = ActualStrdvm.CreateDataView(ActualSP.Tables[0]);
                ActualStrDv.Sort = "ObjectId ASC";
                ActualSPRatingScaleStrTable = ActualStrDv.ToTable();

                string objecttype;

                DataTable ActualSPRatingScaleOppTable;
                DataViewManager ActualOppdvm = ActualSP.DefaultViewManager;
                DataView ActualOppDv = ActualOppdvm.CreateDataView(ActualSP.Tables[1]);
                ActualOppDv.Sort = "ObjectId ASC";
                ActualSPRatingScaleOppTable = ActualOppDv.ToTable();

                if (ActualSPRatingScaleStrTable.Rows.Count != 0)
                    objecttype = ActualSPRatingScaleStrTable.Rows[0][0].ToString();
                else
                    objecttype = ActualSPRatingScaleOppTable.Rows[0][0].ToString();

                string ActualSPBenchCompMeanCol;
                string ActualSPBenchCompFavCol;

                if (CompGrpPos == 1)
                {
                    ActualSPBenchCompMeanCol = "Comp_One_Mean_Diff";
                    ActualSPBenchCompFavCol = "Comp_One_FavPerc_Diff";
                }
                else if (CompGrpPos == 2)
                {
                    ActualSPBenchCompMeanCol = "Comp_Two_Mean_Diff";
                    ActualSPBenchCompFavCol = "Comp_Two_FavPerc_Diff";
                }
                else
                {
                    ActualSPBenchCompMeanCol = "Comp_Three_Mean_Diff";
                    ActualSPBenchCompFavCol = "Comp_Three_FavPerc_Diff";
                }

                if (objecttype.ToLower() == "item")
                {
                    DataTable CompGrpRatingScaleItemTable;
                    DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                    DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[0]);
                    Compdv.Sort = "SurveyFormItemID ASC";
                    CompGrpRatingScaleItemTable = Compdv.ToTable();

                    if (DataCalculation.ToLower() == "average")
                    {
                        var ActualSPBenchmarkStrAverageCol = ActualSPRatingScaleStrTable.Columns[ActualSPBenchCompMeanCol];
                        var ActualSPStrObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                        var ActualSPStrMeanCol = ActualSPRatingScaleStrTable.Columns["MeanScore"];

                        var ActualSPBenchmarkOppAverageCol = ActualSPRatingScaleOppTable.Columns[ActualSPBenchCompMeanCol];
                        var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                        var ActualSPOppMeanCol = ActualSPRatingScaleOppTable.Columns["MeanScore"];

                        var CompSPBenchmarkMeanCol = CompGrpRatingScaleItemTable.Columns["Mean"];
                        var CompSPSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];

                        foreach (DataRow RowTbl1 in ActualSPRatingScaleStrTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleItemTable.Rows)
                            {
                                if ((RowTbl1[ActualSPStrObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    //var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPStrMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                    //if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkAverageCol]) != Diff_benchmark)
                                    //{
                                    //    Strbbflag = false;
                                    //    break;
                                    //}
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrAverageCol])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2))))
                                    {
                                        Strbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkStrAverageCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrAverageCol])), 2).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkStrAverageCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPRatingScaleOppTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleItemTable.Rows)
                            {
                                if ((RowTbl1[ActualSPOppObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    //var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                    //if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppAverageCol]) != Diff_benchmark)
                                    //{
                                    //    Oppbbflag = false;
                                    //    break;
                                    //}
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppAverageCol])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2))))
                                    {
                                        Oppbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkOppAverageCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppAverageCol])), 2).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkOppAverageCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var ActualSPBenchmarkStrFavCol = ActualSPRatingScaleStrTable.Columns[ActualSPBenchCompFavCol];
                        var ActualSPStrObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                        var ActualSPStrFavCol = ActualSPRatingScaleStrTable.Columns["FavPerc"];

                        var ActualSPBenchmarkOppFavCol = ActualSPRatingScaleOppTable.Columns[ActualSPBenchCompFavCol];
                        var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                        var ActualSPOppFavCol = ActualSPRatingScaleOppTable.Columns["FavPerc"];

                        var CompSPBenchmarkFavCol = CompGrpRatingScaleItemTable.Columns["BenchmarkFavPerc"];
                        var CompSPSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];

                        foreach (DataRow RowTbl1 in ActualSPRatingScaleStrTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleItemTable.Rows)
                            {
                                if ((RowTbl1[ActualSPStrObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    //var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                    //if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkFavCol]) != Diff_benchmark)
                                    //{
                                    //    Strbbflag = false;
                                    //    break;
                                    //}
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrFavCol])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0))))
                                    {
                                        Strbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkStrFavCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrFavCol])), 0).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkStrFavCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPRatingScaleOppTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleItemTable.Rows)
                            {
                                if ((RowTbl1[ActualSPOppObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    //var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                    //if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppFavCol]) != Diff_benchmark)
                                    //{
                                    //    Oppbbflag = false;
                                    //    break;
                                    //}
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppFavCol])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0))))
                                    {
                                        Oppbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkOppFavCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppFavCol])), 0).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkOppFavCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                    }

                }
                else
                {
                    DataTable CompGrpRatingScaleCategoryTable;
                    DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                    DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[2]);
                    Compdv.Sort = "ObjectID ASC";
                    CompGrpRatingScaleCategoryTable = Compdv.ToTable();

                    if (DataCalculation.ToLower() == "average")
                    {
                        var ActualSPBenchmarkStrAverageCol = ActualSPRatingScaleStrTable.Columns[ActualSPBenchCompMeanCol];
                        var ActualSPStrObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                        var ActualSPStrMeanCol = ActualSPRatingScaleStrTable.Columns["MeanScore"];

                        var ActualSPBenchmarkOppAverageCol = ActualSPRatingScaleOppTable.Columns[ActualSPBenchCompMeanCol];
                        var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                        var ActualSPOppMeanCol = ActualSPRatingScaleOppTable.Columns["MeanScore"];

                        var CompSPBenchmarkMeanCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkMean"];
                        var CompSPSurveyformItemIDCol = CompGrpRatingScaleCategoryTable.Columns["ObjectId"];

                        foreach (DataRow RowTbl1 in ActualSPRatingScaleStrTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleCategoryTable.Rows)
                            {
                                //if ((Rowtbl1[ActualSPObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                                //{
                                //    if (Rowtbl2[CompSPBenchmarkMeanCol].ToString() == null)
                                //    {
                                //        if ((Rowtbl1[ActualSPBenchmarkAverageCol].ToString()) != (Rowtbl2[CompSPBenchmarkMeanCol].ToString()))
                                //        {
                                //            Strbbflag = false;
                                //            break;
                                //        }
                                //    }
                                //    else
                                //    {
                                //        var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                //        if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkAverageCol]) != Diff_benchmark)
                                //        {
                                //            Strbbflag = false;
                                //            break;
                                //        }
                                //    }
                                //}
                                if ((RowTbl1[ActualSPStrObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrAverageCol])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2))))
                                    {
                                        Strbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkStrAverageCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrAverageCol])), 2).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkStrAverageCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPRatingScaleOppTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleCategoryTable.Rows)
                            {
                                //if ((Rowtbl1[ActualSPOppObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                                //{
                                //    if (Rowtbl2[CompSPBenchmarkMeanCol].ToString() == null)
                                //    {
                                //        if ((Rowtbl1[ActualSPBenchmarkOppAverageCol].ToString()) != (Rowtbl2[CompSPBenchmarkMeanCol].ToString()))
                                //        {
                                //            Oppbbflag = false;
                                //            break;
                                //        }
                                //    }
                                //    else
                                //    {
                                //        var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                //        if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppAverageCol]) != Diff_benchmark)
                                //        {
                                //            Oppbbflag = false;
                                //            break;

                                //        }
                                //    }
                                //}
                                if ((RowTbl1[ActualSPOppObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppAverageCol])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2))))
                                    {
                                        Oppbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkOppAverageCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppAverageCol])), 2).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkOppAverageCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppMeanCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkMeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var ActualSPBenchmarkStrFavCol = ActualSPRatingScaleStrTable.Columns[ActualSPBenchCompFavCol];
                        var ActualSPStrObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                        var ActualSPStrFavCol = ActualSPRatingScaleStrTable.Columns["FavPerc"];

                        var ActualSPBenchmarkOppFavCol = ActualSPRatingScaleOppTable.Columns[ActualSPBenchCompFavCol];
                        var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                        var ActualSPOppFavCol = ActualSPRatingScaleOppTable.Columns["FavPerc"];

                        var CompSPBenchmarkFavCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkFavPerc"];
                        var CompSPSurveyformItemIDCol = CompGrpRatingScaleCategoryTable.Columns["ObjectID"];

                        foreach (DataRow RowTbl1 in ActualSPRatingScaleStrTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleCategoryTable.Rows)
                            {
                                //if ((Rowtbl1[ActualSPObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                                //{
                                //    if (Rowtbl2[CompSPBenchmarkFavCol].ToString() == null)
                                //    {
                                //        if ((Rowtbl1[ActualSPBenchmarkFavCol].ToString()) != (Rowtbl2[CompSPBenchmarkFavCol].ToString()))
                                //        {
                                //            Strbbflag = false;
                                //            break;
                                //        }
                                //    }
                                //    else
                                //    {
                                //        var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                //        if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkFavCol]) != Diff_benchmark)
                                //        {
                                //            Strbbflag = false;
                                //            break;
                                //        }
                                //    }
                                //}
                                if ((RowTbl1[ActualSPStrObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrFavCol])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0))))
                                    {
                                        Strbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkStrFavCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkStrFavCol])), 0).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkStrFavCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPStrFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPRatingScaleOppTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrpRatingScaleCategoryTable.Rows)
                            {
                                //if ((Rowtbl1[ActualSPOppObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                                //{
                                //    if (Rowtbl2[CompSPBenchmarkFavCol].ToString() == null)
                                //    {
                                //        if ((Rowtbl1[ActualSPBenchmarkOppFavCol].ToString()) != (Rowtbl2[CompSPBenchmarkFavCol].ToString()))
                                //        {
                                //            Oppbbflag = false;
                                //            break;
                                //        }
                                //    }
                                //    else
                                //    {
                                //        var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                //        if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppFavCol]) != Diff_benchmark)
                                //        {
                                //            Oppbbflag = false;
                                //            break;
                                //        }
                                //    }
                                //}
                                if ((RowTbl1[ActualSPOppObjectIdCol].ToString()) == (RowTbl2[CompSPSurveyformItemIDCol].ToString()))
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppFavCol])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0))))
                                    {
                                        Oppbbflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + ActualSPBenchmarkOppFavCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPBenchmarkOppFavCol])), 0).ToString(), w);
                                            Log("RowTbl2[" + ActualSPBenchmarkOppFavCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPOppFavCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompSPBenchmarkFavCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }


                    }

                }

                return Oppbbflag && Strbbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareDrillDownReportBenchmarkResults(string DataCalculation, DataSet ActualSP, DataSet CompGrp, string CompType, string reportname, int UseranalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {
            bool bbflag = true;
            string Columnname;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
            {
                Columnname = "SurveyFormItemID";
            }
            else
            {
                Columnname = "CategoryID";
            }
            int groupType = Convert.ToInt32(CompType.Substring(CompType.Length - 1, 1));

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[0]);
            //ActualDv.RowFilter = "CategoryText Not IN('Unassigned')";
            ActualDv.Sort = Columnname + " ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            if (Columnname.ToLower() == "surveyformitemid")
            {

                DataTable CompGrpRatingScaleItemTable;
                DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[0]);
                Compdv.Sort = "SurveyFormItemID ASC";
                CompGrpRatingScaleItemTable = Compdv.ToTable();
                if (DataCalculation.ToLower() == "average")
                {
                    string benchmarkcomparison = "" + CompType + "DiffAverageScore";
                    var ActualSPBenchmarkAverageValueCol = ActualSPRatingScaleTable.Columns[benchmarkcomparison];
                    var ActualSpSurveyformItemIDCol = ActualSPRatingScaleTable.Columns["SurveyFormItemID"];
                    var ActualSPAverageScoreCol = ActualSPRatingScaleTable.Columns["AverageScore"];

                    var CompSPBenchmarkAverageValueCol = CompGrpRatingScaleItemTable.Columns["Mean"];
                    var CompSpSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrpRatingScaleItemTable.Rows)
                        {
                            if ((RowTbl1[ActualSpSurveyformItemIDCol].ToString()) == (RowTbl2[CompSpSurveyformItemIDCol].ToString()))
                            {
                                if ((RowTbl2[CompSPBenchmarkAverageValueCol] == DBNull.Value) || (RowTbl1[ActualSPBenchmarkAverageValueCol] == DBNull.Value))
                                {
                                    if ((RowTbl1[ActualSPBenchmarkAverageValueCol].ToString()) != (RowTbl2[CompSPBenchmarkAverageValueCol].ToString()))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }
                                else
                                {

                                    var Diff_benchmark = Convert.ToDecimal(RowTbl1[ActualSPAverageScoreCol]) - Convert.ToDecimal(RowTbl2[CompSPBenchmarkAverageValueCol]);
                                    if (Convert.ToDecimal(RowTbl1[ActualSPBenchmarkAverageValueCol]) != Convert.ToDecimal(Diff_benchmark))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }

                    }


                }
                else
                {
                    string benchmarkcomparison = "" + CompType + "DiffFavPercent";
                    var ActualSPBenchmarkFavPercCol = ActualSPRatingScaleTable.Columns[benchmarkcomparison];
                    var ActualSpSurveyformItemIDCol = ActualSPRatingScaleTable.Columns["SurveyFormItemID"];
                    var ActualSPFavPercCol = ActualSPRatingScaleTable.Columns["FavPercent"];

                    var CompSPBenchmarkFavPercCol = CompGrpRatingScaleItemTable.Columns["BenchmarkFavPerc"];
                    var CompSpSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];

                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrpRatingScaleItemTable.Rows)
                        {
                            if ((RowTbl1[ActualSpSurveyformItemIDCol].ToString()) == (RowTbl2[CompSpSurveyformItemIDCol].ToString()))
                            {
                                if ((RowTbl2[CompSPBenchmarkFavPercCol] == DBNull.Value) || (RowTbl1[ActualSPBenchmarkFavPercCol] == DBNull.Value))
                                {
                                    if ((RowTbl1[ActualSPBenchmarkFavPercCol].ToString()) != (RowTbl2[CompSPBenchmarkFavPercCol].ToString()))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_benchmark = Convert.ToInt32(RowTbl1[ActualSPFavPercCol]) - Convert.ToInt32(RowTbl2[CompSPBenchmarkFavPercCol]);
                                    if (Convert.ToInt32(RowTbl1[ActualSPBenchmarkFavPercCol]) != Convert.ToInt32(Diff_benchmark))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }

            }
            else
            {
                DataTable CompGrpRatingScaleCategoryTable;
                DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[2]);
                Compdv.Sort = "ObjectID ASC";
                CompGrpRatingScaleCategoryTable = Compdv.ToTable();

                if (DataCalculation.ToLower() == "average")
                {
                    string benchmarkcomparison = "" + CompType + "DiffAverageScore";
                    var ActualSPBenchmarkAverageValueCol = ActualSPRatingScaleTable.Columns[benchmarkcomparison];
                    var ActualSpCategoryIDCol = ActualSPRatingScaleTable.Columns["CategoryID"];
                    var ActualSPAverageScoreCol = ActualSPRatingScaleTable.Columns["AverageScore"];

                    var CompSPBenchmarkAverageValueCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkMean"];
                    var CompSpCategoryIDCol = CompGrpRatingScaleCategoryTable.Columns["ObjectID"];

                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrpRatingScaleCategoryTable.Rows)
                        {
                            if ((RowTbl1[ActualSpCategoryIDCol].ToString()) == (RowTbl2[CompSpCategoryIDCol].ToString()))
                            {
                                if ((RowTbl2[CompSPBenchmarkAverageValueCol] == DBNull.Value) || (RowTbl1[ActualSPBenchmarkAverageValueCol] == DBNull.Value))
                                {
                                    if ((RowTbl1[ActualSPBenchmarkAverageValueCol].ToString()) != (RowTbl2[CompSPBenchmarkAverageValueCol].ToString()))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_benchmark = Convert.ToDecimal(RowTbl1[ActualSPAverageScoreCol]) - Convert.ToDecimal(RowTbl2[CompSPBenchmarkAverageValueCol]);
                                    if (Convert.ToDecimal(RowTbl1[ActualSPBenchmarkAverageValueCol]) != Convert.ToDecimal(Diff_benchmark))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    string benchmarkcomparison = "" + CompType + "DiffFavPercent";
                    var ActualSPBenchmarkFavPercCol = ActualSPRatingScaleTable.Columns[benchmarkcomparison];
                    var ActualSpCategoryIDCol = ActualSPRatingScaleTable.Columns["CategoryID"];
                    var ActualSPFavPercCol = ActualSPRatingScaleTable.Columns["FavPercent"];

                    var CompSPBenchmarkFavPercCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkFavPerc"];
                    var CompSpCategoryCol = CompGrpRatingScaleCategoryTable.Columns["ObjectID"];

                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrpRatingScaleCategoryTable.Rows)
                        {
                            if ((RowTbl1[ActualSpCategoryIDCol].ToString()) == (RowTbl2[CompSpCategoryCol].ToString()))
                            {
                                if ((RowTbl2[CompSPBenchmarkFavPercCol] == DBNull.Value) || (RowTbl1[ActualSPBenchmarkFavPercCol] == DBNull.Value))
                                {
                                    if ((RowTbl1[ActualSPBenchmarkFavPercCol].ToString()) != (RowTbl2[CompSPBenchmarkFavPercCol].ToString()))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }

                                else
                                {
                                    var Diff_benchmark = Convert.ToInt32(RowTbl1[ActualSPFavPercCol]) - Convert.ToInt32(RowTbl2[CompSPBenchmarkFavPercCol]);
                                    if (Convert.ToInt32(RowTbl1[ActualSPBenchmarkFavPercCol]) != Convert.ToInt32(Diff_benchmark))
                                    {
                                        bbflag = false;
                                        WriteFailedData(DataCalculation, reportname, UseranalyzeFilterID, groupType, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }
            }

            return bbflag;

        }

        public static bool CompareTrendReportBenchmarkResult(string DataCalculation, DataSet ActualSP, DataSet CompGrp)
        {
            try
            {
                bool Tbbflag = true;
                DataTable ActualSPRatingScaleTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[0]);
                ActualSPRatingScaleTable = ActualDv.ToTable();

                DataTable CompSPRatingScaleTable;
                DataViewManager Compdvm = CompGrp.DefaultViewManager;
                DataView CompDv = Compdvm.CreateDataView(CompGrp.Tables[0]);
                CompSPRatingScaleTable = CompDv.ToTable();

                if (DataCalculation.ToLower() == "average")
                {
                    var ActualSpMeanvalue = ActualSPRatingScaleTable.Rows[0]["Comp_Bench_Mean"].ToString();
                    var CompGrpMeanValue = CompSPRatingScaleTable.Rows[0]["Mean"].ToString();
                    var ActualSpRespondentCount = ActualSPRatingScaleTable.Rows[0]["Comp_Bench_TotalDistinctRespCount"].ToString();
                    var CompGrpRespondentCount = CompSPRatingScaleTable.Rows[0]["RespondentCount"].ToString();
                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ActualSpMeanvalue)), 2) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(CompGrpMeanValue)), 2))
                        || (CommonActions.IfNullThenZero(ActualSpRespondentCount) != CommonActions.IfNullThenZero(CompGrpRespondentCount) && ActualSpMeanvalue.Length > 0 && CompGrpMeanValue.Length > 0))
                    {
                        Tbbflag = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            Log("ActualSp Benchmark FavPercvalue" + " = " + ActualSpMeanvalue, w);
                            Log("CompGrp Benchmark FavpercValue" + " = " + CompGrpMeanValue, w);
                            Log("ActualSp Benchmark RespondentCount" + " = " + ActualSpRespondentCount, w);
                            Log("CompGrp Benchmark RespondentCount" + " = " + CompGrpRespondentCount, w);
                        }
                    }
                }
                else
                {
                    var ActualSpFavPercvalue = ActualSPRatingScaleTable.Rows[0]["Comp_Bench_FavPerc"].ToString();
                    var CompGrpFavpercValue = CompSPRatingScaleTable.Rows[0]["BenchmarkFavPerc"].ToString();
                    var ActualSpRespondentCount = ActualSPRatingScaleTable.Rows[0]["Comp_Bench_TotalDistinctRespCount"].ToString();
                    var CompGrpRespondentCount = CompSPRatingScaleTable.Rows[0]["RespondentCount"].ToString();
                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ActualSpFavPercvalue)), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(CompGrpFavpercValue)), 0))
                        || (CommonActions.IfNullThenZero(ActualSpRespondentCount) != CommonActions.IfNullThenZero(CompGrpRespondentCount) && ActualSpFavPercvalue.Length > 0 && CompGrpFavpercValue.Length > 0))
                    {
                        Tbbflag = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            Log("ActualSp Benchmark FavPercvalue" + " = " + ActualSpFavPercvalue, w);
                            Log("CompGrp Benchmark FavpercValue" + " = " + CompGrpFavpercValue, w);
                            Log("ActualSp Benchmark RespondentCount" + " = " + ActualSpRespondentCount, w);
                            Log("CompGrp Benchmark RespondentCount" + " = " + CompGrpRespondentCount, w);
                        }
                    }
                }

                return Tbbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }


        public static bool CompareTrendReportBenchmarkResultForNR(DataSet ActualSP, DataSet CompGrp)
        {
            try
            {
                bool Tbbflag = true;
                DataTable ActualSPRatingScaleTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[0]);
               
                ActualSPRatingScaleTable = ActualDv.ToTable();

                DataTable CompSPRatingScaleTable;
                DataViewManager Compdvm = CompGrp.DefaultViewManager;
                DataView CompDv = Compdvm.CreateDataView(CompGrp.Tables[0]);
                CompSPRatingScaleTable = CompDv.ToTable();

                var ActualSpBenchmarkvalue = ActualSPRatingScaleTable.Rows[0]["Comp_Bench_Mean"].ToString();
                var TargetSpBenchmarkvalue = CompSPRatingScaleTable.Rows[0]["RespPerc"].ToString();
                var Comp_TargetBenchmarkvalue = TargetSpBenchmarkvalue + ".00";

                  if (ActualSpBenchmarkvalue != Comp_TargetBenchmarkvalue)
                {
                    Tbbflag = false;
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        Log("ActualSp Benchmark FavPercvalue" + " = " + ActualSpBenchmarkvalue, w);
                        Log("CompGrp Benchmark FavpercValue" + " = " + TargetSpBenchmarkvalue, w);
                      
                    }
                }



                return Tbbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }
        public static bool CompareHeapTrendReportBenchmarkResultForNR( DataSet ActualSP, DataSet CompGrp, string ScaleOptionID)
        {
            try
            {
                bool Tbbflag = true;
                DataTable ActualSPRatingScaleTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                ActualDv.RowFilter = " ObjectTypeID IN(5) AND ObjectID in(" + ScaleOptionID + ") AND GroupBy IN('All')";
                ActualSPRatingScaleTable = ActualDv.ToTable();

                DataTable CompSPRatingScaleTable;
                DataViewManager Compdvm = CompGrp.DefaultViewManager;
                DataView CompDv = Compdvm.CreateDataView(CompGrp.Tables[0]);
                CompSPRatingScaleTable = CompDv.ToTable();

                var ActualSpBenchmarkvalue = ActualSPRatingScaleTable.Rows[0]["BenchMarkData"].ToString();
                var TargetSpBenchmarkvalue = CompSPRatingScaleTable.Rows[0]["RespPerc"].ToString();
                var Comp_TargetBenchmarkvalue = TargetSpBenchmarkvalue + ".00";

                if (ActualSpBenchmarkvalue != TargetSpBenchmarkvalue)
                {
                    Tbbflag = false;
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        Log("ActualSp Benchmark FavPercvalue" + " = " + ActualSpBenchmarkvalue, w);
                        Log("CompGrp Benchmark FavpercValue" + " = " + TargetSpBenchmarkvalue, w);

                    }
                }



                return Tbbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public static bool CompareHeatMapBenchmarkResult(string DataCalculation, string Show, DataSet ActualSP, DataSet CompGrp)
        {
            try
            {
                bool Tbbflag = true;

                DataTable ActualSPRatingScaleTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);

                ActualDv.RowFilter = "ObjectTypeId IN(4) AND GroupBY NOT IN('All')";
                //ActualDv.RowFilter = "GroupBY NOT IN('All')";
                ActualDv.Sort = "GroupBy ASC";
                ActualSPRatingScaleTable = ActualDv.ToTable();


                DataTable CompSPRatingScaleTable;
                DataViewManager Compdvm = CompGrp.DefaultViewManager;
                DataView CompDv = Compdvm.CreateDataView(CompGrp.Tables[0]);
                CompSPRatingScaleTable = CompDv.ToTable();

                if (DataCalculation.ToLower() == "average")
                {
                    decimal? TempBenchMean = 0;
                    decimal? BenchMean = 0;
                    int? BenchCnt = 0;
                    string ActualSpMeanvalue = null; 
                    string CompGrpMeanValue = null;
                    string ActualSpRespondentCount = null;
                    string CompGrpRespondentCount = null;
                    if (ActualSPRatingScaleTable.Rows.Count > 0)
                    {
                        ActualSpMeanvalue = ActualSPRatingScaleTable.Rows[0]["BenchMarkData2"].ToString();
                        CompGrpMeanValue = CompSPRatingScaleTable.Rows[0]["Mean"].ToString();
                        ActualSpRespondentCount = ActualSPRatingScaleTable.Rows[0]["BenchMarkNSize"].ToString();
                        CompGrpRespondentCount = CompSPRatingScaleTable.Rows[0]["RespondentCount"].ToString();
                    }

                    if (Show.ToLower() != "category")
                    {
                        if (ActualSpMeanvalue == null || ActualSpMeanvalue.ToString() == "")
                        {
                            BenchMean = null;
                            BenchCnt = null;
                        }
                        else
                        {
                            BenchMean = Math.Round(Decimal.Parse(ActualSpMeanvalue.ToString()), 2);
                            BenchCnt = Int32.Parse(ActualSpRespondentCount.ToString());
                        }
                    }
                    else
                    {
                        foreach (DataRow row in ActualSPRatingScaleTable.Rows)
                        {
                            if (row["BenchMarkData2"] == null || row["BenchMarkData2"].ToString() == "")
                            {
                                BenchMean = null;
                                BenchCnt = null;
                                break;
                            }
                            else
                            {
                                TempBenchMean = TempBenchMean + Math.Round(Decimal.Parse(row["BenchMarkData2"].ToString()), 2);
                                BenchCnt = Math.Max(Int32.Parse(BenchCnt.ToString()), Int32.Parse(row["BenchMarkNSize"].ToString()));
                            }
                        }
                        BenchMean = (ActualSPRatingScaleTable.Rows.Count == 0) ? 0 : Math.Round(Decimal.Parse((TempBenchMean / ActualSPRatingScaleTable.Rows.Count).ToString()), 2);
                    }

                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(BenchMean)), 2) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(CompGrpMeanValue)), 2))
                        || (CommonActions.IfNullThenZero(BenchCnt) != CommonActions.IfNullThenZero(CompGrpRespondentCount) && (CommonActions.IfNullThenZero(BenchMean)).Length > 0 && (CommonActions.IfNullThenZero(CompGrpMeanValue)).Length > 0))
                    {
                        Tbbflag = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            Log("ActualSp Benchmark FavPercvalue" + " = " + BenchMean.ToString(), w);
                            Log("CompGrp Benchmark FavpercValue" + " = " + CompGrpMeanValue, w);
                            Log("ActualSp Benchmark RespondentCount" + " = " + BenchCnt.ToString(), w);
                            Log("CompGrp Benchmark RespondentCount" + " = " + CompGrpRespondentCount, w);
                        }
                    }
                }
                else
                {
                    decimal? TempBenchFav = 0;
                    decimal? BenchFav = 0;
                    int? BenchCnt = 0;
                    string ActualSpFavPercvalue = null;
                    string CompGrpFavpercValue = null;
                    string ActualSpRespondentCount = null;
                    string CompGrpRespondentCount = null;
                    if (ActualSPRatingScaleTable.Rows.Count > 0)
                    {
                        ActualSpFavPercvalue = ActualSPRatingScaleTable.Rows[0]["BenchMarkData1"].ToString();
                        CompGrpFavpercValue = CompSPRatingScaleTable.Rows[0]["BenchmarkFavPerc"].ToString();
                        ActualSpRespondentCount = ActualSPRatingScaleTable.Rows[0]["BenchMarkNSize"].ToString();
                        CompGrpRespondentCount = CompSPRatingScaleTable.Rows[0]["RespondentCount"].ToString();
                    }

                    if (Show.ToLower() != "category")
                    {
                        if (ActualSpFavPercvalue == null || ActualSpFavPercvalue.ToString() == "")
                        {
                            BenchFav = null;
                            BenchCnt = null;
                        }
                        else
                        {
                            BenchFav = Math.Round(Decimal.Parse(ActualSpFavPercvalue.ToString()), 0);
                            BenchCnt = Int32.Parse(ActualSpRespondentCount.ToString());
                        }
                    }
                    else
                    {
                        foreach (DataRow row in ActualSPRatingScaleTable.Rows)
                        {
                            if (row["BenchMarkData1"] == null || row["BenchMarkData1"].ToString() == "")
                            {
                                BenchFav = null;
                                BenchCnt = null;
                                break;
                            }
                            else
                            {
                                TempBenchFav = TempBenchFav + Math.Round(Decimal.Parse(row["BenchMarkData1"].ToString()), 0);
                                BenchCnt = Math.Max(Int32.Parse(BenchCnt.ToString()), Int32.Parse(row["BenchMarkNSize"].ToString()));
                            }
                        }
                        BenchFav = (ActualSPRatingScaleTable.Rows.Count == 0) ? 0 : Math.Round(Decimal.Parse((TempBenchFav / ActualSPRatingScaleTable.Rows.Count).ToString()), 0);
                    }


                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(BenchFav)), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(CompGrpFavpercValue)), 0))
                        || (CommonActions.IfNullThenZero(BenchCnt) != CommonActions.IfNullThenZero(CompGrpRespondentCount) && (CommonActions.IfNullThenZero(BenchFav)).Length > 0 && (CommonActions.IfNullThenZero(CompGrpFavpercValue)).Length > 0))
                    {
                        Tbbflag = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            Log("ActualSp Benchmark FavPercvalue" + " = " + BenchFav.ToString(), w);
                            Log("CompGrp Benchmark FavpercValue" + " = " + CompGrpFavpercValue, w);
                            Log("ActualSp Benchmark RespondentCount" + " = " + BenchCnt.ToString(), w);
                            Log("CompGrp Benchmark RespondentCount" + " = " + CompGrpRespondentCount, w);
                        }
                    }
                }

                return Tbbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp)
        {
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            int avgORfav = 0;

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable TargetGrpRatingScaleTable;
            DataViewManager TGrpDvm = TargetGrp.DefaultViewManager;
            DataView TGrpDv = TGrpDvm.CreateDataView(TargetGrp.Tables[0]);
            TGrpDv.Sort = "ObjectID ASC";
            TargetGrpRatingScaleTable = TGrpDv.ToTable();

            if (DataCalculation.ToLower() == "average")
            {
                var ActualSPResultItemColumn = ActualSPRatingScaleTable.Columns["ItemID"];
                var TargetSPResultItemColumn = TargetGrpRatingScaleTable.Columns["ObjectID"];
                var AvgScoreCol = ActualSPRatingScaleTable.Columns["AverageScore"];
                var MeanCol = TargetGrpRatingScaleTable.Columns["Mean"];
                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {

                        if (RowTbl1[ActualSPResultItemColumn].ToString() == RowTbl2[TargetSPResultItemColumn].ToString())
                        {
                            if ((RowTbl1[AvgScoreCol] == DBNull.Value) && (RowTbl2[MeanCol] != DBNull.Value))
                            {
                                string actualValue = "0.00";
                                if (actualValue.ToString() != RowTbl2[MeanCol].ToString())
                                {

                                    rsflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                    break;
                                }

                            }
                            else
                            {
                                if (RowTbl1[AvgScoreCol].ToString() != RowTbl2[MeanCol].ToString())
                                {
                                    rsflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                    break;
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                var ActualSPResultItemColumn = ActualSPRatingScaleTable.Columns["ItemID"];
                var TargetSPResultItemColumn = TargetGrpRatingScaleTable.Columns["ObjectID"];
                var FavScoreCol = ActualSPRatingScaleTable.Columns["FavPercent"];
                var NeuScoreCol = ActualSPRatingScaleTable.Columns["NeutralPercent"];
                var UnFavScoreCol = ActualSPRatingScaleTable.Columns["UnFavPercent"];

                var FavorableCol = TargetGrpRatingScaleTable.Columns["Favorable"];
                var NeutralCol = TargetGrpRatingScaleTable.Columns["Neutral"];
                var UnfavorableCol = TargetGrpRatingScaleTable.Columns["Unfavorable"];
                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[ActualSPResultItemColumn].ToString() == RowTbl2[TargetSPResultItemColumn].ToString())
                        {
                            if ((RowTbl1[FavScoreCol] == DBNull.Value) || RowTbl1[NeuScoreCol] == DBNull.Value || (RowTbl1[UnFavScoreCol] == DBNull.Value))
                            {
                                string favScore = "0"; string NeuScore = "0"; string UnfavScore = "0";

                                if ((favScore != RowTbl2[FavorableCol].ToString()) || (NeuScore != RowTbl2[NeutralCol].ToString()) || (UnfavScore != RowTbl2[UnfavorableCol].ToString()))
                                {
                                    rsflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TargetSPResultItemColumn].ToString());
                                    break;
                                }
                            }
                            else
                            {
                                if ((RowTbl1[FavScoreCol].ToString() != RowTbl2[FavorableCol].ToString()) || (RowTbl1[NeuScoreCol].ToString() != RowTbl2[NeutralCol].ToString()) || (RowTbl1[UnFavScoreCol].ToString() != RowTbl2[UnfavorableCol].ToString()))
                                {
                                    rsflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TargetSPResultItemColumn].ToString());
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            DataTable ActualSPNonRatingScaleTable;
            DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
            DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
            ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";
            ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
            ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

            DataTable TargetNonRatingGrpTable;
            DataViewManager TGrpNonRatingDvm = TargetGrp.DefaultViewManager;
            DataView TGrpNonRatingDv = TGrpNonRatingDvm.CreateDataView(TargetGrp.Tables[3]);
            TGrpNonRatingDv.Sort = "SurveyFormItemID,ScaleOptionID ASC";
            TargetNonRatingGrpTable = TGrpNonRatingDv.ToTable();

            var ACTSurveyformItemID = ActualSPNonRatingScaleTable.Columns["ItemID"];
            var TrgSurveyformItemID = TargetNonRatingGrpTable.Columns["SurveyformItemID"];
            var ACTScaleOptionID = ActualSPNonRatingScaleTable.Columns["ScaleOptionID"];
            var TrgScaleOptionID = TargetNonRatingGrpTable.Columns["ScaleOptionID"];
            var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["AverageScore"];
            var TrgNonRatingscore = TargetNonRatingGrpTable.Columns["Response Rate"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetNonRatingGrpTable.Rows)
                {
                    if ((RowTbl1[ACTSurveyformItemID].ToString() == RowTbl2[TrgSurveyformItemID].ToString()) && (RowTbl1[ACTScaleOptionID].ToString() == RowTbl2[TrgScaleOptionID].ToString()))
                    {
                        String ReportNames = "ResultReport_NonRatingScale";
                        var TrgNonRatingScaleAverageScore = RowTbl2[TrgNonRatingscore].ToString() + ".00";
                        if (RowTbl1[ActNonRatingScore].ToString() != TrgNonRatingScaleAverageScore)
                        {
                            nonrsflag = false;
                            WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TrgScaleOptionID].ToString());
                            break;
                        }
                    }

                }
            }

           /* DataTable ActualSPCommentTable;
            DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
            DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
            ActualCommentDv.RowFilter = "ItemType IN(6)";
            ActualCommentDv.Sort = "ItemID ASC";
            ActualSPCommentTable = ActualCommentDv.ToTable();

            DataTable TargetCommentTable;
            DataViewManager TargetCommentTabledvm = TargetGrp.DefaultViewManager;
            DataView TargetCommentDv = TargetCommentTabledvm.CreateDataView(TargetGrp.Tables[4]);
            TargetCommentDv.Sort = "SurveyformItemID ASC";
            TargetCommentTable = TargetCommentDv.ToTable();

            var ActSurveyformitemID = ActualSPCommentTable.Columns["ItemID"];
            var TrgSurveyformitemID = TargetCommentTable.Columns["SurveyformitemID"];
            var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
            var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
            var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];

            var TrgCommentWordID = TargetCommentTable.Columns["WordID"];
            var TrgCommentText = TargetCommentTable.Columns["Word"];
            var TrgCommentWordFrequency = TargetCommentTable.Columns["WordFrequency"];

            /* foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
             {
                 foreach (DataRow RowTbl2 in TargetCommentTable.Rows)
                 {
                     if (RowTbl1[ActSurveyformitemID].ToString() == RowTbl2[TrgSurveyformitemID].ToString())
                     {
                         if ((RowTbl1[ActCommentText].ToString() != RowTbl2[TrgCommentText].ToString()))
                         {
                             string ReportNames = "ResultReportComment";
                             wcflag = false;
                             WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 0, DBConnectionParameters);
                             break;
                         }
                     }
                 }
             }

            int WordCount = ActualSPCommentTable.Rows.Count;

            for( int i= 0; i< WordCount;i++)
            {
                if (ActualSPCommentTable.Rows[i][8].ToString() == TargetCommentTable.Rows[i][1].ToString())
                {
                    if (ActualSPCommentTable.Rows[i][57].ToString() != TargetCommentTable.Rows[i][2].ToString())
                    {
                        string ReportNames = "ResultReportComment";
                        wcflag = false;
                        WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 0, DBConnectionParameters, TargetCommentTable.Rows[i][1].ToString(), TargetCommentTable.Rows[i][2].ToString(),i);
                        //break;

                    }
                 }
            }



            DataTable ActualSPFFTable;
            DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
            DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
            ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
            ActualFFDv.Sort = "ItemID ASC";
            ActualSPFFTable = ActualFFDv.ToTable();

            DataTable TargetFFTable;
            DataViewManager TargetFFTabledvm = TargetGrp.DefaultViewManager;
            DataView TargetFFDv = TargetFFTabledvm.CreateDataView(TargetGrp.Tables[5]);
            TargetFFDv.Sort = "ItemID ASC";
            TargetFFTable = TargetFFDv.ToTable();
            var ActFFItemID = ActualSPFFTable.Columns["ItemID"];
            var ActFFText = ActualSPFFTable.Columns["CommentText"];
            var TrgFFItemID = TargetFFTable.Columns["ItemID"];
            var TrgFFText = TargetFFTable.Columns["Comments"];

            /*foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetFFTable.Rows)
                {
                    if ((RowTbl1[ActFFItemID].ToString() == RowTbl2[TrgFFItemID].ToString()))

                    {
                        if ((RowTbl1[ActFFText].ToString() != RowTbl2[TrgFFText].ToString()))
                        {
                            ffflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                            break;
                        }
                    }
                }
            }
            int FixedFormatCount = ActualSPFFTable.Rows.Count;

            for (int i = 0; i < WordCount; i++)
            {
                if (ActualSPFFTable.Rows[i][8].ToString() == TargetFFTable.Rows[i][0].ToString())
                {
                    if (ActualSPFFTable.Rows[i][57].ToString() != TargetFFTable.Rows[i][1].ToString())
                    {
                        string ReportNames = "ResultReportFixedFormat";
                        wcflag = false;
                        WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 0, DBConnectionParameters);
                        break;

                    }
                }
            }*/


            return rsflag && nonrsflag && wcflag && ffflag;
        }
        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool firstlevelcomparison = CompareResultReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp);
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            bool bbflag = true;
            int avgORfav = 0;

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrp1RatingScaleTable1;
            DataViewManager TGrpDvm1 = CompGrp1.DefaultViewManager;
            DataView TGrpDv1 = TGrpDvm1.CreateDataView(CompGrp1.Tables[0]);
            CompGrp1RatingScaleTable1 = TGrpDv1.ToTable();
            DataColumnCollection Columns = CompGrp1RatingScaleTable1.Columns;
            if (Columns.Contains("BenchmarkFavPerc") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp1, "Comp1", ReportName, UserAnalyzeFilterId, DBConnectionParameters);

            }
            else
            {
                DataTable CompGrp1RatingScaleTable;
                DataViewManager TGrpDvm = CompGrp1.DefaultViewManager;
                DataView TGrpDv = TGrpDvm.CreateDataView(CompGrp1.Tables[0]);
                TGrpDv.Sort = "ObjectID ASC";
                CompGrp1RatingScaleTable = TGrpDv.ToTable();
                var ActSurveyformitemID = ActualSPRatingScaleTable.Columns["ItemId"];
                var TrgSurveyformitemID = CompGrp1RatingScaleTable.Columns["ObjectID"];

                if (DataCalculation.ToLower() == "average")
                {
                    var AvgScoreCol = ActualSPRatingScaleTable.Columns["Comp1AverageScore"];
                    var MeanCol = CompGrp1RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if (RowTbl1[ActSurveyformitemID].ToString() == RowTbl2[TrgSurveyformitemID].ToString())
                            {
                                if ((RowTbl1[AvgScoreCol] == DBNull.Value) && (RowTbl2[MeanCol] != DBNull.Value))
                                {
                                    string actualValue = "0.00";
                                    if (actualValue.ToString() != RowTbl2[MeanCol].ToString())
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                        break;
                                    }

                                }
                                else
                                {
                                    if (RowTbl1[AvgScoreCol].ToString() != RowTbl2[MeanCol].ToString())
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var FavScoreCol = ActualSPRatingScaleTable.Columns["Comp1FavPercent"];
                    var NeuScoreCol = ActualSPRatingScaleTable.Columns["Comp1NeutralPercent"];
                    var UnFavScoreCol = ActualSPRatingScaleTable.Columns["Comp1UnFavPercent"];

                    var FavorableCol = CompGrp1RatingScaleTable.Columns["Favorable"];
                    var NeutralCol = CompGrp1RatingScaleTable.Columns["Neutral"];
                    var UnfavorableCol = CompGrp1RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if (RowTbl1[ActSurveyformitemID].ToString() == RowTbl2[TrgSurveyformitemID].ToString())
                            {
                                if ((RowTbl1[FavScoreCol] == DBNull.Value) || RowTbl1[NeuScoreCol] == DBNull.Value || (RowTbl1[UnFavScoreCol] == DBNull.Value))
                                {
                                    string favScore = "0"; string NeuScore = "0"; string UnfavScore = "0";

                                    if ((favScore != RowTbl2[FavorableCol].ToString()) || (NeuScore != RowTbl2[NeutralCol].ToString()) || (UnfavScore != RowTbl2[UnfavorableCol].ToString()))
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TrgSurveyformitemID].ToString());
                                        break;
                                    }
                                }
                                else
                                {
                                    if ((RowTbl1[FavScoreCol].ToString() != RowTbl2[FavorableCol].ToString()) || (RowTbl1[NeuScoreCol].ToString() != RowTbl2[NeutralCol].ToString()) || (RowTbl1[UnFavScoreCol].ToString() != RowTbl2[UnfavorableCol].ToString()))
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TrgSurveyformitemID].ToString());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }


                DataTable ActualSPNonRatingScaleTable;
                DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
                DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
                ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";
                ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

                DataTable Comp1NonRatingGrpTable;
                DataViewManager comp1NonRatingDvm = CompGrp1.DefaultViewManager;
                DataView Comp1NonRatingDv = comp1NonRatingDvm.CreateDataView(CompGrp1.Tables[3]);
                Comp1NonRatingDv.Sort = "SurveyFormItemID,ScaleOptionID ASC";
                Comp1NonRatingGrpTable = Comp1NonRatingDv.ToTable();
                var ActsurveyformitemID = ActualSPNonRatingScaleTable.Columns["ItemID"];
                var TrgSurveyformItemID = Comp1NonRatingGrpTable.Columns["SurveyFormItemID"];
                var ActScaleOptionID = ActualSPNonRatingScaleTable.Columns["ScaleOptionID"];
                var TrgScaleOptionID = Comp1NonRatingGrpTable.Columns["ScaleOptionID"];
                var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["Comp1AverageScore"];
                var TrgNonRatingscore = Comp1NonRatingGrpTable.Columns["Response Rate"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in Comp1NonRatingGrpTable.Rows)
                    {
                        if ((RowTbl1[ActsurveyformitemID].ToString() == RowTbl2[TrgSurveyformItemID].ToString()) && (RowTbl1[ActScaleOptionID].ToString() == RowTbl2[TrgScaleOptionID].ToString()))
                        {
                            string TrgNonRatingScaleAverageScore = RowTbl2[TrgNonRatingscore].ToString() + ".00";
                            if (RowTbl1[ActNonRatingScore].ToString() != TrgNonRatingScaleAverageScore)
                            {
                                String ReportNames = "ResultReport_NonRatingScale";
                                nonrsflag = false;
                                WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 1, DBConnectionParameters, RowTbl2[TrgScaleOptionID].ToString());
                                break;
                            }
                        }
                    }
                }

                /* DataTable ActualSPCommentTable;
                 DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
                 DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
                 ActualCommentDv.RowFilter = "ItemType IN(6)";
                 ActualCommentDv.Sort = "ItemID ASC";
                 ActualSPCommentTable = ActualCommentDv.ToTable();

                 DataTable CompGrp1TargetCommentTable;
                 DataViewManager CompGrp1TargetCommentTabledvm = CompGrp1.DefaultViewManager;
                 DataView CompGrp1TargetCommentDv = CompGrp1TargetCommentTabledvm.CreateDataView(CompGrp1.Tables[4]);
                 CompGrp1TargetCommentDv.Sort = "SurveyformItemID ASC";
                 CompGrp1TargetCommentTable = CompGrp1TargetCommentDv.ToTable();

                 var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
                 var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
                 var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];
                 var ActSurveyformItemID = ActualSPCommentTable.Columns["ItemID"];
                 var TrgCommentSurveyformItemID = CompGrp1TargetCommentTable.Columns["SurveyformItemID"];

                 var TrgCommentWordID = CompGrp1TargetCommentTable.Columns["WordID"];
                 var TrgCommentText = CompGrp1TargetCommentTable.Columns["Word"];
                 var TrgCommentWordFrequency = CompGrp1TargetCommentTable.Columns["WordFrequency"];

                 /* foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
                  {
                      foreach (DataRow RowTbl2 in CompGrp1TargetCommentTable.Rows)
                      {
                          if (RowTbl1[ActSurveyformItemID].ToString() == RowTbl2[TrgCommentSurveyformItemID].ToString())
                          {
                              if ((RowTbl1[ActCommentWordID] != RowTbl2[TrgCommentWordID]) || (RowTbl1[ActCommentText] != RowTbl2[TrgCommentText]))

                              {
                                  wcflag = false;
                                  WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                  break;
                              }
                          }
                      }
                  }

                 int WordCount = ActualSPCommentTable.Rows.Count;

                 for (int i = 0; i < WordCount; i++)
                 {
                     if (ActualSPCommentTable.Rows[i][8].ToString() == CompGrp1TargetCommentTable.Rows[i][1].ToString())
                     {
                         if (ActualSPCommentTable.Rows[i][57].ToString() != CompGrp1TargetCommentTable.Rows[i][2].ToString())
                         {
                             string ReportNames = "ResultReportComment";
                             wcflag = false;
                             WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 0, DBConnectionParameters, "", CompGrp1TargetCommentTable.Rows[i][2].ToString(), i);
                             break;

                         }
                     }
                 }

                 DataTable ActualSPFFTable;
                 DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
                 DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
                 ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
                 ActualFFDv.Sort = "ItemID ASC";
                 ActualSPFFTable = ActualFFDv.ToTable();

                 DataTable CompGrp1TargetFFTable;
                 DataViewManager CompGrp1TargetFFTabledvm = CompGrp1.DefaultViewManager;
                 DataView CompGrp1TargetFFDv = CompGrp1TargetFFTabledvm.CreateDataView(CompGrp1.Tables[5]);
                 CompGrp1TargetFFDv.Sort = "ItemID ASC";
                 CompGrp1TargetFFTable = CompGrp1TargetFFDv.ToTable();
                 var ActFFItemID = ActualSPCommentTable.Columns["ItemID"];
                 var ActFFText = ActualSPCommentTable.Columns["CommentText"];
                 var TrgFFItemID = CompGrp1TargetCommentTable.Columns["ItemID"];
                 var TrgFFText = CompGrp1TargetCommentTable.Columns["Comments"];

                /* foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
                 {
                     foreach (DataRow RowTbl2 in CompGrp1TargetFFTable.Rows)
                     {
                         if ((RowTbl1[ActFFItemID] == RowTbl2[TrgFFItemID]))
                         {
                             if ((RowTbl1[ActFFText] != RowTbl2[TrgFFText]))
                             {
                                 ffflag = false;
                                 WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                 break;
                             }
                         }
                     }
                 }*/

            }


            return firstlevelcomparison && rsflag && nonrsflag && wcflag && ffflag && bbflag;
        }

        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool secondlevelcomparison = CompareResultReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1);
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrp2RatingScaleTable1;
            DataViewManager TGrpDvm1 = CompGrp2.DefaultViewManager;
            DataView TGrpDv1 = TGrpDvm1.CreateDataView(CompGrp2.Tables[0]);

            CompGrp2RatingScaleTable1 = TGrpDv1.ToTable();

            DataColumnCollection Columns = CompGrp2RatingScaleTable1.Columns;
            if (Columns.Contains("BenchmarkFavPerc") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp2, "Comp2", ReportName, UserAnalyzeFilterId, DBConnectionParameters);

            }
            else
            {
                DataTable CompGrp2RatingScaleTable;
                DataViewManager TGrpDvm = CompGrp2.DefaultViewManager;
                DataView TGrpDv = TGrpDvm.CreateDataView(CompGrp2.Tables[0]);
                TGrpDv.Sort = "ObjectID ASC";
                CompGrp2RatingScaleTable = TGrpDv.ToTable();
                var ActSurveyformitemID = ActualSPRatingScaleTable.Columns["ItemId"];
                var TrgSurveyformitemID = CompGrp2RatingScaleTable.Columns["ObjectID"];
                if (DataCalculation.ToLower() == "average")
                {

                    var AvgScoreCol = ActualSPRatingScaleTable.Columns["Comp2AverageScore"];
                    var MeanCol = CompGrp2RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if (RowTbl1[ActSurveyformitemID].ToString() == RowTbl2[TrgSurveyformitemID].ToString())
                            {
                                if ((RowTbl1[AvgScoreCol] == DBNull.Value) && (RowTbl2[MeanCol] != DBNull.Value))
                                {
                                    string actualValue = "0.00";
                                    if (actualValue.ToString() != RowTbl2[MeanCol].ToString())
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                        break;
                                    }

                                }
                                else
                                {

                                    if (RowTbl1[AvgScoreCol].ToString() != RowTbl2[MeanCol].ToString())
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 2, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var FavScoreCol = ActualSPRatingScaleTable.Columns["Comp2FavPercent"];
                    var NeuScoreCol = ActualSPRatingScaleTable.Columns["Comp2NeutralPercent"];
                    var UnFavScoreCol = ActualSPRatingScaleTable.Columns["Comp2UnFavPercent"];

                    var FavorableCol = CompGrp2RatingScaleTable.Columns["Favorable"];
                    var NeutralCol = CompGrp2RatingScaleTable.Columns["Neutral"];
                    var UnfavorableCol = CompGrp2RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if (RowTbl1[ActSurveyformitemID].ToString() == RowTbl2[TrgSurveyformitemID].ToString())
                            {
                                if ((RowTbl1[FavScoreCol] == DBNull.Value) || (RowTbl1[NeuScoreCol] == DBNull.Value) || (RowTbl1[UnFavScoreCol] == DBNull.Value))
                                {
                                    string favScore = "0"; string NeuScore = "0"; string UnfavScore = "0";

                                    if ((favScore != RowTbl2[FavorableCol].ToString()) || (NeuScore != RowTbl2[NeutralCol].ToString()) || (UnfavScore != RowTbl2[UnfavorableCol].ToString()))
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TrgSurveyformitemID].ToString());
                                        break;
                                    }
                                }
                                else
                                {
                                    if ((RowTbl1[FavScoreCol].ToString() != RowTbl2[FavorableCol].ToString()) || (RowTbl1[NeuScoreCol].ToString() != RowTbl2[NeutralCol].ToString()) || (RowTbl1[UnFavScoreCol].ToString() != RowTbl2[UnfavorableCol].ToString()))
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TrgSurveyformitemID].ToString());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }



                DataTable ActualSPNonRatingScaleTable;
                DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
                DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
                ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";
                ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

                DataTable Comp2NonRatingGrpTable;
                DataViewManager comp2NonRatingDvm = CompGrp2.DefaultViewManager;
                DataView Comp2NonRatingDv = comp2NonRatingDvm.CreateDataView(CompGrp2.Tables[3]);
                Comp2NonRatingDv.Sort = "SurveyFormItemID,ScaleOptionID ASC";
                Comp2NonRatingGrpTable = Comp2NonRatingDv.ToTable();

                var ActsurveyformitemID = ActualSPNonRatingScaleTable.Columns["ItemID"];
                var TrgSurveyformItemID = Comp2NonRatingGrpTable.Columns["SurveyFormItemID"];
                var ActscaleoptionID = ActualSPNonRatingScaleTable.Columns["ScaleOptionID"];
                var TrgScaleOptionID = Comp2NonRatingGrpTable.Columns["ScaleOptionID"];
                var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["Comp2AverageScore"];
                var TrgNonRatingscore = Comp2NonRatingGrpTable.Columns["Response Rate"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in Comp2NonRatingGrpTable.Rows)
                    {
                        if (RowTbl1[ActsurveyformitemID].ToString() == RowTbl2[TrgSurveyformItemID].ToString() && (RowTbl1[ActscaleoptionID].ToString() == RowTbl2[TrgScaleOptionID].ToString()))
                        {
                            string TrgNonRatingScaleAverageScore = RowTbl2[TrgNonRatingscore].ToString() + ".00";
                            if (RowTbl1[ActNonRatingScore].ToString() != TrgNonRatingScaleAverageScore)
                            {
                                String ReportNames = "ResultReport_NonRatingScale";
                                nonrsflag = false;
                                WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 2, DBConnectionParameters, RowTbl2[TrgScaleOptionID].ToString());
                                break;
                            }
                        }
                    }
                }

                /*DataTable ActualSPCommentTable;
                DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
                DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualCommentDv.RowFilter = "ItemType IN(6)";
                ActualCommentDv.Sort = "ItemID ASC";
                ActualSPCommentTable = ActualCommentDv.ToTable();

                DataTable CompGrp2TargetCommentTable;
                DataViewManager CompGrp2TargetCommentTabledvm = CompGrp2.DefaultViewManager;
                DataView CompGrp2TargetCommentDv = CompGrp2TargetCommentTabledvm.CreateDataView(CompGrp2.Tables[4]);
                CompGrp2TargetCommentDv.Sort = "SurveyformItemID ASC";
                CompGrp2TargetCommentTable = CompGrp2TargetCommentDv.ToTable();

                var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
                var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
                var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];
                var ActSurveyformItemID = ActualSPCommentTable.Columns["ItemID"];
                var TrgCommentSurveyformItemID = CompGrp2TargetCommentTable.Columns["SurveyformItemID"];

                var TrgCommentWordID = CompGrp2TargetCommentTable.Columns["WordID"];
                var TrgCommentText = CompGrp2TargetCommentTable.Columns["Word"];
                var TrgCommentWordFrequency = CompGrp2TargetCommentTable.Columns["WordFrequency"];

                /*foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp2TargetCommentTable.Rows)
                    {
                        if (RowTbl1[ActSurveyformItemID].ToString() == RowTbl2[TrgCommentSurveyformItemID].ToString())
                        {
                            if ((RowTbl1[ActCommentWordID].ToString() != RowTbl2[TrgCommentWordID].ToString()) || (RowTbl1[ActCommentText].ToString() != RowTbl2[TrgCommentText].ToString()))

                            {
                                wcflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 2, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }

                int WordCount = ActualSPCommentTable.Rows.Count;

                for (int i = 0; i < WordCount; i++)
                {
                    if (ActualSPCommentTable.Rows[i][8].ToString() == CompGrp2TargetCommentTable.Rows[i][1].ToString())
                    {
                        if (ActualSPCommentTable.Rows[i][57].ToString() != CompGrp2TargetCommentTable.Rows[i][2].ToString())
                        {
                            string ReportNames = "ResultReportComment";
                            wcflag = false;
                            WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 0, DBConnectionParameters, "", CompGrp2TargetCommentTable.Rows[i][2].ToString(), i);
                            break;

                        }
                    }
                }




                DataTable ActualSPFFTable;
                DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
                DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
                ActualFFDv.Sort = "ItemID ASC";
                ActualSPFFTable = ActualFFDv.ToTable();

                DataTable CompGrp2TargetFFTable;
                DataViewManager CompGrp2TargetFFTabledvm = CompGrp2.DefaultViewManager;
                DataView CompGrp2TargetFFDv = CompGrp2TargetFFTabledvm.CreateDataView(CompGrp2.Tables[5]);
                CompGrp2TargetFFDv.Sort = "ItemID ASC";
                CompGrp2TargetFFTable = CompGrp2TargetFFDv.ToTable();
                var ActFFItemID = ActualSPCommentTable.Columns["ItemID"];
                var ActFFText = ActualSPCommentTable.Columns["CommentText"];
                var TrgFFItemID = CompGrp2TargetCommentTable.Columns["ItemID"];
                var TrgFFText = CompGrp2TargetCommentTable.Columns["Comments"];

               /* foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp2TargetFFTable.Rows)
                    {
                        if ((RowTbl1[ActFFItemID].ToString() == RowTbl2[TrgFFItemID].ToString()))
                        {
                            if (RowTbl1[ActFFText].ToString() != RowTbl2[TrgFFText].ToString())
                            {
                                ffflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 2, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }*/
            }


            return secondlevelcomparison && rsflag && nonrsflag && wcflag && ffflag && bbflag;
        }
        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool thirdlevelcomparison = CompareResultReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1, CompGrp2);
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrp3RatingScaleTable1;
            DataViewManager TGrpDvm1 = CompGrp3.DefaultViewManager;
            DataView TGrpDv1 = TGrpDvm1.CreateDataView(CompGrp3.Tables[0]);
            CompGrp3RatingScaleTable1 = TGrpDv1.ToTable();
            DataColumnCollection Columns = CompGrp3RatingScaleTable1.Columns;


            if (Columns.Contains("BenchmarkFavPerc") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp3, "Comp3", ReportName, UserAnalyzeFilterId, DBConnectionParameters);

            }
            else
            {
                DataTable CompGrp3RatingScaleTable;
                DataViewManager TGrpDvm = CompGrp3.DefaultViewManager;
                DataView TGrpDv = TGrpDvm.CreateDataView(CompGrp3.Tables[0]);
                TGrpDv.Sort = "ObjectID ASC";
                CompGrp3RatingScaleTable = TGrpDv.ToTable();

                var ActSurveyformitemID = ActualSPRatingScaleTable.Columns["ItemId"];
                var TrgSurveyformitemID = CompGrp3RatingScaleTable.Columns["ObjectID"];

                if (DataCalculation.ToLower() == "average")
                {
                    var AvgScoreCol = ActualSPRatingScaleTable.Columns["Comp3AverageScore"];
                    var MeanCol = CompGrp3RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if (RowTbl1[ActSurveyformitemID].ToString() == RowTbl2[TrgSurveyformitemID].ToString())
                            {
                                if ((RowTbl1[AvgScoreCol] == DBNull.Value) && (RowTbl2[MeanCol] != DBNull.Value))
                                {
                                    string actualValue = "0.00";
                                    if (actualValue.ToString() != RowTbl2[MeanCol].ToString())
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                        break;
                                    }

                                }
                                else
                                {
                                    if (RowTbl1[AvgScoreCol].ToString() != RowTbl2[MeanCol].ToString())
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                else
                {
                    var FavScoreCol = ActualSPRatingScaleTable.Columns["Comp3FavPercent"];
                    var NeuScoreCol = ActualSPRatingScaleTable.Columns["Comp3NeutralPercent"];
                    var UnFavScoreCol = ActualSPRatingScaleTable.Columns["Comp3UnFavPercent"];

                    var FavorableCol = CompGrp3RatingScaleTable.Columns["Favorable"];
                    var NeutralCol = CompGrp3RatingScaleTable.Columns["Neutral"];
                    var UnfavorableCol = CompGrp3RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if (RowTbl1[ActSurveyformitemID].ToString() == RowTbl2[TrgSurveyformitemID].ToString())
                            {
                                if ((RowTbl1[FavScoreCol] == DBNull.Value) || RowTbl1[NeuScoreCol] == DBNull.Value || (RowTbl1[UnFavScoreCol] == DBNull.Value))
                                {
                                    string favScore = "0"; string NeuScore = "0"; string UnfavScore = "0";

                                    if ((favScore != RowTbl2[FavorableCol].ToString()) || (NeuScore != RowTbl2[NeutralCol].ToString()) || (UnfavScore != RowTbl2[UnfavorableCol].ToString()))
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TrgSurveyformitemID].ToString());
                                        break;
                                    }
                                }
                                else
                                {
                                    if ((RowTbl1[FavScoreCol].ToString() != RowTbl2[FavorableCol].ToString()) || (RowTbl1[NeuScoreCol].ToString() != RowTbl2[NeutralCol].ToString()) || (RowTbl1[UnFavScoreCol].ToString() != RowTbl2[UnfavorableCol].ToString()))
                                    {
                                        rsflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters, RowTbl2[TrgSurveyformitemID].ToString());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }



                DataTable ActualSPNonRatingScaleTable;
                DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
                DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
                ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";
                ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

                DataTable Comp3NonRatingGrpTable;
                DataViewManager comp3NonRatingDvm = CompGrp3.DefaultViewManager;
                DataView Comp3NonRatingDv = comp3NonRatingDvm.CreateDataView(CompGrp3.Tables[3]);
                Comp3NonRatingDv.Sort = "SurveyFormItemID,ScaleOptionID ASC";
                Comp3NonRatingGrpTable = Comp3NonRatingDv.ToTable();

                var ActScaleOptionID = ActualSPNonRatingScaleTable.Columns["ScaleOptionID"];
                var TrgScaleOptionID = Comp3NonRatingGrpTable.Columns["ScaleOptionID"];
                var ActsurveyformitemID = ActualSPNonRatingScaleTable.Columns["ItemID"];
                var TrgSurveyformItemID = Comp3NonRatingGrpTable.Columns["SurveyFormItemID"];

                var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["Comp3AverageScore"];
                var TrgNonRatingscore = Comp3NonRatingGrpTable.Columns["Response Rate"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in Comp3NonRatingGrpTable.Rows)
                    {
                        if ((RowTbl1[ActsurveyformitemID].ToString() == RowTbl2[TrgSurveyformItemID].ToString()) && (RowTbl1[ActScaleOptionID].ToString() == RowTbl2[TrgScaleOptionID].ToString()))
                        {
                            string TrgNonRatingScaleAverageScore = RowTbl2[TrgNonRatingscore].ToString() + ".00";
                            if (RowTbl1[ActNonRatingScore].ToString() != TrgNonRatingScaleAverageScore)
                            {
                                String ReportNames = "ResultReport_NonRatingScale";
                                nonrsflag = false;
                                WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 3, DBConnectionParameters, RowTbl2[TrgScaleOptionID].ToString());
                                break;
                            }
                        }
                    }
                }


                /*DataTable ActualSPCommentTable;
                DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
                DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualCommentDv.RowFilter = "ItemType IN(6)";
                ActualCommentDv.Sort = "ItemID ASC";
                ActualSPCommentTable = ActualCommentDv.ToTable();


                DataTable CompGrp3TargetCommentTable;
                DataViewManager CompGrp3TargetCommentTabledvm = CompGrp3.DefaultViewManager;
                DataView CompGrp3TargetCommentDv = CompGrp3TargetCommentTabledvm.CreateDataView(CompGrp3.Tables[4]);
                CompGrp3TargetCommentDv.Sort = "SurveyformItemID ASC";
                CompGrp3TargetCommentTable = CompGrp3TargetCommentDv.ToTable();

                var ActSurveyformItemID = ActualSPCommentTable.Columns["ItemID"];
                var TrgCommentSurveyformItemID = CompGrp3TargetCommentTable.Columns["SurveyformItemID"];
                var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
                var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
                var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];

                var TrgCommentWordID = CompGrp3TargetCommentTable.Columns["WordID"];
                var TrgCommentText = CompGrp3TargetCommentTable.Columns["Word"];
                var TrgCommentWordFrequency = CompGrp3TargetCommentTable.Columns["WordFrequency"];

                /*foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp3TargetCommentTable.Rows)
                    {
                        if (RowTbl1[ActSurveyformItemID].ToString() == RowTbl2[TrgCommentSurveyformItemID].ToString())
                        {
                            if ((RowTbl1[ActCommentWordID] != RowTbl2[TrgCommentWordID]) || (RowTbl1[ActCommentText] != RowTbl2[TrgCommentText]))

                            {
                                wcflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }

                int WordCount = ActualSPCommentTable.Rows.Count;

                for (int i = 0; i < WordCount; i++)
                {
                    if (ActualSPCommentTable.Rows[i][8].ToString() == CompGrp3TargetCommentTable.Rows[i][1].ToString())
                    {
                        if (ActualSPCommentTable.Rows[i][57].ToString() != CompGrp3TargetCommentTable.Rows[i][2].ToString())
                        {
                            string ReportNames = "ResultReportComment";
                            wcflag = false;
                            WriteFailedData(DataCalculation, ReportNames, UserAnalyzeFilterId, 1, DBConnectionParameters, "", CompGrp3TargetCommentTable.Rows[i][2].ToString(), i);
                            break;

                        }
                    }
                }


                DataTable ActualSPFFTable;
                DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
                DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
                ActualFFDv.Sort = "ItemID ASC";
                ActualSPFFTable = ActualFFDv.ToTable();

                DataTable CompGrp3TargetFFTable;
                DataViewManager CompGrp3TargetFFTabledvm = CompGrp3.DefaultViewManager;
                DataView CompGrp3TargetFFDv = CompGrp3TargetFFTabledvm.CreateDataView(CompGrp3.Tables[5]);
                CompGrp3TargetFFDv.Sort = "ItemID ASC";
                CompGrp3TargetFFTable = CompGrp3TargetFFDv.ToTable();
                var ActFFItemID = ActualSPCommentTable.Columns["ItemID"];
                var ActFFText = ActualSPCommentTable.Columns["CommentText"];
                var TrgFFItemID = CompGrp3TargetCommentTable.Columns["ItemID"];
                var TrgFFText = CompGrp3TargetCommentTable.Columns["Comments"];

               /* foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp3TargetFFTable.Rows)
                    {
                        if ((RowTbl1[ActFFItemID] == RowTbl2[TrgFFItemID]))

                        {
                            if ((RowTbl1[ActFFText] != RowTbl2[TrgFFText]))
                            {
                                ffflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }*/

            }

            return thirdlevelcomparison && rsflag && nonrsflag && wcflag && ffflag && bbflag;
        }
        /// <summary>
        /// Comparing the results of Insight SP and test SP with only target group
        /// </summary>
        /// <param name="DataCalculation"></param>
        /// <param name="ActualSP"></param>
        /// <param name="TargetGrp"></param>
        /// <returns></returns>
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp)
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("Inside CompareInsightReportResults method - Target group comparison", w);
                }
                bool strflag = true;
                bool oppflag = true;

                DataTable ActualSPStrengthTable;
                DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                StrengthDv.Sort = "objectId ASC";
                ActualSPStrengthTable = StrengthDv.ToTable();

                DataTable ActualSPOpportunitiesTable;
                DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                StrengthDv.Sort = "objectId ASC";
                ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

                //DataTable TargetSPStrengthTable;
                //DataViewManager TargetStrDVM = TargetGrp.DefaultViewManager;
                //DataView TargetStrDv = TargetStrDVM.CreateDataView(TargetGrp.Tables[0]);

                DataTable TargetGrpRatingScaleTable;
                DataViewManager TGrpDvm = TargetGrp.DefaultViewManager;
                DataView TGrpDv = TGrpDvm.CreateDataView(TargetGrp.Tables[0]);
                TGrpDv.Sort = "ObjectID ASC";
                TargetGrpRatingScaleTable = TGrpDv.ToTable();

                if (DataCalculation.ToLower() == "average")
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var TargetItemIDCol = TargetGrpRatingScaleTable.Columns["ObjectID"];

                    var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                    var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                    var TargetGrpMeanCol = TargetGrpRatingScaleTable.Columns["Mean"];

                    //var StrTotalResposeCol = ActualSPStrengthTable.Columns["TotalDistinctRespCount"];
                    //var OppTotalResposeCol = ActualSPOpportunitiesTable.Columns["TotalDistinctRespCount"];
                    var StrTotalResposeCol = ActualSPStrengthTable.Columns["TotalRespCount"];
                    var OppTotalResposeCol = ActualSPOpportunitiesTable.Columns["TotalRespCount"];
                    var TargetGrpResponseCol = TargetGrpRatingScaleTable.Columns["Total Responses"];

                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                            {
                                if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreCol])), 2) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpMeanCol])), 2))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrTotalResposeCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpResponseCol])), 0)))
                                {
                                    strflag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrAvgScoreCol].ToString(), w);
                                        Log("RowTbl2[" + TargetItemIDCol + "] = " + RowTbl2[TargetItemIDCol].ToString(), w);
                                        Log("RowTbl1[" + StrAvgScoreCol + "] = " + RowTbl1[StrAvgScoreCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpMeanCol + "] = " + RowTbl2[TargetGrpMeanCol].ToString(), w);
                                        Log("RowTbl1[" + StrTotalResposeCol + "] = " + RowTbl1[StrTotalResposeCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpResponseCol + "] = " + RowTbl2[TargetGrpResponseCol].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                            {
                                if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreCol])), 2) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpMeanCol])), 2))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppTotalResposeCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpResponseCol])), 0)))
                                {
                                    oppflag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                        Log("RowTbl2[" + TargetItemIDCol + "] = " + RowTbl2[TargetItemIDCol].ToString(), w);
                                        Log("RowTbl1[" + OppAvgScoreCol + "] = " + RowTbl1[OppAvgScoreCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpMeanCol + "] = " + RowTbl2[TargetGrpMeanCol].ToString(), w);
                                        Log("RowTbl1[" + OppTotalResposeCol + "] = " + RowTbl1[OppTotalResposeCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpResponseCol + "] = " + RowTbl2[TargetGrpResponseCol].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var TargetItemIDCol = TargetGrpRatingScaleTable.Columns["ObjectID"];

                    var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                    var StrNeuScoreCol = ActualSPStrengthTable.Columns["NeutralPerc"];
                    var StrUnFavScoreCol = ActualSPStrengthTable.Columns["UnFavPerc"];
                    var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                    var OppNeuScoreCol = ActualSPOpportunitiesTable.Columns["NeutralPerc"];
                    var OppUnFavScoreCol = ActualSPOpportunitiesTable.Columns["UnFavPerc"];

                    var TargetGrpFavorableCol = TargetGrpRatingScaleTable.Columns["Favorable"];
                    var TargetGrpNeutralCol = TargetGrpRatingScaleTable.Columns["Neutral"];
                    var TargetGrpUnfavorableCol = TargetGrpRatingScaleTable.Columns["Unfavorable"];

                    //var StrTotalResposeCol = ActualSPStrengthTable.Columns["TotalDistinctRespCount"];
                    //var OppTotalResposeCol = ActualSPOpportunitiesTable.Columns["TotalDistinctRespCount"];
                    var StrTotalResposeCol = ActualSPStrengthTable.Columns["TotalRespCount"];
                    var OppTotalResposeCol = ActualSPOpportunitiesTable.Columns["TotalRespCount"];
                    var TargetGrpResponseCol = TargetGrpRatingScaleTable.Columns["Total Responses"];

                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                            {
                                if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpFavorableCol])), 0))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrNeuScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpNeutralCol])), 0))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrUnFavScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpUnfavorableCol])), 0))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrTotalResposeCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpResponseCol])), 0)))
                                {
                                    strflag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrObjectIDCol].ToString(), w);
                                        Log("RowTbl2[" + TargetItemIDCol + "] = " + RowTbl2[TargetItemIDCol].ToString(), w);
                                        Log("RowTbl1[" + StrFavScoreCol + "] = " + RowTbl1[StrFavScoreCol].ToString(), w);
                                        Log("RowTbl1[" + StrNeuScoreCol + "] = " + RowTbl1[StrNeuScoreCol].ToString(), w);
                                        Log("RowTbl1[" + StrUnFavScoreCol + "] = " + RowTbl1[StrUnFavScoreCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpFavorableCol + "] = " + RowTbl2[TargetGrpFavorableCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNeutralCol + "] = " + RowTbl2[TargetGrpNeutralCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpUnfavorableCol + "] = " + RowTbl2[TargetGrpUnfavorableCol].ToString(), w);
                                        Log("RowTbl1[" + StrTotalResposeCol + "] = " + RowTbl1[StrTotalResposeCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpResponseCol + "] = " + RowTbl2[TargetGrpResponseCol].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                            {
                                if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpFavorableCol])), 0))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppNeuScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpNeutralCol])), 0))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppUnFavScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpUnfavorableCol])), 0))
                                    || (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppTotalResposeCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpResponseCol])), 0)))
                                {
                                    oppflag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                        Log("RowTbl2[" + TargetItemIDCol + "] = " + RowTbl2[TargetItemIDCol].ToString(), w);
                                        Log("RowTbl1[" + OppFavScoreCol + "] = " + RowTbl1[OppFavScoreCol].ToString(), w);
                                        Log("RowTbl1[" + OppNeuScoreCol + "] = " + RowTbl1[OppNeuScoreCol].ToString(), w);
                                        Log("RowTbl1[" + OppUnFavScoreCol + "] = " + RowTbl1[OppUnFavScoreCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpFavorableCol + "] = " + RowTbl2[TargetGrpFavorableCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNeutralCol + "] = " + RowTbl2[TargetGrpNeutralCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpUnfavorableCol + "] = " + RowTbl2[TargetGrpUnfavorableCol].ToString(), w);
                                        Log("RowTbl1[" + OppTotalResposeCol + "] = " + RowTbl1[OppTotalResposeCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpResponseCol + "] = " + RowTbl2[TargetGrpResponseCol].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                return strflag && oppflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            try
            {
                bool firstlevelcomparison = CompareInsightReportResults(DataCalculation, ActualSP, TargetGrp);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("Inside CompareInsightReportResults method - Comp group 1 comparison", w);
                }
                bool strflag = true;
                bool oppflag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp1.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareInsightReportBenchmarkResults(DataCalculation, ActualSP, CompGrp1, 1);

                }
                else
                {
                    DataTable ActualSPStrengthTable;
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    StrengthDv.Sort = "objectId ASC";
                    ActualSPStrengthTable = StrengthDv.ToTable();

                    DataTable ActualSPOpportunitiesTable;
                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    StrengthDv.Sort = "objectId ASC";
                    ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

                    DataTable CompGrp1RatingScaleTable;
                    DataViewManager CompGrp1Dvm = CompGrp1.DefaultViewManager;
                    DataView CompGrp1Dv = CompGrp1Dvm.CreateDataView(CompGrp1.Tables[0]);
                    CompGrp1Dv.Sort = "ObjectID ASC";
                    CompGrp1RatingScaleTable = CompGrp1Dv.ToTable();


                    if (DataCalculation.ToLower() == "average")
                    {
                        var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                        var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                        var CompGrp1ItemIDCol = CompGrp1RatingScaleTable.Columns["ObjectID"];

                        var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                        var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                        var StrAvgScoreColDiff = ActualSPStrengthTable.Columns["Comp_One_Mean_Diff"];
                        var OppAvgScoreColDiff = ActualSPOpportunitiesTable.Columns["Comp_One_Mean_Diff"];
                        var CompGrp1MeanCol = CompGrp1RatingScaleTable.Columns["Mean"];

                        //var StrTotalResposeCol = ActualSPStrengthTable.Columns["Comp_One_TotalRespCount_Diff"];
                        //var OppTotalResposeCol = ActualSPStrengthTable.Columns["Comp_One_TotalRespCount_Diff"];
                        //var TargetGrpResponseCol = CompGrp1RatingScaleTable.Columns["Total Responses"];

                        foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                            {
                                if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreColDiff])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1MeanCol])), 2))))
                                    {
                                        strflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp1ItemIDCol + "] = " + RowTbl2[CompGrp1ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + StrAvgScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreColDiff])), 2).ToString(), w);
                                            Log("RowTbl2[" + StrAvgScoreCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1MeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                            {
                                if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreColDiff])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1MeanCol])), 2))))
                                    {
                                        oppflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp1ItemIDCol + "] = " + RowTbl2[CompGrp1ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + OppAvgScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreColDiff])), 2).ToString(), w);
                                            Log("RowTbl2[" + OppAvgScoreCol + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1MeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                        var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                        var CompGrp1ItemIDCol = CompGrp1RatingScaleTable.Columns["ObjectID"];

                        var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                        var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                        var StrFavScoreColDiff = ActualSPStrengthTable.Columns["Comp_One_FavPerc_Diff"];
                        var OppFavScoreColDiff = ActualSPOpportunitiesTable.Columns["Comp_One_FavPerc_Diff"];

                        var CompGrp1FavorableCol = CompGrp1RatingScaleTable.Columns["Favorable"];

                        //var StrTotalResposeCol = ActualSPStrengthTable.Columns["TotalDistinctRespCount"];
                        //var OppTotalResposeCol = ActualSPStrengthTable.Columns["TotalDistinctRespCount"];
                        //var TargetGrpResponseCol = CompGrp1RatingScaleTable.Columns["Total Responses"];

                        foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                            {
                                if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreColDiff])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1FavorableCol])), 0))))
                                    {
                                        strflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp1ItemIDCol + "] = " + RowTbl2[CompGrp1ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + StrFavScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreColDiff])), 0).ToString(), w);
                                            Log("RowTbl2[" + StrFavScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1FavorableCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                            {
                                if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreColDiff])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1FavorableCol])), 0))))
                                    {
                                        oppflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp1ItemIDCol + "] = " + RowTbl2[CompGrp1ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + OppFavScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreColDiff])), 0).ToString(), w);
                                            Log("RowTbl2[" + OppFavScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp1FavorableCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                return firstlevelcomparison && strflag && oppflag && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            try
            {
                bool secondlevelcomparison = CompareInsightReportResults(DataCalculation, ActualSP, TargetGrp, CompGrp1);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("Inside CompareInsightReportResults method - Comp group 2 comparison", w);
                }
                bool strflag = true;
                bool oppflag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp2.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareInsightReportBenchmarkResults(DataCalculation, ActualSP, CompGrp2, 2);

                }
                else
                {
                    DataTable ActualSPStrengthTable;
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    StrengthDv.Sort = "objectId ASC";
                    ActualSPStrengthTable = StrengthDv.ToTable();

                    DataTable ActualSPOpportunitiesTable;
                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    StrengthDv.Sort = "objectId ASC";
                    ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

                    DataTable CompGrp2RatingScaleTable;
                    DataViewManager CompGrp2Dvm = CompGrp2.DefaultViewManager;
                    DataView CompGrp2Dv = CompGrp2Dvm.CreateDataView(CompGrp2.Tables[0]);
                    CompGrp2Dv.Sort = "ObjectID ASC";
                    CompGrp2RatingScaleTable = CompGrp2Dv.ToTable();

                    if (DataCalculation.ToLower() == "average")
                    {
                        var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                        var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                        var CompGrp2ItemIDCol = CompGrp2RatingScaleTable.Columns["ObjectID"];

                        var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                        var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                        var StrAvgScoreColDiff = ActualSPStrengthTable.Columns["Comp_Two_Mean_Diff"];
                        var OppAvgScoreColDiff = ActualSPOpportunitiesTable.Columns["Comp_Two_Mean_Diff"];
                        var CompGrp2MeanCol = CompGrp2RatingScaleTable.Columns["Mean"];

                        foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                            {
                                if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreColDiff])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2MeanCol])), 2))))
                                    {
                                        strflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp2ItemIDCol + "] = " + RowTbl2[CompGrp2ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + StrAvgScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreColDiff])), 2).ToString(), w);
                                            Log("RowTbl2[" + StrAvgScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2MeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                            {
                                if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreColDiff])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2MeanCol])), 2))))
                                    {
                                        oppflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp2ItemIDCol + "] = " + RowTbl2[CompGrp2ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + OppAvgScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreColDiff])), 2).ToString(), w);
                                            Log("RowTbl2[" + OppAvgScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2MeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                        var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                        var CompGrp2ItemIDCol = CompGrp2RatingScaleTable.Columns["ObjectID"];

                        var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                        var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                        var StrFavScoreColDiff = ActualSPStrengthTable.Columns["Comp_Two_FavPerc_Diff"];
                        var OppFavScoreColDiff = ActualSPOpportunitiesTable.Columns["Comp_Two_FavPerc_Diff"];

                        var CompGrp2FavorableCol = CompGrp2RatingScaleTable.Columns["Favorable"];

                        foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                            {
                                if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreColDiff])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2FavorableCol])), 0))))
                                    {
                                        strflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp2ItemIDCol + "] = " + RowTbl2[CompGrp2ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + StrFavScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreColDiff])), 0).ToString(), w);
                                            Log("RowTbl2[" + StrFavScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2FavorableCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                            {
                                if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreColDiff])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2FavorableCol])), 0))))
                                    {
                                        oppflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp2ItemIDCol + "] = " + RowTbl2[CompGrp2ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + OppFavScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreColDiff])), 0).ToString(), w);
                                            Log("RowTbl2[" + OppFavScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp2FavorableCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                return secondlevelcomparison && strflag && oppflag && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            try
            {
                bool thirdlevelcomparison = CompareInsightReportResults(DataCalculation, ActualSP, TargetGrp, CompGrp1, CompGrp2);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("Inside CompareInsightReportResults method - Comp group 3 comparison", w);
                }
                bool strflag = true;
                bool oppflag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp3.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareInsightReportBenchmarkResults(DataCalculation, ActualSP, CompGrp3, 3);

                }
                else
                {

                    DataTable ActualSPStrengthTable;
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    StrengthDv.Sort = "objectId ASC";
                    ActualSPStrengthTable = StrengthDv.ToTable();

                    DataTable ActualSPOpportunitiesTable;
                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    StrengthDv.Sort = "objectId ASC";
                    ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

                    DataTable CompGrp3RatingScaleTable;
                    DataViewManager CompGrp3Dvm = CompGrp3.DefaultViewManager;
                    DataView CompGrp3Dv = CompGrp3Dvm.CreateDataView(CompGrp3.Tables[0]);
                    CompGrp3Dv.Sort = "ObjectID ASC";
                    CompGrp3RatingScaleTable = CompGrp3Dv.ToTable();

                    if (DataCalculation.ToLower() == "average")
                    {
                        var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                        var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                        var CompGrp3ItemIDCol = CompGrp3RatingScaleTable.Columns["ObjectID"];

                        var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                        var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                        var StrAvgScoreColDiff = ActualSPStrengthTable.Columns["Comp_Three_Mean_Diff"];
                        var OppAvgScoreColDiff = ActualSPOpportunitiesTable.Columns["Comp_Three_Mean_Diff"];

                        var CompGrp3MeanCol = CompGrp3RatingScaleTable.Columns["Mean"];

                        foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                            {
                                if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreColDiff])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3MeanCol])), 2))))
                                    {
                                        strflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp3ItemIDCol + "] = " + RowTbl2[CompGrp3ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + StrAvgScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreColDiff])), 2).ToString(), w);
                                            Log("RowTbl2[" + StrAvgScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3MeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                            {
                                if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreColDiff])), 2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3MeanCol])), 2))))
                                    {
                                        oppflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp3ItemIDCol + "] = " + RowTbl2[CompGrp3ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + OppAvgScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreColDiff])), 2).ToString(), w);
                                            Log("RowTbl2[" + OppAvgScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppAvgScoreCol])), 2) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3MeanCol])), 2)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                        var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                        var CompGrp3ItemIDCol = CompGrp3RatingScaleTable.Columns["ObjectID"];

                        var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                        var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                        var StrFavScoreColDiff = ActualSPStrengthTable.Columns["Comp_Three_FavPerc_Diff"];
                        var OppFavScoreColDiff = ActualSPOpportunitiesTable.Columns["Comp_Three_FavPerc_Diff"];

                        var CompGrp3FavorableCol = CompGrp3RatingScaleTable.Columns["Favorable"];

                        foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                            {
                                if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreColDiff])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3FavorableCol])), 0))))
                                    {
                                        strflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + StrObjectIDCol + "] = " + RowTbl1[StrObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp3ItemIDCol + "] = " + RowTbl2[CompGrp3ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + StrFavScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreColDiff])), 0).ToString(), w);
                                            Log("RowTbl2[" + StrFavScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[StrFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3FavorableCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                            {
                                if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                                {
                                    if ((Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreColDiff])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3FavorableCol])), 0))))
                                    {
                                        oppflag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + OppObjectIDCol + "] = " + RowTbl1[OppObjectIDCol].ToString(), w);
                                            Log("RowTbl2[" + CompGrp3ItemIDCol + "] = " + RowTbl2[CompGrp3ItemIDCol].ToString(), w);
                                            Log("RowTbl1[" + OppFavScoreColDiff + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreColDiff])), 0).ToString(), w);
                                            Log("RowTbl2[" + OppFavScoreColDiff + "] = " + (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[OppFavScoreCol])), 0) - Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[CompGrp3FavorableCol])), 0)).ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                return thirdlevelcomparison && strflag && oppflag && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        /// <summary>
        /// Comparing the results of Drill Down SP and test SP with only target group
        /// </summary>
        /// <param name="DataCalculation"></param>
        /// <param name="ActualSP"></param>
        /// <param name="TargetGrp"></param>
        /// <returns></returns>
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp)
        {
            bool ddflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            //DrillDownDv.RowFilter = "CategoryText Not IN('Unassigned')";
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable TargetGrpDrillDownTable;
            DataViewManager TGrpDvm = TargetGrp.DefaultViewManager;
            DataView TGrpDv = TGrpDvm.CreateDataView(TargetGrp.Tables[0]);
            TGrpDv.Sort = "ObjectID ASC";
            TargetGrpDrillDownTable = TGrpDv.ToTable();

            if (DataCalculation.ToLower() == "average")
            {
                var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                var TargetItemIDCol = TargetGrpDrillDownTable.Columns["ObjectID"];

                var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];
                var TargetGrpMeanCol = TargetGrpDrillDownTable.Columns["Mean"];
                foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDrillDownTable.Rows)
                    {
                        if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if ((RowTbl1[DrillDownAvgScoreCol] == DBNull.Value) || (RowTbl2[TargetGrpMeanCol] == DBNull.Value))
                            {
                                string Actualvalue = "0.00";
                                if (RowTbl2[TargetGrpMeanCol] == DBNull.Value)

                                {
                                    if ((RowTbl1[DrillDownAvgScoreCol].ToString() != RowTbl2[TargetGrpMeanCol].ToString()))
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                        break;
                                    }
                                }

                                else
                                {
                                    if (Actualvalue.ToString() != RowTbl2[TargetGrpMeanCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                        break;
                                    }
                                }

                            }
                            else
                            {

                                if ((RowTbl1[DrillDownAvgScoreCol].ToString()) != (RowTbl2[TargetGrpMeanCol].ToString()))
                                {

                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                var TargetItemIDCol = TargetGrpDrillDownTable.Columns["ObjectID"];

                var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                var DrillDownNeuScoreCol = ActualSPDrillDownTable.Columns["NeutralPercent"];
                var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                var TargetGrpFavorableCol = TargetGrpDrillDownTable.Columns["Favorable"];
                var TargetGrpNeutralCol = TargetGrpDrillDownTable.Columns["Neutral"];
                var TargetGrpUnfavorableCol = TargetGrpDrillDownTable.Columns["Unfavorable"];
                foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDrillDownTable.Rows)
                    {
                        if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if (RowTbl1[DrillDownFavScoreCol] == DBNull.Value || RowTbl1[DrillDownNeuScoreCol] == DBNull.Value || RowTbl1[DrillDownUnFavScoreCol] == DBNull.Value)
                            {
                                var favpercentage = 0; var Neupercentage = 0; var UnfavPercantage = 0;
                                if (favpercentage != Convert.ToInt32(RowTbl2[TargetGrpFavorableCol]) || Neupercentage != Convert.ToInt32(RowTbl2[TargetGrpNeutralCol]) || UnfavPercantage != Convert.ToInt32(RowTbl2[TargetGrpUnfavorableCol]))
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                    break;
                                }
                            }
                            else
                            {
                                if (((RowTbl1[DrillDownFavScoreCol].ToString()) != (RowTbl2[TargetGrpFavorableCol].ToString())) || ((RowTbl1[DrillDownNeuScoreCol].ToString()) != (RowTbl2[TargetGrpNeutralCol].ToString()))
                                     || ((RowTbl1[DrillDownUnFavScoreCol].ToString()) != (RowTbl2[TargetGrpUnfavorableCol].ToString())))
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 0, DBConnectionParameters);
                                    break;
                                }
                            }

                        }
                    }
                }
            }
            return ddflag;
        }
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool firstlevelcomparison = CompareDrillDownReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp);
            bool ddflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable CompGrp1DrillDownTable;
            DataViewManager CompGrp1Dvm = CompGrp1.DefaultViewManager;
            DataView CompGrp1Dv = CompGrp1Dvm.CreateDataView(CompGrp1.Tables[0]);
            CompGrp1DrillDownTable = CompGrp1Dv.ToTable();
            DataColumnCollection Columns = CompGrp1DrillDownTable.Columns;
            if (Columns.Contains("BenchmarkFavPerc") == true)
            {

                bbflag = CompareDrillDownReportBenchmarkResults(DataCalculation, ActualSP, CompGrp1, "Comp1", ReportName, UserAnalyzeFilterId, DBConnectionParameters);

            }
            else
            {
                DataTable CompGrp1DrillDownTable1;
                DataViewManager CompGrp1Dvm1 = CompGrp1.DefaultViewManager;
                DataView CompGrp1Dv1 = CompGrp1Dvm1.CreateDataView(CompGrp1.Tables[0]);
                CompGrp1Dv1.Sort = "ObjectID Asc";
                CompGrp1DrillDownTable1 = CompGrp1Dv1.ToTable();
                if (DataCalculation.ToLower() == "average")
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp1DrillDownTable1.Columns["ObjectID"];

                    var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];
                    var DrillDownAvgDiffScoreCol = ActualSPDrillDownTable.Columns["Comp1DiffAverageScore"];
                    var CompGrp1MeanCol = CompGrp1DrillDownTable1.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1DrillDownTable1.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if ((RowTbl1[DrillDownAvgDiffScoreCol] == DBNull.Value) || (RowTbl2[CompGrp1MeanCol] == DBNull.Value) || RowTbl1[DrillDownAvgScoreCol] == DBNull.Value)
                                {

                                    if (RowTbl1[DrillDownAvgDiffScoreCol].ToString() != RowTbl2[CompGrp1MeanCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                        break;
                                    }

                                }
                                else
                                {
                                    var Diff_Comp1 = (Convert.ToDecimal(RowTbl1[DrillDownAvgScoreCol]) - Convert.ToDecimal(RowTbl2[CompGrp1MeanCol]));
                                    if (Convert.ToDecimal(RowTbl1[DrillDownAvgDiffScoreCol]) != Diff_Comp1)
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp1DrillDownTable1.Columns["ObjectID"];

                    var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                    var DrillDownDiffFavScoreCol = ActualSPDrillDownTable.Columns["Comp1DiffFavPercent"];
                    // var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                    var CompGrp1FavorableCol = CompGrp1DrillDownTable1.Columns["Favorable"];
                    var CompGrp1NeutralCol = CompGrp1DrillDownTable1.Columns["Neutral"];
                    var CompGrp1UnfavorableCol = CompGrp1DrillDownTable1.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1DrillDownTable1.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if ((RowTbl1[DrillDownDiffFavScoreCol] == DBNull.Value) || (RowTbl1[DrillDownFavScoreCol] == DBNull.Value))
                                {
                                    var Actualvalue = 0;
                                    if (Actualvalue != Convert.ToInt32(RowTbl2[CompGrp1FavorableCol]))
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                        break;
                                    }
                                }


                                else
                                {

                                    var Diff_comp1 = (Convert.ToInt32(RowTbl1[DrillDownFavScoreCol]) - Convert.ToInt32(RowTbl2[CompGrp1FavorableCol]));
                                    if (Convert.ToInt32(RowTbl1[DrillDownDiffFavScoreCol]) != Diff_comp1)
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 1, DBConnectionParameters);
                                        break;
                                    }
                                }

                            }
                        }
                    }
                }
            }
            return firstlevelcomparison && ddflag && bbflag;
        }
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool secondlevelcomparison = CompareDrillDownReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1);
            bool ddflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable CompGrp2DrillDownTable1;
            DataViewManager CompGrp2Dvm1 = CompGrp2.DefaultViewManager;
            DataView CompGrp2Dv1 = CompGrp2Dvm1.CreateDataView(CompGrp2.Tables[0]);
            CompGrp2DrillDownTable1 = CompGrp2Dv1.ToTable();
            DataColumnCollection Columns = CompGrp2DrillDownTable1.Columns;
            if (Columns.Contains("BenchmarkFavPerc") == true)
            {
                bbflag = CompareDrillDownReportBenchmarkResults(DataCalculation, ActualSP, CompGrp2, "Comp2", ReportName, UserAnalyzeFilterId, DBConnectionParameters);

            }
            else
            {
                DataTable CompGrp2DrillDownTable;
                DataViewManager CompGrp2Dvm = CompGrp2.DefaultViewManager;
                DataView CompGrp2Dv = CompGrp2Dvm.CreateDataView(CompGrp2.Tables[0]);
                CompGrp2Dv.Sort = "ObjectID ASC";
                CompGrp2DrillDownTable = CompGrp2Dv.ToTable();


                if (DataCalculation.ToLower() == "average")
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp2ItemIDCol = CompGrp2DrillDownTable.Columns["ObjectID"];

                    var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];
                    var DrillDownAvgDiffScoreCol = ActualSPDrillDownTable.Columns["Comp2DiffAverageScore"];
                    var CompGrp2MeanCol = CompGrp2DrillDownTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                if ((RowTbl1[DrillDownAvgDiffScoreCol] == DBNull.Value) || (RowTbl2[CompGrp2MeanCol] == DBNull.Value) || RowTbl1[DrillDownAvgScoreCol] == DBNull.Value)
                                {
                                    if (RowTbl1[DrillDownAvgDiffScoreCol].ToString() != RowTbl2[CompGrp2MeanCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 2, DBConnectionParameters);
                                        break;
                                    }

                                }
                                else
                                {

                                    var Diff_Comp2 = (Convert.ToDecimal(RowTbl1[DrillDownAvgScoreCol]) - Convert.ToDecimal(RowTbl2[CompGrp2MeanCol]));
                                    if (Convert.ToDecimal(RowTbl1[DrillDownAvgDiffScoreCol]) != Diff_Comp2)
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 2, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp2ItemIDCol = CompGrp2DrillDownTable.Columns["ObjectID"];

                    var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                    var DrillDownDiffFavScoreCol = ActualSPDrillDownTable.Columns["Comp2DiffFavPercent"];
                    var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                    var CompGrp2FavorableCol = CompGrp2DrillDownTable.Columns["Favorable"];
                    var CompGrp2NeutralCol = CompGrp2DrillDownTable.Columns["Neutral"];
                    var CompGrp2UnfavorableCol = CompGrp2DrillDownTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                if ((RowTbl1[DrillDownDiffFavScoreCol] == DBNull.Value) || (RowTbl1[DrillDownFavScoreCol] == DBNull.Value))
                                {
                                    var Actualvalue = 0;
                                    if (Actualvalue != Convert.ToInt32(RowTbl2[CompGrp2FavorableCol]))
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 2, DBConnectionParameters);
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_comp2 = (Convert.ToInt32(RowTbl1[DrillDownFavScoreCol]) - Convert.ToInt32(RowTbl2[CompGrp2FavorableCol]));
                                    if (Convert.ToInt32(RowTbl1[DrillDownDiffFavScoreCol]) != Diff_comp2)
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 2, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return secondlevelcomparison && ddflag && bbflag;
        }
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool thirdlevelcomparison = CompareDrillDownReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1, CompGrp2);
            bool ddflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable CompGrp3DrillDownTable1;
            DataViewManager CompGrp3Dvm1 = CompGrp3.DefaultViewManager;
            DataView CompGrp3Dv1 = CompGrp3Dvm1.CreateDataView(CompGrp3.Tables[0]);
            CompGrp3DrillDownTable1 = CompGrp3Dv1.ToTable();
            DataColumnCollection Columns = CompGrp3DrillDownTable1.Columns;
            if (Columns.Contains("BenchmarkFavPerc") == true)
            {
                bbflag = CompareDrillDownReportBenchmarkResults(DataCalculation, ActualSP, CompGrp3, "Comp3", ReportName, UserAnalyzeFilterId, DBConnectionParameters);

            }
            else
            {
                DataTable CompGrp3DrillDownTable;
                DataViewManager CompGrp3Dvm = CompGrp3.DefaultViewManager;
                DataView CompGrp3Dv = CompGrp3Dvm.CreateDataView(CompGrp3.Tables[0]);
                CompGrp3Dv1.Sort = "ObjectID ASC";
                CompGrp3DrillDownTable = CompGrp3Dv.ToTable();

                if (DataCalculation.ToLower() == "average")
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp3DrillDownTable.Columns["ObjectID"];
                    var DrillDownAvgDiffScoreCol = ActualSPDrillDownTable.Columns["Comp3DiffAverageScore"];
                    var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];

                    var CompGr3MeanCol = CompGrp3DrillDownTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if ((RowTbl1[DrillDownAvgDiffScoreCol] == DBNull.Value) || (RowTbl2[CompGr3MeanCol] == DBNull.Value) || RowTbl1[DrillDownAvgScoreCol] == DBNull.Value)
                                {
                                    if (RowTbl1[DrillDownAvgDiffScoreCol].ToString() != RowTbl2[CompGr3MeanCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                        break;
                                    }

                                }
                                else
                                {
                                    var Diff_Comp3 = (Convert.ToDecimal(RowTbl1[DrillDownAvgScoreCol]) - Convert.ToDecimal(RowTbl2[CompGr3MeanCol]));
                                    if (Convert.ToDecimal(RowTbl1[DrillDownAvgDiffScoreCol]) != Diff_Comp3)
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp3DrillDownTable.Columns["ObjectID"];

                    var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                    var DrillDownDiffFavScoreCol = ActualSPDrillDownTable.Columns["Comp3DiffFavPercent"];
                    var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                    var CompGrp3FavorableCol = CompGrp3DrillDownTable.Columns["Favorable"];
                    var CompGrp3NeutralCol = CompGrp3DrillDownTable.Columns["Neutral"];
                    var CompGrp3UnfavorableCol = CompGrp3DrillDownTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if ((RowTbl1[DrillDownDiffFavScoreCol] == DBNull.Value) || (RowTbl1[DrillDownFavScoreCol] == DBNull.Value))
                                {
                                    var actualvalue = 0;

                                    if (actualvalue != Convert.ToInt32(RowTbl2[CompGrp3FavorableCol]))
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                        break;
                                    }
                                }

                                else
                                {
                                    var Diff_comp2 = (Convert.ToInt32(RowTbl1[DrillDownFavScoreCol]) - Convert.ToInt32(RowTbl2[CompGrp3FavorableCol]));
                                    if (Convert.ToInt32(RowTbl1[DrillDownDiffFavScoreCol]) != Diff_comp2)
                                    {
                                        ddflag = false;
                                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, 3, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return thirdlevelcomparison && ddflag && bbflag;
        }

        /// <summary>
        /// Comparing the results of trend Sp and Test Sp with only target group
        /// </summary>
        /// <param name  ="DataCalculation"></param>
        /// <param name  ="ActualSP"></param>
        /// <param name  ="TargetGrp"></param>
        /// <returns></returns>

        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp)
        {
            try
            {
                bool RSFlag = true;

                DataTable ActualSPRatingTrendTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                ActualDv.Sort = "GroupBy ASC";
                ActualSPRatingTrendTable = ActualDv.ToTable();

                DataTable TargetGrpRatingTrendTable;
                DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
                DataView TargetGrpDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
                TargetGrpDv.Sort = "GroupBy ASC";
                TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                if (Datacalculation.ToLower() == "average")
                {
                    var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["MeanScore"];
                    var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["TotalDistinctRespCount"];
                    var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                            {
                                if (RowTbl1[TrendOverallMeanCol].ToString() != RowTbl2[TargetOverallMeanCol].ToString()
                                    || RowTbl1[TrendResponseCountCol].ToString() != RowTbl2[TargetResponseCountCol].ToString())
                                {
                                    RSFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + TrendOverallMeanCol + "] = " + RowTbl1[TrendOverallMeanCol].ToString(), w);
                                        Log("RowTbl2[" + TargetOverallMeanCol + "] = " + RowTbl2[TargetOverallMeanCol].ToString(), w);
                                        Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                        Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                }
                else
                {
                    var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    var TrendFavCol = ActualSPRatingTrendTable.Columns["FavPerc"];
                    var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["TotalDistinctRespCount"];
                    var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendFavCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetFavCol])), 0)
                                    || Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendResponseCountCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetResponseCountCol])), 0))
                                {
                                    RSFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + TrendFavCol + "] = " + RowTbl1[TrendFavCol].ToString(), w);
                                        Log("RowTbl2[" + TargetFavCol + "] = " + RowTbl2[TargetFavCol].ToString(), w);
                                        Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                        Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                    }
                                    break;
                                }
                            }

                        }

                    }
                }
                return RSFlag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            try
            {
                bool FirstLevelComparison = CompareRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp1.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp1);
                }
                else
                {
                    DataTable ActualSPRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();

                    DataTable TargetGrpRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
                    DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
                    TargetGrpDv.Sort = "GroupBy ASC";
                    TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    if (Datacalculation.ToLower() == "average")
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["Comp_One_Mean"];
                        var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_One_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendOverallMeanCol].ToString() != RowTbl2[TargetOverallMeanCol].ToString()
                                        || RowTbl1[TrendResponseCountCol].ToString() != RowTbl2[TargetResponseCountCol].ToString())
                                    {
                                        RSFlag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + TrendOverallMeanCol + "] = " + RowTbl1[TrendOverallMeanCol].ToString(), w);
                                            Log("RowTbl2[" + TargetOverallMeanCol + "] = " + RowTbl2[TargetOverallMeanCol].ToString(), w);
                                            Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                            Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendFavCol = ActualSPRatingTrendTable.Columns["Comp_One_FavPerc"];
                        var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_One_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendFavCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetFavCol])), 0)
                                        || Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendResponseCountCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetResponseCountCol])), 0))
                                    {
                                        RSFlag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + TrendFavCol + "] = " + RowTbl1[TrendFavCol].ToString(), w);
                                            Log("RowTbl2[" + TargetFavCol + "] = " + RowTbl2[TargetFavCol].ToString(), w);
                                            Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                            Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                        }
                                        break;
                                    }
                                }

                            }

                        }
                    }
                }

                return FirstLevelComparison && RSFlag & bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            try
            {
                bool SecondLevelComparison = CompareRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp, CompGrp1);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp2.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp2);
                }
                else
                {
                    DataTable ActualSPRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();

                    DataTable TargetGrpRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
                    DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
                    TargetGrpDv.Sort = "GroupBy ASC";
                    TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    if (Datacalculation.ToLower() == "average")
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["Comp_Two_Mean"];
                        var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Two_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendOverallMeanCol].ToString() != RowTbl2[TargetOverallMeanCol].ToString()
                                        || RowTbl1[TrendResponseCountCol].ToString() != RowTbl2[TargetResponseCountCol].ToString())
                                    {
                                        RSFlag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + TrendOverallMeanCol + "] = " + RowTbl1[TrendOverallMeanCol].ToString(), w);
                                            Log("RowTbl2[" + TargetOverallMeanCol + "] = " + RowTbl2[TargetOverallMeanCol].ToString(), w);
                                            Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                            Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendFavCol = ActualSPRatingTrendTable.Columns["Comp_Two_FavPerc"];
                        var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Two_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendFavCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetFavCol])), 0)
                                        || Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendResponseCountCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetResponseCountCol])), 0))
                                    {
                                        RSFlag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + TrendFavCol + "] = " + RowTbl1[TrendFavCol].ToString(), w);
                                            Log("RowTbl2[" + TargetFavCol + "] = " + RowTbl2[TargetFavCol].ToString(), w);
                                            Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                            Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                        }
                                        break;
                                    }
                                }

                            }

                        }
                    }
                }
                return SecondLevelComparison && RSFlag && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            try
            {
                bool ThirdLevelComparison = CompareRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp, CompGrp1, CompGrp2);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp3.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp3);
                }
                else
                {
                    DataTable ActualSPRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();

                    DataTable TargetGrpRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp3.DefaultViewManager;
                    DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
                    TargetGrpDv.Sort = "GroupBy ASC";
                    TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    if (Datacalculation.ToLower() == "average")
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["Comp_Three_Mean"];
                        var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Three_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendOverallMeanCol].ToString() != RowTbl2[TargetOverallMeanCol].ToString()
                                        || RowTbl1[TrendResponseCountCol].ToString() != RowTbl2[TargetResponseCountCol].ToString())
                                    {
                                        RSFlag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + TrendOverallMeanCol + "] = " + RowTbl1[TrendOverallMeanCol].ToString(), w);
                                            Log("RowTbl2[" + TargetOverallMeanCol + "] = " + RowTbl2[TargetOverallMeanCol].ToString(), w);
                                            Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                            Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendFavCol = ActualSPRatingTrendTable.Columns["Comp_Three_FavPerc"];
                        var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Three_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendFavCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetFavCol])), 0)
                                        || Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendResponseCountCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetResponseCountCol])), 0))
                                    {
                                        RSFlag = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            Log("RowTbl1[" + TrendFavCol + "] = " + RowTbl1[TrendFavCol].ToString(), w);
                                            Log("RowTbl2[" + TargetFavCol + "] = " + RowTbl2[TargetFavCol].ToString(), w);
                                            Log("RowTbl1[" + TrendResponseCountCol + "] = " + RowTbl1[TrendResponseCountCol].ToString(), w);
                                            Log("RowTbl2[" + TargetResponseCountCol + "] = " + RowTbl2[TargetResponseCountCol].ToString(), w);
                                        }
                                        break;
                                    }
                                }

                            }

                        }
                    }
                }
                return ThirdLevelComparison && RSFlag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp,String ScaleOptionID)
        {
            try
            {
                bool NRFlag = true;
               
                DataTable TargetGrpSPRatingTrendTable;
                DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
                DataView TargetDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
                TargetDv.RowFilter = "ScaleoptionID IN(" + ScaleOptionID + ")";
                TargetDv.Sort = "GroupBy ASC";
                TargetGrpSPRatingTrendTable = TargetDv.ToTable();

                DataTable ActualSPNonRatingTrendTable;
                DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
                ActualDV.Sort = "GroupBy ASC";
                ActualSPNonRatingTrendTable = ActualDV.ToTable();

                var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
                var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["MeanScore"];
                var TrendTotalScaleOptionRespCount = ActualSPNonRatingTrendTable.Columns["TotalRespCount"];

                var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];
                var TargetGrpTotalScaleOptionRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                    {
                        if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                        {
                            if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0)
                                || CommonActions.IfNullThenZero(RowTbl1[TrendTotalScaleOptionRespCount]) != CommonActions.IfNullThenZero(RowTbl2[TargetGrpTotalScaleOptionRespCount]))
                            {
                                NRFlag = false;
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    Log("Trend Chart Non Rating Target group comparison", w);
                                    Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                    Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                    Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                    Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                    Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0), w);
                                    Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0), w);
                                    Log("RowTbl1[" + TrendTotalScaleOptionRespCount + "] = " + RowTbl1[TrendTotalScaleOptionRespCount].ToString(), w);
                                    Log("RowTbl2[" + TargetGrpTotalScaleOptionRespCount + "] = " + RowTbl2[TargetGrpTotalScaleOptionRespCount].ToString(), w);
                                }
                                break;
                            }
                        }
                    }
                }
                return NRFlag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }


        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1,string scaleOptionId)
        {
            try
            {
                bool NRFlag = true;
                bool bbflag = true;
                bool FirstLevelComparison = CompareNonRatingTrendReportResult(ActualSP, TargetGrp,scaleOptionId);
                DataColumnCollection Columns = CompGrp1.Tables[0].Columns;
                if (Columns.Contains("BenchmarkScaleOptionId") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResultForNR(ActualSP, CompGrp1);
                }
                else
                {
                    DataTable TargetGrpSPRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
                    DataView TargetDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
                    TargetDv.RowFilter = "ScaleoptionID IN(" + scaleOptionId + ")";
                    TargetDv.Sort = "GroupBy ASC";
                    TargetGrpSPRatingTrendTable = TargetDv.ToTable();

                    DataTable ActualSPNonRatingTrendTable;
                    DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                    DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
                    ActualDV.Sort = "GroupBy ASC";
                    ActualSPNonRatingTrendTable = ActualDV.ToTable();

                    var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                    var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
                    var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["Comp_one_Mean"];
                    var TrendTotalScaleOptionRespCount = ActualSPNonRatingTrendTable.Columns["Comp_one_TotalRespCount"];

                    var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                    var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];
                    var TargetGrpTotalScaleOptionRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

                    foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0)
                                    || CommonActions.IfNullThenZero(RowTbl1[TrendTotalScaleOptionRespCount]) != CommonActions.IfNullThenZero(RowTbl2[TargetGrpTotalScaleOptionRespCount]))
                                {
                                    NRFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("Trend Chart Non Rating Comparison group 1 comparison", w);
                                        Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0), w);
                                        Log("RowTbl1[" + TrendTotalScaleOptionRespCount + "] = " + RowTbl1[TrendTotalScaleOptionRespCount].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpTotalScaleOptionRespCount + "] = " + RowTbl2[TargetGrpTotalScaleOptionRespCount].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                return NRFlag && bbflag && FirstLevelComparison;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }


        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, string scaleOptionId)
        {
            try
            {
                bool NRFlag = true;
                bool bbflag = true;
                bool SecondLevelComparison = CompareNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1,scaleOptionId);
                DataColumnCollection Columns = CompGrp2.Tables[0].Columns;
                if (Columns.Contains("BenchmarkScaleOptionId") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResultForNR(ActualSP, CompGrp2);
                }
                else
                {
                    //string ScaleOption = "";
                    DataTable TargetGrpSPRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
                    DataView TargetDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
                    TargetDv.RowFilter = "ScaleoptionID IN(" + scaleOptionId + ")";
                    TargetDv.Sort = "GroupBy ASC";
                    TargetGrpSPRatingTrendTable = TargetDv.ToTable();

                    DataTable ActualSPNonRatingTrendTable;
                    DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                    DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
                    ActualDV.Sort = "GroupBy ASC";
                    ActualSPNonRatingTrendTable = ActualDV.ToTable();

                    var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                    var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
                    var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["Comp_Two_Mean"];
                    var TrendTotalScaleOptionRespCount = ActualSPNonRatingTrendTable.Columns["Comp_Two_TotalRespCount"];

                    var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                    var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];
                    var TargetGrpTotalScaleOptionRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

                    foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0)
                                    || CommonActions.IfNullThenZero(RowTbl1[TrendTotalScaleOptionRespCount]) != CommonActions.IfNullThenZero(RowTbl2[TargetGrpTotalScaleOptionRespCount]))
                                {
                                    NRFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("Trend Chart Non Rating Comparison group 2 comparison", w);
                                        Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0), w);
                                        Log("RowTbl1[" + TrendTotalScaleOptionRespCount + "] = " + RowTbl1[TrendTotalScaleOptionRespCount].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpTotalScaleOptionRespCount + "] = " + RowTbl2[TargetGrpTotalScaleOptionRespCount].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                return NRFlag && bbflag && SecondLevelComparison;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }


        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3, string scaleOptionId)
        {
            try
            {
                bool NRFlag = true;
                bool bbflag = true;
                bool ThirdLevelComparison = CompareNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1, CompGrp2,scaleOptionId);
              
                DataColumnCollection Columns = CompGrp3.Tables[0].Columns;
                if (Columns.Contains("BenchmarkScaleOptionId") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResultForNR(ActualSP, CompGrp3);
                }
                else
                {
                    //string ScaleOption = "";
                    DataTable TargetGrpSPRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp3.DefaultViewManager;
                    DataView TargetDv = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
                    TargetDv.RowFilter = "ScaleoptionID IN(" + scaleOptionId + ")";
                    TargetDv.Sort = "GroupBy ASC";
                    TargetGrpSPRatingTrendTable = TargetDv.ToTable();

                    DataTable ActualSPNonRatingTrendTable;
                    DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                    DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
                    ActualDV.Sort = "GroupBy ASC";
                    ActualSPNonRatingTrendTable = ActualDV.ToTable();

                    var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                    var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
                    var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["Comp_Three_Mean"];
                    var TrendTotalScaleOptionRespCount = ActualSPNonRatingTrendTable.Columns["Comp_Three_TotalRespCount"];

                    var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                    var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];
                    var TargetGrpTotalScaleOptionRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

                    foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0)
                                    || CommonActions.IfNullThenZero(RowTbl1[TrendTotalScaleOptionRespCount]) != CommonActions.IfNullThenZero(RowTbl2[TargetGrpTotalScaleOptionRespCount]))
                                {
                                    NRFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("Trend Chart Non Rating Comparison group 3 comparison", w);
                                        Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0), w);
                                        Log("RowTbl1[" + TrendTotalScaleOptionRespCount + "] = " + RowTbl1[TrendTotalScaleOptionRespCount].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpTotalScaleOptionRespCount + "] = " + RowTbl2[TargetGrpTotalScaleOptionRespCount].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                return NRFlag && bbflag &&ThirdLevelComparison;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }


        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, string Show, DataSet ActualSP, DataSet TargetGrp)
        {
            try
            {
                bool RSFlag = true;

                DataTable ActualSPRatingTrendTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                if (Show.ToLower() != "category")
                {
                    ActualDv.RowFilter = "ObjectTypeId IN(4) AND GroupBY NOT IN('All')";
                    //ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();
                }
                else
                {
                    ActualDv.RowFilter = "ObjectTypeId IN(3) AND GroupBY NOT IN('All')";
                    //ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();
                }

                DataTable TargetGrpRatingTrendTable;
                DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
                DataView TargetGrpDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
                TargetGrpDv.Sort = "GroupBy ASC";
                TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                if (Datacalculation.ToLower() == "average")
                {
                    var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (RowTbl1[ActualSPAverageValue].ToString() != RowTbl2[TargetGrpAverageValue].ToString())
                                {
                                    RSFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + ActualSPAverageValue + "] = " + RowTbl1[ActualSPAverageValue].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpAverageValue + "] = " + RowTbl2[TargetGrpAverageValue].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }


                }
                else
                {
                    var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPPercentageValue])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpPerCentageValue])), 0))
                                {
                                    RSFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("RowTbl1[" + ActualSPPercentageValue + "] = " + RowTbl1[ActualSPPercentageValue].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpPerCentageValue + "] = " + RowTbl2[TargetGrpPerCentageValue].ToString(), w);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                return RSFlag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, string Show, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            try
            {
                bool FirstLevelComparison = CompareHeapRatingTrendReportResult(Datacalculation, Show, ActualSP, TargetGrp);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp1.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareHeatMapBenchmarkResult(Datacalculation, Show, ActualSP, CompGrp1);
                }
                else
                {
                    //DataTable ActualSPRatingTrendTable;
                    //DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    //DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    //ActualDv.RowFilter = "ObjectTypeId IN(4)";
                    //ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    //ActualDv.Sort = "GroupBy ASC";
                    //ActualSPRatingTrendTable = ActualDv.ToTable();

                    //DataTable TargetGrpRatingTrendTable;
                    //DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
                    //DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
                    //TargetGrpDv.Sort = "GroupBy ASC";
                    //TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    //if (Datacalculation.ToLower() == "average")
                    //{
                    //    var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                    //    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    //    var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    //    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    //    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    //    {
                    //        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    //        {
                    //            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                    //            {
                    //                if (RowTbl1[ActualSPAverageValue] != RowTbl2[TargetGrpGroupBy])
                    //                {
                    //                    RSFlag = false;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }


                    //}
                    //else
                    //{
                    //    var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                    //    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    //    var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    //    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    //    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    //    {
                    //        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    //        {
                    //            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                    //            {
                    //                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPPercentageValue])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpPerCentageValue])), 0))
                    //                {
                    //                    RSFlag = false;
                    //                    using (StreamWriter w = File.AppendText(LogFileName))
                    //                    {
                    //                        Log("RowTbl1[" + ActualSPPercentageValue + "] = " + RowTbl1[ActualSPPercentageValue].ToString(), w);
                    //                        Log("RowTbl2[" + TargetGrpPerCentageValue + "] = " + RowTbl2[TargetGrpPerCentageValue].ToString(), w);
                    //                    }
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    RSFlag = true;
                }

                return RSFlag && FirstLevelComparison && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, string Show, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            try
            {
                bool SecondLevelComparison = CompareHeapRatingTrendReportResult(Datacalculation, Show, ActualSP, TargetGrp, CompGrp1);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp2.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareHeatMapBenchmarkResult(Datacalculation, Show, ActualSP, CompGrp2);
                }
                else
                {
                    //DataTable ActualSPRatingTrendTable;
                    //DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    //DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    //ActualDv.RowFilter = "ObjectTypeId IN(4)";
                    //ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    //ActualDv.Sort = "GroupBy ASC";
                    //ActualSPRatingTrendTable = ActualDv.ToTable();

                    //DataTable TargetGrpRatingTrendTable;
                    //DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
                    //DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
                    //TargetGrpDv.Sort = "GroupBy ASC";
                    //TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    //if (Datacalculation.ToLower() == "average")
                    //{
                    //    var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                    //    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    //    var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    //    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    //    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    //    {
                    //        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    //        {
                    //            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                    //            {
                    //                if (RowTbl1[ActualSPAverageValue] != RowTbl2[TargetGrpGroupBy])
                    //                {
                    //                    RSFlag = false;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }


                    //}
                    //else
                    //{
                    //    var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                    //    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    //    var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    //    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    //    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    //    {
                    //        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    //        {
                    //            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                    //            {
                    //                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPPercentageValue])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpPerCentageValue])), 0))
                    //                {
                    //                    RSFlag = false;
                    //                    using (StreamWriter w = File.AppendText(LogFileName))
                    //                    {
                    //                        Log("RowTbl1[" + ActualSPPercentageValue + "] = " + RowTbl1[ActualSPPercentageValue].ToString(), w);
                    //                        Log("RowTbl2[" + TargetGrpPerCentageValue + "] = " + RowTbl2[TargetGrpPerCentageValue].ToString(), w);
                    //                    }
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    RSFlag = true;
                }

                return RSFlag && SecondLevelComparison && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, string Show, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            try
            {
                bool ThirdLevelComparison = CompareHeapRatingTrendReportResult(Datacalculation, Show, ActualSP, TargetGrp, CompGrp1, CompGrp2);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp3.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareHeatMapBenchmarkResult(Datacalculation, Show, ActualSP, CompGrp3);
                }
                else
                {
                    //DataTable ActualSPRatingTrendTable;
                    //DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    //DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    //ActualDv.RowFilter = "ObjectTypeId IN(4)";
                    //ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    //ActualDv.Sort = "GroupBy ASC";
                    //ActualSPRatingTrendTable = ActualDv.ToTable();

                    //DataTable TargetGrpRatingTrendTable;
                    //DataViewManager Targetdvm = CompGrp3.DefaultViewManager;
                    //DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
                    //TargetGrpDv.Sort = "GroupBy ASC";
                    //TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    //if (Datacalculation.ToLower() == "average")
                    //{
                    //    var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                    //    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    //    var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    //    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    //    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    //    {
                    //        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    //        {
                    //            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                    //            {
                    //                if (RowTbl1[ActualSPAverageValue] != RowTbl2[TargetGrpGroupBy])
                    //                {
                    //                    RSFlag = false;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }


                    //}
                    //else
                    //{
                    //    var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                    //    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    //    var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    //    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    //    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    //    {
                    //        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    //        {
                    //            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                    //            {
                    //                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[ActualSPPercentageValue])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpPerCentageValue])), 0))
                    //                {
                    //                    RSFlag = false;
                    //                    using (StreamWriter w = File.AppendText(LogFileName))
                    //                    {
                    //                        Log("RowTbl1[" + ActualSPPercentageValue + "] = " + RowTbl1[ActualSPPercentageValue].ToString(), w);
                    //                        Log("RowTbl2[" + TargetGrpPerCentageValue + "] = " + RowTbl2[TargetGrpPerCentageValue].ToString(), w);
                    //                    }
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    RSFlag = true;
                }

                return RSFlag && ThirdLevelComparison && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp,string ScaleOptionID)
        {
            try
            {
                bool NRFlag = true;

                
                DataTable TargetGrpSPRatingTrendTable;
                DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
                DataView TargetDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
                TargetDv.RowFilter = "ScaleoptionID IN(" + ScaleOptionID + ")";
                TargetDv.Sort = "GroupBy ASC";
                TargetGrpSPRatingTrendTable = TargetDv.ToTable();

                DataTable ActualSPNonRatingTrendTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                ActualDv.RowFilter = "ObjectTypeId IN(5) and GroupBY NOT IN('All') ";
                //ActualDv.RowFilter = "GroupBY NOT IN('All')";
                ActualDv.Sort = "GroupBy ASC";
                ActualSPNonRatingTrendTable = ActualDv.ToTable();


                var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
                var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

                var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                    {
                        if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                        {
                            if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0))
                            {
                                NRFlag = false;
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    Log("Heat Map Non Rating Target group comparison", w);
                                    Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                    Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                    Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                    Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                    Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl1[TrendScaleOptionScoreCol].ToString()), 0), w);
                                    Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl2[TargetGrpScaleOptionScoreCol].ToString()), 0), w);
                                }
                                break;

                            }
                        }
                    }
                }

                return NRFlag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, string ScaleOptionID)
        {
            try
            {
                bool NRFlag = true;
                bool bbflag = true;
                bool FirstLevelComparison = CompareHeapNonRatingTrendReportResult(ActualSP, TargetGrp,ScaleOptionID);
                DataColumnCollection Columns = CompGrp1.Tables[0].Columns;
                if (Columns.Contains("BenchmarkScaleOptionId") == true)
                {
                    bbflag = CompareHeapTrendReportBenchmarkResultForNR(ActualSP, CompGrp1,ScaleOptionID);
                }
                else
                {
                    /*DataTable TargetGrpSPRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
                    DataView TargetDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
                    TargetDv.RowFilter = "ScaleoptionID IN(" + ScaleOptionID + ")";
                    TargetDv.Sort = "GroupBy ASC";
                    TargetGrpSPRatingTrendTable = TargetDv.ToTable();


                    DataTable ActualSPNonRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.RowFilter = "ObjectTypeId IN(5)";
                    ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPNonRatingTrendTable = ActualDv.ToTable();

                    var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                    var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
                    var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

                    var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                    var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];

                    foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0))
                                {
                                    NRFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("Heat Map Non Rating Comparison group 1 comparison", w);
                                        Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl1[TrendScaleOptionScoreCol].ToString()), 0), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl2[TargetGrpScaleOptionScoreCol].ToString()), 0), w);
                                    }
                                    break;

                                }
                            }
                        }
                    }*/
                }

                return NRFlag && bbflag && FirstLevelComparison;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2,string scaleoptionId)
        {
            try
            {
                bool NRFlag = true;
                bool bbflag = true;
                bool SecondLevelComparison = CompareHeapNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1, scaleoptionId);
                DataColumnCollection Columns = CompGrp2.Tables[0].Columns;
                if (Columns.Contains("BenchmarkScaleOptionId") == true)
                {
                    bbflag = CompareHeapTrendReportBenchmarkResultForNR(ActualSP, CompGrp2, scaleoptionId);
                }
                else
                {
                    /*DataTable TargetGrpSPRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
                    DataView TargetDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
                    TargetDv.RowFilter = "ScaleoptionID IN(" + scaleoptionId + ")";
                    TargetDv.Sort = "GroupBy ASC";
                    TargetGrpSPRatingTrendTable = TargetDv.ToTable();

                    DataTable ActualSPNonRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.RowFilter = "ObjectTypeId IN(5)";
                    ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPNonRatingTrendTable = ActualDv.ToTable();


                    var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                    var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
                    var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

                    var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                    var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];

                    foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0))
                                {
                                    NRFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("Heat Map Non Rating Comparison group 2 comparison", w);
                                        Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl1[TrendScaleOptionScoreCol].ToString()), 0), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl2[TargetGrpScaleOptionScoreCol].ToString()), 0), w);
                                    }
                                    break;

                                }
                            }
                        }
                    }*/
                }

                return NRFlag && bbflag && SecondLevelComparison;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3, string scaleoptionid)
        {
            try
            {
                bool NRFlag = true;
                bool bbflag = true;
                bool thirdLevelComparison = CompareHeapNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1, CompGrp2, scaleoptionid);
                DataColumnCollection Columns = CompGrp3.Tables[0].Columns;
                if (Columns.Contains("BenchmarkScaleOptionId") == true)
                {
                    bbflag = CompareHeapTrendReportBenchmarkResultForNR(ActualSP, CompGrp3, scaleoptionid);
                }
                else
                {
                    //String ScaleOption = "";
                    /*DataTable TargetGrpSPRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp3.DefaultViewManager;
                    DataView TargetDv = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
                    TargetDv.Sort = "GroupBy DESC";
                    TargetDv.RowFilter = "ScaleoptionID IN(" + scaleoptionid + ")";
                    TargetDv.Sort = "GroupBy ASC";
                    TargetGrpSPRatingTrendTable = TargetDv.ToTable();


                    DataTable ActualSPNonRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.RowFilter = "ObjectTypeId IN(5)";
                    ActualDv.RowFilter = "GroupBY NOT IN('All')";
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPNonRatingTrendTable = ActualDv.ToTable();


                    var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
                    var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
                    var TrendScaleOptionScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

                    var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
                    var TargetGrpScaleOptionScoreCol = TargetGrpSPRatingTrendTable.Columns["Response Rate"];

                    foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                            {
                                if (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl1[TrendScaleOptionScoreCol])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(RowTbl2[TargetGrpScaleOptionScoreCol])), 0))
                                {
                                    NRFlag = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        Log("Heat Map Non Rating Comparison group 3 comparison", w);
                                        Log("RowTbl1[" + TrendNRGroupByCol + "] = " + RowTbl1[TrendNRGroupByCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpNRGroupByCol + "] = " + RowTbl2[TargetGrpNRGroupByCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionCol + "] = " + RowTbl1[TrendScaleOptionCol].ToString(), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionCol + "] = " + RowTbl2[TargetGrpScaleOptionCol].ToString(), w);
                                        Log("RowTbl1[" + TrendScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl1[TrendScaleOptionScoreCol].ToString()), 0), w);
                                        Log("RowTbl2[" + TargetGrpScaleOptionScoreCol + "] = " + Math.Round(Decimal.Parse(RowTbl2[TargetGrpScaleOptionScoreCol].ToString()), 0), w);
                                    }
                                    break;

                                }
                            }
                        }
                    }*/
                }

                return NRFlag && bbflag && thirdLevelComparison;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CompareDemographicReportResults_category(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, String Reportname, int UserAnalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {
            bool ddflag = true;
            DataTable ActualSPDemographicTable;
            DataViewManager DemographicDVM = ActualSP.DefaultViewManager;
            DataView DemographicDV = DemographicDVM.CreateDataView(ActualSP.Tables[1]);
            DemographicDV.RowFilter = "ObjectTypeid in(3) and Objectid not in (38766)";
            DemographicDV.Sort = "ObjectID, Demovalue ASC";
            ActualSPDemographicTable = DemographicDV.ToTable();


            DataTable TargetGrpDemographicTable;
            DataViewManager TGrpDemographic = TargetGrp.DefaultViewManager;
            DataView TGrpDemo = TGrpDemographic.CreateDataView(TargetGrp.Tables[0]);
            TGrpDemo.Sort = "CategoryID ASC";
            TargetGrpDemographicTable = TGrpDemo.ToTable();

            if (DataCalculation.ToLower() == "average")

            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["Objectid"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["CategoryID"];


                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data2"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Mean"];

                var DemographicDemovalueCol = ActualSPDemographicTable.Columns["Demovalue"];
                var TargetdemopercentageDemovalueCol = TargetGrpDemographicTable.Columns["Demo Value"];


                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {
                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString() && RowTbl1[DemographicDemovalueCol].ToString() == RowTbl2[TargetdemopercentageDemovalueCol].ToString())
                        {
                            if ((RowTbl1[DemographicpercentagevalueCol] == DBNull.Value) || RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)
                            {
                                if (RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)

                                {
                                    if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters, 0);
                                        break;
                                    }
                                }
                                else
                                {
                                    var Actualvalue = 0;
                                    if ((Actualvalue.ToString()).ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }


                            else
                            if (RowTbl1[DemographicpercentagevalueCol].ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                            {
                                ddflag = false;
                                WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["ObjectID"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["CategoryID"];

                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data1"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Favorable"];

                var DemographicDemovalueCol = ActualSPDemographicTable.Columns["Demovalue"];
                var TargetdemopercentageDemovalueCol = TargetGrpDemographicTable.Columns["Demo Value"];

                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {
                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString() && RowTbl1[DemographicDemovalueCol].ToString() == RowTbl2[TargetdemopercentageDemovalueCol].ToString())

                        {

                            if ((RowTbl1[DemographicpercentagevalueCol] == DBNull.Value) || RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)
                            {
                                if (RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)
                                {
                                    if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                        break;
                                    }
                                }
                                else
                                {
                                    var Actualvalue = 0;
                                    if ((Actualvalue.ToString()).ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }

                            else

                                if (RowTbl1[DemographicpercentagevalueCol].ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                            {
                                ddflag = false;
                                WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
            }

            return ddflag;
        }

        public static bool CompareDemographicReportResults_questions(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, String Reportname, int UserAnalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {
            bool ddflag = true;
            DataTable ActualSPDemographicTable;
            DataViewManager DemographicDVM = ActualSP.DefaultViewManager;
            DataView DemographicDV = DemographicDVM.CreateDataView(ActualSP.Tables[1]);
            DemographicDV.RowFilter = "objectTypeId in(4) and Objectid not in (38766)";
            DemographicDV.Sort = "ObjectID, Demovalue ASC";
            ActualSPDemographicTable = DemographicDV.ToTable();


            DataTable TargetGrpDemographicTable;
            DataViewManager TGrpDemographic = TargetGrp.DefaultViewManager;
            DataView TGrpDemo = TGrpDemographic.CreateDataView(TargetGrp.Tables[0]);
            TGrpDemo.Sort = "QuestionID ASC";
            TargetGrpDemographicTable = TGrpDemo.ToTable();

            if (DataCalculation.ToLower() == "average")

            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["Objectid"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["QuestionID"];

                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data2"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Mean"];

                var DemographicDemovalueCol = ActualSPDemographicTable.Columns["Demovalue"];
                var TargetdemopercentageDemovalueCol = TargetGrpDemographicTable.Columns["Demo Value"];

                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {

                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString() && RowTbl1[DemographicDemovalueCol].ToString() == RowTbl2[TargetdemopercentageDemovalueCol].ToString())
                        {
                            if ((RowTbl1[DemographicpercentagevalueCol] == DBNull.Value) || RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)
                            {
                                if (RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)
                                {
                                    if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                        break;
                                    }
                                }
                                else
                                {
                                    var Actualvalue = 0;
                                    if ((Actualvalue.ToString()).ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }


                            else

                               if (RowTbl1[DemographicpercentagevalueCol].ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                            {
                                ddflag = false;
                                WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters, 0);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["Objectid"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["QuestionID"];

                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data1"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Favorable"];

                var DemographicDemovalueCol = ActualSPDemographicTable.Columns["Demovalue"];
                var TargetdemopercentageDemovalueCol = TargetGrpDemographicTable.Columns["Demo Value"];

                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {
                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString() && RowTbl1[DemographicDemovalueCol].ToString() == RowTbl2[TargetdemopercentageDemovalueCol].ToString())
                        {
                            if ((RowTbl1[DemographicpercentagevalueCol] == DBNull.Value) || RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)
                            {
                                if (RowTbl2[TargetGrpdemographicpercentagevalueCol] == DBNull.Value)
                                {
                                    if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                        break;
                                    }
                                }
                                else
                                {
                                    var Actualvalue = 0;
                                    if ((Actualvalue.ToString()).ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                                    {
                                        ddflag = false;
                                        WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                        break;
                                    }
                                }
                            }


                            else

                    if (RowTbl1[DemographicpercentagevalueCol].ToString() != RowTbl2[TargetGrpdemographicpercentagevalueCol].ToString())
                            {
                                ddflag = false;
                                WriteFailedDatacomment(DataCalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
            }

            return ddflag;
        }
        public static bool SwitchtocommentComparsion(DataSet surveyform, DBConnectionStringDTO DBConnectionParameters, string CategoryText, DataSet ActualSp, DataSet TargetGrp, string Datacalculation, string Reportname, int UserAnalyzeFilterID)
        {

            bool ddflag = true;



            string SurveyFormID = surveyform.Tables[0].Rows[0][0].ToString();
            string query = "SELECT COUNT(*) FROM DimItem_V11 WHERE SurveyFormID = '" + SurveyFormID + "' and CategoryText = '" + CategoryText + "' AND GroupName = 'RatingScale' AND CategoryText != 'Unassigned'";
            DataSet rscount = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);
            if (rscount.Tables[0].Rows[0][0].ToString() == "0")
            {
                ddflag = CompareCommentResults_unscoredcategory(ActualSp, TargetGrp, Datacalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);

            }
            else
            {
                ddflag = CompareCommentResults_scoredcategory(ActualSp, TargetGrp, Datacalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);


            }
            return ddflag;
        }
        public static bool CompareCommentResults_unscoredcategory(DataSet ActualSP, DataSet TargetGrp, string Datacalculation, string Reportname, int UserAnalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {
            bool unscorecommentsflag = true;

            //unscoredcategory  comments dev output

            DataTable ActualSPCommentUnscoreTable;
            DataViewManager UnsocreCommentDVM = ActualSP.DefaultViewManager;
            DataView UnscoreCommentDV = UnsocreCommentDVM.CreateDataView(ActualSP.Tables[0]);

            var sp = ActualSP.Tables[0].Rows.Count;

            if (sp > 0)
            {
                UnscoreCommentDV.RowFilter = "RespondentType in(0)";

            }


            ActualSPCommentUnscoreTable = UnscoreCommentDV.ToTable();

            // unscored category comments testsp output	        

            DataTable TargetGrpCommentUnscoreTable;
            DataViewManager TGrpUnscoreComments = TargetGrp.DefaultViewManager;

            DataView TGrpUnscoreCm = TGrpUnscoreComments.CreateDataView(TargetGrp.Tables[0]);
            TargetGrpCommentUnscoreTable = TGrpUnscoreCm.ToTable();

            //defining the columns for comparision 

            var CommentObjectTextCol = ActualSPCommentUnscoreTable.Columns["Comments"];
            var TargetCommentsTextCol = TargetGrpCommentUnscoreTable.Columns["Comments"];

            var itemObjectTextCol = ActualSPCommentUnscoreTable.Columns["ItemText"];
            var TargetitemtextCol = TargetGrpCommentUnscoreTable.Columns["ReportItemText"];


            var ActualSpTableCountn = ActualSPCommentUnscoreTable.Rows.Count;

            var TargetSpTableCountn = TargetGrpCommentUnscoreTable.Rows.Count;


            if (ActualSpTableCountn == TargetSpTableCountn)
            {
                for (int i = 0; i < ActualSpTableCountn; i++)
                {
                    if ((ActualSPCommentUnscoreTable.Rows[i][5].ToString()) != (TargetGrpCommentUnscoreTable.Rows[i][1].ToString()))

                    {
                        unscorecommentsflag = false;

                        WriteFailedDatacomment(Datacalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);

                    }

                }


            }
            return unscorecommentsflag;
        }



        // foreach (DataRow RowTbl1 in ActualSPCommentUnscoreTable.Rows)
        //  {
        //      foreach (DataRow RowTbl2 in TargetGrpCommentUnscoreTable.Rows)
        //     {

        //       if (RowTbl1[CommentObjectTextCol] != RowTbl2[TargetCommentsTextCol])
        //       {
        //      unscorecommentsflag = false;
        //     WriteFailedDatacomment(Datacalculation,Reportname, UserAnalyzeFilterID,0, DBConnectionParameters);
        //     break;
        // }
        //  }
        //    }

        //   return unscorecommentsflag; 







        public static bool CompareCommentResults_scoredcategory(DataSet ActualSP, DataSet TargetGrp, String Datacalculation, String Reportname, int UserAnalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {

            bool abovecommentsflag = true;
            bool belowcommentsflag = true;

            //scored category and above average comments dev output



            DataTable ActualSPCommentAboveAvgTable;
            DataViewManager AboveCommentDVM = ActualSP.DefaultViewManager;
            DataView AboveCommentDV = AboveCommentDVM.CreateDataView(ActualSP.Tables[0]);
            var sp = ActualSP.Tables[0].Rows.Count;

            if (sp > 0)
            {
                AboveCommentDV.RowFilter = "RespondentType in(1)";

            }

            ActualSPCommentAboveAvgTable = AboveCommentDV.ToTable();

            //scored category and above belowaverage comments dev output

            DataTable ActualSPCommentBelowAvgTable;
            DataViewManager BelowCommentDVM = ActualSP.DefaultViewManager;
            DataView BelowCommentDV = BelowCommentDVM.CreateDataView(ActualSP.Tables[0]);

            if (sp > 0)
            {

                BelowCommentDV.RowFilter = "RespondentType in(-1)";

            }



            ActualSPCommentBelowAvgTable = BelowCommentDV.ToTable();

            //scored category and above average comments testsp output	        

            DataTable TargetGrpCommentTable;
            DataViewManager TGrpComments = TargetGrp.DefaultViewManager;

            DataView TGrpCm = TGrpComments.CreateDataView(TargetGrp.Tables[0]);
            TargetGrpCommentTable = TGrpCm.ToTable();

            //scored category and above below comments test output    
            DataTable TargetGrpbelowCommentTable;
            DataViewManager TGrpbelowComments = TargetGrp.DefaultViewManager;

            DataView TGrpbelowCm = TGrpbelowComments.CreateDataView(TargetGrp.Tables[1]);
            TargetGrpbelowCommentTable = TGrpbelowCm.ToTable();

            //defining columns for comparision 


            var CommentObjectTextCol = ActualSPCommentAboveAvgTable.Columns["Comments"];
            var TargetCommentsTextCol = TargetGrpCommentTable.Columns["Comments"];
            var ActualSpTableCount = ActualSPCommentAboveAvgTable.Rows.Count;
            var ActualSpTableCount1 = ActualSPCommentBelowAvgTable.Rows.Count;

            var itemObjectTextCol = ActualSPCommentAboveAvgTable.Columns["ItemText"];
            var TargetitemtextCol = TargetGrpCommentTable.Columns["ReportItemText"];


            var BelowCommentObjecttextCol = ActualSPCommentBelowAvgTable.Columns["Comments"];
            var TargetBelowCommentstextCol = TargetGrpbelowCommentTable.Columns["Comments"];

            var itembelowObjecttextCol = ActualSPCommentBelowAvgTable.Columns["ItemText"];
            var TargetitemtextbelowCol = TargetGrpbelowCommentTable.Columns["ReportItemText"];
            var TargetSpTableCount = TargetGrpCommentTable.Rows.Count;
            var TargetSpTableCount1 = TargetGrpbelowCommentTable.Rows.Count;



            if (ActualSpTableCount == TargetSpTableCount)
            {
                for (int i = 0; i < ActualSpTableCount; i++)
                {
                    if ((ActualSPCommentAboveAvgTable.Rows[i][5].ToString()) != (TargetGrpCommentTable.Rows[i][1].ToString()))

                    {
                        abovecommentsflag = false;

                        WriteFailedDatacomment(Datacalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);

                    }


                }

            }
            if (ActualSpTableCount1 == TargetSpTableCount1)
            {
                for (int i = 0; i < ActualSpTableCount1; i++)
                {
                    if ((ActualSPCommentBelowAvgTable.Rows[i][5].ToString()) != (TargetGrpbelowCommentTable.Rows[i][1].ToString()))
                    {
                        belowcommentsflag = false;

                        WriteFailedDatacomment(Datacalculation, Reportname, UserAnalyzeFilterID, DBConnectionParameters);

                    }
                }
            }

            return abovecommentsflag && belowcommentsflag;
        }
    }
}





//comparision process  for above comments


/*foreach (DataRow RowTbl1 in ActualSPCommentAboveAvgTable.Rows)
  {
     foreach (DataRow RowTbl2 in TargetGrpCommentTable.Rows)

{

/* if (RowTbl1[CommentObjectTextCol].ToString() != RowTbl2[TargetCommentsTextCol].ToString())
     {
         abovecommentsflag = false;
       break;
         // WriteFailedDatacomment(ReportName, UserAnalyzeFilterId, DBConnectionParameters);

     }
 }*/




//comparision process  for below comments


/*  foreach (DataRow RowTbl1 in ActualSPCommentBelowAvgTable.Rows)
     {
          foreach (DataRow RowTbl2 in TargetGrpbelowCommentTable.Rows)
           {

               if (RowTbl1[BelowCommentObjecttextCol] != RowTbl2[TargetBelowCommentstextCol])
               {
                   belowcommentsflag = false;
                   break;
               }

           }
       }

       return abovecommentsflag && belowcommentsflag;


   }*/





