﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Aspose.Slides;
using OpenQA.Selenium;
using System.Collections;
using NUnit.Framework;
using LumenWorks.Framework.IO.Csv;
using Aspose.Cells;


namespace Common
{
    public class DBOperations
    {
        public static string connectionString;
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";
        public static string getSurveyURLSByDistribution(int SurveyID, string userName, string password, string serverName)
        {
            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;User ID=" + userName +
                ";Password=" + password;
                string playerURL = null;
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = sqlConn;
                    if (command.Connection.State == ConnectionState.Closed)
                        if (command.Connection.State == ConnectionState.Closed)
                            sqlConn.Open();
                    command.CommandText = @"SELECT TOP 1 CONCAT('http://qa-surveys.cebglobal.com/Pulse/Player/Start/',SurveyWaveID,'/1/',Guid) FROM SurveyWaveParticipant WHERE SurveyID = @SurveyID AND [Status] = 1 AND IsSystemGenerated = 0 AND SurveyStatusFlag = 0";
                    //command.CommandText = "SELECT TOP 1 SurveyWaveID FROM SurveyWaveParticipant WHERE SurveyID = @SurveyID AND [Status] = 1 AND IsSystemGenerated = 0 AND SurveyStatusFlag = 0";
                    command.Parameters.AddWithValue("@SurveyID", SurveyID);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        playerURL = reader.GetString(0);
                    }
                    reader.Close();
                    sqlConn.Close();
                }
                return playerURL;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static void executesqlandwritetocsv(IWebDriver driver, string filepath, string filename, DBConnectionStringDTO DBConnectionParameters, string InputSPText)
        {
            DistributeActions.WriteLog(driver, filepath, "**************************************");
            DistributeActions.WriteLog(driver, filepath, "Inside executesqlandwritetocsv \n \n");
            DistributeActions.WriteLog(driver, filepath, "********************************************");
            DataSet ds = new DataSet();
            object cell = null;
            string s = null;
            try
            {

                //common login
                connectionString = "Data Source =" + DBConnectionParameters.TCESReportingserverName + ";Initial Catalog="
                + DBConnectionParameters.TCESReportingDB + ";User ID=" + DBConnectionParameters.userName + ";Password=" + DBConnectionParameters.password;
                string playerURL = null;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = InputSPText;
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection = connection;
                    command.CommandTimeout = 3600;
                    command.CommandType = CommandType.Text;
                    command.CommandText = InputSPText;
                    Console.WriteLine("Executing the Input SP query");
                    DistributeActions.WriteLog(driver, filepath, "Executing the Input SP query");
                    DistributeActions.WriteLog(driver, filepath, "Connecting database ::" + connectionString);
                    DistributeActions.WriteLog(driver, filepath, "Connected database successfully...");
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    var outCsvFile = @filepath + "\\" + filename + ".csv";
                    foreach (DataTable dt in ds.Tables)
                    {
                        int countRow = dt.Rows.Count;
                        int countCol = dt.Columns.Count;
                        StringBuilder sb = new StringBuilder();
                        foreach (DataColumn col in dt.Columns)
                        {
                            sb.Append(col.ColumnName + ',');
                        }
                        sb.Remove(sb.Length - 1, 1);
                        sb.AppendLine();
                        foreach (DataRow row in dt.Rows)
                        {
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                sb.Append(row[i].ToString() + ",");
                            }
                            sb.AppendLine();
                        }
                        File.WriteAllText(outCsvFile, sb.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



        public static void executesqlandwritetocsvmultipletables(IWebDriver driver, string filepath, string filename, DBConnectionStringDTO DBConnectionParameters, string InputSPText)
        {
            DistributeActions.WriteLog(driver, filepath, "**************************************");
            DistributeActions.WriteLog(driver, filepath, "Inside executesqlandwritetocsvmultipletables \n \n");
            DistributeActions.WriteLog(driver, filepath, "********************************************");
            DataSet ds = new DataSet();
            object cell = null;
            string s = null;
            try
            {

                //common login
                connectionString = "Data Source =" + DBConnectionParameters.TCESReportingserverName + ";Initial Catalog="
                + DBConnectionParameters.TCESReportingDB + ";User ID=" + DBConnectionParameters.userName + ";Password=" + DBConnectionParameters.password;
                string playerURL = null;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString = InputSPText;
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection = connection;
                    command.CommandTimeout = 3600;
                    command.CommandType = CommandType.Text;
                    command.CommandText = InputSPText;
                    Console.WriteLine("Executing the Input SP query");
                    //        DistributeActions.WriteLog(driver, filepath, "Executing the Input SP query");

                    DistributeActions.WriteLog(driver, filepath, "**********************");
                    DistributeActions.WriteLog(driver, filepath, "Connecting database::\n\n" + connectionString);
                    DistributeActions.WriteLog(driver, filepath, "**********************");
                    DistributeActions.WriteLog(driver, filepath, "Connected database successfully...\n\n");

                    DistributeActions.WriteLog(driver, filepath, "**********************");
                    DistributeActions.WriteLog(driver, filepath, "Executing the SP query  :--\n\n"+ InputSPText);


                    DistributeActions.WriteLog(driver, filepath, "**********************");

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    var outCsvFile = @filepath + "\\" + filename + ".csv";
                    StringBuilder sb = new StringBuilder();
                    foreach (DataTable dt in ds.Tables)
                    {
                        int countRow = dt.Rows.Count;
                        int countCol = dt.Columns.Count;

                        foreach (DataColumn col in dt.Columns)
                        {

                            sb.Append(col.ColumnName + ',');

                        }
                        sb.Remove(sb.Length - 1, 1);
                        sb.AppendLine();
                        foreach (DataRow row in dt.Rows)
                        {
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                sb.Append(row[i].ToString() + ",");





                            }
                            sb.AppendLine();
                        }

                    }

                    File.WriteAllText(@filepath + "\\" + filename + ".csv", String.Empty);//  Remove all previous text before writing

                    File.AppendAllText(outCsvFile, sb.ToString()); //Append all Tables ( Table 1... to Table n )data one below other



                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



        public static void Readfromcsv(IWebDriver driver, string filepath, string filename, string header)
        {
            DistributeActions.WriteLog(driver, filepath, "**********************");
            DistributeActions.WriteLog(driver, filepath, "Inside method Readfromcsv");
            DistributeActions.WriteLog(driver, filepath, "***********************");
            TextReader textReader = null;
            List<string> columns = new List<string>();
            try
            {
                using (CsvReader csv = new CsvReader(new StreamReader(@filepath + "\\" + filename + ".csv"), true))
                {
                    int fieldCount = csv.FieldCount;
                    string[] headers;
                    int i;
                    int idx;
                    for (i = 0; i <= fieldCount - 1; i++)
                    {
                        //  Display the column names:
                        headers = csv.GetFieldHeaders();
                        Console.WriteLine("Column names present in csv are ->" + Convert.ToString(i) + ": " + headers[i]);
                        DistributeActions.WriteLog(driver, filepath, "Column names present in csv at index ->" + Convert.ToString(i) + " is ->: " + headers[i]);
                        //  The following line demonstrates to to get the column index given a column name:
                        idx = csv.GetFieldIndex(headers[i]);
                        Console.WriteLine(headers + " is at column index " + Convert.ToString(idx));
                        DistributeActions.WriteLog(driver, filepath, headers[i] + " is present at column index " + Convert.ToString(idx));
                        if (headers[i].Contains(header))
                        {
                            while (csv.ReadNextRecord())
                            {
                                columns.Add(csv[i]);
                                foreach (var item in columns)
                                {
                                    DistributeActions.WriteLog(driver, filepath, "  Data present in CSV in Column Header ->" + header + "  are--> " + item);
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                if (textReader != null)
                {
                    textReader.Close();
                }
            }
        }
        public static void Readfromcsvrowcolumnwise(IWebDriver driver, string csvfilepath, string csvfilename, int srownum, int erownum, int scolnum, int ecolnum)
        {
            DistributeActions.WriteLog(driver, csvfilepath, "**********************");

            DistributeActions.WriteLog(driver, csvfilepath, "Inside method Readfromcsvrowrowcolumnwise \n \n");
            DistributeActions.WriteLog(driver, csvfilepath, "***********************");

            string FileLines = System.IO.File.ReadAllText(@csvfilepath + "\\" + csvfilename + ".csv");
            // Split into lines.
            FileLines = FileLines.Replace('\n', '\r');
            string[] lines = FileLines.Split(new char[] { '\r' },
                StringSplitOptions.RemoveEmptyEntries);
            // See how many rows and columns there are.
            int num_rows = lines.Length;
            int num_cols = lines[0].Split(',').Length;
            // Allocate the data array.
            string[,] al = new string[num_rows, num_cols];
            List<string> al1 = new List<string>(num_rows * num_cols);
            // Load the array.
            for (int r = srownum; r <= erownum; r++)
            {
                string[] line_r = lines[r].Split(',');
                for (int c = scolnum; c <= ecolnum; c++)
                {
                    al[r, c] = line_r[c];
                    al1.Add(al[r, c]);
                    //    DistributeActions.WriteLog(driver, csvfilepath, "  Data present in CSV at  Row [" + r + "] Column [ " + c + " ] has value -->[ " + al1[r, c] + "]");
                    DistributeActions.WriteLog(driver, csvfilepath, "  Data present in CSV at  Row [" + r + "] Column [ " + c + " ] has value -->[ " + al[r, c] + "]");
                }
            }
        }


        public static bool csvcompare(IWebDriver driver, string sourcecsvfilepath, string sourcecsvfilename, string destinationcsvfilepath, string destinationcsvfilename)
        {
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "**********************");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "Inside method csvcompare ");
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "***********************");



            string[] fileContentsOne = File.ReadAllLines(@sourcecsvfilepath + "\\" + sourcecsvfilename + ".csv");

            string[] fileContentsTwo = File.ReadAllLines(@destinationcsvfilepath + "\\" + destinationcsvfilename + ".csv");


            System.IO.StreamReader file1 = new System.IO.StreamReader(@sourcecsvfilepath + "\\" + sourcecsvfilename + ".csv");

            System.IO.StreamReader file2 = new System.IO.StreamReader(@destinationcsvfilepath + "\\" + destinationcsvfilename + ".csv");


            //Compare length of files


            bool result = false;


            if (!fileContentsOne.Length.Equals(fileContentsTwo.Length))
                return false;



            for (int i = 0; i < fileContentsOne.Length; i++)
            {





                string[] columnsOne = fileContentsOne[i].Split(new char[] { ';' });
                string[] columnsTwo = fileContentsTwo[i].Split(new char[] { ';' });






                //If length of files are equal , Compare number of columns on each row

                if (!columnsOne.Length.Equals(columnsTwo.Length))
                    return false;


                //If Columns length are equal, Compare column values


                for (int j = 0; j < columnsOne.Length; j++)
                {




                    DistributeActions.WriteLog(driver, destinationcsvfilepath, "  Data present in CSV1 at row [ " + i + " ] has value -->[ " + columnsOne[j] + " ]");

                    DistributeActions.WriteLog(driver, destinationcsvfilepath, "  Data present in CSV2 at row [ " + i + " ] has value -->[ " + columnsTwo[j] + " ]");


                    if (columnsOne[j].Equals(columnsTwo[j])) //if line 1 equals line 2


                    {
                        DistributeActions.WriteLog(driver, destinationcsvfilepath, " No differences found"); //no differences found



                        continue;
                    }



                    else if (!columnsOne[j].Equals(columnsTwo[j]))


                    {
                        DistributeActions.WriteLog(driver, destinationcsvfilepath, " Differences found at Line Number [ " + i + " ] "); //no differences found

                        throw new Exception(" Differences found at Line Number [ " + i + " ]");
                        return false;
                    }

                }
            }

            return result;
        }






        public static bool csvComparerowcolumnnwise(IWebDriver driver, string sourcecsvfilepath, string sourcecsvfilename, int sourcesrownum, int sourceerownum, int sourcescolnum, int sourceecolnum, string destinationcsvfilepath, string destinationcsvfilename, int destinationsrownum, int destinationerownum, int destinationscolnum, int destinationecolnum)
        {
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "**********************");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "Inside method csvComparerowcolumnnwise");
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "***********************");

            //CSV 1 



            DistributeActions.WriteLog(driver, sourcecsvfilepath, "**********************");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "Reading CSV1 Data");
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "***********************");

            //     string FileLinesone = System.IO.File.ReadAllText(@sourcecsvfilepath + "\\" + sourcecsvfilename + ".csv");
            // Split into lines.
            //        FileLinesone = FileLinesone.Replace('\n', '\r');

            //   string[] linesone = FileLinesone.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);

            string[] linesone = File.ReadAllLines(@sourcecsvfilepath + "\\" + sourcecsvfilename + ".csv");

            // See how many rows and columns there are.
            int num_rowsone = linesone.Length;


            int max_num_colsone = 0;
            int num_colsone = 0;

            for (int i = 0; i < num_rowsone; i++)
            {
                DistributeActions.WriteLog(driver, @sourcecsvfilepath, " Reading Row ->" + i + "in CSV 1 ");




                num_colsone = linesone[i].Split(',').Length;


                DistributeActions.WriteLog(driver, @sourcecsvfilepath, " Number of columns in CSV 1 at Row  ->" + i + " are " + num_colsone);


                if (max_num_colsone < num_colsone)

                {
                    max_num_colsone = num_colsone;

                }

            }



            // Allocate the data array.

            string[,] al = new string[num_rowsone, max_num_colsone];

            List<string> al1 = new List<string>(num_rowsone * max_num_colsone);
            // Load the array.
            for (int r1 = sourcesrownum; r1 <= sourceerownum; r1++)
            {
                string[] line_r = linesone[r1].Split(',');

                for (int c1 = sourcescolnum; c1 <= sourceecolnum; c1++)
                {
                    al[r1, c1] = line_r[c1];

                    al1.Add(al[r1, c1]);

                    //    DistributeActions.WriteLog(driver, csvfilepath, "  Data present in CSV at  Row [" + r + "] Column [ " + c + " ] has value -->[ " + al1[r, c] + "]");


                    DistributeActions.WriteLog(driver, @sourcecsvfilepath, "  Data present in CSV1 at  Row [ " + r1 + " ] Column [ " + c1 + " ] has value -->[ " + al[r1, c1] + " ]");
                }
            }



            //CSV 2
            bool result = false;


            DistributeActions.WriteLog(driver, sourcecsvfilepath, "**********************");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "Reading CSV2 Data");
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "***********************");


            //string FileLinestwo = System.IO.File.ReadAllText(@destinationcsvfilepath + "\\" + destinationcsvfilename + ".csv");
            //// Split into lines.

            //FileLinestwo = FileLinestwo.Replace('\n', '\r');

            //string[] linestwo = FileLinestwo.Split(new char[] { '\r' },StringSplitOptions.RemoveEmptyEntries);

            string[] linestwo = File.ReadAllLines(@destinationcsvfilepath + "\\" + destinationcsvfilename + ".csv");

            // See how many rows and columns there are.
            int num_rowstwo = linestwo.Length;


            int max_num_colstwo = 0;
            int num_colstwo = 0;

            for (int j = 0; j < num_rowstwo; j++)
            {
                DistributeActions.WriteLog(driver, @destinationcsvfilepath, " Reading Row ->" + j + "in CSV2 ");




                num_colstwo = linestwo[j].Split(',').Length;


                DistributeActions.WriteLog(driver, @destinationcsvfilepath, " Number of columns in CSV 2 at Row  ->" + j + " are " + num_colstwo);


                if (max_num_colstwo < num_colstwo)

                {
                    max_num_colstwo = num_colstwo;

                }

            }





            // Allocate the data array.
            string[,] all = new string[num_rowstwo, max_num_colstwo];


            List<string> all1 = new List<string>(num_rowstwo * max_num_colstwo);
            // Load the array.
            for (int r2 = destinationsrownum; r2 <= destinationerownum; r2++)
            {



                string[] line_r1 = linestwo[r2].Split(',');

                for (int c2 = destinationscolnum; c2 <= destinationecolnum; c2++)
                {
                    all[r2, c2] = line_r1[c2];
                    all1.Add(all[r2, c2]);
                    //    DistributeActions.WriteLog(driver, csvfilepath, "  Data present in CSV at  Row [" + r + "] Column [ " + c + " ] has value -->[ " + al1[r, c] + "]");


                    DistributeActions.WriteLog(driver, destinationcsvfilepath, "  Data present in CSV2 at  Row [ " + r2 + " ] Column [ " + c2 + " ] has value -->[ " + all[r2, c2] + " ]");
                }
            }



            DistributeActions.WriteLog(driver, sourcecsvfilepath, "**********************");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "Comparison of CSV1 & CSV2  Starts \n \n");
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "***********************");







            List<string> al3 = new List<string>();
            string result1 = null;
            string result2 = null;
            string result3 = null;
            //foreach (string temp in al1)
            //{
            //    result1 = temp;
            foreach (string item in al1)
            {
                result2 = item;
                al3.Add(all1.Contains(item) ? "Pass" : "Fail");
                foreach (string cc in al3)
                {
                    result3 = cc;


                }
            }
            //}
            DistributeActions.WriteLog(driver, destinationcsvfilepath, "Data Comparison between CSV1 [" + sourcecsvfilename + " ] and CSV2  [ " + destinationcsvfilename + " ] is-- >  [ " + result3 + " ]");


            if (al3.Contains("Fail"))
            {
                DistributeActions.WriteLog(driver, destinationcsvfilepath, "Data Comparison between CSV1 [" + sourcecsvfilename + " ] and CSV2  [ " + destinationcsvfilename + " ]  is failed ");

                throw new Exception("Data Comparison between CSV1 [" + sourcecsvfilename + " ] and CSV2  [ " + destinationcsvfilename + " ]  is failed ");

                return false;

            }
            else
            {
                DistributeActions.WriteLog(driver, destinationcsvfilepath, "Data Comparison between CSV1 [" + sourcecsvfilename + " ] and CSV2  [ " + destinationcsvfilename + " ]  is Passed ");


                return true;
            }



            return result;
        }



        public static bool csvCompareExcel(IWebDriver driver, string sourcecsvfilepath, string sourcecsvfilename, int sourcesrownum, int sourceerownum, int sourcescolnum, int sourceecolnum, string destinationfilepath, string destinationfilename, int destinationsrownum, int destinationerownum, int destinationscolnum, int destinationecolnum, string SheetName)


        {
            DistributeActions.WriteLog(driver, sourcecsvfilepath, "**********************");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "Inside method csvCompareexcel \n \n");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "***********************");


            DistributeActions.WriteLog(driver, sourcecsvfilepath, "**********************");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "Reading CSV Data \n\n ");

            DistributeActions.WriteLog(driver, sourcecsvfilepath, "***********************");



            //CSV 1 


            //     string FileLinesone = System.IO.File.ReadAllText(@sourcecsvfilepath + "\\" + sourcecsvfilename + ".csv");
            // Split into lines.
            //        FileLinesone = FileLinesone.Replace('\n', '\r');

            //   string[] linesone = FileLinesone.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);

            string[] linesone = File.ReadAllLines(@sourcecsvfilepath + "\\" + sourcecsvfilename + ".csv");

            // See how many rows and columns there are.
            int num_rowsone = linesone.Length;


            int max_num_colsone = 0;
            int num_colsone = 0;

            for (int i = 0; i < num_rowsone; i++)
            {
               // DistributeActions.WriteLog(driver, @sourcecsvfilepath, " Reading Row ->" + i + "in CSV  ");




                num_colsone = linesone[i].Split(',').Length;


             //   DistributeActions.WriteLog(driver, @sourcecsvfilepath, " Number of columns in CSV  at Row  ->" + i + " are " + num_colsone);


                if (max_num_colsone < num_colsone)

                {
                    max_num_colsone = num_colsone;

                }

            }



            // Allocate the data array.

            string[,] al = new string[num_rowsone, max_num_colsone];

            List<string> al1 = new List<string>(num_rowsone * max_num_colsone);
            // Load the array.
            for (int r1 = sourcesrownum; r1 <= sourceerownum; r1++)
            {
                string[] line_r = linesone[r1].Split(',');

                for (int c1 = sourcescolnum; c1 <= sourceecolnum; c1++)
                {
                    al[r1, c1] = line_r[c1];

                    al1.Add(al[r1, c1]);

                    //    DistributeActions.WriteLog(driver, csvfilepath, "  Data present in CSV at  Row [" + r + "] Column [ " + c + " ] has value -->[ " + al1[r, c] + "]");


                    DistributeActions.WriteLog(driver, @sourcecsvfilepath, "  Data present in CSV ->[" + sourcecsvfilename + " ] at  Row [ " + r1 + " ] Column [ " + c1 + " ] has value -->[ " + al[r1, c1] + " ]");
                }
            }



            //Reading EXCEL




            //*********************************************
            //     using aspose.cells

            //************************************************

            DistributeActions.WriteLog(driver, destinationfilepath, "**************************************");


            DistributeActions.WriteLog(driver, destinationfilepath, "Reading Excel Data \n \n");


            DistributeActions.WriteLog(driver, destinationfilepath, "********************************************");


            string s = null;

            Workbook wb = new Workbook(destinationfilepath + "\\" + destinationfilename);


         //   bool result = false;


            Worksheet worksheet = wb.Worksheets[SheetName];



            //    Worksheet worksheet = wb.Worksheets[0];


            Cells cells = worksheet.Cells;



            DistributeActions.WriteLog(driver, destinationfilepath, "Reading Excel contents of file " + destinationfilepath + "\\" + destinationfilename + "   are :-");



            DistributeActions.WriteLog(driver, destinationfilepath, "**************************************");


            List<string> al2 = new List<string>();


            for (int i = destinationsrownum; i <= destinationerownum; i++)

            {

                for (int j = destinationscolnum; j <= destinationecolnum; j++)

                {

                    //       s = cells[i, j].StringValue.Trim();

                    s = Convert.ToString(cells[i, j].StringValue.Trim());


                    DistributeActions.WriteLog(driver, destinationfilepath, "Data Present in Excel [ " + destinationfilename + " ] at  Row Number ->[ " + i + " ] and Column Number[ " + j + " ] in Sheet [ " + SheetName + " ]  is :-->[ " + s + " ]");

                    al2.Add(s);


                }



            }

           

 

            DistributeActions.WriteLog(driver, destinationfilepath, "Data Read Successfully \n \n");




            //Comparison of CSV & Excel  Starts
            DistributeActions.WriteLog(driver, destinationfilepath, "**************************************");


            DistributeActions.WriteLog(driver, destinationfilepath, "Comparison of CSV & Excel  Starts \n \n");


            DistributeActions.WriteLog(driver, destinationfilepath, "*******************************************");


            List<string> al3 = new List<string>();
            string result1 = null;
            string result2 = null;
            string result3 = null;

            bool result = false;



            for (int i = 0; i < al1.Count; i++)


            {

                al3.Add(al1[i].Equals(al2[i]) ? "Pass" : "Fail");
                foreach (string cc in al3)
                {
                    result3 = cc;


                }

                DistributeActions.WriteLog(driver, destinationfilepath, "Data Comparison between CSV [" + sourcecsvfilename + " ] and Excel  [ " + destinationfilename + " ] for Data [ "+al1[i] + " ] is-- >  [ " + result3 + " ]");

            }


            if (al3.Contains("Fail"))



            {

                DistributeActions.WriteLog(driver, destinationfilepath, "********************************************\n\n");




                DistributeActions.WriteLog(driver, destinationfilepath, "Data Comparison between CSV [" + sourcecsvfilename + " ] and Excel  [ " + destinationfilename + " ]  is failed \n\n ");


                DistributeActions.WriteLog(driver, destinationfilepath, "********************************************\n\n");


                throw new Exception("Data Comparison between CSV [" + sourcecsvfilename + " ] and Excel  [ " + destinationfilename + " ]  is failed ");

                return false;

            }
            else
            {

                DistributeActions.WriteLog(driver, destinationfilepath, "********************************************\n\n");


                DistributeActions.WriteLog(driver, destinationfilepath, "Data Comparison between CSV [" + sourcecsvfilename + " ] and Excel  [ " + destinationfilename + " ]  is Passed \n\n ");

                DistributeActions.WriteLog(driver, destinationfilepath, "********************************************\n\n");

                return true;
            }



        
            


            return result;
    

            
        }




    public static bool verifyCSVColumnTextWithPPT(IWebDriver driver, string csvfilepath, string csvfilename, int srownum, int erownum, int scolnum, int ecolnum, string pptfilepath, string pptfilename, string slidenumber)
        {
            DistributeActions.WriteLog(driver, csvfilepath, "**********************");
            DistributeActions.WriteLog(driver, csvfilepath, "Inside verifyCSVColumnTextWithPPT \n \n");
            DistributeActions.WriteLog(driver, csvfilepath, "***********************");
            // Get the file's text.
            string FileLines = System.IO.File.ReadAllText(@csvfilepath + "\\" + csvfilename + ".csv");
            // Split into lines.
            FileLines = FileLines.Replace('\n', '\r');
            string[] lines = FileLines.Split(new char[] { '\r' },
                StringSplitOptions.RemoveEmptyEntries);
            // See how many rows and columns there are.
            int num_rows = lines.Length;
            int num_cols = lines[0].Split(',').Length;
            // Allocate the data array.
            string[,] al = new string[num_rows, num_cols];
            List<string> al1 = new List<string>(num_rows * num_cols);
            // Load the array.
            for (int r = srownum; r <= erownum; r++)
            {
                string[] line_r = lines[r].Split(',');
                for (int c = scolnum; c <= ecolnum; c++)
                {
                    al[r, c] = line_r[c];
                    al1.Add(al[r, c]);
                    DistributeActions.WriteLog(driver, csvfilepath, "  Data present in CSV at  Row [" + r + "] Column [ " + c + " ] has value -->[ " + al[r, c] + "]");
                }
            }
            //***************************Print PPT Data  using Aspose ****************************************
            bool result = false;
            string s = null;
            Aspose.Slides.License license = new Aspose.Slides.License();
            license.SetLicense("C:\\Automation\\Aspose.Slides.lic");
            List<string> al2 = new List<string>();
            Presentation pres = new Presentation(pptfilepath + "\\" + pptfilename);
            int slideCount = pres.Slides.Count;
            //Get an Array of ITextFrame objects from all slides in the PPTX
            //ITextFrame[] textFramesPPT = Aspose.Slides.Util.SlideUtil.GetAllTextFrames(pres, true);
            int slidenumber1 = Int32.Parse(slidenumber);
            ISlide slide = pres.Slides[slidenumber1];
            //Get an Array of TextFrameEx objects from the first slide
            ITextFrame[] textFramesPPT = Aspose.Slides.Util.SlideUtil.GetAllTextBoxes(slide);
            //Loop through the Array of TextFrames
            for (int i = 0; i < textFramesPPT.Length; i++)
            {
                //Loop through paragraphs in current ITextFrame
                foreach (IParagraph para in textFramesPPT[i].Paragraphs)
                {
                    //Loop through portions in the current IParagraph
                    foreach (IPortion port in para.Portions)
                    {
                        //Display text in the current portion
                        Console.WriteLine(port.Text);
                        s = port.Text;
                        al2.Add(s);
                    }
                }
                foreach (string item in al2)
                {
                    DistributeActions.WriteLog(driver, pptfilepath, "  Data in slide number  ---> " + slidenumber + "    is -->" + item);
                }
            }
            //*********************************************
            //Comparing csv data with ppt data
            //Storing the comparison output in ArrayList<String>

            List<string> al3 = new List<string>();
            string result1 = null;
            string result2 = null;
            string result3 = null;
            //foreach (string temp in al1)
            //{
            //    result1 = temp;
            foreach (string item in al1)
            {
                result2 = item;
                al3.Add(al2.Contains(item) ? "Pass" : "Fail");
                foreach (string cc in al3)
                {
                    result3 = cc;
                }
            }
            //}
            DistributeActions.WriteLog(driver, pptfilepath, "Data Comparison between Csv data and PPT data is -->  [" + result3 + " ]");
            if (al3.Contains("Pass"))
            {
                DistributeActions.WriteLog(driver, pptfilepath, "Data Comparison between csv and ppt is passed ");

                return true;
            }
            else
            {
                DistributeActions.WriteLog(driver, pptfilepath, "Data Comparison between csv and ppt is failed ");
                throw new Exception("Data Comparison between csv and ppt is failed ");
                return false;
            }
            return result;
        }
        public static bool DeleteCompanyDirectoryParticipant(string emailid)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["DataModel"].ToString()))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = sqlConn;
                    if (command.Connection.State == ConnectionState.Closed)
                        if (command.Connection.State == ConnectionState.Closed)
                            sqlConn.Open();
                    command.CommandText = @"update CompanyDirectoryParticipant set [status]=0 where emailaddress = '" + emailid + "'";
                    //command.CommandText = "SELECT TOP 1 SurveyWaveID FROM SurveyWaveParticipant WHERE SurveyID = @SurveyID AND [Status] = 1 AND IsSystemGenerated = 0 AND SurveyStatusFlag = 0";
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string data = reader.GetString(0);
                    }
                    reader.Close();
                    sqlConn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static int GetCompanyDirectoryParticipant(string emailid)
        {
            int data = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["DataModel"].ToString()))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = sqlConn;
                    if (command.Connection.State == ConnectionState.Closed)
                        if (command.Connection.State == ConnectionState.Closed)
                            sqlConn.Open();
                    command.CommandText = @"select count(*) from CompanyDirectoryParticipant where [status]= 1 and  emailaddress = '" + emailid + "'";
                    //command.CommandText = "SELECT TOP 1 SurveyWaveID FROM SurveyWaveParticipant WHERE SurveyID = @SurveyID AND [Status] = 1 AND IsSystemGenerated = 0 AND SurveyStatusFlag = 0";
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        data = reader.GetInt32(0);
                    }
                    reader.Close();
                    sqlConn.Close();
                    return data;
                }
            }
            catch (Exception ex)
            {
                return data;
            }
        }
        public static DataSet ExecuteSPwithInputParameters(string InputSPText, DBConnectionStringDTO DBConnectionParameters)
        {
            try
            {
                DataSet ds = new DataSet();
                //  connectionString = "data source =" + DBConnectionParameters.TCESReportingserverName + ";Initial Catalog="
                //    + DBConnectionParameters.TCESReportingDB + ";User ID=" + DBConnectionParameters.userName +
                //";Password=" + DBConnectionParameters.password;
                connectionString = "Server=" + DBConnectionParameters.TCESReportingserverName + ";Database=" + DBConnectionParameters.TCESReportingDB + ";User ID = " + DBConnectionParameters.userName +
               ";Password=" + DBConnectionParameters.password + ";";
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    if (sqlConn.State == ConnectionState.Closed)
                    {
                        sqlConn.Open();
                         Console.WriteLine("Connection is Opened");
                    }
                    SqlCommand command = new SqlCommand(InputSPText, sqlConn);
                    command.Connection = sqlConn;
                    command.CommandTimeout = 3600;
                    command.CommandType = CommandType.Text;
                    command.CommandText = InputSPText;
                    
                    Console.WriteLine("Executing the Input SP query2");
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    command.CommandTimeout = 3600;
                    adapter.Fill(ds);
                
                    Console.WriteLine("SP execution is completed");
                    sqlConn.Close();
                }
                return ds;
            }
           catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("Input Query : " + InputSPText, w);
                }
                return null;
            }
        }



        public static DataSet ExecuteSPwithInputParameters_TCES(string InputSPText, DBConnectionStringDTO DBConnectionParameters)
        {
            try
            {
                DataSet ds = new DataSet();
                //  connectionString = "data source =" + DBConnectionParameters.TCESReportingserverName + ";Initial Catalog="
                //    + DBConnectionParameters.TCESReportingDB + ";User ID=" + DBConnectionParameters.userName +
                //";Password=" + DBConnectionParameters.password;
                connectionString = "Server=" + DBConnectionParameters.TCESserverName + ";Database=" + DBConnectionParameters.TCESDB + ";User ID = " + DBConnectionParameters.userName +
                     ";Password=" + DBConnectionParameters.password+ ";";
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    if (sqlConn.State == ConnectionState.Closed)
                    {
                        sqlConn.Open();
                        Console.WriteLine("Connection is Opened");
                    }
                    SqlCommand command = new SqlCommand(InputSPText, sqlConn);
                    command.Connection = sqlConn;
                    command.CommandTimeout = 3600;
                    command.CommandType = CommandType.Text;
                    command.CommandText = InputSPText;
                    Console.WriteLine("Executing the Input SP query");
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    Console.WriteLine("SP execution is completed");
                    sqlConn.Close();
                }
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }


        public static DataSet ExecuteTestSP(SqlConnection sqlConn, string SPQuery, int SurveyID, string DemoQuery, string TimePeriodQuery)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand(SPQuery, sqlConn);
                command.Connection = sqlConn;
                command.CommandTimeout = 3600;
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter _SurveyID = command.Parameters.Add("@SurveyID", SqlDbType.BigInt);
                _SurveyID.Direction = ParameterDirection.Input;
                _SurveyID.Value = SurveyID;
                SqlParameter _DemoQuery = command.Parameters.Add("@DemoQuery", SqlDbType.NVarChar);
                _DemoQuery.Direction = ParameterDirection.Input;
                _DemoQuery.Value = DemoQuery;
                SqlParameter _TimePeriodQuery = command.Parameters.Add("@TImePeriodQuery", SqlDbType.NVarChar);
                _TimePeriodQuery.Direction = ParameterDirection.Input;
                _TimePeriodQuery.Value = TimePeriodQuery;
                Console.WriteLine("Test SP - Executing the Command " + command.CommandText);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                Console.WriteLine("Test SP - Command execution completed");
                foreach (System.Data.DataTable table in ds.Tables)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        for (int k = 0; k < table.Columns.Count; k++)
                        {
                            Console.Write(table.Rows[j].ItemArray[k].ToString() + " ");
                        }
                        Console.WriteLine("");
                    }
                }
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static DataSet ExecuteTestSP_MultipleDistribution(SqlConnection sqlConn, string SPQuery, string SurveyIDs, string DemoQuery, string TimePeriodQuery)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand(SPQuery, sqlConn);
                command.Connection = sqlConn;
                command.CommandTimeout = 3600;
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter _SurveyID = command.Parameters.Add("@SurveyIDs", SqlDbType.NVarChar);
                _SurveyID.Direction = ParameterDirection.Input;
                _SurveyID.Value = SurveyIDs;
                SqlParameter _DemoQuery = command.Parameters.Add("@DemoQuery", SqlDbType.NVarChar);
                _DemoQuery.Direction = ParameterDirection.Input;
                _DemoQuery.Value = DemoQuery;
                SqlParameter _TimePeriodQuery = command.Parameters.Add("@TImePeriodQuery", SqlDbType.NVarChar);
                _TimePeriodQuery.Direction = ParameterDirection.Input;
                _TimePeriodQuery.Value = TimePeriodQuery;
                Console.WriteLine("Test SP - Executing the Command " + command.CommandText);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                Console.WriteLine("Test SP - Command execution completed");
                foreach (System.Data.DataTable table in ds.Tables)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        for (int k = 0; k < table.Columns.Count; k++)
                        {
                            Console.Write(table.Rows[j].ItemArray[k].ToString() + " ");
                        }
                        Console.WriteLine("");
                    }
                }
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static DataSet ExecuteTrendSP(SqlConnection sqlConn, string Query)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand(Query, sqlConn);
                command.Connection = sqlConn;
                command.CommandTimeout = 3600;
                command.CommandType = CommandType.Text;
                command.CommandText = Query;
                Console.WriteLine("Trend SP - Executing the Command");
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                Console.WriteLine("Trend SP - Command execution completed");
                //foreach (System.Data.DataTable table in ds.Tables)
                //{
                //    for (int j = 0; j < table.Rows.Count; j++)
                //    {
                //        for (int k = 0; k < table.Columns.Count; k++)
                //        {
                //            Console.Write(table.Rows[j].ItemArray[k].ToString() + " ");
                //        }
                //        Console.WriteLine("");
                //    }
                //}
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static DataSet ExecuteTestSP_NR(SqlConnection sqlConn, string SPQuery, int SurveyID, int SurveyFormItemID, string DemoQuery, string TimePeriodQuery)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand(SPQuery, sqlConn);
                command.Connection = sqlConn;
                command.CommandTimeout = 3600;
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter _SurveyID = command.Parameters.Add("@SurveyID", SqlDbType.BigInt);
                _SurveyID.Direction = ParameterDirection.Input;
                _SurveyID.Value = SurveyID;
                SqlParameter _SurveyFormItemID = command.Parameters.Add("@SurveyFormItemID", SqlDbType.BigInt);
                _SurveyFormItemID.Direction = ParameterDirection.Input;
                _SurveyFormItemID.Value = SurveyFormItemID;
                SqlParameter _DemoQuery = command.Parameters.Add("@DemoQuery", SqlDbType.NVarChar);
                _DemoQuery.Direction = ParameterDirection.Input;
                _DemoQuery.Value = DemoQuery;
                SqlParameter _TimePeriodQuery = command.Parameters.Add("@TImePeriodQuery", SqlDbType.NVarChar);
                _TimePeriodQuery.Direction = ParameterDirection.Input;
                _TimePeriodQuery.Value = TimePeriodQuery;
                Console.WriteLine("Test SP - Executing the Command " + command.CommandText);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                Console.WriteLine("Test SP - Command execution completed");
                //foreach (System.Data.DataTable table in ds.Tables)
                //{
                //    for (int j = 0; j < table.Rows.Count; j++)
                //    {
                //        for (int k = 0; k < table.Columns.Count; k++)
                //        {
                //            Console.Write(table.Rows[j].ItemArray[k].ToString() + " ");
                //        }
                //        Console.WriteLine("");
                //    }
                //}
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static DataSet ExecuteTestSP_NR_MultipleDistribution(SqlConnection sqlConn, string SPQuery, string SurveyIDs, int SurveyFormItemID, string DemoQuery, string TimePeriodQuery)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand(SPQuery, sqlConn);
                command.Connection = sqlConn;
                command.CommandTimeout = 3600;
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter _SurveyID = command.Parameters.Add("@SurveyIDs", SqlDbType.NVarChar);
                _SurveyID.Direction = ParameterDirection.Input;
                _SurveyID.Value = SurveyIDs;
                SqlParameter _SurveyFormItemID = command.Parameters.Add("@SurveyFormItemID", SqlDbType.BigInt);
                _SurveyFormItemID.Direction = ParameterDirection.Input;
                _SurveyFormItemID.Value = SurveyFormItemID;
                SqlParameter _DemoQuery = command.Parameters.Add("@DemoQuery", SqlDbType.NVarChar);
                _DemoQuery.Direction = ParameterDirection.Input;
                _DemoQuery.Value = DemoQuery;
                SqlParameter _TimePeriodQuery = command.Parameters.Add("@TImePeriodQuery", SqlDbType.NVarChar);
                _TimePeriodQuery.Direction = ParameterDirection.Input;
                _TimePeriodQuery.Value = TimePeriodQuery;
                Console.WriteLine("Test SP - Executing the Command " + command.CommandText);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                Console.WriteLine("Test SP - Command execution completed");
                foreach (System.Data.DataTable table in ds.Tables)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        for (int k = 0; k < table.Columns.Count; k++)
                        {
                            Console.Write(table.Rows[j].ItemArray[k].ToString() + " ");
                        }
                        Console.WriteLine("");
                    }
                }
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static DataSet ExecuteTestHeatMapSP(SqlConnection sqlConn, string SPName, string SPQuery, int SurveyID, string GroupBy)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand(SPName, sqlConn);
                command.Connection = sqlConn;
                command.CommandTimeout = 3600;
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter _SurveyID = command.Parameters.Add("@SurveyID", SqlDbType.BigInt);
                _SurveyID.Direction = ParameterDirection.Input;
                _SurveyID.Value = SurveyID;
                SqlParameter _DemoQuery = command.Parameters.Add("@SPQuery", SqlDbType.NVarChar);
                _DemoQuery.Direction = ParameterDirection.Input;
                _DemoQuery.Value = SPQuery;
                SqlParameter _TimePeriodQuery = command.Parameters.Add("@GroupBy", SqlDbType.NVarChar);
                _TimePeriodQuery.Direction = ParameterDirection.Input;
                _TimePeriodQuery.Value = GroupBy;
                Console.WriteLine("Test SP - Executing the Command " + command.CommandText);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                Console.WriteLine("Test SP - Command execution completed");
                //foreach (System.Data.DataTable table in ds.Tables)
                //{
                //    for (int j = 0; j < table.Rows.Count; j++)
                //    {
                //        for (int k = 0; k < table.Columns.Count; k++)
                //        {
                //            Console.Write(table.Rows[j].ItemArray[k].ToString() + " ");
                //        }
                //        Console.WriteLine("");
                //    }
                //}
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static DataSet ExecuteTestHeatMapSP_NR(SqlConnection sqlConn, string SPName, string SPQuery, int SurveyID, string GroupBy, string SurveyFormItemId)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlCommand command = new SqlCommand(SPName, sqlConn);
                command.Connection = sqlConn;
                command.CommandTimeout = 3600;
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter _SurveyID = command.Parameters.Add("@SurveyID", SqlDbType.BigInt);
                _SurveyID.Direction = ParameterDirection.Input;
                _SurveyID.Value = SurveyID;
                SqlParameter _DemoQuery = command.Parameters.Add("@SPQuery", SqlDbType.NVarChar);
                _DemoQuery.Direction = ParameterDirection.Input;
                _DemoQuery.Value = SPQuery;
                SqlParameter _TimePeriodQuery = command.Parameters.Add("@GroupBy", SqlDbType.NVarChar);
                _TimePeriodQuery.Direction = ParameterDirection.Input;
                _TimePeriodQuery.Value = GroupBy;
                SqlParameter _SurveyFormItemId = command.Parameters.Add("@SurveyFormItemId", SqlDbType.BigInt);
                _SurveyFormItemId.Direction = ParameterDirection.Input;
                _SurveyFormItemId.Value = Int32.Parse(SurveyFormItemId);
                Console.WriteLine("Test SP - Executing the Command " + command.CommandText);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                Console.WriteLine("Test SP - Command execution completed");
                //foreach (System.Data.DataTable table in ds.Tables)
                //{
                //    for (int j = 0; j < table.Rows.Count; j++)
                //    {
                //        for (int k = 0; k < table.Columns.Count; k++)
                //        {
                //            Console.Write(table.Rows[j].ItemArray[k].ToString() + " ");
                //        }
                //        Console.WriteLine("");
                //    }
                //}
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public static bool verifyTextnotpresentinPowerPoint(IWebDriver driver, string filepath, string filename, string slidenumber, string texttocompare)
        {
            bool result = false;
            //**************************************Interop ************************************
            //  string presentation_text = "";
            //  Microsoft.Office.Interop.PowerPoint.Application PowerPoint_App = new Microsoft.Office.Interop.PowerPoint.Application();
            //Microsoft.Office.Interop.PowerPoint.Presentations multi_presentations = PowerPoint_App.Presentations;
            //  Microsoft.Office.Interop.PowerPoint.Presentation presentation = multi_presentations.Open(@"C:\Automation\SpotlightReport3627237636684590143688007.ppt", Microsoft.Office.Core.MsoTriState.msoTrue,
            //       Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoFalse);
            //  PowerPoint.Slides pptSlides = presentation.Slides;
            //      if (pptSlides != null)
            //      {
            //          var slidesCount = pptSlides.Count;
            //          StringBuilder sb = new StringBuilder();
            //          if (slidesCount > 0)
            //          {
            //              for (int slideIndex = 1; slideIndex <= slidesCount; slideIndex++)
            //              {
            //                  var slide = pptSlides[slideIndex];
            //                  foreach (PowerPoint.Shape textShape in slide.Shapes)
            //                  {
            //                      if (textShape.HasTextFrame == MsoTriState.msoTrue &&
            //                             textShape.TextFrame.HasText == MsoTriState.msoTrue)
            //                      {
            //                          PowerPoint.TextRange pptTextRange = textShape.TextFrame.TextRange;
            //                          if (pptTextRange != null && pptTextRange.Length > 0)
            //                          {
            //                              //     sb = stringBuilder.Append(" " + pptTextRange.Text);
            //                              var textRange = textShape.TextFrame.TextRange;
            //                              var text = textRange.Text;
            //                              presentation_text = text + " ";
            //                          }
            //                      }
            //                  }
            //              }
            //          }
            //      }
            //***************************Aspose ****************************************
            string s = null;
            Aspose.Slides.License license = new Aspose.Slides.License();
            license.SetLicense("C:\\Automation\\Aspose.Slides.lic");
            ArrayList al2 = new ArrayList();
            //Load the desired the presentation
            //            Presentation pres = new Presentation(@"C:\Automation\SpotlightReport3627237636684590143688007.ppt");
            //           Presentation pres = new Presentation(filepath+"\\SpotlightReport3627237636684590143688007.ppt");
            Presentation pres = new Presentation(filepath + "\\" + filename);
            //using (Presentation prestg = new Presentation(@"C:\Automation\SpotlightReport3627237636684590143688007.ppt"))
            //{
            int slideCount = pres.Slides.Count;
            //Get an Array of ITextFrame objects from all slides in the PPTX
            //ITextFrame[] textFramesPPT = Aspose.Slides.Util.SlideUtil.GetAllTextFrames(pres, true);
            int slidenumber1 = Int32.Parse(slidenumber);
            ISlide slide = pres.Slides[slidenumber1];
            //Get an Array of TextFrameEx objects from the first slide
            ITextFrame[] textFramesPPT = Aspose.Slides.Util.SlideUtil.GetAllTextBoxes(slide);
            //Loop through the Array of TextFrames
            for (int i = 0; i < textFramesPPT.Length; i++)
            {
                //Loop through paragraphs in current ITextFrame
                foreach (IParagraph para in textFramesPPT[i].Paragraphs)
                {
                    //Loop through portions in the current IParagraph
                    foreach (IPortion port in para.Portions)
                    {
                        //Display text in the current portion
                        Console.WriteLine(port.Text);
                        s = port.Text;
                        //        DistributeActions.WriteLog(driver, filepath, s);
                        al2.Add(s);
                    }
                }
                foreach (var item in al2)
                {
                    DistributeActions.WriteLog(driver, filepath, "  Data in slide number  ---> " + slidenumber + "    is -->" + item);
                }
            }
            if (!(al2.Contains(texttocompare)))
            {
                DistributeActions.WriteLog(driver, filepath, "Data -->" + texttocompare + " -->is not  present in ppt !! Test Case Passed ");
                return true;
            }
            else
            {
                DistributeActions.WriteLog(driver, filepath, "Data -->" + texttocompare + " -->is  present in ppt !! Test Case Failed ");
                throw new Exception("Data -->" + texttocompare + " -->is   present in ppt ");
                return false;
            }
            return result;
        }
        public static bool verifyTextpresentinPowerPoint(IWebDriver driver, string filepath, string filename, string slidenumber, string texttocompare)
        {
            bool result = false;
            //**************************************Interop ************************************
            //  string presentation_text = "";
            //  Microsoft.Office.Interop.PowerPoint.Application PowerPoint_App = new Microsoft.Office.Interop.PowerPoint.Application();
            //Microsoft.Office.Interop.PowerPoint.Presentations multi_presentations = PowerPoint_App.Presentations;
            //  Microsoft.Office.Interop.PowerPoint.Presentation presentation = multi_presentations.Open(@"C:\Automation\SpotlightReport3627237636684590143688007.ppt", Microsoft.Office.Core.MsoTriState.msoTrue,
            //       Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoFalse);
            //  PowerPoint.Slides pptSlides = presentation.Slides;
            //      if (pptSlides != null)
            //      {
            //          var slidesCount = pptSlides.Count;
            //          StringBuilder sb = new StringBuilder();
            //          if (slidesCount > 0)
            //          {
            //              for (int slideIndex = 1; slideIndex <= slidesCount; slideIndex++)
            //              {
            //                  var slide = pptSlides[slideIndex];
            //                  foreach (PowerPoint.Shape textShape in slide.Shapes)
            //                  {
            //                      if (textShape.HasTextFrame == MsoTriState.msoTrue &&
            //                             textShape.TextFrame.HasText == MsoTriState.msoTrue)
            //                      {
            //                          PowerPoint.TextRange pptTextRange = textShape.TextFrame.TextRange;
            //                          if (pptTextRange != null && pptTextRange.Length > 0)
            //                          {
            //                              //     sb = stringBuilder.Append(" " + pptTextRange.Text);
            //                              var textRange = textShape.TextFrame.TextRange;
            //                              var text = textRange.Text;
            //                              presentation_text = text + " ";
            //                          }
            //                      }
            //                  }
            //              }
            //          }
            //      }
            //***************************Aspose ****************************************
            string s = null;
            Aspose.Slides.License license = new Aspose.Slides.License();
            license.SetLicense("C:\\Automation\\Aspose.Slides.lic");
            ArrayList al2 = new ArrayList();
            //Load the desired the presentation
            //            Presentation pres = new Presentation(@"C:\Automation\SpotlightReport3627237636684590143688007.ppt");
            //           Presentation pres = new Presentation(filepath+"\\SpotlightReport3627237636684590143688007.ppt");
            Presentation pres = new Presentation(filepath + "\\" + filename);
            //using (Presentation prestg = new Presentation(@"C:\Automation\SpotlightReport3627237636684590143688007.ppt"))
            //{
            int slideCount = pres.Slides.Count;
            //Get an Array of ITextFrame objects from all slides in the PPTX
            //ITextFrame[] textFramesPPT = Aspose.Slides.Util.SlideUtil.GetAllTextFrames(pres, true);
            int slidenumber1 = Int32.Parse(slidenumber);
            ISlide slide = pres.Slides[slidenumber1];
            //Get an Array of TextFrameEx objects from the first slide
            ITextFrame[] textFramesPPT = Aspose.Slides.Util.SlideUtil.GetAllTextBoxes(slide);
            //Loop through the Array of TextFrames
            for (int i = 0; i < textFramesPPT.Length; i++)
            {
                //Loop through paragraphs in current ITextFrame
                foreach (IParagraph para in textFramesPPT[i].Paragraphs)
                {
                    //Loop through portions in the current IParagraph
                    foreach (IPortion port in para.Portions)
                    {
                        //Display text in the current portion
                        Console.WriteLine(port.Text);
                        s = port.Text;
                        //        DistributeActions.WriteLog(driver, filepath, s);
                        al2.Add(s);
                    }
                }
                foreach (var item in al2)
                {
                    DistributeActions.WriteLog(driver, filepath, "  Data in slide number  ---> " + slidenumber + "    is -->" + item);
                }
            }
            if (al2.Contains(texttocompare))
            {
                DistributeActions.WriteLog(driver, filepath, "Data -->" + texttocompare + " -->is present in ppt !! Test Case Passed ");
                return true;
            }
            else
            {
                DistributeActions.WriteLog(driver, filepath, "Data -->" + texttocompare + " -->is  not  present in ppt !! Test Case Failed ");
                throw new Exception("Data -->" + texttocompare + " -->is  not  present in ppt ");
                return false;
            }
            return result;
        }

        public static List<int> GetUserAnalysisIds(int userId, int surveyFormId, DBConnectionStringDTO DBConnectionParameters)
        {
            connectionString = "Server=" + DBConnectionParameters.TCESReportingserverName + ";Database=" + DBConnectionParameters.TCESReportingDB + ";User ID = " + DBConnectionParameters.userName +
                    ";Password=" + DBConnectionParameters.password + ";";
            List<int> ids = new List<int>();
            string query = "SELECT UserAnalyzeFilterID FROM UserAnalyzeFilter WHERE UserID=" + userId + " AND SurveyFormID=" + surveyFormId;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        ids.Add(Convert.ToInt32(reader["UserAnalyzeFilterID"]));
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return ids;

        }

        public static void DeleteUserAnalysisRecords(int userAnalyseFilterId, DBConnectionStringDTO DBConnectionParameters)
        {
            connectionString = "Server=" + DBConnectionParameters.TCESReportingserverName + ";Database=" + DBConnectionParameters.TCESReportingDB + ";User ID = " + DBConnectionParameters.userName +
                   ";Password=" + DBConnectionParameters.password + ";";
            SqlConnection conn = new  SqlConnection(connectionString);
            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.CommandText = @"DELETE FROM UserAnalyzeFilter WHERE UserAnalyzeFilterID =" + userAnalyseFilterId
                                     + "; DELETE FROM DemoFilter WHERE UserAnalyzeFilterID =" + userAnalyseFilterId
                                     + "; DELETE FROM SurveyFilter WHERE UserAnalyzeFilterID =" + userAnalyseFilterId
                                     + "; DELETE FROM DemoRangeFilter WHERE UserAnalyzeFilterID =" + userAnalyseFilterId +
                                     ";";
            cmdCommand.CommandType = CommandType.Text;
            cmdCommand.Connection = conn;
            try
            {
                conn.Open();
                cmdCommand.ExecuteNonQuery();
            }
            finally
            {
                cmdCommand.Dispose();
                conn.Dispose();
            }

        }

        public static List<string> GetFavName(DBConnectionStringDTO DBConnectionParameters)
        {
            connectionString = "Server=" + DBConnectionParameters.TCESReportingserverName + ";Database=" + DBConnectionParameters.TCESReportingDB + ";User ID = " + DBConnectionParameters.userName +
                    ";Password=" + DBConnectionParameters.password + ";";
            List<string> ids = new List<string>();
            string query = "select distinct favouritename  from useranalyzefilter where clientid = 37400 and favouritename is not null";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);

                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        ids.Add(Convert.ToString(reader["favouritename"]));
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return ids;

        }
    }
}