﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace Common
{
    public class WebDriverFactory
    {

        public static IWebDriver GetWebDriver(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    driver = new FirefoxDriver();
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "Chrome":
                default:
                    driver = new ChromeDriver();
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public static void Logout(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(driver.Url.Substring(0, driver.Url.IndexOf("Pulse/")) + "Pulse/Home/Logout");

        }
        public static void Login(IWebDriver driver, string username, string password, string accountName)
        {
            try
            {
                IWebElement usernameTextBox = driver.FindElement(By.Id("j_username"));
                IWebElement passwordTextBox = driver.FindElement(By.Id("j_password"));
                usernameTextBox.Clear();
                passwordTextBox.Clear();
                usernameTextBox.SendKeys(username);
                passwordTextBox.SendKeys(password);
                usernameTextBox.Submit();
                Thread.Sleep(10000);
                IWebElement accountselectionComboBox = new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                    .Until(ExpectedConditions.ElementExists(By.Id("SelectedUserAccountCode")));
                //.Until(ExpectedConditions.ElementExists(By.Id("j_company")));
                var accountdropdown = new SelectElement(accountselectionComboBox);
                accountdropdown.SelectByText(accountName);
                accountselectionComboBox.Submit();
                UIActions.GetElementWithWait(driver, "#navbar", 420);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static RemoteWebDriver GetWebDriverwithcapabilities(string driverName, string url, string filepath)
        {
            RemoteWebDriver driver = null;

            switch (driverName)
            {
                case "Firefox":
                    FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    driver = new FirefoxDriver(service);


                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", filepath);
                    options.SetPreference("browser.download.downloadDir", filepath);
                    options.SetPreference("browser.download.defaultFolder", filepath);


                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);

                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                        "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);

                    break;

                case "IE":
                    driver = new InternetExplorerDriver();

                    break;
                case "Chrome":
                default:

                    var chromeOptions = new ChromeOptions();
                 
                    chromeOptions.AddUserProfilePreference("download.default_directory", filepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    driver = new ChromeDriver(chromeOptions);

                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;

        }
    }
}
