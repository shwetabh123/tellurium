﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Common
{
    public class CustomReportValidation
    {
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";
        public static bool CompareSuppressionForInsightReport(DataSet ActualSP, DataSet TestSP, string ResultDisplay, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                bool Status = true;
                string ColumnName = "ObjectID";
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                string TestSPCompColName;
                string ResponseCol = null;

                string CategoryOrItemID = (DataCalculation.ToLower() == "category") ? "CategoryID" : "SurveyFormItemID";

                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_One_FavPerc_Diff" : "Comp_One_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_One_TotalRespCount_Diff";
                        TestSPCompColName = (DataCalculation.ToLower() == "percentage") ? "Favorable" : "Mean";
                        break;
                    case 2:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Two_FavPerc_Diff" : "Comp_Two_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_Two_TotalRespCount_Diff";
                        TestSPCompColName = (DataCalculation.ToLower() == "percentage") ? "Favorable" : "Mean";
                        break;
                    case 3:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Three_FavPerc_Diff" : "Comp_Three_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_Three_TotalRespCount_Diff";
                        TestSPCompColName = (DataCalculation.ToLower() == "percentage") ? "Favorable" : "Mean";
                        break;
                    default:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "FavPerc" : "MeanScore";
                        ActualSPCompColumnResp = "TotalRespCount";
                        TestSPCompColName = (DataCalculation.ToLower() == "percentage") ? "Favorable" : "Mean";
                        break;
                }
                if (TestSP.Tables[0].Columns.Contains("BenchmarkFavPerc") == false)
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                    ResponseCol = (ResultDisplay.ToLower() == "category") ? "CategoryTotalResponses" : "Total Responses";
                    TestSPDv = DT_TestSP.DefaultView;
                    TestSPDv.Sort = "ObjectID ASC";
                    DT_TestSP = TestSPDv.ToTable();
                }
                else
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    if (ResultDisplay.ToLower() == "category")
                    {
                        DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[2]);
                        DT_TestSP = TestSPDv.ToTable(false, "ObjectID", "BenchmarkFavPerc", "BenchmarkMean", "Total Responses");
                        TestSPDv = DT_TestSP.DefaultView;
                        TestSPDv.Sort = "ObjectID ASC";
                        DT_TestSP = TestSPDv.ToTable();
                        ColumnName = "ObjectID";
                    }
                    else
                    {
                        DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                        DT_TestSP = TestSPDv.ToTable(false, "SurveyFormItemID", "BenchmarkFavPerc", "Mean", "Total Responses");
                        TestSPDv = DT_TestSP.DefaultView;
                        TestSPDv.Sort = "SurveyFormItemID ASC";
                        DT_TestSP = TestSPDv.ToTable();
                        ColumnName = "SurveyFormItemID";
                    }
                }

                if (GroupOrder == 0)
                {
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    DT_ActualSP = StrengthDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp);

                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    DT_ActualSP.Merge(OpportunitiesDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp));
                    DT_ActualSP.DefaultView.Sort = "objectId ASC";

                    foreach (DataRow testRow in DT_TestSP.Rows)
                    {
                        foreach (DataRow actualRow in DT_ActualSP.DefaultView.ToTable().Rows)
                        {
                            if (testRow["ObjectID"].ToString() == actualRow["objectId"].ToString())
                            {
                                if (DataCalculation.ToLower() == "percentage")
                                {
                                    if (
                                         Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["FavPerc"])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Favorable"])), 0) ||
                                         Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["NeutralPerc"])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Neutral"])), 0) ||
                                         Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["UnFavPerc"])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Unfavorable"])), 0) ||
                                         Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["TotalRespCount"])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Total Responses"])), 0)
                                        )
                                    {
                                        Status = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Test SP Target Fav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Favorable"])), 0)
                                                + "\tActual SP Target Fav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["FavPerc"])), 0), w);

                                            CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Test SP Target Neutral : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Neutral"])), 0)
                                                + "\tActual SP Target Neutral : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["NeutralPerc"])), 0), w);

                                            CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Target Test SP UnFav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Unfavorable"])), 0)
                                                + "\tActual SP Target UnFav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["UnFavPerc"])), 0), w);

                                            CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Target Test SP Response Count : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Total Responses"])), 0)
                                                + "\tActual SP Target Response Count : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["TotalRespCount"])), 0), w);
                                        }
                                    }
                                }
                                else
                                {
                                    if (
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["MeanScore"])), 2) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Mean"])), 2) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["TotalRespCount"])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Total Responses"])), 0)
                                    )
                                    {
                                        Status = false;
                                        using (StreamWriter w = File.AppendText(LogFileName))
                                        {
                                            CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Test SP Target Mean: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Mean"])), 0)
                                                + "\tActual SP Target Mean: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["MeanScore"])), 0), w);

                                            CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Target Test SP Response Count : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Total Responses"])), 0)
                                                + "\tActual SP Target Response Count : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow["TotalRespCount"])), 0), w);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    DT_ActualSP = StrengthDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp);

                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    DT_ActualSP.Merge(OpportunitiesDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp));
                    DT_ActualSP.DefaultView.Sort = "objectId ASC";

                    foreach (DataRow testRow in DT_TestSP.Rows)
                    {
                        foreach (DataRow actualRow in DT_ActualSP.DefaultView.ToTable().Rows)
                        {
                            if (testRow[ColumnName].ToString() == actualRow["objectId"].ToString())
                            {
                                int RoundNum = (DataCalculation.ToLower() == "percentage") ? 0 : 2;
                                if (
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow[ActualSPCompColumnName])), RoundNum) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow[TestSPCompColName])), RoundNum) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow[ActualSPCompColumnResp])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Total Responses"])), 0)
                                    )
                                {
                                    Status = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Test SP Comp " + GroupOrder + " Response Count: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow[TestSPCompColName])), RoundNum)
                                            + "\tActual SP Comp " + GroupOrder + " Response Count: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow[ActualSPCompColumnName])), RoundNum), w);

                                        CommonActions.Log("\t" + CategoryOrItemID + " = " + testRow["ObjectId"] + "\tInsight Test SP Comp " + GroupOrder + " Response Count: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(testRow["Total Responses"])), 0)
                                            + "\tActual SP Comp " + GroupOrder + " Response Count: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(actualRow[ActualSPCompColumnResp])), 0), w);
                                    }
                                }
                            }
                        }
                    }
                }
                return Status;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    CommonActions.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
    }
}
