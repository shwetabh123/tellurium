﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ResultsReportDbOperations
    {
        private string connectionString;



        public void  Insert3Comparisions(int Userid, int clientid ,int surveyformid,string userName, string password, string serverName, List<int> benchmarkids, string demolabel = "")
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;
            List<MatchPairParticipants> participants = new List<MatchPairParticipants>();

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCESReporting;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    List<int> groupids = new List<int> { 1, 2, 3 };
                    int index = 0;
                    string demo = string.IsNullOrEmpty(demolabel) ? "null" : @"'" + demolabel + @"'";
                    // 1. create a command object identifying
                    groupids.ForEach(t =>
                       {
                           string  benchmarkid = benchmarkids != null && benchmarkids.Count > index ? benchmarkids[index++].ToString() : "null";
                           string nullstring = "null";
                           string datasourceid = benchmarkid != nullstring ? "1" : "2";
                            string insertsql = @"INSERT INTO [dbo].[UserAnalyzeFilter]
                           ([UserID]
                           ,[ClientID]
                           ,[SurveyFormID]
                           ,[GroupTypeID]
                           ,[GroupOrder]
                           ,[GroupColor]
                           ,[DataSourceID]
                           ,[IsLastSaved]
                           ,[IsFavourite]
                           ,[FavouriteName]
                           ,[BenchMarkID]
                           ,[CreatedBy]
                           ,[LastUpdatedBy]
                           ,[InsertTimestampGMT]
                           ,[UpdateTimestampGMT]
                           ,[IsTimePeriodIncluded]
                           ,[TimePeriod]
                           ,[StartDate]
                           ,[EndDate]
                           ,[DemoLabel])
                            VALUES (" + 
                            Userid.ToString() + "," + clientid.ToString() + "," + surveyformid.ToString()
                            +
                            @",2," + t.ToString() +
                            @"
                           ,null
                           ," + datasourceid
                           + @",1
                           ,0
                           ,''
                           ," + benchmarkid
                           + @",2854515
                           ,2854515
                           , getutcdate()
                           ,getutcdate()
                           ,0
                           ,1
                           ,null
                           ,null
                           ," + demo + ")";

                           // the stored procedure
                           SqlCommand cmd = new SqlCommand(
                               insertsql, sqlConn);

                           // 2. set the command object so it knows
                           // to execute a stored procedure
                           cmd.CommandType = CommandType.Text;


                           // execute the command
                           cmd.ExecuteNonQuery();

                       });


                }
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }
           
        }
        public void InsertDemofilters(int Userid, int clientid, int surveyformid, string userName, string password, string serverName)
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;
            List<MatchPairParticipants> participants = new List<MatchPairParticipants>();

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCESReporting;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    List<string> demolabels = new List<string> { "gender","marital status" };
                    List<string> demovals = new List<string> { "Female", "Married" };

                    int index = 0;
                    // 1. create a command object identifying
                    demolabels.ForEach(t =>
                    {
                        string insertsql = @" INSERT INTO [dbo].[DemoFilter]
                                   ([UserAnalyzeFilterID]
                                   ,[DemoLabel]
                                   ,[DemoValue]
                                   ,[IsRemoved]) select  UserAnalyzeFilterID, '" + t + @"','" + demovals[index++] +
                                   @"',0 from [dbo].[UserAnalyzeFilter] where userid = " + Userid.ToString() + @" and GroupOrder = 1 and surveyformid = " + surveyformid.ToString() ;

                        // the stored procedure
                        SqlCommand cmd = new SqlCommand(
                            insertsql, sqlConn);

                        // 2. set the command object so it knows
                        // to execute a stored procedure
                        cmd.CommandType = CommandType.Text;


                        // execute the command
                        cmd.ExecuteNonQuery();

                    });


                }
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }

        }
        public int GetUserIdForThisEmail(string EmailAddress, string userName, string password, string serverName)
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;
            int userid = 0;
            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                int excludedemo = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    string selectstmt = @"Select Userid from [dbo].[user] where Email = '" + EmailAddress + @"'";
                    // 1. create a command object identifying
                    // the stored procedure
                    SqlCommand cmd = new SqlCommand(
                        selectstmt, sqlConn);

                    // 2. set the command object so it knows
                    // to execute a stored procedure
                    cmd.CommandType = CommandType.Text;

                    // 3. add parameter to command, which
                    // will be passed to the stored procedure
              
                    // execute the command
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {

                        // iterate through results, printing each to console
                        rdr.Read();
                        {
                            userid = Convert.ToInt32(rdr["Userid"]);
                 
                        }
                    }
                }


            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }
            return userid;
        }
        public void DeleteComparisions(int Userid,  int surveyformid, string userName, string password, string serverName)
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;
            List<MatchPairParticipants> participants = new List<MatchPairParticipants>();

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCESReporting;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                   
                    // 1. create a command object identifying
                   
                        string deletesql = @"delete from  [dbo].[UserAnalyzeFilter]
                        where userid =  " + Userid.ToString() + " and surveyformid = "
                                          + surveyformid.ToString();
                        string deletesql2 = @"delete from  [dbo].[Demofilter]
                        where UserAnalyzeFilterid in (select UserAnalyzeFilterid from [dbo].[UserAnalyzeFilter] where  userid =  " + Userid.ToString() + " and surveyformid = "
                                   + surveyformid.ToString() + ")"; 
                    List<string> stmts = new List<string> { deletesql2, deletesql };
                     stmts.ForEach(t =>
                     {
                         // the stored procedure 
                         SqlCommand cmd = new SqlCommand(
                            t, sqlConn);

                         // 2. set the command object so it knows
                         // to execute a stored procedure
                         cmd.CommandType = CommandType.Text;


                         // execute the command
                         cmd.ExecuteNonQuery();
                     });

                 


                }
            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }
            
        }
    }
}
