﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspose.Cells;
using System.Data;

namespace Common
{
    public class ExcelOperations
    {
        public static DataTable ReadFromExcelInput(string InputFileName)
        {
            Workbook workBook = new Workbook(InputFileName);
            Console.WriteLine("Calling the ReadExcel Method");
            int numSheets = workBook.Worksheets.Count;
            DataTable dt = new DataTable();
            Worksheet worksheet = workBook.Worksheets[0];
            Cells cells = worksheet.Cells;
            List<string> myList = new List<string>();
            int LastRow = cells.MaxDataRow;
            int LastColumn = cells.MaxDataColumn;
            for (int i = 0; i <= LastColumn; i++)
            {
                myList.Add(Convert.ToString((worksheet.Cells[0, i]).Value));
            }
            foreach (string list in myList)
            {
                dt.Columns.Add(list);
            }
            for (int i = 0; i <= LastRow; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j <= LastColumn; j++)
                {
                    dr[j] = Convert.ToString((worksheet.Cells[i,j]).Value);
                }
                dt.Rows.Add(dr);
                dt.AcceptChanges();
            }
            workBook.Dispose();
            return dt;
        }
    }
}
