﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium.Remote;
using System.Drawing;

namespace Common
{
    public class AccountDefaults
    {
        public static void NavigateToAccountDefaults(IWebDriver driver)
        {
            try
            {
                UIActions.Click(driver, ".user-info");
                UIActions.Click(driver, "[href='/Pulse/AccountSettings']");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#menu-account-defaults");
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void CreateNewStyleSheet(IWebDriver driver, string StyleName, StyleSheetParametersDTO styleSheetParameters)
        {
            try
            {
                UIActions.Click(driver, "#defaults-create-new-stylesheet");
                Thread.Sleep(3000);
                IWebElement StyleSheetName = UIActions.FindElement(driver, "#stylesheet-name");
                IWebElement PrimaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-primary-background-text");
                IWebElement SecondaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-secondary-background-text");
                IWebElement TitleBarBckText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-background-text");
                IWebElement TitleBarText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-text-text");
                IWebElement SectionHeadingText = UIActions.FindElement(driver, "#stylesheet-color-picker-section-heading-text-text");
                IWebElement SelectionOptionText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-option-text");
                IWebElement SelectionOutlineText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-outline-text");
                IWebElement ScaleLabelText = UIActions.FindElement(driver, "#stylesheet-color-picker-scale-label-text-text");
                
                StyleSheetName.Clear();
                StyleSheetName.SendKeys(StyleName);
            
                PrimaryBckColor.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                PrimaryBckColor.SendKeys(OpenQA.Selenium.Keys.Delete);
              PrimaryBckColor.SendKeys(styleSheetParameters.BackgroundColor);

                SecondaryBckColor.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                SecondaryBckColor.SendKeys(OpenQA.Selenium.Keys.Delete);
                SecondaryBckColor.SendKeys(styleSheetParameters.SecondaryBackgroundColor);

                TitleBarBckText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                TitleBarBckText.SendKeys(OpenQA.Selenium.Keys.Delete);
                TitleBarBckText.SendKeys(styleSheetParameters.TitleBarColor);


                TitleBarText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                TitleBarText.SendKeys(OpenQA.Selenium.Keys.Delete);
                TitleBarText.SendKeys(styleSheetParameters.TitleFontColor);

                SectionHeadingText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                SectionHeadingText.SendKeys(OpenQA.Selenium.Keys.Delete);
                SectionHeadingText.SendKeys(styleSheetParameters.SectionHeadingFontColor);

                SelectionOptionText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                SelectionOptionText.SendKeys(OpenQA.Selenium.Keys.Delete);
                SelectionOptionText.SendKeys(styleSheetParameters.SelectionColor);

                SelectionOutlineText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                SelectionOutlineText.SendKeys(OpenQA.Selenium.Keys.Delete);
                SelectionOutlineText.SendKeys(styleSheetParameters.SelectionOutlineColor);

                ScaleLabelText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                ScaleLabelText.SendKeys(OpenQA.Selenium.Keys.Delete);
                ScaleLabelText.SendKeys(styleSheetParameters.ScaleLabelFontColor);

                UIActions.Click(driver, "#btn-stylesheets-save");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void EditStyleSheet(IWebDriver driver, string StyleName, StyleSheetParametersDTO styleSheetParameters, string NewName = null)
        {
            try
            {

           
                UIActions.Click(driver, "#defaults-edit-stylesheets");
                Thread.Sleep(3000);
                IWebElement StyleSheetName = UIActions.FindElement(driver, "#stylesheet-name");
                IWebElement PrimaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-primary-background-text");
                IWebElement SecondaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-secondary-background-text");
                IWebElement TitleBarBckText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-background-text");
                IWebElement TitleBarText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-text-text");
                IWebElement SectionHeadingText = UIActions.FindElement(driver, "#stylesheet-color-picker-section-heading-text-text");
                IWebElement SelectionOptionText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-option-text");
                IWebElement SelectionOutlineText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-outline-text");
                IWebElement ScaleLabelText = UIActions.FindElement(driver, "#stylesheet-color-picker-scale-label-text-text");

               // UIActions.SelectInCEBDropdownByText(driver, "#stylesheets-modal .ceb-dropdown-container", StyleName);
                if (NewName != null)
                {
                    StyleSheetName.Clear();
                 
                    StyleSheetName.SendKeys(NewName);
                }
                if (styleSheetParameters.BackgroundColor != null)
                {
                    PrimaryBckColor.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    PrimaryBckColor.SendKeys(OpenQA.Selenium.Keys.Delete);
                    PrimaryBckColor.SendKeys(styleSheetParameters.BackgroundColor);
                }
                if (styleSheetParameters.SecondaryBackgroundColor != null)
                {
                    SecondaryBckColor.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    SecondaryBckColor.SendKeys(OpenQA.Selenium.Keys.Delete);
                    SecondaryBckColor.SendKeys(styleSheetParameters.SecondaryBackgroundColor);
                }
                if (styleSheetParameters.TitleBarColor != null)
                {
                    TitleBarBckText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    TitleBarBckText.SendKeys(OpenQA.Selenium.Keys.Delete);
                    TitleBarBckText.SendKeys(styleSheetParameters.TitleBarColor);
                }
                if (styleSheetParameters.TitleFontColor != null)
                {

                    TitleBarText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    TitleBarText.SendKeys(OpenQA.Selenium.Keys.Delete);
                    TitleBarText.SendKeys(styleSheetParameters.TitleFontColor);
                }
                if (styleSheetParameters.SectionHeadingFontColor != null)
                {
                    SectionHeadingText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    SectionHeadingText.SendKeys(OpenQA.Selenium.Keys.Delete);
                    SectionHeadingText.SendKeys(styleSheetParameters.SectionHeadingFontColor);
                }
                if (styleSheetParameters.SelectionColor != null)
                {
                    SelectionOptionText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    SelectionOptionText.SendKeys(OpenQA.Selenium.Keys.Delete);
                    SelectionOptionText.SendKeys(styleSheetParameters.SelectionColor);
                }
                if (styleSheetParameters.SelectionOutlineColor != null)
                {
                    SelectionOutlineText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    SelectionOutlineText.SendKeys(OpenQA.Selenium.Keys.Delete);
                    SelectionOutlineText.SendKeys(styleSheetParameters.SelectionOutlineColor);
                }
                if (styleSheetParameters.ScaleLabelFontColor != null)
                {
                    ScaleLabelText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                    ScaleLabelText.SendKeys(OpenQA.Selenium.Keys.Delete);
                    ScaleLabelText.SendKeys(styleSheetParameters.ScaleLabelFontColor);
                }

                UIActions.Click(driver, "#btn-stylesheets-save");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void DeleteStyleSheet(IWebDriver driver, string StyleSheetName)
        {
            try
            {
                UIActions.Click(driver, "#defaults-edit-stylesheets");
                Thread.Sleep(3000);
                UIActions.SelectInCEBDropdownByText(driver, "#stylesheets-modal .ceb-dropdown-container", StyleSheetName);
                UIActions.Click(driver, "#btn-stylesheet-delete");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        
        public static void deletestylesheetnotpresent(IWebDriver driver, string StyleSheetName)
        {
            string xpathgenerated = null;
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            UIActions.clickwithXapth(driver, "//button[@title='Pulse Standard']");
            xpath1 = "//ul[@class='multiselect-container dropdown-menu']//a[@class='dropdown-item']/label[contains(.,'";

            xpath2 = StyleSheetName;
                xpath3 = "')] ";
            xpathgenerated = xpath1 + xpath2 + xpath3;
            UIActions.IsElementNotVisible(driver, xpathgenerated);
            UIActions.IsElementVisible(driver, "//div[@class='btn btn - secondary disabled' and contains(.,'Edit Stylesheets')]");
        }

        public static void  surveysettings(IWebDriver driver)
        {
             UIActions.clickwithXapth(driver,"//a[@id='btnFormSettings']");
            UIActions.clickwithXapth(driver, "//a[@id='branding-form-option']");


        }

        public static void Launchformfromhomepage(IWebDriver driver)
            {
              UIActions.ClickWithWait(driver, "#btnAuthorNewSurvey", 30);
              UIActions.ClickWithWait(driver, "#create-from-scratch", 30);             }
        } 
}
