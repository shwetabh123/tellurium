﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;
using System.IO;


namespace Common
{
    public class BenchmarkActions
    {
        public static void NavigateToBenchmark(IWebDriver driver)
        {
            UIActions.clickwithXapth(driver, "//div[@id='navbar-menu']/ul[@class='nav navbar-nav']//span[contains(.,'Manage')]");
            UIActions.clickwithXapth(driver, "//a[@ href='/Pulse/Manage/BenchMarks']");

        }

        public static void Downloadbenchmarktemplate(IWebDriver driver, string[] SurveyName)
        {
            UIActions.clickwithXapth(driver, "//*[@id='download-survey-ids']");
            UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#download-survey-Ids_Drodown", SurveyName);
            UIActions.clickwithXapth(driver, "//*[@id='download-surveytemplate']");

        }

        public static bool uploadbenchmark(IWebDriver driver, string FileName, string Benchmarkfile)
        {

            UIActions.Click(driver, "#upload-benchmark-data");
            Thread.Sleep(5000);
            IWebElement SelectFileButton = UIActions.GetElement(driver, "#benchmark-file");
            string applicationPath = CommonActions.GetApplicationPath();
            //UIActions.GetElementwithXpath(driver, "(//div[@class='col-sm-3 fileUploadInputCls padding-0 btn-info btn'])[1]", 45);
            SelectFileButton.SendKeys(applicationPath + @"DataSet\" + FileName);
            UIActions.clickwithXapth(driver, "//*[@id='ReaduploadBenchmark']");
            Thread.Sleep(120000);
            UIActions.clickwithXapth(driver, "//*[@id='ShowImportTrackerWidget-id']");
            IWebElement SearchBox = UIActions.GetElementswithXpath(driver, "//*[@id='import-tracker-table_filter']/label/input", 45);
            SearchBox.Clear();
            SearchBox.SendKeys(Benchmarkfile);

            bool isUploadSuccess = false;
            try
            {

                IWebElement status = UIActions.GetElementswithXpath(driver, "(//span[@data-bind='FileStatus'])[1]", 45);


                if (status.Text == "Upload process completed.")
                {

                    isUploadSuccess = true;

                }
                Thread.Sleep(2000);
                return isUploadSuccess;
            }

            catch (Exception e)
            {

                bool isUploadparticalsuccess = false;

                try
                {

                    IWebElement status2 = UIActions.GetElementswithXpath(driver, "(//a[@widget-data-event='click::DownloadErrorFile'])[1]", 45);
                    if (status2.Text == "Process completed with errors")
                    {
                        isUploadparticalsuccess = true;
                        UIActions.clickwithXapth(driver, "(//a[@widget-data-event='click::DownloadErrorFile'])[1]");
                        Thread.Sleep(2000);
                    }
                }
                catch (Exception ie)
                {
                    return isUploadparticalsuccess;
                }
                return isUploadparticalsuccess;
            }
        }

        public static void Verifybenchmarkname(IWebDriver driver, string[] BenchmarkName)
        {

            IWebElement SearchBox = UIActions.GetElementswithXpath(driver, "//*[@id='txtSearch']", 45);


            for (int i = 0; i < BenchmarkName.Length; i++)
            {

                SearchBox.Clear();

                SearchBox.SendKeys(BenchmarkName[i]);
                UIActions.clickwithXapth(driver, "//*[@id='btnSearch']");
                Thread.Sleep(2000);
                string xpath1 = null;
                string xpath2 = null; string xpath3 = null;
                string xpathgenerated = null;
                xpath1 = "//tr[@id='[object Object]']/td[1]//span[contains(.,'";
                xpath2 = BenchmarkName[i];
                xpath3 = "')] ";

                xpathgenerated = xpath1 + xpath2 + xpath3;
                UIActions.FindElementWithXpath(driver, xpathgenerated);

            }
        }

        public static void EditBenchmark(IWebDriver driver, string BenchmarkName, string Industry, string Region, string other, string name)
        {

            IWebElement SearchBox = UIActions.GetElementswithXpath(driver, "//*[@id='txtSearch']", 45);

            SearchBox.Clear();
            Thread.Sleep(1000);

            SearchBox.SendKeys(BenchmarkName);
            UIActions.clickwithXapth(driver, "//*[@id='btnSearch']");
            Thread.Sleep(2000);
            UIActions.clickwithXapth(driver, "//*[@id='edit-benchamark']");
            Thread.Sleep(2000);
            if (Industry != null)
            {
                UIActions.SelectInDropdownByText(driver, "#identifier_industry", Industry);
                Thread.Sleep(3000);
            }

            if (Region != null)
            {
                UIActions.SelectInDropdownByText(driver, "#identifier_region", Region);
                Thread.Sleep(3000);
            }

            if (name != null)
            {
                IWebElement textBox = UIActions.FindElementWithXpath(driver, "//*[@id='bmInputCls']");
                textBox.Clear();
                textBox.SendKeys(name);
                Thread.Sleep(4000);

            }

            if (other != null)
            {
                IWebElement textBox = UIActions.FindElementWithXpath(driver, "//*[@id='otherValText']");

                textBox.Clear();
                textBox.SendKeys(other);
                Thread.Sleep(4000);

            }

            UIActions.clickwithXapth(driver, "//*[@id='uploadBenchmark']");
            Thread.Sleep(5000);


        }

        public static void exportbenchmark(IWebDriver driver, string BenchmarkName, string Multiple)
        {


            {
                IWebElement SearchBox = UIActions.GetElementswithXpath(driver, "//*[@id='txtSearch']", 45);
                SearchBox.Clear();

                SearchBox.SendKeys(BenchmarkName);
                Thread.Sleep(3000);
                UIActions.clickwithXapth(driver, "//*[@id='btnSearch']");
                Thread.Sleep(3000);

                if (Multiple != null)
                {
                    UIActions.clickwithXapth(driver, "//*[@id='exportAll-benchmark-id']");

                    UIActions.clickwithXapth(driver, "//*[@id='ExportAll-benchmark-data']");

                    Thread.Sleep(3000);
                }
                else

                {
                    UIActions.clickwithXapth(driver, "(//td[contains(@class,'select-checkbox width-minimum padding-left-20 padding-right-20')])[1]");


                    UIActions.clickwithXapth(driver, "//*[@id='download-benchamarkdetail']");
                    Thread.Sleep(2000);
                }
            }
        }



        public static void removebenchmark(IWebDriver driver, string BenchmarkName)
        {

            IWebElement SearchBox = UIActions.GetElementswithXpath(driver, "//*[@id='txtSearch']", 45);
            SearchBox.Clear();

            SearchBox.SendKeys(BenchmarkName);
            UIActions.clickwithXapth(driver, "//*[@id='btnSearch']");
            Thread.Sleep(2000);

            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpath4 = null;
            string xpath5 = null;
            string xpathgenerated = null;
            xpath1 = "//tr[@id='[object Object]']/td[1]//span[contains(.,'";
            xpath2 = BenchmarkName;
            xpath3 = "')][1] ";
            xpath4 = "/..following-sibling::td[5]";
            xpath5 = "//*[@id='remove-benchamark']";
            xpathgenerated = xpath1 + xpath2 + xpath3 + xpath4;
            UIActions.clickwithXapth(driver, "xpathgenerated");
        }




        public static void Editratingitemaction(IWebDriver driver, string itemName = null, string newquestiontext = null, string scaleoption = null)

        {
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpath4 = null;
            string xpathgenerated = null;

            xpath1 = "//span[contains(.,'";
            xpath2 = itemName;
            xpath3 = "')]";
            xpath4 = "/..//following-sibling::h2//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']";

            xpathgenerated = xpath1 + xpath2 + xpath3 + xpath4;
            UIActions.clickwithXapth(driver, xpathgenerated);

            Thread.Sleep(3000);

            if (newquestiontext != null)
            {
                IWebElement Questionbox = UIActions.GetElementswithXpath(driver, "//DIV[contains(@aria-label,'Rich Text Editor, defaultQuestion')]", 15);
                Thread.Sleep(1000);
                UIActions.clickwithXapth(driver, "//DIV[contains(@aria-label,'Rich Text Editor, defaultQuestion')]");
                Thread.Sleep(1000);
                Questionbox.Clear();


                Questionbox.SendKeys(newquestiontext);
                Thread.Sleep(2000);
            }

            if (scaleoption != null)
            {
                var newscaleoption = "test";
                UIActions.clickwithXapth(driver, "(//span[@class='arrow shl-icon_arrow_small_right text-primary push-left'])[2]");
                IWebElement scaleoptionbox = UIActions.GetElementswithXpath(driver, "(//DIV[@class='cke_textarea_inline cke_textarea_responses col-md-9 cke_editable cke_editable_inline cke_contents_ltr cke_show_borders'])[1]", 45);
                Thread.Sleep(1000);
                UIActions.clickwithXapth(driver, "(//DIV[@class='cke_textarea_inline cke_textarea_responses col-md-9 cke_editable cke_editable_inline cke_contents_ltr cke_show_borders'])[1]");
                scaleoptionbox.Clear();

                scaleoptionbox.SendKeys(newscaleoption);
                Thread.Sleep(2000);
            }
            UIActions.clickwithXapth(driver, "//*[@id='btnRatingScale']");
            Thread.Sleep(2000);


        }

        public static void checkbenchmarkmapping(IWebDriver driver, string itemName, string itemname2, string benchmakrname)
        {

            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpath4 = null;
            string xpathgenerated = null;

            xpath1 = "//span[contains(.,'";
            xpath2 = itemName;
            xpath3 = "')]";
            xpath4 = "/..//following-sibling::*//span[@class='btn ceb-icon_benchmark action-question-benchmark secondary font-sz-19']";

            xpathgenerated = xpath1 + xpath2 + xpath3 + xpath4;
            UIActions.clickwithXapth(driver, xpathgenerated);
            Thread.Sleep(6000);
            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkContinue']");


            string xpath5 = null;
            string xpath6 = null;
            string xpath7 = null;
            string xpath8 = null;
            string xpath9 = null;
            string xpath10 = null;
            string xpathgenerated2 = null;

            xpath5 = "//tr[@id='[object Object]']//span[contains(.,'";
            xpath6 = itemname2;
            xpath7 = "')]";
            xpath8 = " /..//following-sibling::*//button[@title='";
            xpath9 = benchmakrname;
            xpath10 = "']";
            xpathgenerated2 = xpath5 + xpath6 + xpath7 + xpath8 + xpath9 + xpath10;
            UIActions.GetElementswithXpath(driver, xpathgenerated2, 45);
            UIActions.clickwithXapth(driver, "(//button[@class='btn btn-secondary btnNo'])[6]");
        }


        public static void Nobenchmarkmapping(IWebDriver driver, string itemName)
        {

            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpath4 = null;
            string xpathgenerated = null;

            xpath1 = "//span[contains(.,'";
            xpath2 = itemName;
            xpath3 = "')]";
            xpath4 = "/..//following-sibling::*//span[@class='btn ceb-icon_benchmark action-question-benchmark secondary font-sz-19']";

            xpathgenerated = xpath1 + xpath2 + xpath3 + xpath4;
            UIActions.clickwithXapth(driver, xpathgenerated);
            UIActions.FindelementWithXpath(driver, "(//button[@title='Please Select...'])[1]");
        }
        public static void Editnonratingitemaction(IWebDriver driver, string itemName = null, string newquestiontext = null, string scaleoption = null, string newscaleoption = null)

        {
            string xpath1 = null;
            string xpath2 = null;
            string xpath3 = null;
            string xpath4 = null;
            string xpathgenerated = null;

            xpath1 = "//span[contains(.,'";
            xpath2 = itemName;
            xpath3 = "')]";
            xpath4 = "/..//following-sibling::h2//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']";

            xpathgenerated = xpath1 + xpath2 + xpath3 + xpath4;
            UIActions.clickwithXapth(driver, xpathgenerated);

            Thread.Sleep(3000);

            if (newquestiontext != null)
            {
                IWebElement Questionbox = UIActions.GetElementswithXpath(driver, "//DIV[contains(@aria-label,'Rich Text Editor, txtQuestion')]", 15);
                Thread.Sleep(2000);
                UIActions.clickwithXapth(driver, "//DIV[contains(@aria-label,'Rich Text Editor, txtQuestion')]");
                Questionbox.Clear();

                Thread.Sleep(2000);
                Questionbox.SendKeys(newquestiontext);
                Thread.Sleep(2000);
            }
            
            if (scaleoption != null)
            {

       //  UIActions.clickwithXapth(driver, "(//span[@class='arrow shl-icon_arrow_small_right text-primary push-left'])[2]");

                IWebElement scaleoptionbox = UIActions.GetElementswithXpath(driver, "(//DIV[@class='cke_textarea_inline cke_textarea_responses cke_editable cke_editable_inline cke_contents_ltr cke_show_borders'])[2]", 45);
                Thread.Sleep(2000);
                UIActions.clickwithXapth(driver, "(//DIV[@class='cke_textarea_inline cke_textarea_responses cke_editable cke_editable_inline cke_contents_ltr cke_show_borders'])[2]");
                Thread.Sleep(2000);
                scaleoptionbox.Clear();
                Thread.Sleep(2000);
                scaleoptionbox.SendKeys(newscaleoption);
                Thread.Sleep(2000);
            }
            UIActions.clickwithXapth(driver, "//*[@id='btnMultipleChoiceSave']");
            Thread.Sleep(2000);


        }


              }
    }









