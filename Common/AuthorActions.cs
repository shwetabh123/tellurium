﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Data;

using OpenQA.Selenium.Remote;
using System.Drawing;
namespace Common
{
    public class AuthorActions
    {
        public static string DemoQuestionText { get; set; }
        public static string DemoResponseText { get; set; }
        public static DBConnectionStringDTO DBConnectionParameters;

        private static ThankYouPageDto ThankYouPage = new ThankYouPageDto();
        public static IWebElement CreateForm(IWebDriver driver, string formName, string styleSheet = null)
        {
            UIActions.ClickWithWait(driver, "#btnAuthorNewSurvey", 30);
            UIActions.ClickWithWait(driver, "#create-from-scratch", 30);
            UIActions.GetElementWithWait(driver, "#surveyName", 2000);
            IWebElement surveyNameTextBox = driver.FindElement(By.Id("surveyName"));
            surveyNameTextBox.Clear();
            surveyNameTextBox.SendKeys(formName);
            if (styleSheet != null)
            {
                UIActions.SelectInCEBDropdownByText(driver, "#select-style", styleSheet);
            }


            UIActions.Click(driver, "#btn-create-survey");
            var firstEmptyPage = UIActions.GetElementWithWait(driver, ".page-list a span", 45);
            return firstEmptyPage;
        }

        public static IWebElement CreateFormwithstylesheet(IWebDriver driver, string formName, string styleSheetname = null)
        {
            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(2000);
            UIActions.ClickWithWait(driver, "#btnAuthorNewSurvey", 30);
            UIActions.ClickWithWait(driver, "#create-from-scratch", 30);
            UIActions.GetElementWithWait(driver, "#surveyName", 2000);
            IWebElement surveyNameTextBox = driver.FindElement(By.Id("surveyName"));
            surveyNameTextBox.Clear();
            surveyNameTextBox.SendKeys(formName);
            if (styleSheetname != null)
            {
                UIActions.SelectInCEBDropdownByText(driver, "#select-style", styleSheetname);
            }


            UIActions.Click(driver, "#btn-create-survey");
            var firstEmptyPage = UIActions.GetElementWithWait(driver, ".page-list a span", 45);
            return firstEmptyPage;
        }



        public static IWebElement CopyForm(IWebDriver driver, string formName = null, string newFormName = null)
        {
            UIActions.ClickWithWait(driver, "#btnAuthorNewSurvey", 60);
            UIActions.ClickWithWait(driver, "#copy-from-survey-library", 5);
            var copyFormGrid = UIActions.GetElementWithWait(driver, "#Copy-Form-table", 30);
            if (!string.IsNullOrEmpty(formName))
            {
                IWebElement searchBox = driver.FindElement(By.Id("txtCopySearch"));
                searchBox.Clear();
                searchBox.SendKeys(formName);
                UIActions.Click(driver, "#btnCopySearch");
                Thread.Sleep(1000);
            }
            var copyButtons = UIActions.FindElements(driver, ".btnCopy");
            if (copyButtons != null && copyButtons.FirstOrDefault() != null)
                copyButtons.FirstOrDefault().Click();
            UIActions.GetElementWithWait(driver, "#surveyName", 30);
            IWebElement surveyNameTextBox = driver.FindElement(By.Id("surveyName"));
            if (newFormName != null)
            {
                surveyNameTextBox.Clear();
                surveyNameTextBox.SendKeys(newFormName);
            }

            else
                surveyNameTextBox.SendKeys(surveyNameTextBox.Text + DateTime.Now.Ticks.ToString());
            UIActions.Click(driver, "#btn-create-survey");
            var firstEmptyPage = UIActions.GetElementWithWait(driver, ".page-list a span", 45);
            Thread.Sleep(15000);
            return firstEmptyPage;
        }

        public static IWebElement SearchAndOpenForm(IWebDriver driver, string formName)
        {
            UIActions.GetElementWithWait(driver, "#Form-table", 30);
            IWebElement searchBox = driver.FindElement(By.Id("txtSearch"));
            searchBox.Clear();
            searchBox.SendKeys(formName);
            UIActions.Click(driver, "#btnSearch");
            IWebElement formLink = UIActions.GetElementWithWait(driver, "a[data-bind='FormName']", 40);
            if (formLink != null)
                UIActions.Click(driver, "#Form-table a");
            var firstEmptyPage = UIActions.GetElementWithWait(driver, ".page-list a span", 45);
            Thread.Sleep(15000);
            return firstEmptyPage;
        }

        public static void SearchAndOpenPermissions(IWebDriver driver, string formName)
        {
            UIActions.GetElementWithWait(driver, "#Form-table", 30);
            IWebElement searchBox = driver.FindElement(By.Id("txtSearch"));
            searchBox.Clear();
            searchBox.SendKeys(formName);
            UIActions.Click(driver, "#btnSearch");

            UIActions.Click(driver, "#Form-table tr button");
            UIActions.Click(driver, ".liClsPermissions ");

            Thread.Sleep(5000);
        }

        public static void SearchUserAndSelect(IWebDriver driver, string userEmail)
        {
            IWebElement userSearchBox = UIActions.GetElement(driver, "#user-access-table_filter input");
            userSearchBox.Clear();
            userSearchBox.SendKeys(userEmail);
            UIActions.Click(driver, "#user-access-table tr td.select-checkbox");

            Thread.Sleep(5000);
        }

        public static void NavigateToAuthor(IWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "ul.sidebar-menu li span");
            navbuttons.FirstOrDefault(b => b.Text == "Author").Click();
            Thread.Sleep(10000);
        }

        public static void AddRatingScaleQuestion(IWebDriver driver, string questionText, string scaleRange = null, string[] scaleHeaders = null)
        {
            UIActions.Click(driver, "#add-rating-scale");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtSingleItemQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);

            if (!string.IsNullOrEmpty(scaleRange))
            {
                UIActions.Click(driver, "#rating-Response-heading");
                Thread.Sleep(1000);
                // UIActions.SelectInCEBDropdownByText(driver, "#available-rating-scales", scaleRange);
            }

            if (scaleHeaders != null && scaleHeaders.Length > 0)
            {
                var scaleHeaderCheckBoxes = UIActions.FindElements(driver, "#divAddedRowRatingScale input");
                var scaleHeaderTextAreas = UIActions.FindElements(driver, "#divAddedRowRatingScale div.cke_textarea_responses");
                for (int i = 0; i < scaleHeaders.Length; i++)
                {
                    if (!string.IsNullOrEmpty(scaleHeaders[i]))
                    {
                        scaleHeaderCheckBoxes.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Clear();
                        scaleHeaderTextAreas.ElementAt(i).SendKeys(scaleHeaders[i]);
                    }
                }
            }
            Thread.Sleep(2000);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            ac.MoveToElement(driver.FindElement(By.CssSelector("#btnRatingScale"))).Build().Perform();
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btnRatingScale");
        }

        public static void AddMultiItemRatingScaleQuestion(IWebDriver driver, string instructionText, string[] questions, string scaleRange = null, string[] scaleHeaders = null)
        {
            UIActions.Click(driver, "#add-rating-scale");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#rating-scale-style", "multiItem");
            var instructionBox = UIActions.GetElement(driver, "#txtRatingScaleInstruction + .cke_textarea_inline");
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#addedRowsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }

            if (!string.IsNullOrEmpty(scaleRange))
            {
                UIActions.Click(driver, "#rating-Response-heading");
                Thread.Sleep(1000);
                //UIActions.SelectInCEBDropdownByText(driver, "#available-rating-scales", scaleRange);
            }


            if (scaleHeaders != null && scaleHeaders.Length > 0)
            {
                var scaleHeaderCheckBoxes = UIActions.FindElements(driver, "#divAddedRowRatingScale input");
                var scaleHeaderTextAreas = UIActions.FindElements(driver, "#divAddedRowRatingScale div.cke_textarea_responses");
                for (int i = 0; i < scaleHeaders.Length; i++)
                {
                    if (!string.IsNullOrEmpty(scaleHeaders[i]))
                    {
                        Thread.Sleep(1000);
                        scaleHeaderCheckBoxes.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Clear();
                        scaleHeaderTextAreas.ElementAt(i).SendKeys(scaleHeaders[i]);
                    }
                }
            }
            UIActions.Click(driver, "#btnRatingScale");
        }

        public static void AddMultipleChoiceQuestion(IWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            UIActions.Click(driver, "#btnMultipleChoiceSave");
        }

        public static void AddMultipleChoiceMultiItemQuestion(IWebDriver driver, string instructionText, string[] questions, string[] responses)
        {
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#mcScaleType", "multiItem");
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtIncludeMultiItemInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#mcMultiItemsQuestions .addmoreButton button");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#mcMultiItemsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            UIActions.Click(driver, "#btnMultipleChoiceSave");
        }

        public static void AddDropDownQuestion(IWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            UIActions.Click(driver, "#btnDropDownSave");
        }

        public static void AddCommentQuestion(IWebDriver driver, string questionText)
        {
            var message = UIActions.GetElementWithWait(driver, "#add-Text", 60);
            if (message.Text.Trim() == "Comment")
            {
                UIActions.Click(driver, "#add-Text");
                Thread.Sleep(5000);
                IWebElement questionTextEditor = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
                questionTextEditor.Clear();
                questionTextEditor.SendKeys(questionText);
                UIActions.Click(driver, "#btnFixedFormatSave");
            }

        }

        public static void AddPage(IWebDriver driver)
        {
            var message = UIActions.GetElementWithWait(driver, "#page-break", 30);
            if (message.Text.Trim() == "Page")
            {
                UIActions.Click(driver, "#page-break");
                Thread.Sleep(5000);
            }
        }

        public static void OpenEndPage(IWebDriver driver)
        {
            var message = UIActions.GetElementWithWait(driver, "#end-page", 30);
            if (message.Text.Trim() == "End Page")
            {
                UIActions.Click(driver, "#end-page");
                Thread.Sleep(5000);
            }
        }

        public static void AddSection(IWebDriver driver)
        {

            var message = UIActions.GetElementWithWait(driver, "#section-break", 30);
            if (message.Text.Trim() == "Section")
            {
                UIActions.Click(driver, "#section-break");
                Thread.Sleep(5000);
            }
        }

        public static void AddInformationText(IWebDriver driver, string info)
        {

            UIActions.clickwithID(driver, "page-content-text");
            Thread.Sleep(5000);
            var infoquestionbox = UIActions.GetElement(driver, "#PageContentTextIncludeContext + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            UIActions.clickwithID(driver, "btnTextContentSave");

        }

        public static void AddLongCommentQuestion(IWebDriver driver, string info)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }

        public static void AddShortCommentQuestion(IWebDriver driver, string info)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }

        public static void AddFixedFormatQuestion(IWebDriver driver, string info, string type)
        {
            UIActions.clickwithID(driver, "add-fixed-format");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#fixed-format-type", type);
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }

        public static void AddFixedFormatDateQuestionAsDemo(IWebDriver driver, string info, string[] CategoryName)
        {
            UIActions.clickwithID(driver, "add-fixed-format");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#fixed-format-type", "format_date");
            UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-category-Fixed", CategoryName);
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            UIActions.clickwithID(driver, "btnFixedFormatSave");

        }

        public static void DragAndDropLogic(IWebDriver driver, int tileno)
        {
            IWebElement from = UIActions.Drag(driver, "#add-Logic");

            IWebElement to = UIActions.Drop(driver, "//*[@class='panel panel-default question-list ui-droppable'][" + tileno + "]");
            Actions act = new Actions(driver);

            act.ClickAndHold(from);
            act.Perform();
            Thread.Sleep(10000);
            act.Release(to);
            act.Perform();
        }
        public static void DragAndDropPage(IWebDriver driver)
        {
            IWebElement from = UIActions.Drag(driver, "#page-break");
            IWebElement to = driver.FindElement(By.ClassName("section-list"));
            Actions act = new Actions(driver);

            act.ClickAndHold(from);
            act.Perform();
            Thread.Sleep(10000);
            act.Release(to);
            act.Perform();
        }

        public static void DragAndDropSection(IWebDriver driver)
        {

            IWebElement from = UIActions.Drag(driver, "#section-break");
            IWebElement to = driver.FindElement(By.ClassName("question-list"));
            Actions act = new Actions(driver);
            //act.DragAndDrop(from, to).Build().Perform();
            act.ClickAndHold(from);
            act.Perform();
            Thread.Sleep(10000);
            act.Release(to);
            act.Perform();
        }

        public static void DragAndDrop(IWebDriver driver, string dragto, string dropto)
        {
            IWebElement from = UIActions.Drag(driver, dragto);
            IWebElement to = driver.FindElement(By.ClassName(dropto));
            Actions act = new Actions(driver);
            act.ClickAndHold(from);
            act.Perform();
            Thread.Sleep(10000);
            act.Release(to);
            act.Perform();
        }

        public static void DragAndDropwithCSS(IWebDriver driver, string dragto, string dropto)
        {
            IWebElement from = UIActions.Drag(driver, dragto);
            IWebElement to = driver.FindElement(By.CssSelector(dropto));
            Actions act = new Actions(driver);
            act.ClickAndHold(from);
            act.Perform();
            Thread.Sleep(10000);
            act.Release(to);
            act.Perform();
        }

        public static void AddBenchmarkMappingSingleRatingScale(IWebDriver driver, string questionText, string benchmarkQuestion)
        {
            var elements = UIActions.FindElements(driver, ".question-list .ceb-icon_ratingscalestar_1 + span");
            var questionElement = elements.FirstOrDefault(e => e.GetAttribute("title") == questionText);
            var parentDiv = questionElement.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            Actions action = new Actions(driver);
            action.MoveToElement(parentDiv).Perform();
            var benchmarkIcon = parentDiv.FindElement(By.CssSelector("h2 span.action-question-benchmark"));
            benchmarkIcon.Click();
            Thread.Sleep(20000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark-question", benchmarkQuestion);
            UIActions.Click(driver, "#btnBenchmarkMap");
        }

        public static void AddBenchmarkMappingSingleMultiChoice(IWebDriver driver, string questionText, string benchmarkQuestion, List<string> benchmarkResponses = null)
        {
            var elements = UIActions.FindElements(driver, ".question-list .ceb-icon_multiplechoice_1 + span");
            var questionElement = elements.FirstOrDefault(e => e.GetAttribute("title") == questionText);
            var parentDiv = questionElement.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            Actions action = new Actions(driver);
            action.MoveToElement(parentDiv).Perform();
            var benchmarkIcon = parentDiv.FindElement(By.CssSelector("h2 span.action-question-benchmark"));
            benchmarkIcon.Click();
            Thread.Sleep(20000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark-question", benchmarkQuestion);
            UIActions.Click(driver, "#btnBenchmarkContinue");
            Thread.Sleep(20000);
            int index = 0;
            foreach (var response in benchmarkResponses)
            {
                UIActions.SelectInCEBDropdownByText(driver, ".benchmarkResponseSelect", response, index);
                index++;
            }
            Thread.Sleep(6000);
            UIActions.Click(driver, "#btnBenchmarkMap");
        }

        public static void AddDemoRestrictionLogic(IWebDriver driver, string caseAction, string joinType, List<LogicCaseDto> logicCases)
        {
            UIActions.SelectInCEBDropdownByText(driver, "#demodatarestriction-select-caseAction", caseAction);
            Thread.Sleep(2000);


            for (int i = 1; i < logicCases.Count; i++)
            {
                UIActions.Click(driver, "#disable-addCase-DemoRest");
                Thread.Sleep(200);
            }

            for (int i = 0; i < logicCases.Count; i++)
            {
                UIActions.SelectInCEBDropdownByValue(driver, ".DemoDataRestriction-ObjectType select", logicCases[i].SourceType, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByText(driver, ".questionDDL select", logicCases[i].SourceItem, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByText(driver, ".conditionalDDL select", logicCases[i].SourceCondition, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByText(driver, ".logicDDL select", logicCases[i].SourceLogic, i);
                if (logicCases[i].SourceLogic == "Any of")
                    UIActions.SelectInMultiSelectCEBDropdownByText(driver, ".responseDDL select", logicCases[i].SourceResponses, i);
                else if (logicCases[i].SourceLogic == "Exactly")
                    UIActions.SelectInCEBDropdownByText(driver, ".responseDDL select", logicCases[i].SourceResponses[0], i);
            }

            Thread.Sleep(3000);
            UIActions.Click(driver, "#demodatarestriction-save-LogicBuilder");
            Thread.Sleep(5000);

        }

        public static void AddBenchmarkMappingDropdown(IWebDriver driver, string questionText, string benchmarkQuestion, List<string> benchmarkResponses = null)
        {
            var elements = UIActions.FindElements(driver, ".question-list .ceb-icon_dropdown + span");
            var questionElement = elements.FirstOrDefault(e => e.GetAttribute("title") == questionText);
            var parentDiv = questionElement.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            Actions action = new Actions(driver);
            action.MoveToElement(parentDiv).Perform();
            var benchmarkIcon = parentDiv.FindElement(By.CssSelector("h2 span.action-question-benchmark"));
            benchmarkIcon.Click();
            Thread.Sleep(20000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark-question", benchmarkQuestion);
            UIActions.Click(driver, "#btnBenchmarkContinue");
            Thread.Sleep(20000);
            int index = 0;
            foreach (var response in benchmarkResponses)
            {
                UIActions.SelectInCEBDropdownByText(driver, ".benchmarkResponseSelect", response, index);
                index++;
            }
            UIActions.Click(driver, "#btnBenchmarkMap");
        }

        public static void AddBenchmarkMappingMultiRatingScale(IWebDriver driver, string questionInstruction, string benchmarkQuestionInstruction, List<string> benchmarkItems = null)
        {
            var elements = UIActions.FindElements(driver, ".question-list .ceb-icon_ratingscalestar_1 + span");
            var questionElement = elements.FirstOrDefault(e => e.GetAttribute("title") == questionInstruction);
            var parentDiv = questionElement.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            Actions action = new Actions(driver);
            action.MoveToElement(parentDiv).Perform();
            var benchmarkIcon = parentDiv.FindElement(By.CssSelector("h2 span.action-question-benchmark"));
            benchmarkIcon.Click();
            Thread.Sleep(20000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark-question", benchmarkQuestionInstruction);
            UIActions.Click(driver, "#btnBenchmarkContinue");
            Thread.Sleep(20000);
            int index = 0;
            foreach (var item in benchmarkItems)
            {
                UIActions.SelectInCEBDropdownByText(driver, ".benchmarkItemSelect", item, index);
                index++;
            }
            UIActions.Click(driver, "#btnBenchmarkMap");
        }

        public static void AddBenchmarkMappingMultiChoiceMultiItem(IWebDriver driver, string questionInstruction, string benchmarkQuestionInstruction, List<string> benchmarkItems = null, List<string> benchmarkResponses = null)
        {
            var elements = UIActions.FindElements(driver, ".question-list .ceb-icon_multiplechoice_1 + span");
            var questionElement = elements.FirstOrDefault(e => e.GetAttribute("title") == questionInstruction);
            var parentDiv = questionElement.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            Actions action = new Actions(driver);
            action.MoveToElement(parentDiv).Perform();
            var benchmarkIcon = parentDiv.FindElement(By.CssSelector("h2 span.action-question-benchmark"));
            benchmarkIcon.Click();
            Thread.Sleep(20000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-benchmark-question", benchmarkQuestionInstruction);
            UIActions.Click(driver, "#btnBenchmarkContinue");
            Thread.Sleep(20000);
            int index = 0;
            foreach (var item in benchmarkItems)
            {
                UIActions.SelectInCEBDropdownByText(driver, ".benchmarkItemSelect", item, index);
                index++;
            }
            index = 0;
            foreach (var response in benchmarkResponses)
            {
                UIActions.SelectInCEBDropdownByText(driver, ".benchmarkResponseSelect", response, index);
                index++;
            }
            UIActions.Click(driver, "#btnBenchmarkMap");
        }

        public static void OpenBenchmarkMappingGridModal(IWebDriver driver)
        {
            UIActions.Click(driver, "#btnBenchmarkMapping");
            Thread.Sleep(1000);
        }

        public static void ClickEditBenchmarkMappingOnGrid(IWebDriver driver, string questionText)
        {
            var searchElement = UIActions.GetElement(driver, "input[type='search']");
            searchElement.Clear();
            searchElement.SendKeys(questionText);
            var tableRows = UIActions.FindElements(driver, "#benchmark-map-grid tr td");
            if (tableRows.Any())
            {
                UIActions.Click(driver, "#benchmark-map-grid tr td span.ceb-icon_mtmhome_edit");
                Thread.Sleep(20000);
            }
        }

        public static void ClickDeleteBenchmarkMappingOnGrid(IWebDriver driver, string questionText)
        {
            var searchElement = UIActions.GetElement(driver, "input[type='search']");
            searchElement.Clear();
            searchElement.SendKeys(questionText);
            var tableRows = UIActions.FindElements(driver, "#benchmark-map-grid tr td");
            if (tableRows.Any())
            {
                UIActions.Click(driver, "#benchmark-map-grid tr td span.ceb-icon_delete1_knockout");
                Thread.Sleep(2000);
            }
        }

        public static void AddLogic(IWebDriver driver, string caseAction, string targetType, string[] targets, string joinType, LogicCaseDto[] logicCases)
        {
            UIActions.SelectInCEBDropdownByText(driver, "#select-caseAction", caseAction);
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#select-caseTargetType", targetType);
            Thread.Sleep(5000);
            UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-Destination", targets);

            Thread.Sleep(2000);


            UIActions.SelectInCEBDropdownByValue(driver, "#select-caseJoinType", joinType);

            Thread.Sleep(2000);

            for (int i = 1; i < logicCases.Length; i++)
            {
                UIActions.Click(driver, "#AddCase");
                Thread.Sleep(2000);
            }
            for (int i = 0; i < logicCases.Length; i++)
            {
                UIActions.SelectInCEBDropdownByValue(driver, "#select-ObjectType", logicCases[i].SourceType, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByText(driver, "#select-sourceQuestion", logicCases[i].SourceItem, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByValue(driver, "#select-condition", logicCases[i].SourceCondition, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByValue(driver, "#select-logic", logicCases[i].SourceLogic, i);
                if (logicCases[i].SourceLogic == "Any of")
                    UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-Response", logicCases[i].SourceResponses, i);
                else if (logicCases[i].SourceLogic == "Exactly")
                    UIActions.SelectInCEBDropdownByText(driver, "#select-Response", logicCases[i].SourceResponses[0], i);
            }

            Thread.Sleep(1000);
            UIActions.Click(driver, "#save-LogicBuilder");
            Thread.Sleep(2000);
            var warningModal = UIActions.GetElement(driver, "#progress-bar-warning-logic");
            if (warningModal.Displayed)
                UIActions.Click(driver, "#btnYes-HideProgressBar");
        }

        public static void AddResponseLogic(IWebDriver driver, string caseAction, string targetType, string targets, string[] response, string joinType, LogicCaseDto[] logicCases)
        {
            UIActions.SelectInCEBDropdownByText(driver, "#select-caseAction", caseAction);
            Thread.Sleep(5000);

            UIActions.SelectInCEBDropdownByValue(driver, "#select-caseTargetType", targetType);

            Thread.Sleep(4000);

            UIActions.SelectInCEBDropdownByText(driver, "#select-Destination", targets);

            Thread.Sleep(2000);

            UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-Destination-Response", response);

            Thread.Sleep(2000);


            UIActions.SelectInCEBDropdownByValue(driver, "#select-caseJoinType", joinType);

            Thread.Sleep(2000);

            for (int i = 1; i < logicCases.Length; i++)
            {
                UIActions.Click(driver, "#AddCase");
                Thread.Sleep(2000);
            }
            for (int i = 0; i < logicCases.Length; i++)
            {
                UIActions.SelectInCEBDropdownByValue(driver, "#select-ObjectType", logicCases[i].SourceType, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByText(driver, "#select-sourceQuestion", logicCases[i].SourceItem, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByValue(driver, "#select-condition", logicCases[i].SourceCondition, i);

                Thread.Sleep(2000);

                UIActions.SelectInCEBDropdownByValue(driver, "#select-logic", logicCases[i].SourceLogic, i);
                if (logicCases[i].SourceLogic == "Any of")
                    UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-Response", logicCases[i].SourceResponses, i);
                else if (logicCases[i].SourceLogic == "Exactly")
                    UIActions.SelectInCEBDropdownByText(driver, "#select-Response", logicCases[i].SourceResponses[0], i);
            }

            Thread.Sleep(1000);
            UIActions.Click(driver, "#save-LogicBuilder");
            Thread.Sleep(2000);
            var warningModal = UIActions.GetElement(driver, "#progress-bar-warning-logic");
            if (warningModal.Displayed)
                UIActions.Click(driver, "#btnYes-HideProgressBar");
        }

        public static void OpenSettingsModalTab(IWebDriver driver, string tabName = null)
        {
            var settingButton = UIActions.FindElements(driver, "#btnFormSettings");
            settingButton.FirstOrDefault(x => x.Text == "Settings").Click();
            if (tabName != null)
            {
                Thread.Sleep(3000);
                var currentTab = UIActions.FindElementWithXpath(driver, "//div[@id='form-options-modal']//ul[@class='nav nav-pills']//li[a='" + tabName + "']");
                currentTab.Click();
            }
        }

        public static string EnablePageName(IWebDriver driver, string surveyName = null)
        {
            string newSurveyName = "Stage Page Name" + DateTime.Now.ToString();
            CopyForm(driver, surveyName, newSurveyName);

            driver.FindElement(By.XPath("//div[@class='margin-left-10 margin-top-10']/h2")).GetAttribute("title");
            //newSurveyName = driver.FindElement(By.XPath("//div[@class='margin-left-10 margin-top-10']/h2")).GetAttribute("title");
            OpenSettingsModalTab(driver, "Options");
            IWebElement pageNameSwitch = driver.FindElement(By.ClassName("bootstrap-switch-container"));
            Actions ac = new Actions(driver);
            ac.MoveToElement(pageNameSwitch).Build().Perform();
            try
            {
                string className = driver.FindElement(By.XPath("//*[@id='settings']/div[1]/div/div/div[15]/div/label/div/div/label"))
                    .GetAttribute("class");

                if (className == "bootstrap-switch-label switchOff")
                {
                    driver.FindElement(By.XPath("//*[@id='settings']/div[1]/div/div/div[15]/div/label/div/div/label")).Click();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            finally
            {
                var saveBtn = driver.FindElement(By.Id("btnFormSettingsSave"));
                ac.MoveToElement(saveBtn).Build().Perform();
                saveBtn.Click();
            }

            return newSurveyName;
        }

        public static string SetPageName(IWebDriver driver, string pageName = null)
        {
            Actions ac2 = new Actions(driver);
            ac2.Click(driver.FindElement(By.XPath("//*[@id='col-main']/div/div[2]/div/div[3]/div/div[1]/div[1]/a/div/div/div"))).Build().Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(2));
            wait.Until(ExpectedConditions.ElementToBeClickable(
                driver.FindElement(By.XPath("//*[@class='shl-icon_edit action-page-edit']"))));
            IWebElement editBtn = driver.FindElement(By.XPath("//*[@class='shl-icon_edit action-page-edit']"));
            editBtn.GetAttribute("title");
            if (editBtn.GetAttribute("title") == "Edit")
            {
                editBtn.Click();
                IWebElement pageNameIp = driver.FindElement(By.XPath("//input[@data-bind='Title'][1]"));
                pageNameIp.Clear();
                pageNameIp.SendKeys(pageName);
                IWebElement saveBtn =
                    driver.FindElement(By.XPath("//span[@class='ceb-icon_tick action-page-edit-confirm']"));
                saveBtn.Click();
                IWebElement re = driver.FindElement(By.XPath("//span[@data-bind ='Title'][1]"));
                string presentPageName = re.Text;
                if (re.Text.Equals(pageName))
                {
                    Console.WriteLine("Pass");
                }
            }
            return pageName;
        }


        public static string DisablePageName(IWebDriver driver, string surveyName = null)
        {
            string clsName = null;
            Actions ac = new Actions(driver);
            ac.MoveToElement(driver.FindElement(By.XPath("//*['@id=navbar-menu']/ul/li[5]/a/span"))).Build().Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(2));
            wait.Until(ExpectedConditions.ElementToBeClickable(
                driver.FindElement(By.XPath("//*['@id=navbar-menu']/ul/li[5]/a/span"))));
            NavigateToAuthor(driver);
            Thread.Sleep(5000);
            SearchAndOpenForm(driver, surveyName);
            OpenSettingsModalTab(driver, "Options");
            var pageNameSwitch = driver.FindElement(By.ClassName("bootstrap-switch-container"));
            Actions ac1 = new Actions(driver);
            ac1.MoveToElement(pageNameSwitch).Build().Perform();

            try
            {
                string className = driver.FindElement(By.XPath("//*[@id='settings']/div[1]/div/div/div[15]/div/label/div/div"))
                    .GetAttribute("class");

                if (className == "bootstrap-switch-label switchOn")
                {
                    driver.FindElement(By.XPath("//*[@id='settings']/div[1]/div/div/div[15]/div/label/div/div/label")).Click();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            finally
            {
                var saveBtn = driver.FindElement(By.Id("btnFormSettingsSave"));
                ac1.MoveToElement(saveBtn).Build().Perform();
                saveBtn.Click();
            }
            return clsName;
        }
        public static bool AddInformationTextWithLimitCheckCase(IWebDriver driver, string surveyName, int len)
        {
            AuthorActions.SearchAndOpenForm(driver, surveyName);
            //AuthorActions.CreateForm(driver, "InformationText" + DateTime.Now);
            IWebElement infoquestionbox;
            IWebElement editorPreview;
            int i = 0;
            int remainingLimit;
            string txt;
            bool testStatus = false;
            List<string> iText = new List<string>();
            //string txt = CommonActions.RandomString(50);

            Thread.Sleep(5000);
            UIActions.clickwithID(driver, "page-content-text");
            Thread.Sleep(5000);
            infoquestionbox = UIActions.GetElement(driver, "#PageContentTextIncludeContext + .cke_textarea_inline");
            infoquestionbox.Clear();
            //while (i < repeatTimes)
            //{
            switch (len)
            {
                case 100:
                    testStatus = CreateInfoTextLessThanLimit(driver, surveyName, out infoquestionbox, out editorPreview, out remainingLimit, out txt, out testStatus, len);
                    break;

                case 3000:
                    testStatus = CreateInfoTextAtLimit(driver, surveyName, out infoquestionbox, out editorPreview, out remainingLimit, out txt, out testStatus, len);
                    break;
                case 3001:
                    testStatus = CreateInfoTextExceedingLimit(driver, surveyName, out infoquestionbox, out remainingLimit, out txt, out testStatus, len);
                    break;
            }
            //}

            return testStatus;
        }

        private static bool CreateInfoTextExceedingLimit(IWebDriver driver, string surveyName, out IWebElement infoquestionbox, out int remainingLimit, out string txt, out bool testStatus, int len)
        {
            Thread.Sleep(6000);
            AuthorActions.NavigateToAuthor(driver);
            AuthorActions.SearchAndOpenForm(driver, surveyName);
            Thread.Sleep(5000);
            //AddInformationText(driver, txt);
            UIActions.clickwithID(driver, "page-content-text");
            Thread.Sleep(5000);
            infoquestionbox = UIActions.GetElement(driver, "#PageContentTextIncludeContext + .cke_textarea_inline");
            infoquestionbox.Clear();
            using (StreamReader sr1 = new StreamReader("C:\\Automation_C#\\Tellurium\\Inputdata\\InfoTextMore.txt"))
            {
                txt = sr1.ReadToEnd();
                Clipboard.SetText(txt);
                infoquestionbox.SendKeys(OpenQA.Selenium.Keys.Control + "v");
                //Thread.Sleep(2000);
            }
            //string alertMsg = driver.FindElement(By.Id("action-status")).Text;
            //bool lenStatus = (alertMsg == "Information Text has exceeded the maximum length.") ? true : false;
            string alertMsg = UIActions.FindElementWithXpath(driver, "//div[@class='alert alert-danger']//span[@id='action-status']").Text;
            bool lenStatus = (alertMsg == "Information Text has exceeded the maximum length.") ? true : false;
            remainingLimit = Convert.ToInt32(UIActions.GetValueByElement(driver, "#spnInfoTextCharsRemaining"));
            bool cntVal1 = (remainingLimit == 0) ? true : false;
            driver.FindElement(By.XPath("//*[@id='pagecontent-text-preview-pane']/div[3]/div/div/div")).Click();
            testStatus = (cntVal1.Equals(lenStatus)) ? true : false;
            Console.WriteLine(testStatus + "Info Text:" + len + lenStatus + alertMsg);
            return testStatus;
        }

        private static bool CreateInfoTextAtLimit(IWebDriver driver, string surveyName, out IWebElement infoquestionbox, out IWebElement editorPreview, out int remainingLimit, out string txt, out bool testStatus, int len)
        {
            Thread.Sleep(6000);
            AuthorActions.NavigateToAuthor(driver);
            AuthorActions.SearchAndOpenForm(driver, surveyName);
            Thread.Sleep(5000);
            UIActions.clickwithID(driver, "page-content-text");
            Thread.Sleep(5000);
            infoquestionbox = UIActions.GetElement(driver, "#PageContentTextIncludeContext + .cke_textarea_inline");
            infoquestionbox.Clear();
            using (StreamReader sr1 = new StreamReader("C:\\Automation_C#\\Tellurium\\Inputdata\\InfoTextEqual.txt"))
            {
                txt = sr1.ReadToEnd();
                Clipboard.SetText(txt);
                infoquestionbox.SendKeys(OpenQA.Selenium.Keys.Control + "v");
                Thread.Sleep(2000);
            }

            remainingLimit = Convert.ToInt32(UIActions.GetValueByElement(driver, "#spnInfoTextCharsRemaining"));
            bool cntVal2 = (remainingLimit == 0) ? true : false;
            testStatus = cntVal2;
            driver.FindElement(By.XPath("//*[@id='pagecontent-text-preview-pane']/div[3]/div/div/div")).Click();
            editorPreview = UIActions.FindElementWithXpath(driver,
                "//*[@class='test-content survey-content col-xs-12']//div[@data-bind='prompt']");
            bool qVal2 = editorPreview.Text == txt ? true : false;
            Thread.Sleep(2000);
            Actions ac1 = new Actions(driver);
            ac1.MoveToElement(driver.FindElement(By.Id("btnTextContentSave"))).Build().Perform();
            UIActions.clickwithID(driver, "btnTextContentSave");
            //testStatus = (cntVal2.Equals(qVal2)) ? true : false;
            Console.WriteLine(testStatus + "Info Text:" + len + editorPreview.Text);
            return testStatus;
        }

        private static bool CreateInfoTextLessThanLimit(IWebDriver driver, string surveyName, out IWebElement infoquestionbox, out IWebElement editorPreview, out int remainingLimit, out string txt, out bool testStatus, int len)
        {
            AuthorActions.SearchAndOpenForm(driver, surveyName);

            Thread.Sleep(5000);
            UIActions.clickwithID(driver, "page-content-text");
            Thread.Sleep(5000);
            infoquestionbox = UIActions.GetElement(driver, "#PageContentTextIncludeContext + .cke_textarea_inline");
            infoquestionbox.Clear();
            using (StreamReader sr = new StreamReader("C:\\Automation_C#\\Tellurium\\Inputdata\\InfoTextLess.txt"))
            {
                txt = sr.ReadToEnd();
                Clipboard.SetText(txt);
                infoquestionbox.SendKeys(OpenQA.Selenium.Keys.Control + "v");

                // infoquestionbox.SendKeys(Clipboard.GetText());

                Thread.Sleep(2000);
                remainingLimit = Convert.ToInt32(UIActions.GetValueByElement(driver, "#spnInfoTextCharsRemaining"));
                bool cntVal = (remainingLimit > 0) ? true : false;
                driver.FindElement(By.XPath("//*[@id='pagecontent-text-preview-pane']/div[3]/div/div/div")).Click();
                editorPreview = UIActions.FindElementWithXpath(driver,
                    "//*[@class='test-content survey-content col-xs-12']//div[@data-bind='prompt']");
                bool qVal1 = editorPreview.Text == txt ? true : false;
                Thread.Sleep(2000);
                Actions ac = new Actions(driver);
                ac.MoveToElement(driver.FindElement(By.Id("btnTextContentSave"))).Build().Perform();
                UIActions.clickwithID(driver, "btnTextContentSave");
                testStatus = (cntVal.Equals(qVal1)) ? true : false;
            }

            Console.WriteLine(testStatus + "Info Text:" + len);
            return testStatus;
        }

        public static void NavToAccountSettings(IWebDriver driver)
        {
            Actions ac = new Actions(driver);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            UIActions.FindElementWithXpath(driver, "//span[@class='user-info']").Click();
            UIActions.FindElementWithXpath(driver, "//a[@href='/Pulse/AccountSettings']").Click();
        }

        public static void SelectAccountDefaults(IWebDriver driver)
        {
            var navButton = UIActions.FindElements(driver, ".container-fluid div div div div button");
            navButton.FirstOrDefault(b => b.Text == "Account Defaults").Click();
        }

        public static void NavToIntegration(IWebDriver driver)
        {
            var navButton = UIActions.FindElements(driver, ".container-fluid div div div div button");
            navButton.FirstOrDefault(b => b.Text == "Integration").Click();
        }

        public static void NavToLibAccess(IWebDriver driver)
        {
            var navButton = UIActions.FindElements(driver, "#library-accordion div a div");
            navButton.FirstOrDefault(b => (b.Text).Trim() == "Library Access").Click();
        }

        public static void NavToBenchMark(IWebDriver driver)
        {
            var navButton = UIActions.FindElements(driver, "#library-section div div button");
            navButton.FirstOrDefault(b => b.Text == "Benchmarks").Click();
        }

        public static bool EnableThankUInAccSettings(IWebDriver driver)
        {
            Actions ac = new Actions(driver);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            UIActions.FindElementWithXpath(driver, "//span[@class='user-info']").Click();
            UIActions.FindElementWithXpath(driver, "//a[@href='/Pulse/AccountSettings']").Click();
            UIActions.clickwithID(driver, "menu-account-defaults");
            wait.Until(ExpectedConditions.ElementExists(By.XPath("//input[@name='allow-custom-thankyou'][@value='true']")));
            ac.MoveToElement(driver.FindElement(By.XPath("//input[@name='allow-custom-thankyou'][@value='true']"))).Build().Perform();
            UIActions.FindElementWithXpath(driver, "//input[@name='allow-custom-thankyou'][@value='true']").Click();
            ac.MoveToElement(driver.FindElement(By.Id("defaults-save"))).Build().Perform();
            driver.FindElement(By.Id("defaults-save")).Click();
            string alertMsg = UIActions.GetElement(driver, "#action-status").Text;
            bool isSelected = UIActions.FindElementWithXpath(driver, "//input[@name='allow-custom-thankyou'][@value='true']").Selected;
            return isSelected;
        }


        public static void EditPublishedForm(IWebDriver driver)
        {
            try
            {
                bool trueorfalse = UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']");
                if ((trueorfalse))
                {
                    Thread.Sleep(3000);
                    driver.FindElement(By.Id("btnFormEdit")).Click();
                    Thread.Sleep(3000);
                    driver.FindElement(By.Id("edit-form-button")).Click();
                    Thread.Sleep(15000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void PublishEditedForm(IWebDriver driver)
        {
            IWebElement Publish = driver.FindElement(By.XPath("//*[@class='btn btn-primary pub  no-margin-top ']"));
            Publish.Click();
            Thread.Sleep(2000);
            IWebElement ContinuePublish = driver.FindElement(By.Id("btnConfirmPublishClicked"));
            ContinuePublish.Click();
            Thread.Sleep(7000);
            IWebElement ClosePublishSurvey = driver.FindElement(By.XPath("//*[@class='btn btn-secondary btn-lg']"));
            ClosePublishSurvey.Click();
            Thread.Sleep(5000);
        }
        public static ThankYouPageDto SetThankUTypeInAtr(IWebDriver driver, string editType, string surveyName)
        {

            //ThankYouPageDto ThankYouPage = new ThankYouPageDto();
            try
            {
                Actions ac = new Actions(driver);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
                string applicationPath = CommonActions.GetApplicationPath();
                string _filePath = applicationPath + @"\DataSet\CustomText.txt";
                Thread.Sleep(5000);
                IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='Title']"));
                ac.MoveToElement(titleList.FirstOrDefault(d => d.Text == "End Page: Survey Completed")).Build().Perform();
                driver.FindElement(By.XPath("//span[@class='btn ceb-icon_home_class_edit action-Thank-you-edit secondary font-sz-23']")).Click();
                Thread.Sleep(6000);
                IReadOnlyCollection<IWebElement> thankyouEditType = driver.FindElements(By.XPath("//div[@id='thank-you-page-context']//div[@class='row']//label//span"));
                thankyouEditType.FirstOrDefault(d => d.Text == editType).Click();
                switch (editType)
                {
                    case "Default Text":
                        break;
                    case "Customize Text":
                        ThankYouPage = EditAsCustomText(driver, _filePath, surveyName);
                        break;
                    case "Redirect to a URL":
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return ThankYouPage;
        }

        public static ThankYouPageDto EditAsCustomText(IWebDriver driver, object filePath, string surveyName)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));

            string applicationPath = CommonActions.GetApplicationPath();
            string _filePath = applicationPath + @"DataSet\CustomText.txt";

            Actions ac = new Actions(driver);
            string copiedContent = ReadFromTextFile(_filePath);
            Clipboard.SetText(copiedContent);
            IWebElement ele = driver.FindElement(By.XPath("//div[@title='Rich Text Editor, ThankYouPageTextContent']"));
            ac.MoveToElement(ele).Build().Perform();
            ac.Click();
            ele.Clear();
            ele.SendKeys(OpenQA.Selenium.Keys.Control + "v");
            wait.Until<IWebElement>(d =>
            {
                return d.FindElement(By.Id("btnThankYouPageSave"));
            });
            Actions ac1 = new Actions(driver);
            ac1.MoveToElement(driver.FindElement(By.Id("btnThankYouPageSave"))).Build().Perform();
            UIActions.clickwithID(driver, "btnThankYouPageSave");
            IWebElement statusMsg = UIActions.GetElement(driver, "#action-status");
            Thread.Sleep(4000);
            // ThankYouPage._previewLink = capturePreviewLink(driver);
            ThankYouPage._successMsg = "Success";
            ThankYouPage._formTitle = surveyName;
            ThankYouPage._surveyName = surveyName;



            return ThankYouPage;
        }



        public static string ReadFromTextFile(string _filePath)
        {
            string contentText;
            using (StreamReader sr3 = new StreamReader(_filePath))

            {
                contentText = sr3.ReadToEnd();
            }
            return contentText;
        }

        public static string capturePreviewLink(IWebDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5000));
            ClkPreviewBtn(driver);
            string existingWindowHandle = switchWindow(driver);

            wait.Until<bool>(d =>
            {
                return d.Url.ToLowerInvariant().Contains("https://stg2-surveys.cebglobal.com/player/link/");
                //return d.Url.ToLowerInvariant().Contains("https://stg-surveys.cebglobal.com/player/link/");
                //return d.Url.ToLowerInvariant().Contains("https://qa-surveys.cebglobal.com/player/link/");
            });
            driver.Manage().Window.Maximize();
            Console.WriteLine(driver.Url);
            return driver.Url;
        }

        public static string switchWindow(IWebDriver driver)
        {

            int oldwindowsize = 0;
            int newwindowsize = 0;

            //get the current window handles /
            string popupHandle = string.Empty;
            ReadOnlyCollection<string> all_windowHandles = driver.WindowHandles;

            //get the current window handles count
            oldwindowsize = all_windowHandles.Count;

            //ClkPreviewBtn(driver);
            ReadOnlyCollection<string> all_windowHandles_new = driver.WindowHandles;
            newwindowsize = all_windowHandles_new.Count;
            string existingWindowHandle = driver.CurrentWindowHandle;
            ThankYouPage._parentWindowHandle = existingWindowHandle;
            foreach (string handle in all_windowHandles_new)
            {
                if (handle != existingWindowHandle)
                {
                    popupHandle = handle;

                    break;
                }
            }

            //switch to new window 

            driver.SwitchTo().Window(popupHandle);
            return existingWindowHandle;
        }

        public static void ClkPreviewBtn(IWebDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5000));
            //Open new tabs
            wait.Until<IWebElement>(d =>
            {
                return d.FindElement(By.CssSelector("#btnFullPreview"));
            });

            UIActions.GetElement(driver, "#btnFullPreview").Click();
            //get the new window handles count
        }

        public static void redirectURL(IWebDriver driver)
        {

        }

        public static string readTxt(string _filePath)
        {
            string contentText;
            using (StreamReader sr = new StreamReader(_filePath))
            {
                contentText = sr.ReadToEnd();

            }
            return contentText;
        }
        public static String CopyRedirectURL(IWebDriver driver)
        {
            IWebElement Redirect = driver.FindElement(By.XPath("//span[contains(text(),'Redirect to a URL')]"));
            Redirect.Click();
            IWebElement RedirectURL = driver.FindElement(By.Id("thank-you-page-redirect-url"));
            RedirectURL.SendKeys(OpenQA.Selenium.Keys.Control + "a");
            RedirectURL.SendKeys(OpenQA.Selenium.Keys.Control + "c");
            String CopiedRedirectURL = System.Windows.Forms.Clipboard.GetText();
            return CopiedRedirectURL;
        }
        public static void CopyCustomizedThankYouContent(IWebDriver driver)
        {
            if (UIActions.IsElementVisible(driver, "//*[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']"))
            {
                IWebElement CustomizedText = driver.FindElement(By.XPath("//*[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']"));
                CustomizedText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                CustomizedText.SendKeys(OpenQA.Selenium.Keys.Control + "c");
            }
            IWebElement CancelThankYouPage = driver.FindElement(By.Id("btnThankYouPageCancel"));
            Actions action1 = new Actions(driver);
            action1.MoveToElement(CancelThankYouPage).Click().Perform();
        }
        public static void ThankYouPageRedirectURL(IWebDriver driver)
        {
            //AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
            Thread.Sleep(2000);
            IWebElement Redirect = driver.FindElement(By.XPath("//span[contains(text(),'Redirect to a URL')]"));
            Redirect.Click();
            IWebElement RedirectURL = driver.FindElement(By.Id("thank-you-page-redirect-url"));
            RedirectURL.SendKeys(OpenQA.Selenium.Keys.Control + "a");
            RedirectURL.SendKeys(OpenQA.Selenium.Keys.Delete);
            driver.FindElement(By.Id("thank-you-page-redirect-url")).SendKeys("https://www.google.com/");
            driver.FindElement(By.Id("btnThankYouPageSave")).Click();
        }

        public static bool EditandUpdateThankYouPageRedirectURL(IWebDriver driver)
        {
            // AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
            Thread.Sleep(5000);
            //IWebElement RedirectURLText = driver.FindElement(By.XPath("(//*[text()[contains(.,' https://www.google.com')] ])[3]"));
            // String URL = RedirectURLText.Text;
            IWebElement RedirectURL = driver.FindElement(By.Id("thank-you-page-redirect-url"));
            RedirectURL.SendKeys(OpenQA.Selenium.Keys.Control + "a");
            RedirectURL.SendKeys(OpenQA.Selenium.Keys.Control + 'c');
            String URL = System.Windows.Forms.Clipboard.GetText();
            if (URL.Contains("https://www.google.com/"))
            {
                RedirectURL.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                RedirectURL.SendKeys(OpenQA.Selenium.Keys.Delete);
                RedirectURL.SendKeys("https://www.quora.com");
                driver.FindElement(By.Id("btnThankYouPageSave")).Click();
                return true;
            }
            else
                return false;
        }
        public static void PublishForm(IWebDriver driver, string surveyName)
        {
            UIActions.GetElementswithXpath(driver, "//button[@class='btn btn-primary pub  no-margin-top ']", 60).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.Id("btnConfirmPublishClicked")).Click();
            Thread.Sleep(5000);

        }

        public static void OpenAccountSettings(IWebDriver driver)
        {
            Actions ac = new Actions(driver);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            UIActions.FindElementWithXpath(driver, "//span[@class='user-info']").Click();
            UIActions.FindElementWithXpath(driver, "//a[@href='/Pulse/AccountSettings']").Click();
            UIActions.clickwithID(driver, "menu-account-defaults");
            wait.Until(ExpectedConditions.ElementExists(By.XPath("//input[@name='allow-custom-thankyou'][@value='true']")));
            ac.MoveToElement(driver.FindElement(By.XPath("//input[@name='allow-custom-thankyou'][@value='true']"))).Build().Perform();
            UIActions.FindElementWithXpath(driver, "//input[@name='allow-custom-thankyou'][@value='true']").Click();
            ac.MoveToElement(driver.FindElement(By.Id("defaults-save"))).Build().Perform();
            driver.FindElement(By.Id("defaults-save")).Click();
        }

        public static string SetAsMandatory(IWebDriver driver)
        {

            IReadOnlyCollection<IWebElement> ddOptions = driver.FindElements(By.XPath("//div[@class='btn-group ceb-dropdown-container dropup open']/ul//label"));
            ddOptions.FirstOrDefault(x => x.Text.Trim() == "All").Click();
            string _isMandatoryMsgDisplayed = UIActions.GetElement(driver, "#mandatory-question-types-warning").Text;

            //bool _isMandatoryMsgDisplayed = UIActions.GetElement(driver, "#mandatory-question-types-warning").Displayed;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            ac.MoveToElement(UIActions.GetElement(driver, "#mandatory-question-types-warning")).Build().Perform();
            UIActions.GetElement(driver, "#btnFormSettingsSave").Click();
            return _isMandatoryMsgDisplayed;
        }

        public static bool EnableThankYouPageEdit(IWebDriver driver, string editType)
        {
            try
            {
                Actions ac = new Actions(driver);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
                Thread.Sleep(5000);
                IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='Title']"));
                foreach (IWebElement title in titleList)
                {
                    string titleText = title.Text;
                    if (titleText.Contains("End Page: Survey Completed"))
                    {
                        ac.MoveToElement(title).Build().Perform();
                        driver.FindElement(By.XPath("//span[@class='btn ceb-icon_home_class_edit action-Thank-you-edit secondary font-sz-23']")).Click();

                    }
                }

                string labelPath = "//div[@id='thank-you-page-context']//div[@class='row']//label";
                IReadOnlyCollection<IWebElement> thankyouEditType = driver.FindElements(By.XPath("//div[@id='thank-you-page-context']//div[@class='row']//label//span"));
                int i = 1;

                foreach (IWebElement singleType in thankyouEditType)
                {


                    if (editType.Equals(singleType.Text))
                    {

                        driver.FindElement(By.XPath("//div[@id='thank-you-page-context']//div[@class='row'][" + i + "]//label//span")).Click();
                        break;
                    }
                    i++;
                }

                switch (editType)
                {
                    case "Default Text":
                        break;
                    case "Customize Text":
                        Console.WriteLine("customThankUPage");
                        break;
                    case "Redirect to a URL":
                        break;
                }
            }
            catch (Exception e)
            {

            }


            return true;
        }

        public static void CopyQuestion(IWebDriver driver, string questionToCopy, string copyFromSurvey = null)
        {
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").Click();
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(questionToCopy);
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(OpenQA.Selenium.Keys.Enter);
            UIActions.GetElementswithXpath(driver, "//a[@aria-controls='CopySurveyPanel']", 10).Click();
            UIActions.GetElementswithXpath(driver, "//button[@data-bind='SurveyId']", 10).Click();

        }

        public static void CopyQuestionfromLeftSidecopy(IWebDriver driver, string questionToCopy, string QuestionFrom="Benchmarked")
        {
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").Click();
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(questionToCopy);
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(OpenQA.Selenium.Keys.Enter);
            Thread.Sleep(5000);
            if (QuestionFrom.ToString().Contains("Benchmarked"))
            {
                driver.FindElement(By.XPath("//*[@widget-data-event='click::SelectBenchmarkQuestionsClicked']")).Click();
            }
            else
            {
                driver.FindElement(By.XPath("//*[@widget-data-event='click::SelectCompanyQuestionsClicked']")).Click();

            }
            Thread.Sleep(5000);
        }
    

        public static void CopyQuestionFromQuestionsTab(IWebDriver driver, string questionToCopy, string copyFromSurvey = null)
        {
            Thread.Sleep(6000);
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").Click();
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(questionToCopy);
            Thread.Sleep(2000);
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(OpenQA.Selenium.Keys.Enter);
            Thread.Sleep(4000);
            UIActions.GetElement(driver, ".copyQuestions span.copyQuestionParent").Click();
            Thread.Sleep(2000);
        }
        public static void AddRatingScaleWithHoverText(IWebDriver driver, string questionText, string[] responses, string hoTxt)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            SetHoverTextForQuestion(driver, questionText, hoTxt, questionTextEditor);

            //UIActions.Click(driver, "#btnDropDownSave");
        }

        public static string AddCommentQuestionWithHoverText(IWebDriver driver, string questionText, string hoTxt)
        {
            var message = UIActions.GetElementWithWait(driver, "#add-Text", 60);
            string _hoTxt = "";
            if (message.Text.Trim() == "Comment")
            {
                UIActions.Click(driver, "#add-Text");
                Thread.Sleep(5000);
                IWebElement questionTextEditor = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
                Thread.Sleep(2000);
                questionTextEditor.Clear();
                questionTextEditor.SendKeys(questionText);
                Thread.Sleep(2000);
                SetHoverTextForQuestion_bkp(driver, questionText, hoTxt, questionTextEditor, "//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__tooltip cke_button_off']");
                Thread.Sleep(3000);
                _hoTxt = UIActions.GetAttrForElementByXpath(driver, "//span[@data-bind='isMandatory']/span/span", "title");
                UIActions.Click(driver, "#btnFixedFormatSave");
            }
            return _hoTxt;
        }

        public static string AddMultipleChoiceDemographicQuestion(IWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            DemoQuestionText = questionText;
            string[] categoryList = { "Demographic" };
            IWebElement categoryDropdown = driver.FindElement(By.XPath("(//*[@class='multiselect dropdown-toggle form-control ceb-dropdown align-left'])[3]"));
            categoryDropdown.Click();
            driver.FindElement(By.XPath("//a[@class='multiselect-clear-filter']")).Click();
            IWebElement CategroySearch = driver.FindElement(By.XPath("//*[@class='form-control multiselect-search']"));
            CategroySearch.Clear();
            CategroySearch.SendKeys("Demographic");
            categoryDropdown.Click();
            UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-category-Multiple", categoryList);
            Thread.Sleep(2000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            UIActions.Click(driver, "#btnMultipleChoiceSave");
            return DemoQuestionText;
        }

        public static string EditMCQSingleEditResponse(IWebDriver driver, string questionText, string UpdateQuestionText = null, string AddResponseText = null, int index = 0)
        {
            IList<IWebElement> QuestionList = driver.FindElements(By.XPath("//*[@class='panel panel-default question-list ui-droppable']"));
            for (int i = 0; i < QuestionList.Count; i++)
            {
                if (QuestionList[i].Text.Contains(questionText))
                {
                    IWebElement QuestionEditButton = driver.FindElement(By.XPath("(//*[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23'])[" + (i + 1) + "]"));
                    Actions action = new Actions(driver);
                    action.MoveToElement(QuestionEditButton, 27, 812).Click().Perform();
                    QuestionEditButton.Click();
                    Thread.Sleep(5000);
                    if (UpdateQuestionText != null)
                    {
                        DemoQuestionText = UpdateQuestionText;
                        IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtQuestion + .cke_textarea_inline");
                        questionTextEditor.Clear();
                        questionTextEditor.SendKeys(UpdateQuestionText);
                    }

                    if (AddResponseText != null)
                    {
                        DemoResponseText = AddResponseText;
                        var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");

                        responseBoxes.ElementAt(index).Click();
                        responseBoxes.ElementAt(index).Clear();
                        responseBoxes.ElementAt(index).SendKeys(AddResponseText);
                    }



                    UIActions.Click(driver, "#btnMultipleChoiceSave");
                    break;
                }

            }
            return DemoResponseText;
        }


        public static string EditMCQSingleAddResponse(IWebDriver driver, string questionText, string UpdateQuestionText = null, string updateResponseText = null, int index = 0)
        {
            IWebElement QuestionList = driver.FindElement(By.XPath("(//*[@class='panel panel-default question-list ui-droppable'])[" + index + "]"));

            if (QuestionList.Text.Contains(questionText))
            {
                IWebElement QuestionEditButton = driver.FindElement(By.XPath("(//*[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23'])[" + index + "]"));
                QuestionEditButton.Click();

                Thread.Sleep(7000);
                if (UpdateQuestionText != null)
                {
                    IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtQuestion + .cke_textarea_inline");
                    questionTextEditor.Clear();
                    questionTextEditor.SendKeys(UpdateQuestionText);
                }
                IWebElement AddMCQResponseOption = driver.FindElement(By.Id("btnAddRowsResponse"));

                AddMCQResponseOption.Click();


                if (updateResponseText != null)
                {
                    DemoResponseText = updateResponseText;
                    var responseBoxes = driver.FindElement(By.XPath("//*[@class='cke_textarea_inline cke_textarea_responses cke_editable cke_editable_inline cke_contents_ltr placeholder1 cke_show_borders']"));

                    responseBoxes.Click();
                    responseBoxes.Clear();
                    responseBoxes.SendKeys(updateResponseText);

                }



                UIActions.Click(driver, "#btnMultipleChoiceSave");

            }
            return DemoResponseText;
        }

        public static void DemographicEditConfirmationPoPup(IWebDriver driver, string ConfirmationOption)
        {
            if (ConfirmationOption == "Yes")
            {
                IWebElement ConfirmationMessage = driver.FindElement(By.Id("btnQuestionConfirmYes"));
                ConfirmationMessage.Click();
            }
            else if (ConfirmationOption == "No")
            {
                IWebElement ConfirmationMessageNo = driver.FindElement(By.Id("btnQuestionConfirmNo"));
                ConfirmationMessageNo.Click();
            }
        }


        public static void EditSectionName(IWebDriver driver, string sectionname)
        {
            try
            {
                IWebElement sectionedit = UIActions.FindElement(driver, ".action-section-edit");
                sectionedit.Click();
                IWebElement sectioneditbox = UIActions.FindElement(driver, ".section-list [data-bind-type=formvalue]");
                sectioneditbox.Clear();
                sectioneditbox.SendKeys(sectionname);
                UIActions.Click(driver, ".section-list .ceb-icon_tick");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void disableProgressBar(IWebDriver driver)
        {
            try
            {
                IWebElement sectionedit = UIActions.FindElementWithXpath(driver, "//*[@id='btnFormSettings']");
                sectionedit.Click();
                Thread.Sleep(4000);
                IWebElement sectioneditbox = UIActions.FindElementWithXpath(driver, "//a[@id='branding-form-option']");
                sectioneditbox.Click();
                Thread.Sleep(4000);
                IWebElement progressbarButton = UIActions.FindElementWithXpath(driver, "//div[@class='col-sm-8']//input[@type='checkbox']");
                if (progressbarButton.Selected)
                {
                    progressbarButton.Click();
                }



                sectionedit = UIActions.FindElementWithXpath(driver, "//*[@id='btnFormSettingsSave']");
                sectionedit.Click();
                Thread.Sleep(4000);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void enableQuestionNumber(IWebDriver driver)
        {
            try
            {
                IWebElement sectionedit = UIActions.FindElementWithXpath(driver, "//*[@id='btnFormSettings']");
                sectionedit.Click();
                Thread.Sleep(4000);
                IWebElement sectioneditbox = UIActions.FindElementWithXpath(driver, "//a[@id='branding-form-option']");
                sectioneditbox.Click();
                Thread.Sleep(4000);
                IWebElement progressbarButton = UIActions.FindElementWithXpath(driver, "//input[@type='checkbox' and @data-bind='ShowQuestionNumber']");
                if (!progressbarButton.Selected)
                {
                    progressbarButton.Click();
                }

                sectionedit = UIActions.FindElementWithXpath(driver, "//*[@id='btnFormSettingsSave']");
                sectionedit.Click();
                Thread.Sleep(4000);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void disableQuestionNumber(IWebDriver driver)
        {
            try
            {
                IWebElement sectionedit = UIActions.FindElementWithXpath(driver, "//*[@id='btnFormSettings']");
                sectionedit.Click();
                Thread.Sleep(4000);
                IWebElement sectioneditbox = UIActions.FindElementWithXpath(driver, "//a[@id='branding-form-option']");
                sectioneditbox.Click();
                Thread.Sleep(4000);
                IWebElement progressbarButton = UIActions.FindElementWithXpath(driver, "//input[@type='checkbox' and @data-bind='ShowQuestionNumber']");
                if (progressbarButton.Selected)
                {
                    progressbarButton.Click();
                }

                sectionedit = UIActions.FindElementWithXpath(driver, "//*[@id='btnFormSettingsSave']");
                sectionedit.Click();
                Thread.Sleep(4000);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void EditPageName(IWebDriver driver, string pagename)
        {
            try
            {
                IWebElement pageedit = UIActions.FindElement(driver, ".action-page-edit");
                pageedit.Click();
                IWebElement pageeditbox = UIActions.FindElement(driver, ".page-list [data-bind-type=formvalue]");
                pageeditbox.Clear();
                pageeditbox.SendKeys(pagename);
                UIActions.Click(driver, ".page-list .ceb-icon_tick");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static bool CheckStyleSheetValues(IWebDriver driver, StyleSheetParametersDTO styleSheetParameters)
        {
            bool b_BackgroundColor = true;
            bool b_TitleFontColor = true;
            bool b_TitleBarColor = true;
            bool b_SectionHeadingFontColor = true;
            bool b_NextButton = true;
            bool b_PreviousButton = true;
            bool b_FinishButton = true;
            bool b_ProgressBar = true;
            bool b_ScaleLabelFontColor = true;
            try
            {
                if (styleSheetParameters.BackgroundColor != null && styleSheetParameters.SecondaryBackgroundColor != null)
                {
                    IWebElement bgelement = UIActions.FindElement(driver, "#wrapper-inner");
                    string bgcolortext = bgelement.GetCssValue("background").ToString();
                    string gradientcolortext = CommonActions.GetBetweenStrings(bgcolortext, "deg, ", ") ");
                    string[] gradientcolors = gradientcolortext.Split(',');
                    int grad1_red = Int32.Parse(gradientcolors[0].Trim().Substring(4));
                    int grad1_green = Int32.Parse(gradientcolors[1]);
                    int grad1_blue = Int32.Parse(gradientcolors[2].Trim().Substring(0, gradientcolors[2].Trim().Length - 1));
                    int grad2_red = Int32.Parse(gradientcolors[3].Trim().Substring(4));
                    int grad2_green = Int32.Parse(gradientcolors[4]);
                    int grad2_blue = Int32.Parse(gradientcolors[5].Trim().Substring(0, gradientcolors[5].Trim().Length - 1));
                    string hex_grad1 = ColorTranslator.ToHtml(Color.FromArgb(1, grad1_red, grad1_green, grad1_blue));
                    string hex_grad2 = ColorTranslator.ToHtml(Color.FromArgb(1, grad2_red, grad2_green, grad2_blue));
                    b_BackgroundColor = (styleSheetParameters.BackgroundColor == hex_grad1) && (styleSheetParameters.SecondaryBackgroundColor == hex_grad2);
                }
                if (styleSheetParameters.TitleFontColor != null)
                {
                    IWebElement SurveyTitle = UIActions.FindElement(driver, "#txtSurveyNameTitle");
                    string[] surveytitlecolor = SurveyTitle.GetCssValue("color").ToString().Split(',');
                    int red = Int32.Parse(surveytitlecolor[0].Substring(5));
                    int green = Int32.Parse(surveytitlecolor[1]);
                    int blue = Int32.Parse(surveytitlecolor[2]);
                    string hex_surveytitlecolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_TitleFontColor = (styleSheetParameters.TitleFontColor == hex_surveytitlecolor);
                }
                if (styleSheetParameters.TitleBarColor != null)
                {
                    IWebElement SurveyTitleHeading = UIActions.FindElement(driver, ".survey-title-heading");
                    string[] surveytitleheadingcolor = SurveyTitleHeading.GetCssValue("background-color").ToString().Split(',');
                    int red = Int32.Parse(surveytitleheadingcolor[0].Substring(5));
                    int green = Int32.Parse(surveytitleheadingcolor[1]);
                    int blue = Int32.Parse(surveytitleheadingcolor[2]);
                    string hex_surveytitleheadingcolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_TitleBarColor = (styleSheetParameters.TitleBarColor == hex_surveytitleheadingcolor);
                }
                if (styleSheetParameters.SectionHeadingFontColor != null)
                {
                    IWebElement SectionTitle = UIActions.FindElement(driver, "#txtSectionNameTitle");
                    string[] sectiontitlecolor = SectionTitle.GetCssValue("color").ToString().Split(',');
                    int red = Int32.Parse(sectiontitlecolor[0].Substring(5));
                    int green = Int32.Parse(sectiontitlecolor[1]);
                    int blue = Int32.Parse(sectiontitlecolor[2]);
                    string hex_sectiontitlecolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_SectionHeadingFontColor = (styleSheetParameters.SectionHeadingFontColor == hex_sectiontitlecolor);
                }
                if (styleSheetParameters.SelectionColor != null)
                {
                    int red = 0;
                    int green = 0;
                    int blue = 0;
                    IWebElement NextButton = UIActions.FindElement(driver, "#btnNext");
                    // IWebElement PreviousButton = UIActions.FindElement(driver, ".btn-primary[title='Previous']");
                    IWebElement FinishButton = UIActions.FindElement(driver, "#btnFinish");
                    IWebElement ProgressBar = UIActions.FindElement(driver, ".progress-bar");

                    string[] nextbuttoncolor = NextButton.GetCssValue("background-color").ToString().Split(',');
                    red = Int32.Parse(nextbuttoncolor[0].Substring(5));
                    green = Int32.Parse(nextbuttoncolor[1]);
                    blue = Int32.Parse(nextbuttoncolor[2]);
                    string hex_nextbuttoncolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_NextButton = (styleSheetParameters.SelectionColor == hex_nextbuttoncolor);

                    // string[] previousbuttoncolor = PreviousButton.GetCssValue("background-color").ToString().Split(',');
                    //  red = Int32.Parse(previousbuttoncolor[0].Substring(5));
                    //   green = Int32.Parse(previousbuttoncolor[1]);
                    //   blue = Int32.Parse(previousbuttoncolor[2]);
                    string hex_previousbuttoncolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_PreviousButton = (styleSheetParameters.SelectionColor == hex_previousbuttoncolor);

                    string[] finishcolor = FinishButton.GetCssValue("background-color").ToString().Split(',');
                    red = Int32.Parse(finishcolor[0].Substring(5));
                    green = Int32.Parse(finishcolor[1]);
                    blue = Int32.Parse(finishcolor[2]);
                    string hex_finishcolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_FinishButton = (styleSheetParameters.SelectionColor == hex_finishcolor);

                    string[] progressbarcolor = ProgressBar.GetCssValue("background-color").ToString().Split(',');
                    red = Int32.Parse(progressbarcolor[0].Substring(5));
                    green = Int32.Parse(progressbarcolor[1]);
                    blue = Int32.Parse(progressbarcolor[2]);
                    string hex_progressbarcolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_ProgressBar = (styleSheetParameters.SelectionColor == hex_progressbarcolor);

                    //UIActions.Click(driver, ".item-responses[data-bind-iterate='itemResponses']");
                    ////IWebElement ResponseOption = UIActions.FindElement(driver, ".item-responses[data-bind-iterate='itemResponses']");
                    //IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                    ////String colorRGB = js.ExecuteScript("return window.getComputedStyle(arguments[0], ':before').getPropertyValue('background-color');", ResponseOption).ToString();
                    //Actions actions = new Actions(driver);
                    //actions.MoveToElement(driver.FindElement(By.CssSelector(".item-responses[data-bind-iterate='itemResponses']"))).Build().Perform();
                    //IWebElement ResponseOption = UIActions.FindElement(driver, "span.lbl");
                    //string[] responseoptioninnercolor = ResponseOption.GetCssValue("background-color").ToString().Split(',');
                }
                if (styleSheetParameters.ScaleLabelFontColor != null)
                {
                    IWebElement scaleheading = UIActions.FindElement(driver, ".all-positive-headers [data-bind='text']");
                    string[] scaleheadingcolor = scaleheading.GetCssValue("color").ToString().Split(',');
                    int red = Int32.Parse(scaleheadingcolor[0].Substring(5));
                    int green = Int32.Parse(scaleheadingcolor[1]);
                    int blue = Int32.Parse(scaleheadingcolor[2]);
                    string hex_scaleheadingcolor = ColorTranslator.ToHtml(Color.FromArgb(1, red, green, blue));
                    b_ScaleLabelFontColor = (styleSheetParameters.ScaleLabelFontColor == hex_scaleheadingcolor);
                }
                return b_BackgroundColor && b_TitleFontColor && b_TitleBarColor && b_SectionHeadingFontColor && b_NextButton && b_PreviousButton && b_FinishButton && b_ProgressBar && b_ScaleLabelFontColor;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }


        public static DataSet DBValidationWithQuery(DBConnectionStringDTO DBConnectionParameters, string Query)
        {
            DataSet ds = new DataSet();

            ds = DBOperations.ExecuteSPwithInputParameters_TCES(Query, DBConnectionParameters);

            return ds;
        }
        public static DataSet DBValidationWithQuery_TCESReporting(DBConnectionStringDTO DBConnectionParameters, string Query)
        {
            DataSet ds = new DataSet();

            ds = DBOperations.ExecuteSPwithInputParameters(Query, DBConnectionParameters);

            return ds;
        }

        public static bool ValidateDemoScaleoptionInDB(DataSet ds)
        {
            string dsDemoText = null;
            foreach (DataRow dsrow in ds.Tables[0].Rows)
            {
                Console.WriteLine(dsrow[0].ToString());

                if (dsrow[0].ToString() == DemoResponseText)
                {
                    dsDemoText = dsrow[0].ToString();
                }

            }

            return dsDemoText == DemoResponseText ? true : false;
        }

        public static bool ValidateAnyDemoQuestionInDB(DataSet ds, string DemoQuestionText)
        {

            string dSDemoText = null;
            foreach (DataRow dsrow in ds.Tables[0].Rows)
            {
                Console.WriteLine(dsrow[0].ToString());

                if (dsrow[0].ToString() == DemoQuestionText)
                {
                    dSDemoText = dsrow[0].ToString();
                }

            }

            return dSDemoText == DemoQuestionText ? true : false;
        }
        public static bool ValidateDemoQuestionInDB(DataSet ds)
        {

            string dSDemoText = null;
            foreach (DataRow dsrow in ds.Tables[0].Rows)
            {
                Console.WriteLine(dsrow[0].ToString());

                if (dsrow[0].ToString() == DemoQuestionText)
                {
                    dSDemoText = dsrow[0].ToString();
                }

            }

            return dSDemoText == DemoQuestionText ? true : false;
        }

        public static bool ValidateDeletedemo(DataSet ds)
        {

            string dSDemoText = null;
            foreach (DataRow dsrow in ds.Tables[0].Rows)
            {
                Console.WriteLine(dsrow[0].ToString());

                if (dsrow[0].ToString() != DemoQuestionText)
                {
                    dSDemoText = dsrow[0].ToString();
                }

            }

            return dSDemoText != DemoQuestionText ? true : false;
        }


        public static int ValidateQueryEntriesCount(DataSet ds)
        {
            int dsrowCount = 0;
            dsrowCount = ds.Tables[0].Rows.Count;
            return dsrowCount;
        }

        public static bool ValidateDataTransferParticipantQueue(DBConnectionStringDTO DBConnectionParameters, string Query)
        {
            string ds_datatransferStatus = null;
            DataSet ds = new DataSet();

            while (ds_datatransferStatus != "7")
            {

                ds = DBOperations.ExecuteSPwithInputParameters_TCES(Query, DBConnectionParameters);

                //ds_datatransferStatus = ds.Tables[0].Rows.ToString();

                foreach (DataRow dsrow in ds.Tables[0].Rows)
                {
                    ds_datatransferStatus = dsrow[0].ToString();
                }

            }

            return ds_datatransferStatus == "7" ? true : false;



        }

        public static bool ValidateDataTransferItem_Queue(DBConnectionStringDTO DBConnectionParameters, string Query)
        {
            string ds_datatransferStatus = null;
            DataSet ds = new DataSet();

            while (ds_datatransferStatus != "2")
            {

                ds = DBOperations.ExecuteSPwithInputParameters_TCES(Query, DBConnectionParameters);

                //ds_datatransferStatus = ds.Tables[0].Rows.ToString();

                foreach (DataRow dsrow in ds.Tables[0].Rows)
                {
                    ds_datatransferStatus = dsrow[0].ToString();
                }

            }

            return ds_datatransferStatus == "2" ? true : false;
        }

        public static void NavigateToSupportandImpersonate(IWebDriver driver, string username, int index = 0)
        {
            driver.FindElement(By.XPath("//a[text()='Users']")).Click();
            IWebElement Search = driver.FindElement(By.Id("search-string"));
            Search.Click();
            Search.SendKeys(username);
            driver.FindElement(By.Id("btnSearchLinkButton")).Click();
            Thread.Sleep(8000);
            IWebElement ActionGear = driver.FindElement(By.XPath("(//*[@class='btn btn-link no-underline dropdown-toggle'])[" + index + "]"));
            Actions action = new Actions(driver);
            action.MoveToElement(ActionGear).Click().Perform();
            IWebElement ImpersonateButton = driver.FindElement(By.XPath("//*[@widget-data-event='click::ImpersonateUser']"));
            ImpersonateButton.Click();
            Thread.Sleep(20000);

        }



        public static void SearchandCreateForm(IWebDriver driver, string surveyform)
        {

            IWebElement SearchField = driver.FindElement(By.Id("txtSearch"));
            SearchField.Clear();
            SearchField.SendKeys(surveyform);
            IWebElement SearchButton = driver.FindElement(By.Id("btnSearch"));
            SearchButton.Click();
            Thread.Sleep(4000);
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SearchedValue = (string)js.ExecuteScript("return $('#Form-table').text()");
            string NoRecords = "No matching records found ";

            if (SearchedValue.Contains(NoRecords))
            {
                Thread.Sleep(5000);
                AuthorActions.CreateForm(driver, surveyform);

            }
            else if (SearchedValue.Contains("Draft"))
            {
                Thread.Sleep(5000);
                IWebElement OpenForm = driver.FindElement(By.XPath("(//*[@widget-data-event='click::EditClicked'])[1]"));
                OpenForm.Click();
                Thread.Sleep(20000);
            }
            else
            {
                IWebElement OpenForm = driver.FindElement(By.XPath("(//*[@widget-data-event='click::EditClicked'])[1]"));
                OpenForm.Click();
                Thread.Sleep(15000);
                AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(20000);
            }

        }

        public static void DeleteResponseOption(IWebDriver driver, string questionText, string ResponseOption)
        {
            IList<IWebElement> QuestionList = driver.FindElements(By.XPath("//*[@class='panel panel-default question-list ui-droppable']"));
            for (int i = 0; i < QuestionList.Count; i++)
            {
                if (QuestionList[i].Text.Contains(questionText))
                {
                    IWebElement QuestionEditButton = driver.FindElement(By.XPath("(//*[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23'])[" + (i + 1) + "]"));
                    Actions action = new Actions(driver);
                    action.MoveToElement(QuestionEditButton, 27, 812).Click().Perform();
                    QuestionEditButton.Click();
                    Thread.Sleep(5000);


                    IList<IWebElement> ResponseList = driver.FindElements(By.XPath("//*[@class='cke_textarea_inline cke_textarea_responses cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']"));
                    for (int j = 0; j < ResponseList.Count; j++)
                    {
                        if (ResponseList[j].Text == ResponseOption)
                        {
                            IWebElement DeleteButton = driver.FindElement(By.XPath("(//*[@id='btnRemoveResponse'])[" + (j) + "]"));
                            Actions action1 = new Actions(driver);
                            action1.MoveToElement(DeleteButton).Click().Perform();
                            UIActions.Click(driver, "#btnMultipleChoiceSave");
                            break;
                        }
                    }

                    break;

                }
            }


        }
        public static void TranslationTemplateUpload(IWebDriver driver, IWebElement element, String filePath)
        {
            try
            {

                string image_path = filePath;
                IWebElement selectBtn = element;
                selectBtn.SendKeys(image_path);
                Actions action = new Actions(driver);
                action.MoveToElement(driver.FindElement(By.Id("btnContentSave"))).Build().Perform();
                driver.FindElement(By.Id("btnContentSave")).Click();
                Thread.Sleep(3000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void NavigatetoLanguagespopup(IWebDriver driver)
        {
            try
            {
                IWebElement Languages = driver.FindElement(By.Id("btnlangSettings"));
                Languages.Click();
                Thread.Sleep(4000);
                IReadOnlyCollection<IWebElement> AdditionalLanguage = driver.FindElements(By.XPath("(//button[@class='multiselect dropdown-toggle form-control ceb-dropdown align-left'])[3]"));
                AdditionalLanguage.FirstOrDefault(x => x.Text.Trim() == "Please Select...").Click();
                IReadOnlyCollection<IWebElement> AfricanLanguage = driver.FindElements(By.XPath("//*[@class='checkbox']"));
                AfricanLanguage.FirstOrDefault(x => x.Text.Trim() == "Afrikaans as spoken in South Africa").Click();
                IReadOnlyCollection<IWebElement> AdditionalLanguage1 = driver.FindElements(By.XPath("(//button[@class='multiselect dropdown-toggle form-control ceb-dropdown align-left'])[3]"));
                AdditionalLanguage.FirstOrDefault(x => x.Text.Trim() == "Afrikaans as spoken in South Africa").Click();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void GotoQuestionTileAndExpand(IWebDriver driver, string questionText)
        {
            IList<IWebElement> QuestionList = driver.FindElements(By.XPath("//*[@class='panel panel-default question-list ui-droppable']"));
            for (int i = 0; i < QuestionList.Count; i++)
            {
                if (QuestionList[i].Text.Contains(questionText))
                {
                    QuestionList[i].Click();
                    break;
                }
            }

        }

        public static string AddDDtQuestionWithHoverText(IWebDriver driver, string questionText, string hoverTxt, string[] responses)
        {
            string _hoTxt = "";
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            SetHoverTextForQuestion_bkp(driver, questionText, hoverTxt, questionTextEditor, "//div[@id='cke_txtDropDownQuestion']//a[@class='cke_button cke_button__tooltip cke_button_off']");
            Thread.Sleep(5000);
            _hoTxt = UIActions.GetAttrForElementByXpath(driver, "//span[@data-bind='isMandatory']/span/span", "title");
            UIActions.Click(driver, "#btnDropDownSave");
            return _hoTxt;
        }
        public static void SetHoverTextForQuestion(IWebDriver driver, string qText, string hoverText, IWebElement element)
        {
            /* Set hover text for the instruction text */
            //IWebElement _textArea = driver.FindElement(By.XPath("//*[@id='fixedFormat-response']/div/div[1]/div[2]/div"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            element.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
            Thread.Sleep(1000);
            //IWebElement item = driver.FindElement(By.XPath("//span[@class='cke_button_icon cke_button__tooltip_icon']"));
            Thread.Sleep(1000);
            IWebElement ckEditorlink = driver.FindElement(By.XPath("//div[@id='cke_txtDropDownQuestion']//a[@class='cke_button cke_button__tooltip cke_button_off']"));

            executor.ExecuteScript("arguments[0].click();", ckEditorlink);
            UIActions.GetElementswithXpath(driver, "//input[@class='cke_dialog_ui_input_text']", 25).SendKeys(qText);
            UIActions.GetElementswithXpath(driver, "//div[@class='cke_dialog_ui_input_textarea']/textarea", 25).SendKeys(hoverText);
            UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();
        }
        public static void SetHoverTextForQuestion_bkp(IWebDriver driver, string qText, string hoverText, IWebElement element, string ckEditorElement)
        {
            /* Set hover text for the instruction text */
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            element.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            IWebElement ckEditorlink = driver.FindElement(By.XPath(ckEditorElement));
            executor.ExecuteScript("arguments[0].click();", ckEditorlink);
            UIActions.GetElementswithXpath(driver, "//input[@class='cke_dialog_ui_input_text']", 25).SendKeys(qText);
            UIActions.GetElementswithXpath(driver, "//div[@class='cke_dialog_ui_input_textarea']/textarea", 25).SendKeys(hoverText);
            Thread.Sleep(1000);
            UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();

        }

        public static void SetHoverTextForQuestion_mcq(IWebDriver driver, string qText, string hoverText, IWebElement element, string ckEditorElement)
        {
            /* Set hover text for the instruction text */
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            // element.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            IWebElement ckEditorlink = driver.FindElement(By.XPath(ckEditorElement));
            executor.ExecuteScript("arguments[0].click();", ckEditorlink);
            UIActions.GetElementswithXpath(driver, "//input[@class='cke_dialog_ui_input_text']", 25).SendKeys(qText);
            UIActions.GetElementswithXpath(driver, "//div[@class='cke_dialog_ui_input_textarea']/textarea", 25).SendKeys(hoverText);
            UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();

        }
        public static void SetHoverTextForQuestion_bkpArray(IWebDriver driver, string[] qText, string hoverText, IWebElement element, string ckEditorElement)
        {
            /* Set hover text for the instruction text */
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            //element.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            IWebElement ckEditorlink = driver.FindElement(By.XPath(ckEditorElement));

            executor.ExecuteScript("arguments[0].click();", ckEditorlink);
            for (int i = 1; i < qText.Length; i++)
            {
                UIActions.Click(driver, "#mcMultiItemsQuestions .addmoreButton button");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#mcMultiItemsQuestions .cke_textarea_inline");
            for (int i = 0; i < qText.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(qText[i]);
                UIActions.GetElementswithXpath(driver, "//div[@class='cke_dialog_ui_input_textarea']/textarea", 25).SendKeys(hoverText);
                UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();
            }
            //UIActions.GetElementswithXpath(driver, "//input[@class='cke_dialog_ui_input_text']", 25).SendKeys(qText);
            UIActions.GetElementswithXpath(driver, "//div[@class='cke_dialog_ui_input_textarea']/textarea", 25).SendKeys(hoverText);
            UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();

        }

        public static string AddMCQQuestionWithHoverText(IWebDriver driver, string[] questionTxt, string hoverTxt, string[] responses)
        {

            string _hoTxt = "";
            string instructionText = "This is instructionText";
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#mcScaleType", "multiItem");
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtIncludeMultiItemInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questionTxt.Length; i++)
            {
                UIActions.Click(driver, "#mcMultiItemsQuestions .addmoreButton button");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#mcMultiItemsQuestions .cke_textarea_inline");
            for (int i = 0; i < questionTxt.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                //questionBoxes.ElementAt(i).SendKeys(questionTxt[i]); 
                questionBoxes.ElementAt(i).SendKeys(questionTxt[i]);
                IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
                OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
                questionBoxes.ElementAt(i).SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);

                try
                {
                    SetHoverTextForQuestion_mcq(driver, questionTxt[i], hoverTxt, instructionBox, "//div[@id='cke_mcDefaultQuestion']//a[@class='cke_button cke_button__tooltip cke_button_off']");
                }
                catch (Exception)
                {
                    SetHoverTextForQuestion_mcq(driver, "What the training duration sufficient", hoverTxt, instructionBox, "//div[@id='cke_mcDefaultQuestion']//a[@class='cke_button cke_button__tooltip cke_button_off']");

                }
            }
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            Thread.Sleep(5000);
            _hoTxt = UIActions.GetAttrForElementByXpath(driver, "//span[@data-bind='isMandatory']/span/span", "title");
            UIActions.Click(driver, "#btnMultipleChoiceSave");
            Thread.Sleep(5000);
            return _hoTxt;
        }

        public static string AddFixedFormatDateQuestionAsDemoWithHO(IWebDriver driver, string info, string hoverTxt)
        {
            string _hoTxt = "";
            UIActions.clickwithID(driver, "add-fixed-format");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#fixed-format-type", "format_date");
            IWebElement _categoryDropdownField = UIActions.GetElementswithXpath(driver, "//div[@id='divFixedQuestionCategory']//button[@class='multiselect dropdown-toggle form-control ceb-dropdown align-left']", 25);
            _categoryDropdownField.Click();
            //IReadOnlyCollection<IWebElement> categoryList = UIActions.FindElements(driver, ".checkbox");
            IReadOnlyCollection<IWebElement> categoryList = UIActions.FindElementsWithXpath(driver, "//label[@class='checkbox']");

            categoryList.FirstOrDefault(x => x.Text.Contains("Demographic")).Click();
            //UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-category-Fixed", new string[] { " Demographic" });

            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            // infoquestionbox.SendKeys(info);
            SetHoverTextForQuestion_bkp(driver, info, hoverTxt, infoquestionbox, "//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__tooltip cke_button_off']");
            Thread.Sleep(2000);
            _hoTxt = UIActions.GetAttrForElementByXpath(driver, "//span[@data-bind='isMandatory']/span/span", "title");
            UIActions.clickwithID(driver, "btnFixedFormatSave");
            return _hoTxt;

        }
        public static void SetHoverText(IWebDriver driver, string hoverText)
        {
            /* Set hover text for the instruction text */
            IWebElement _textArea = driver.FindElement(By.XPath("//*[@id='fixedFormat-response']/div/div[1]/div[2]/div"));
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            _textArea.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
            UIActions.GetElementswithXpath(driver, "//span[@class='cke_button_icon cke_button__tooltip_icon']", 25).Click();
            UIActions.GetElementswithXpath(driver, "//div[@class='cke_dialog_ui_input_textarea']/textarea", 25).SendKeys(hoverText);
            UIActions.GetElementswithXpath(driver, "//span[@class='cke_dialog_ui_button']", 25).Click();
        }

        public static void EditHoverTextForQuestion_bkp(IWebDriver driver, string qText, string hoverText, IWebElement element, string ckEditorElement, string hoverTextEditorElement, string okBtn)
        {
            /* Set hover text for the instruction text */
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            element.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            IWebElement ckEditorlink = driver.FindElement(By.XPath(ckEditorElement));
            executor.ExecuteScript("arguments[0].click();", ckEditorlink);

            UIActions.GetElementswithXpath(driver, hoverTextEditorElement, 25).Click();
            UIActions.GetElementswithXpath(driver, hoverTextEditorElement, 25).Clear();
            UIActions.GetElementswithXpath(driver, hoverTextEditorElement, 25).SendKeys(hoverText);
            UIActions.GetElementswithXpath(driver, okBtn, 20).Click();

        }
        public static void EditHoverTextForSingleQuestion(IWebDriver driver, string questionText, string hoverText, string saveButtonElement, string questionEditorElement, string hoverElement, string hoverTextEditorElement, string okBtn)
        {
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IReadOnlyCollection<IWebElement> _questionCollection = UIActions.FindElementsWithXpath(driver, "//span[@data-bind='BindQuestionTile']");
            // IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline);
            Thread.Sleep(1000);
            try
            {
                ac.MoveToElement(_questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questionText))).Build().Perform();
                if (!_questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questionText)).Displayed)
                {
                    ac.MoveToElement(_questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questionText))).Build().Perform();
                }
                ac.MoveToElement(_questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questionText)));
                _questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questionText)).Click();
                ac.MoveToElement(_questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questionText))).Build().Perform();
                Thread.Sleep(1000);
                IReadOnlyCollection<IWebElement> _questions = UIActions.FindElementsWithXpath(driver, "//div[@class='panel panel-default question-list ui-droppable']//span[@title='Edit']");
                for (int i = 1; i <= _questions.Count(); i++)
                {
                    string r = UIActions.FindElementWithXpath(driver, "//div[@class][" + i + "]//span[@data-bind='BindQuestionTile']").Text;
                    try
                    {
                        bool v = (UIActions.GetElementswithXpath(driver, "//div[@class='panel panel-default question-list ui-droppable'][" + i + "]//span[@title='Edit']", 2).Displayed
                            && (UIActions.FindElementWithXpath(driver, "//div[@class][" + i + "]//span[@data-bind='BindQuestionTile']").Text.Contains(questionText)));
                        UIActions.GetElementswithXpath(driver, "//div[@class='panel panel-default question-list ui-droppable'][" + i + "]//span[@title='Edit']", 2).Click();
                        break;
                    }
                    catch (Exception)
                    {

                    }
                }
                Thread.Sleep(3000);
                Thread.Sleep(4000);
                /* Click on editor */
                ac.MoveToElement(driver.FindElement(By.CssSelector(questionEditorElement + " +  .cke_textarea_inline"))).Build().Perform();
                IWebElement questionTextEditor = UIActions.GetElement(driver, questionEditorElement + " +  .cke_textarea_inline");
                EditHoverTextForQuestion_bkp(driver, questionText, hoverText, questionTextEditor, "//div[@id='" + hoverElement + "']//a[@class='cke_button cke_button__tooltip cke_button_off']", hoverTextEditorElement, okBtn);

                Thread.Sleep(3000);
                ac.MoveToElement(driver.FindElement(By.CssSelector(saveButtonElement))).Build().Perform();
                driver.FindElement(By.CssSelector(saveButtonElement)).Click();
                //    UIActions.clickwithID(driver, saveButtonElement);
                Thread.Sleep(3000);
            }
            catch (Exception)
            {

            }

        }
        public static void EditQuestion(IWebDriver driver, String questionText)
        {
            IList<IWebElement> QuestionList = driver.FindElements(By.XPath("//*[@class='inline-block']"));
            IList<IWebElement> EditQuestionButton = driver.FindElements(By.XPath("//*[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']"));
            for (int i = 0; i < QuestionList.Count; i++)
            {
                if (QuestionList[i].Text.Contains(questionText))
                {
                    QuestionList[i].Click();
                    EditQuestionButton[i].Click();
                    break;
                }
            }
            Thread.Sleep(3000);
        }

        public static void DuplicateQuestion(IWebDriver driver,string QuestionText)
        {
            IList<IWebElement> QuestionList = driver.FindElements(By.XPath("//*[@class='inline-block']"));
            IList<IWebElement> hiddenButton = driver.FindElements(By.XPath("//*[@class='btn shl-icon_assessment_catalogue action-question-copy secondary font-sz-19']"));
            for (int i = 0; i < QuestionList.Count; i++)
            {
                if (QuestionList[i].Text.Contains(QuestionText))
                {
                    QuestionList[i].Click();
                    hiddenButton[i].Click();
                    break;

                }
            }
            Thread.Sleep(7000);
        }

        public static void DeleteQuestion(IWebDriver driver,string questionText)
        {
            IList<IWebElement> QuestionList = driver.FindElements(By.XPath("//*[@class='inline-block']"));
            IList<IWebElement> DeleteButton = driver.FindElements(By.XPath("//*[@class='btn ceb-icon_delete2_knockout action-question-delete secondary font-sz-23']"));
            for (int i = 0; i < QuestionList.Count; i++)
            {
                if (QuestionList[i].Text.Contains(questionText))
                {
                    QuestionList[i].Click();
                    DeleteButton[i].Click();
                    driver.FindElement(By.Id("btnConfirmDeleteSectionClicked")).Click();
                    break;

                }
            }
            Thread.Sleep(7000);
        }


        public static void ChangingQuestionCategoryToDemo(IWebDriver driver, String Category = " Demographic")
        {
            IWebElement categoryDropdown = driver.FindElement(By.XPath("(//*[@class='multiselect dropdown-toggle form-control ceb-dropdown align-left'])[3]"));
            categoryDropdown.Click();
            driver.FindElement(By.XPath("//a[@class='multiselect-clear-filter']")).Click();
            IWebElement CategroySearch = driver.FindElement(By.XPath("//*[@class='form-control multiselect-search']"));
            CategroySearch.Clear();
            CategroySearch.SendKeys("Demographic");
            Thread.Sleep(5000);
            var questions = driver.FindElements(By.XPath("//*[@class='checkbox']"));
            var DemoCategory = questions.FirstOrDefault(e => e.Displayed && e.Text.Trim() == "Demographic");
            if (DemoCategory != null)
                DemoCategory.Click();
            categoryDropdown.Click();
            Thread.Sleep(2000);
        }
        public static void SaveMultipleChoiceQuestion(IWebDriver driver)
        {
            UIActions.Click(driver, "#btnMultipleChoiceSave");
        }
        public static void SaveDropDownQuestion(IWebDriver driver)
        {
            UIActions.Click(driver, "#btnDropDownSave");
        }
        public static void SaveFixedFormatQuestion(IWebDriver driver)
        {
            UIActions.Click(driver, "#btnFixedFormatSave");
        }
        public static void ChangingQuestionCategoryToNonDemo(IWebDriver driver, String Category = " Demographic")
        {
            IWebElement categoryDropdown = driver.FindElement(By.XPath("(//*[@class='multiselect dropdown-toggle form-control ceb-dropdown align-left'])[3]"));
            categoryDropdown.Click();
            driver.FindElement(By.XPath("//a[@class='multiselect-clear-filter']")).Click();
            IWebElement CategroySearch = driver.FindElement(By.XPath("//*[@class='form-control multiselect-search']"));
            CategroySearch.Clear();
            CategroySearch.SendKeys("category1");
            var questions = driver.FindElements(By.XPath("//*[@class='checkbox']"));
            var DemoCategory = questions.FirstOrDefault(e => e.Displayed && e.Text.Trim() == "category1");
            if (DemoCategory != null)
                DemoCategory.Click();
            categoryDropdown.Click();
            Thread.Sleep(2000);
        }
        public static void DownloadLanguageTemplate(IWebDriver driver, string[] Languages)
        {
            try
            {
                IWebElement LanguageLink = UIActions.GetElement(driver, "#btnlangSettings");
                LanguageLink.Click();
                Thread.Sleep(4000);
                UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-Additional-language", Languages, 0);
                UIActions.Click(driver, "#select-Additional-language + .ceb-dropdown-container");
                UIActions.Click(driver, ".additionalLanguageButtons button");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void HideResponseOption(IWebDriver driver, int QuestionTileOrder, int ScaleOrder)
        {
            try
            {
                IReadOnlyCollection<IWebElement> QuestionEdit = UIActions.FindElements(driver, ".action-question-edit");
                QuestionEdit.ElementAt(QuestionTileOrder - 1).Click();
                Thread.Sleep(10000);
                IReadOnlyCollection<IWebElement> ResponseEdits = UIActions.FindElements(driver, "#btnHideResponse");
                ResponseEdits.ElementAt(ScaleOrder - 1).Click();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}








