﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using Aspose.Cells;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Linq;
using System.Drawing;
using OpenQA.Selenium.Interactions;

namespace Common
{
    public class CommonActions
    {
        public static Aspose.Cells.License _AsposeLicense;

        public static string partiallogFilePath = "\\" + "Log-" + System.DateTime.Now.ToString("MM-dd-yyyy_HHmmss") +
                                                  "." + "txt";
        //private static void SetLicense()
        //{
        //    if (_AsposeLicense == null)
        //    {
        //        _AsposeLicense = new Aspose.Cells.License();
        //        _AsposeLicense.SetLicense("Aspose.Total.lic");
        //    }
        //}

        public static DriverAndLoginDto ReadDriveAndLoginDetails()
        {
            //SetLicense();
            DriverAndLoginDto dto = new DriverAndLoginDto();
            using (Workbook wb = new Workbook(ConfigurationManager.AppSettings["LoginFile"]))
            {
                var valueRow = wb.Worksheets[0].Cells.Rows[1];
                dto.Browser = valueRow.GetCellOrNull(0).Value.ToString();
                dto.Url = valueRow.GetCellOrNull(1).Value.ToString();
                dto.Username = valueRow.GetCellOrNull(2).Value.ToString();
                dto.Password = valueRow.GetCellOrNull(3).Value.ToString();
                dto.Account = valueRow.GetCellOrNull(4).Value.ToString();
            }
            return dto;
        }

        public static string CreateResultsFile(string fileName)
        {
            //SetLicense();
            Workbook wb = new Workbook(ConfigurationManager.AppSettings["ResultsTemplateFile"]);
            string filePath = ConfigurationManager.AppSettings["ResultsPath"] + fileName + ".xlsx";
            wb.Save(filePath);
            return filePath;
        }

        public static void WriteTestResult(string fileName, string testMethod, string result)
        {
            //SetLicense();
            using (Workbook wb = new Workbook(fileName))
            {
                int row = wb.Worksheets[0].Cells.MaxDataRow;
                wb.Worksheets[0].Cells[row + 1, 0].Value = testMethod;
                wb.Worksheets[0].Cells[row + 1, 1].Value = result;
                wb.Save(fileName);
            }
        }

        public static Uri GetExecutorURLFromDriver(RemoteWebDriver driver)
        {
            var executorField = typeof(OpenQA.Selenium.Remote.RemoteWebDriver)
                .GetField("executor",
                    System.Reflection.BindingFlags.NonPublic
                    | System.Reflection.BindingFlags.Instance);

            object executor = executorField.GetValue(driver);

            var internalExecutorField = executor.GetType()
                .GetField("internalExecutor",
                    System.Reflection.BindingFlags.NonPublic
                    | System.Reflection.BindingFlags.Instance);
            object internalExecutor = internalExecutorField.GetValue(executor);

            //executor.CommandInfoRepository
            var remoteServerUriField = internalExecutor.GetType()
                .GetField("remoteServerUri",
                    System.Reflection.BindingFlags.NonPublic
                    | System.Reflection.BindingFlags.Instance);
            var remoteServerUri = remoteServerUriField.GetValue(internalExecutor) as Uri;

            return remoteServerUri;
        }

        public static bool CompareExcels(String filePath1, String filePath2, string logPath, bool bSortAndCompare = false)
        {
            bool IsIdentical = true;
            try
            {
                    var wb1 = new Workbook(filePath1);
                    var wb2 = new Workbook(filePath2);
                    int sheetcount = wb1.Worksheets.Count;

                for (int scnt = 0; scnt < sheetcount; scnt++)
                {
                    Worksheet worksheet1 = wb1.Worksheets[scnt];
                    Worksheet worksheet2 = wb2.Worksheets[scnt];

                    StringBuilder fileContent = new StringBuilder();

                    if (bSortAndCompare)
                    {
                        var ds1 = wb1.DataSorter;

                        ds1.Key1 = 0;
                        ds1.Order1 = SortOrder.Ascending;
                        CellArea ca1 = new CellArea();
                        ca1.StartRow = 0;
                        ca1.StartColumn = 0;
                        ca1.EndRow = worksheet1.Cells.MaxDataRow;
                        ca1.EndColumn = worksheet1.Cells.MaxDataColumn;

                        ds1.Sort(worksheet1.Cells, ca1);

                        var ds2 = wb1.DataSorter;

                        ds2.Key1 = 0;
                        ds2.Order1 = SortOrder.Ascending;
                        CellArea ca2 = new CellArea();
                        ca2.StartRow = 0;
                        ca2.StartColumn = 0;
                        ca2.EndRow = worksheet2.Cells.MaxDataRow;
                        ca2.EndColumn = worksheet2.Cells.MaxDataColumn;

                        ds2.Sort(worksheet2.Cells, ca2);
                    }

                    Cells SourceCells = worksheet1.Cells;
                    Cells DestinationCells = worksheet2.Cells;
                    fileContent.AppendLine("Excel comparision begins for the following files");
                    fileContent.AppendLine("File1:" + filePath1).AppendLine("File2:" + filePath2);

                    for (int i = 0; i < SourceCells.MaxDataRow + 1; i++)
                    {
                        for (int j = 0; j < SourceCells.MaxDataColumn + 1; j++)
                        {
                            string sourceValue = SourceCells[i, j].StringValue.Trim();
                            string destinationValue = DestinationCells[i, j].StringValue.Trim();
                            if (sourceValue != destinationValue)
                            {
                                IsIdentical = false;
                                fileContent.AppendLine("There is a mismatch in the Cell [" + i + "," + j + "] The values are:");
                                fileContent.AppendLine("file1 value:" + sourceValue);
                                fileContent.AppendLine("file2 value:" + destinationValue);
                            }
                        }
                    }
                    fileContent.AppendLine((IsIdentical) ? "The files are identical" : "The files are not identical");
                    fileContent.AppendLine("File Comparision has been Successfully completed");
                    WriteLog(logPath, fileContent.ToString());
                }
            }
            catch (Exception e)
            {
                WriteLog(logPath, "File comparision failed with the following execption:" + e.Message);
                Console.WriteLine(e);
                return false;
            }

            return IsIdentical;
        }

        public static void WriteLog(String filepath, String strLog)
        {


            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;


            string logFilePath = filepath;


            logFilePath = logFilePath + partiallogFilePath;

            logFileInfo = new FileInfo(logFilePath);

            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);

            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();




        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string GetApplicationPath()
        {
            var dllPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().Location);
            return dllPath.Substring(0, dllPath.IndexOf("Driver"));
        }
         
        public static void ClickByElementPosition(IWebElement element , IWebDriver driver)
        {
            Actions ac = new Actions(driver);
            Point className = element.Location;
            int xAxis = className.X;
            int yAxis = className.Y;
            ac.MoveByOffset(xAxis, yAxis).MoveToElement(element).Build().Perform();
        }

        public static void ScrollDown(IWebDriver driver)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("window.scrollBy(0,250)", "");
        }

        public static bool CheckExcelContains(String filePath, string TextToFind, bool bSortAndCompare = false)

        {
            try
            {
                var wb1 = new Workbook(filePath);
                int sheetcount = wb1.Worksheets.Count;
                StringBuilder fileContent = new StringBuilder();
                    Worksheet worksheet1 = wb1.Worksheets[0];
                    Cells SourceCells = worksheet1.Cells;
                    for (int i = 0; i < SourceCells.MaxDataRow + 1; i++)
                    {
                        for (int j = 0; j < SourceCells.MaxDataColumn + 1; j++)
                        {
                            string sourceValue = SourceCells[i, j].StringValue.Trim();
                            if (sourceValue.Contains(TextToFind))
                            {
                                bSortAndCompare = true;
                                fileContent.AppendLine("The text" + TextToFind + " is  in the Cell [" + i + "," + j + "]");
                                fileContent.AppendLine("Excel value:" + sourceValue);
                            break;
                            }
                        }
                    }
                }
                
            catch (Exception e)
            {
                bSortAndCompare = false;
                Console.WriteLine(e);
                return bSortAndCompare;
            }
            return bSortAndCompare;
        }
        public static string GetBetweenStrings(string ActualText, string starttext, string endtext)
        {
            int Start, End;
            if (ActualText.Contains(starttext) && ActualText.Contains(endtext))
            {
                Start = ActualText.IndexOf(starttext, 0) + starttext.Length;
                End = ActualText.IndexOf(endtext, Start);
                return ActualText.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        public static string IfNullThenZero(object value)
        {
            try
            {
                if (value == null || value.ToString() == "")
                {
                    return "0";
                }
                else
                {
                    return value.ToString();
                }
            }
            catch (Exception e)
            {
                return "0";
            }
        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\n ");
            w.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.Write("\t:\t");
            w.Write("{0}", logMessage);
        }
    }
}
