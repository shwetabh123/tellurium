﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace Driver
{
    [TestClass]
    public class SurveyDemographicTest
    {
        public static bool IsChromeDriverAvailable;
        public static bool IsFirefoxDriverAvailable;
        public static bool IsIEDriverAvailable;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        private static string FormName;
        private static byte CopyScenario = 0;


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = "DirectoryDemoDelete"
            };
            //DriverAndLogin = new DriverAndLoginDto
            //{
            //    Url = "https://qa-surveys.cebglobal.com/pulse",
            //    Browser = "Chrome",
            //    Username = "avenkitaraman@cebglobal.com",
            //    Password = "Password@1",
            //    Account = "DirectoryDemoDelete"
            //};
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static void OpenOrCreateForm(RemoteWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SurveyDemographicTest_" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        [TestMethod]
        public void ValidateSurveyDemographicModalIntialState()
        {
            //RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
            }
            var demographicGrid = UIActions.GetElement(driver, "#add-edit-demographics");
            Assert.IsTrue(demographicGrid != null && demographicGrid.Displayed);
            var standardColumns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var labels = standardColumns.Select(c => c.Text).ToList();
            Assert.IsTrue(labels.Contains("Name") && labels.Contains("Email") && labels.Contains("Employee ID"));
            var buttons = driver.FindElements(By.CssSelector("#add-edit-demographics-modal button.btn-primary"));
            Assert.IsTrue(buttons.Any(b => b.Text.Trim() == "Add New Demographic") &&
                          buttons.Any(b => b.Text.Trim() == "Copy from Company Directory") &&
                          buttons.Any(b => b.Text.Trim() == "Copy from Survey"));
            var headers = driver.FindElements(By.CssSelector("#add-edit-demographics thead th"));
            Assert.IsTrue(headers.Any(h => h.Text.Trim().Contains("Survey Demographic Name")) 
                                && headers.Any(h => h.Text.Trim().Contains("Datatype")) 
                                && headers.Any(h => h.Text.Trim().Contains("Available for Filtering"))
                                && headers.Any(h => h.Text.Trim().Contains("Designate as Personal Identifier"))
                                && headers.Any(h => h.Text.Trim().Contains("Available in Upload"))
                                && headers.Any(h => h.Text.Trim().Contains("Mandatory")));
        }

        [TestMethod]
        public void ValidateAddNewDemographicTest()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
            }
            UIActions.Click(driver, "#btn-add-demographic");
            SurveyDemographicActions.AddEditDemographic(driver, "NewDemo1","Number");
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var labels = columns.Select(c => c.Text).ToList();
            Assert.IsTrue(labels.Contains("NewDemo1"));
        }

        [TestMethod]
        public void ValidateAddNewDemographic_Mandatory()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
            }
            UIActions.Click(driver, "#btn-add-demographic");
            SurveyDemographicActions.AddEditDemographic(driver, "", "Number");
            var message = UIActions.GetElement(driver, ".demo-status-area #action-status");
            Assert.IsTrue(message.Text.Trim() == "Demographic name is mandatory");
            SurveyDemographicActions.AddEditCancel(driver);
        }

        [TestMethod]
        public void ValidateAddNewDemographic_SpecialCharacters()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
            }
            UIActions.Click(driver, "#btn-add-demographic");
            SurveyDemographicActions.AddEditDemographic(driver, "Demo@#1", "Number");
            var message = UIActions.GetElement(driver, ".demo-status-area #action-status");
            Assert.IsTrue(message.Text.Trim() == "Please enter a valid demographic name.");
            SurveyDemographicActions.AddEditCancel(driver);
        }

        [TestMethod]
        public void ValidateEditDemographicTest()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
            }
            UIActions.Click(driver, "#btn-add-demographic");
            SurveyDemographicActions.AddEditDemographic(driver, "NewDemo2", "Text");
            SurveyDemographicActions.EditDemographicsActions(driver,"NewDemo2");
            SurveyDemographicActions.AddEditDemographic(driver,"NewDemo2_Edit","Text");
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var labels = columns.Select(c => c.Text).ToList();
            Assert.IsTrue(labels.Contains("NewDemo2_Edit"));
        }

        [TestMethod]
        public void ValidateCopyFromCompanyDirectory()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
            }
            SurveyDemographicActions.CopyDirectoryDemographics(driver, new List<string>() { "City", "Region" });
            var message = UIActions.GetElement(driver, "#divStatusForAddEditDemos #action-status");
            if (CopyScenario == 0)
            {
                Assert.IsTrue(message.Text.Trim() == "Demographics copied successfully.");
                CopyScenario = 1;
            }
            else
                Assert.IsTrue(message.Text.Trim() ==
                              "Demographics copied successfully. City cannot be added to the survey as it already exists.");
        }

        [TestMethod]
        public void ValidateCopyFromOtherSurvey()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
            }
            SurveyDemographicActions.CopySurveyDemographics(driver, "Mint-O-Fresh", new List<string>() { "Gender", "City" });
            var message = UIActions.GetElement(driver, "#divStatusForAddEditDemos #action-status");
            if (CopyScenario == 1)
                Assert.IsTrue(message.Text.Trim() == "Demographics copied successfully. City cannot be added to the survey as it already exists.");
            else
            {
                Assert.IsTrue(message.Text.Trim() == "Demographics copied successfully.");
                CopyScenario = 1;
            }
        }

        [TestMethod]
        public void ValidateReorderDemographic_ToPosition()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                SurveyDemographicActions.ReorderDemographics(driver, "City","1");
                var row = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr"))[4];
                var columns = row.FindElements(By.CssSelector("td label"));
                var labels = columns.Select(c => c.Text).ToList();
                Assert.IsTrue(labels.Contains("City"));
            }
            else
                Assert.Fail();
        }

        [TestMethod]
        public void ValidateDeleteDemographic()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if(driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
                UIActions.Click(driver, "#btn-add-demographic");
                SurveyDemographicActions.AddEditDemographic(driver, "NewDemo1", "Text");
            }
            SurveyDemographicActions.DeleteDemographics(driver, "NewDemo1");
            var message = UIActions.GetElement(driver, "#divStatusForAddEditDemos #action-status");
            Assert.IsTrue(message.Text.Trim() == "Demographic deleted successfully");
        }

        [TestMethod]
        public void ValidateReorderDemographic_ToEnd()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                SurveyDemographicActions.ReorderDemographics(driver, "City", "-1");
                var row = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr")).LastOrDefault();
                var columns = row.FindElements(By.CssSelector("td label"));
                var labels = columns.Select(c => c.Text).ToList();
                Assert.IsTrue(labels.Contains("City"));
            }
            else
                Assert.Fail();
        }

        [TestMethod]
        public void ValidatePIIToggle()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
                UIActions.Click(driver, "#btn-add-demographic");
                SurveyDemographicActions.AddEditDemographic(driver, "NewDemo1", "Text");
            }
            SurveyDemographicActions.TogglePII(driver, "NewDemo1");
            Assert.IsFalse(SurveyDemographicActions.GetToggleFiltering(driver, "NewDemo1"));
        }

        [TestMethod]
        public void ValidateFilteringToggle()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if(driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
                UIActions.Click(driver, "#btn-add-demographic");
                SurveyDemographicActions.AddEditDemographic(driver, "NewDemo1", "Text");
            }
            SurveyDemographicActions.ToggleFiltering(driver, "NewDemo1");
            Assert.IsFalse(SurveyDemographicActions.GetTogglePII(driver, "NewDemo1"));
          
        }

        [TestMethod]
        public void ValidateMandatoryToggle()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if(driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
                UIActions.Click(driver, "#btn-add-demographic");
                SurveyDemographicActions.AddEditDemographic(driver, "NewDemo1", "Text");
            }
            SurveyDemographicActions.ToggleMandatory(driver, "NewDemo1");
            Assert.IsTrue(SurveyDemographicActions.GetToggleUpload(driver, "NewDemo1"));
        }


        [TestMethod]
        public void ValidateUploadToggle()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if(driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                OpenOrCreateForm(driver);
                SurveyDemographicActions.OpenDemographicModal(driver);
                Thread.Sleep(2000);
                UIActions.Click(driver, "#btn-add-demographic");
                SurveyDemographicActions.AddEditDemographic(driver, "NewDemo1", "Text");
            }
            SurveyDemographicActions.ToggleUpload(driver, "NewDemo1");
            Assert.IsFalse(SurveyDemographicActions.GetToggleMandatory(driver, "NewDemo1"));
        }

        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }
    }
}
