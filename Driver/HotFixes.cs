﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Data;
using System.Threading;


namespace Driver
{

    [TestClass]


    class HotFixes
    {

        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DBConnectionStringDTO DBConnectionParameters1;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString(),
            };
            //DBConnectionParameters = new DBConnectionStringDTO
            //{
            //    userName = "pulseqa",
            //    password = "password-1",
            //    TCESserverName = "USA-QIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-QIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            //DBConnectionParameters1 = new DBConnectionStringDTO
            //{
            //    userName = "pulsestg",
            //    password = "password-1",
            //    TCESserverName = "USA-SIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-SIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString(),
                IntegratedSecurity = context.Properties["IntegratedSecurity"].ToString(),
                PersistSecurityInfo = context.Properties["PersistSecurityInfo"].ToString()
            };
        }
        //shwetabh srivastava--------added capabilities for download file in specified location at run time//
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    // FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    //     service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    //   driver = new FirefoxDriver();//
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }




        [TestMethod]
        public void PS47039()
        {



            DriverAndLogin.Account = "AutomationReporting new";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //Survey
            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Black Pearl");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "City");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "City");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Bhopal");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Bhopal");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);

            AnalyzeAction.ResultsTab(driver);

            Thread.Sleep(30000);





            AnalyzeAction.Exportas(driver);

            Thread.Sleep(5000);



            AnalyzeAction.ExportasPPTPresentation(driver);

            Thread.Sleep(5000);



            AnalyzeAction.Exportbtnnew(driver);

            Thread.Sleep(70000);

            AnalyzeAction.ResultsReport(driver);

            Thread.Sleep(30000);



            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);


            //verify  All Time period is present  in ppt at slide 6

            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "0", "All Time period");

            driver.Quit();







        }





        [TestMethod]
        public void PS47035()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "City");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "City");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Bhopal");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Bhopal");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



        }





    }
}
