﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using Common;
using Microsoft.Office.Interop.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Driver
{
    [TestClass]
    public class AuthorStyleSheetTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static StyleSheetParametersDTO StyleSheetParameters;
        private static string FormName;
        public static string FileName;
        public static string styleSheetname;
        public static string updated_stylesheetname = styleSheetname + "Edited";

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static void OpenOrCreateForm(RemoteWebDriver driver, string stylesheet = null)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateFormwithstylesheet(driver, FormName, styleSheetname);
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToForm(string stylesheet = null)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                 OpenOrCreateForm(driver, styleSheetname);
            }
            else if (driver.Url.Contains("ManageForms"))
                OpenOrCreateForm(driver, styleSheetname);
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);
            }
            else
            {
                OpenOrCreateForm(driver, styleSheetname);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToManageForms()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
            }
            else
            {
                AuthorActions.NavigateToAuthor(driver);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToHome()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            else if (driver.Title.Contains("Home Page - Pulse"))
                Thread.Sleep(2000);
            else
                NavigateToHome(driver);
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToHomeicon()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            else
                NavigateToHomeicon(driver);
            return driver;

        }


        public static RemoteWebDriver SetDriverAndNavigateToDistribute()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        }

        public static void NavigateToHome(RemoteWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "ul.sidebar-menu li span");
            navbuttons.FirstOrDefault(b => b.Text == "Home").Click();
        }

        public static void NavigateToHomeicon(RemoteWebDriver driver)
        {
            UIActions.clickwithXapth(driver, "//a[@id='show-home']");
        }



        public static void NavigateToAuthor(RemoteWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Author").Click();
        }

        /// <summary>
        /// Test cases starts from here
        /// </summary>
        /// 


        [TestMethod]
                public void Allcreatestylesheetandpreviewmethods()
        {
            AACreateNewStyleSheet();
            BBAddingQuestions();
            IIValidateEditStyleSheetIncludingName();
            JJValidateEditStyleSheetExcludingName();
            KKValidateStyleSheetForExistingFormAfterEditInPreview();
            LLValidateDeleteStyleSheet();
                        RemovalofDeletedStyleSheetAccountdefault();
                        RemovalofDeletedStyleSheetnewform();
            DDValidateStyleSheetForTranslationInPreview();
                        delstylesheetretain();
            publishdelstylesheet();

        }

        [TestMethod]
        public void AACreateNewStyleSheet()
        {
            styleSheetname = "SelenuimTestSheet" + Guid.NewGuid().ToString().Substring(0, 10);
            RemoteWebDriver driver = SetDriverAndNavigateToHome();
            Thread.Sleep(5000);
            AccountDefaults.NavigateToAccountDefaults(driver);
            AccountDefaults.CreateNewStyleSheet(driver, styleSheetname, new StyleSheetParametersDTO
            {
                BackgroundColor = "#7CBB00",
                SecondaryBackgroundColor = "#00A1F1",
                TitleBarColor = "#F65314",
                TitleFontColor = "#FFFFFF",
                SectionHeadingFontColor = "#00A1F1",
                SelectionColor = "#FFBB00",
                SelectionOutlineColor = "#F65314",
                ScaleLabelFontColor = "#00A1F1"
            });
            var messageArea = UIActions.GetElementWithWait(driver, ".alert-success", 30);
            Assert.AreEqual("Created " + styleSheetname + "!", messageArea.Text.Trim());
        }

        [TestMethod]
        public void BBAddingQuestions()
        {
           
                RemoteWebDriver driver = SetDriverAndNavigateToForm(styleSheetname);
                Thread.Sleep(1000);
                FormName = UIActions.FindElement(driver, "#formHeader").Text.Trim();
                AuthorActions.AddRatingScaleQuestion(driver, "How do you rate this Automation framework?");
                var messageArea_rsqn = UIActions.GetElementWithWait(driver, ".status-area", 30);
                bool rsqn = messageArea_rsqn.Text.Trim().Equals("Question added successfully.");
                Thread.Sleep(2000);

                AuthorActions.EditPageName(driver, "Page 1");
                Thread.Sleep(2000);
                AuthorActions.EditSectionName(driver, "Section 1");
                Thread.Sleep(2000);

                var sectionList = UIActions.FindElements(driver, ".section-list");
                AuthorActions.AddSection(driver);
                var newSectionList = UIActions.FindElements(driver, ".section-list");
                bool seccnt = newSectionList.Count > sectionList.Count;
                Thread.Sleep(2000);

                string[] questions_mirs = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
                string[] scaleHeaders = { "Strongly Disagree", "Disagree", "Neutral", "Neither Agree Nor Disagree", "Agree", "Strongly Agree" };
                AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below new automation frameworks", questions_mirs, "1-6", scaleHeaders);
                var messageArea_mirs = UIActions.GetElementWithWait(driver, ".status-area", 30);
                bool mirs = messageArea_mirs.Text.Trim().Equals("Question added successfully.");
                Thread.Sleep(2000);

                var pageList = UIActions.FindElements(driver, ".page-list");
                AuthorActions.AddPage(driver);
                var newPageList = UIActions.FindElements(driver, ".page-list");
                bool pgcnt = newPageList.Count > pageList.Count;
                Thread.Sleep(2000);

                string[] responseOptions_mcq = { "HR", "IT", "Finance", "Sales" };
                AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions_mcq);
                var messageArea_mcq = UIActions.GetElementWithWait(driver, ".status-area", 30);
                bool mcq = messageArea_mcq.Text.Trim().Equals("Question added successfully.");
                Thread.Sleep(2000);

                string[] questions_mcqmi = { "First", "Second", "Third" };
                string[] responseOptions_mcqmi = { "HR", "IT", "Finance", "Sales" };
                AuthorActions.AddMultipleChoiceMultiItemQuestion(driver, "Choose your favorite Departments", questions_mcqmi, responseOptions_mcqmi);
                var messageArea_mcqmi = UIActions.GetElementWithWait(driver, ".status-area", 30);
                bool mcqmi = messageArea_mcqmi.Text.Trim().Equals("Question added successfully.");
                Thread.Sleep(2000);

                AuthorActions.AddSection(driver);
                Thread.Sleep(2000);

                string[] responseOptions_dd = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
                AuthorActions.AddDropDownQuestion(driver, "Which of the following best describes your level in the organization?", responseOptions_dd);
                var messageArea_dd = UIActions.GetElementWithWait(driver, ".status-area", 30);
                bool dd = messageArea_dd.Text.Trim().Equals("Question added successfully.");
                Thread.Sleep(2000);

                AuthorActions.AddCommentQuestion(driver, "Please take this time to let us know anything about your benefits that you have not already had a chance to express.");
                var messageArea_c = UIActions.GetElementWithWait(driver, ".status-area", 30);
                bool com = messageArea_c.Text.Trim().Equals("Question added successfully.");
                Thread.Sleep(2000);

              Assert.IsTrue(rsqn && seccnt && mirs && pgcnt && mcq && mcqmi && dd && com);
          
         //    RemoteWebDriver driver = SetDriverAndNavigateToForm(stylesheetname);
                Thread.Sleep(3000);
            UIActions.Click(driver, "#btnFullPreview");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(5000);
            bool firstpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
            {
                BackgroundColor = "#7CBB00",
                SecondaryBackgroundColor = "#00A1F1",
                TitleBarColor = "#F65314",
                TitleFontColor = "#FFFFFF",
                SectionHeadingFontColor = "#00A1F1",
                SelectionColor = "#FFBB00",
                SelectionOutlineColor = "#F65314",
                ScaleLabelFontColor = "#00A1F1"
            });
          //  UIActions.Click(driver, "#btnNext");
                       // UIActions.MouseHoverToElementAndClick(driver, "#btnNext");
                bool secondpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
            {
                //BackgroundColor = "#7CBB00",
                //SecondaryBackgroundColor = "#00A1F1",
                //TitleBarColor = "#F65314",
                //TitleFontColor = "#FFFFFF",
                //SectionHeadingFontColor = "#00A1F1",
                SelectionColor = "#FFBB00",
                SelectionOutlineColor = "#F65314",
                //ScaleLabelFontColor = "#00A1F1"
            });
            Assert.IsTrue(firstpage && secondpage);
            driver.SwitchTo().Window(driver.WindowHandles.First());

        }
               


        [TestMethod]
        public void IIValidateEditStyleSheetIncludingName()
        {
            try
            {
                updated_stylesheetname = "SelenuimTestSheet" + Guid.NewGuid().ToString().Substring(0, 10);

                RemoteWebDriver driver = SetDriverAndNavigateToHomeicon();

                               Thread.Sleep(5000);
                AccountDefaults.NavigateToAccountDefaults(driver);
                AccountDefaults.EditStyleSheet(driver, styleSheetname, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#2D04CF",
                    SecondaryBackgroundColor = "#FF0000",
                    TitleBarColor = "#FF0000",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#FF0000",
                    SelectionColor = "#FF0000",
                    SelectionOutlineColor = "#FF0000",
                    ScaleLabelFontColor = "#FF0000"
                }
                , updated_stylesheetname);
                var messageArea = UIActions.GetElementWithWait(driver, ".alert-success",30);
                Assert.AreEqual("Updated : "+updated_stylesheetname + "!", messageArea.Text.Trim());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void JJValidateEditStyleSheetExcludingName()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToHome();
                Thread.Sleep(5000);
                AccountDefaults.NavigateToAccountDefaults(driver);
                AccountDefaults.EditStyleSheet(driver, updated_stylesheetname, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#2D04CF",
                    SecondaryBackgroundColor = "#FF0000",
                    TitleBarColor = "#FF0000",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#FF0000",
                    SelectionColor = "#FF0000",
                    SelectionOutlineColor = "#FF0000",
                    ScaleLabelFontColor = "#FF0000"
                });
                var messageArea = UIActions.GetElementWithWait(driver, ".alert-success", 10);
                Assert.AreEqual("Updated : "+updated_stylesheetname + "!", messageArea.Text.Trim());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();

            }
        }
        [TestMethod]

        public void KKValidateStyleSheetForExistingFormAfterEditInPreview()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToManageForms();
                Thread.Sleep(5000);

                string NewFormname = FormName.Substring(0, 52);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, NewFormname);
                
                UIActions.Click(driver, "#btnFullPreview");
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(5000);
                bool firstpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#2D04CF",
                    SecondaryBackgroundColor = "#FF0000",
                    TitleBarColor = "#FF0000",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#FF0000",
                    SelectionColor = "#FF0000",
                    SelectionOutlineColor = "#FF0000",
                    ScaleLabelFontColor = "#FF0000"
                });
                UIActions.Click(driver, "#btnNext");
                Thread.Sleep(5000);
                bool secondpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    SelectionColor = "#FF0000",
                    SelectionOutlineColor = "#FF0000",
                });
                Assert.IsTrue(firstpage && secondpage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void LLValidateDeleteStyleSheet()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToHome();
                Thread.Sleep(5000);
                AccountDefaults.NavigateToAccountDefaults(driver);
                AccountDefaults.DeleteStyleSheet(driver, updated_stylesheetname);
                var messageArea = UIActions.GetElementWithWait(driver, ".alert-success #action-status", 30);
                Assert.AreEqual("The stylesheet has been deleted and will no longer be available when creating a new account. No stylesheet changes have been made to existing surveys, so you might still need to update stylesheets for any active surveys.", messageArea.Text.Trim());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]

        public void RemovalofDeletedStyleSheetAccountdefault()
        {
            try
            {

                RemoteWebDriver driver = SetDriverAndNavigateToHome();
                Thread.Sleep(5000);
                AccountDefaults.NavigateToAccountDefaults(driver);
                AccountDefaults.deletestylesheetnotpresent(driver, updated_stylesheetname);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void RemovalofDeletedStyleSheetnewform()
        {
            try
            {

                RemoteWebDriver driver = SetDriverAndNavigateToHome();
                Thread.Sleep(5000);
                AccountDefaults.Launchformfromhomepage(driver);
                Thread.Sleep(3000);
                AccountDefaults.deletestylesheetnotpresent(driver, updated_stylesheetname);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

       

        [TestMethod]
        public void DDValidateStyleSheetForTranslationInPreview()
        {
            try
            {
                // DriverAndLogin.Account = "Author kkvp";
                RemoteWebDriver driver = SetDriverAndNavigateToManageForms();
                Thread.Sleep(5000);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, "Author stylesheetupdated");
                UIActions.Click(driver, "#btnFullPreview");
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(5000);
                UIActions.SelectInCEBDropdownByValue(driver, "#selectLanguageOnHeader", "JA_JP");
                Thread.Sleep(5000);
                bool firstpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#7CBB00",
                    SecondaryBackgroundColor = "#00A1F1",
                    TitleBarColor = "#F65314",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#00A1F1",
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                    ScaleLabelFontColor = "#00A1F1"
                });
                UIActions.MouseHoverToElementAndClick(driver, "//*[@id='btnNext']/div");
                bool secondpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                });
                Assert.IsTrue(firstpage && secondpage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void delstylesheetretain()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms();
            Thread.Sleep(5000);
            var formBuilder = AuthorActions.SearchAndOpenForm(driver, "FormName");
            AccountDefaults.surveysettings(driver);
            UIActions.FindelementWithXpath(driver, "//button[contains(@title,'deleted')]");
            UIActions.clickwithXapth(driver, "//button[@id='btnFormSettingCancel']");
        }

        [TestMethod]
        public void publishdelstylesheet()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms();
            Thread.Sleep(5000);
            var formBuilder = AuthorActions.SearchAndOpenForm(driver, "FormName");
            AuthorActions.PublishEditedForm(driver);

            var messageArea = UIActions.FindElementsWithXpath(driver, "(//span[contains(.,'PUBLISHED')])[2])");
            Assert.Equals("PUBLISHED", messageArea);

        }
        [TestMethod]
        public void EEValidateStyleSheetForUniqueLinkParticipant()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                var summary = DistributeActions.SearchAndLaunchDistributionSummary(driver, "Author stylesheetupdated");
                DistributeActions.CopyUniqueLink(driver);
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(5000);
                bool firstpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#7CBB00",
                    SecondaryBackgroundColor = "#00A1F1",
                    TitleBarColor = "#F65314",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#00A1F1",
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                    ScaleLabelFontColor = "#00A1F1"
                });
               UIActions.MouseHoverToElementAndClick(driver, "//*[@id='btnNext']/div");
             
                Thread.Sleep(5000);
                bool secondpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                });
                Assert.IsTrue(firstpage && secondpage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [TestMethod]
        public void FFValidateStyleSheetForTranslationInUniqueLinkParticipant()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                var summary = DistributeActions.SearchAndLaunchDistributionSummary(driver, "Author stylesheetupdated");
                DistributeActions.CopyUniqueLink(driver);
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(5000);
                UIActions.SelectInCEBDropdownByValue(driver, "#selectLanguageOnHeader", "JA_JP");
                Thread.Sleep(5000);
                bool firstpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#7CBB00",
                    SecondaryBackgroundColor = "#00A1F1",
                    TitleBarColor = "#F65314",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#00A1F1",
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                    ScaleLabelFontColor = "#00A1F1"
                });
                UIActions.Click(driver, "#btnNext");
                Thread.Sleep(5000);
                bool secondpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                });
                Assert.IsTrue(firstpage && secondpage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [TestMethod]
        public void GGValidateStyleSheetForOpenLinkParticipant()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                var summary = DistributeActions.SearchAndOpenDistribution(driver, "Author stylesheetupdated");
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(5000);
                bool firstpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#7CBB00",
                    SecondaryBackgroundColor = "#00A1F1",
                    TitleBarColor = "#F65314",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#00A1F1",
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                    ScaleLabelFontColor = "#00A1F1"
                });
                UIActions.Click(driver, "#btnNext");
                Thread.Sleep(5000);
                bool secondpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                });
                Assert.IsTrue(firstpage && secondpage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [TestMethod]
        public void HHValidateStyleSheetForTranslationInOpenLinkParticipant()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                var summary = DistributeActions.SearchAndOpenDistribution(driver, "Author stylesheetupdated");
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);
                Thread.Sleep(5000);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(5000);
                UIActions.SelectInCEBDropdownByText(driver, ".ceb-dropdown-container", "简体中文");
                Thread.Sleep(5000);
                bool firstpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#7CBB00",
                    SecondaryBackgroundColor = "#00A1F1",
                    TitleBarColor = "#F65314",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#00A1F1",
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                    ScaleLabelFontColor = "#00A1F1"
                });
                UIActions.Click(driver, "#btnNext");
                Thread.Sleep(5000);
                bool secondpage = AuthorActions.CheckStyleSheetValues(driver, new StyleSheetParametersDTO
                {
                    SelectionColor = "#FFBB00",
                    SelectionOutlineColor = "#F65314",
                });
                Assert.IsTrue(firstpage && secondpage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

           

        [TestMethod]
        public void validatedeletedStyleSheetInPreview()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToHome();
                Thread.Sleep(5000);
                AccountDefaults.NavigateToAccountDefaults(driver);
                AccountDefaults.EditStyleSheet(driver, styleSheetname, new StyleSheetParametersDTO
                {
                    BackgroundColor = "#2D04CF",
                    SecondaryBackgroundColor = "#FF0000",
                    TitleBarColor = "#FF0000",
                    TitleFontColor = "#FFFFFF",
                    SectionHeadingFontColor = "#FF0000",
                    SelectionColor = "#FF0000",
                    SelectionOutlineColor = "#FF0000",
                    ScaleLabelFontColor = "#FF0000"
                });
                var messageArea = UIActions.GetElementWithWait(driver, ".alert-success", 30);
                Assert.AreEqual("Updated " + styleSheetname + "!", messageArea.Text.Trim());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }
        

     
        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }
    }
}
