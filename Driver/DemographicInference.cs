﻿using Common;

using Common;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Data;
using System.Threading;



namespace Driver
{

    [TestClass]



    public class DemographicInference
    {

        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DBConnectionStringDTO DBConnectionParameters1;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString(),
            };

            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString(),
                IntegratedSecurity = context.Properties["IntegratedSecurity"].ToString(),
                PersistSecurityInfo = context.Properties["PersistSecurityInfo"].ToString()
            };
        }
        //shwetabh srivastava--------added capabilities for download file in specified location at run time//
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    // FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    //     service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    //   driver = new FirefoxDriver();//
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }






        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionSingleDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionSingleDistributionCompletedthesurvey_Scenario1and2()
        {







            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);







            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13
            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);

            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);




        }




        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionMultiDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);





            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13
            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);

            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionMultiDistributionCompletedthesurvey_Scenario1and2()
        {







            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);





            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13

      

            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);

            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);







        }









        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is Suppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);






        }


 

        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey_Scenario1and2()
        {




            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
        Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);








            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);

            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);






            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


        Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


        Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is Suppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Manufacturing']");


        Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


        Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


        Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);





}

        
               [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
        Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);

            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);






            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


        Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


        Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is Suppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Manufacturing']");


        Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


        Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


        Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }











               [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey_Scenario1and2()
        {





            DriverAndLogin.Account = "Automation Suppression";
        DriverAndLogin.Browser = "Chrome";



        IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
        Thread.Sleep(10000);
        WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);







            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
        Thread.Sleep(10000);

        SuppressionActions.ClickAccountDefaults(driver);
        Thread.Sleep(10000);
        SuppressionActions.Clickcompletedthesurvey(driver);
        Thread.Sleep(10000);
        SuppressionActions.SaveAnalysisdefaults(driver);
        Thread.Sleep(10000);




        AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



        Thread.Sleep(80000);

        //AnalyzeAction.ClickonFiltersComparisons(driver);


        //Thread.Sleep(10000);



        AnalyzeAction.ClickAddFilter(driver);


        Thread.Sleep(30000);




        AnalyzeAction.searchsegmentcategory(driver, "Distribution");


        Thread.Sleep(10000);


        AnalyzeAction.searchsegement(driver);


        Thread.Sleep(10000);


        AnalyzeAction.ClickChip(driver, "Distribution");


        Thread.Sleep(5000);

        AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


        Thread.Sleep(5000);

        AnalyzeAction.searchsegement(driver);


        Thread.Sleep(5000);


        AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



        Thread.Sleep(10000);



        AnalyzeAction.Apply(driver);


        Thread.Sleep(30000);





        AnalyzeAction.ClickAddFilter(driver);


        Thread.Sleep(30000);




        AnalyzeAction.searchsegmentcategory(driver, "Distribution");


        Thread.Sleep(10000);


        AnalyzeAction.searchsegement(driver);


        Thread.Sleep(10000);


        AnalyzeAction.ClickChip(driver, "Distribution");


        Thread.Sleep(5000);

        AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


        Thread.Sleep(5000);

        AnalyzeAction.searchsegement(driver);


        Thread.Sleep(5000);


        AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



        Thread.Sleep(10000);



        AnalyzeAction.Apply(driver);


        Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);







            AnalyzeAction.ClickAddFilter(driver);


        Thread.Sleep(30000);


        //type additional filters
        //search additional filters

        AnalyzeAction.searchsegmentcategory(driver, "function");


        Thread.Sleep(10000);




        AnalyzeAction.searchsegement(driver);


        Thread.Sleep(10000);




        AnalyzeAction.ClickChip(driver, "function");




        Thread.Sleep(10000);




        //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

        IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


        Assert.IsTrue(Chip1.Displayed);


        Thread.Sleep(5000);

        //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

        IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


        Assert.IsTrue(Chip2.Displayed);


        Thread.Sleep(5000);



        //verify demovalue-Manufacturing is Suppressed in  LeftsideDemo dropdown

        IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Manufacturing']");


        Assert.IsTrue(Chip3.Displayed);


        Thread.Sleep(5000);



        //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

        IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


        Assert.IsTrue(Chip4.Displayed);


        Thread.Sleep(5000);



        //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

        IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


        Assert.IsTrue(Chip5.Displayed);


        Thread.Sleep(5000);



        driver.Quit();
        Thread.Sleep(5000);








            

    }





        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionSingleDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=3
            //Demographic Threshold to Prevent Inference of Results   =3


            ReportDataSuppression.UpdateGroupMinimumSizenew("3", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("3", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("3", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("3", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("3", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "business group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "business group");




            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Economy Section");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Economy Section");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);


            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }







        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionMultiDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=5
            //Demographic Threshold to Prevent Inference of Results   =5


            ReportDataSuppression.UpdateGroupMinimumSizenew("5", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("5", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("5", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("5", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("5", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "business group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "business group");




            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Economy Section");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Economy Section");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);


            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=2
            //Demographic Threshold to Prevent Inference of Results   =2


            ReportDataSuppression.UpdateGroupMinimumSizenew("2", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("2", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("2", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("2", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("2", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);






            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);








            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "business group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "business group");




            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Economy Section");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Economy Section");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);


            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);




            //verify demovalue-Research and Development is Suppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Research and Development']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is Suppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);




            driver.Quit();
            Thread.Sleep(5000);

        }







        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=4
            //Demographic Threshold to Prevent Inference of Results   =4


            ReportDataSuppression.UpdateGroupMinimumSizenew("4", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("4", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("4", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("4", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("4", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);






            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "business group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "business group");




            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Economy Section");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Economy Section");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);


            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



     



            //verify demovalue-Manufacturing is Suppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);





            //verify demovalue-Research and Development is Suppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Research and Development']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);




            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);

            driver.Quit();
            Thread.Sleep(5000);

        }


        

        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionSingleDistributionAnsweratleast1question_Scenario5()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Data Checks Survey 1");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "SA - Single Distribution with one participant");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "SA - Single Distribution with one participant");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "03/10/2017 - 04/10/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(80000);

            //Insight -Strengths


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var datedemo1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelectorAll('label'))[5]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo1.Text);

            Thread.Sleep(10000);


            //Insight -Opportunity

            var datedemo2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[7]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo2.Text);

            Thread.Sleep(10000);


            //Click on Demographics Report tab

            IWebElement Demographics = UIActions.FindElementWithXpath(driver, " //a[@href='#demographic-report']");
            Demographics.Click();

            Thread.Sleep(80000);


            //Demographic Report 


            var datedemo3 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[36]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo3.Text);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "01/10/2017 - 02/10/2017");

            Thread.Sleep(20000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);
            Thread.Sleep(80000);

            //Insight -Strengths

            var datedemo4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#strength-data').querySelectorAll('.progress'))[0]");

            Assert.IsTrue(datedemo4.Displayed);




            Thread.Sleep(10000);


            //DrillDown

            var datedemo5 = (IWebElement)js.ExecuteScript("return      (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelector('#drilldownContainer').querySelectorAll('.progress'))[0]");



            Assert.IsTrue(datedemo5.Displayed);


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionMultiDistributionAnsweratleast1question_Scenario5()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Data Checks Survey 1");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "SA - Single Distribution with one participant");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "SA - Single Distribution with one participant");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);
            

            AnalyzeAction.txtDateDemoPicker(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "03/10/2017 - 04/10/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(80000);

            //Insight -Strengths

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var datedemo1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelectorAll('label'))[5]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo1.Text);

            Thread.Sleep(10000);


            //Insight -Opportunity

            var datedemo2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[7]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo2.Text);

            Thread.Sleep(10000);


            //Click on Demographics Report tab

            IWebElement Demographics = UIActions.FindElementWithXpath(driver, " //a[@href='#demographic-report']");
            Demographics.Click();
            Thread.Sleep(80000);


            //Demographic Report 


            var datedemo3 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[36]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo3.Text);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "01/10/2017 - 02/10/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(80000);
            //Insight -Strengths

            var datedemo4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#strength-data').querySelectorAll('.progress'))[0]");

            Assert.IsTrue(datedemo4.Displayed);




            Thread.Sleep(10000);


            //DrillDown

            var datedemo5 = (IWebElement)js.ExecuteScript("return      (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelector('#drilldownContainer').querySelectorAll('.progress'))[0]");



            Assert.IsTrue(datedemo5.Displayed);


            Thread.Sleep(10000);

            driver.Quit();
            Thread.Sleep(5000);

        }











        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1questionn_Scenario5()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Data Checks Survey 1");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "SA - Single Distribution with one participant");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "SA - Single Distribution with one participant");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "03/10/2017 - 04/10/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            //Insight -Strengths

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var datedemo1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelectorAll('label'))[5]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo1.Text);

            Thread.Sleep(10000);


            //Insight -Opportunity

            var datedemo2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[7]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo2.Text);

            Thread.Sleep(10000);



            //Click on Demographics Report tab
            IWebElement Demographics = UIActions.FindElementWithXpath(driver, " //a[@href='#demographic-report']");
            Demographics.Click();


            Thread.Sleep(20000);

            //Demographic Report 


            var datedemo3 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[36]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo3.Text);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/10/2017 - 02/10/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            //Insight -Strengths

            var datedemo4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#strength-data').querySelectorAll('.progress'))[0]");

            Assert.IsTrue(datedemo4.Displayed);




            Thread.Sleep(10000);


            //DrillDown

            var datedemo5 = (IWebElement)js.ExecuteScript("return      (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelector('#drilldownContainer').querySelectorAll('.progress'))[0]");



            Assert.IsTrue(datedemo5.Displayed);


            Thread.Sleep(10000);





            driver.Quit();
            Thread.Sleep(5000);

        }












        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario5()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Data Checks Survey 1");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "SA - Single Distribution with one participant");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "SA - Single Distribution with one participant");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "03/10/2017 - 04/10/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            //Insight -Strengths

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var datedemo1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelectorAll('label'))[5]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo1.Text);

            Thread.Sleep(10000);


            //Insight -Opportunity

            var datedemo2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[7]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo2.Text);

            Thread.Sleep(10000);



            //Click on Demographics Report tab

            IWebElement Demographics = UIActions.FindElementWithXpath(driver, " //a[@href='#demographic-report']");
            Demographics.Click();


            Thread.Sleep(20000);

            //Demographic Report 


            var datedemo3 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('label'))[36]");



            Assert.AreEqual("Not enough data is available with the current filters applied.", datedemo3.Text);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "Hire Date");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "Hire Date");



            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.txtDateDemoPicker(driver, "01/10/2017 - 02/10/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            //Insight -Strengths

            var datedemo4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#strength-data').querySelectorAll('.progress'))[0]");

            Assert.IsTrue(datedemo4.Displayed);




            Thread.Sleep(10000);


            //DrillDown

            var datedemo5 = (IWebElement)js.ExecuteScript("return      (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelector('#drilldownContainer').querySelectorAll('.progress'))[0]");



            Assert.IsTrue(datedemo5.Displayed);


            Thread.Sleep(10000);



            driver.Quit();
            Thread.Sleep(5000);

        }










        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionSingleDistributionAnsweratleast1question_Scenario3()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionMultiDistributionAnsweratleast1question_Scenario3()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }











        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1questionn_Scenario3()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }












        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario3()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }












        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionSingleDistributionAnsweratleast1question_Scenario4()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionMultiDistributionAnsweratleast1question_Scenario4()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }











        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1questionn_Scenario4()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }












        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario4()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=1
            //Demographic Threshold to Prevent Inference of Results   =1


            ReportDataSuppression.UpdateGroupMinimumSizenew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("1", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("1", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Ey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinDemographicReportSingleDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



      



            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //Click on Function in dropdown

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var SelectDemographicDropdowninDemographicReport1 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[23].querySelector('input')");
            SelectDemographicDropdowninDemographicReport1.Click();

            Thread.Sleep(10000);


            var ClickondotdotDemographicReport1 = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport1.Click();


            Thread.Sleep(10000);


            //"Information Technology
           



            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");

            Assert.AreEqual("--", demovalue1.Text);


           // Sales

           var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[3]");

            Assert.AreEqual("--", demovalue2.Text);




               // Research and Development


            var demovalue3 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[6].querySelectorAll('td'))[0]");

            Assert.AreNotEqual("--", demovalue3.Text);

            //Consulting Services

            var demovalue4 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[6].querySelectorAll('td'))[1]");

            Assert.AreNotEqual("--", demovalue4.Text);



         //   Manufacturing

              var demovalue5 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[6].querySelectorAll('td'))[2]");

            Assert.AreNotEqual("--", demovalue5.Text);


            driver.Quit();
            Thread.Sleep(5000);

        }






        [TestMethod]
        public void ValidateInferenceinDemographicReportMultiDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);







            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            //Click on Function in dropdown
            var SelectDemographicDropdowninDemographicReport1 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[23].querySelector('input')");
            SelectDemographicDropdowninDemographicReport1.Click();

            Thread.Sleep(10000);


            var ClickondotdotDemographicReport1 = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport1.Click();


            Thread.Sleep(10000);


            //"Information Technology




            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");

            Assert.AreEqual("--", demovalue1.Text);


            // Sales

            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[3]");

            Assert.AreEqual("--", demovalue2.Text);




            // Research and Development


            var demovalue3 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[6].querySelectorAll('td'))[0]");

            Assert.AreNotEqual("--", demovalue3.Text);

            //Consulting Services

            var demovalue4 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[6].querySelectorAll('td'))[1]");

            Assert.AreNotEqual("--", demovalue4.Text);



            //   Manufacturing

            var demovalue5 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[6].querySelectorAll('td'))[2]");

            Assert.AreNotEqual("--", demovalue5.Text);




            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            


            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario1and2()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            driver.Quit();
            Thread.Sleep(5000);

        }







        [TestMethod]
        public void ValidateInferenceinDemographicReportSingleDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


         
            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            ////verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }






        [TestMethod]
        public void ValidateInferenceinDemographicReportMultiDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario6()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinDemographicReportSingleDistributionAnsweratleast1question_Scenario5()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }






        [TestMethod]
        public void ValidateInferenceinDemographicReportMultiDistributionAnsweratleast1question_Scenario5()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question_Scenario5()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario5()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinDemographicReportSingleDistributionAnsweratleast1question_Scenario3()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }






        [TestMethod]
        public void ValidateInferenceinDemographicReportMultiDistributionAnsweratleast1question_Scenario3()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question_Scenario3()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario3()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinDemographicReportSingleDistributionAnsweratleast1question_Scenario4()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }






        [TestMethod]
        public void ValidateInferenceinDemographicReportMultiDistributionAnsweratleast1question_Scenario4()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }








        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question_Scenario4()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





        [TestMethod]
        public void ValidateInferenceinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question_Scenario4()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);






            //Clinet id:1561 & Inference=13
            //Demographic Threshold to Prevent Inference of Results   =13


            ReportDataSuppression.UpdateGroupMinimumSizenew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateNoOfCommentsReceived("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("13", "1561", "7625", "11999", DBConnectionParameters);

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);


            ReportDataSuppression.UpdateDemoThresholdToPreventResultInference("13", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(5000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(5000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "function");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "function");




            Thread.Sleep(10000);




            //verify demovalue-Sales is   Suppressed in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Sales']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(5000);

            //verify demovalue-Information Technology is Suppressed in LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='Information Technology']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Manufacturing is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Manufacturing']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Consulting Services is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Consulting Services']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(5000);



            //verify demovalue-Research and Development is UnSuppressed in  LeftsideDemo dropdown

            IWebElement Chip5 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-light']//*[text()='Research and Development']");


            Assert.IsTrue(Chip5.Displayed);


            Thread.Sleep(5000);



            driver.Quit();
            Thread.Sleep(5000);

        }





    }
}

