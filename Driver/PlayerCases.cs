﻿using System;
using System.Windows.Forms;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.PageObjects;

using Common;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Driver
{
    /// <summary>
    /// Summary description for MigratedPlayerCases
    /// </summary>
    [TestClass]
    public class PlayerCases
    {
        public PlayerCases()
        {
            //
            // TODO: Add constructor logic here
            //
        }

       
        public static bool isLocal = true;
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DBConnectionStringDTO DBConnectionParameters1;
        private static string FormName;
        private static Random random = new Random();


        private static string Locator_GoToHome = "//a[@id='show-home']//span[@class='menu-text'][contains(text(),'Home')]";
        private static string Locator_AuthorNewSurvey = "//*[(@id = 'btnAuthorNewSurvey')]";
        private static string Locator_CreateNewDistrubution = "//a[@id='show-home']//span[@class='menu-text'][contains(text(),'Home')]";


        // Player Objects  Start

        private static string ClosedSurveyMessage = "//div[@class='red jumbotron']";
        private static string FinishButton = "//button[@id='btnFinish']";
        private static string PrivacyLink = "//a[contains(text(),'Privacy')]";
        private static string CopyRightLink = "//div[@class='col-md-6 pulsefooter-cr']";
        private static string MainLogo = "//img[@id='brand-logo-ce-testdriver']";

        //DROP DOWN

        private static string selectDropDown = "//button[@title='Please Select…']";

        private static string element1DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[1]//a[1]//label[1]";
        private static string element2DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[2]//a[1]//label[1]";
        private static string element3DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[3]//a[1]//label[1]";
        private static string element4DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[4]//a[1]//label[1]";

        private static string errDropDown = "//label[@class='error margin-top-10 error-multiselect']";

        //MULTIPLE CHOICE MULTI SELECT

        private static string multiChoiceInsText = "//div[contains(text(),'1. Chose your favorite Departments')]";
        private static string multiChoiceWithoutInsText = "//div[contains(text(),'1. Without Instruction set : TODO')]";
        private static string NAOtpionisShown = "//span[contains(text(),'Not Applicable')]";
        //Fixed format date paramenter check
        private static string fixedFormatDateCheck = "//input[@type='text']";

        // Player Objects  End


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        //shwetabh srivastava--------added capabilities for download file in specified location at run time
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    // FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    //     service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    //   driver = new FirefoxDriver();
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    InternetExplorerOptions ieoptions = new InternetExplorerOptions();
                    ieoptions.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                    driver = new InternetExplorerDriver(ieoptions);
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    chromeOptions.AddArgument("start-maximized");
                    chromeOptions.AddArgument("no-sandbox");
                    //chromeOptions.AddArgument("disable-infobars");
                    //chromeOptions.AddUserProfilePreference("credentials_enable_service", false);
                    //chromeOptions.AddUserProfilePreference("profile.password_manager_enabled", false);
                    //chromeOptions.AddArgument("--dns-prefetch-disable");

                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }


        public static void OpenOrCreateForm(IWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        public static void AddDropDownQuestionLocal_WithInsText(RemoteWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }

            EnableInstructionText_DropDown(driver);
            UIActions.Click(driver, "#btnDropDownSave");
        }

        public static IWebElement WaitForSometing(IWebDriver driver, string selector, int wait, string type)
        {
            switch (type)
            {
                case "css":
                    return (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                            .Until(ExpectedConditions.ElementIsVisible(By.CssSelector(selector)));


                case "xpath":
                    return (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                        .Until(ExpectedConditions.ElementIsVisible(By.XPath(selector)));


                default:
                    return (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                            .Until(ExpectedConditions.ElementIsVisible(By.CssSelector(selector)));


            }

        }

        public static void AuthorNewSurvey(IWebDriver driver)
        {
            ClickWithWaitXpath(driver, Locator_AuthorNewSurvey, 30);
        }

        public static void CreateNewDistribution(IWebDriver driver)
        {
            ClickWithWaitXpath(driver, Locator_CreateNewDistrubution, 30);
        }

        public static void GotoHome(IWebDriver driver)
        {
            ClickWithWaitXpath(driver, Locator_GoToHome, 30);
        }

        public static void ClickWithWaitXpath(IWebDriver driver, string selector, int wait)
        {
            (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                .Until(ExpectedConditions.ElementIsVisible(By.XPath(selector))).Click();
        }

        public static IWebElement CreateFormLocal(IWebDriver driver, string formName, string styleSheet = null)
        {
            UIActions.ClickWithWait(driver, "#btnAuthorNewSurvey", 30);
            UIActions.ClickWithWait(driver, "#create-from-scratch", 30);
            UIActions.GetElementWithWait(driver, "#surveyName", 2000);
            IWebElement surveyNameTextBox = driver.FindElement(By.Id("surveyName"));
            surveyNameTextBox.Clear();
            surveyNameTextBox.SendKeys(formName);
            if (styleSheet != null)
            {
                UIActions.SelectInCEBDropdownByText(driver, "#select-style", styleSheet);
            }

            ClickWithWaitXpath(driver, "//a[contains(text(),'Upload logo')]", 30);
            ClickWithWaitXpath(driver, "//button[text()='Upload' and @type='button']", 30);
            Thread.Sleep(5000);

            string imagePath = Environment.CurrentDirectory + "\\yellow-brand.gif";

            var input_field = driver.FindElement(By.XPath("//input[@type='text' and @disabled='']"));
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string title = (string)js.ExecuteScript("arguments[0].removeAttribute(\"disabled\");", input_field);
            Thread.Sleep(5000);

            ClickWithWaitXpath(driver, "//label[@for='input-image-upload']", 30);
            Thread.Sleep(5000);


            SendKeys.SendWait(imagePath);
            Thread.Sleep(10000);
            SendKeys.SendWait(@"{Enter}");


            driver.FindElement(By.XPath("//input[@type='text' and @placeholder='Image Name']")).SendKeys("Gartner Logo - " + RandomString(5));
            ClickWithWaitXpath(driver, "//button[contains(@class,'btn-primary') and text()='Upload']", 30);
            Thread.Sleep(10000);

            UIActions.Click(driver, "#btn-create-survey");
            var firstEmptyPage = UIActions.GetElementWithWait(driver, ".page-list a span", 45);
            return firstEmptyPage;
        }


        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }

        public static void AddDropDownQuestionLocal(RemoteWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            EnableMultiSelect(driver);
            EnableInstructionText_DropDown(driver);
            UIActions.Click(driver, "#btnDropDownSave");
        }

        public static void AddLongCommentQuestionLocal(RemoteWebDriver driver, string info)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            Thread.Sleep(1000);

            EnableInstructionText_DropDown(driver);

            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }


        public static void AddShortCommentQuestionLocal(RemoteWebDriver driver, string info)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            Thread.Sleep(1000);

            EnableInstructionText_DropDown(driver);

            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }

        public static void AddFixedFormatQuestionLocal(RemoteWebDriver driver, string info, string type)
        {
            UIActions.clickwithID(driver, "add-fixed-format");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#fixed-format-type", type);
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);

            EnableInstructionText_DropDown(driver);

            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }

        public static void AddDropDownQuestionLocal_EnableNA(RemoteWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            EnableNotApplicableDropDownSingleSelect(driver);

            UIActions.Click(driver, "#btnDropDownSave");
        }

        private static void EnableMultiSelect(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElement(driver, "#chkMultiSelect").Click();
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='maxResponses']", 10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(3.ToString());
            infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='minResponses']", 10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(2.ToString());
        }

        private static void EnableMultiSelectMultiChoice(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElementwithXpath(driver, "//div[@class='row allowMultiSelect']//label", 10).Click();
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='maxMultipleChoice']", 10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(3.ToString());
            infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='minMultipleChoice']", 10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(2.ToString());
        }

        public static void AddFixedFormatDateQuestionAsDemoLocal(RemoteWebDriver driver, string info, string[] CategoryName)
        {
            UIActions.clickwithID(driver, "add-fixed-format");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#fixed-format-type", "format_date");
            UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-category-Fixed", CategoryName);
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);

            EnableInstructionText(driver);
            EnterInstructionText(driver, "this is instruction text for comment question");

            UIActions.clickwithID(driver, "btnFixedFormatSave");

        }

        public static void AddMultipleChoiceMultiItemQuestionLocal(RemoteWebDriver driver, string instructionText, string[] questions, string[] responses)
        {
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#mcScaleType", "multiItem");
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtIncludeMultiItemInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#mcMultiItemsQuestions .addmoreButton button");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#mcMultiItemsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }

            //EnableNotApplicable(driver);
            Thread.Sleep(5000);
            EnableMultiSelectMultiChoice(driver);

            UIActions.Click(driver, "#btnMultipleChoiceSave");
        }

        public static void AddMultipleChoiceMultiItemQuestionLocalWithoutInsText(RemoteWebDriver driver, string instructionText, string[] questions, string[] responses)
        {
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#mcScaleType", "multiItem");
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtIncludeMultiItemInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#mcMultiItemsQuestions .addmoreButton button");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#mcMultiItemsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }

            UIActions.Click(driver, "#btnMultipleChoiceSave");
        }

        private static void EnterInstructionText(RemoteWebDriver driver, string text)
        {
            /* Click on instruction text */
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            Thread.Sleep(2000);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath("//*[@id='fixedFormat-response']/div/div[1]/div[2]/div"));
            foreach (var item in cont)
            {
                executor.ExecuteScript("arguments[0].click();", item);
                item.Click();
                item.SendKeys("Instruction text for comment question");
            }
        }

        private static void EnableInstructionText(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElement(driver, "#chkInclude").Click();
        }

        private static void EnableInstructionText_DropDown(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(3000);
            UIActions.GetElement(driver, "#includeChkbox").Click();
            //ClickWithWaitXpath(driver, "//input[@id='includeChkbox']", 10);
            Thread.Sleep(5000);

            string instructionText = "Instruction text for drop down question";
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtDropDownInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);


            Thread.Sleep(3000);
        }

        private static void EnableInstructionText_RatingSingleItem(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            ClickWithWaitXpath(driver, "//*[@id='chkIncludeInstruction']", 10);
            Thread.Sleep(5000);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            Thread.Sleep(2000);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath("//*[@id='fixedFormat-response']/div/div[1]/div[2]/div"));
            foreach (var item in cont)
            {
                executor.ExecuteScript("arguments[0].click();", item);
                item.Click();
                item.SendKeys("Instruction text for comment question");
            }

        }

        private static void EnableNotApplicable(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElementwithXpath(driver, "//div[@class='row NotInclude IncludeNotApplicable']//label", 10).Click();
        }

        private static void EnableNotApplicableDropDownSingleSelect(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(5000);
            UIActions.GetElementwithXpath(driver, "//div[@class='row IncludeNotApplicable margin-bottom-10']//label", 10).Click();
        }


        public static void AddRatingScaleQuestionLocal(RemoteWebDriver driver, string questionText, string scaleRange = null, string[] scaleHeaders = null)
        {
            UIActions.Click(driver, "#add-rating-scale");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtSingleItemQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            if (!string.IsNullOrEmpty(scaleRange))
            {
                UIActions.Click(driver, "#rating-Response-heading");
                Thread.Sleep(1000);
                UIActions.SelectInCEBDropdownByText(driver, "#available-rating-scales", scaleRange);
            }
            if (scaleHeaders != null && scaleHeaders.Length > 0)
            {
                var scaleHeaderCheckBoxes = UIActions.FindElements(driver, "#divAddedRowRatingScale input");
                var scaleHeaderTextAreas = UIActions.FindElements(driver, "#divAddedRowRatingScale div.cke_textarea_responses");
                for (int i = 0; i < scaleHeaders.Length; i++)
                {
                    if (!string.IsNullOrEmpty(scaleHeaders[i]))
                    {
                        scaleHeaderCheckBoxes.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Clear();
                        scaleHeaderTextAreas.ElementAt(i).SendKeys(scaleHeaders[i]);
                    }
                }
            }
            Thread.Sleep(2000);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            ac.MoveToElement(driver.FindElement(By.CssSelector("#btnRatingScale"))).Build().Perform();
            Thread.Sleep(5000);


            EnableInstructionText_RatingSingleItem(driver);

            UIActions.Click(driver, "#btnRatingScale");
        }

        public static void AddMultiItemRatingScaleQuestionLocal(RemoteWebDriver driver, string instructionText, string[] questions, string scaleRange = null, string[] scaleHeaders = null)
        {
            UIActions.Click(driver, "#add-rating-scale");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#rating-scale-style", "multiItem");
            var instructionBox = UIActions.GetElement(driver, "#txtRatingScaleInstruction + .cke_textarea_inline");
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#addedRowsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }
            if (!string.IsNullOrEmpty(scaleRange))
            {
                UIActions.Click(driver, "#rating-Response-heading");
                Thread.Sleep(1000);
                UIActions.SelectInCEBDropdownByText(driver, "#available-rating-scales", scaleRange);
            }
            if (scaleHeaders != null && scaleHeaders.Length > 0)
            {
                var scaleHeaderCheckBoxes = UIActions.FindElements(driver, "#divAddedRowRatingScale input");
                var scaleHeaderTextAreas = UIActions.FindElements(driver, "#divAddedRowRatingScale div.cke_textarea_responses");
                for (int i = 0; i < scaleHeaders.Length; i++)
                {
                    if (!string.IsNullOrEmpty(scaleHeaders[i]))
                    {
                        scaleHeaderCheckBoxes.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Clear();
                        scaleHeaderTextAreas.ElementAt(i).SendKeys(scaleHeaders[i]);
                    }
                }
            }


            EnableInstructionText(driver);
            EnterInstructionText(driver, "this is instruction text for rating scale question");



            UIActions.Click(driver, "#btnRatingScale");
        }


        /// <summary>
        /// ////////////////////////////   TEST CASES START ////////////////////////////////////////
        /// </summary>
        [TestMethod]
        public void Test_ClosedSurveyCheck()
        {
            

            // URL is hard coded for now

            string stageClosedURL = "https://stg-surveys.cebglobal.com/Pulse/Player/Start/26827/0/5273628d-39e2-44db-bdfe-89fea982509e";

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            }



            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, ClosedSurveyMessage, 10);
            
            Assert.IsTrue(_embLnk.Displayed);
            
            Assert.AreEqual(_embLnk.Text.ToString(), "This survey is now closed. Thank you for your interest. To exit this survey, please close your browser.");

            
            CleanUp();
        }

        [TestMethod]
        public void Test_TwoSimultaneousBrowsers()
        {
            

            // URL is hard coded for now

            string stageClosedURL = "https://stg-surveys.cebglobal.com/player/link/a632207b11fc41d3b484d75698de0f7b?lang=EN_US";
            
            RemoteWebDriver driver1 = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);
            RemoteWebDriver driver2 = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver1, FinishButton, 10);
            
            Assert.IsTrue(_embLnk.Displayed);

            _embLnk = UIActions.GetElementswithXpath(driver2, FinishButton, 10);
            
            Assert.IsTrue(_embLnk.Displayed);

            
            if (driver1 != null)
            {
                driver1.Close();
                driver1.Dispose();
            }

            if (driver2 != null)
            {
                driver2.Close();
                driver2.Dispose();
            }

            //
            //CleanUp();
        }

        [TestMethod]
        public void Test_InvalidSurveyURL()
        {
            

            // URL is hard coded for now

            //Initialize the Webdriver and login

            string stageClosedURL = "https://stg-surveys.clearforce.com/player/link/a632207b11fc41d3b484d75698de0f7b?lang=EN_US";
            

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            }
            bool _result = true;
            
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, PrivacyLink);
                _result = false;
            }
            catch (Exception)
            {
                _result = true;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            
            CleanUp();
        }
    }
}