﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Driver.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using System.Data;
using OpenQA.Selenium.Support.UI;

namespace Driver
{
    [TestClass]
    public class AnalyzeUserFilterAndComparisonsTest
    {
        public static bool IsChromeDriverAvailable;
        public static bool IsFirefoxDriverAvailable;
        public static bool IsIEDriverAvailable;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;
        private static string FormID;
        public static string GFESurveyformId;
        public static string surveyID;
        public static string surveyform = "DemoAnalyzeEdit1";
        public static DBConnectionStringDTO DBConnectionParameters;


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString()
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public bool DeleteUserAnalysisSettings(int userId, int surveyFormId)
        {
            bool hasError = false;
            try
            {
                List<int> ids = DBOperations.GetUserAnalysisIds(userId, surveyFormId, DBConnectionParameters);
                if (ids.Count > 0)
                {
                    for (int i = 0; i < ids.Count; i++)
                    {
                        DBOperations.DeleteUserAnalysisRecords(ids[i], DBConnectionParameters);
                    }
                }

            }
            catch (Exception Ex)
            {
                hasError = true;
            }

            return hasError;

        }
        [TestMethod]
        public void Validate_IfOnlyDefaultFilterExist()
        {
            int userId = 2573534;
            int surveyFormId = 5959;
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(20000);
            }

            if (!DeleteUserAnalysisSettings(userId, surveyFormId))
            {
                AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "bench_GN_NA Response Testing");
                Thread.Sleep(20000);
                var accordians = driver.FindElements(By.ClassName("accordion-toggle"));
                Assert.IsTrue(accordians.SingleOrDefault(x=>x.Text == "Comparison 1") == null && accordians.SingleOrDefault(x => x.Text == "Comparison 2") == null &&
                    accordians.SingleOrDefault(x => x.Text == "Comparison 3") == null);

            }

        }


        #region Favourites
        [TestMethod]
        public void VerifyFavouriteSpecialCharc()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "Last 30 Days");
            Thread.Sleep(5000);
            AnalyzeAction.Apply(driver);
            Thread.Sleep(5000);
            var fav =driver.FindElementByClassName("favorites-star");
            fav.Click();
            Thread.Sleep(5000);
            var favtextbox = driver.FindElementByClassName("react-autosuggest__input");
            favtextbox.Clear();
            Thread.Sleep(5000);
            var favname = "fav@" + Guid.NewGuid();
            favtextbox.SendKeys(favname);
            Thread.Sleep(5000);

            AnalyzeAction.ApplySave(driver);
            Thread.Sleep(5000);
            DBConnectionParameters.userName = "ReportAdmin";
            DBConnectionParameters.password = "Test@123";
           
          List<string> names= DBOperations.GetFavName(DBConnectionParameters);
            if (names.Contains(favname))
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }
        [TestMethod]
        public void VerifyFavouritehasValue()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "Last 30 Days");
            Thread.Sleep(5000);
            AnalyzeAction.Apply(driver);
            Thread.Sleep(5000);
            var fav = driver.FindElementByClassName("favorites-star");
            fav.Click();
            Thread.Sleep(5000);
            var favtextbox = driver.FindElementByClassName("react-autosuggest__input");
            favtextbox.Clear();
            Thread.Sleep(5000);
            var favname = "fav@" + Guid.NewGuid();
            favtextbox.SendKeys(favname);
            Thread.Sleep(5000);

            AnalyzeAction.ApplySave(driver);
            Thread.Sleep(5000);
            DBConnectionParameters.userName = "ReportAdmin";
            DBConnectionParameters.password = "Test@123";

            List<string> names = DBOperations.GetFavName(DBConnectionParameters);
            if (names.Count>0)
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void VerifyFavouritealreadyexists()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "Last 30 Days");
            Thread.Sleep(5000);
            AnalyzeAction.Apply(driver);
            Thread.Sleep(5000);
            var fav = driver.FindElementByClassName("favorites-star");
            fav.Click();
            Thread.Sleep(5000);
            var favtextbox = driver.FindElementByClassName("react-autosuggest__input");
            favtextbox.Clear();
            Thread.Sleep(5000);
            DBConnectionParameters.userName = "ReportAdmin";
            DBConnectionParameters.password = "Test@123";

            List<string> names = DBOperations.GetFavName(DBConnectionParameters);
            if(names.Count>0)
            {
                var favname = names[0];
                favtextbox.SendKeys(favname);
                Thread.Sleep(5000);
                AnalyzeAction.ApplySave(driver);
                Thread.Sleep(5000);
                Assert.IsTrue(true);
            }

            Assert.IsTrue(true);



           
        }
        [TestMethod]
        public void VerifyFavouriteinSurveyTabalphabatecically()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "Last 30 Days");
            Thread.Sleep(5000);
            AnalyzeAction.Apply(driver);
            Thread.Sleep(5000);
            var fav = driver.FindElementByClassName("favorites-star");
            fav.Click();
            Thread.Sleep(5000);
            var favtextbox = driver.FindElementByClassName("react-autosuggest__input");
            favtextbox.Clear();
            Thread.Sleep(5000);
            var favname = "fav@" + Guid.NewGuid();
            favtextbox.SendKeys(favname);
            Thread.Sleep(5000);

            AnalyzeAction.ApplySave(driver);
            Thread.Sleep(5000);
            DBConnectionParameters.userName = "ReportAdmin";
            DBConnectionParameters.password = "Test@123";

            List<string> names = DBOperations.GetFavName(DBConnectionParameters);
            if(names.Count>0)
            {
                AnalyzeAction.ClickChip(driver, "My Survey");
                Thread.Sleep(5000);
                AnalyzeAction.ClickChip(driver, "Favorites");
                Thread.Sleep(5000);
                if (AnalyzeAction.FindChip(driver, names[0]))
                    Assert.IsTrue(true);
                else
                    Assert.IsTrue(false);



            }
        }

        [TestMethod]
        public void VerifyRemoveFavourite()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "Last 30 Days");
            Thread.Sleep(5000);
            AnalyzeAction.Apply(driver);
            Thread.Sleep(5000);
            var fav = driver.FindElementByClassName("favorites-star");
            fav.Click();
            Thread.Sleep(5000);
            var favtextbox = driver.FindElementByClassName("react-autosuggest__input");
            favtextbox.Clear();
            Thread.Sleep(5000);
            var favname = "fav@" + Guid.NewGuid();
            favtextbox.SendKeys(favname);
            Thread.Sleep(5000);

            AnalyzeAction.ApplySave(driver);
            Thread.Sleep(5000);
            fav.Click();
            Thread.Sleep(5000);
            DBConnectionParameters.userName = "ReportAdmin";
            DBConnectionParameters.password = "Test@123";

            List<string> names = DBOperations.GetFavName(DBConnectionParameters);
            if (names.Contains(favname))
                Assert.IsTrue(false);
            else
                Assert.IsTrue(true);
        }

        [TestMethod]
        public void VerifyFavouriteinSurveyClearFilter()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "Last 30 Days");
            Thread.Sleep(5000);
            AnalyzeAction.Apply(driver);
            Thread.Sleep(5000);
            var fav = driver.FindElementByClassName("favorites-star");
            fav.Click();
            Thread.Sleep(5000);
            var favtextbox = driver.FindElementByClassName("react-autosuggest__input");
            favtextbox.Clear();
            Thread.Sleep(5000);
            var favname = "fav@" + Guid.NewGuid();
            favtextbox.SendKeys(favname);
            Thread.Sleep(5000);

            AnalyzeAction.ApplySave(driver);
            Thread.Sleep(5000);
            DBConnectionParameters.userName = "ReportAdmin";
            DBConnectionParameters.password = "Test@123";

            List<string> names = DBOperations.GetFavName(DBConnectionParameters);
            if (names.Count > 0)
            {
                AnalyzeAction.ClickChip(driver, "My Survey");
                Thread.Sleep(5000);
                AnalyzeAction.ClickChip(driver, "Favorites");
                Thread.Sleep(5000);
                var nn =names[0].ToString();
                AnalyzeAction.ClickChip(driver, nn);
                Thread.Sleep(5000);
                AnalyzeAction.Apply(driver);
                Thread.Sleep(5000);
                

                string button = "//button[contains(.,'Clear Filters')]";
                AnalyzeAction.ApplySave(driver, button);
                Thread.Sleep(10000);
                if (AnalyzeAction.FindChip(driver, "All Time Period"))
                    Assert.IsTrue(true);
                else
                    Assert.IsTrue(false);


            }
        }

        #endregion

        #region TimePeriod

        [TestMethod]
        public void VerifyTimePeriodRightPane()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            if(AnalyzeAction.FindChip(driver, "Last 30 Days"))
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void VerifyTimePeriodChooseFrequency()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            if (AnalyzeAction.FindChip(driver, "Survey Completion Date"))
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void VerifyTimePeriodAllTimePeriod()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            if (AnalyzeAction.FindChip(driver, "All Time Period"))
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void VerifyTimePeriodclosesonX()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            var ee = driver.FindElementById("close-filter-right-section");
            ee.Click();
            Thread.Sleep(5000);
            if (AnalyzeAction.FindChip(driver, "Survey Completion Date"))
                Assert.IsTrue(false);
            else
                Assert.IsTrue(true);

        }

        [TestMethod]
        public void VerifyTimePeriodWaterMark()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password,
                    DriverAndLogin.Account);
                Thread.Sleep(10000);
            }
            AnalyzeAction.NavigateToReportsFromAnalyzeCards(driver, "DV Survey - Consuming Account - SA");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(5000);
            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);
            AnalyzeAction.CustomTimePeriod(driver, "");
            IWebElement CustomTimePeriod = UIActions.FindElementWithXpath(driver, " //*[@id='txtCustomDateRange']");
            var data = CustomTimePeriod.GetAttribute("value");
            if (!string.IsNullOrEmpty(data))
                Assert.IsTrue(true);
            else
                Assert.IsTrue(false);
        }

        #endregion


        [ClassCleanup()]
        public static void CleanUp()
        {
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }

    }
}
