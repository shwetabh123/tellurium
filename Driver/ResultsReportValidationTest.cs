﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;
using Driver.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Drawing;

namespace Driver
{
    [TestClass]
    public class ResultReportValidationTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;

        public static string distributionName = "Enroute";
        public static DBConnectionStringDTO DBConnectionParams;
        public static string TCESUsername = "PulseQA";
        public static string TCESpassword = "password-1";
        public static string TCESserverName = "10.111.110.210,1605";
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {

                Url = @"https://qa-surveys.cebglobal.com/Pulse" ,//context.Properties["Url"].ToString(),
                Browser = "Firefox", // context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = "Sing'le Quote"//context.Properties["Account"].ToString(),
                //downloadFilepath = Convert.ToString(context.Properties["downloadFilepath"])
            };
            DBConnectionParams = new DBConnectionStringDTO
            {
                userName = "ReportAdmin",
                password = "Test@123",
                TCESserverName = "10.111.110.211,1605"
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName, string accountName = null)
        {

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            if (driver == null)
            {
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;

        }
        //[TestMethod]
        //public void ValidateCopyMatchPairDistributionValidations()
        //{
        //    RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
        //    //   DistributeActions.SearchAndOpenDistribution(driver, "testfullupdate");
        //    UIActions.GetElementWithWait(driver, "#distribute-survey-table", 30);
        //    IWebElement searchBox = driver.FindElement(By.Id("txtSearch"));
        //    searchBox.Clear();
        //    searchBox.SendKeys("testfullupdate");
        //    UIActions.Click(driver, "#btnSearch");
        //    Thread.Sleep(200000);
        //    IWebElement ActionGear = driver.FindElement(By.XPath("//*[@class='btn btn-link no-underline dropdown-toggle']"));
        //    ActionGear.Click();

        //    UIActions.clickwithXapth(driver, "//*[@widget-data-event='click::CopyDistributionClicked']");
        //    Thread.Sleep(1000);
        //    UIActions.Click(driver, "#chkCopyParticipants");
        //    Thread.Sleep(1000);
        //    UIActions.Click(driver, "#chkCopyEmails");
        //    UIActions.clickwithXapth(driver, "//*[@widget-data-event='click::CopyClicked']");
        //    Thread.Sleep(200000);
        //    IWebElement chkbox = driver.FindElement(By.Id("recurring-survey"));
        //    bool result = chkbox.Enabled;
        //    Assert.AreEqual(result, true);
        //}
        [TestMethod]
        public void ValidateLegendsInResults()
        {
           ResultsReportDbOperations oprations = new ResultsReportDbOperations();
            int userid = oprations.GetUserIdForThisEmail(DriverAndLogin.Username, TCESUsername, TCESpassword, TCESserverName);
            oprations.DeleteComparisions(userid,  7392,DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            oprations.Insert3Comparisions(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName,null);

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            string accountName = "Sing'le Quote";
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            //      RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
          //      driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                Thread.Sleep(30000);

                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
           //     WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

                Thread.Sleep(1000);
                IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//a[@title='Employee Engagement Survey']/..//following-sibling::div[2]//a[@class='btn show-analyze']");
                surveyLink1.Click();
                Thread.Sleep(30000);
                IWebElement surveyLink2 = UIActions.FindElementWithXpath(driver, " //a[@title='Results Summary']");

                surveyLink2.Click();
                Thread.Sleep(30000);
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-fav :first-child')");
                var unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-unfav :first-child')");
                var neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-neutral :first-child')");
                //  Assert.AreEqual("Not enough responses", SuppressedText.Text);
                // 
                var comp1favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-fav :first-child')");
                var comp1unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-unfav :first-child')");
                var comp1neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-neutral :first-child')");
                var comp2favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-fav :first-child')");
                var comp2unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-unfav :first-child')");
                var comp2neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-neutral :first-child')");

                var comp3favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-fav :first-child')");
                var comp3unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-unfav :first-child')");
                var comp3neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-neutral :first-child')");


                string color = favbar.GetCssValue("color");

                string targetfavColor = "rgb(0, 40, 86)";
                //  string htmlColor = ColorTranslator.ToHtml(Color.FromArgb(0, 40, 86));
                Assert.AreEqual(color== targetfavColor, true);

                color = unfavbar.GetCssValue("color");
                string targetUnfavColor = "rgb(50, 82, 119)";
                Assert.AreEqual(color == targetUnfavColor, true);

                color = neutbar.GetCssValue("color");
                string targetneutColor = "rgb(24, 60, 102)";
                Assert.AreEqual(color == targetneutColor, true);


                color = comp1favbar.GetCssValue("color");
                string comp1favColor = "rgb(255, 84, 10)";
                Assert.AreEqual(color == comp1favColor, true);

                color = comp1unfavbar.GetCssValue("color");
                string comp1UnfavColor = "rgb(255, 118, 58)";
                Assert.AreEqual(color == comp1UnfavColor, true);

                color = comp1neutbar.GetCssValue("color");
                string comp1neutColor = "rgb(255, 100, 33)";
                Assert.AreEqual(color == comp1neutColor, true);


                color = comp2favbar.GetCssValue("color");
                string comp2favColor = "rgb(0, 154, 215)";
                Assert.AreEqual(color == comp2favColor, true);

                color = comp2unfavbar.GetCssValue("color");
                string comp2UnfavColor = "rgb(50, 174, 222)";
                Assert.AreEqual(color == comp2UnfavColor, true);

                color = comp2neutbar.GetCssValue("color");
                string comp2neutColor = "rgb(24, 163, 218)";
                Assert.AreEqual(color == comp2neutColor, true);

                color = comp3favbar.GetCssValue("color");
                string comp3favColor = "rgb(254, 193, 13)";
                Assert.AreEqual(color == comp3favColor, true);

                color = comp3unfavbar.GetCssValue("color");
                string comp3UnfavColor = "rgb(254, 192, 61)";
                Assert.AreEqual(color == comp3UnfavColor, true);

                color = comp3neutbar.GetCssValue("color");
                string comp3neutColor = "rgb(254, 184, 36)";
                Assert.AreEqual(color == comp3neutColor, true);


            }
        }
        [TestMethod]
        public void ValidateResultsFor3BenchmarkComparisons()
        {
            ResultsReportDbOperations oprations = new ResultsReportDbOperations();
            int userid = oprations.GetUserIdForThisEmail(DriverAndLogin.Username, TCESUsername, TCESpassword, TCESserverName);
            oprations.DeleteComparisions(userid, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            List<int> benchmarkids = new List<int> {  102662,
                                                    103031,103032};
            oprations.Insert3Comparisions(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName, benchmarkids);
           
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            string accountName = "Sing'le Quote";
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            //      RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                //      driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                Thread.Sleep(30000);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
                //     WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

                Thread.Sleep(1000);
                IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//a[@title='Employee Engagement Survey']/..//following-sibling::div[2]//a[@class='btn show-analyze']");
                surveyLink1.Click();
                Thread.Sleep(30000);
                IWebElement surveyLink2 = UIActions.FindElementWithXpath(driver, " //a[@title='Results Summary']");

                surveyLink2.Click();
                Thread.Sleep(30000);
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-fav :first-child')");
                var unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-unfav :first-child')");
                var neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-neutral :first-child')");
                //  Assert.AreEqual("Not enough responses", SuppressedText.Text);
                // 
                var comp1favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-fav :first-child')");
                var comp1unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-unfav :first-child')");
                var comp1neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-neutral :first-child')");
                var comp2favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-fav :first-child')");
                var comp2unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-unfav :first-child')");
                var comp2neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-neutral :first-child')");

                var comp3favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-fav :first-child')");
                var comp3unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-unfav :first-child')");
                var comp3neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-neutral :first-child')");


                string color = favbar.GetCssValue("color");

                string targetfavColor = "rgb(0, 40, 86)";
                //  string htmlColor = ColorTranslator.ToHtml(Color.FromArgb(0, 40, 86));
                Assert.AreEqual(color == targetfavColor, true);

                color = unfavbar.GetCssValue("color");
                string targetUnfavColor = "rgb(50, 82, 119)";
                Assert.AreEqual(color == targetUnfavColor, true);

                color = neutbar.GetCssValue("color");
                string targetneutColor = "rgb(24, 60, 102)";
                Assert.AreEqual(color == targetneutColor, true);


                color = comp1favbar.GetCssValue("color");
                string comp1favColor = "rgb(255, 84, 10)";
                Assert.AreEqual(color == comp1favColor, true);

                color = comp1unfavbar.GetCssValue("color");
                string comp1UnfavColor = "rgb(255, 118, 58)";
                Assert.AreEqual(color == comp1UnfavColor, true);

                color = comp1neutbar.GetCssValue("color");
                string comp1neutColor = "rgb(255, 100, 33)";
                Assert.AreEqual(color == comp1neutColor, true);


                color = comp2favbar.GetCssValue("color");
                string comp2favColor = "rgb(0, 154, 215)";
                Assert.AreEqual(color == comp2favColor, true);

                color = comp2unfavbar.GetCssValue("color");
                string comp2UnfavColor = "rgb(50, 174, 222)";
                Assert.AreEqual(color == comp2UnfavColor, true);

                color = comp2neutbar.GetCssValue("color");
                string comp2neutColor = "rgb(24, 163, 218)";
                Assert.AreEqual(color == comp2neutColor, true);

                color = comp3favbar.GetCssValue("color");
                string comp3favColor = "rgb(254, 193, 13)";
                Assert.AreEqual(color == comp3favColor, true);

                color = comp3unfavbar.GetCssValue("color");
                string comp3UnfavColor = "rgb(254, 192, 61)";
                Assert.AreEqual(color == comp3UnfavColor, true);

                color = comp3neutbar.GetCssValue("color");
                string comp3neutColor = "rgb(254, 184, 36)";
                Assert.AreEqual(color == comp3neutColor, true);


            }
        }
        [TestMethod]
        public void ValidateResultsAfter2DemofilterApplied()
        {
            ResultsReportDbOperations oprations = new ResultsReportDbOperations();
            int userid = oprations.GetUserIdForThisEmail(DriverAndLogin.Username, TCESUsername, TCESpassword, TCESserverName);
            oprations.DeleteComparisions(userid, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
          
            oprations.Insert3Comparisions(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName, null, "date of joining");
            oprations.InsertDemofilters(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
           RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            string accountName = "Sing'le Quote";
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            //      RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                //      driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                Thread.Sleep(30000);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
                //     WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

                Thread.Sleep(1000);
                IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//a[@title='Employee Engagement Survey']/..//following-sibling::div[2]//a[@class='btn show-analyze']");
                surveyLink1.Click();
                Thread.Sleep(30000);
                IWebElement surveyLink2 = UIActions.FindElementWithXpath(driver, " //a[@title='Results Summary']");

                surveyLink2.Click();
                Thread.Sleep(30000);
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-fav :first-child')");
                var unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-unfav :first-child')");
                var neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-neutral :first-child')");
                //  Assert.AreEqual("Not enough responses", SuppressedText.Text);
                // 
                var comp1favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-fav :first-child')");
                var comp1unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-unfav :first-child')");
                var comp1neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-1-neutral :first-child')");
                var comp2favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-fav :first-child')");
                var comp2unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-unfav :first-child')");
                var comp2neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-2-neutral :first-child')");

                var comp3favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-fav :first-child')");
                var comp3unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-unfav :first-child')");
                var comp3neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.comparison-3-neutral :first-child')");


                string color = favbar.GetCssValue("color");

                string targetfavColor = "rgb(0, 40, 86)";
                //  string htmlColor = ColorTranslator.ToHtml(Color.FromArgb(0, 40, 86));
                Assert.AreEqual(color == targetfavColor, true);

                color = unfavbar.GetCssValue("color");
                string targetUnfavColor = "rgb(50, 82, 119)";
                Assert.AreEqual(color == targetUnfavColor, true);

                color = neutbar.GetCssValue("color");
                string targetneutColor = "rgb(24, 60, 102)";
                Assert.AreEqual(color == targetneutColor, true);


                color = comp1favbar.GetCssValue("color");
                string comp1favColor = "rgb(255, 84, 10)";
                Assert.AreEqual(color == comp1favColor, true);

                color = comp1unfavbar.GetCssValue("color");
                string comp1UnfavColor = "rgb(255, 118, 58)";
                Assert.AreEqual(color == comp1UnfavColor, true);

                color = comp1neutbar.GetCssValue("color");
                string comp1neutColor = "rgb(255, 100, 33)";
                Assert.AreEqual(color == comp1neutColor, true);


                color = comp2favbar.GetCssValue("color");
                string comp2favColor = "rgb(0, 154, 215)";
                Assert.AreEqual(color == comp2favColor, true);

                color = comp2unfavbar.GetCssValue("color");
                string comp2UnfavColor = "rgb(50, 174, 222)";
                Assert.AreEqual(color == comp2UnfavColor, true);

                color = comp2neutbar.GetCssValue("color");
                string comp2neutColor = "rgb(24, 163, 218)";
                Assert.AreEqual(color == comp2neutColor, true);

                color = comp3favbar.GetCssValue("color");
                string comp3favColor = "rgb(254, 193, 13)";
                Assert.AreEqual(color == comp3favColor, true);

                color = comp3unfavbar.GetCssValue("color");
                string comp3UnfavColor = "rgb(254, 192, 61)";
                Assert.AreEqual(color == comp3UnfavColor, true);

                color = comp3neutbar.GetCssValue("color");
                string comp3neutColor = "rgb(254, 184, 36)";
                Assert.AreEqual(color == comp3neutColor, true);


            }
        }
        [TestMethod]
        public void ValidatResultsWithoutComparisions()
        {
            ResultsReportDbOperations oprations = new ResultsReportDbOperations();
            int userid = oprations.GetUserIdForThisEmail(DriverAndLogin.Username, TCESUsername, TCESpassword, TCESserverName);
            oprations.DeleteComparisions(userid, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);

      //      oprations.Insert3Comparisions(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName, null, "date of joining");
      //      oprations.InsertDemofilters(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            string accountName = "Sing'le Quote";
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            //      RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                //      driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                Thread.Sleep(30000);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
                //     WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

                Thread.Sleep(1000);
                IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//a[@title='Employee Engagement Survey']/..//following-sibling::div[2]//a[@class='btn show-analyze']");
                surveyLink1.Click();
                Thread.Sleep(30000);
                IWebElement surveyLink2 = UIActions.FindElementWithXpath(driver, " //a[@title='Results Summary']");

                surveyLink2.Click();
                Thread.Sleep(30000);
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-fav :first-child')");
                var unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-unfav :first-child')");
                var neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-neutral :first-child')");
                //  Assert.AreEqual("Not enough responses", SuppressedText.Text);
                // 

                string color = favbar.GetCssValue("color");

                string targetfavColor = "rgb(0, 40, 86)";
                //  string htmlColor = ColorTranslator.ToHtml(Color.FromArgb(0, 40, 86));
                Assert.AreEqual(color == targetfavColor, true);

                color = unfavbar.GetCssValue("color");
                string targetUnfavColor = "rgb(50, 82, 119)";
                Assert.AreEqual(color == targetUnfavColor, true);

                color = neutbar.GetCssValue("color");
                string targetneutColor = "rgb(24, 60, 102)";
                Assert.AreEqual(color == targetneutColor, true);
            }
        }
        [TestMethod]
        public void SearchQuestionInResults()
        {
            ResultsReportDbOperations oprations = new ResultsReportDbOperations();
            int userid = oprations.GetUserIdForThisEmail(DriverAndLogin.Username, TCESUsername, TCESpassword, TCESserverName);
            oprations.DeleteComparisions(userid, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);

            //      oprations.Insert3Comparisions(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName, null, "date of joining");
            //      oprations.InsertDemofilters(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            string accountName = "Sing'le Quote";
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            //      RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                //      driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                Thread.Sleep(30000);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
                //     WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

                Thread.Sleep(1000);
                IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//a[@title='Employee Engagement Survey']/..//following-sibling::div[2]//a[@class='btn show-analyze']");
                surveyLink1.Click();
                Thread.Sleep(30000);
                IWebElement surveyLink2 = UIActions.FindElementWithXpath(driver, " //a[@title='Results Summary']");

                surveyLink2.Click();
                Thread.Sleep(30000);
                string QuestionText = "I feel my Company has good benefits.";
                AnalyzeAction.SearchQuestion(driver, QuestionText);
                  Thread.Sleep(10000);
             

                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var favbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-fav :first-child')");
                var unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-unfav :first-child')");
                var neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-neutral :first-child')");
                //  Assert.AreEqual("Not enough responses", SuppressedText.Text);
                // 

                string color = favbar.GetCssValue("color");

                string targetfavColor = "rgb(0, 40, 86)";
                //  string htmlColor = ColorTranslator.ToHtml(Color.FromArgb(0, 40, 86));
                Assert.AreEqual(color == targetfavColor, true);

                color = unfavbar.GetCssValue("color");
                string targetUnfavColor = "rgb(50, 82, 119)";
                Assert.AreEqual(color == targetUnfavColor, true);

                color = neutbar.GetCssValue("color");
                string targetneutColor = "rgb(24, 60, 102)";
                Assert.AreEqual(color == targetneutColor, true);
            }
        }
        [TestMethod]
        public void VerifyNpsDial()
        {
            ResultsReportDbOperations oprations = new ResultsReportDbOperations();
            int userid = oprations.GetUserIdForThisEmail(DriverAndLogin.Username, TCESUsername, TCESpassword, TCESserverName);
            oprations.DeleteComparisions(userid, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);

            //      oprations.Insert3Comparisions(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName, null, "date of joining");
            //      oprations.InsertDemofilters(userid, 36287, 7392, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            string accountName = "Andromeda";
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            //      RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                //      driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                Thread.Sleep(30000);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
                //     WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

                Thread.Sleep(1000);
                IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//a[@title='Employee Engagement Survey']/..//following-sibling::div[2]//a[@class='btn show-analyze']");
                surveyLink1.Click();
                Thread.Sleep(30000);
                IWebElement surveyLink2 = UIActions.FindElementWithXpath(driver, " //a[@title='Results Summary']");

                surveyLink2.Click();
                Thread.Sleep(30000);
                string QuestionText = @"Considering your complete experience";
                AnalyzeAction.SearchQuestion(driver, QuestionText);
                Thread.Sleep(10000);


                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var npsArc = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.nps-current-1')");
           //     var unfavbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-unfav :first-child')");
          //      var neutbar = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('paper-card').querySelector('.card-content').querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('#content').querySelector('#svg').querySelector('.current-neutral :first-child')");
                //  Assert.AreEqual("Not enough responses", SuppressedText.Text);
                // 

                string color = npsArc.GetCssValue("color");

                string targetfavColor = "rgb(0, 40, 86)";
                //  string htmlColor = ColorTranslator.ToHtml(Color.FromArgb(0, 40, 86));
                Assert.AreEqual(color == targetfavColor, true);

            
            }
        }
        [ClassCleanup()]
        public static void CleanUp()
        {
            //RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            //if (driver != null)
            //{
            //    driver.Close();
            //    driver.Dispose();
            //}
        }
    }
}