﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Driver
{
    [TestClass]
    public class Integration
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }


        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToCompanyDirectoy()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
                CompanyDirectoryActions.NavigateToCompanyDirectory(driver);
            }
            else if (UIActions.IsElementVisible(driver, "#directory-participants-title"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                CompanyDirectoryActions.NavigateToCompanyDirectory(driver);
            }
            return driver;
        }

        [TestMethod]
        public void TestFTP()
        {
            try
            {

                if (Common.DBOperations.DeleteCompanyDirectoryParticipant(ConfigurationManager.AppSettings["EmailAddressFTP"]))
                {
                    var connectionInfo = new ConnectionInfo(ConfigurationManager.AppSettings["SFTPUrl"],
                                           ConfigurationManager.AppSettings["SFTPUserName"],
                                           new PasswordAuthenticationMethod(ConfigurationManager.AppSettings["SFTPUserName"], ConfigurationManager.AppSettings["SFTPPassword"]),
                                           new PrivateKeyAuthenticationMethod("rsa.key"));
                    using (var client = new SftpClient(connectionInfo))
                    {
                        client.Connect();
                        client.ChangeDirectory(ConfigurationManager.AppSettings["WorkingDirectory"]);
                        using (var fileStream = new FileStream(ConfigurationManager.AppSettings["FTPFile"], FileMode.Open))
                        {
                            client.BufferSize = 4 * 1024; // bypass Payload error large files
                            client.UploadFile(fileStream, Path.GetFileName(ConfigurationManager.AppSettings["FTPFile"]));
                            client.Disconnect();
                        }

                    }

                    FTPImporterHangfire.FTPImporterProcess ss = new FTPImporterHangfire.FTPImporterProcess();
                    ss.ProcessItems();

                }
                int result = Common.DBOperations.GetCompanyDirectoryParticipant(ConfigurationManager.AppSettings["EmailAddressFTP"]);
                Assert.AreEqual(1, result);

            }
            catch (Exception ex)
            {
                if (ex.Message == "An existing connection was forcibly closed by the remote host")
                    TestFTP();
                else
                {
                    Assert.Fail();
                    string message = ex.Message;
                }
                
            }

        }

        [TestMethod]

        public void CheckIntegration()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(1000);

            IntegrationAction.NavigatetoSettings(driver);
            Thread.Sleep(2000);
            IntegrationAction.OpenIntegrationtab(driver);
            Thread.Sleep(1000);
         

            IWebElement toggle = UIActions.GetElementswithXpath(driver, "(//LABEL[contains(@for,'integrationEnableFTPImport')])", 45);
            if (!toggle.GetAttribute("class").Contains("switchOn"))
                toggle.Click();
            IWebElement datasourcefolder = UIActions.GetElementswithXpath(driver, "//*[@id='integrationDataSourceFolder']", 45);
            datasourcefolder.Clear();
            datasourcefolder.SendKeys(DriverAndLogin.Account);
            IWebElement datasourceprefix = UIActions.GetElementswithXpath(driver, "//*[@id='integrationFilePrefix']", 45);
            datasourceprefix.Clear();
            datasourceprefix.SendKeys("XFTP");
            IWebElement save = UIActions.GetElementswithXpath(driver, "//*[@id='integration-save']", 45);
            save.Click();
            Thread.Sleep(2000);
            string Message;

            Message = UIActions.GetValueByElement(driver, ".alert-success #action-status");
            Thread.Sleep(3000);
            Assert.AreEqual("Account Settings updated.", Message.ToString().Trim());



        }

        [TestMethod]

        public void CheckSurveyUrlIntegration()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(1000);

            IntegrationAction.NavigatetoSettings(driver);
            Thread.Sleep(2000);
            IntegrationAction.OpenIntegrationtab(driver);
            Thread.Sleep(1000);


         
           
            Thread.Sleep(2000);
            IWebElement save = UIActions.GetElementswithXpath(driver, "//*[@id='integrationSFTPURL']", 45);
            if (save != null)
                Assert.IsTrue(true);
            else
                Assert.Fail();



        }


    }
}
