﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Data;
using Common;

namespace Driver
{
    [TestClass]
    public class rawdatalogic
    {


        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;



        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };

            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()

            };
            
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
           
            return driver;
        }
        [TestMethod]

        public void AnyhidequestionExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();



                string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
                DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

                int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
                Thread.Sleep(2000);

                string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
                DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

                //survey response table

                string query2 = @"select distinct surveywaveparticipantid from surveywaveresponse where  surveyid = 32750 and
 surveywaveparticipantid not in (select  SurveyWaveParticipantID from SurveyWaveResponse where surveyid = 32750 and
  surveyformitemid = 83634 and scaleoptionid in(445537,445538) OR surveyformitemid in(83636) and scaleoptionid in(445550))";

                DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

                var spresults = Rawdata.compareresults(ActualSP, rawdata);


            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }


        [TestMethod]

        public void AnyhidedateExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata2@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
               
           

            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @" select distinct surveywaveparticipantid, UpdateTimestampGMT from surveywaveresponse where 
            surveyid = 32750  and surveywaveparticipantid not in (select surveywaveparticipantid where 
			 (UpdateTimestampGMT  between '2018-10-23 00:00:00.000' and '2018-10-24 00:00:00.000')  
				Or (UpdateTimestampGMT  in ('2018-12-25 00:00:00.000')))";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);
                Thread.Sleep(5000);
              
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


        }

        [TestMethod]

        public void AllhidequestionExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata3@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
                Thread.Sleep(5000);
               
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @"select distinct surveywaveparticipantid  from SurveyWaveResponse where surveyid = 32750
 and   SurveyWaveParticipantID not in  (select  surveywaveparticipantid from SurveyWaveResponse where surveyid = 32750 
							and (surveyformitemid in (83638) and scaleoptionid in (445560,445561))

							Intersect 

select  surveywaveparticipantid from SurveyWaveResponse where surveyid = 32750 
							and (surveyformitemid in (83634) and scaleoptionid in (445542)))";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);

        }
        [TestMethod]
        public void AllhidedateExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata4@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
                Thread.Sleep(5000);
               
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @"select distinct surveywaveparticipantid  from SurveyWaveResponse where SurveyWaveParticipantID not in (
select distinct surveywaveparticipantid 
        from SurveyWaveResponse where surveyid = 32750
        and  UpdateTimestampGMT ='2018-12-25 00:00:00.000'
        AND   (surveyformitemid in (83634) and scaleoptionid in (445539,445542))) and  surveyid = 32750";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);

        }



        [TestMethod]



        public void AnyshowquestionExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata5@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
                Thread.Sleep(5000);
                
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @"select distinct surveywaveparticipantid  
        from SurveyWaveResponse where surveyid = 32750
      and 
         surveyformitemid in (83635) and scaleoptionid in (445544,445549)
		
		OR
              surveyformitemid in (83637) and scaleoptionid != (445559)";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);

        }

        [TestMethod]
        public void AllshowdquestionExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata7@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
                Thread.Sleep(5000);
               
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @"select distinct surveywaveparticipantid  
        from SurveyWaveResponse where surveyid = 32750  and  
		(surveyformitemid = 83639 and scaleoptionid = 445563) AND
		         (UpdateTimestampGMT Between  '2018-06-06 00:00:00.000' AND '2018-12-31 00:00:00.000')
		";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);

        }
        [TestMethod]
        public void AnyshowdateExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata6@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
                Thread.Sleep(5000);
            
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @"select distinct surveywaveparticipantid  
        from SurveyWaveResponse where surveyid = 32750 and 

         (UpdateTimestampGMT in  ('2018-12-25 00:00:00.000') OR  UpdateTimestampGMT !=('2018-10-23 00:00:00.000'))";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);

        }


        [TestMethod]
        public void AnyhidedemoExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata8@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
                Thread.Sleep(5000);

            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @"select distinct surveywaveparticipantid  from surveywaveresponse where surveyid = 32750 and surveywaveparticipantid not in (


(select SurveyWaveParticipantID from  Surveywaveparticipantdemo where surveyid = 32750 and  Demo6 ='velachery')
union
 (select SurveyWaveParticipantID from  surveywaveresponse where surveyformitemid = 83634 and scaleoptionid = 445537))";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);

        }

        [TestMethod]
        public void AllshowdemoExportRawdataExcel()
        {
            try
            {
                DriverAndLogin.Username = "rawdata10@cebglobal.com";
                DriverAndLogin.Password = "Rawdata@2017";
                DriverAndLogin.Account = "Automation kkvp";
                // DriverAndLogin.Browser = "Firefox";
                string DistributionName = "Automation Rawdata";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                // DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenExportMenu(driver);
                UIActions.Click(driver, "#expradio-3");
                var exportBtn = UIActions.FindElement(driver, "ul.dropdown-menu #exportBtn");
                exportBtn.Click();
                Thread.Sleep(5000);

            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }


            string query = "select  top 1 * from FileExportQueue where ProcessTypeID = 3 and SurveyFormid = 8636 order by 1 desc ";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters_TCES(query, DBConnectionParameters);

            int filexportqueueid = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            Thread.Sleep(2000);

            string InputQuery = "EXEC dbo.usp_exp_ExportWrapper_Green " + filexportqueueid;
            DataSet rawdata = DBOperations.ExecuteSPwithInputParameters_TCES(InputQuery, DBConnectionParameters);

            //survey response table

            string query2 = @"select distinct surveywaveparticipantid  from surveywaveresponse where surveyid = 32750 and surveywaveparticipantid  in (


(select SurveyWaveParticipantID from  Surveywaveparticipantdemo where surveyid = 32750 and  Demo6 ='velachery')
 intersect  
 (select SurveyWaveParticipantID from  surveywaveresponse where surveyformitemid = 83634 and UpdateTimestampGMT = '2018-12-25 00:00:00.000'))";

            DataSet ActualSP = DBOperations.ExecuteSPwithInputParameters_TCES(query2, DBConnectionParameters);

            var spresults = Rawdata.compareresults(ActualSP, rawdata);

        }
        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }

    }




}












