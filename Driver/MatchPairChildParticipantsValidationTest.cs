﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using Common;
namespace Driver
{
    [TestClass]
    public class MatchPairChildParticipantsValidationTest
    {
        public  static DBConnectionStringDTO DBConnectionParams;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {


            DBConnectionParams = new DBConnectionStringDTO
            {
                userName = "PulseQA",
                password = "password-1",
                TCESserverName = "USA-QIN-PLSP-01,1605"
            };
        }

        [TestMethod]
        public void ValidateMatchPairChildParticipants()
        {
            MatchPairDBOperations oprations = new MatchPairDBOperations();
            var results = oprations.GetDistributionParticipants(28722, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            var result = results.FindAll(t => t.SourceType == "1");
            bool bvalid = result.Count == 1 && result.FirstOrDefault().EmailAddress == "sseetharaman@cebglobal.com";
            Assert.AreEqual(true, bvalid);
        }
        [TestMethod]
        public void ValidateMatchPairChildTabLastEmailDates()
        {
            MatchPairDBOperations oprations = new MatchPairDBOperations();
            var results = oprations.GetDistributionParticipantsForChildTab(26333, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            var error = oprations.ValidateDistributionParticipants(26333, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName, results);
            Assert.AreEqual(0, error);
        }
        [TestMethod]
        public void ValidateMatchPairParentTabLastEmailDates()
        {
            MatchPairDBOperations oprations = new MatchPairDBOperations();
            var results = oprations.GetDistributionParticipantsForParentTab(26333, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            var parentemailresults = oprations.GetDistributionParentParticipants(26333, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            for ( int i = 0; i < parentemailresults.Count; i++)
            {
                var childemailsForthisParent = results.FindAll(t => t.ParentEmail == parentemailresults[i].EmailAddress);
                var dates = childemailsForthisParent.FindAll(t => t.LastEmailSentDate == parentemailresults[i].LastEmailSentDate);
                Assert.AreEqual(childemailsForthisParent.Count, dates.Count);
            }
        
        }
        [TestMethod]
        public void ValidateMatchPairCopyToNonMatchPair()
        {
            MatchPairDBOperations oprations = new MatchPairDBOperations();
            var results = oprations.GetDistributionParticipantsForChildTab(28722, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName);
            var error = oprations.ValidateDestinationDistributionParticipants(30794, DBConnectionParams.userName, DBConnectionParams.password, DBConnectionParams.TCESserverName, results);
            Assert.AreEqual(0, error);

        }
    }
}
