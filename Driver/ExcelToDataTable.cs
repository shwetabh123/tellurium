﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using Aspose.Cells.ExternalConnections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;
using Aspose.Cells;
using System.Collections;


namespace Driver
{
    [TestClass]
    public class ExcelToDataTable
    {

        public static ExcelToDataTable xl;
        public static HoverTxtDto HoverTxt;

        public HoverTxtDto readXl(string questionType)
        {
            string sConnect = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Automation_C#\\Tellurium\\DataSet\\Reverify.xlsx;Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\"";

            string startupPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName; 

            //Workbook workbook1 = new Workbook(@"C:\\Automation_C#\\Tellurium\\DataSet\\Reverify.xlsx");
            Workbook workbook = new Workbook(startupPath+"\\Reverify.xlsx");
            Worksheet firstSheet = workbook.Worksheets[0];
            Aspose.Cells.Row firstRow = firstSheet.Cells.GetRow(0);
            int maxRow = firstSheet.Cells.MaxDataRow;
            int maxColumn = firstSheet.Cells.MaxDataColumn;

            DataSet ds = new DataSet();
            DataTable dtSheet = new DataTable();
            dtSheet = firstSheet.Cells.ExportDataTableAsString(0, 0, maxRow + 1, maxColumn + 1);
 
            Console.WriteLine(dtSheet.Columns.Count);
            for (int i = 0; i < dtSheet.Rows.Count; i++)
            {
                string v = dtSheet.Rows[i][1].ToString();
                if (dtSheet.Rows[i][1].ToString().Equals(questionType))
                {
                    HoverTxt = new HoverTxtDto
                    {
                        formName = dtSheet.Rows[i][0].ToString(),
                        questionType = dtSheet.Rows[i][1].ToString(),
                        questionTxt = dtSheet.Rows[i][2].ToString(),
                        hoverTxt = dtSheet.Rows[i][3].ToString(),
                        alterHoverTxt = dtSheet.Rows[i][4].ToString(),
                        saveBtn = dtSheet.Rows[i][5].ToString(),
                        QuestionTextEditorElement = dtSheet.Rows[i][6].ToString(),
                        HoverElement = dtSheet.Rows[i][7].ToString(),
                        HoverTextEditorElement = dtSheet.Rows[i][8].ToString(),
                        okBtn = dtSheet.Rows[i][9].ToString()
                    };
                    break;
                   
                   
                }
            }
            return HoverTxt;
        }
        
        //[TestMethod]
        public void writeXl(string result1, string result2, bool comparison, string questionType)
        {

            string startupPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
           // Workbook wb = new Workbook(@"C:\\Automation_C#\\Tellurium\\DataSet\\Reverify.xlsx");
            Workbook wb = new Workbook(startupPath + "\\Reverify.xlsx");
            Worksheet sheet = wb.Worksheets[0];
            Cells cells = sheet.Cells;
            int maxRow = cells.MaxDataRow + 1;
            int maxCol = cells.MaxDataColumn + 1;
            for(int i=1; i <= maxRow; i++)
            {
                    if (sheet.Cells[i,1].GetStringValue(CellValueFormatStrategy.CellStyle).Equals(questionType))
                    {
                        Cell cell = sheet.Cells[i, 5];
                    sheet.Cells[i, 10].PutValue(result1);
                    sheet.Cells[i, 11].PutValue(result2);
                    sheet.Cells[i, 12].PutValue(comparison);
                    wb.Save(@"C:\\Automation_C#\\Tellurium\\DataSet\\Reverify.xlsx");
                        break;
                    }
            }
        }


        // [TestMethod]
        public void readXl2()
        {
            string sConnect = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Automation_C#\\Tellurium\\DataSet\\Reverify.xlsx;Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\"";

            Workbook workbook = new Workbook(@"C:\\Automation_C#\\Tellurium\\DataSet\\Reverify.xlsx");
            Worksheet firstSheet = workbook.Worksheets[0];
            Row firstRow = firstSheet.Cells.GetRow(0);
            int maxRow = firstSheet.Cells.MaxDataRow;
            int maxColumn = firstSheet.Cells.MaxDataColumn;
            
            DataSet ds = new DataSet();
            DataTable dtSheet = new DataTable();
            dtSheet = firstSheet.Cells.ExportDataTableAsString(0, 0, maxRow + 1, maxColumn + 1);
            //using (OleDbConnection conn = new OleDbConnection(sConnect))
            //{
            //    conn.Open();
            //    OleDbCommand cmd = new OleDbCommand();
            //    cmd.Connection = conn;

            //    // Get all Sheets in Excel File
            //    dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            //    // Loop through all Sheets to get data
            //    foreach (DataRow dr in dtSheet.Rows)
            //    {
            //        string sheetName = dr["TABLE_NAME"].ToString();

            //        if (!sheetName.EndsWith("$"))
            //            continue;

            //        // Get all rows from the Sheet
            //        cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

            //        dtSheet.TableName = sheetName;
            //        Console.WriteLine("sheetname" + dtSheet.TableName);
            //        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            //        da.Fill(dtSheet);
            //    }
            Console.WriteLine(dtSheet.Columns.Count);
            for (int i = 0; i < dtSheet.Rows.Count; i++)
                    {
                        string v = dtSheet.Rows[i][1].ToString();
                        if (dtSheet.Rows[i][1].ToString().Equals("RatingScale"))
                        {
                            //for (int j = 9; j < dtSheet.Columns.Count; j++)
                            //{
                                HoverTxt = new HoverTxtDto
                                {
                                    formName = dtSheet.Rows[i][0].ToString(),
                                    questionType = dtSheet.Rows[i][1].ToString(),
                                    questionTxt = dtSheet.Rows[i][2].ToString(),
                                    hoverTxt = dtSheet.Rows[i][3].ToString(),
                                    alterHoverTxt = dtSheet.Rows[i][4].ToString()
                            };
                            //}
                            break;
                    }
                }

            //}

            //cmd = null;
            //conn.Close();
        }
    
    public void readXl1()
    {

        string sSheetName = null;
        //string sConnection = null;
        DataTable dtTablesList = default(DataTable);
        OleDbCommand oleExcelCommand = default(OleDbCommand);
        OleDbDataReader oleExcelReader = default(OleDbDataReader);
        OleDbConnection oleExcelConnection = default(OleDbConnection);

        string sConnect = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Automation_C#\\Tellurium\\DataSet\\Reverify.xlsx;Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\"";
        //oleExcelConnection = new OleDbConnection(sConnect);
        //oleExcelConnection.Open();
        //OleDbDataAdapter da = new OleDbDataAdapter(oleExcelCommand);

        //dtTablesList = oleExcelConnection.GetSchema("Tables");
        //DataTable dt = new DataTable();
        //DataSet ds = new DataSet();

        //if(dtTablesList.Rows.Count > 0)
        //{
        //    sSheetName = dtTablesList.Rows[0]["TABLE_NAME"].ToString();
        //}

        //dtTablesList.Clear();
        //dtTablesList.Dispose();


        //if (!string.IsNullOrEmpty(sSheetName))
        //{
        //    oleExcelCommand = oleExcelConnection.CreateCommand();
        //    oleExcelCommand.CommandText = "Select * From [" + sSheetName + "]";
        //    dt.TableName = sSheetName;
        //    oleExcelCommand.CommandType = CommandType.Text;
        //    oleExcelReader = oleExcelCommand.ExecuteReader();
        //    int nOutputRow = 0;

        //    while (oleExcelReader.Read())
        //    {
        //        da.Fill(dt);
        //        ds.Tables.Add(dt);
        //    }
        //    oleExcelReader.Close();
        //}
        //oleExcelConnection.Close();
        DataSet ds = new DataSet();

        // string connectionString = GetConnectionString();
        DataTable dtSheet;

        using (OleDbConnection conn = new OleDbConnection(sConnect))
        {
            conn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = conn;

            // Get all Sheets in Excel File
            dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            // dtSheet.Rows[0].Delete();
            // Loop through all Sheets to get data
            foreach (DataRow dr in dtSheet.Rows)
            {

                //Console.WriteLine("row count" + dtSheet.Rows.Count);
                //Console.WriteLine("ColumnCount" + dtSheet.Columns.Count);
                string sheetName = dr["TABLE_NAME"].ToString();

                if (!sheetName.EndsWith("$"))
                    continue;

                // Get all rows from the Sheet
                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                dtSheet.TableName = sheetName;
                Console.WriteLine("sheetname" + dtSheet.TableName);
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dtSheet);
                //Console.WriteLine("row count" + dtSheet.Rows.Count);
                //Console.WriteLine("ColumnCount" + dtSheet.Columns.Count);
                foreach (DataColumn col in dtSheet.Columns)
                {
                    //   Console.WriteLine("ColumnCount" + col.ColumnName);
                }
                foreach (DataRow r in dtSheet.Rows)
                {

                    //Console.WriteLine(r["F1"].ToString());
                    //Console.WriteLine(r["F2"].ToString());
                }
                for (int i = 2; i < dtSheet.Rows.Count; i++)
                {
                    for (int j = 9; j < dtSheet.Columns.Count; j++)
                    {
                        string v = dtSheet.Rows[i][10].ToString();
                        if (dtSheet.Rows[i][10].ToString().Equals("RatingScale"))
                        {
                            Console.WriteLine(dtSheet.Rows[i][j].ToString());
                        }
                        else
                            i++;


                    }
                    //Console.WriteLine(dtSheet.Rows[i][9].ToString());
                    //Console.WriteLine(r["F2"].ToString());
                }

            }
            //foreach (DataRow dr in dtSheet.Rows)
            //{
            //    foreach (var item in dr.ItemArray)
            //    {
            //        Console.WriteLine(item);
            //        Console.WriteLine(dr["F1"].ToString());
            //    }
            //}
            //foreach (DataRow row in dtSheet.Rows)
            //{
            //    //row["Formname"].ToString();
            //    string description = dtSheet.Rows[1][2].ToString();
            //    //row["QuestionType"].ToString();
            //    // string icoFileName =
            //    //row["QuestionTxt"].ToString();
            //    //string installScript =
            //    //row["HoverTxt"].ToString();
            //}
            cmd = null;
            conn.Close();


        }

        // return ds;
    }

}
}
