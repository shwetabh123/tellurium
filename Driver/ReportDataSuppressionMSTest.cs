﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Data;
using Common;

namespace Driver
{
    [TestClass]
    public class ReportDataSuppressionMSTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string SPColor;
        public static string CompletionRuleFlag;
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";
        /// QA Parameters
        public static string TrendCategoryID = "17318";
        public static string TrendItemID = "46720";
        public static string TrendNRQuestionID = "46716";
        public static string TrendNRScaleOptionID = "432590";
        /// </summary>

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
            SPColor = context.Properties["SPColor"].ToString();
            CompletionRuleFlag = context.Properties["CompletionRuleFlag"].ToString();
            DataPopulationForReportDataValidation.PopulateData(DriverAndLogin.Url, DBConnectionParameters);
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        [TestMethod]
        public void ValidateSuppressionForInsightReport_Percentage_Category()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForInsightReport_Percentage_Category", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetCategoryItemCount(row["SurveyFormID"].ToString(), DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Target_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForInsightReport_Percentage_Item()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForInsightReport_Percentage_Item", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetCategoryItemCount(row["SurveyFormID"].ToString(), DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Target_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForInsightReport_Average_Category()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForInsightReport_Average_Category", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetCategoryItemCount(row["SurveyFormID"].ToString(), DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Target_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForInsightReport_Average_Item()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForInsightReport_Average_Item", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetCategoryItemCount(row["SurveyFormID"].ToString(), DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Target_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForInsightReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForDrillDownReport_Percentage_Category()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "USP_DrillDownReport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForDrillDownReport_Average_Category()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "USP_DrillDownReport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForDrillDownReport_Percentage_Item()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "USP_DrillDownReport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForDrillDownReport_Average_Item()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "USP_DrillDownReport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[TargetIndex], ReportDisplay, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp1_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp2_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForDrillDownReport(ResultSet, datasets[Comp3_Index], ReportDisplay, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Year_EntireSurvey_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Year_EntireSurvey_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);

                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Month_EntireSurvey_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Month_EntireSurvey_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Quarter_EntireSurvey_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Quarter_EntireSurvey_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Distribution_EntireSurvey_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Distribution_EntireSurvey_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "distribution";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Year_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Year_EntireSurvey_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Month_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Month_EntireSurvey_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Quarter_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Quarter_EntireSurvey_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Distribution_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Distribution_EntireSurvey_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "distribution";
                string Show = "survey";
                string ItemOrCategory = "1 = 1";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Year_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Year_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Month_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Month_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Quarter_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Quarter_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Distribution_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Distribution_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "distribution";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Year_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Year_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Month_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Month_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Quarter_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Quarter_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Distribution_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Distribution_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "distribution";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Year_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Year_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Month_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Month_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Quarter_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Quarter_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Distribution_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Distribution_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "distribution";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Year_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Year_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Month_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Month_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Quarter_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Quarter_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendChart_RatingScale_Distribution_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendChart_RatingScale_Distribution_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "distribution";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp1_Index], Show, DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        DataSet ResultSet = null;
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp2_Index], Show, DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        DataSet ResultSet = null;
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendChartRatingScaleReport(ResultSet, datasets[Comp3_Index], Show, DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Year_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Year_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "year";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Month_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Month_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "month";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "quarter";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Category_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "distribution";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Year_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Year_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "year";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Month_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Month_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "month";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "quarter";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Category_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "distribution";
                string Show = "Category";
                string ItemOrCategory = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", ItemOrCategory, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Year_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Year_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "year";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Month_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Month_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "month";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "quarter";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Item_Percentage", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "distribution";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Year_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Year_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "year";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Month_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Month_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "month";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Quarter_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "quarter";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("Executing Test Method ValidateSuppressionForTrendHeatMap_RatingScale_Distribution_Item_Average", w);
                    ReportDataValidation.Log("------------------------------------------------------------------------------------------------", w);
                }
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string GroupBy = "distribution";
                string Show = "Item";
                string ItemOrCategory = TrendItemID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, ItemOrCategory, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, ItemOrCategory, DBConnectionParameters);
                    }
                }
                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }
                    bool Target_Supp = true;
                    bool Comp_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    ReportDataSuppression.GetItemCountForTrendGroups(row["SurveyFormID"].ToString(), Show, ItemOrCategory, DBConnectionParameters);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        DataSet ResultSet = null;
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                        do
                        {
                            ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                            if (ResultSet.Tables.Count > 0)
                                break;
                        } while (true);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[TargetIndex], Show, DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        DataSet ResultSet = null;
                        int Comp_Index = 1;
                        string Comp_NSize = null;
                        for (int j = 1; j < TestSPIDFromScenarios.Tables[0].Rows.Count; j++)
                        {
                            Comp_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString()) - 1;
                            Comp_NSize = TestSPIDFromScenarios.Tables[0].Rows[j]["" + CompletionRuleFlag + ""].ToString();
                            if (datasets[Comp_Index].Tables[0].Columns.Contains("BenchmarkFavPerc") == true)
                            {
                                ReportDataSuppression.UpdateGroupMinimumSize(Comp_NSize, row["SurveyFormID"].ToString(), DBConnectionParameters);
                                do
                                {
                                    ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, ItemOrCategory, GroupBy);
                                    if (ResultSet.Tables.Count > 0)
                                        break;
                                } while (true);
                                Comp_Supp = ReportDataSuppression.CompareSuppressionForHeatMapRatingScaleReport(ResultSet, datasets[Comp_Index], Show, DataCalculation, Int32.Parse(Comp_NSize), 1);
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    ReportDataValidation.Log("Comparison ID : " + TestSPIDFromScenarios.Tables[0].Rows[j]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                                    if (Comp_Supp == true)
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                                    else
                                        ReportDataValidation.Log("Suppression comparison status of Comparison for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                                }
                                break;
                            }
                        }
                    }

                    if ((Target_Supp && Comp_Supp) == false)
                        AssertStatus = false;
                    loopcnt++;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForResultReport_Percentage_Item()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Percentage";
                string ReportSPName = "USP_GN_ResultsReport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetResultReportData(ReportFilters, DBConnectionParameters);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyformID"].ToString(), DBConnectionParameters,Target_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[TargetIndex], DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyformID"].ToString(), DBConnectionParameters, Comp1_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[Comp1_Index], DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyformID"].ToString(), DBConnectionParameters, Comp2_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[Comp2_Index], DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyformID"].ToString(), DBConnectionParameters, Comp3_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[Comp3_Index], DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForResultReport_Average_Item()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string ReportDisplay = "Item";
                string DataCalculation = "Average";
                string ReportSPName = "USP_GN_ResultsReport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetResultReportData(ReportFilters, DBConnectionParameters);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true;
                    bool Comp1_Supp = true;
                    bool Comp2_Supp = true;
                    bool Comp3_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyformID"].ToString(), DBConnectionParameters, Target_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Target_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[TargetIndex], DataCalculation, Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyformID"].ToString(), DBConnectionParameters, Comp1_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[Comp1_Index], DataCalculation, Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyformID"].ToString(), DBConnectionParameters, Comp2_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[Comp2_Index], DataCalculation, Int32.Parse(Comp2_NSize), 2);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyformID"].ToString(), DBConnectionParameters, Comp3_NSize);
                        DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForResultReport(ResultSet, datasets[Comp3_Index], DataCalculation, Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendNonRatingScale_Year()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleoptionID = TrendNRScaleOptionID;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend_NR(ReportFilters, "46716", "432590", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "year", QuestionID, DBConnectionParameters);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true; bool Target_Heap_Supp = true;
                    bool Comp1_Supp = true; bool Comp1_Heap_Supp = true;
                    bool Comp2_Supp = true; bool Comp2_Heap_Supp = true;
                    bool Comp3_Supp = true; bool Comp3_Heap_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[TargetIndex],Int32.Parse(Target_NSize), 0);
                        Target_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[TargetIndex], Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Target_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);

                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        Comp1_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp1_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 2);
                        Comp2_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp2_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Year");
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        Comp3_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + HeapReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);
                        
                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) && (Target_Heap_Supp && Comp1_Heap_Supp && Comp2_Heap_Supp && Comp3_Heap_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendNonRatingScale_Quarter()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleoptionID = TrendNRScaleOptionID;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend_NR(ReportFilters, "46716", "432590", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "Quarter", QuestionID, DBConnectionParameters);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true; bool Target_Heap_Supp = true;
                    bool Comp1_Supp = true; bool Comp1_Heap_Supp = true;
                    bool Comp2_Supp = true; bool Comp2_Heap_Supp = true;
                    bool Comp3_Supp = true; bool Comp3_Heap_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[TargetIndex], Int32.Parse(Target_NSize), 0);
                        Target_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[TargetIndex], Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Target_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);

                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        Comp1_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp1_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + Comp1_Heap_Supp + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + Comp1_Heap_Supp + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 2);
                        Comp2_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp2_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Quarter");
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        Comp3_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);

                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) && (Target_Heap_Supp && Comp1_Heap_Supp && Comp2_Heap_Supp && Comp3_Heap_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]
        public void ValidateSuppressionForTrendNonRatingScale_Month()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleoptionID = TrendNRScaleOptionID;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend_NR(ReportFilters, "46716", "432590", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "Month", QuestionID, DBConnectionParameters);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true; bool Target_Heap_Supp = true;
                    bool Comp1_Supp = true; bool Comp1_Heap_Supp = true;
                    bool Comp2_Supp = true; bool Comp2_Heap_Supp = true;
                    bool Comp3_Supp = true; bool Comp3_Heap_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[TargetIndex], Int32.Parse(Target_NSize), 0);
                        Target_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[TargetIndex], Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Target_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);

                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        Comp1_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp1_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + Comp1_Heap_Supp + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + Comp1_Heap_Supp + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 2);
                        Comp2_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp2_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Month");
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        Comp3_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);

                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) && (Target_Heap_Supp && Comp1_Heap_Supp && Comp2_Heap_Supp && Comp3_Heap_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateSuppressionForTrendNonRatingScale_Distribution()
        {
            try
            {
                int ScenarioCount = 0;
                bool AssertStatus = true;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleoptionID = TrendNRScaleOptionID;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend_NR(ReportFilters, "46716", "432590", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "Distribution", QuestionID, DBConnectionParameters);
                    }
                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    bool Target_Supp = true; bool Target_Heap_Supp = true;
                    bool Comp1_Supp = true; bool Comp1_Heap_Supp = true;
                    bool Comp2_Supp = true; bool Comp2_Heap_Supp = true;
                    bool Comp3_Supp = true; bool Comp3_Heap_Supp = true;
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters, CompletionRuleFlag);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount > 0)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString()) - 1;
                        string Target_NSize = TestSPIDFromScenarios.Tables[0].Rows[0]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Target_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        Target_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[TargetIndex], Int32.Parse(Target_NSize), 0);
                        Target_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[TargetIndex], Int32.Parse(Target_NSize), 0);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (Target_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Target_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = PASS", w);

                            else
                                ReportDataValidation.Log("Suppression comparison status of Target for " + HeapReportSPName + "_" + SPColor + " = FAIL", w);

                        }
                    }
                    if (TestSPIDFromScenariosCount > 1)
                    {
                        int Comp1_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString()) - 1;
                        string Comp1_NSize = TestSPIDFromScenarios.Tables[0].Rows[1]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp1_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        Comp1_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        Comp1_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp1_Index], Int32.Parse(Comp1_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp1_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp1_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + Comp1_Heap_Supp + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 1 for " + Comp1_Heap_Supp + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 2)
                    {
                        int Comp2_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString()) - 1;
                        string Comp2_NSize = TestSPIDFromScenarios.Tables[0].Rows[2]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp2_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        Comp2_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 2);
                        Comp2_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp2_Index], Int32.Parse(Comp2_NSize), 1);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp2_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp2_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 2 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                        }
                    }
                    if (TestSPIDFromScenariosCount > 3)
                    {
                        int Comp3_Index = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString()) - 1;
                        string Comp3_NSize = TestSPIDFromScenarios.Tables[0].Rows[3]["" + CompletionRuleFlag + ""].ToString();
                        ReportDataSuppression.UpdateGroupMinimumSize(Comp3_NSize, row["SurveyformID"].ToString(), DBConnectionParameters);
                        DataSet ResultSet_Chart = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        DataSet ResultSet_Heap = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "432590", "Distribution");
                        Comp3_Supp = ReportDataSuppression.CompareSuppressionForTrendNonRating(ResultSet_Chart, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        Comp3_Heap_Supp = ReportDataSuppression.CompareSuppressionForHeapTrendNonRating(ResultSet_Heap, datasets[Comp3_Index], Int32.Parse(Comp3_NSize), 3);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["ScenarioID"].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);
                            if (Comp3_Heap_Supp == true)
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = PASS", w);
                            else
                                ReportDataValidation.Log("Suppression comparison status of Comparison 3 for " + ReportSPName + "_" + SPColor + " = FAIL", w);

                        }
                    }
                    if ((Target_Supp && Comp1_Supp && Comp2_Supp && Comp3_Supp) && (Target_Heap_Supp && Comp1_Heap_Supp && Comp2_Heap_Supp && Comp3_Heap_Supp) == false)
                        AssertStatus = false;
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }
    }
}
