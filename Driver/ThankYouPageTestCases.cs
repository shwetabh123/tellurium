﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Reflection;
using System.Threading;
using System.Data;
using OpenQA.Selenium.Interactions;


namespace Driver
{
    [TestClass]
    public class ThankYouPageTestCases
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        private static string FormName;
        public static string FileName;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = "2A8F",
            };
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        [TestMethod]
        public void VerifyRedirectURLinpreview()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck_RedirectURL");
                AuthorActions.EditPublishedForm(driver);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                AuthorActions.ThankYouPageRedirectURL(driver);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                String CopiedRedirectURL = AuthorActions.CopyRedirectURL(driver);
                IWebElement ThankyouCancelButton = driver.FindElement(By.Id("btnThankYouPageCancel"));
                ThankyouCancelButton.Click();
                driver.FindElementById("btnFullPreview").Click();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(10000);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                Assert.IsTrue(driver.Url.Equals(CopiedRedirectURL));

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [TestMethod]
        public void VerifyRedirectURLEdit()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck_RedirectURL");
                Thread.Sleep(2000);
                AuthorActions.EditPublishedForm(driver);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                AuthorActions.ThankYouPageRedirectURL(driver);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                Boolean edit = AuthorActions.EditandUpdateThankYouPageRedirectURL(driver);
                Assert.IsTrue(edit.Equals("true"));
                driver.FindElementById("btnFullPreview").Click();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(10000);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                Assert.IsTrue(driver.Url.Equals("https://www.quora.com"));

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void VerifyRedirectURLforOpenLinkinplayer()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck_RedirectURL");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(6000);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                AuthorActions.ThankYouPageRedirectURL(driver);
                Thread.Sleep(5000);
                AuthorActions.PublishEditedForm(driver);
                Thread.Sleep(10000);
                DistributeActions.NavigateToManageDistrbution(driver);
                Thread.Sleep(4000);
                DistributeActions.CreateDistributionFromDistributePage(driver, new DistributionParametersDTO
                {
                    FormName = "ThankYouPageCheck",
                    DistributionName = "ThankYouPageCheck_RedirectURL" + Guid.NewGuid(),
                    StartDate = "07/23/2018 10:00 PM",
                    StartTimeZone = "Chennai",
                    EndDate = "05/28/2022 10:00 PM",
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Schedule Emails", "Share Manually" }
                });
                IWebElement NextButton = driver.FindElement(By.Id("btn-next"));
                NextButton.Click();
                Thread.Sleep(8000);
                IWebElement NextButton1 = driver.FindElement(By.Id("btn-next"));
                NextButton.Click();
                Thread.Sleep(8000);
                IWebElement CopyOpenLink = driver.FindElement(By.Id("btn-copy-url"));
                CopyOpenLink.Click();
                String URL = System.Windows.Forms.Clipboard.GetText();
                UIActions.OpenNewTab(driver, URL);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                Assert.IsTrue(driver.Url.Equals("https://www.google.com/"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void VerifyRedirectURLforUniqueLinkinplayer()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck_RedirectURL");
                Thread.Sleep(2000);
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(2000);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                AuthorActions.ThankYouPageRedirectURL(driver);
                Thread.Sleep(2000);
                AuthorActions.PublishEditedForm(driver);
                Thread.Sleep(5000);
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.CreateDistributionFromDistributePage(driver, new DistributionParametersDTO
                {
                    FormName = "ThankYouPageCheck",
                    DistributionName = "ThankYouPageCheck" + Guid.NewGuid(),
                    StartDate = "07/23/2018 10:00 PM",
                    StartTimeZone = "Chennai",
                    EndDate = "05/28/2022 10:00 PM",
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Schedule Emails", "Share Manually" }
                });
                IWebElement NextButton = driver.FindElement(By.Id("btn-next"));
                NextButton.Click();
                Thread.Sleep(6000);
                DistributeActions.AddParticipantsFromDirectoryTab(driver, 1);
                IWebElement emailaddress = driver.FindElement(By.XPath("//span[@class='email']"));
                DistributeActions.CopyUniqueLinkBySearchParticipantInParticipantGrid(driver, emailaddress.Text);
                String URL = System.Windows.Forms.Clipboard.GetText();
                UIActions.OpenNewTab(driver, URL);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                Assert.IsTrue(driver.Url.Equals("https://www.google.com/"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [TestMethod]
        public void CopySurveyWithThankYouPage()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(5000);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                string CopiedRedirectURL = AuthorActions.CopyRedirectURL(driver);
                IWebElement ThankyouCancelButton = driver.FindElement(By.Id("btnThankYouPageCancel"));
                ThankyouCancelButton.Click();
                AuthorActions.PublishEditedForm(driver);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                Thread.Sleep(5000);
                AuthorActions.CopyForm(driver, "ThankYouPageCheck");
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                string CopiedFormRedirectURL = AuthorActions.CopyRedirectURL(driver);
                Assert.IsTrue(CopiedFormRedirectURL.Contains(CopiedRedirectURL));
                IWebElement ThankyouCancelButton1 = driver.FindElement(By.Id("btnThankYouPageCancel"));
                ThankyouCancelButton1.Click();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
        [TestMethod]
        public void ExitandContinueinPlayer()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndOpenDistribution(driver, "ThankYouPageCheck708ea830-0eec-429d-80f3-78d63ec9bf62");
                IWebElement CopyOpenLink = driver.FindElement(By.Id("btn-copy-url"));
                CopyOpenLink.Click();
                String URL = System.Windows.Forms.Clipboard.GetText();
                UIActions.OpenNewTab(driver, URL);
                IWebElement ExitandContinue = driver.FindElement(By.XPath("//*[@class='dropdown-toggle blue tp-header-exit dropdown-toggle']"));
                ExitandContinue.Click();
                IWebElement ExitandContinueOk = driver.FindElement(By.XPath("(//*[@class='btn btn-primary confirmExit margin-left-1'])[2]"));
                ExitandContinueOk.Click();
                Assert.IsTrue(!(driver.Url.Equals("https://www.google.com")));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void ExitandContinueinPreview()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                AuthorActions.ThankYouPageRedirectURL(driver);
                driver.FindElementById("btnFullPreview").Click();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                //IWebElement SurveyTitle = driver.FindElementById("txtSurveyNameTitle");
                Thread.Sleep(10000);
                IWebElement ExitandContinue = driver.FindElement(By.XPath("//*[@class='dropdown-toggle blue tp-header-exit dropdown-toggle']"));
                ExitandContinue.Click();
                IWebElement ExitandContinueOk = driver.FindElement(By.XPath("(//*[@class='btn btn-primary confirmExit margin-left-1'])[2]"));
                ExitandContinueOk.Click();
                Assert.IsTrue(!(driver.Url.Equals("https://www.google.com")));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void MetaTagsVerificationinPlayer()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                Thread.Sleep(5000);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(5000);
                AuthorActions.EnableThankYouPageEdit(driver, "Customize Text");
                Thread.Sleep(4000);
                if (UIActions.IsElementVisible(driver, "//*[contains(text(),'hire_-date')]"))
                {
                    Thread.Sleep(2000);
                    driver.FindElement(By.Id("btnThankYouPageCancel")).Click();
                    AuthorActions.PublishEditedForm(driver);
                    DistributeActions.NavigateToManageDistrbution(driver);
                    DistributeActions.SearchAndOpenDistribution(driver, "ThankYouPageCheck708ea830-0eec-429d-80f3-78d63ec9bf62");
                    DistributeActions.AddParticipantsFromDirectoryTab(driver, 2);
                    Thread.Sleep(10000);
                    // DistributeActions.CopyUniqueLink(driver);
                    var emailaddress = driver.FindElements(By.XPath("//span[@class='email']"));
                    var demo = driver.FindElements(By.XPath("//*[@class='demo-column demo2']"));
                    int count = demo.Count();
                    string[] demovalues = new string[count];
                    int i = 0;
                    foreach (var demo1 in demo)
                    {
                        demovalues[i] = demo1.Text;
                        i++;
                    }
                    Console.WriteLine(demo);
                    foreach (var email1 in emailaddress)
                    {
                        if (!(email1.Text.Equals(" ")))
                        {
                            int j = 0;
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                            DistributeActions.CopyUniqueLinkBySearchParticipantInParticipantGrid(driver, email1.Text);
                            UIActions.OpenNewTab(driver, System.Windows.Forms.Clipboard.GetText());
                            PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                            PlayerActions.CheckMetaTagReplacementinPlayerorPreview(driver, ".content-inner", demovalues[j]);
                            driver.Close();
                            //Assert.IsTrue()
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
        [TestMethod]
        public void ContentVerificationinCustomThankyouPreview()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(3000);
                AuthorActions.EnableThankYouPageEdit(driver, "Customize Text");
                Thread.Sleep(3000);
                IWebElement CustomizeText = driver.FindElement(By.XPath("//*[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']"));
                CustomizeText.Click();
                AuthorActions.CopyCustomizedThankYouContent(driver);
                String ThankYou = System.Windows.Forms.Clipboard.GetText();
                driver.FindElementById("btnFullPreview").Click();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                //IWebElement SurveyTitle = driver.FindElementById("txtSurveyNameTitle");
                Thread.Sleep(10000);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                PlayerActions.CompareThankYouPageContent(driver, ".content - inner", ThankYou);
                UIActions.MouseHoverToElementAndClick(driver, "//*[@href='https://www.google.com']");
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Assert.IsTrue(driver.Url.Equals("https://www.google.com/"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void ContentVerificationinCustomThankyouPlayer()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(3000);
                AuthorActions.EnableThankYouPageEdit(driver, "Customize Text");
                OpenQA.Selenium.Support.UI.WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromMinutes(2));
                wait.Until(OpenQA.Selenium.Support.UI.ExpectedConditions.ElementToBeClickable(
                driver.FindElement(By.XPath("//*[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']"))));
                AuthorActions.CopyCustomizedThankYouContent(driver);
                String ThankYou = System.Windows.Forms.Clipboard.GetText();
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndOpenDistribution(driver, "ThankYouPageCheck708ea830-0eec-429d-80f3-78d63ec9bf62");
                DistributeActions.AddParticipantsFromDirectoryTab(driver, 2);
                String URL = System.Windows.Forms.Clipboard.GetText();
                var emailaddress = driver.FindElements(By.XPath("//span[@class='email']"));
                bool flag1 = false;
                foreach (var email1 in emailaddress)
                {
                    DistributeActions.CopyUniqueLinkBySearchParticipantInParticipantGrid(driver, email1.Text);
                    UIActions.OpenNewTab(driver, URL);
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    Thread.Sleep(10000);
                    PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                    flag1 = PlayerActions.CompareThankYouPageContent(driver, ".content - inner", ThankYou);
                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }
                Assert.IsTrue(flag1);
                //IWebElement SurveyTitle = driver.FindElementById("txtSurveyNameTitle");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void BoundaryValidationOfThankYouPageContent()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(5000);
                AuthorActions.EnableThankYouPageEdit(driver, "Customize Text");
                Thread.Sleep(5000);
                IWebElement CustomizedText = driver.FindElement(By.XPath("//*[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']"));
                CustomizedText.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                CustomizedText.SendKeys(OpenQA.Selenium.Keys.Delete);
                for (int i = 0; i < 750; i++)
                    CustomizedText.SendKeys("test");
                CustomizedText.SendKeys("string");
                AuthorActions.CopyCustomizedThankYouContent(driver);
                string ThankYouPageContent = System.Windows.Forms.Clipboard.GetText();
                Assert.IsFalse(ThankYouPageContent.Contains("string"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void TranslationCheckWithEmptyUploadFile()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(5000);
                //AuthorActions.TranslationTemplateDownload(driver);
                //IWebElement UploadFileBtn = UIActions.FindElementWithXpath(driver, "//span[@class='group-span-filestyle input-group-btn']");
                //IWebElement UploadFileBtn = UIActions.FindElementWithXpath(driver, "//*[contains(text(),' Upload Content')]");
                AuthorActions.NavigatetoLanguagespopup(driver);
                IWebElement UploadFileBtn = UIActions.FindElement(driver, "#select-upload-file");
                AuthorActions.TranslationTemplateUpload(driver, UploadFileBtn, @"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868.xlsx");
                Thread.Sleep(2000);
                driver.FindElementById("btnFullPreview").Click();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                //IWebElement SurveyTitle = driver.FindElementById("txtSurveyNameTitle");
                Thread.Sleep(10000);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                AuthorActions.CopyCustomizedThankYouContent(driver);
                PlayerActions.CompareThankYouPageContent(driver, ".content - inner", System.Windows.Forms.Clipboard.GetText());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void TranslationCheckCustomizedThankYouPageWithUploadFilePreview()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(5000);
                AuthorActions.NavigatetoLanguagespopup(driver);
                IWebElement UploadFileBtn = UIActions.FindElement(driver, "#select-upload-file");
                DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868_languagecheck.xlsx");
                string o = dt.Rows[18][3].ToString();
                Console.WriteLine(o);
                AuthorActions.TranslationTemplateUpload(driver, UploadFileBtn, @"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868_languagecheck.xlsx");
                Thread.Sleep(2000);
                driver.FindElementById("btnFullPreview").Click();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(10000);
                PlayerActions.TranslationLanguageClick(driver, " Afrikaans");
                Thread.Sleep(2000);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                bool flag = PlayerActions.CompareThankYouPageContent(driver, ".content-inner", o);
                Assert.IsTrue(flag);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void TranslationCheckCustomizedThankYouPageWithUploadFilePlayer()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(5000);
                AuthorActions.NavigatetoLanguagespopup(driver);
                IWebElement UploadFileBtn = UIActions.FindElement(driver, "#select-upload-file");
                DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868_languagecheck.xlsx");
                string o = dt.Rows[18][3].ToString();
                Console.WriteLine(o);
                AuthorActions.TranslationTemplateUpload(driver, UploadFileBtn, @"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868_languagecheck.xlsx");
                Thread.Sleep(2000);
                AuthorActions.PublishEditedForm(driver);
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndOpenDistribution(driver, "ThankYouPageCheck708ea830-0eec-429d-80f3-78d63ec9bf62");
                DistributeActions.AddParticipantsFromDirectoryTab(driver, 2);
                String URL = System.Windows.Forms.Clipboard.GetText();
                UIActions.OpenNewTab(driver, URL);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                //IWebElement SurveyTitle = driver.FindElementById("txtSurveyNameTitle");
                Thread.Sleep(10000);
                PlayerActions.TranslationLanguageClick(driver, " Afrikaans");
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                // AuthorActions.CopyCustomizedThankYouContent(driver);
                bool flag = PlayerActions.CompareThankYouPageContent(driver, ".content - inner", o);
                Assert.IsTrue(flag);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void RedirectURLTranslationCheckEmptyFile()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(10000);
                bool flag;
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                IWebElement ThankyouCancelButton = driver.FindElement(By.Id("btnThankYouPageCancel"));
                ThankyouCancelButton.Click();
                DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868 (10)_RedirectURL_OtherLanguage.xlsx");
                string o = dt.Rows[18][3].ToString();
                Console.WriteLine(o);
                if (o.Equals(" "))
                    flag = true;
                else
                    flag = false;
                Assert.IsTrue(flag);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [TestMethod]
        public void RedirectURLTranslationCheckFilewithPreviewandPlayer()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, "ThankYouPageCheck");
                if (UIActions.IsElementVisible(driver, "//*[@id='btnFormEdit']"))
                    AuthorActions.EditPublishedForm(driver);
                Thread.Sleep(10000);
                AuthorActions.EnableThankYouPageEdit(driver, "Redirect to a URL");
                AuthorActions.ThankYouPageRedirectURL(driver);
                AuthorActions.NavigatetoLanguagespopup(driver);
                IWebElement UploadFileBtn = UIActions.FindElement(driver, "#select-upload-file");
                DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868 (10)_RedirectURL.xlsx");
                string o = dt.Rows[18][3].ToString();
                Console.WriteLine(o);
                AuthorActions.TranslationTemplateUpload(driver, UploadFileBtn, @"C:\Workspace\Tellurium\DataSet\FormContentInformation_1500294868 (10)_RedirectURL.xlsx");
                Thread.Sleep(2000);
                AuthorActions.PublishEditedForm(driver);
                driver.FindElementById("btnFullPreview").Click();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                //IWebElement SurveyTitle = driver.FindElementById("txtSurveyNameTitle");
                Thread.Sleep(10000);
                PlayerActions.TranslationLanguageClick(driver, " Afrikaans");
                Thread.Sleep(2000);
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                bool flag1;
                if (driver.Title.Equals("https://www.google.com"))
                    flag1 = true;
                else
                    flag1 = false;
                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndOpenDistribution(driver, "ThankYouPageCheck708ea830-0eec-429d-80f3-78d63ec9bf62");
                DistributeActions.AddParticipantsFromDirectoryTab(driver, 2);
                String URL = System.Windows.Forms.Clipboard.GetText();
                UIActions.OpenNewTab(driver, URL);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                //IWebElement SurveyTitle = driver.FindElementById("txtSurveyNameTitle");
                Thread.Sleep(10000);
                PlayerActions.TranslationLanguageClick(driver, " Afrikaans");
                PlayerActions.NextorFinishButtonCheckinPrevieworPlayer(driver);
                // AuthorActions.CopyCustomizedThankYouContent(driver);
                bool flag2;
                if (driver.Title.Equals("https://www.google.com"))
                    flag2 = true;
                else
                    flag2 = false;
                bool flag3 = false;
                if (flag1 && flag2)
                    flag3 = true;
                Assert.IsTrue(flag3);
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }

        }
    }
}

