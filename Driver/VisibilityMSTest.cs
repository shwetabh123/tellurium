﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Driver.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using System.Data;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Windows.Forms;

namespace Driver
{
    [TestClass]
    public class VisibilityMSTest
    {
        public static bool IsChromeDriverAvailable;
        public static bool IsFirefoxDriverAvailable;
        public static bool IsIEDriverAvailable;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;
        private static string FormID;
        private static string BrowserURl = "http://localhost:3832/";
        private static SessionId SessionID = new SessionId("cfb65526b4b3b53bcc96f6911f61d6fa");
        public static string GFESurveyformId;
        public static string surveyID;
        public static string surveyform = "DemoAnalyzeEdit1";
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string VisibilityFormName = "Visibility" + Guid.NewGuid();
        public static string VisibilityDistributionName = "Visibility" + DateTime.Now;
        public static string distributionName = "Date Night 1";

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString()
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }


        public static RemoteWebDriver SetDriverAndNavigateToManageForms()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                AuthorActions.NavigateToAuthor(driver);
            }
            else if (driver.Title.Contains("Manage Surveys - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                AuthorActions.NavigateToAuthor(driver);
            }
            return driver;
        }
        public static RemoteWebDriver SetDriverAndNavigateToDistribute()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(10000);
                DistributeActions.NavigateToManageDistrbution(driver);
                Thread.Sleep(5000);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
                Thread.Sleep(5000);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigatetoAnalyze(string DistributionName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(30000);
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);


            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
            }

            return driver;
        }

        [TestMethod]
        public void ValidateScaleOptionVisibilityInFormAndPlayer_Hide()
        {
            try
            {
                string QuestionText = "Where does your annual income level fall (approximately)?";
                string[] ResponseOptions = { "$120,001 - $200,000", "$30,000 - $60,000", "$60,000 - $90,000", "$90,001 - $120,000", "Less than $30,000", "More than $200,000" };
                string ScaleOption = "Less than $30,000";
                DriverAndLogin.Browser = "Chrome";
                DriverAndLogin.Account = "Komitet Gosudarstvennoy Bezopasnosti";
                RemoteWebDriver driver = SetDriverAndNavigateToManageForms();
                Thread.Sleep(8000);
                AuthorActions.CreateForm(driver, VisibilityFormName);
                Thread.Sleep(8000);
                AuthorActions.AddDropDownQuestion(driver, QuestionText, ResponseOptions);
                Thread.Sleep(5000);
                IReadOnlyCollection<IWebElement> QuestionEdit = UIActions.FindElements(driver, ".action-question-edit");
                QuestionEdit.ElementAt(0).Click();
                Thread.Sleep(5000);
                IReadOnlyCollection<IWebElement> ResponseEdits = UIActions.FindElements(driver, "#btnHideResponse");
                ResponseEdits.ElementAt(4).Click();

                bool QuestionPreviewStatus = UIActions.IsOptionPresentInCEBDropDown(driver, "#dropDown-preview-pane .ceb-dropdown-container", ScaleOption);

                UIActions.Click(driver, "#btnDropDownSave");
                Thread.Sleep(5000);

                bool SurveyEditorStatus = UIActions.IsOptionPresentInCEBDropDown(driver, ".qs-section .ceb-dropdown-container", ScaleOption);

                UIActions.Click(driver, "#btnFullPreview");
                Thread.Sleep(8000);
                UIActions.SwitchToWindowWithTitle("Make Out | Pulse Survey", driver);

                bool PlayerPreviewStatus = UIActions.IsOptionPresentInCEBDropDown(driver, ".qs-section .ceb-dropdown-container", ScaleOption);

                driver.Close();
                UIActions.SwitchToWindowWithTitle("Manage Surveys - Pulse", driver);
                AuthorActions.PublishForm(driver, VisibilityFormName);
                DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
                {
                    FormName = VisibilityFormName,
                    DistributionName = VisibilityDistributionName,
                    StartDate = Convert.ToString(DateTime.UtcNow),
                    StartTimeZone = "Chennai",
                    EndDate = null,
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Schedule Emails", "Share Manually" }
                });
                UIActions.Click(driver, "#btn-next");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#btn-next");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#btn-copy-url");
                UIActions.OpenNewTab(driver, Clipboard.GetText());

                bool PlayerStatus = UIActions.IsOptionPresentInCEBDropDown(driver, ".qs-section .ceb-dropdown-container", ScaleOption);

                driver.Close();
                UIActions.SwitchToWindowWithTitle("Manage Surveys - Pulse", driver);
                Assert.IsTrue(!QuestionPreviewStatus && !SurveyEditorStatus && !PlayerPreviewStatus && !PlayerStatus);
            }
            catch (Exception e)
            {
                Assert.Fail();
                Console.WriteLine(e.Message);
            }
        }

        [TestMethod]
        public void ValidateScaleOptionVisibilityInFormAndPlayer_Show()
        {
            try
            {
                string ScaleOption = "Less than $30,000";
                DriverAndLogin.Browser = "Chrome";
                DriverAndLogin.Account = "Komitet Gosudarstvennoy Bezopasnosti";
                RemoteWebDriver driver = SetDriverAndNavigateToManageForms();
                Thread.Sleep(8000);
                AuthorActions.SearchAndOpenForm(driver, VisibilityFormName);
                Thread.Sleep(8000);
                AuthorActions.EditPublishedForm(driver);
                IReadOnlyCollection<IWebElement> QuestionEdit = UIActions.FindElements(driver, ".action-question-edit");
                QuestionEdit.ElementAt(0).Click();
                Thread.Sleep(5000);
                IReadOnlyCollection<IWebElement> ResponseEdits = UIActions.FindElements(driver, "#btnHideResponse");
                ResponseEdits.ElementAt(4).Click();

                bool QuestionPreviewStatus = UIActions.IsOptionPresentInCEBDropDown(driver, "#dropDown-preview-pane .ceb-dropdown-container", ScaleOption);

                UIActions.Click(driver, "#btnDropDownSave");
                Thread.Sleep(5000);

                bool SurveyEditorStatus = UIActions.IsOptionPresentInCEBDropDown(driver, ".qs-section .ceb-dropdown-container", ScaleOption);

                UIActions.Click(driver, "#btnFullPreview");
                Thread.Sleep(8000);
                UIActions.SwitchToWindowWithTitle("Make Out | Pulse Survey", driver);

                bool PlayerPreviewStatus = UIActions.IsOptionPresentInCEBDropDown(driver, ".qs-section .ceb-dropdown-container", ScaleOption);

                driver.Close();
                UIActions.SwitchToWindowWithTitle("Manage Surveys - Pulse", driver);
                AuthorActions.PublishForm(driver, VisibilityFormName);
                UIActions.Click(driver, "#publishPopUpModal .btn-secondary");
                Thread.Sleep(10000);
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndOpenDistribution(driver, VisibilityDistributionName);
                UIActions.Click(driver, "#btn-copy-url");
                UIActions.OpenNewTab(driver, Clipboard.GetText());

                bool PlayerStatus = UIActions.IsOptionPresentInCEBDropDown(driver, ".qs-section .ceb-dropdown-container", ScaleOption);

                driver.Close();
                UIActions.SwitchToWindowWithTitle("Manage Surveys - Pulse", driver);
                Assert.IsTrue(QuestionPreviewStatus && SurveyEditorStatus && PlayerPreviewStatus && PlayerStatus);
            }
            catch (Exception e)
            {
                Assert.Fail();
                Console.WriteLine(e.Message);
            }
        }



 
        [TestMethod]
        public void ValidateQuestionVisibilityInResult()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DistributionName = "Look Out";
            string RatingscaleQuestionText = "I feel Year end celebration in gartner is good";
            string NonRatingScaleQuestionText = "My benefits are Good";
            string CommentQuestiontext = "Please provide any additional comments you have concerning medical benefits";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(DistributionName);
            Thread.Sleep(5000);
            AnalyzeAction.NavigatetoDifferentTab(driver, "Results Summary");
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2016-12/12/2017");
            bool RatingScaleQuestion= AnalyzeAction.CheckForQuestionVisibilityInResultSummary(driver, RatingscaleQuestionText);
            bool NonRatingScaleQuestion= AnalyzeAction.CheckForQuestionVisibilityInResultSummary(driver, NonRatingScaleQuestionText);
            bool CommentQuestion= AnalyzeAction.CheckForQuestionVisibilityInResultSummary(driver, CommentQuestiontext);
            Assert.IsTrue(RatingScaleQuestion & NonRatingScaleQuestion & CommentQuestion);
            //Assert.IsFalse(CommentQuestion);

        }

        [TestMethod]
        public void ValidateQuestionVisibilityInDrillDown()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DistributionName = "Look Out";
            string QuestionText = "I feel Year end celebration in gartner is good";
            //string NonRatingScaleQuestionText = "My benefits are Good";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(DistributionName);
            Thread.Sleep(5000);
            AnalyzeAction.NavigatetoDifferentTab(driver, "Favorability");
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2016-12/12/2017");
           Assert.IsTrue(AnalyzeAction.ChecktheVisibilityinDrillDown(driver, QuestionText));
       }
        [TestMethod]
        public void ValidateQuestionVisibilityInTrendReport()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DistributionName = "Look Out";
            string QuestionText = "I feel Year end celebration in gartner is good";
            string NonRatingScale = "My benefits are Good";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(DistributionName);
            Thread.Sleep(5000);
            AnalyzeAction.NavigatetoDifferentTab(driver, "Trends");
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2016-12/12/2017");
            bool ratingscaleflag =AnalyzeAction.ChecktheVisibilityinTrendReport(driver, QuestionText);
            bool nonratingScaleflag= AnalyzeAction.ChecktheVisibilityinTrendReport(driver, NonRatingScale);
            Assert.IsTrue(ratingscaleflag & nonratingScaleflag);

        }

        [TestMethod]

        public void ValidateDemoQuestionIsVisibleinFilter()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DemoQuestion = "How long have you worked this company?";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(distributionName);
            Thread.Sleep(5000);
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2000-03/03/2002");
            bool DemoQuestionVisibility = AnalyzeAction.CheckHiddenDemoQuestionsdisplayinFilter(driver, DemoQuestion);
            Assert.IsTrue(DemoQuestionVisibility);
         }
        [TestMethod]
        public void ValidateDateDemoQuestionIsNotVisibleinFilter()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DemoQuestion = "Approximately how many sick days did you take last year?";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(distributionName);
            Thread.Sleep(5000);
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2000-03/03/2002");
            bool DemoQuestionVisibility = AnalyzeAction.CheckHiddenDemoQuestionsdisplayinFilter(driver, DemoQuestion);
            Assert.IsFalse(DemoQuestionVisibility);
        }
        [TestMethod]
        public void ValidateDateDemoQuestionIsNotVisibleinTimePeriod()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DemoQuestion = "Approximately how many sick days did you take last year?";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(distributionName);
            Thread.Sleep(5000);
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            bool DateDemoQuestionVisibility = AnalyzeAction.CheckDateDemoQuestionHiddeninTimePeriod(driver, DemoQuestion);
            Assert.IsTrue(DateDemoQuestionVisibility);
        }
           
        [TestMethod]
        public void ValidateDemoQuestionIsVisibleinCommentIdentifierDropdown()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DemoQuestion = "How long have you worked this company?";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(distributionName);
            Thread.Sleep(5000);
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2000-03/03/2002");
            UIActions.clickwithXapth(driver, "//span[@class='icon-angle-right']");
            AnalyzeAction.NavigatetoDifferentTab(driver, "Comments");
            IReadOnlyCollection<IWebElement> DropDownText = driver.FindElements(By.XPath("//*[@id='analyse-identifier']"));
            List<string> DemoQuestionText = new List<string>(DropDownText.Select(x => x.Text));
            Assert.IsTrue(DemoQuestionText.Any(x => x.Contains(DemoQuestion)));
        }

        [TestMethod]
        public void ValidateDemoQuestionIsVisibleinSegmentToWatch()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string SpotLigthDistributionName = "Phoenix";
            string DemoQuestion = "What is your race?";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(SpotLigthDistributionName);
            Thread.Sleep(5000);
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            //AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2000-03/03/2002");
            IReadOnlyCollection<IWebElement> DropDownText = driver.FindElements(By.XPath("//*[@class='multiselect-container dropdown-menu']//li//a//input[@type='radio']"));
            List<string> DemoQuestionText = new List<string>(DropDownText.Select(x => x.GetAttribute("value")));
            Assert.IsTrue(DemoQuestionText.Any(x => x.Contains(DemoQuestion)));

        }

        [TestMethod]

        public void ValidateHiddenQuestionIsVisibleinRawData()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string DemoQuestion = "How long have you worked this company?";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(distributionName);
            Thread.Sleep(5000);
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2000-03/03/2002");
            AnalyzeAction.RawdataExport(driver);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Assert.IsTrue(CommonActions.CheckExcelContains(@"C:\Automation\"+latestfile, DemoQuestion));
        }

        [TestMethod]

        public void ValidateHiddenScaleOptionIsVisibleinRawData()
        {
            DriverAndLogin.Account = "Mossad";
            DriverAndLogin.Browser = "Firefox";
            string distributionName = "Phoenix";
            string DemoResponse = "Prefer not to respond";
            RemoteWebDriver driver = SetDriverAndNavigatetoAnalyze(distributionName);
            Thread.Sleep(5000);
            AnalyzeAction.ExapandAndCollapseFilters(driver, "Survey Results");
            //AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "Custom Time Period", "02/02/2011-03/03/2018");
            AnalyzeAction.RawdataExport(driver);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Assert.IsTrue(CommonActions.CheckExcelContains(@"C:\Automation\" + latestfile, DemoResponse));
        }

        [TestMethod]
        public void ValidateQuestionVisibilityInInsight()
        {
            try
            {
                string DistributionName = "Make Out";
                string StrengthQuestion = "I feel my Company has good benefits.";
                string OppQuestion = "The Company benefits package was an important factor in my decision to join the Company.";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.DataCalculationSelection(driver, "Percentage");
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Questions");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                AnalyzeAction.ExpandAndCollapseFilters(driver, "Survey Results");
                AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "custom time period", "01/20/2017 - 01/20/2018");
                Thread.Sleep(5000);
                bool status = AnalyzeAction.CheckForQuestionVisibilityInInsights(driver, StrengthQuestion, OppQuestion);
                Assert.IsTrue(!status);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateQuestionsVisibilityInDrillDown()
        {
            try
            {
                string DistributionName = "Make Out";
                string Question = "I feel my Company has good benefits.";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.DataCalculationSelection(driver, "Percentage");
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Questions");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                AnalyzeAction.ExpandAndCollapseFilters(driver, "Survey Results");
                AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "custom time period", "01/20/2017 - 01/20/2018");
                Thread.Sleep(5000);
                bool status = AnalyzeAction.CheckForQuestionVisibilityInDrillDown(driver, Question);
                Assert.IsTrue(!status);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateQuestionVisibilityInCommentsReport_Hide()
        {
            try
            {
                string DistributionName = "Make Out";
                string Question = "Please provide any additional comments you have concerning retirement plans.";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.SwitchToReportTab(driver, "Comments");
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.DataCalculationSelection(driver, "Percentage");
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Category");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                AnalyzeAction.ExpandAndCollapseFilters(driver, "Survey Results");
                AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "custom time period", "01/20/2017 - 01/20/2018");
                Thread.Sleep(5000);
                bool status_p = AnalyzeAction.CheckForQuestionVisibilityInCommentsReport(driver, Question);
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                bool status_q = AnalyzeAction.CheckForQuestionVisibilityInCommentsReport(driver, Question);
                Assert.IsTrue(!(status_p || status_q));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

       
        
       

        [TestMethod]
        public void ValidateQuestionVisibilityInCommentsReport_Show()
        {
            try
            {
                string DistributionName = "Make Out";
                string Question = "Please provide any additional comments you have concerning retirement plans.";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.SwitchToReportTab(driver, "Comments");
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.DataCalculationSelection(driver, "Percentage");
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Category");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                AnalyzeAction.ExpandAndCollapseFilters(driver, "Survey Results");
                AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "All Time Period");
                Thread.Sleep(5000);
                bool status_p = AnalyzeAction.CheckForQuestionVisibilityInCommentsReport(driver, Question);
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                bool status_q = AnalyzeAction.CheckForQuestionVisibilityInCommentsReport(driver, Question);
                Assert.IsTrue(status_p && status_q);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateQuestionVisibilityInCommentsReport_EditQuestion()
        {
            try
            {
                string DistributionName = "Make Out";
                string Question = "Please provide any additional comments you have concerning retirement plans.";
                string SurveyID = "32668";
                string ShowOrHide = "Show";
                AnalyzeAction.UpdateQuestionVisibilityInTables(DBConnectionParameters, Question, SurveyID, ShowOrHide);
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.SwitchToReportTab(driver, "Comments");
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.DataCalculationSelection(driver, "Percentage");
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Category");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                AnalyzeAction.ExpandAndCollapseFilters(driver, "Survey Results");
                AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "custom time period", "01/20/2017 - 01/20/2018");
                Thread.Sleep(5000);
                bool status_p = AnalyzeAction.CheckForQuestionVisibilityInCommentsReport(driver, Question);
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                bool status_q = AnalyzeAction.CheckForQuestionVisibilityInCommentsReport(driver, Question);
                AnalyzeAction.UpdateQuestionVisibilityInTables(DBConnectionParameters, Question, SurveyID, "Hide");
                Assert.IsTrue(status_p && status_q);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateQuestionVisibilityInTranslationTemplate()
        {
            try
            {
                bool TextStatus = true;
                DriverAndLogin.Browser = "Chrome";
                string FormName = "Make Out";
                string[] HiddenQuestion = {
                "Have you considered enrolling in one of the High Deductible Health Plans (HDHPs)?"
                ,"How long have you worked this company?"
                ,"I feel my Company has good benefits."
                ,"Please provide any additional comments you have concerning medical benefits."
                ,"Please provide any additional comments you have concerning retirement plans."
                ,"The Company benefits package was an important factor in my decision to join the Company."
                };
                string[] Languages ={
                    " French as spoken in Belgium"
                    ," Hindi as spoken in India"
                };
                var directory = new DirectoryInfo("C:\\Users\\anath\\Downloads");

                RemoteWebDriver driver = SetDriverAndNavigateToManageForms();
                Thread.Sleep(8000);
                AuthorActions.SearchAndOpenForm(driver, FormName);
                AuthorActions.DownloadLanguageTemplate(driver, Languages);
                Thread.Sleep(10000);
                var myFile = (from f in directory.GetFiles()
                              orderby f.LastWriteTime descending
                              select f).First();

                foreach (string question in HiddenQuestion)
                {
                    TextStatus = CommonActions.CheckExcelContains(myFile.FullName, question);
                    if (TextStatus == false)
                        break;
                }
                Assert.IsTrue(TextStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateQuestionVisibilityInDemographics_QuestionView()
        {
            try
            {
                string DistributionName = "Make Out";
                string Question = "The Company benefits package was an important factor in my decision to join the Company.";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.SwitchToReportTab(driver, "Demographics");
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.DataCalculationSelection(driver, "Percentage");
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Questions");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                AnalyzeAction.ExpandAndCollapseFilters(driver, "Survey Results");
                AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "custom time period", "01/20/2017 - 01/20/2018");
                Thread.Sleep(5000);
                bool status = AnalyzeAction.CheckForQuestionVisibilityInDemographics(driver, Question);
                Assert.IsTrue(!status);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateQuestionVisibilityInDemographics_CategoryView()
        {
            try
            {
                string DistributionName = "Make Out";
                string Question = "The Company benefits package was an important factor in my decision to join the Company.";
                string CategoryName = "General";
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute();
                DistributeActions.SearchAndLaunchDistributionSummary(driver, DistributionName);
                Thread.Sleep(5000);
                UIActions.Click(driver, "#distributeSumrybutton");
                Thread.Sleep(15000);
                AnalyzeAction.SwitchToReportTab(driver, "Demographics");
                AnalyzeAction.OpenAnalysisSettings(driver);
                AnalyzeAction.DataCalculationSelection(driver, "Percentage");
                AnalyzeAction.GroupingInAnalysisSetting(driver, "Category");
                UIActions.Click(driver, "#dialog-analysisSetting .mdc-button--raised");
                AnalyzeAction.ExpandAndCollapseFilters(driver, "Survey Results");
                AnalyzeAction.SelectTimePeriod(driver, "Survey Completion Date", "custom time period", "01/20/2017 - 01/20/2018");
                Thread.Sleep(5000);

                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                IWebElement Demowebelements = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.getElementById('demoContainer')");
                IReadOnlyCollection<IWebElement> Categories = Demowebelements.FindElements(By.XPath("//*[@class='inline-block pull-left caretDiv']"));
                var Category = Categories.FirstOrDefault(e => e.Displayed && e.Text.Trim() == CategoryName);
                Category.Click();

                bool status = AnalyzeAction.CheckForQuestionVisibilityInDemographics(driver, Question);
                Assert.IsTrue(!status);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }
        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }
    }
}
