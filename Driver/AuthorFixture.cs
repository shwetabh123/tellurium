﻿using System;
using System.Linq;
using System.Threading;
using Common;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;

namespace Driver
{
    [TestFixture]
    public class AuthorFixture
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        private static string FormName;
        public static string FileName;

        static AuthorFixture()
        {
            DriverAndLogin = CommonActions.ReadDriveAndLoginDetails();
            FileName = CommonActions.CreateResultsFile("AuthorTestResuls_"+DateTime.Now.ToString("u").Replace(':','_'));
            FileActionAttribute.FileName = FileName;
        }
        
        public static IWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static IWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if(!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver) WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver) WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver) WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static void OpenOrCreateForm(IWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        public static IWebDriver SetDriverAndNavigateToForm(string driverName)
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username,DriverAndLogin.Password, DriverAndLogin.Account);
                OpenOrCreateForm(driver);
            }
            else if (driver.Url.Contains("ManageForms"))
                OpenOrCreateForm(driver);
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);
            }
            else
            {
                OpenOrCreateForm(driver);
            }
            return driver;
        }

        public static IWebDriver SetDriverAndNavigateToHome(string driverName)
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            else
                NavigateToHome(driver);
            return driver;
        }

        public static void NavigateToHome(IWebDriver driver)
        {
            UIActions.Click(driver, "[href='/Pulse/']");
        }

        [Test, Order(1), FileAction]
        public void ValidateCreateFormFromHome()
        {
            IWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(10000);
            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Assert.AreEqual(true, formBuilder != null);
        }

        [Test, Order(2), FileAction]
        [Ignore ("Not used")]
        public void ValidateCopyFormFromHome()
        {
            IWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            var formBuilder = AuthorActions.CopyForm(driver);
            Assert.AreEqual(true, formBuilder != null);
            driver.Close();
        }

        [Test, Order(3), FileAction]
        [Ignore("Not used")]
        public void ValidateCreateFormFromManageForms()
        {
            IWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AuthorActions.NavigateToAuthor(driver);
            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Assert.AreEqual(true, formBuilder != null);
        }

        [Test, Order(4), FileAction]
        [Ignore("Not used")]
        public void ValidateEmptyPageAndThankYouPage()
        {
            IWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var firstEmptyPage = UIActions.FindElements(driver, ".page-list a span");
            var thankyouPage = UIActions.FindElements(driver, ".end-page-list a span");
            Assert.IsNotEmpty(firstEmptyPage.Where(x=>x.Text == "Survey Page").ToList());
            Assert.IsNotEmpty(firstEmptyPage.Where(x => x.Text=="Section Name").ToList());
            Assert.IsNotEmpty(thankyouPage.Where(x => x.Text == "Survey Thank You Page").ToList());
        }

        [Test, Order(5), FileAction]
        [Ignore("Not used")]
        public void ValidateOpenFormFromManageForms()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            AuthorActions.NavigateToAuthor(driver);
            var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            Assert.AreEqual(true, formBuilder != null);
        }

        [Test, Order(6), FileAction]
        [Ignore("Not used")]
        public void ValidateAddSingleRatingScaleQuestion()
        {
            IWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate this Automation framework?");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [Test, Order(7), FileAction]
        [Ignore("Not used")]
        public void ValidateAddSingleRatingScaleQuestionWithScaleRangeAndHeaders()
        {
            IWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] scaleHeaders = {"One", "Two", "Three", "Four", "Five", "Six"};
            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate the old Automation framework?","1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [Test, Order(8), FileAction]
        [Ignore("Not used")]

        public void ValidateAddMultipleRatingScaleQuestion()
        {
            IWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] questions = {"Selenium with Java", "Selenium with C#", "Selenium with NUnit"};
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below automation frameworks", questions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [Test, Order(9), FileAction]
        [Ignore("Not used")]
        public void AddPageSectionWithInformationQuestion()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            Thread.Sleep(1000);
            AuthorActions.AddPage(driver);
            var firstEmptyPage = UIActions.FindElements(driver, ".page-list a span");
            Assert.IsNotEmpty(firstEmptyPage.Where(x => x.Text == "Survey Page").ToList());
            Assert.IsNotEmpty(firstEmptyPage.Where(x => x.Text == "Section Name").ToList());
            Thread.Sleep(1000);
            AuthorActions.AddInformationText(driver, "Welcome! Hi Please take survey");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }


        [Test, Order(9), FileAction]
        [Ignore("Not used")]
        public void ValidateAddPage()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            Thread.Sleep(1000);
            var pageList = UIActions.FindElements(driver, ".page-list");
            AuthorActions.AddPage(driver);
            var newPageList = UIActions.FindElements(driver, ".page-list");
            Assert.IsTrue(newPageList.Count > pageList.Count);
        }

        [Test, Order(10), FileAction]
        [Ignore("Not used")]
        public void ValidateAddMultipleRatingScaleQuestionWithScaleRangeAndHeaders()
        {
            IWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            string[] scaleHeaders = { "One", "Two", "Three", "Four", "Five", "Six" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below automation frameworks", questions, "1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [Test, Order(11), FileAction]
        [Ignore("Not used")]
        public void ValidateAddMultipleChoiceSingleItem()
        {
            IWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] responseOptions = {"HR", "IT", "Finance", "Sales"};
            AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [Test, Order(12), FileAction]
        [Ignore("Not used")]

        public void ValidateAddMultipleChoiceMultiItem()
        {
            IWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] questions = { "First", "Second", "Third"};
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceMultiItemQuestion(driver, "Chose your favorite Departments", questions, responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [Test, Order(13), FileAction]
        [Ignore("Not used")]
        public void ValidatePublishForm()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null && driver.Url.Contains("ManageForm/"))
            {
                Thread.Sleep(5000);
                UIActions.Click(driver, "#formHeader .pub");
                var pubConfirm = UIActions.GetElementWithWait(driver, "#publish-form-modal", 5);
                UIActions.Click(driver, "#btnConfirmPublishClicked");
                var pubSuccess = UIActions.GetElementWithWait(driver, "#publishPopUpModal", 45);
                if (pubSuccess != null)
                {
                    UIActions.Click(driver, "#publishPopUpModal button.close");
                    var editSurvey = UIActions.GetElementWithWait(driver, "#btnFormEdit", 10);
                    Assert.AreEqual(true, editSurvey != null);
                }
                else
                    Assert.Fail();
            }
            else
                Assert.Fail();
        }

        [Test, Order(13), FileAction]
        [Ignore("Not used")]
        public void ValidateCopyPublishedFormFromManageForms()
        {
            IWebDriver driver = InitializeAndGetDriver(false, DriverAndLogin.Browser);
            if (driver != null)
            {
                Thread.Sleep(5000);
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder= AuthorActions.CopyForm(driver, FormName);
                Assert.AreEqual(true, formBuilder != null);
                driver.Close();
            }
            else
                Assert.Fail();
        }

        [Test, Order(14), FileAction]
        [Ignore("Not used")]
        public void ValidateAddPageSectionWithInformationQuestion()
        {
           
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            AuthorActions.AddInformationText(driver, "Welcome! Hi Please take survey");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

        }

        [Test, Order(15), FileAction]
        [Ignore("Not used")]

        public void  ValidateAddLongCommentQuestion()
        {
           IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Longcomment"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);

        }

        [Test, Order(16), FileAction]
        [Ignore("Not used")]
        public void ValidateAddShortCommentQuestion()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            var QuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Shortcomment"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }

        [Test, Order(17), FileAction]
        [Ignore("Not used")]
        public void ValidateAddNumberQuestion()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "Number");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatNumberQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }

        [Test, Order(18), FileAction]
        [Ignore("Not used")]
        public void ValidateAddNumberRangeQuestion()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatNumberRangeQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_number_range");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatNumberRangeQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }

        [Test, Order(19), FileAction]
        [Ignore("Not used")]
        public void ValidateAddWebsiteURLQuestion()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatWebSiteURLQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_website_url");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatWebSiteURLQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }
        [Test, Order(20), FileAction]
        [Ignore("Not used")]
        public void ValidateAddEmailAddressQuestion()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatEmailAddressQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_email_address");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatEmailAddressQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }
        [Test, Order(21), FileAction]
        public void ValidateAddDateAsDemoQuestion()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatDATEQuestion" + Guid.NewGuid();
            String [] CategoryName = { "Demographic"};
            AuthorActions.AddFixedFormatDateQuestionAsDemo(driver, QuestionText, CategoryName);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatDATEQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
         }





        [Test, Order(22), FileAction]
        [Ignore("Not used")]
        public void DragAndDroplogic()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            AuthorActions.DragAndDropLogic(driver,1);
            var logicbuilder = driver.FindElement(By.Id("Add-Logic-modal"));
            Assert.AreEqual(true, logicbuilder != null);           
        }

        [Test, Order(22), FileAction]
        [Ignore("Not used")]
        public void AddLogicWithTwoCases()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            AuthorActions.DragAndDropLogic(driver, 1);
            var logicbuilder = driver.FindElement(By.Id("Add-Logic-modal"));
            if (logicbuilder != null)
            {
                Thread.Sleep(1000);
                AuthorActions.AddLogic(driver, "Hide", "1", new string[] { "Q4.1.Selenium with Java" }, "Any of", new LogicCaseDto[]
                {
                    new LogicCaseDto
                    {
                        SourceType = "1",
                        SourceItem =  "Q1.How do you rate this Automation framework?",
                        SourceCondition = "Not Equal To",
                        SourceLogic = "Any of",
                        SourceResponses = new string[] { "1","2"}
                    },
                    new LogicCaseDto
                    {
                        SourceType = "2",
                        SourceItem =  "City",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "Chennai" }
                    }

                });
                Thread.Sleep(10000);
                logicbuilder = UIActions.GetElement(driver, "#Add-Logic-modal");
                Assert.False(logicbuilder != null && logicbuilder.Displayed);
            }
            else
            {
                Assert.Fail();
            }
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }
    }
}
