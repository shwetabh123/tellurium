﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Data;
using System.Threading;
namespace Driver
{
    [TestClass]
    public class DemoValueSuppressionMSTests
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DBConnectionStringDTO DBConnectionParameters1;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString(),
            };
            //DBConnectionParameters = new DBConnectionStringDTO
            //{
            //    userName = "pulseqa",
            //    password = "password-1",
            //    TCESserverName = "USA-QIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-QIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            //DBConnectionParameters1 = new DBConnectionStringDTO
            //{
            //    userName = "pulsestg",
            //    password = "password-1",
            //    TCESserverName = "USA-SIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-SIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString(),
                IntegratedSecurity = context.Properties["IntegratedSecurity"].ToString(),
                PersistSecurityInfo = context.Properties["PersistSecurityInfo"].ToString()
            };
        }
        //shwetabh srivastava--------added capabilities for download file in specified location at run time//
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    // FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    //     service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    //   driver = new FirefoxDriver();//
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }


        [TestMethod]
        public void ValidateInferenceinLeftsideDemoSelectionSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            //Clinet id:1561 & NSize=15
            //Demographic Value Minimum   =15

            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "1561", "7625", "11999",DBConnectionParameters);

            

            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.Expandleftsidedemo(driver, "How long have you worked this company?");





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);



            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "How long have you worked this company?");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "How long have you worked this company?");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "1-4 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(1-4 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='1-4 years']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);





            AnalyzeAction.searchsegmentcategory(driver, "16-19 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(16-19 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='16-19 years']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "");



            AnalyzeAction.searchsegmentcategory(driver, "20-25 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            //verify demovalue(20-25 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='20-25 years']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "");


            AnalyzeAction.searchsegmentcategory(driver, "5-11 months");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);

            //verify demovalue(5-11 months) is disabled in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='5-11 months']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(10000);






            //type additional filters
            //search additional filters
            //    SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "1-4 years");


            //verify demovalue is disabled in  LeftsideDemo dropdown
            //input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue1.Displayed);
            //Thread.Sleep(30000);


            //type additional filters
            //search additional filters
            //SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "16-19 years");


            //verify demovalue is disabled in  LeftsideDemo dropdown
            //input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue2 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue2.Displayed);
            //Thread.Sleep(30000);



            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinLeftsideDemoSelectionSingleDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:1561 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "1561", DBConnectionParameters);




            //Clinet id:1561 & NSize=15
            //Demographic Value Minimum   =15

            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.Expandleftsidedemo(driver, "How long have you worked this company?");




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");

            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "How long have you worked this company?");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "How long have you worked this company?");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "1-4 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(1-4 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='1-4 years']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "16-19 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(16-19 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='16-19 years']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "20-25 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            //verify demovalue(20-25 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip3 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='20-25 years']");


            Assert.IsTrue(Chip3.Displayed);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "5-11 months");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);

            //verify demovalue(5-11 months) is disabled in  LeftsideDemo dropdown

            IWebElement Chip4 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='5-11 months']");


            Assert.IsTrue(Chip4.Displayed);


            Thread.Sleep(10000);







            //type additional filters
            //search additional filters
            //SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "1-4 years");
            ////verify demovalue is disabled in  LeftsideDemo dropdown
            ////input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue1.Displayed);
            //Thread.Sleep(30000);


            ////type additional filters
            ////search additional filters
            //SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "16-19 years");
            ////verify demovalue is disabled in  LeftsideDemo dropdown
            ////input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue2 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue2.Displayed);
            //Thread.Sleep(30000);






            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinLeftsideDemoSelectionMultiDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "10");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=10

            //ReportDataSuppression.UpdateDemoValueMinimum("10", "1561", DBConnectionParameters);




            //Clinet id:1561 & NSize=10
            //Demographic Value Minimum   =10

            ReportDataSuppression.UpdateDemoValueMinimumnew("10", "1561", "7625", "11999", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "10");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "age group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "age group");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "35 - 40");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='35 - 40']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "25 - 30");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='25 - 30']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "(//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            //SuppressionActions.SelectAdditionalFilters(driver, "age group");
            //SuppressionActions.Expandleftsidedemo(driver, "age group");



            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "35 - 40");

            ////verify demovalue is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue.Displayed);




            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "25 - 30");
            ////verify demovalue is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue1.Displayed);
            //Thread.Sleep(30000);



            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinLeftsideDemoSelectionMultiDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "10");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=10

            //ReportDataSuppression.UpdateDemoValueMinimum("10", "1561", DBConnectionParameters);





            //Clinet id:1561 & NSize=10
            //Demographic Value Minimum   =10

            ReportDataSuppression.UpdateDemoValueMinimumnew("10", "1561", "7625", "11999", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "10");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "age group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "age group");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "35 - 40");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='35 - 40']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "25 - 30");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='25 - 30']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            //SuppressionActions.SelectAdditionalFilters(driver, "age group");
            //SuppressionActions.Expandleftsidedemo(driver, "age group");




            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "35 - 40");
            ////verify demovalue is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue.Displayed);
            //Thread.Sleep(30000);


            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "25 - 30");
            ////verify demovalue is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue1.Displayed);



            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=13

            //ReportDataSuppression.UpdateDemoValueMinimum("13", "1561", DBConnectionParameters);





            //Clinet id:1561 & NSize=13
            //Demographic Value Minimum   =13

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(20000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "How long have you worked this company?");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "How long have you worked this company?");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "20-25 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(20-25 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='20-25 years']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);


       
            AnalyzeAction.searchsegmentcategory(driver, "16-19 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(16-19 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='16-19 years']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);


         


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.Expandleftsidedemo(driver, "How long have you worked this company?");


            ////type additional filters
            ////search additional filters
            //SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "20-25 years");
            ////verify demovalue( 20-25 years) is disabled in  LeftsideDemo dropdown
            ////input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue1.Displayed);
            //Thread.Sleep(30000);



            ////type additional filters
            ////search additional filters
            //SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "16-19 years");
            ////verify demovalue(16-19 years) is disabled in  LeftsideDemo dropdown
            ////input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue2 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue2.Displayed);


        
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=13

            //ReportDataSuppression.UpdateDemoValueMinimum("13", "1561", DBConnectionParameters);







            //Clinet id:1561 & NSize=13
            //Demographic Value Minimum   =13

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(20000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(20000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "How long have you worked this company?");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "How long have you worked this company?");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "20-25 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(20-25 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='20-25 years']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "16-19 years");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //verify demovalue(16-19 years) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='16-19 years']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.Expandleftsidedemo(driver, "How long have you worked this company?");


            ////type additional filters
            ////search additional filters
            //SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "20-25 years");
            ////verify demovalue ( 20-25 years)is disabled in  LeftsideDemo dropdown
            ////input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue1.Displayed);
            //Thread.Sleep(30000);


            ////type additional filters
            ////search additional filters
            //SuppressionActions.typeandSearchleftsidedemo(driver, "How long have you worked this company?", "16-19 years");
            ////verify demovalue ( 16-19 years) is disabled in  LeftsideDemo dropdown
            ////input[@class="form-control demo-value-search"][@name="How long have you worked this company?"]/..//i/..//following-sibling::div//ul//li[@class="disabled"]
            //IWebElement demodropdownvalue2 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='How long have you worked this company?']/..//i/..//following-sibling::div//ul//li[@class='disabled']");
            //Assert.IsTrue(demodropdownvalue2.Displayed);
            //Thread.Sleep(30000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=11

            //ReportDataSuppression.UpdateDemoValueMinimum("11", "1561", DBConnectionParameters);





            //Clinet id:1561 & NSize=11
            //Demographic Value Minimum   =11

            ReportDataSuppression.UpdateDemoValueMinimumnew("11", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "age group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "age group");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "35 - 40");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='35 - 40']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "25 - 30");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='25 - 30']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);

            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "(//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            //SuppressionActions.SelectAdditionalFilters(driver, "age group");
            //SuppressionActions.Expandleftsidedemo(driver, "age group");



            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "35 - 40");
            ////verify demovalue(35-40) is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue.Displayed);


            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "25 - 30");
            ////verify demovalue(25-20) is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue1.Displayed);
            //Thread.Sleep(30000);


            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "30 - 35");
            ////verify demovalue (30-35 )is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue2 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue2.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=11

            //ReportDataSuppression.UpdateDemoValueMinimum("11", "1561", DBConnectionParameters);



            //Clinet id:1561 & NSize=11
            //Demographic Value Minimum   =11

            ReportDataSuppression.UpdateDemoValueMinimumnew("11", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);


            //type additional filters
            //search additional filters

            AnalyzeAction.searchsegmentcategory(driver, "age group");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickChip(driver, "age group");




            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "35 - 40");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip1 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='35 - 40']");


            Assert.IsTrue(Chip1.Displayed);


            Thread.Sleep(10000);

      

            AnalyzeAction.searchsegmentcategory(driver, "25 - 30");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);





            //verify demovalue(35 - 40) is disabled in  LeftsideDemo dropdown

            IWebElement Chip2 = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='25 - 30']");


            Assert.IsTrue(Chip2.Displayed);


            Thread.Sleep(10000);

            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            //SuppressionActions.SelectAdditionalFilters(driver, "age group");
            //SuppressionActions.Expandleftsidedemo(driver, "age group");


            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "35 - 40");
            ////verify demovalue(35-40) is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue.Displayed);


            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "25 - 30");
            ////verify demovalue(25-20) is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue1 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue1.Displayed);
            //Thread.Sleep(30000);


            //SuppressionActions.typeandSearchleftsidedemo(driver, "age group", "30 - 35");
            ////verify demovalue (30-35 )is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue2 = UIActions.FindElementWithXpath(driver, "//input[@class='form-control demo-value-search'][@name='age group']/..//i/..//following-sibling::div//ul//li[@class='disabled'] ");
            //Assert.IsTrue(demodropdownvalue2.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisableDemographicValueinDemographicReportSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "1561", DBConnectionParameters);


            //Clinet id:1561 & NSize=15
            //Demographic Value Minimum   =15

            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "1561", "7625", "11999", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(70000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);





            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);





            //SelectDemographicDropdowninDemographicReport---How long have you worked this company?


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[3].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();


            Thread.Sleep(10000);


            var ClickDemographicDropDown = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('.btn-group'))[0] ");

            ClickDemographicDropDown.Click();

            


            Thread.Sleep(20000);


            var SelectDemographicDropdowninDemographicReport1 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[1].querySelector('input')");
            SelectDemographicDropdowninDemographicReport1.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport1 = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport1.Click();


            Thread.Sleep(10000);



            //Click on .. Demographic Reports

            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");


            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            //var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            //ClickondotdotDemographicReport.Click();


            //Thread.Sleep(10000);



            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);




            //xpath of 1-4 years


            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


        
            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");

         

            Assert.AreEqual("--", demovalue1.Text);

            Thread.Sleep(10000);






            //xpath of 16-19 years 

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[5]");



            Assert.AreEqual("--", demovalue2.Text);


            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicValueinDemographicReportSingleDistributionCompletedthesurvey()
        {





            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "1561", DBConnectionParameters);



            //Clinet id:1561 & NSize=15
            //Demographic Value Minimum   =15

            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "1561", "7625", "11999", DBConnectionParameters);






            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");

            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);





            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);





            //SelectDemographicDropdowninDemographicReport---How long have you worked this company?


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[3].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();


            Thread.Sleep(10000);


            var ClickDemographicDropDown = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('.btn-group'))[0] ");

            ClickDemographicDropDown.Click();




            Thread.Sleep(20000);


            var SelectDemographicDropdowninDemographicReport1 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[1].querySelector('input')");
            SelectDemographicDropdowninDemographicReport1.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport1 = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport1.Click();


            Thread.Sleep(10000);

            //Click on .. Demographic Reports

            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");


            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            //var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            //ClickondotdotDemographicReport.Click();


            //Thread.Sleep(10000);


            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);




            //xpath of 1-4 years


            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");

            Assert.AreEqual("--", demovalue1.Text);


       


            Thread.Sleep(10000);






            //xpath of 16-19 years 

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[5]");

            Assert.AreEqual("--", demovalue2.Text);

            Thread.Sleep(10000);



            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);
            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            ////xpath of 1-4 years
            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);
            ////xpath of 16-19 years 
            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);



            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicValueinDemographicReportMultiDistributionAnsweratleast1question()
        {




            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "10");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=10

            //ReportDataSuppression.UpdateDemoValueMinimum("10", "1561", DBConnectionParameters);




            //Clinet id:1561 & NSize=10
            //Demographic Value Minimum   =10

            ReportDataSuppression.UpdateDemoValueMinimumnew("10", "1561", "7625", "11999", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "10");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);

          

            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);

            

            //SelectDemographicDropdowninDemographicReport---"age group"


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[10].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();

            Thread.Sleep(10000);


            //Click on .. Demographic Reports

            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");


            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();


            Thread.Sleep(10000);



            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);




            ////xpath of 25-30 years

            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[3]");



            Assert.AreEqual("--", demovalue1.Text);


            Thread.Sleep(10000);







            ////xpath of 35-40 years 

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");




            Assert.AreEqual("--", demovalue2.Text);

            Thread.Sleep(10000);






            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);



            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);
            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);


            ////xpath of 25-30 years
            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[4]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            ////xpath of 35-40 years 
            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);



            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicValueinDemographicReportMultiDistributionCompletedthesurvey()
        {




            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "10");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=10

            //ReportDataSuppression.UpdateDemoValueMinimum("10", "1561", DBConnectionParameters);



            //Clinet id:1561 & NSize=10
            //Demographic Value Minimum   =10

            ReportDataSuppression.UpdateDemoValueMinimumnew("10", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);







            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);



            //SelectDemographicDropdowninDemographicReport---"age group"


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[10].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();


            Thread.Sleep(10000);


            //Click on .. Demographic Reports

            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");


            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();


            Thread.Sleep(10000);



            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);




            ////xpath of 25-30 years

            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[3]");



            Assert.AreEqual("--", demovalue1.Text);



            Thread.Sleep(10000);







            ////xpath of 35-40 years 

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");

      

            Assert.AreEqual("--", demovalue2.Text);



            Thread.Sleep(10000);
            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);

            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, "age group");


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);
            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);


            ////xpath of 25-30 years
            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[4]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            ////xpath of 35-40 years 
            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }



        [TestMethod]
        public void ValidatedisabledDemographicValueDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=13

            //ReportDataSuppression.UpdateDemoValueMinimum("13", "1561", DBConnectionParameters);





            //Clinet id:1561 & NSize=13
            //Demographic Value Minimum   =13

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);





            //SelectDemographicDropdowninDemographicReport---How long have you worked this company?


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[3].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();


            Thread.Sleep(10000);


            var ClickDemographicDropDown = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('.btn-group'))[0] ");

            ClickDemographicDropDown.Click();




            Thread.Sleep(20000);


            var SelectDemographicDropdowninDemographicReport1 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[1].querySelector('input')");
            SelectDemographicDropdowninDemographicReport1.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport1 = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport1.Click();


            Thread.Sleep(10000);


            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);



            //xpath of 16-19 years 

         


            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[5]");

  

            Assert.AreEqual("--", demovalue1.Text);



            Thread.Sleep(10000);





            //xpath of 20-25 years

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[6]");



            Assert.AreEqual("--", demovalue2.Text);


            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);
            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);


            ////xpath of 20-25 years
            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            ////xpath of 16-19 years years
            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[7]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicValueinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=13

            //ReportDataSuppression.UpdateDemoValueMinimum("13", "1561", DBConnectionParameters);






            //Clinet id:1561 & NSize=13
            //Demographic Value Minimum   =13

            ReportDataSuppression.UpdateDemoValueMinimumnew("13", "1561", "7625", "11999", DBConnectionParameters);







            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "13");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);







            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);





            //SelectDemographicDropdowninDemographicReport---How long have you worked this company?


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);




            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[3].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();


            Thread.Sleep(10000);


            var ClickDemographicDropDown = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('.btn-group'))[0] ");

            ClickDemographicDropDown.Click();




            Thread.Sleep(20000);


            var SelectDemographicDropdowninDemographicReport1 = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[1].querySelector('input')");
            SelectDemographicDropdowninDemographicReport1.Click();

            Thread.Sleep(10000);

            var ClickondotdotDemographicReport1 = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport1.Click();


            Thread.Sleep(10000);



            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);



            //xpath of 16-19 years 




            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[5]");

          

            Assert.AreEqual("--", demovalue1.Text);




            Thread.Sleep(10000);





            //xpath of 20-25 years

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[6]");

         

            Assert.AreEqual("--", demovalue2.Text);


            Thread.Sleep(10000);




            








            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);
            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);


            ////xpath of 20-25 years
            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            ////xpath of 16-19 years years
            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[7]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicValueinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            //Clinet id:1561 & NSize=11

            ReportDataSuppression.UpdateDemoValueMinimum("11", "1561", DBConnectionParameters);




            //Clinet id:1561 & NSize=11
            //Demographic Value Minimum   =11

            ReportDataSuppression.UpdateDemoValueMinimumnew("11", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);



            //SelectDemographicDropdowninDemographicReport---"age group"


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[10].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();


            Thread.Sleep(10000);




            //Click on .. Demographic Reports

            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");


            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();




            Thread.Sleep(10000);



            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);




            ////xpath of 25-30 years

            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[2]");

     

            Assert.AreEqual("--", demovalue1.Text);



            Thread.Sleep(10000);




            ////xpath of 30-35 years

            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[3]");

 

            Assert.AreEqual("--", demovalue2.Text);



            Thread.Sleep(10000);



            ////xpath of 35-40 years 

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue3 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");

         

            Assert.AreEqual("--", demovalue3.Text);

            Thread.Sleep(10000);




            





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);


            ////SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            ////SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, "age group");
            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);
            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);


            ////xpath of 25-30 years
            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[3]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            ////xpath of 30-35 years
            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[4]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            ////xpath of 35-40 years
            //IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue3.Displayed);
            //Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicValueinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=11

            //ReportDataSuppression.UpdateDemoValueMinimum("11", "1561", DBConnectionParameters);





            //Clinet id:1561 & NSize=11
            //Demographic Value Minimum   =11

            ReportDataSuppression.UpdateDemoValueMinimumnew("11", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "11");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);



            //SelectDemographicDropdowninDemographicReport---"age group"


            //SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, " How long have you worked this company?");
            //Thread.Sleep(30000);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var SelectDemographicDropdowninDemographicReport = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li'))[10].querySelector('input')");
            SelectDemographicDropdowninDemographicReport.Click();


            Thread.Sleep(10000);




            //Click on .. Demographic Reports

            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");


            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);


            var ClickondotdotDemographicReport = (IWebElement)js.ExecuteScript("return document.querySelector('demographic-view').shadowRoot.querySelector('.paperIcon') ");

            ClickondotdotDemographicReport.Click();


            Thread.Sleep(10000);





            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            var NumberofSegments = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1] ");

            NumberofSegments.Click();

            Thread.Sleep(10000);



            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);



            var NumberofSegmentsvalue = (IWebElement)js.ExecuteScript("return  ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('.btn-group'))[1].querySelectorAll('li'))[2].querySelector('input')");

            NumberofSegmentsvalue.Click();

            Thread.Sleep(10000);




            ////xpath of 25-30 years

            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue1 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[2]");



            Assert.AreEqual("--", demovalue1.Text);

            Thread.Sleep(10000);




            ////xpath of 30-35 years

            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            var demovalue2 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[3]");

        
            Assert.AreEqual("--", demovalue2.Text);


            Thread.Sleep(10000);



            ////xpath of 35-40 years 

            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[6]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            var demovalue3 = (IWebElement)js.ExecuteScript("return ((document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelectorAll('tr'))[2].querySelectorAll('td'))[4]");

           

            Assert.AreEqual("--", demovalue3.Text);



            Thread.Sleep(10000);




            






            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);


            ////SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            ////SuppressionActions.SelectDemographicDropdowninDemographicReport(driver, "age group");
            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            //IWebElement NumberofSegments = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]");
            //NumberofSegments.Click();
            //Thread.Sleep(10000);


            //IWebElement NumberofSegmentsvalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[2]/..//ul//li//label[contains(.,' 10')]//input");
            //NumberofSegmentsvalue.Click();
            //Thread.Sleep(20000);


            ////xpath of 25-30 years
            //IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[3]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue1.Displayed);
            //Thread.Sleep(20000);


            ////xpath of 30-35 years
            //IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[4]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue2.Displayed);
            //Thread.Sleep(10000);


            ////xpath of 35-40 years
            //IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "//tr[3]//td[5]//*[@class='colorBlocks demo-white style-scope demographic-view'][contains(.,'--')]");
            //Assert.IsTrue(demovalue3.Displayed);
            //Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            //  IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            //  Thread.Sleep(10000);
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //   WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            ////Clinet id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7","16",DBConnectionParameters);



            //Clinet id:16 & NSize=7
            //Demographic Value Minimum   =7
            //published surveyform id = 3627
             //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "3627", "6388", DBConnectionParameters);







            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(10000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);


            //surveyid:12323
            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(40000);






            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);



            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(10000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(20000);



            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue1.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[19]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());


            driver.Quit();
            Thread.Sleep(10000);
        }


        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportSingleDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            //  IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            //  Thread.Sleep(10000);
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //   WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            ////Clinet id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=7
            //Demographic Value Minimum   =7
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "3627", "6388", DBConnectionParameters);






            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);


            //surveyid:12323

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);




            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);


            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(30000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue1.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[19]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());


            driver.Quit();
            Thread.Sleep(10000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(50000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=15
            //Demographic Value Minimum   =15
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "3627", "6388", DBConnectionParameters);






            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);

            ////NPS Distribution
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);


            ////EVP Allignment
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();


            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);


            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);






            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);

            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(30000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue1.Text.Trim());

            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[19]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());


            //show more
            IWebElement showmore = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='HasMoreComments']//input[@data-bind='setShowMoreButton' and @type='button'])[9]");
            showmore.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[36]/li)[1]");
            Assert.AreNotEqual("26", demovalue3.Text.Trim());


            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportMultiDistributionCompletedthesurvey()
        {
            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=15
            //Demographic Value Minimum   =15
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "3627", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            ////NPS Distribution
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);


            ////EVP Allignment
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();




            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);



            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);






            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(30000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue1.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[19]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());


            //show more
            IWebElement showmore = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='HasMoreComments']//input[@data-bind='setShowMoreButton' and @type='button'])[9]");
            showmore.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[36]/li)[1]");
            Assert.AreNotEqual("26", demovalue3.Text.Trim());


            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "5");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Clinet id:16 & NSize=5

            //ReportDataSuppression.UpdateDemoValueMinimum("5", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=5
            //Demographic Value Minimum   =5
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("5", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);

            



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);








            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);
            
            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(30000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue1.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[19]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());


            //xpath of 26 demo value
            //show more
            IWebElement showmore = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='HasMoreComments']//input[@data-bind='setShowMoreButton' and @type='button'])[9]");
            showmore.Click();
            Thread.Sleep(30000);


            IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[29]/li)[1]");
            Assert.AreNotEqual("26", demovalue3.Text.Trim());


            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "5");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Clinet id:16 & NSize=5

            //ReportDataSuppression.UpdateDemoValueMinimum("5", "16", DBConnectionParameters);

            //Clinet id:16 & NSize=5
            //Demographic Value Minimum   =5
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("5", "16", "3627", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);


            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(30000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue1.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[19]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());

            //show more
            IWebElement showmore = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='HasMoreComments']//input[@data-bind='setShowMoreButton' and @type='button'])[9]");
            showmore.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[29]/li)[1]");
            Assert.AreNotEqual("26", demovalue3.Text.Trim());


            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Clinet id:16 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "16", "3627", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);






            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            ////NPS Distribution
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);


            ////EVP Allignment
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);



            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);


            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);

            


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);




            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);


            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(30000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(30000);




            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[8]/li)[1]");
            Assert.AreNotEqual("23", demovalue1.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[10]/li)[1]");
            Assert.AreNotEqual("26", demovalue3.Text.Trim());


            //show more
            IWebElement showmore = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='HasMoreComments']//input[@data-bind='setShowMoreButton' and @type='button'])[9]");
            showmore.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue4 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[18]/li)[1]");
            Assert.AreNotEqual("26", demovalue4.Text.Trim());


            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:16 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "16", DBConnectionParameters);

            //Clinet id:16 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");



            //Thread.Sleep(30000);
            ////NPS Distribution
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            ////EVP Allignment



            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();


            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);


            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);


            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);

            

            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);


            IWebElement Whatisyourageinyears = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?')]");
            Whatisyourageinyears.Click();
            Thread.Sleep(30000);


            IWebElement ExpandWhatiswasyourlastdayofemploymentwithOrganizationName = UIActions.FindElementWithXpath(driver, "(//*[@id='comments']//label//span[contains(.,'What is/was your last day of employment with #Organization Name##')])[1]");
            ExpandWhatiswasyourlastdayofemploymentwithOrganizationName.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[8]/li)[1]");
            Assert.AreNotEqual("23", demovalue1.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[9]/li)[1]");
            Assert.AreNotEqual("26", demovalue2.Text.Trim());


            //xpath of 26 demo value
            IWebElement demovalue3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[10]/li)[1]");
            Assert.AreNotEqual("26", demovalue3.Text.Trim());


            //show more
            IWebElement showmore = UIActions.FindElementWithXpath(driver, "(//*[@data-bind='HasMoreComments']//input[@data-bind='setShowMoreButton' and @type='button'])[9]");
            showmore.Click();
            Thread.Sleep(30000);


            //xpath of 26 demo value
            IWebElement demovalue4 = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'category-question-comments-section')]//ul[18]/li)[1]");
            Assert.AreNotEqual("26", demovalue4.Text.Trim());


            driver.Quit();
            Thread.Sleep(30000);
        }



        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:16 & NSize=25

            //ReportDataSuppression.UpdateDemoValueMinimum("25", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=25
            //Demographic Value Minimum   =25
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("25", "16", "3627", "6388", DBConnectionParameters);






            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);


            //surveyid:12323

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);


            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);


            Thread.Sleep(30000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:16 & NSize=25

            //ReportDataSuppression.UpdateDemoValueMinimum("25", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=25
            //Demographic Value Minimum   =25
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("25", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);


            //surveyid:12323

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);


            Thread.Sleep(30000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "60");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Clinet id:16 & NSize=60

            //ReportDataSuppression.UpdateDemoValueMinimum("60", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=60
            //Demographic Value Minimum   =60
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("60", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "60");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);







            ////EVP Allignment
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();





            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);





            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);


            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);
            Thread.Sleep(30000);



            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownMultiDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);



            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "60");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:16 & NSize=60

            //ReportDataSuppression.UpdateDemoValueMinimum("60", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=60
            //Demographic Value Minimum   =60
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("60", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "60");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            ////EVP Allignment
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();




            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);





            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);


            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);
            Thread.Sleep(30000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:16 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "16", "3627", "6388", DBConnectionParameters);






            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);







            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);



            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);

            Thread.Sleep(30000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {
            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);





            ////Clinet id:16 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);






            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);



            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);
            Thread.Sleep(30000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "41");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:16 & NSize=41

            //ReportDataSuppression.UpdateDemoValueMinimum("41", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=41
            //Demographic Value Minimum   =41
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("41", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            ////EVP Allignment
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);

            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);




            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);


            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }


        //Account -UAT Data Checks
        [TestMethod]
        public void ValidatedisabledDemoValueinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "41");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);

            ////Clinet id:16 & NSize=41

            //ReportDataSuppression.UpdateDemoValueMinimum("41", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=41
            //Demographic Value Minimum   =41
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("41", "16", "3627", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("Copy Of Copy Of Departure view product testing consumer survey");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[3]");
            //distributionLink.Click();
            //Thread.Sleep(40000);
            ////EVP Allignment
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);

            //Multi Distribution ---EVP(14646) & NPS(12323)

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Net Promoter Score");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Net Promoter Score");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "EVP Alignment");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "EVP Alignment");



            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);



            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);



            IWebElement Whatisyourageinyearsdisabled = UIActions.FindElementWithXpath(driver, "//*[@id='analyse-identifier']/option[contains(.,'What is your age in years?') and @disabled='disabled']");
            Assert.IsTrue(Whatisyourageinyearsdisabled.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }


        //Account -Automation Suppression
        [TestMethod]
        public void ValidatedisableDemographicLabelinDemographicReportSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "1561", DBConnectionParameters);




            //Clinet id:16 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "1561", "7625", "11999", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");

            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            // How long have you worked this company?--should b e disabled

            //       (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.," How long have you worked this company?")]//input[@disabled]
            //xpath of "How long have you worked this company"


            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,'How long have you worked this company?')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[0] ");

            Assert.IsTrue(demovalue.Displayed);



            driver.Quit();
            Thread.Sleep(10000);
        }




        //Account -Automation Suppression
        [TestMethod]
        public void ValidatedisabledDemographicLabelinDemographicReportSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);




            ////Clinet id:1561 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "1561", DBConnectionParameters);



            //Clinet id:16 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "1561", "7625", "11999", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(10000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(20000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");

            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);

            // How long have you worked this company?--should be disabled


            //xpath of "How long have you worked this company"
            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,' How long have you worked this company?')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;


            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[0] ");

            Assert.IsTrue(demovalue.Displayed);



            Thread.Sleep(1000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicLabelinDemographicReportMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "120");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:1561 & NSize=120

            //ReportDataSuppression.UpdateDemoValueMinimum("120", "1561", DBConnectionParameters);


            //Clinet id:1561 & NSize=120
            //Demographic Value Minimum   =120
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("120", "1561", "7625", "11999", DBConnectionParameters);






            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "120");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);





            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);




            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);


            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(60000);


            ////click on DemographicDropDown
            //IWebElement ClickDemographicDropDown = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]");
            //ClickDemographicDropDown.Click();
            //Thread.Sleep(60000);

            //"age group"--should be disabled


            //xpath of "age group"
            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,' age group')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);
            //Thread.Sleep(10000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[8] ");

            Assert.IsTrue(demovalue.Displayed);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicLabelinDemographicReportMultiDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "120");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:1561 & NSize=120

            //ReportDataSuppression.UpdateDemoValueMinimum("120", "1561", DBConnectionParameters);


            //Clinet id:1561 & NSize=120
            //Demographic Value Minimum   =120
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("120", "1561", "7625", "11999", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "120");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);



            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);



            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            ////click on DemographicDropDown
            //IWebElement ClickDemographicDropDown = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]");
            //ClickDemographicDropDown.Click();
            //Thread.Sleep(30000);




            //"age group"--should be disabled



            //xpath of "age group"
            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, "    (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,' age group')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);
            //Thread.Sleep(10000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;


            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[8] ");

            Assert.IsTrue(demovalue.Displayed);




            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicLabelDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "Automation Suppression";



            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:1561 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "1561", DBConnectionParameters);



            //Clinet id:1561 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "1561", "7625", "11999", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);



            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);
            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            ////click on DemographicDropDown
            //IWebElement ClickDemographicDropDown = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]");
            //ClickDemographicDropDown.Click();
            //Thread.Sleep(30000);




            //How long have you worked this company --should be disabled

            //xpath of "How long have you worked this company"
            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,'How long have you worked this company?')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;


            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[0] ");

            Assert.IsTrue(demovalue.Displayed);



            Thread.Sleep(1000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicLabelinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:1561 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "1561", DBConnectionParameters);

            //Clinet id:1561 & NSize=20
            //Demographic Value Minimum   =20
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "1561", "7625", "11999", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);
            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            ////click on DemographicDropDown
            //IWebElement ClickDemographicDropDown = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]");
            //ClickDemographicDropDown.Click();
            //Thread.Sleep(30000);



            //How long have you worked this company --should be disabled


            //xpath of "How long have you worked this company"
            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,'How long have you worked this company?')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[0] ");

            Assert.IsTrue(demovalue.Displayed);



            Thread.Sleep(1000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicLabelinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "105");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Clinet id:1561 & NSize=105

            //ReportDataSuppression.UpdateDemoValueMinimum("105", "1561", DBConnectionParameters);


            //Clinet id:1561 & NSize=105
            //Demographic Value Minimum   =105
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("105", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "105");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "(//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);



            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            ////click on DemographicDropDown
            //IWebElement ClickDemographicDropDown = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]");
            //ClickDemographicDropDown.Click();
            //Thread.Sleep(30000);

            // age group--should be disbaled

            //xpath of "age group"
            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,' age group')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[8] ");

            Assert.IsTrue(demovalue.Displayed);




            Thread.Sleep(1000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDemographicLabelinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "105");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);

            ////Clinet id:1561 & NSize=105

            //ReportDataSuppression.UpdateDemoValueMinimum("105", "1561", DBConnectionParameters);



            //Clinet id:1561 & NSize=105
            //Demographic Value Minimum   =105
            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateDemoValueMinimumnew("105", "1561", "7625", "11999", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "105");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(130000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2014", "01/01/2016");
            //Thread.Sleep(80000);







            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(80000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);

            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(70000);
            ////Click on .. Demographic Reports
            //IWebElement ClickondotdotDemographicReport = UIActions.FindElementWithXpath(driver, " //*[@id='demoContainer']//*[@id='icon']");
            //ClickondotdotDemographicReport.Click();
            //Thread.Sleep(30000);
            ////click on DemographicDropDown
            //IWebElement ClickDemographicDropDown = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]");
            //ClickDemographicDropDown.Click();
            //Thread.Sleep(30000);


            // age group--should be disbaled


            //xpath of "age group"
            //IWebElement demovalue = UIActions.FindElementWithXpath(driver, "    (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]/..//ul//li//label[contains(.,' age group')]//input[@disabled]");
            //Assert.IsTrue(demovalue.Displayed);
            //Thread.Sleep(10000);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demovalue = (IWebElement)js.ExecuteScript("return    (document.querySelector('demographic-view').shadowRoot.querySelector('paper-card').querySelector('.btn-group').querySelectorAll('li[class=disabled]'))[8] ");

            Assert.IsTrue(demovalue.Displayed);




            driver.Quit();
            Thread.Sleep(10000);
        }
        //[TestMethod]
        //public void getSurveyURLSByDistribution()
        //{
        //    IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
        //    Thread.Sleep(10000);
        //    WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
        //    DBOperations.getSurveyURLSByDistribution1(DBConnectionParameters.userName,DBConnectionParameters.password,DBConnectionParameters.TCESserverName);
        //}


        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchDVSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Client id:16 & NSize=25

            //ReportDataSuppression.UpdateDemoValueMinimum("25", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=25
            //Demographic Value Minimum   =25
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("25", "16", "3627", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //Distribution---Unsuppressed

            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
            //Thread.Sleep(200000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);





            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);



            //verify xpath of "23" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'23')]//input[@disabled]");
            Assert.IsTrue(demovalue1.Displayed);
            Thread.Sleep(20000);
            //verify xpath of "26" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'26')]//input[@disabled]");
            Assert.IsTrue(demovalue2.Displayed);




            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchDVSingleDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:16 & NSize=25

            //ReportDataSuppression.UpdateDemoValueMinimum("25", "16", DBConnectionParameters);




            //Clinet id:16 & NSize=25
            //Demographic Value Minimum   =25
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("25", "16", "3627", "6388", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);






            //Distribution---Unsuppressed


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
            //Thread.Sleep(200000);
           


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);


            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);



            //verify xpath of "23" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'23')]//input[@disabled]");
            Assert.IsTrue(demovalue1.Displayed);
            Thread.Sleep(20000);
            //verify xpath of "26" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'26')]//input[@disabled]");
            Assert.IsTrue(demovalue2.Displayed);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchDVMultiDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "40");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:16 & NSize=40

            //ReportDataSuppression.UpdateDemoValueMinimum("40", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=40
            //Demographic Value Minimum   =40
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("40", "16", "3627", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "40");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //Muti Distribution ---Unsuppressed & 	Employee Satisfaction



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Employee Satisfaction");
            //Thread.Sleep(50000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);
            AnalyzeAction.ClickAddFilter(driver);




            Thread.Sleep(30000);


            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Employee Satisfaction");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Employee Satisfaction");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);


            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);






            //verify xpath of "23" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'23')]//input[@disabled]");
            Assert.IsTrue(demovalue1.Displayed);
            Thread.Sleep(20000);


            //verify xpath of "26" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'26')]//input[@disabled]");
            Assert.IsTrue(demovalue2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchDVMultiDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);




            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "40");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.Clickcompletedthesurvey(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=40

            //ReportDataSuppression.UpdateDemoValueMinimum("40", "16", DBConnectionParameters);






            //Clinet id:16 & NSize=40
            //Demographic Value Minimum   =40
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("40", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "40");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //Muti Distribution ---Unsuppressed & 	Employee Satisfaction


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Employee Satisfaction");
            //Thread.Sleep(50000);








            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);
            AnalyzeAction.ClickAddFilter(driver);




            Thread.Sleep(30000);



            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Employee Satisfaction");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Employee Satisfaction");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //verify xpath of "23" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'23')]//input[@disabled]");
            Assert.IsTrue(demovalue1.Displayed);
            Thread.Sleep(20000);


            //verify xpath of "26" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'26')]//input[@disabled]");
            Assert.IsTrue(demovalue2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }


        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchDVwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);





            //Clinet id:16 & NSize=15
            //Demographic Value Minimum   =15
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "3627", "6388", DBConnectionParameters);






            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);








            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //verify xpath of "23" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'23')]//input[@disabled]");
            Assert.IsTrue(demovalue1.Displayed);
            Thread.Sleep(20000);


            //verify xpath of "26" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'26')]//input[@disabled]");
            Assert.IsTrue(demovalue2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchDVwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);




            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);

            ////Client id:16 & NSize=25

            //ReportDataSuppression.UpdateDemoValueMinimum("25", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=25
            //Demographic Value Minimum   =25
            //published surveyform id = 3627
            //draft surveyform id = 6388


            ReportDataSuppression.UpdateDemoValueMinimumnew("25", "16", "3627", "6388", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "25");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Employee Satisfaction");
            //Thread.Sleep(50000);
            //SuppressionActions.SelectDateRanges(driver, "11/16/2017", "11/17/2017");
            //Thread.Sleep(80000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickAddFilter(driver);




            Thread.Sleep(30000);

            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Employee Satisfaction");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Employee Satisfaction");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "11/16/2017 - 11/17/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);




            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //verify xpath of "23" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'23')]//input[@disabled]");
            Assert.IsTrue(demovalue1.Displayed);
            Thread.Sleep(20000);


            //verify xpath of "26" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'row panel panel-default padding-top')]//*[contains(@class,'btn-group ceb-dropdown-container')])[2]//ul//li//label[contains(.,'26')]//input[@disabled]");
            Assert.IsTrue(demovalue2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchTRSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=7
            //Demographic Value Minimum   =7
            //published surveyform id = 2394
            //draft surveyform id =3499


            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"20")]//input
            //verify xpath of "100" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'100')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            //verify xpath of "20" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'20')]//input");
            Assert.IsTrue(demovalue2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchTRMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=7
            //Demographic Value Minimum   =7
            //published surveyform id = 2394
            //draft surveyform id =3499


            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Retrospective");
            //Thread.Sleep(50000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            AnalyzeAction.ClickAddFilter(driver);




            Thread.Sleep(50000);

            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Retrospective");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Retrospective");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"20")]//input
            //verify xpath of "100" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'100')]//input");
            Assert.IsTrue(demovalue1.Displayed);

            //verify xpath of "20" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'20')]//input");
            Assert.IsTrue(demovalue2.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchTRwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);

            //Clinet id:16 & NSize=7
            //Demographic Value Minimum   =7
            //published surveyform id = 2394
            //draft surveyform id =3499


            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "02/08/2017", "03/09/2017");
            //Thread.Sleep(80000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


    

            AnalyzeAction.CustomTimePeriod(driver, "02/08/2017 - 03/09/2017");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);






            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);



            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"20")]//input
            //verify xpath of "100" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'100')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            //verify xpath of "20" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'20')]//input");
            Assert.IsTrue(demovalue2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchTRwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=7
            //Demographic Value Minimum   =7
            //published surveyform id = 2394
            //draft surveyform id =3499


            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Retrospective");
            //Thread.Sleep(50000);
            //SuppressionActions.SelectDateRanges(driver, "02/08/2017", "03/09/2017");
            //Thread.Sleep(80000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            AnalyzeAction.ClickAddFilter(driver);




            Thread.Sleep(50000);

            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Retrospective");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Retrospective");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "02/08/2017 - 03/09/2017");
       

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);




            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "What is your age in years?");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"20")]//input
            //verify xpath of "100" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'100')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            //verify xpath of "20" is suppressed
            IWebElement demovalue2 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'20')]//input");
            Assert.IsTrue(demovalue2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchLDSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);




            //Clinet id:16 & NSize=15
            //Demographic Value Minimum   =15
            //published surveyform id = 7142
            //draft surveyform id =11039


            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "7142", "11039", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);





            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input
            //verify xpath of "Male" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Male')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchLDMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=30

            //ReportDataSuppression.UpdateDemoValueMinimum("30", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=30
            //Demographic Value Minimum   =30
            //published surveyform id = 7142
            //draft surveyform id =11039


            ReportDataSuppression.UpdateDemoValueMinimumnew("30", "16", "7142", "11039", DBConnectionParameters);





            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Previous");
            //Thread.Sleep(50000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Previous");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input
            //verify xpath of "Male" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Male')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchLDwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";


            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=15
            //Demographic Value Minimum   =15
            //published surveyform id = 7142
            //draft surveyform id =11039


            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "7142", "11039", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "02/01/2017", "11/05/2018");
            //Thread.Sleep(80000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/02/2017 - 05/11/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);



            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);

            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input
            //verify xpath of "Male" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Male')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchLDwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Client id:16 & NSize=30

            //ReportDataSuppression.UpdateDemoValueMinimum("30", "16", DBConnectionParameters);




            //Clinet id:16 & NSize=30
            //Demographic Value Minimum   =30
            //published surveyform id = 7142
            //draft surveyform id =11039


            ReportDataSuppression.UpdateDemoValueMinimumnew("30", "16", "7142", "11039", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Previous");
            //Thread.Sleep(50000);
            //SuppressionActions.SelectDateRanges(driver, "01/02/2017", "05/11/2018");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Previous");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");



            Thread.Sleep(50000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/02/2017 - 05/11/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);


            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(10000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input
            //verify xpath of "Male" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Male')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchREDSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "AutomationREDmatchpair";


            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:1483 & NSize=3

            //ReportDataSuppression.UpdateDemoValueMinimum("3", "1483", DBConnectionParameters);





            //Clinet id:1483 & NSize=3
            //Demographic Value Minimum   =3
            //published surveyform id = 7403
            //draft surveyform id =11638


            ReportDataSuppression.UpdateDemoValueMinimumnew("3", "1483", "7403", "11638", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "3");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input
           
            
            //verify xpath of "Male" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Male')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchREDMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "AutomationREDmatchpair";




            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "22");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:1483 & NSize=32

            //ReportDataSuppression.UpdateDemoValueMinimum("32", "1483", DBConnectionParameters);




            //Clinet id:1483 & NSize=32
            //Demographic Value Minimum   =32
            //published surveyform id = 7403
            //draft surveyform id =11638


            ReportDataSuppression.UpdateDemoValueMinimumnew("32", "1483", "7403", "11638", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "32");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            //Thread.Sleep(50000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "RED Distribution");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "RED Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);










            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input
            
            //verify xpath of "Female" is suppressed
            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Female')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchREDwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "AutomationREDmatchpair";




            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);




            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:1483 & NSize=3

            //ReportDataSuppression.UpdateDemoValueMinimum("3", "1483", DBConnectionParameters);



            //Clinet id:1483 & NSize=3
            //Demographic Value Minimum   =3
            //published surveyform id = 7403
            //draft surveyform id =11638


            ReportDataSuppression.UpdateDemoValueMinimumnew("3", "1483", "7403", "11638", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "3");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            //Thread.Sleep(80000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

            
            AnalyzeAction.CustomTimePeriod(driver, "01/01/2018 - 05/18/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);



            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input


            //verify xpath of "Male" is suppressed

            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Male')]//input");
            Assert.IsTrue(demovalue1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDemoValueinSegmenttowatchREDwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "AutomationREDmatchpair";






            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "22");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);






            ////Client id:1483 & NSize=32

            //ReportDataSuppression.UpdateDemoValueMinimum("32", "1483", DBConnectionParameters);



            //Clinet id:1483 & NSize=32
            //Demographic Value Minimum   =32
            //published surveyform id = 7403
            //draft surveyform id =11638


            ReportDataSuppression.UpdateDemoValueMinimumnew("32", "1483", "7403", "11638", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "32");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            //Thread.Sleep(50000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "RED Distribution");



            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "RED Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2018 - 05/18/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);



            SuppressionActions.ClickMultipleDepartureViewSpotlightReports(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstDropdown(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureFirstdDropdownValue(driver, "Gender");
            Thread.Sleep(20000);
            SuppressionActions.ClickMultipleDepartureSecondDropdown(driver);
            Thread.Sleep(20000);


            //*[@widget-on-change="SpotlightReportConfigurationWidget.SegmentValueList"]//*[contains(@class,"btn-group ceb-dropdown-container")]//ul//li//label[contains(.,"Male")]//input

            //verify xpath of "Female" is suppressed

            IWebElement demovalue1 = UIActions.FindElementWithXpath(driver, " //*[@widget-on-change='SpotlightReportConfigurationWidget.SegmentValueList']//*[contains(@class,'btn-group ceb-dropdown-container')]//ul//li//label[contains(.,'Female')]//input");
            Assert.IsTrue(demovalue1.Displayed);

            driver.Quit();
            Thread.Sleep(30000);
        }


        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentResultsReportSingleDistributionAnsweratleast1question()
        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentresultstab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.Resultstab(driver);
            Thread.Sleep(50000);


            //verify xpath of "stop button-Thinking of the position you filled with this employee" is suppressed
            IWebElement stopbutton1 = UIActions.FindElementWithXpath(driver, "  //*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')]");

            Assert.IsTrue(stopbutton1.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton2.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton3.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton4 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[3]");
            Assert.IsTrue(stopbutton4.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton5 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton5.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton6 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton6.Displayed);
            //verify xpath of "bar -Thinking of the position you filled with this employee" is suppressed
            IWebElement bar1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'back-bars style-scope rating-scale-chart')])[3]");
            Assert.IsTrue(bar1.Displayed);

            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentResultsReportMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "22");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(50000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentresultstab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.Resultstab(driver);
            Thread.Sleep(50000);



            //verify xpath of "stop button-Thinking of the position you filled with this employee" is suppressed
            IWebElement stopbutton1 = UIActions.FindElementWithXpath(driver, "  //*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')]");
            Assert.IsTrue(stopbutton1.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton2.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton3.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton4 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[3]");
            Assert.IsTrue(stopbutton4.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton5 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton5.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton6 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton6.Displayed);
            //verify xpath of "bar -Thinking of the position you filled with this employee" is suppressed
            IWebElement bar1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'back-bars style-scope rating-scale-chart')])[3]");
            Assert.IsTrue(bar1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentResultsReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);

            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentresultstab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.Resultstab(driver);
            Thread.Sleep(50000);


            //verify xpath of "stop button-Thinking of the position you filled with this employee" is suppressed
            IWebElement stopbutton1 = UIActions.FindElementWithXpath(driver, "  //*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')]");
            Assert.IsTrue(stopbutton1.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton2.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton3.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton4 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[3]");
            Assert.IsTrue(stopbutton4.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton5 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton5.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton6 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton6.Displayed);
            //verify xpath of "bar -Thinking of the position you filled with this employee" is suppressed
            IWebElement bar1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'back-bars style-scope rating-scale-chart')])[3]");
            Assert.IsTrue(bar1.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentResultsReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "22");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(30000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentresultstab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.Resultstab(driver);
            Thread.Sleep(50000);


            //verify xpath of "stop button-Thinking of the position you filled with this employee" is suppressed
            IWebElement stopbutton1 = UIActions.FindElementWithXpath(driver, "  //*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')]");
            Assert.IsTrue(stopbutton1.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton2.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton3 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton3.Displayed);
            //verify xpath of "stop button-Which candidate accepted" is suppressed
            IWebElement stopbutton4 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Which candidate accepted ')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[3]");
            Assert.IsTrue(stopbutton4.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton5 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[1]");
            Assert.IsTrue(stopbutton5.Displayed);
            //verify xpath of "stop button-Do you know this employee’s performance well enough to rate it" is suppressed
            IWebElement stopbutton6 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Do you know this employee’s performance well enough to rate it?')]/../..//*[@id='content']//*[contains(@class,'suppress-icon-back')])[2]");
            Assert.IsTrue(stopbutton6.Displayed);
            //verify xpath of "bar -Thinking of the position you filled with this employee" is suppressed
            IWebElement bar1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@title,'Thinking of the position you filled with this employee')]/../..//*[@id='content']//*[contains(@class,'back-bars style-scope rating-scale-chart')])[3]");
            Assert.IsTrue(bar1.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentInsightsSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);


            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength1 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(.,'--')]");
            Assert.IsTrue(insightstrength1.Displayed);
            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength2 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the onboarding process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(., '--')]");
            Assert.IsTrue(insightstrength2.Displayed);
            //    (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity1.Displayed);
            //       (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"How would you rate your overall recruiting experience?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'How would you rate your overall recruiting experience?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity2.Displayed);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentInsightsMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "22");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(30000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);


            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength1 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(., '--')]");
            Assert.IsTrue(insightstrength1.Displayed);
            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength2 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the onboarding process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(., '--')]");
            Assert.IsTrue(insightstrength2.Displayed);
            //    (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity1.Displayed);
            //       (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"How would you rate your overall recruiting experience?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'How would you rate your overall recruiting experience?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity2.Displayed);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentInsightswhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);



            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);



            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength1 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(.,'--')]");
            Assert.IsTrue(insightstrength1.Displayed);
            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength2 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the onboarding process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(., '--')]");
            Assert.IsTrue(insightstrength2.Displayed);
            //    (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity1.Displayed);
            //       (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"How would you rate your overall recruiting experience?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'How would you rate your overall recruiting experience?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentInsightswhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "22");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(30000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength1 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(., '--')]");
            Assert.IsTrue(insightstrength1.Displayed);
            //verify xpath of Insights-strength --vs segment is suppressed
            IWebElement insightstrength2 = UIActions.FindElementWithXpath(driver, "//*[contains(@id,'category-strength') and contains(.,'How would you rate the onboarding process?')]/../..//*[@class='comparison-lbl text-center style-scope insights-view']//*[@class='style-scope insights-view' and contains(., '--')]");
            Assert.IsTrue(insightstrength2.Displayed);
            //    (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity1 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'Thinking of the position you filled with this employee, how satisfied were you with the quality of the candidates you interviewed?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity1.Displayed);
            //       (//*[contains(@class,"compColRow style-scope insights-opportunity")and contains(.,"How would you rate your overall recruiting experience?")]/../..//*[@class="comparison-lbl text-center style-scope insights-opportunity"]//*[@class="style-scope insights-opportunity" and contains(.,"--")])[1]
            //verify xpath of Insights-Opportunity --vs segment is suppressed
            IWebElement insightopportunity2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'compColRow style-scope insights-opportunity')and contains(.,'How would you rate your overall recruiting experience?')]/../..//*[@class='comparison-lbl text-center style-scope insights-opportunity']//*[@class='style-scope insights-opportunity' and contains(.,'--')])[1]");
            Assert.IsTrue(insightopportunity2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentDrilldownSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //      (//*[contains(@class,"regular-font style-scope drilldown-view-item")and contains(.,"How would you rate the application process?")]/../..//*[@class="comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item"]//*[@class="style-scope drilldown-view-item" and contains(.,"--")])[1]
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed1 = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed1.Displayed);
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'If given the option, would you accept this job again?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentDrilldownMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(30000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //      (//*[contains(@class,"regular-font style-scope drilldown-view-item")and contains(.,"How would you rate the application process?")]/../..//*[@class="comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item"]//*[@class="style-scope drilldown-view-item" and contains(.,"--")])[1]
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed1 = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed1.Displayed);
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'If given the option, would you accept this job again?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentDrilldownwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //      (//*[contains(@class,"regular-font style-scope drilldown-view-item")and contains(.,"How would you rate the application process?")]/../..//*[@class="comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item"]//*[@class="style-scope drilldown-view-item" and contains(.,"--")])[1]
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed1 = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed1.Displayed);
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'If given the option, would you accept this job again?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentDrilldownwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(30000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //      (//*[contains(@class,"regular-font style-scope drilldown-view-item")and contains(.,"How would you rate the application process?")]/../..//*[@class="comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item"]//*[@class="style-scope drilldown-view-item" and contains(.,"--")])[1]
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed1 = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'How would you rate the application process?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed1.Displayed);
            //verify xpath of drill down --vs segment is suppressed
            IWebElement drilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'regular-font style-scope drilldown-view-item')and contains(.,'If given the option, would you accept this job again?')]/../..//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view-item']//*[@class='style-scope drilldown-view-item' and contains(.,'--')])[1]");
            Assert.IsTrue(drilldownsuppressed2.Displayed);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentTrendSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //xpath for trend -suppressed dot
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope')])[2]");

            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentTrendMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "22");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(30000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //xpath for trend -suppressed dot
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope')])[2]");
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentTrendwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Female");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //xpath for trend -suppressed dot
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope')])[2]");
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforvssegmentTrendwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetDemographicValueMinimum(driver, "22");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(40000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(80000);
            SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            Thread.Sleep(30000);
            SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            Thread.Sleep(80000);
            SuppressionActions.ClickAnalysisSettings(driver);
            Thread.Sleep(20000);
            SuppressionActions.vssegmentFavorabilitytab(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown1value(driver, "What is your gender?");
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2(driver);
            Thread.Sleep(20000);
            SuppressionActions.segmentdropdown2value(driver, "Male");
            Thread.Sleep(20000);
            SuppressionActions.clickapplybutton(driver);
            Thread.Sleep(20000);
            SuppressionActions.FavorabilityTab(driver);
            Thread.Sleep(20000);
            //xpath for trend -suppressed dot
            UIActions.IsElementNotVisible(driver, "(/[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope')])[2]");
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void verifyTextFromPowerPoint1()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            DataSet ds = new DataSet();
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //        DBOperations.executesqlandwritetocsv(DriverAndLogin.logFilePath,"resultset5", DBConnectionParameters);
            // DistributeActions.downloadSampleParticipantUploadfile(driver, "AuthenticationLogicUn");
            //Thread.Sleep(10000);
            // //getlatestdownloadedfile
            // string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            // DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            // //Thread.Sleep(10000);
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, "SpotlightReport7403237636687987512183546.ppt", "6", "Male");
            //    DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, s);
        }
        [TestMethod]
        public void verifyTextFromPowerPoint()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(1000);
            AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            Thread.Sleep(20000);
            //SuppressionActions.btnSelectBenchMark(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.VsBenchmarkCheckbox(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.clickapplybutton(driver);
            //Thread.Sleep(10000);
            SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            Thread.Sleep(10000);
            SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            Thread.Sleep(10000);
            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Function");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(40000);
            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);
            string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 2, 2, 1, 1);
            DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            Thread.Sleep(10000);
            //   DBOperations.Readfromcsv(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4);
            //Thread.Sleep(10000);
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4, DriverAndLogin.logFilePath, latestfile, "6");
            Thread.Sleep(20000);
            driver.Quit();
            Thread.Sleep(30000);
        }


        [TestMethod]
        public void VerifyDemographicValueissuppressedforREDpptSingleDistributionAnsweratleast1question()
        {




            DriverAndLogin.Account = "AutomationREDmatchpair";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:1483 & NSize=3

            //ReportDataSuppression.UpdateDemoValueMinimum("3", "1483", DBConnectionParameters);


            //Clinet id:1483 & NSize=3
            //Demographic Value Minimum   =3
            //published surveyform id = 7403
            //draft surveyform id =11638


            ReportDataSuppression.UpdateDemoValueMinimumnew("3", "1483", "7403", "11638", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "3");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(30000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);





            //SuppressionActions.btnSelectBenchMark(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.VsBenchmarkCheckbox(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.clickapplybutton(driver);
            //Thread.Sleep(20000);








            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);




            //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            //Thread.Sleep(10000);
            //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            //Thread.Sleep(10000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Function");
            Thread.Sleep(20000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);









            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);



            //Male will be suppressed

            //compare Male in ppt at slide 6

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "6", "Male");
            driver.Quit();

            





            ////getlatestdownloadedfile
            //string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            //DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            //Thread.Sleep(10000);


            //string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 2, 2, 1, 1);
            ////Male will be suppressed
            //DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            //Thread.Sleep(10000);


            ////DBOperations.Readfromcsv(driver, DriverAndLogin.logFilePath, "result", "Segment1");
            ////Thread.Sleep(10000);


            ////compare Male at (1,1,4,4) at csv  with ppt at slide 6
            //DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4, DriverAndLogin.logFilePath, latestfile, "6");
            //Thread.Sleep(20000);


            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforREDpptMultiDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "AutomationREDmatchpair";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "22");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            //Clinet id:1483 & NSize=32 & PublishSurveyFormID=7403 & DraftSurveyFormID=11638

            ReportDataSuppression.UpdateDemoValueMinimumnew("32", "1483","7403 ","11638", DBConnectionParameters);







            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "32");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(40000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            //Thread.Sleep(30000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "RED Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "RED Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            //SuppressionActions.btnSelectBenchMark(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.VsBenchmarkCheckbox(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.clickapplybutton(driver);
            //Thread.Sleep(10000);






            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);





            //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            //Thread.Sleep(10000);
            //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            //Thread.Sleep(10000);




            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Function");
            Thread.Sleep(20000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);






            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);




            //Female will be suppressed

            //compare Female in ppt at slide 6

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "6", "Female");
            driver.Quit();






            ////getlatestdownloadedfile
            //string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            //DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            //Thread.Sleep(10000);


            //string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 3, 3, 1, 1);
            ////Female will be suppressed
            //DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            //Thread.Sleep(10000);


            ////compare Female at (1,1,4,4) at csv  with ppt at slide 6
            //DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 2, 2, 4, 4, DriverAndLogin.logFilePath, latestfile, "6");


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforREDpptwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "AutomationREDmatchpair";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);





            ////Clinet id:1483 & NSize=3

            //ReportDataSuppression.UpdateDemoValueMinimum("3", "1483", DBConnectionParameters);


            //Clinet id:1483 & NSize=3 & PublishSurveyFormID=7403 & DraftSurveyFormID=11638

            ReportDataSuppression.UpdateDemoValueMinimumnew("3", "1483", "7403 ", "11638", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "3");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(40000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            //Thread.Sleep(40000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

    

            AnalyzeAction.CustomTimePeriod(driver, "01/01/2018 - 05/18/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);





            //SuppressionActions.btnSelectBenchMark(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.VsBenchmarkCheckbox(driver);
            //Thread.Sleep(10000);
            
            //SuppressionActions.clickapplybutton(driver);
            //Thread.Sleep(10000);





            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);






            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(10000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(10000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Function");
            Thread.Sleep(20000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);




            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);



            //Male will be suppressed

            //compare Male in ppt at slide 6

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "6", "Male");
            driver.Quit();





            ////getlatestdownloadedfile
            //string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            //DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            //Thread.Sleep(10000);


            //string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 4, 4, 1, 1);
            ////Male will be suppressed
            //DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            //Thread.Sleep(10000);


            ////Compare Male at (1,1,4,4) at csv  with ppt at slide 6
            //DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4, DriverAndLogin.logFilePath, latestfile, "6");


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforREDpptwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {




            DriverAndLogin.Account = "AutomationREDmatchpair";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "22");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:1483 & NSize=32 & PublishSurveyFormID=7403 & DraftSurveyFormID=11638

            //ReportDataSuppression.UpdateDemoValueMinimumnew("32", "1483", "7403 ", "11638", DBConnectionParameters);


            //Clinet id:1483 & NSize=32 & PublishSurveyFormID=7403 & DraftSurveyFormID=11638

            ReportDataSuppression.UpdateDemoValueMinimumnew("32", "1483", "7403 ", "11638", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "32");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "REDmatchpairautomation");
            //Thread.Sleep(40000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "RED Distribution");
            //Thread.Sleep(30000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2018", "05/18/2018");
            //Thread.Sleep(40000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "RED Spotlight Surveynew");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "REDmatchpairautomation");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "REDmatchpairautomation");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "RED Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "RED Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "01/01/2018 - 05/18/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);





            //SuppressionActions.btnSelectBenchMark(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.VsBenchmarkCheckbox(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.clickapplybutton(driver);
            //Thread.Sleep(10000);






            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);






            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(10000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(10000);


            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Function");
            Thread.Sleep(20000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);





            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);




            //Female will be suppressed

            //compare Female in ppt at slide 6

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "6", "Female");
            driver.Quit();






            ////getlatestdownloadedfile
            //string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            //DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            //Thread.Sleep(10000);
            //string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 5, 5, 1, 1);
            ////Female will be suppressed
            //DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            //Thread.Sleep(10000);


            ////Compare Female at (1,1,4,4) at csv  with ppt at slide 6
            //DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 3, 3, 4, 4, DriverAndLogin.logFilePath, latestfile, "6");




            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforDVpptSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Clinet id:16 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=20 & PublishSurveyFormID=3627 & DraftSurveyFormID=6388

            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "16", "3627 ", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            /***

                        AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                        Thread.Sleep(1000);
                        AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
                        Thread.Sleep(80000);
                        //SuppressionActions.btnSelectBenchMark(driver);
                        //Thread.Sleep(20000);
                        //SuppressionActions.VsBenchmarkCheckbox(driver);
                        //Thread.Sleep(10000);
                        //SuppressionActions.clickapplybutton(driver);
                        //Thread.Sleep(10000);

            */

                        AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



                        Thread.Sleep(80000);

                        //AnalyzeAction.ClickonFiltersComparisons(driver);


                        //Thread.Sleep(10000);



                        AnalyzeAction.ClickAddFilter(driver);


                        Thread.Sleep(30000);




                        AnalyzeAction.searchsegmentcategory(driver, "Distribution");


                        Thread.Sleep(10000);


                        AnalyzeAction.searchsegement(driver);


                        Thread.Sleep(10000);


                        AnalyzeAction.ClickChip(driver, "Distribution");


                        Thread.Sleep(10000);

                        AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


                        Thread.Sleep(10000);

                        AnalyzeAction.searchsegement(driver);


                        Thread.Sleep(10000);


                        AnalyzeAction.ClickChip(driver, "Unsuppressed");



                        Thread.Sleep(10000);
                        AnalyzeAction.Apply(driver);


                        Thread.Sleep(50000);







                        AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

                        Thread.Sleep(10000);






                        //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
                        //Thread.Sleep(10000);
                        //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
                        //Thread.Sleep(10000);


                        SuppressionActions.Segment1Dropdown(driver);
                        Thread.Sleep(10000);
                        SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
                        Thread.Sleep(10000);
                        SuppressionActions.Segment2Dropdown(driver);
                        Thread.Sleep(10000);
                        SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
                        Thread.Sleep(10000);





                        SuppressionActions.RunReport(driver);
                        Thread.Sleep(80000);






                        //getlatestdownloadedfile
                        string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
                        DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
                        Thread.Sleep(10000);


                        //Check for 26 (What is your age in years?) will be suppressed in slide 5
                        DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "26");
                        driver.Quit();




                        Thread.Sleep(10000);
                    }
                    [TestMethod]
                    public void VerifyDemographicValueissuppressedforDVpptMultiDistributionAnsweratleast1question()
                    {


                        DriverAndLogin.Account = "UAT Data Checks";
                        DriverAndLogin.Browser = "Chrome";



                        IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
                        Thread.Sleep(10000);
                        WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                        SuppressionActions.NavigatetoAccountSuppression(driver);
                        Thread.Sleep(20000);



                        //SuppressionActions.SetDemographicValueMinimum(driver, "20");
                        //SuppressionActions.Saveaccountsuppression(driver);
                        //Thread.Sleep(20000);
                        //SuppressionActions.ClickAccountDefaults(driver);
                        //Thread.Sleep(10000);
                        //SuppressionActions.ClickAnsweredatleast1question(driver);
                        //Thread.Sleep(10000);
                        //SuppressionActions.SaveAnalysisdefaults(driver);
                        //Thread.Sleep(10000);


                        ////Clinet id:16 & NSize=20

                        //ReportDataSuppression.UpdateDemoValueMinimum("20", "16", DBConnectionParameters);


                       //Clinet id:16 & NSize=20 & PublishSurveyFormID=3627 & DraftSurveyFormID=6388

                       ReportDataSuppression.UpdateDemoValueMinimumnew("20", "16", "3627 ", "6388", DBConnectionParameters);





                        SuppressionActions.NavigatetoAccountSuppression(driver);
                        Thread.Sleep(10000);
                        //SuppressionActions.SetDemographicValueMinimum(driver, "20");
                        //SuppressionActions.Saveaccountsuppression(driver);
                        //Thread.Sleep(20000);
                        SuppressionActions.ClickAccountDefaults(driver);
                        Thread.Sleep(10000);
                        SuppressionActions.ClickAnsweredatleast1question(driver);
                        Thread.Sleep(10000);
                        SuppressionActions.SaveAnalysisdefaults(driver);
                        Thread.Sleep(10000);



                        /*

                                    AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                                    Thread.Sleep(1000);
                                    AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
                                    Thread.Sleep(80000);
                                    SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Employee Satisfaction");
                                    Thread.Sleep(30000);


                                    //SuppressionActions.btnSelectBenchMark(driver);
                                    //Thread.Sleep(20000);
                                    //SuppressionActions.VsBenchmarkCheckbox(driver);
                                    //Thread.Sleep(10000);
                                    //SuppressionActions.clickapplybutton(driver);
                                    //Thread.Sleep(10000);



                                    */




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Employee Satisfaction");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Employee Satisfaction");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(40000);







            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);



            //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            //Thread.Sleep(10000);
            //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            //Thread.Sleep(10000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);



            //Check for 26 (What is your age in years?) will be suppressed in slide 5
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "26");
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforDVpptwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);


            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Clinet id:16 & NSize=20

            //ReportDataSuppression.UpdateDemoValueMinimum("20", "16", DBConnectionParameters);





            //Clinet id:16 & NSize=20 & PublishSurveyFormID=3627 & DraftSurveyFormID=6388

            ReportDataSuppression.UpdateDemoValueMinimumnew("20", "16", "3627 ", "6388", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "20");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);

            /*



                        AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                        Thread.Sleep(1000);
                        AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
                        Thread.Sleep(80000);
                        SuppressionActions.SelectDateRanges(driver, "11/15/2017", "11/16/2017");
                        Thread.Sleep(30000);





                        //SuppressionActions.btnSelectBenchMark(driver);
                        //Thread.Sleep(10000);
                        //SuppressionActions.VsBenchmarkCheckbox(driver);
                        //Thread.Sleep(10000);
                        //SuppressionActions.clickapplybutton(driver);
                        //Thread.Sleep(10000);


                        */


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(40000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "11/15/2017 - 11/16/2017");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);






            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);



            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(10000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(10000);




            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);



            //Check for 26 (What is your age in years?) will be suppressed in slide 5
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "26");
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforDVpptwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "5");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);


            ////Clinet id:16 & NSize=5

            //ReportDataSuppression.UpdateDemoValueMinimum("5", "16", DBConnectionParameters);





            //Clinet id:16 & NSize=5 & PublishSurveyFormID=3627 & DraftSurveyFormID=6388

            ReportDataSuppression.UpdateDemoValueMinimumnew("5", "16", "3627 ", "6388", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "5");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);

            /*



                        AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                        Thread.Sleep(1000);
                        AnalyzeAction.OpenAnalyzeForm(driver, "Unsuppressed");
                        Thread.Sleep(80000);
                        SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Employee Satisfaction");
                        Thread.Sleep(20000);
                        SuppressionActions.SelectDateRanges(driver, "11/15/2017", "11/16/2017");
                        Thread.Sleep(80000);



                        //SuppressionActions.btnSelectBenchMark(driver);
                        //Thread.Sleep(10000);
                        //SuppressionActions.VsBenchmarkCheckbox(driver);
                        //Thread.Sleep(10000);
                        //SuppressionActions.clickapplybutton(driver);
                        //Thread.Sleep(10000);




                        */

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Copy Of Copy Of Departure view product testing consumer survey");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Unsuppressed");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Unsuppressed");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(40000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Employee Satisfaction");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Employee Satisfaction");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "11/15/2017 - 11/16/2017");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);






            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);



            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(10000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(10000);


            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);


            //Check for 26 (What is your age in years?) will be suppressed in slide 5
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "26");


            driver.Quit();
            Thread.Sleep(10000);

        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforLDpptSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);    
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Clinet id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=15 & PublishSurveyFormID=7142 & DraftSurveyFormID=11039

            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "7142", "11039", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);





            //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            //Thread.Sleep(20000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Department");
            Thread.Sleep(10000);




            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);


            //Verify that Male will be suppressed( Not present in PPT--Slide5)

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "Male");
            driver.Quit();


            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforLDpptMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=30

            //ReportDataSuppression.UpdateDemoValueMinimum("30", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=30 & PublishSurveyFormID=7142 & DraftSurveyFormID=11039

            ReportDataSuppression.UpdateDemoValueMinimumnew("30", "16", "7142", "11039", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);






            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Previous");
            //Thread.Sleep(30000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "Previous");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(40000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(30000);




            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);




            //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            //Thread.Sleep(20000);




            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Department");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);



            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);



            //Verify that Male will be suppressed( Not present in PPT--Slide 5)

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "Male");


            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforLDpptwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=15 & PublishSurveyFormID=7142 & DraftSurveyFormID=11039

            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "7142", "11039", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "15");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);







            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "1/2/2017", "5/11/2018");
            //Thread.Sleep(80000);







            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/02/2017 - 05/11/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);




            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);





            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(20000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Department");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);



            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);


            //Verify that Male will be suppressed( Not present in PPT-Slide 5)

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "Male");
            driver.Quit();



            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforLDpptwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);




            ////Client id:16 & NSize=15

            //ReportDataSuppression.UpdateDemoValueMinimum("15", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=15 & PublishSurveyFormID=7142 & DraftSurveyFormID=11039

            ReportDataSuppression.UpdateDemoValueMinimumnew("15", "16", "7142", "11039", DBConnectionParameters);




            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "30");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Current Distribution");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Previous");
            //Thread.Sleep(30000);
            //SuppressionActions.SelectDateRanges(driver, "1/2/2017", "5/11/2018");
            //Thread.Sleep(80000);








            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(80000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);



            Thread.Sleep(50000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Previous");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);



            Thread.Sleep(50000);






            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/02/2017 - 05/11/2018");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);





            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);





            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(20000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "Gender");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "Department");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);


            //Verify that Male will be suppressed( Not present in PPT--Slide 5)

            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "Male");
            driver.Quit();


            Thread.Sleep(10000);
        }


        [TestMethod]
        public void VerifyDemographicValueissuppressedforTRpptSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);




            //Clinet id:16 & NSize=7 & PublishSurveyFormID=2394 & DraftSurveyFormID=3499

            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);







            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(30000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);


            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);






            //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            //Thread.Sleep(20000);




            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);


            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "100");
            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "20");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "67");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "24");



            string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 14, 14, 1, 1);


            //100 & 20 will be suppressed
            DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            Thread.Sleep(10000);


            //DBOperations.Readfromcsv(driver, DriverAndLogin.logFilePath, "result", "Segment1");
            //Thread.Sleep(10000);
          
            //Verify that 67 & 24 will not  be suppressed
            
            //compare 67 at (1,1,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");
            
            //Verify that 67 & 24 will not  be suppressed
            
            //compare 24 at (8,8,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 8, 8, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforTRpptMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);




            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);


            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);





            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=7 & PublishSurveyFormID=2394 & DraftSurveyFormID=3499

            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Retrospective");
            //Thread.Sleep(30000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(30000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(30000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Retrospective");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Retrospective");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);





            //SuppressionActions.PreviousStartDate(driver, "01/01/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "08/03/2018");
            //Thread.Sleep(20000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);
            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "100");
            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "20");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "67");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "24");



            string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 14, 14, 1, 1);
            //100 & 20 will be suppressed
            DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            Thread.Sleep(10000);
            //DBOperations.Readfromcsv(driver, DriverAndLogin.logFilePath, "result", "Segment1");
            //Thread.Sleep(10000);
            
            
            //Verify that 67 & 24 will not  be suppressed
            //compare 67 at (1,1,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");
            
            
            //Verify that 67 & 24 will not  be suppressed
            //compare 24 at (8,8,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 8, 8, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");


            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void VerifyDemographicValueissuppressedforTRpptwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);





            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);


            //Clinet id:16 & NSize=7 & PublishSurveyFormID=2394 & DraftSurveyFormID=3499

            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);



            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "2/8/2017", "3/9/2017");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(30000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "02/08/2017 - 03/09/2017");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);



            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);







            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(20000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);


            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);
      
            
            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "100");
            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "20");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "67");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "24");

            string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 14, 14, 1, 1);
            
            
            //100 & 20 will be suppressed
            DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            Thread.Sleep(10000);
            //DBOperations.Readfromcsv(driver, DriverAndLogin.logFilePath, "result", "Segment1");
            //Thread.Sleep(10000);
            
            
            //Verify that 67 & 24 will not  be suppressed
            //compare 67 at (1,1,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");
            
            //Verify that 67 & 24 will not  be suppressed
            //compare 24 at (8,8,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 8, 8, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");


            driver.Quit();
            Thread.Sleep(10000);
        }


        [TestMethod]
        public void VerifyDemographicValueissuppressedforTRpptwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(40000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);


            ////Client id:16 & NSize=7

            //ReportDataSuppression.UpdateDemoValueMinimum("7", "16", DBConnectionParameters);



            //Clinet id:16 & NSize=7 & PublishSurveyFormID=2394 & DraftSurveyFormID=3499

            ReportDataSuppression.UpdateDemoValueMinimumnew("7", "16", "2394", "3499", DBConnectionParameters);


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            //SuppressionActions.SetDemographicValueMinimum(driver, "7");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Departure view/Total Rewards product test current data set");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickMultipleDistributionFromleftsideFilters(driver, "Retrospective");
            //Thread.Sleep(30000);

            //SuppressionActions.SelectDateRanges(driver, "2/8/2017", "3/9/2017");
            //Thread.Sleep(80000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Departure view product testing consumer survey");



            Thread.Sleep(30000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(30000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Departure view/Total Rewards product test current data set");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Departure view/Total Rewards product test current data set");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Retrospective");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Retrospective");



            Thread.Sleep(10000);
            AnalyzeAction.Apply(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(50000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "02/08/2017 - 03/09/2017");


            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(50000);




            AnalyzeAction.PreviousStartEndDate(driver, "01/01/2018 - 08/03/2018");

            Thread.Sleep(10000);




            //SuppressionActions.PreviousStartDate(driver, "1/1/2018");
            //Thread.Sleep(20000);
            //SuppressionActions.PreviousEndDate(driver, "8/3/2018");
            //Thread.Sleep(20000);



            SuppressionActions.Segment1Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment1DropdownValue(driver, "What is your age in years?");
            Thread.Sleep(10000);
            SuppressionActions.Segment2Dropdown(driver);
            Thread.Sleep(10000);
            SuppressionActions.Segment2DropdownValue(driver, "What is your gender?");
            Thread.Sleep(10000);
            SuppressionActions.RunReport(driver);
            Thread.Sleep(80000);



            //getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);
            Thread.Sleep(10000);
         
            
            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "100");
            //Verify that 100 & 20 will be suppressed
            DBOperations.verifyTextnotpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "20");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "67");
            //Verify that 67 & 24 will not  be suppressed
            DBOperations.verifyTextpresentinPowerPoint(driver, DriverAndLogin.logFilePath, latestfile, "4", "24");
            string InputSPText = DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, "SP DATA.xlsx", 14, 14, 1, 1);

            //100 & 20 will be suppressed
            DBOperations.executesqlandwritetocsv(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);
            Thread.Sleep(10000);
            //DBOperations.Readfromcsv(driver, DriverAndLogin.logFilePath, "result", "Segment1");
            //Thread.Sleep(10000);
        
            
            
            //Verify that 67 & 24 will not  be suppressed
            //compare 67 at (1,1,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 1, 1, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");
         
            
            
            //Verify that 67 & 24 will not  be suppressed
            //compare 24 at (8,8,4,4) at csv  with ppt at slide 5
            DBOperations.verifyCSVColumnTextWithPPT(driver, DriverAndLogin.logFilePath, "result", 8, 8, 4, 4, DriverAndLogin.logFilePath, latestfile, "4");
            driver.Quit();
            Thread.Sleep(10000);
        }






        [TestMethod]
        public void ValidatedisabledDemoValueininSegmentReportingSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Client id:1561 & NSize=(1,10)
            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   10



            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("10", "1561", DBConnectionParameters);


            //Client id:1561 & NSize=(1,10)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =10



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("10", "1561", "7625", "11999", DBConnectionParameters);





            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            ////SuppressionActions.SetDemographicValueMinimum(driver, "7");
            ////SuppressionActions.Saveaccountsuppression(driver);
            ////Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);






            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(30000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(20000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(20000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(30000);





            //AnalyzeAction.ClickAddFilter(driver);


            //Thread.Sleep(10000);




            //AnalyzeAction.searchsegmentcategory(driver, "country");


            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "country");

            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegmentcategory(driver, "India");


            //Thread.Sleep(10000);

            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "India");
            //Thread.Sleep(10000);

            //AnalyzeAction.Apply(driver);

            //Thread.Sleep(10000);


            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(30000);


            //verify 20-25 is present


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea0 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");


            //verify 20-25 is present

            Assert.AreEqual("20 - 25", insightopportunitiesarea0.Text.Trim());

            Thread.Sleep(10000);


            //var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[8]");



            ////verify 20-25 is present

            //Assert.AreEqual("20 - 25", insightopportunitiesarea1.Text.Trim());




            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());

            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());


            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            //var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            //var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());




            ////Client id:1561 & NSize=(1,80)


            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   80

            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("80", "1561", DBConnectionParameters);



            //Client id:1561 & NSize=(1,80)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =80



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("80", "1561", "7625", "11999", DBConnectionParameters);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(30000);




            //verify 20-25 is suppressed i.e.Not enough responses
            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");



            Assert.AreNotEqual("20 - 25", insightopportunitiesarea2.Text.Trim());


            ////Not enough responses

            //var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[7]");




            //Assert.AreNotEqual("20 - 25", insightopportunitiesarea3.Text.Trim());

            

            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
       


        [TestMethod]
        public void ValidatedisabledDemoValueininSegmentReportingMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);



            ////Client id:1561 & NSize=(1,80)
            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   80



            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("80", "1561", DBConnectionParameters);





            //Client id:1561 & NSize=(1,80)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =80



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("80", "1561", "7625", "11999", DBConnectionParameters);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(10000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(10000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            //AnalyzeAction.ClickAddFilter(driver);


            //Thread.Sleep(10000);




            //AnalyzeAction.searchsegmentcategory(driver, "country");


            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "country");

            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegmentcategory(driver, "India");


            //Thread.Sleep(10000);

            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "India");
            //Thread.Sleep(10000);

            //AnalyzeAction.Apply(driver);

            //Thread.Sleep(10000);




            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);


            //verify 20-25 is present

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea0 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");



            //verify 20-25 is present

            Assert.AreEqual("20 - 25", insightopportunitiesarea0.Text.Trim());

            Thread.Sleep(10000);


            //var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[8]");



            ////verify 20-25 is present

            //Assert.AreEqual("20 - 25", insightopportunitiesarea1.Text.Trim());





            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            ////var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            ////var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            ////var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());




            ////Client id:1561 & NSize=(1,120)


            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   120

            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("120", "1561", DBConnectionParameters);






            //Client id:1561 & NSize=(1,120)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =120



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("120", "1561", "7625", "11999", DBConnectionParameters);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            ////AnalyzeAction.ClickonFiltersComparisons(driver);


            ////Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(40000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            //verify 20-25 is suppressed i.e.Not enough responses
            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");



            Assert.AreNotEqual("20 - 25", insightopportunitiesarea2.Text.Trim());



            ////Not enough responses
            //var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[7]");




            //Assert.AreNotEqual("20 - 25", insightopportunitiesarea3.Text.Trim());


            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());


            //var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());



            //var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());



            //var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


            //var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());

            //var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea7 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea8 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea9 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());




            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
     
        [TestMethod]
        public void ValidatedisabledDemoValueinininSegmentReportingwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";


            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(20000);



            ////Client id:1561 & NSize=(1,10)
            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   10



            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("10", "1561", DBConnectionParameters);




            //Client id:1561 & NSize=(1,10)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =10



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("10", "1561", "7625", "11999", DBConnectionParameters);









            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            //AnalyzeAction.ClickAddFilter(driver);


            //Thread.Sleep(10000);




            //AnalyzeAction.searchsegmentcategory(driver, "country");


            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "country");

            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegmentcategory(driver, "India");


            //Thread.Sleep(10000);

            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "India");
            //Thread.Sleep(10000);

            //AnalyzeAction.Apply(driver);

            //Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);



            //verify 20-25 is present

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea0 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");


            //verify 20-25 is present

            Assert.AreEqual("20 - 25", insightopportunitiesarea0.Text.Trim());

            Thread.Sleep(10000);


            //var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[8]");



            ////verify 20-25 is present

            //Assert.AreEqual("20 - 25", insightopportunitiesarea1.Text.Trim());





            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            //var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            //var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            //var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());






            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());




            //var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());



            //var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());



            //var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


            //var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());


            //var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //var insightopportunitiesarea7 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());





            //var insightopportunitiesarea8 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());


            //var insightopportunitiesarea9 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());





            ////Client id:1561 & NSize=(1,70)


            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   70

            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("70", "1561", DBConnectionParameters);





            //Client id:1561 & NSize=(1,70)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =70



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("70", "1561", "7625", "11999", DBConnectionParameters);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            //Not enough responses
            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");



            Assert.AreNotEqual("20 - 25", insightopportunitiesarea2.Text.Trim());



            ////Not enough responses
            //var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[7]");




            //Assert.AreNotEqual("20 - 25", insightopportunitiesarea3.Text.Trim());



            

            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
      
        [TestMethod]
        public void ValidatedisabledDemoValueininSegmentReportingwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //SuppressionActions.NavigatetoAccountSuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            //SuppressionActions.SetDemographicValueMinimum(driver, "2");
            //SuppressionActions.Saveaccountsuppression(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAccountDefaults(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnsweredatleast1question(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.SaveAnalysisdefaults(driver);
            //Thread.Sleep(10000);






            ////Client id:1561 & NSize=(1,10)
            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   10



            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("10", "1561", DBConnectionParameters);




            //Client id:1561 & NSize=(1,10)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =10



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("10", "1561", "7625", "11999", DBConnectionParameters);







            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);

            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            //Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            //AnalyzeAction.ClickAddFilter(driver);


            //Thread.Sleep(10000);




            //AnalyzeAction.searchsegmentcategory(driver, "country");


            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "country");

            //Thread.Sleep(10000);


            //AnalyzeAction.searchsegmentcategory(driver, "India");


            //Thread.Sleep(10000);

            //AnalyzeAction.searchsegement(driver);


            //Thread.Sleep(10000);


            //AnalyzeAction.ClickChip(driver, "India");
            //Thread.Sleep(10000);

            //AnalyzeAction.Apply(driver);

            //Thread.Sleep(10000);








            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(50000);





            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea0 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");


            //verify 20-25 is present

            Assert.AreEqual("20 - 25", insightopportunitiesarea0.Text.Trim());

            Thread.Sleep(10000);


            //var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[8]");



            ////verify 20-25 is present

            //Assert.AreEqual("20 - 25", insightopportunitiesarea1.Text.Trim());





            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            ////var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            ////var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            ////var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());




            //var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());




            //var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());





            //var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


            //var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());




            //var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            ////IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea7 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea8 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea9 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());





            ////Client id:1561 & NSize=(1,105)
            ////Group N-Size to Report Demographics 1
            ////Demographic Value Minimum   105



            //ReportDataSuppression.UpdateGroupNSizeToReportDemo("1", "1561", DBConnectionParameters);
            //ReportDataSuppression.UpdateDemoValueMinimum("105", "1561", DBConnectionParameters);



            //Client id:1561 & NSize=(1,105)
            //Group N-Size to Report Demographics =1
            //Demographic Value Minimum   =105



            //published surveyform id = 7625
            //draft surveyform id = 11999


            ReportDataSuppression.UpdateGroupNSizeToReportDemonew("1", "1561", "7625", "11999", DBConnectionParameters);

            Thread.Sleep(10000);

            ReportDataSuppression.UpdateDemoValueMinimumnew("105", "1561", "7625", "11999", DBConnectionParameters);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            //AnalyzeAction.ClickonFiltersComparisons(driver);


            ////Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(30000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2014 - 01/01/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            //Not enough responses
            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[3]");



            Assert.AreNotEqual("20 - 25", insightopportunitiesarea2.Text.Trim());



            ////Not enough responses
            //var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('.pointer'))[7]");




            //Assert.AreNotEqual("20 - 25", insightopportunitiesarea3.Text.Trim());



            
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
   



























































































































        [TestMethod]
        public void Comparecsv()
        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);



            DBOperations.csvcompare(driver, DriverAndLogin.logFilePath, "result", DriverAndLogin.logFilePath, "result1");




            //  DBOperations.csvCompare(driver, DriverAndLogin.logFilePath, "result",1,4,0,2, DriverAndLogin.logFilePath, "result1",1,4,0,2);




            //DBOperations.csvCompare(driver, DriverAndLogin.logFilePath, "result", 2, 2, 0, 0, DriverAndLogin.logFilePath, "result1", 2, 2, 0, 0);



            //DBOperations.csvCompare(driver, DriverAndLogin.logFilePath, "result", 3, 3, 0, 0, DriverAndLogin.logFilePath, "result1", 3, 3, 2, 2);



            //DBOperations.csvCompare(driver, DriverAndLogin.logFilePath, "result", 1, 1, 1, 1, DriverAndLogin.logFilePath, "result1", 1, 1, 1, 1);




            //DBOperations.csvCompare(driver, DriverAndLogin.logFilePath, "result", 2, 2, 1, 1, DriverAndLogin.logFilePath, "result1", 2, 2, 1, 1);



            //DBOperations.csvCompare(driver, DriverAndLogin.logFilePath, "result", 3, 3, 1, 1, DriverAndLogin.logFilePath, "result1", 2, 2, 1, 1);

            Thread.Sleep(10000);






        }





        [TestMethod]
        public void TR()
        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);




      //      DV Reports-SP Comparison


            string sheetname = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "SP DATA--Analyse Reports.xlsx", "0");





            string InputSPText = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "SP DATA--Analyse Reports.xlsx", 2, 2, 2, 2, sheetname);


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText);


            DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 0, 0, 3, 3, DriverAndLogin.logFilePath, "DVresult", 0, 0, 3, 3);// ---PASS


       //     OverallPercentage


               DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 3, 3, 3, 3, DriverAndLogin.logFilePath, "DVresult", 3, 3, 3, 3); //--PASS


   //         PercentageChange

                      DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 3, 3, 4, 4, DriverAndLogin.logFilePath, "DVresult", 3, 3, 4, 4); //--PASS



    //        CompetitivePlacement
                     DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 3, 3, 5, 5, DriverAndLogin.logFilePath, "DVresult", 3, 3, 5, 5); //--PASS



        //    SixtiethPercentileScore
                DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 3, 3, 6, 6, DriverAndLogin.logFilePath, "DVresult", 3, 3, 6, 6); //--PASS

    //        NintiethPercentileScore

               DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 3, 3, 7, 7, DriverAndLogin.logFilePath, "DVresult", 3, 3, 7, 7); //--PASS


     //       FortiethPercentileScore

            DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 3, 3, 8, 8, DriverAndLogin.logFilePath, "DVresult", 3, 3, 8, 8); //--PASS



     //       Segment1
               DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 8, 8, 4, 4, DriverAndLogin.logFilePath, "DVresult", 8, 8, 4, 4); //--PASS

    //        Segment2
                  DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 8, 8, 5, 5, DriverAndLogin.logFilePath, "DVresult", 8, 8, 5, 5); //--PASS



        //    https://www.cebglobal.com/member/corporate-leadership-council/research/study/13/managing-leadership-performance-risks.html 
            DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 26, 26, 5, 5, DriverAndLogin.logFilePath, "DVresult", 26, 26, 5, 5); //--PASS


            DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 22, 22, 3, 3, DriverAndLogin.logFilePath, "DVresult", 22, 22, 3, 3); //--PASS


            DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 14, 14, 5, 5, DriverAndLogin.logFilePath, "DVresult", 14, 14, 5, 5); //--PASS



        //    LD Reports-SP Comparison




            string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "SP DATA--Analyse Reports.xlsx", "2");





            string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "SP DATA--Analyse Reports.xlsx", 8, 8, 2, 2, sheetname1);


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText1);

           // PASS when value matches
                   DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 1, 1, 1, 1, DriverAndLogin.logFilePath, "LDresult", 1, 1, 1, 1); //--PASS

           // Fails(2, 1--29) & (1,1-- - 30)  for Mismatched values
                       DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 2, 2, 1, 1, DriverAndLogin.logFilePath, "LDresult", 1, 1, 1, 1); //--FAIL

         //   Fails(4, 0 - 104) & (3, 0----110) for Mismatch values)

                    DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 4, 4, 0, 0, DriverAndLogin.logFilePath, "LDresult", 3, 3, 0, 0); //--FAIL

       //     Fails(5, 6 - Career Growth) & (4,6----Charu) for Mismatch values)

                DBOperations.csvComparerowcolumnnwise(driver, DriverAndLogin.logFilePath, "result", 5, 5, 6, 6, DriverAndLogin.logFilePath, "LDresult", 4, 4, 6, 6); //--FAIL






        }


        [TestMethod]
        public void LD()
        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


                     //LD Reports-SP Comparison


            string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "SP DATA--Analyse Reports.xlsx", "1");





            string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "SP DATA--Analyse Reports.xlsx", 2, 2, 1, 1, sheetname1);


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "result", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TRresult-excel-STG.xlsx", "0");

            //Locally


            //Fails for Mismatch  ( 2,1---29 ) & ( 1,1---30  )
            //      DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "result", 2, 2, 1, 1, DriverAndLogin.logFilePath, "LDresult1.xlsx", 1, 1, 1, 1, sheetname2);


            //PASSED for Matched value  ( 2,1---29 ) & ( 1,1---29  )
            //     DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "result", 2, 2, 1, 1, DriverAndLogin.logFilePath, "LDresult1.xlsx", 1, 1, 1, 1, sheetname2);




            //Fails for Mismatch  ( 4,0---104 ) & ( 1,0---110 )
            //          DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "result", 4, 4, 0, 0, DriverAndLogin.logFilePath, "LDresult1.xlsx", 1, 1, 0, 0, sheetname2);



            //server

            //Passed for Matched values(4,6-Job Preparedness) & ( 1,6---Job Preparedness)

            //  DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "result", 4, 4, 6, 6, DriverAndLogin.logFilePath, "LDresult1.xlsx", 1, 1, 6, 6, sheetname2);







            //Passed for Matched values(18,4-Organizing & Executing) & ( 11,4---Organizing & Executing)

            //   DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "result", 18, 18, 4, 4, DriverAndLogin.logFilePath, "LDresult1.xlsx", 11, 11, 4, 4, sheetname2);


            //Fails for  MisMatched values(18,4-Organizing & Executing) & ( 11,4---Creating & Conceptualizing)

            //   DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "result", 18, 18, 4, 4, DriverAndLogin.logFilePath, "LDresult1.xlsx", 11, 11, 4, 4, sheetname2);




            //TR Reports-SP Comparison--STAGE --working in server

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "result", 2, 2, 2, 2, DriverAndLogin.logFilePath, "TRresult-excel-STG.xlsx", 2, 2, 2, 2, sheetname2);





        }

       



        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide2()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");


            //Slide 2 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);


            driver.Quit();





        }

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");


            //Slide 3 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 10, 13, 0, 8, sheetname2);


            driver.Quit();





        }

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide4()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");


            //Slide 4 comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 17, 21, 0, 5, sheetname2);




            driver.Quit();




        }



        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7,2, 2, sheetname1);


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");

            //Slide 5 comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 25, 32, 0, 4, sheetname2);



            driver.Quit();



        }



    }

}
    