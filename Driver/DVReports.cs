﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Data;
using System.Threading;

namespace Driver
{

    [TestClass]
    public  class DVReports
    {

        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DBConnectionStringDTO DBConnectionParameters1;


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString(),


            };
            //DBConnectionParameters = new DBConnectionStringDTO
            //{
            //    userName = "pulseqa",
            //    password = "password-1",
            //    TCESserverName = "USA-QIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-QIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            //DBConnectionParameters1 = new DBConnectionStringDTO
            //{
            //    userName = "pulsestg",
            //    password = "password-1",
            //    TCESserverName = "USA-SIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-SIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString(),
                IntegratedSecurity = context.Properties["IntegratedSecurity"].ToString(),
                PersistSecurityInfo = context.Properties["PersistSecurityInfo"].ToString()
            };
        }
        //shwetabh srivastava--------added capabilities for download file in specified location at run time
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    // FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    //     service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    //   driver = new FirefoxDriver();
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }




        //Scenario 1


        [TestMethod]
        public void Validate_DV_TargetGroup_Slide2()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5001", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "1");

           
            //Slide 2
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);


            driver.Quit();




        }


        //Scenario 1//

        [TestMethod]
        public void Validate_DV_TargetGroup_Slide3()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);




            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            ///string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5001", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "1");



            //Slide 3 
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 6, 9, 0,8, sheetname2);



            driver.Quit();



        }


        //Scenario 1
        [TestMethod]
        public void Validate_DV_TargetGroup_Slide4()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx",1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5001", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "1");

            //Slide 4
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 13, 17, 0, 5, sheetname2);

  


            driver.Quit();



        }

        //Scenario 1

        [TestMethod]
        public void Validate_DV_TargetGroup_Slide5()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5001", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "1");

            //Slide 5
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 21,28, 0, 4, sheetname2);

    



            driver.Quit();


        }


        //Scenario 2


        [TestMethod]
        public void Validate_DV_Targetgroup_demofilter_Slide2()

        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5002", "DV", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "2");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 2, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);




            driver.Quit();



        }

        //Scenario 2


        [TestMethod]
        public void Validate_DV_Targetgroup_demofilter_Slide3()

        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5002", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "2");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 8, 11, 0, 8, sheetname2);




            driver.Quit();



        }

        //Scenario 2


        [TestMethod]
        public void Validate_DV_Targetgroup_demofilter_Slide4()

        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5002", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "2");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 15, 19, 0, 5, sheetname2);




            driver.Quit();





        }



        //Scenario 2

        [TestMethod]
        public void Validate_DV_Targetgroup_demofilter_Slide5()

        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5002", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "2");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 23, 30, 0, 4, sheetname2);




            driver.Quit();



        }





        //Scenario 3


        [TestMethod]
        public void Validate_DV_Targetgroup_distributionfilter_Slide2()



        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


           // string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



           // string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);




       string InputSPText1 =    Spotlightreports.Spotlightfilters( "5003" , "DV", DBConnectionParameters,"Green");

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "DEV SP to Execute :-" + InputSPText1);



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "3");


            //Slide 2 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);




            driver.Quit();





        }



        //Scenario 3

        [TestMethod]
        public void Validate_DV_Targetgroup_distributionfilter_Slide3()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5003", "DV", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "3");

            //Slide 3 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 8, 11, 0, 8, sheetname2);





            driver.Quit();


        }


        //Scenario 3

        [TestMethod]
        public void Validate_DV_Targetgroup_distributionfilter_Slide4()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");

       

            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5003", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);
            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "3");

            //Slide 4 Comparison


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0,5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 15, 19, 0, 5, sheetname2);



            driver.Quit();




        }



        //Scenario 3

        [TestMethod]
        public void Validate_DV_Targetgroup_distributionfilter_Slide5()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5003", "DV", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "3");

            //Slide 5 Comparison


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 23, 30, 0, 4, sheetname2);




            driver.Quit();



        }


        //Scenario 4


        [TestMethod]
        public void Validate_DV_Targetgroup_customdaterange_Slide2()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5004", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "4");

            //Slide 2 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0,0, sheetname2);



            driver.Quit();




        }


        //Scenario 4
        [TestMethod]
        public void Validate_DV_Targetgroup_customdaterange_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5004", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "4");


            //Slide 3 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 9, 12, 0, 8, sheetname2);



            driver.Quit();




        }


        //Scenario 4
        [TestMethod]
        public void Validate_DV_Targetgroup_customdaterange_Slide4()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5004", "DV", DBConnectionParameters, "Green");





            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "4");


            //Slide 4 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 16, 20, 0,5, sheetname2);



            driver.Quit();




        }


        //Scenario 4//

        [TestMethod]
        public void Validate_DV_Targetgroup_customdaterange_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5004", "DV", DBConnectionParameters, "Green");








            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "4");


            //Slide 5 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 24, 31, 0,4, sheetname2);


            driver.Quit();



        }


        //Scenario 5


        [TestMethod]
        public void Validate_DV_Targetgroup_demo_customdaterange_Slide2()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5005", "DV", DBConnectionParameters, "Green");






            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "5");


            //Slide 2 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 2, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);



            driver.Quit();




        }
        //Scenario 5


        [TestMethod]
        public void Validate_DV_Targetgroup_demo_customdaterange_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx",5,5, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5005", "DV", DBConnectionParameters, "Green");






            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "5");

            //Slide 3 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 10, 13, 0, 8, sheetname2);





            driver.Quit();


        }

        //Scenario 5

        [TestMethod]
        public void Validate_DV_Targetgroup_demo_customdaterange_Slide4()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5005", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "5");


            //Slide 4 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 17, 21, 0, 5, sheetname2);



            driver.Quit();




        }


        //Scenario 5


        [TestMethod]
        public void Validate_DV_Targetgroup_demo_customdaterange_Slide5()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5005", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "5");



            //Slide 5 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 25, 32, 0, 4, sheetname2);



            driver.Quit();




        }


        //Scenario 6 
        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Slide2()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5006", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "6");


            //Slide 2 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 1, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 1, sheetname2);

            
            driver.Quit();


        }

        //Scenario 6 

        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5006", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "6");

            //Slide 3 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 10, 13, 0, 8, sheetname2);


            driver.Quit();





        }


        //Scenario 6 
        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Slide4()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5006", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "6");


            
            //Slide 4 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 17, 21, 0, 5, sheetname2);




            driver.Quit();
            

        }

        //Scenario 6 

        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Slide5()



        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5006", "DV", DBConnectionParameters, "Green");





            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "6");




            //Slide 5 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 25, 32, 0, 4, sheetname2);


            driver.Quit();

        }




        //Scenario 7

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide2()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5007", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");


            //Slide 2 Comparison

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);


            driver.Quit();





        }


        //Scenario 7

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5007", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");


            //Slide 3 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 10, 13, 0, 8, sheetname2);


            driver.Quit();





        }

        //Scenario 7

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide4()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5007", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");


            //Slide 4 comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 17, 21, 0, 5, sheetname2);




            driver.Quit();




        }



        //Scenario 7
        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5007", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "7");

            //Slide 5 comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 25, 32, 0, 4, sheetname2);



            driver.Quit();



        }


        //Scenario 8
        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_datedemo_customdaterange_Slide2()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5008", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "8");

            //Slide 2 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 1, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 1, sheetname2);


            driver.Quit();




        }


        //Scenario 8

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_datedemo_customdaterange_Slide3()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5008", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "8");

            //Slide 3 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 11, 14, 0, 8, sheetname2);


            driver.Quit();




        }

        //Scenario 8

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_datedemo_customdaterange_Slide4()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5008", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "8");

            //Slide 4 Comparison
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 18, 22, 0, 5, sheetname2);


            driver.Quit();




        }

        //Scenario 8

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_datedemo_customdaterange_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5008", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "8");


            //Slide 5 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 26, 33, 0, 4, sheetname2);



            driver.Quit();



        }



        //Scenario 9
        [TestMethod]
        public void Validate_DV_TargetGroup_Comparison_Slide2()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5009", "DV", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "9");


            //Slide 2 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);




            driver.Quit();



        }


        //Scenario 9

        [TestMethod]
        public void Validate_DV_TargetGroup_Comparison_Slide3()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5009", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "9");



            //Slide 3 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 9, 12, 0, 8, sheetname2);



            driver.Quit();




        }

        //Scenario 9

        [TestMethod]
        public void Validate_DV_TargetGroup_Comparison_Slide4()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5009", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "9");



            //Slide 4 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 16, 20, 0, 5, sheetname2);



            driver.Quit();




        }


        //Scenario 9

        [TestMethod]
        public void Validate_DV_TargetGroup_Comparison_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx",9, 9, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5009", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "9");



            //Slide 5 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 24, 31, 0, 4, sheetname2);



            driver.Quit();




        }



        //Scenario 10

        [TestMethod]
        public void Validate_DV_TargetGroup_demofilter_Comparison_Slide2()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);




            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5011", "DV", DBConnectionParameters, "Green");





            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "10");

            //Slide 2 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 4, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 4, sheetname2);




            driver.Quit();



        }


        //Scenario 10

        [TestMethod]
        public void Validate_DV_TargetGroup_demofilter_Comparison_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10,2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5011", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "10");


            //Slide 3 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 10, 13, 0, 8, sheetname2);



            driver.Quit();




        }


        //Scenario 10
        [TestMethod]
        public void Validate_DV_TargetGroup_demofilter_Comparison_Slide4()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 1, 1, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5011", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "10");


            //Slide 4 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 17, 21, 0, 5, sheetname2);




            driver.Quit();



        }

        //Scenario 10

        [TestMethod]
        public void Validate_DV_TargetGroup_demofilter_Comparison_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 1, 1, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5011", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "10");


            //Slide 5 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 25, 32, 0, 4, sheetname2);






            driver.Quit();

        }



        //Scenario 11

        [TestMethod]
        public void Validate_DV_TargetGroup_distributionfilter_Comparison_Slide2()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5013", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "11");

            //Slide 2 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);




            driver.Quit();



        }


        //Scenario 11
        [TestMethod]
        public void Validate_DV_TargetGroup_distributionfilter_Comparison_Slide3()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5013", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "11");


            //Slide 3 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 10, 13, 0, 8, sheetname2);




            driver.Quit();



        }


        //Scenario 11
        [TestMethod]
        public void Validate_DV_TargetGroup_distributionfilter_Comparison_Slide4()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5013", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "11");

            //Slide 4 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 17, 21, 0, 5, sheetname2);




            driver.Quit();



        }


        //Scenario 11

        [TestMethod]
        public void Validate_DV_TargetGroup_distributionfilter_Comparison_Slide5()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5013", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "11");


            //Slide 4 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 25, 32, 0, 4, sheetname2);




            driver.Quit();



        }



        //Scenario 12

        [TestMethod]
        public void Validate_DV_TargetGroup_customdaterange_Comparison_Slide2()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5015", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "12");
            
            //Slide 2 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);




            driver.Quit();



        }



        //Scenario 12

        [TestMethod]
        public void Validate_DV_TargetGroup_customdaterange_Comparison_Slide3()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5015", "DV", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);
            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "12");

            //Slide 3 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 11,14, 0, 8, sheetname2);






            driver.Quit();

        }


        //Scenario 12

        [TestMethod]
        public void Validate_DV_TargetGroup_customdaterange_Comparison_Slide4()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5015", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);
            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "12");


            //Slide 4 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx",19, 23, 0, 5, sheetname2);





            driver.Quit();


        }

        //Scenario 12



        [TestMethod]
        public void Validate_DV_TargetGroup_customdaterange_Comparison_Slide5()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 1, 1, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5015", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "12");

            //Slide 5 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 27, 34, 0, 4, sheetname2);



            driver.Quit();




        }


        //Scenario 13

        [TestMethod]
        public void Validate_DV_TargetGroup_demo_customdaterange_Comparison_Slide2()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5017", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "13");


            //Slide 2 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 4, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 4, sheetname2);




            driver.Quit();



        }


        //Scenario 13
        [TestMethod]
        public void Validate_DV_TargetGroup_demo_customdaterange_Comparison_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5017", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "13");
            
            //Slide 3 Comparison//


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 12, 15, 0, 8, sheetname2);




            driver.Quit();



        }

        //Scenario 13

        [TestMethod]
        public void Validate_DV_TargetGroup_demo_customdaterange_Comparison_Slide4()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5017", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "13");

            //Slide 4 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 20, 24, 0, 5, sheetname2);




            driver.Quit();



        }


        //Scenario 13

        [TestMethod]
        public void Validate_DV_TargetGroup_demo_customdaterange_Comparison_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5017", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);


            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "13");
            
            //Slide 5 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 28, 35, 0, 4, sheetname2);



            driver.Quit();




        }


        //Scenario 14


        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide2()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5019", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "14");

            //Slide 2 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 4, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 4, sheetname2);





            driver.Quit();


        }


        //Scenario 14

        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5019", "DV", DBConnectionParameters, "Green");




            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "14");

            //Slide 3 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 12, 15, 0, 8, sheetname2);




            driver.Quit();



        }



        //Scenario 14
        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide4()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5019", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "14");

            //Slide 4 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 19, 23, 0, 5, sheetname2);




            driver.Quit();



        }


        //Scenario 14
        [TestMethod]
        public void Validate_DV_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide5()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14 ,14, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5019", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "14");

            //Slide 5 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 27, 34, 0, 4, sheetname2);





            driver.Quit();


        }



        //Scenario 15
        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide2()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5021", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "15");


            //Slide 2 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);





            driver.Quit();


        }

        //Scenario 15

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide3()



        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);

            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5021", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "15");

            //Slide 3 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 12, 15, 0, 8, sheetname2);





            driver.Quit();


        }


        //Scenario 15


        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide4()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5021", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "15");

            //Slide 4 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 19, 23, 0, 5, sheetname2);



            driver.Quit();




        }


        //Scenario 15

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide5()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5021", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "15");


            //Slide 5 Comparison//
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 27, 34, 0, 4, sheetname2);


            driver.Quit();





        }


        //Scenario 16


        [TestMethod]
        public void ValidateDVTargetGroupdistributiondatedemocustomdaterangeComparisonSlide2()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5023", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "16");



            //Slide 2 Comparison//

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 1, 1, 0, 3, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 1, 1, 0, 3, sheetname2);



            driver.Quit();




        }


        //Scenario 16

        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide3()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5023", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "16");


            //Slide 3 Comparison//


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 3, 6, 0, 8, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 13, 16, 0, 8, sheetname2);




            driver.Quit();



        }


        //Scenario 16
        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide4()



        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);


            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5023", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "16");

            //Slide 4 Comparison//


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 8, 12, 0, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 19, 24, 0, 5, sheetname2);


            driver.Quit();





        }



        //Scenario 16
        [TestMethod]
        public void Validate_DV_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide5()



        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "0");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5023", "DV", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "DV-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", "16");


            //Slide 5 Comparison//



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "DV-DEVresult", 14, 21, 1, 5, DriverAndLogin.logFilePath, "DV_QA_Data set.xlsx", 28, 35, 0, 4, sheetname2);




            driver.Quit();



        }

    }











}

