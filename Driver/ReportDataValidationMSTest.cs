﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Data;
using Common;

namespace Driver
{
    [TestClass]
    public class ReportDataValidationMSTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string SPColor;
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";
        //public static string TrendCategoryID = "38768";
        //public static string TrendItemID = "83292";
        /// <summary>
        /// QA Parameters
         public static string TrendCategoryID = "17318";
         public static string TrendItemID = "46720";
        public static string TrendNRQuestionID = "46716";
        public static string TrendNRScaleOptionID = "224850";
        /// </summary>



        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
            SPColor = context.Properties["SPColor"].ToString();
            DataPopulationForReportDataValidation.PopulateData(DriverAndLogin.Url, DBConnectionParameters);
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver SetDriverAndNavigateToAccountDefaults()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            else if (UIActions.IsElementVisible(driver, "#account-defaults-title"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            return driver;
        }

        [TestMethod]
        public void ValidateTestSPForResultsReport_Percentage()
        {

            int ScenarioCount = 0;
            int i;
            DataSet ReportFilters;
            string DataCalculation = "Percentage";
            //bool[] SPResultArray;
            string ReportSPName = "USP_GN_ResultsReport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "item", "Percentage");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetResultReportData(ReportFilters, DBConnectionParameters);
                }
            }
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);

                }


            }

        }


        [TestMethod]
        public void ValidateTestSPForResultsReport_Average()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Average";
            string ReportSPName = "USP_GN_ResultsReport";

            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "item", "average");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetResultReportData(ReportFilters, DBConnectionParameters);
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }



            }

        }

        [TestMethod]
        public void ValidateTestSPForInsightsReportForCategory_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForInsightsReportForQuestion_Percentage", w);
                }
                int ScenarioCount = 0;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {

                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    ////ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]
        public void ValidateTestSPForInsightsReportForCategory_Average()
        {
            try
            {
                int ScenarioCount = 0;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();


                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateTestSPForInsightsReportForQuestion_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForInsightsReportForQuestion_Percentage", w);
                }

                int ScenarioCount = 0;
                string ReportDisplay = "Question";
                string DataCalculation = "Percentage";
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]
        public void ValidateTestSPForInsightsReportForQuestion_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForInsightsReportForQuestion_Percentage", w);
                }
                int ScenarioCount = 0;
                string ReportDisplay = "Question";
                string DataCalculation = "Average";
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                    }
                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForCommentReportForscoredandunscoredy()
        {
            String Datacalculation = " ";
            int ScenarioCount = 0;
            string CategoryText = "Medical Benefits";
            int CategoryId = 38769;
            int SurveyFormItemId = 82713;
            DataSet ReportFilters;
            string ReportSPName = "TEST_usp_GetCommentDetailsForCategoryQuestion";
            // string ReportSPName = "usp_GetCommentDetailsForCategoryQuestion";

            //  string Identifier = "'Which of the following best describes your level in the organization?'";
            string Identifier = "'city'";
            //unscored details
            // int SurveyFormItemId = 46760;
            //  string CategoryText = "Unassigned";
            //    int  CategoryId = 17315;
            int PageNumber = 1;
            int PageSize = 20;


            //  @Identifier = 'Which of the following best describes your level in the organization?',@SortBy = '[Score] DESC',@ThenBy = '[CharCount] DESC',
            //@CategoryId = 17319,@SurveyFormItemId = 46717,@PageNumber = @p13 output,@PageSize = 20
            // @p13 = 1

            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetCommentReportData(ReportFilters, DBConnectionParameters, CategoryText);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;

                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + " "
                    + "@Identifier=" + Identifier + "," + "@SortBy='[Score] DESC'" + "," + "@ThenBy='[CharCount] DESC'" + "," + "@CategoryId=" + CategoryId +
                    ", " + "@SurveyFormItemId=" + SurveyFormItemId + "," + "@PageNumber=" + PageNumber + "," + "@PageSize=" + PageSize + ",");

                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;


                //  if (TestSPIDFromScenariosCount == 1)

                //{
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;

                string UserNalyzeFilterID1 = "'" + TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString() + "'";

                int UserAnalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());

                string query = "SELECT SurveyFormID FROM UserAnalyzeFilter WHERE UserAnalyzeFilterID = " + UserNalyzeFilterID1;
                DataSet surveyform = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);


                //DataSet rscount = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);

                // if (rscount.Tables[0].Rows[0][0].ToString() == "0")
                //  {

                spresult = ReportDataValidation.SwitchtocommentComparsion(surveyform, DBConnectionParameters, CategoryText, ResultSet, datasets[TargetIndex], Datacalculation, ReportSPName, UserAnalyzeFilterID);

                //   }
                ReportDataValidation.WriteTestExecutionResult(Datacalculation, ReportSPName, UserAnalyzeFilterID, spresult, DBConnectionParameters);

            }

        }




        [TestMethod]

        public void ValidateTestSPForDrillDown_Category_Percentage()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Percentage";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "category", "percentage");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "category");

                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    //spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);

                }


            }

        }

        [TestMethod]

        public void ValidateTestSPForDrillDown_Category_Average()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Average";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "category", "average");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "category");
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }


            }



        }
        [TestMethod]

        public void ValidateTestSPForDrillDown_Question_Percentage()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "Percentage";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Item", "percentage");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "Item");
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }




            }


        }


        [TestMethod]

        public void ValidateTestSPForDrillDown_Question_Average()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "Average";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "item", "average");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "Item");
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }

            }


        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Year_EntireSurvey_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Year_EntireSurvey_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                String DataCalculation = "Average";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "survey", "1=1", DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }

                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_EntireSurvey_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Year_EntireSurvey_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Year_EntireSurvey_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                String DataCalculation = "percentage";
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "year";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "survey", "1=1", DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }

                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }
        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Year_Item_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Year_Item_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                String DataCalculation = "percentage";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "year";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Year_Item_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Year_Item_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                String DataCalculation = "Average";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "year";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {

                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                             //   AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Year_Category_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Year_Category_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                String DataCalculation = "Percentage";
                string Show = "category";
                string GroupBy = "year";
                string CategoryID = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Year_Category_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Year_Category_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                String DataCalculation = "Average";
                string Show = "category";
                string GroupBy = "year";
                string CategoryID = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }



        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_EntireSurvey_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Quarter_EntireSurvey_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Quarter_EntireSurvey_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                String DataCalculation = "Percentage";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "survey", "1=1", DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }

                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Quarter_EntireSurvey_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Quarter_EntireSurvey_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "quarter";
                String DataCalculation = "Average";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "survey", "1=1", DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }

                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Quarter_Item_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Quarter_Item_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Percentage";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "quarter";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }



        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Quarter_Item_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Quarter_Item_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Average";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "quarter";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Quarter_Category_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Quarter_Category_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Percentage";
                string Show = "category";
                string GroupBy = "quarter";
                string CategoryID = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Quarter_Category_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Quarter_Category_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Average";
                string Show = "category";
                string GroupBy = "quarter";
                string CategoryID = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_EntireSurvey_percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Month_EntireSurvey_percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Month_EntireSurvey_percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string DataCalculation = "percentage";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "survey", "1=1", DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    ////ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }



        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Month_EntireSurvey_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Month_EntireSurvey_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string GroupBy = "month";
                string DataCalculation = "Average";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "survey", "1=1", DBConnectionParameters);

                    }
                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }

                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Month_Item_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Month_Item_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Percentage";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "month";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Month_Item_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Month_Item_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Average";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "month";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Month_Category_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Month_Category_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Percentage";
                string Show = "category";
                string GroupBy = "month";
                string CategoryID = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                             //   AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Month_Category_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Month_Category_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Average";
                string Show = "category";
                string GroupBy = "month";
                string CategoryID = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }

                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_EntireSurvey_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Distribution_EntireSurvey_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Distribution_EntireSurvey_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string DataCalculation = "Percentage";
                string GroupBy = "distribution";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);

                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, "survey", "1=1", DBConnectionParameters);

                    }
                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }

                    }
                    ////ReportDataValidation.WriteTestExecutionResult();
                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_EntireSurvey_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Distribution_EntireSurvey_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Distribution_EntireSurvey_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string DataCalculation = "Average";
                string GroupBy = "distribution";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Category", DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, "survey", "1=1", DBConnectionParameters);
                    }
                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false)
                            {
                                AssertStatus = false;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_Item_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Distribution_Item_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Distribution_Item_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Percentage";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "distribution";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                              //  AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }

        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_Item_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Distribution_Item_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Distribution_Item_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Average";
                string Show = "item";
                string QuestionID = TrendItemID;
                string GroupBy = "distribution";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, QuestionID, "1=1", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, QuestionID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, QuestionID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateTestSPForTrendRatingScaleReport_Distribution_Category_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Distribution_Category_Percentage", w);
                }
                string ReportName = "TrendRatingScaleReport_Distribution_Category_Percentage";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Percentage";
                string Show = "category";
                string CategoryID = TrendCategoryID;
                //string CategoryText = "general";
                string GroupBy = "distribution";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("i = " + i, w);
                        }
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]
        public void ValidateTestSPForTrendRatingScaleReport_Distribution_Category_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendRatingScaleReport_Distribution_Category_Average", w);
                }
                string ReportName = "TrendRatingScaleReport_Distribution_Category_Average";
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Average";
                string Show = "category";
                string GroupBy = "distribution";
                string CategoryID = TrendCategoryID;
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, Show, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend(ReportFilters, "1=1", CategoryID, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, GroupBy, Show, CategoryID, DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, Show, CategoryID, GroupBy);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                            }
                            else
                            {
                               // AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult_hm = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, Show, ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString()), 0, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString()), 1, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString()), 2, DBConnectionParameters);
                                ReportDataValidation.WriteFailedData(DataCalculation, ReportName, Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString()), 3, DBConnectionParameters);
                            }
                            else
                            {
                                //AssertStatus = true;
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }



        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Year()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendNonRatingScaleReport_Year", w);
                }
                int ScenarioCount = 0;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleoptionID = TrendNRScaleOptionID;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "item", "average");
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("i = " + i, w);
                    }
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkDataForTrend_NR(ReportFilters,"46716","224850", DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "year", QuestionID, DBConnectionParameters);
                    }

                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, "224850", "Year");
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, "224850", "Year");
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], ScaleoptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], ScaleoptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], ScaleoptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], ScaleoptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleoptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleoptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleoptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleoptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }

                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Quarter()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendNonRatingScaleReport_Quarter", w);
                }
                int ScenarioCount = 0;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleOptionId = TrendNRScaleOptionID;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("i = " + i, w);
                    }
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "quarter", QuestionID, DBConnectionParameters);

                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, ScaleOptionId, "quarter");
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, ScaleOptionId, "quarter");
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Month()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendNonRatingScaleReport_Month", w);
                }
                int ScenarioCount = 0;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleOptionID = TrendNRScaleOptionID;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("i = " + i, w);
                    }
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "month", QuestionID, DBConnectionParameters);

                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, ScaleOptionID, "Month");
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, ScaleOptionID, "Month");
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], ScaleOptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], ScaleOptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], ScaleOptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], ScaleOptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleOptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleOptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleOptionID);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleOptionID);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Distribution()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForTrendNonRatingScaleReport_Distribution", w);
                }
                int ScenarioCount = 0;
                DataSet ReportFilters;
                string QuestionID = TrendNRQuestionID;
                string ScaleOptionId = TrendNRScaleOptionID;
                bool spresult = false;
                bool spresult_hm = false;
                bool AssertStatus = true;
                string ReportSPName = "Usp_TrendChartReportForNonRating";
                string HeapReportSPName = "Usp_TrendTableReportNonRatingScale";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("i = " + i, w);
                    }
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "distribution", QuestionID, DBConnectionParameters);

                }
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, QuestionID, ScaleOptionId, "Distribution");
                    DataSet ResultSet_hm = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, QuestionID, ScaleOptionId, "Distribution");
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleOptionId);
                        spresult_hm = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet_hm, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3], ScaleOptionId);
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("Target ID : " + TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[0]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 1 ID : " + TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[1]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 2 ID : " + TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[2]["UserAnalyzeFilterID"].ToString(), w);
                            ReportDataValidation.Log("Comparison 3 ID : " + TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString() + "; UserAnalyzeFilterID : " + TestSPIDFromScenarios.Tables[0].Rows[3]["UserAnalyzeFilterID"].ToString(), w);
                            if (spresult == false || spresult_hm == false)
                            {
                                AssertStatus = false;
                                if (spresult == false)
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                if (spresult_hm == false)
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": FAIL ", w);
                                else
                                    ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                            else
                            {
                                ReportDataValidation.Log("Comparison Status of : " + ReportSPName + "_" + SPColor + ": PASS ", w);
                                ReportDataValidation.Log("Comparison Status of : " + HeapReportSPName + "_" + SPColor + ": PASS ", w);
                            }
                        }
                    }
                    //ReportDataValidation.WriteTestExecutionResult();

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }
        [TestMethod]
        public void ValidateTestSPForDemographicReportForCategory_Average()
        {

            int ScenarioCount = 0;
            string ReportDisplay = "Category";
            String DataCalculation = "Average";
            DataSet ReportFilters;
            string DemoLabelForSlice = "'country'";
            int DemographicGroupCount = 0;



            string ReportSPName = "GetDemoOrDistributionSlice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "category", "average");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + " " + "@DemoLabelForSlice=" + DemoLabelForSlice + "," + "@DemographicGroupCount=" + DemographicGroupCount + ",");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;

                int UserAnalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                Console.WriteLine(UserAnalyzeFilterID);
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;



                spresult = ReportDataValidation.CompareDemographicReportResults_category(DataCalculation, ResultSet, datasets[TargetIndex], ReportSPName, UserAnalyzeFilterID, DBConnectionParameters);

                ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UserAnalyzeFilterID, spresult, DBConnectionParameters);

            }
        }

        [TestMethod]
        public void ValidateTestSPForDemographicReportForCategory_Percentage()
        {

            int ScenarioCount = 0;
            string ReportDisplay = "Category";
            String DataCalculation = "Percentage";
            DataSet ReportFilters;
            string DemoLabelForSlice = "'country'";
            int DemographicGroupCount = 0;



            string ReportSPName = "GetDemoOrDistributionSlice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + "  " + "@DemoLabelForSlice= " + DemoLabelForSlice + "," + "@DemographicGroupCount=" + DemographicGroupCount + ",");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                // {

                int UserAnalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                spresult = ReportDataValidation.CompareDemographicReportResults_category(DataCalculation, ResultSet, datasets[TargetIndex], ReportSPName, UserAnalyzeFilterID, DBConnectionParameters);
                //   }

                ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UserAnalyzeFilterID, spresult, DBConnectionParameters);

            }
        }
        [TestMethod]
        public void ValidateTestSPForDemographicReportForQuestion_Average()
        {

            int ScenarioCount = 0;
            string ReportDisplay = "Question";
            String DataCalculation = "Average";
            DataSet ReportFilters;
            string DemoLabelForSlice = "'country'";
            int DemographicGroupCount = 0;

            string ReportSPName = "GetDemoOrDistributionSlice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Item", "average");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + "  " + "@DemoLabelForSlice= " + DemoLabelForSlice + "," + "@DemographicGroupCount=" + DemographicGroupCount + ",");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;

                int UserAnalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                spresult = ReportDataValidation.CompareDemographicReportResults_questions(DataCalculation, ResultSet, datasets[TargetIndex], ReportSPName, UserAnalyzeFilterID, DBConnectionParameters);

                ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UserAnalyzeFilterID, spresult, DBConnectionParameters);


            }
        }


        [TestMethod]

        public void ValidateTestSPForDemographicReportForQuestionItem_Precentage()
        {

            int ScenarioCount = 0;
            string ReportDisplay = "Question";
            String DataCalculation = "Percentage";
            DataSet ReportFilters;
            string DemoLabelForSlice = "'country'";
            int DemographicGroupCount = 0;
            string ReportSPName = "GetDemoOrDistributionSlice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Item", "percentage");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + " " + "@DemoLabelForSlice=" + DemoLabelForSlice + "," + "@DemographicGroupCount=" + DemographicGroupCount + ",");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;

                int UserAnalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());

                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                spresult = ReportDataValidation.CompareDemographicReportResults_questions(DataCalculation, ResultSet, datasets[TargetIndex], ReportSPName, UserAnalyzeFilterID, DBConnectionParameters);
                ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UserAnalyzeFilterID, spresult, DBConnectionParameters);

            }
        }

    }
}
