﻿using System;
using System.Windows.Forms;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.PageObjects;

using Common;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Driver
{
    /// <summary>
    /// Summary description for MigratedPlayerCases
    /// </summary>
    [TestClass]
    public class PlayerCommon
    {
        public PlayerCommon()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        public static bool isLocal = true;
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DBConnectionStringDTO DBConnectionParameters1;
        private static string FormName;
        private static Random random = new Random();


        private static string Locator_GoToHome = "//a[@id='show-home']//span[@class='menu-text'][contains(text(),'Home')]";
        private static string Locator_AuthorNewSurvey = "//*[(@id = 'btnAuthorNewSurvey')]";
        private static string Locator_CreateNewDistrubution = "//a[@id='show-home']//span[@class='menu-text'][contains(text(),'Home')]";


        // Player Objects  Start

        private static string ClosedSurveyMessage = "//div[@class='red jumbotron']";
        private static string FinishButton = "//button[@id='btnFinish']";
        private static string PrivacyLink = "//a[contains(text(),'Privacy')]";
        private static string CopyRightLink = "//div[@class='col-md-6 pulsefooter-cr']";
        private static string MainLogo = "//img[@id='brand-logo-ce-testdriver']";

        //DROP DOWN

        private static string selectDropDown = "//button[@title='Please Select…']";

        private static string element1DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[1]//a[1]//label[1]";
        private static string element2DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[2]//a[1]//label[1]";
        private static string element3DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[3]//a[1]//label[1]";
        private static string element4DropDown = "//div//div[@id='main-container container']//ul[@class='multiselect-container dropdown-menu']//li[4]//a[1]//label[1]";

        private static string errDropDown = "//label[@class='error margin-top-10 error-multiselect']";

        //MULTIPLE CHOICE MULTI SELECT

        private static string multiChoiceInsText = "//div[contains(text(),'1. Chose your favorite Departments')]";
        private static string multiChoiceWithoutInsText ="//div[contains(text(),'1. Without Instruction set : TODO')]";
        private static string NAOtpionisShown = "//span[contains(text(),'Not Applicable')]";
        //Fixed format date paramenter check
        private static string fixedFormatDateCheck = "//input[@type='text']";

        // Player Objects  End


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        //shwetabh srivastava--------added capabilities for download file in specified location at run time
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    // FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    //     service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    //   driver = new FirefoxDriver();
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    InternetExplorerOptions ieoptions = new InternetExplorerOptions();
                    ieoptions.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                    driver = new InternetExplorerDriver(ieoptions);
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    chromeOptions.AddArgument("start-maximized");
                    chromeOptions.AddArgument("no-sandbox");
                    //chromeOptions.AddArgument("disable-infobars");
                    //chromeOptions.AddUserProfilePreference("credentials_enable_service", false);
                    //chromeOptions.AddUserProfilePreference("profile.password_manager_enabled", false);
                    //chromeOptions.AddArgument("--dns-prefetch-disable");

                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
      

        public static void OpenOrCreateForm(IWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        public static void AddDropDownQuestionLocal_WithInsText(RemoteWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }

            EnableInstructionText_DropDown(driver);
            UIActions.Click(driver, "#btnDropDownSave");
        }

        public static IWebElement WaitForSometing(IWebDriver driver, string selector, int wait, string type)
        {
            switch (type)
            {
                case "css":
                    return (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                            .Until(ExpectedConditions.ElementIsVisible(By.CssSelector(selector)));


                case "xpath":
                    return (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                        .Until(ExpectedConditions.ElementIsVisible(By.XPath(selector)));


                default:
                    return (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                            .Until(ExpectedConditions.ElementIsVisible(By.CssSelector(selector)));


            }

        }

        public static void AuthorNewSurvey(IWebDriver driver)
        {
            ClickWithWaitXpath(driver, Locator_AuthorNewSurvey, 30);
        }

        public static void CreateNewDistribution(IWebDriver driver)
        {
            ClickWithWaitXpath(driver, Locator_CreateNewDistrubution, 30);
        }

        public static void GotoHome(IWebDriver driver)
        {
            ClickWithWaitXpath(driver, Locator_GoToHome, 30);
        }

        public static void ClickWithWaitXpath(IWebDriver driver, string selector, int wait)
        {
            (new WebDriverWait(driver, TimeSpan.FromSeconds(wait)))
                .Until(ExpectedConditions.ElementIsVisible(By.XPath(selector))).Click();
        }

        public static IWebElement CreateFormLocal(IWebDriver driver, string formName, string styleSheet = null)
        {
            UIActions.ClickWithWait(driver, "#btnAuthorNewSurvey", 30);
            UIActions.ClickWithWait(driver, "#create-from-scratch", 30);
            UIActions.GetElementWithWait(driver, "#surveyName", 2000);
            IWebElement surveyNameTextBox = driver.FindElement(By.Id("surveyName"));
            surveyNameTextBox.Clear();
            surveyNameTextBox.SendKeys(formName);
            if (styleSheet != null)
            {
                UIActions.SelectInCEBDropdownByText(driver, "#select-style", styleSheet);
            }

            ClickWithWaitXpath(driver, "//a[contains(text(),'Upload logo')]", 30);
            ClickWithWaitXpath(driver, "//button[text()='Upload' and @type='button']", 30);
            Thread.Sleep(5000);

            string imagePath = Environment.CurrentDirectory + "\\yellow-brand.gif";

            var input_field = driver.FindElement(By.XPath("//input[@type='text' and @disabled='']"));
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string title = (string)js.ExecuteScript("arguments[0].removeAttribute(\"disabled\");", input_field);
            Thread.Sleep(5000);

            ClickWithWaitXpath(driver, "//label[@for='input-image-upload']", 30);
            Thread.Sleep(5000);


            SendKeys.SendWait(imagePath);
            Thread.Sleep(10000);
            SendKeys.SendWait(@"{Enter}");


            driver.FindElement(By.XPath("//input[@type='text' and @placeholder='Image Name']")).SendKeys("Gartner Logo - " + RandomString(5));
            ClickWithWaitXpath(driver, "//button[contains(@class,'btn-primary') and text()='Upload']", 30);
            Thread.Sleep(10000);

            UIActions.Click(driver, "#btn-create-survey");
            var firstEmptyPage = UIActions.GetElementWithWait(driver, ".page-list a span", 45);
            return firstEmptyPage;
        }


        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
      
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }

        public static void AddDropDownQuestionLocal(RemoteWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            EnableMultiSelect(driver);
            EnableInstructionText_DropDown(driver);
            UIActions.Click(driver, "#btnDropDownSave");
        }

        public static void AddLongCommentQuestionLocal(RemoteWebDriver driver, string info)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            Thread.Sleep(1000);

            EnableInstructionText_DropDown(driver);

            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }


        public static void AddShortCommentQuestionLocal(RemoteWebDriver driver, string info)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);
            Thread.Sleep(1000);

            EnableInstructionText_DropDown(driver);

            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }

        public static void AddFixedFormatQuestionLocal(RemoteWebDriver driver, string info, string type)
        {
            UIActions.clickwithID(driver, "add-fixed-format");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#fixed-format-type", type);
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);

            EnableInstructionText_DropDown(driver);

            UIActions.clickwithID(driver, "btnFixedFormatSave");
        }

        public static void AddDropDownQuestionLocal_EnableNA(RemoteWebDriver driver, string questionText, string[] responses)
        {
            UIActions.Click(driver, "#add-dropdown");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtDropDownQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddDropDownResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divDropDown .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }
            EnableNotApplicableDropDownSingleSelect(driver);

            UIActions.Click(driver, "#btnDropDownSave");
        }

        private static void EnableMultiSelect(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElement(driver, "#chkMultiSelect").Click();
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='maxResponses']",10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(3.ToString());
            infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='minResponses']", 10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(2.ToString());
        }

        private static void EnableMultiSelectMultiChoice(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElementwithXpath(driver, "//div[@class='row allowMultiSelect']//label", 10).Click();
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='maxMultipleChoice']", 10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(3.ToString());
            infoquestionbox = UIActions.GetElementwithXpath(driver, "//input[@id='minMultipleChoice']", 10);
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(2.ToString());
        }

        public static void AddFixedFormatDateQuestionAsDemoLocal(RemoteWebDriver driver, string info, string[] CategoryName)
        {
            UIActions.clickwithID(driver, "add-fixed-format");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#fixed-format-type", "format_date");
            UIActions.SelectInMultiSelectCEBDropdownByText(driver, "#select-category-Fixed", CategoryName);
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(info);

            EnableInstructionText(driver);
            EnterInstructionText(driver, "this is instruction text for comment question");

            UIActions.clickwithID(driver, "btnFixedFormatSave");

        }

        public static void AddMultipleChoiceMultiItemQuestionLocal(RemoteWebDriver driver, string instructionText, string[] questions, string[] responses)
        {
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#mcScaleType", "multiItem");
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtIncludeMultiItemInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#mcMultiItemsQuestions .addmoreButton button");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#mcMultiItemsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }

            //EnableNotApplicable(driver);
            Thread.Sleep(5000);
            EnableMultiSelectMultiChoice(driver);

            UIActions.Click(driver, "#btnMultipleChoiceSave");
        }

        public static void AddMultipleChoiceMultiItemQuestionLocalWithoutInsText(RemoteWebDriver driver, string instructionText, string[] questions, string[] responses)
        {
            UIActions.Click(driver, "#add-multiple-choice");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#mcScaleType", "multiItem");
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtIncludeMultiItemInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#mcMultiItemsQuestions .addmoreButton button");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#mcMultiItemsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }
            for (int i = 1; i < responses.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }

            var responseBoxes = UIActions.FindElements(driver, "#divMultipleChoices .cke_textarea_inline");
            for (int i = 0; i < responses.Length; i++)
            {
                responseBoxes.ElementAt(i).Click();
                responseBoxes.ElementAt(i).Clear();
                responseBoxes.ElementAt(i).SendKeys(responses[i]);
            }

           UIActions.Click(driver, "#btnMultipleChoiceSave");
        }

        private static void EnterInstructionText(RemoteWebDriver driver, string text)
        {
            /* Click on instruction text */
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            Thread.Sleep(2000);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath("//*[@id='fixedFormat-response']/div/div[1]/div[2]/div"));
            foreach (var item in cont)
            {
                executor.ExecuteScript("arguments[0].click();", item);
                item.Click();
                item.SendKeys("Instruction text for comment question");
            }
        }

        private static void EnableInstructionText(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElement(driver, "#chkInclude").Click();
        }

        private static void EnableInstructionText_DropDown(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(3000);
            UIActions.GetElement(driver, "#includeChkbox").Click();
            //ClickWithWaitXpath(driver, "//input[@id='includeChkbox']", 10);
            Thread.Sleep(5000);

            string instructionText = "Instruction text for drop down question";
            var instructionBox = UIActions.GetElementWithWait(driver, "#txtDropDownInstruction + .cke_textarea_inline", 1);
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);

                    
            Thread.Sleep(3000);
        }

        private static void EnableInstructionText_RatingSingleItem(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            ClickWithWaitXpath(driver, "//*[@id='chkIncludeInstruction']", 10);
            Thread.Sleep(5000);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            Thread.Sleep(2000);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath("//*[@id='fixedFormat-response']/div/div[1]/div[2]/div"));
            foreach (var item in cont)
            {
                executor.ExecuteScript("arguments[0].click();", item);
                item.Click();
                item.SendKeys("Instruction text for comment question");
            }

        }

        private static void EnableNotApplicable(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElementwithXpath(driver, "//div[@class='row NotInclude IncludeNotApplicable']//label",10).Click();
        }

        private static void EnableNotApplicableDropDownSingleSelect(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(5000);
            UIActions.GetElementwithXpath(driver, "//div[@class='row IncludeNotApplicable margin-bottom-10']//label", 10).Click();
        }


        public static void AddRatingScaleQuestionLocal(RemoteWebDriver driver, string questionText, string scaleRange = null, string[] scaleHeaders = null)
        {
            UIActions.Click(driver, "#add-rating-scale");
            Thread.Sleep(5000);
            IWebElement questionTextEditor = UIActions.GetElement(driver, "#txtSingleItemQuestion + .cke_textarea_inline");
            questionTextEditor.Clear();
            questionTextEditor.SendKeys(questionText);
            if (!string.IsNullOrEmpty(scaleRange))
            {
                UIActions.Click(driver, "#rating-Response-heading");
                Thread.Sleep(1000);
                UIActions.SelectInCEBDropdownByText(driver, "#available-rating-scales", scaleRange);
            }
            if (scaleHeaders != null && scaleHeaders.Length > 0)
            {
                var scaleHeaderCheckBoxes = UIActions.FindElements(driver, "#divAddedRowRatingScale input");
                var scaleHeaderTextAreas = UIActions.FindElements(driver, "#divAddedRowRatingScale div.cke_textarea_responses");
                for (int i = 0; i < scaleHeaders.Length; i++)
                {
                    if (!string.IsNullOrEmpty(scaleHeaders[i]))
                    {
                        scaleHeaderCheckBoxes.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Clear();
                        scaleHeaderTextAreas.ElementAt(i).SendKeys(scaleHeaders[i]);
                    }
                }
            }
            Thread.Sleep(2000);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            ac.MoveToElement(driver.FindElement(By.CssSelector("#btnRatingScale"))).Build().Perform();
            Thread.Sleep(5000);


            EnableInstructionText_RatingSingleItem(driver);

            UIActions.Click(driver, "#btnRatingScale");
        }

        public static void AddMultiItemRatingScaleQuestionLocal(RemoteWebDriver driver, string instructionText, string[] questions, string scaleRange = null, string[] scaleHeaders = null)
        {
            UIActions.Click(driver, "#add-rating-scale");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#rating-scale-style", "multiItem");
            var instructionBox = UIActions.GetElement(driver, "#txtRatingScaleInstruction + .cke_textarea_inline");
            instructionBox.Clear();
            instructionBox.SendKeys(instructionText);
            for (int i = 1; i < questions.Length; i++)
            {
                UIActions.Click(driver, "#btnAddRowsResponse");
                Thread.Sleep(1000);
            }
            var questionBoxes = UIActions.FindElements(driver, "#addedRowsQuestions .cke_textarea_inline");
            for (int i = 0; i < questions.Length; i++)
            {
                questionBoxes.ElementAt(i).Click();
                questionBoxes.ElementAt(i).Clear();
                questionBoxes.ElementAt(i).SendKeys(questions[i]);
            }
            if (!string.IsNullOrEmpty(scaleRange))
            {
                UIActions.Click(driver, "#rating-Response-heading");
                Thread.Sleep(1000);
                UIActions.SelectInCEBDropdownByText(driver, "#available-rating-scales", scaleRange);
            }
            if (scaleHeaders != null && scaleHeaders.Length > 0)
            {
                var scaleHeaderCheckBoxes = UIActions.FindElements(driver, "#divAddedRowRatingScale input");
                var scaleHeaderTextAreas = UIActions.FindElements(driver, "#divAddedRowRatingScale div.cke_textarea_responses");
                for (int i = 0; i < scaleHeaders.Length; i++)
                {
                    if (!string.IsNullOrEmpty(scaleHeaders[i]))
                    {
                        scaleHeaderCheckBoxes.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Click();
                        scaleHeaderTextAreas.ElementAt(i).Clear();
                        scaleHeaderTextAreas.ElementAt(i).SendKeys(scaleHeaders[i]);
                    }
                }
            }


            EnableInstructionText(driver);
            EnterInstructionText(driver, "this is instruction text for rating scale question");



            UIActions.Click(driver, "#btnRatingScale");
        }

      
        /// <summary>
        /// ////////////////////////////   TEST CASES START ////////////////////////////////////////
        /// </summary>
        [TestMethod]
        public void PlayerTest_ClosedSurveyCheck()
        {
            TestContext.WriteLine("PS9638: Verify if the user is able to view the following message in the welcome page when user launches a closed survey - survey is closed");

            // URL is hard coded for now

            string stageClosedURL = "https://stg-surveys.cebglobal.com/Pulse/Player/Start/26827/0/5273628d-39e2-44db-bdfe-89fea982509e";

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            }



            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, ClosedSurveyMessage, 10);
            TestContext.WriteLine("Checking whether the survey closed element is present");
            Assert.IsTrue(_embLnk.Displayed);
            TestContext.WriteLine("Validating the survey closed text is displayed.");
            Assert.AreEqual(_embLnk.Text.ToString(), "This survey is now closed. Thank you for your interest. To exit this survey, please close your browser.");

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_TwoSimultaneousBrowsers()
        {
            TestContext.WriteLine("PS9618: Verify if the exit page is displayed when the survey is launched from different browsers at the same time");

            // URL is hard coded for now

            string stageClosedURL = "https://stg-surveys.cebglobal.com/player/link/a632207b11fc41d3b484d75698de0f7b?lang=EN_US";
            TestContext.WriteLine("Opening a survey url in 2 simultaneous browser sessions");
            RemoteWebDriver driver1 = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);
            RemoteWebDriver driver2 = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver1, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session 1");
            Assert.IsTrue(_embLnk.Displayed);

            _embLnk = UIActions.GetElementswithXpath(driver2, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session 2");
            Assert.IsTrue(_embLnk.Displayed);

            TestContext.WriteLine("Cleaning up the test and exiting");
            if (driver1 != null)
            {
                driver1.Close();
                driver1.Dispose();
            }

            if (driver2 != null)
            {
                driver2.Close();
                driver2.Dispose();
            }

            //TestContext.WriteLine("Cleaning up the test and exiting");
            //CleanUp();
        }

        [TestMethod]
        public void PlayerTest_InvalidSurveyURL()
        {
            TestContext.WriteLine("PS9640: Verify if error thrown in the browser when user clicks on a invalid URL.");

            // URL is hard coded for now

            //Initialize the Webdriver and login

            string stageClosedURL = "https://stg-surveys.clearforce.com/player/link/a632207b11fc41d3b484d75698de0f7b?lang=EN_US";
            TestContext.WriteLine("Opening an invalid survey url ");

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, stageClosedURL);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            }
            bool _result = true;
            TestContext.WriteLine("Checking for error...");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, PrivacyLink);
                _result = false;
            }
            catch (Exception)
            {
                _result = true;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_AddDropDownMultiSelectQuestion_NoResponseBeyondLimit()
        {
            TestContext.WriteLine("Test_AddDropDownMultiSelectQuestion_NoResponseBeyondLimit");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Validate Add DropDown SingleSelect Question

            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AddDropDownQuestionLocal(driver, "Which of the following best describes your level in the organization?", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session");
            Assert.IsTrue(_embLnk.Displayed);

            ClickWithWaitXpath(driver, selectDropDown, 10);

            ClickWithWaitXpath(driver, element1DropDown, 10);
            _embLnk = UIActions.GetElementswithXpath(driver, errDropDown, 10);
            TestContext.WriteLine("Checking whether the warning message is displayed to select response in accordance with minimum limit set");
            Assert.IsTrue(_embLnk.Displayed);

            ClickWithWaitXpath(driver, element2DropDown, 10);
            bool _result = true;
            TestContext.WriteLine("Checking whether the warning message is displayed to select response in accordance with minimum limit set");
            try
            {
                _embLnk = UIActions.FindElementWithXpath(driver, errDropDown);
                _result = false;
            }
            catch (Exception)
            {
                _result = true;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }

        [TestMethod]
        public void PlayerTest_AddDropDownMultiSelectQuestion_DisabledResponseBeyondLimit()
        {
            TestContext.WriteLine("Test_AddDropDownMultiSelectQuestion_DisabledResponseBeyondLimit");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Validate Add DropDown SingleSelect Question

            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AddDropDownQuestionLocal(driver, "Which of the following best describes your level in the organization?", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session ");
            Assert.IsTrue(_embLnk.Displayed);

            ClickWithWaitXpath(driver, selectDropDown, 10);
            Thread.Sleep(5000);

            ClickWithWaitXpath(driver, element1DropDown, 10);
            Thread.Sleep(3000);
            ClickWithWaitXpath(driver, element2DropDown, 10);
            Thread.Sleep(3000);
            ClickWithWaitXpath(driver, element3DropDown, 10);
            Thread.Sleep(3000);

            bool _result = false;
            TestContext.WriteLine("Checking whether outstanding reponses were disabled in accordance with maximum limit set");
            try
            {
                _embLnk = UIActions.FindElementWithXpath(driver, element4DropDown);
                if (_embLnk.Enabled)
                {
                    _result = true;
                }
            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_AddFixedFormatDate_WithoutInstructionText()
        {
            TestContext.WriteLine(" cTest_AddFixedFormatDate_WithoutInstructionText");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add FixedFormat Date Without InstructionText

            var QuestionText = "FixedFormatDATEQuestion" + Guid.NewGuid();
            String[] CategoryName = { "Demographic" };
            AuthorActions.AddFixedFormatDateQuestionAsDemo(driver, QuestionText, CategoryName);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session ");
            Assert.IsTrue(_embLnk.Displayed);

            bool _result = false;
            TestContext.WriteLine("Checking whether date picker class is displayed");
            try
            {
                _embLnk = UIActions.FindElementWithXpath(driver, fixedFormatDateCheck);
                if (_embLnk.GetAttribute("class").Contains("datepicker"))
                {
                    _result = true;
                }
            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }

        [TestMethod]
        public void PlayerTest_AddFixedFormatDate_WithInstructionText()
        {
            TestContext.WriteLine("Test_AddFixedFormatDate_WithInstructionText");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add FixedFormat Date Without InstructionText

            var QuestionText = "FixedFormatDATEQuestion" + Guid.NewGuid();
            String[] CategoryName = { "Demographic" };
            AddFixedFormatDateQuestionAsDemoLocal(driver, QuestionText, CategoryName);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session ");
            Assert.IsTrue(_embLnk.Displayed);

            bool _result = false;
            TestContext.WriteLine("Checking whether date picker class is displayed");
            try
            {
                _embLnk = UIActions.FindElementWithXpath(driver, fixedFormatDateCheck);
                if (_embLnk.GetAttribute("class").Contains("datepicker"))
                {
                    _result = true;
                }
            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_AddMultipleChoiceMultiItemQuestion_WithInstructionSet()
        {
            TestContext.WriteLine("Test_AddMultipleChoiceMultiItemQuestion_WithInstructionSet");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice MultiItem Question

            string[] questions = { "First", "Second", "Third" };
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceMultiItemQuestion(driver, "Chose your favorite Departments", questions, responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            bool _result = false;
            TestContext.WriteLine("Checking whether instruction text is displayed");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, multiChoiceInsText);
                _result = true;
                
            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_AddMultipleChoiceMultiItemQuestion_WithNAEnabled()
        {
            TestContext.WriteLine("Test_AddMultipleChoiceMultiItemQuestion_WithNAEnabled");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice MultiItem Question

            string[] questions = { "First", "Second", "Third" };
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AddMultipleChoiceMultiItemQuestionLocal(driver, "Chose your favorite Departments", questions, responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            bool _result = false;
            TestContext.WriteLine("Checking whether NA Option text is displayed");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, NAOtpionisShown);
                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_AddMultipleChoiceMultiItemQuestion_WithOutInstructionSet()
        {
            TestContext.WriteLine("Test_AddMultipleChoiceMultiItemQuestion_WithOutInstructionSet");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice MultiItem Question

            string[] questions = { "First", "Second", "Third" };
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AddMultipleChoiceMultiItemQuestionLocalWithoutInsText(driver, "Without Instruction set : TODO", questions, responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            bool _result = false;
            TestContext.WriteLine("Checking whether instruction text is displayed");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, multiChoiceWithoutInsText);
                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_AddMultipleChoiceMultiItemQuestion_WithResponseLimit()
        {
            TestContext.WriteLine("Test_AddMultipleChoiceMultiItemQuestion_WithResponseLimit");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice MultiItem Question

            string[] questions = { "First", "Second", "Third" };
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AddMultipleChoiceMultiItemQuestionLocal(driver, "Chose your favorite Departments", questions, responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            bool _result = false;
            TestContext.WriteLine("Checking whether Finish button is displayed");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, FinishButton);
                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath("//tr[contains(@id,'qgroup_tabular')]/td[2]/div/label"));
            foreach (var item in cont)
            { 
                item.Click();
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, errDropDown);
                Assert.IsTrue(_embLnk.Displayed);
                break;
            }

            
            Thread.Sleep(5000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_AddMultipleChoiceSingleItemQuestion_WithInstructionSet()
        {
            TestContext.WriteLine("Test_AddMultipleChoiceSingleItemQuestion_WithInstructionSet");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice Question Single Item

            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            bool _result = false;
            TestContext.WriteLine("Checking whether instruction text is displayed");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, "//span[contains(text(),'1. Choose you Department')]");
                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_BackButtonAfterFinishingtheSurvey()
        {
            TestContext.WriteLine("Test_BackButtonAfterFinishingtheSurvey");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice Question Single Item

            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            bool _result = false;
            TestContext.WriteLine("Checking whether instruction text is displayed");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, "//span[contains(text(),'1. Choose you Department')]");
                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            ClickWithWaitXpath(driver, FinishButton, 10);
            Thread.Sleep(10000);
            string actualvalue = driver.Url;
            Assert.IsTrue(actualvalue.Contains("Finish"));

            driver.Navigate().Back();

            _result = false;
            TestContext.WriteLine("Checking whether back func is working fine after finishing the survey");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, "//h2[contains(text(),'This link is not currently valid.')]");
                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }

        
        [TestMethod]
        public void PlayerTest_RatingScaleSingleItem_WithInstructionSet()
        {
            TestContext.WriteLine("Test_RatingScaleSingleItem_WithInstructionSet");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Rating Scale Question

            AddRatingScaleQuestionLocal(driver, "How do you rate this Automation framework?");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string listOptions = "//div[contains(text(),'8')]";

            bool _result = false;
            TestContext.WriteLine("Checking whether user could able to add responses to the single item rating scale question type");
            try
            {
                IWebElement _emlink = UIActions.FindElementWithXpath(driver, listOptions);
                _emlink.Click();

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_RatingScaleSingleItem_WithDifferentResponses_FirstLastHeaders()
        {
            TestContext.WriteLine("Test_RatingScaleSingleItem_WithDifferentResponses_FirstLastHeaders");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            // Add Rating Scale Question

            string[] scaleHeaders = { "One", "", "", "", "", "" };
            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate the old Automation framework?", "1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string listOptions = "//div[contains(text(),'8')]";

            bool _result = false;
            TestContext.WriteLine("Checking whether user could able to add responses to the single item rating scale question type");
            try
            {
                IWebElement _emlink = UIActions.FindElementWithXpath(driver, listOptions);
                _emlink.Click();

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_RatingScaleSingleItem_WithDifferentResponses_WithoutHeaders()
        {
            TestContext.WriteLine("Test_RatingScaleSingleItem_WithDifferentResponses_WithoutHeaders");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Rating Scale Question

            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate this Automation framework?");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string listOptions = "//div[contains(text(),'8')]";

            bool _result = false;
            TestContext.WriteLine("Checking whether user could able to add responses to the single item rating scale question type");
            try
            {
                IWebElement _emlink = UIActions.FindElementWithXpath(driver, listOptions);
                _emlink.Click();
               
                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }



        [TestMethod]
        public void PlayerTest_RatingScaleMultiItem_WithDifferentResponses_WithallResponses()
        {
            TestContext.WriteLine("Test_RatingScaleSingleItem_WithDifferentResponses_WithoutHeaders");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);


            //  Add MultiItem Rating Scale Question

            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below automation frameworks", questions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string listOptions = "//tr[contains(@id,'qgroup_tabular')]/td[2]/div/label";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to Click all responses of the mutli item rating question ");
            try
            {
                IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath(listOptions));
                foreach (var item in cont)
                {
                    item.Click();
                    Thread.Sleep(1000);
                }

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }




        [TestMethod]
        public void PlayerTest_RatingScaleMultiItem_WithDifferentResponses_WithFirstLastHeaders()
        {
            TestContext.WriteLine("Test_RatingScaleMultiItem_WithDifferentResponses_WithFirstLastHeaders");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            Thread.Sleep(5000);
            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            string[] scaleHeaders = { "One", "", "", "", "", "Six" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below new automation frameworks", questions, "1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string listOptions = "//tr[contains(@id,'qgroup_tabular')]/td[2]/div/label";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath(listOptions));
                foreach (var item in cont)
                {
                    item.Click();
                    Thread.Sleep(1000);
                }

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }



        [TestMethod]
        public void PlayerTest_RatingScaleMultiItem_WithDifferentResponses_WithallHeaders()
        {
            TestContext.WriteLine("Test_RatingScaleMultiItem_WithDifferentResponses_WithallHeaders");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);


            //  Add MultiItem Rating Scale Question


            Thread.Sleep(5000);
            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            string[] scaleHeaders = { "One", "Two", "Three", "Four", "Five", "Six" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below new automation frameworks", questions, "1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string listOptions = "//tr[contains(@id,'qgroup_tabular')]/td[2]/div/label";
            
            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath(listOptions));
                foreach (var item in cont)
                {
                    item.Click();
                    Thread.Sleep(1000);
                }

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_ShortComment_SpecialCharactersResponse()
        {
            TestContext.WriteLine("Test_ShortComment_SpecialCharactersResponse");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string commentBox = "//input[@type='text']";
            string longComment = @"!@#$%^&*()_+{}| were special characters";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);


                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }


        [TestMethod]
        public void PlayerTest_ShortComment_OtherLanguageResponse()
        {
            TestContext.WriteLine("Test_ShortComment_SpecialCharacters");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//input[@type='text']";
            string longComment = @"ஒழுக்கமற்றவர்கள் பல நேரங்களில் தங்களை அதீத சுதந்திரவாதிகளாக கருதிக்கொள்கிறார்கள். ஆனால் அவர்கள் உணர்வதிலும் அன்பு செலுத்துவதிலும் குறைபாடுடையவர்கள் - சார்லஸ் புகோவ்ஸ்கி";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);


                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }


        [TestMethod]
        public void PlayerTest_ShortComment_WithInstructionSet()
        {
            TestContext.WriteLine("Test_ShortComment_WithInstructionSet");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AddShortCommentQuestionLocal(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//input[@type='text']";
            string longComment = @"Conveniently implement worldwide processes and process-centric platforms. Synergistically myocardinate business architectures and cross functional deliverables. Competently synthesize reliable information before front-end infrastructures. Synergistically benchmark long-term high-impact vortals before user-centric architectures. Dramatically recaptiualize equity invested innovation vis-a-vis high-quality results.";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);


                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }


        [TestMethod]
        public void PlayerTest_ShortComment_ResponsesBeyondLimit()
        {
            TestContext.WriteLine("Test_ShortComment_ResponsesBeyondLimit");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//input[@type='text']";
            string longComment = @"Conveniently implement worldwide processes and process-centric platforms. Synergistically myocardinate business architectures and cross functional deliverables. Competently synthesize reliable information before front-end infrastructures. Synergistically benchmark long-term high-impact vortals before user-centric architectures. Dramatically recaptiualize equity invested innovation vis-a-vis high-quality results.

Monotonectally morph optimal methods of empowerment after focused meta-services. Compellingly benchmark viral models rather than cross-unit value. Assertively cultivate robust results with timely leadership skills. Appropriately engage low-risk high-yield expertise whereas impactful testing procedures. Continually mesh market positioning catalysts for change rather than long-term high-impact scenarios.

Energistically monetize cost effective networks via cross functional infomediaries. Rapidiously plagiarize enterprise architectures via client-based sources. Compellingly aggregate principle-centered solutions and integrated platforms. Uniquely pontificate robust data after intuitive e-tailers. Credibly visualize impactful communities vis-a-vis highly efficient e-tailers.

Energistically initiate tactical outsourcing via timely models. Dramatically enable clicks-and-mortar functionalities whereas accurate meta-services. Compellingly conceptualize reliable meta-services vis-a-vis sticky platforms.";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the long comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);

                _embLnk = UIActions.FindElementWithXpath(driver, errMsg);
                Assert.IsTrue(_embLnk.Displayed);
                Thread.Sleep(3000);

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_LongComment_WithoutInstructionText()
        {
            TestContext.WriteLine("Test_LongComment_WithoutInstructionText");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);


            //  Add Long Comment Question

            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//textarea[contains(@class,'form-control mandatoryValue')]";
            string longComment = @"Some Comment for testing";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the long comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);

                //_embLnk = UIActions.FindElementWithXpath(driver, errMsg);
                //Assert.IsTrue(_embLnk.Displayed);
                //Thread.Sleep(3000);

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_LongComment_SpecialCharacterResponse()
        {
            TestContext.WriteLine("Test_LongComment_SpecialCharacterResponse");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);


            //  Add Long Comment Question

            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//textarea[contains(@class,'form-control mandatoryValue')]";
            string longComment = @"(/*-+_@&$#%) in a string?";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the long comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);

                //_embLnk = UIActions.FindElementWithXpath(driver, errMsg);
                //Assert.IsTrue(_embLnk.Displayed);
                //Thread.Sleep(3000);

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();


        }


        [TestMethod]
        public void PlayerTest_LongComment_OtherLanguageResponse()
        {
            TestContext.WriteLine("Test_LongComment_OtherLanguageResponse");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);


            //  Add Long Comment Question

            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//textarea[contains(@class,'form-control mandatoryValue')]";
            string longComment = @"ஒழுக்கமற்றவர்கள் பல நேரங்களில் தங்களை அதீத சுதந்திரவாதிகளாக கருதிக்கொள்கிறார்கள். ஆனால் அவர்கள் உணர்வதிலும் அன்பு செலுத்துவதிலும் குறைபாடுடையவர்கள் - சார்லஸ் புகோவ்ஸ்கி";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the long comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);

                //_embLnk = UIActions.FindElementWithXpath(driver, errMsg);
                //Assert.IsTrue(_embLnk.Displayed);
                //Thread.Sleep(3000);

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_LongComment_BeyondCharacterLimitResponse()
        {
            TestContext.WriteLine("Test_LongComment_OtherLanguageResponse");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);


            //  Add Long Comment Question

            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//textarea[contains(@class,'form-control mandatoryValue')]";
            string longComment = @"Synergistically transform multimedia based intellectual capital with pandemic information. Phosfluorescently initiate resource-leveling leadership skills via sustainable solutions. Completely negotiate cost effective expertise for unique convergence. Compellingly communicate proactive users for magnetic outsourcing. Completely utilize installed base leadership skills for fully tested technologies.

Compellingly repurpose leading-edge strategic theme areas via diverse e-commerce. Intrinsicly promote transparent niches with technically sound infomediaries. Interactively utilize robust channels before web-enabled total linkage. Assertively pursue an expanded array of paradigms before transparent action items. Appropriately reintermediate granular e-business after B2C outsourcing.

Conveniently harness enabled technologies with parallel interfaces. Synergistically reinvent stand-alone potentialities for quality infrastructures. Distinctively iterate economically sound sources after leading-edge alignments. Seamlessly e-enable standards compliant results without virtual technologies. Professionally promote premium methods of empowerment with resource-leveling action items.

Compellingly target high-payoff process improvements after worldwide ideas. Distinctively drive timely niche markets with innovative catalysts for change. Holisticly scale extensive networks rather than standardized metrics. Phosfluorescently reintermediate innovative ROI for interoperable users. Objectively deliver client-based technology vis-a-vis interactive outsourcing.

Proactively evisculate enabled methods of empowerment with user-centric quality vectors. Credibly scale 2.0 ROI rather than technically sound e-markets. Quickly synergize proactive platforms after granular collaboration and idea-sharing. Holisticly maintain market positioning experiences for enterprise-wide vortals. Professionally evisculate value-added partnerships after user friendly core competencies.

Conveniently strategize turnkey strategic theme areas via interdependent schemas. Holisticly fabricate end-to-end infomediaries with front-end process improvements. Dynamically revolutionize B2C content whereas client-centric mindshare. Rapidiously grow distributed collaboration and idea-sharing vis-a-vis transparent results. Efficiently communicate efficient experiences whereas client-centric architectures.

Energistically administrate bricks-and-clicks customer service for interdependent outside the box thinking. Phosfluorescently engage magnetic technologies through standards compliant customer service. Distinctively optimize state of the art customer service before interactive deliverables. Progressively revolutionize progressive methodologies before one-to-one growth strategies. Uniquely disseminate backward-compatible e-commerce via turnkey web services.

Rapidiously target efficient mindshare after robust customer service. Rapidiously benchmark cross functional technologies vis-a - vis out-of - the - box niche markets. Interactively underwhelm clicks - and - mortar information rather than ethical bandwidth.Rapidiously grow user - centric outsourcing vis-a - vis maintainable outsourcing. Authoritatively innovate leveraged interfaces before revolutionary technology.

Uniquely generate maintainable scenarios vis - a - vis goal - oriented web services. Collaboratively engage strategic content for alternative benefits. Progressively develop plug - and - play systems vis - a - vis visionary methodologies.Collaboratively expedite customized products and extensive materials.Dramatically reinvent interactive methodologies after alternative interfaces.
      

Energistically productize multidisciplinary alignments via emerging information.Intrinsicly harness maintainable e - services via equity invested human capital.Synergistically impact premier deliverables through state of the art ROI.Completely procrastinate customized data and plug - and - play communities.Interactively simplify an expanded array of quality vectors through fully tested catalysts for change.

Enthusiastically strategize extensible initiatives through tactical supply chains.Rapidiously predominate premier manufactured products through efficient outsourcing.Dramatically morph functionalized e - tailers vis - a - vis market - driven total linkage.Intrinsicly create innovative metrics rather than extensive materials.Dramatically harness holistic infomediaries and intuitive intellectual capital.
        

Collaboratively recaptiualize visionary partnerships and client - based vortals.Professionally procrastinate long - term high - impact infrastructures whereas interdependent content.Seamlessly architect excellent networks without sustainable total linkage.Credibly impact highly efficient benefits before turnkey e - markets.Assertively create installed base content for e - business catalysts for change.";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the long comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);

                _embLnk = UIActions.FindElementWithXpath(driver, errMsg);
                Assert.IsTrue(_embLnk.Displayed);
                Thread.Sleep(3000);

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_LongComment_WithInstructionText()
        {
            TestContext.WriteLine("Test_LongComment_WithInstructionText");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);


            //  Add Long Comment Question

            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AddLongCommentQuestionLocal(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string commentBox = "//textarea[contains(@class,'form-control mandatoryValue')]";
            string longComment = "Holisticly evolve cost effective e-commerce for ethical quality vectors. Energistically empower flexible e-commerce via reliable manufactured products. Collaboratively generate virtual systems and economically sound quality.";
            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the long comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_FixedFormat_EmailAddressCheck()
        {
            TestContext.WriteLine("Test_LongComment_WithInstructionText");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Fixed Format Question

            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_email_address");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string inputURL = "//input[@type='text']";
            string errorText = "//label[contains(@class,'error')]";
            bool _result = false;
            TestContext.WriteLine("Checking whether invalid entry to email is giving error message");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, inputURL);
                _embLnk.SendKeys("abc");
                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(5000);
                _embLnk = UIActions.FindElementWithXpath(driver, errorText);
                Assert.IsTrue(_embLnk.Displayed);
                Thread.Sleep(3000);

                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            _result = true;
            TestContext.WriteLine("Checking whether valid entry to email is not giving error message");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, inputURL);
                _embLnk.Clear();
                _embLnk.SendKeys("abc@abc.com");
                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(5000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));


                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }
            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_FixedFormat_URLCheck_OtherLanguageResponse()
        {
            TestContext.WriteLine("Test_FixedFormat_URLCheck_OtherLanguageResponse");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Fixed Format Question

            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_website_url");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string inputURL = "//input[@type='text']";
            string errorText = "//label[contains(@class,'error')]";
            bool _result = false;
            TestContext.WriteLine("Checking whether invalid entry to url is giving error message");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, inputURL);
                _embLnk.SendKeys("வாழ்க வளமுடன்");
                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(5000);
                _embLnk = UIActions.FindElementWithXpath(driver, errorText);
                Assert.IsTrue(_embLnk.Displayed);
                Thread.Sleep(3000);

                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }


            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_FixedFormat_URLCheck()
        {
            TestContext.WriteLine("Test_FixedFormat_URLCheck");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Fixed Format Question

            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_website_url");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(10000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string inputURL = "//input[@type='text']";
            string errorText = "//label[contains(@class,'error')]";
            bool _result = false;
            TestContext.WriteLine("Checking whether invalid entry to url is giving error message");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, inputURL);
                _embLnk.SendKeys("abc");
                ClickWithWaitXpath(driver, FinishButton,10);
                Thread.Sleep(5000);
                _embLnk = UIActions.FindElementWithXpath(driver, errorText);
                Assert.IsTrue(_embLnk.Displayed);
                Thread.Sleep(3000);

                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            _result = true;
            TestContext.WriteLine("Checking whether valid entry to url is giving error message");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, inputURL);
                _embLnk.Clear();
                _embLnk.SendKeys("http://google.com");
                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(5000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));


                _result = true;

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }
            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_FixedFormat_NumberCheck_WithInstructionSet()
        {
            TestContext.WriteLine("Test_FixedFormat_NumberCheck_WithInstructionSet");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Fixed Format Question

            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AddFixedFormatQuestionLocal(driver, QuestionText, "Number");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(10000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string inputFF = "//input[@type='text']";
            UIActions.GetElementwithXpath(driver, inputFF, 10).SendKeys("abc");
            Thread.Sleep(3000);

            bool _result = false;
            TestContext.WriteLine("Checking whether alphabets entered were cleared automatically");
            try
            {
                string textDisplayed = UIActions.GetElementwithXpath(driver, inputFF, 10).Text;
                if (textDisplayed == "")
                {
                    _result = true;
                }
                else
                {
                    _result = false;
                }

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_FixedFormat_NumberCheck_WithOutInstructionSet()
        {
            TestContext.WriteLine("Test_FixedFormat_NumberCheck_WithOutInstructionSet");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Fixed Format Question

            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "Number");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(10000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string inputFF = "//input[@type='text']";
            UIActions.GetElementwithXpath(driver, inputFF, 10).SendKeys("abc");
            Thread.Sleep(3000);

            bool _result = false;
            TestContext.WriteLine("Checking whether alphabets entered were cleared automatically");
            try
            {
                string textDisplayed = UIActions.GetElementwithXpath(driver, inputFF, 10).Text;
                if (textDisplayed == "")
                {
                    _result = true;
                }
                else
                {
                    _result = false;
                }

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_FixedFormat_NumberCheck_WithAlphabets()
        {
            TestContext.WriteLine("Test_FixedFormat_NumberCheck_WithAlphabets");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);



            //  Add Fixed Format Question

            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "Number");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(10000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string inputFF = "//input[@type='text']";
            UIActions.GetElementwithXpath(driver, inputFF, 10).SendKeys("abc");
            Thread.Sleep(3000);

            bool _result = false;
            TestContext.WriteLine("Checking whether alphabets entered were cleared automatically");
            try
            {
                string textDisplayed = UIActions.GetElementwithXpath(driver, inputFF, 10).Text;
                if (textDisplayed == "")
                {
                    _result = true;
                }
                else
                {
                    _result = false;
                }

            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_AddDropDownMultiSelectQuestion_WithInstructionText()
        {
            TestContext.WriteLine("Test_AddDropDownMultiSelectQuestion_WithInstructionText");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Validate Add DropDown SingleSelect Question

            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AddDropDownQuestionLocal(driver, "Which of the following best describes your level in the organization?", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session");
            Assert.IsTrue(_embLnk.Displayed);

            string insText = "//span[@data-bind-type='sanitizedhtml']";

            _embLnk = UIActions.GetElementswithXpath(driver, insText, 10);
            TestContext.WriteLine("Checking whether instuction set is displayed");
            Assert.IsTrue(_embLnk.Displayed);
            
            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_AddDropDown_WithInstructionText()
        {
            TestContext.WriteLine("Test_AddDropDown_WithInstructionText");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Validate Add DropDown SingleSelect Question

            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AddDropDownQuestionLocal_WithInsText(driver, "Which of the following best describes your level in the organization?", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(10000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementwithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session");
            Assert.IsTrue(_embLnk.Displayed);

            string insText = "//span[@data-bind-type='sanitizedhtml']";

            _embLnk = UIActions.GetElementwithXpath(driver, insText, 10);
            TestContext.WriteLine("Checking whether Instruction text is displayed");
            Assert.IsTrue(_embLnk.Displayed);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }



        [TestMethod]
        public void PlayerTest_AddDropDown_WithOutInstructionText()
        {
            TestContext.WriteLine("Test_AddDropDown_WithOutInstructionText");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Validate Add DropDown SingleSelect Question

            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AuthorActions.AddDropDownQuestion(driver, "Which of the following best describes your level in the organization?", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(10000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementwithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session");
            Assert.IsTrue(_embLnk.Displayed);

            string insText = "//span[@data-bind-type='sanitizedhtml']";
            
            _embLnk = UIActions.GetElementwithXpath(driver, insText, 10);
            TestContext.WriteLine("Checking whether Instruction text is displayed");
            Assert.IsTrue(_embLnk.Displayed);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_LogoTest_UpdatedLogoPreview()
        {
            TestContext.WriteLine("PS10680: Verify Survey Logo is displayed correctly");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = CreateFormLocal(driver, FormName);
            Thread.Sleep(10000);

            // Add Multi Choice Scale Question

            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);
            Assert.IsTrue(UIActions.IsElementVisible(driver, "//*[@id='surveyLogoID']/img"));
            Thread.Sleep(5000);
            IWebElement imageSrc = UIActions.FindElementWithXpath(driver, "//*[@id='surveyLogoID']/img");
            string imageSrcURL = imageSrc.GetAttribute("src");



            driver.Navigate().GoToUrl(DriverAndLogin.Url);

            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            AuthorActions.SearchAndOpenForm(driver, FormName);
            Thread.Sleep(10000);
            ClickWithWaitXpath(driver, "//*[@id='btnFormEdit']", 30);
            ClickWithWaitXpath(driver, "//*[@id='edit-form-button']", 30);
            Thread.Sleep(10000);
            Thread.Sleep(10000);
            ClickWithWaitXpath(driver, "//*[@id='btnFormSettings']", 30);

            ClickWithWaitXpath(driver, "//*[@id='branding-form-option']", 30);


            //Upload Image 
            ClickWithWaitXpath(driver, "//a[contains(text(),'Upload logo')]", 30);
            ClickWithWaitXpath(driver, "//button[text()='Upload' and @type='button']", 30);
            Thread.Sleep(5000);

            string imagePath = Environment.CurrentDirectory + "\\black-brand.gif";

            var input_field = driver.FindElement(By.XPath("//input[@type='text' and @disabled='']"));
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            string title = (string)js.ExecuteScript("arguments[0].removeAttribute(\"disabled\");", input_field);
            Thread.Sleep(5000);

            ClickWithWaitXpath(driver, "//label[@for='input-image-upload']", 30);
            Thread.Sleep(5000);


            SendKeys.SendWait(imagePath);
            Thread.Sleep(10000);
            SendKeys.SendWait(@"{Enter}");


            driver.FindElement(By.XPath("//input[@type='text' and @placeholder='Image Name']")).SendKeys("Gartner Logo - " + RandomString(5));
            ClickWithWaitXpath(driver, "//button[contains(@class,'btn-primary') and text()='Upload']", 30);
            Thread.Sleep(10000);

            Thread.Sleep(5000);
            var editSurveyBtn = WaitForSometing(driver, "//*[@id='btnFullPreview']", 30, "xpath");

            ClickWithWaitXpath(driver, "//*[@id='btnFullPreview']", 30);

            IReadOnlyCollection<String> handle = driver.WindowHandles;
            string lastHandle = handle.Last();
            driver.SwitchTo().Window(lastHandle);


            Assert.IsTrue(UIActions.IsElementVisible(driver, "//*[@id='surveyLogoID']/img"));

            IWebElement imageSrcNew = UIActions.FindElementWithXpath(driver, "//*[@id='surveyLogoID']/img");
            string imageSrcNewURL = imageSrc.GetAttribute("src");

            Assert.AreNotEqual(imageSrcURL, imageSrcNewURL, true, "New Logo URL is being populated after edit in preview view");

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_SurveyLevelSetting_EnableQuestionNumber()
        {
            TestContext.WriteLine("Test_SurveyLevelSetting_EnableQuestionNumber");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            AuthorActions.enableQuestionNumber(driver);
            Thread.Sleep(10000);
            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//input[@type='text']";
            string longComment = @"Conveniently implement worldwide processes and process-centric platforms. Synergistically myocardinate business architectures and cross functional deliverables. Competently synthesize reliable information before front-end infrastructures. Synergistically benchmark long-term high-impact vortals before user-centric architectures. Dramatically recaptiualize equity invested innovation vis-a-vis high-quality results.";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);


                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();


        }



        [TestMethod]
        public void PlayerTest_SurveyLevelSetting_DisableQuestionNumber()
        {
            TestContext.WriteLine("Test_SurveyLevelSetting_DisableQuestionNumber");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            AuthorActions.disableQuestionNumber(driver);
            Thread.Sleep(10000);
            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//input[@type='text']";
            string longComment = @"Conveniently implement worldwide processes and process-centric platforms. Synergistically myocardinate business architectures and cross functional deliverables. Competently synthesize reliable information before front-end infrastructures. Synergistically benchmark long-term high-impact vortals before user-centric architectures. Dramatically recaptiualize equity invested innovation vis-a-vis high-quality results.";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);


                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();


        }



        [TestMethod]
        public void PlayerTest_SurveyLevelSetting_SetionNameValidation()
        {
            TestContext.WriteLine("Test_SurveyLevelSetting_SetionNameValidation");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            AuthorActions.EditSectionName(driver, "Player Test Section");
            Thread.Sleep(10000);

            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            string sectionName = "//*[@id='txtSectionNameTitle']";
            string commentBox = "//input[@type='text']";
            string longComment = @"Conveniently implement worldwide processes and process-centric platforms. Synergistically myocardinate business architectures and cross functional deliverables. Competently synthesize reliable information before front-end infrastructures. Synergistically benchmark long-term high-impact vortals before user-centric architectures. Dramatically recaptiualize equity invested innovation vis-a-vis high-quality results.";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);

                _embLnk = UIActions.FindElementWithXpath(driver, sectionName);
                string sectionText = _embLnk.Text.ToString();
                Assert.AreEqual(sectionText, "Player Test Section");
                Thread.Sleep(2000);

                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }


        [TestMethod]
        public void PlayerTest_SurveyLevelSetting_DisableProgressBar()
        {
            TestContext.WriteLine("Test_SurveyLevelSetting_DisableProgressBar");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            AuthorActions.disableProgressBar(driver);
            Thread.Sleep(10000);
            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);


            string commentBox = "//input[@type='text']";
            string longComment = @"Conveniently implement worldwide processes and process-centric platforms. Synergistically myocardinate business architectures and cross functional deliverables. Competently synthesize reliable information before front-end infrastructures. Synergistically benchmark long-term high-impact vortals before user-centric architectures. Dramatically recaptiualize equity invested innovation vis-a-vis high-quality results.";
            string errMsg = "//label[@class='error margin-top-10']";

            bool _result = false;
            TestContext.WriteLine("Checking whether able to add comments to the short comment box ");
            try
            {
                IWebElement _embLnk = UIActions.FindElementWithXpath(driver, commentBox);
                _embLnk.SendKeys(longComment);
                Thread.Sleep(15000);


                ClickWithWaitXpath(driver, FinishButton, 10);
                Thread.Sleep(10000);
                string actualvalue = driver.Url;
                Assert.IsTrue(actualvalue.Contains("Finish"));

                _result = true;


            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();


        }

        [TestMethod]
        public void PlayerTest_DropDownSingleSelect_NAEnabled()
        {
            TestContext.WriteLine("Test_DropDown_NAEnabled");
            TestContext.WriteLine("Data Setup : Create Survey, Distribution");
            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Validate Add DropDown SingleSelect Question

            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AddDropDownQuestionLocal_EnableNA(driver, "Which of the following best describes your level in the organization?", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");
            TestContext.WriteLine("Open Survey URL");
            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, FinishButton, 10);
            TestContext.WriteLine("Checking whether the Finish button element is present at browser session");
            Assert.IsTrue(_embLnk.Displayed);

            ClickWithWaitXpath(driver, selectDropDown, 10);
            Thread.Sleep(5000);

            string elementDropDown = "//span[contains(text(),'Not Applicable')]";
            

            bool _result = true;
            TestContext.WriteLine("Checking whether the NA option is displayed to select response in accordance with settings");
            try
            {
                 ClickWithWaitXpath(driver, elementDropDown, 10);
                _result = true;
            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }

            Thread.Sleep(5000);
            ClickWithWaitXpath(driver, FinishButton, 10);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();

        }
        /// <summary>
        /// TEMPLATES
        /// </summary>

        [TestMethod]
        public void PlayerTest_AddRatingScaleQuestion()
        {
            TestContext.WriteLine("Test_AddRatingScaleQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Rating Scale Question

            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate this Automation framework?");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);
        }


        [TestMethod]
        public void PlayerTest_AddMultiItemRatingScaleQuestion()
        {
            TestContext.WriteLine("Test_AddMultiItemRatingScaleQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add MultiItem Rating Scale Question

            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below automation frameworks", questions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void PlayerTest_AddMultipleChoiceQuestionSingleItem()
        {
            TestContext.WriteLine("Test_AddMultipleChoiceQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice Question Single Item

            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_AddMultipleChoiceMultiItemQuestion()
        {
            TestContext.WriteLine("Test_AddMultipleChoiceMultiItemQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Multiple Choice MultiItem Question

            string[] questions = { "First", "Second", "Third" };
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceMultiItemQuestion(driver, "Chose your favorite Departments", questions, responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }


        [TestMethod]
        public void PlayerTest_AddDropDownSingleSelectQuestion()
        {
            TestContext.WriteLine("Test_ValidateAddDropDownSingleSelectQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Validate Add DropDown SingleSelect Question

            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AuthorActions.AddDropDownQuestion(driver, "Which of the following best describes your level in the organization?", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }


        [TestMethod]
        public void PlayerTest_AddCommentQuestion()
        {
            TestContext.WriteLine("Test_AddCommentQuestion");


            //Initialize the Webdriver and login

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                
            }

            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Comment Question

            Thread.Sleep(3000);
            AuthorActions.AddCommentQuestion(driver, "Please take this time to let us know anything about your benefits that you have not already had a chance to express.");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_AddLongCommentQuestion()
        {
            TestContext.WriteLine("Test_AddLongCommentQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Long Comment Question

            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }


        [TestMethod]
        public void PlayerTest_AddShortCommentQuestion()
        {
            TestContext.WriteLine("Test_AddShortCommentQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Short Comment Question

            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_AddFixedFormatQuestion()
        {
            TestContext.WriteLine("Test_AddFixedFormatQuestion");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Fixed Format Question

            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "Number");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);

            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }

        [TestMethod]
        public void PlayerTest_AddFixedFormatQuestion_WebsiteURL()
        {
            TestContext.WriteLine("Test_AddFixedFormatQuestion_WebsiteURL");

            //Initialize the Webdriver and login

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


            //Navigate to Author Page

            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(10000);

            //Create a Form

            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Thread.Sleep(10000);

            //  Add Fixed Format Question_WebsiteURL

            var QuestionText = "FixedFormatWebSiteURLQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_website_url");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(5000);

            // Publish Form

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);

            // Distribute Form
            string DistributionNameStr = "DemofilterAnalyzecheck" + DateTime.Now;

            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = DistributionNameStr,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddMinutes(1000)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();

            Thread.Sleep(5000);
            //Thread.Sleep(1000 * 60 * 6);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//span[@class='survey-url']");

            driver.Navigate().GoToUrl(surveyLink1.Text.ToString());
            Thread.Sleep(10000);
            TestContext.WriteLine("Cleaning up the test and exiting");
            CleanUp();
        }
    }
}