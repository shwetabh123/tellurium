﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Data;
using Common;

namespace Driver
{
    [TestClass]
    public class LocalTestClass
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string SPColor;
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";
        //public static string TrendCategoryID = "38768";
        //public static string TrendItemID = "83292";
        /// <summary>
        /// QA Parameters
        public static string TrendCategoryID = "17318";
        public static string TrendItemID = "46720";
        public static string TrendNRQuestionID = "46716";
        public static string TrendNRScaleOptionID = "224850";
        /// </summary>



        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
            SPColor = context.Properties["SPColor"].ToString();
            DataPopulationForReportDataValidation.PopulateData(DriverAndLogin.Url, DBConnectionParameters);
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver SetDriverAndNavigateToAccountDefaults()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            else if (UIActions.IsElementVisible(driver, "#account-defaults-title"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            return driver;
        }

        public static bool CompareInsightAndDrillDownReportResults(string DataCalculation, DataSet Insight, DataSet DrillDown, string CatOrItem)
        {

            try
            {
                bool status = true;
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                int tno = (CatOrItem.ToLower() == "category") ? 1 : 0;

                string DDColumnName = (CatOrItem.ToLower() == "category") ? "CategoryID" : "SurveyFormItemID";

                string InsightComp1 = (DataCalculation.ToLower() == "percentage") ? "Comp_One_FavPerc_Diff" : "Comp_One_Mean_Diff";
                string InsightComp2 = (DataCalculation.ToLower() == "percentage") ? "Comp_Two_FavPerc_Diff" : "Comp_Two_Mean_Diff";
                string InsightComp3 = (DataCalculation.ToLower() == "percentage") ? "Comp_Three_FavPerc_Diff" : "Comp_Three_Mean_Diff";

                string DrillDownComp1 = (DataCalculation.ToLower() == "percentage") ? "Comp1DiffFavPercent" : "Comp1DiffAverageScore";
                string DrillDownComp2 = (DataCalculation.ToLower() == "percentage") ? "Comp2DiffFavPercent" : "Comp2DiffAverageScore";
                string DrillDownComp3 = (DataCalculation.ToLower() == "percentage") ? "Comp3DiffFavPercent" : "Comp3DiffAverageScore";

                if(DataCalculation.ToLower() == "percentage")
                {
                    foreach(DataRow inrow in Insight.Tables[tno].Rows)
                    {
                        foreach(DataRow ddrow in DrillDown.Tables[0].Rows)
                        {
                            if(CommonActions.IfNullThenZero(inrow["ObjectId"]) == CommonActions.IfNullThenZero(ddrow[DDColumnName]))
                            {
                                if(
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["FavPerc"])),0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["FavPercent"])),0) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["NeutralPerc"])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["NeutralPercent"])),0) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["UnFavPerc"])), 0) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["UnFavPercent"])),0)
                                    )
                                {
                                    status = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Target Fav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["FavPerc"])), 0)
                                            + "\tDrillDown Target Fav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["FavPercent"])), 0), w);

                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Target Neutral : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["NeutralPerc"])), 0)
                                            + "\tDrillDown Target Neutral : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["NeutralPercent"])), 0), w);

                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Target UnFav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["UnFavPerc"])), 0)
                                            + "\tDrillDown Target UnFav : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["UnFavPercent"])), 0), w);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataRow inrow in Insight.Tables[tno].Rows)
                    {
                        foreach (DataRow ddrow in DrillDown.Tables[0].Rows)
                        {
                            if (CommonActions.IfNullThenZero(inrow["ObjectId"]) == CommonActions.IfNullThenZero(ddrow[DDColumnName]))
                            {
                                if (
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["MeanScore"])),2) != Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["AverageScore"])),2) 
                                    )
                                {
                                    status = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Target Mean: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow["MeanScore"])), 0)
                                            + "\tDrillDown Target Mean: " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow["AverageScore"])), 0), w);
                                    }
                                }
                            }
                        }
                    }
                }

                /*
                 * Comparison calculation starts here
                 */

                if (DataCalculation.ToLower() == "percentage")
                {
                    foreach (DataRow inrow in Insight.Tables[tno].Rows)
                    {
                        foreach (DataRow ddrow in DrillDown.Tables[0].Rows)
                        {
                            if (CommonActions.IfNullThenZero(inrow["ObjectId"]) == CommonActions.IfNullThenZero(ddrow[DDColumnName]))
                            {
                                if (
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp1])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp1])), 0)) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp2])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp2])), 0)) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp3])), 0) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp3])), 0))
                                    )
                                {
                                    status = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Comp1 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp1])), 0) 
                                            + "\tDrillDown Comp1 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp1])), 0), w);

                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Comp2 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp2])), 0)
                                            + "\tDrillDown Comp2 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp2])), 0), w);

                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Comp3 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp3])), 0)
                                            + "\tDrillDown Comp3 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp3])), 0), w);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataRow inrow in Insight.Tables[tno].Rows)
                    {
                        foreach (DataRow ddrow in DrillDown.Tables[0].Rows)
                        {
                            if (CommonActions.IfNullThenZero(inrow["ObjectId"]) == CommonActions.IfNullThenZero(ddrow[DDColumnName]))
                            {
                                if (
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp1])),2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp1])), 2)) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp2])),2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp2])), 2)) ||
                                    Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp3])),2) != (Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp3])), 2))
                                    )
                                {
                                    status = false;
                                    using (StreamWriter w = File.AppendText(LogFileName))
                                    {
                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Comp1 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp1])), 2)
                                            + "\tDrillDown Comp1 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp1])), 2), w);

                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Comp2 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp2])), 2)
                                           + "\tDrillDown Comp2 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp2])), 2), w);

                                        ReportDataValidation.Log("\t" + DDColumnName + " = " + inrow["ObjectId"] + "\tInsight Comp3 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(inrow[InsightComp3])), 2)
                                           + "\tDrillDown Comp3 : " + Math.Round(Decimal.Parse(CommonActions.IfNullThenZero(ddrow[DrillDownComp3])), 2), w);
                                    }
                                }
                            }
                        }
                    }
                }



                return status;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }


        }

        [TestMethod]
        public void ValidateTestSPForInsightsReportForCategory_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForInsightsReportForCategory_Percentage", w);
                }
                int ScenarioCount = 0;
                string ReportDisplay = "Category";
                string DataCalculation = "Percentage";
                DataSet ReportFilters;
                bool spresult = true;
                bool AssertStatus = true;
                string InsightReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                string DrillDownReportSPName = "usp_drilldownreport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];

                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, InsightReportSPName + "_" + SPColor + " @Debug=1,");
                    DataSet DDResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, DrillDownReportSPName + "_" + SPColor);
                    spresult = CompareInsightAndDrillDownReportResults(DataCalculation, ResultSet, DDResultSet, ReportDisplay);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;

                    if (spresult == false)
                    {
                        AssertStatus = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("\tFAILED", w);
                            ReportDataValidation.Log("\t" + InsightReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                            ReportDataValidation.Log("\t" + DrillDownReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                        }
                    }

                    loopcnt++;
                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]
        public void ValidateTestSPForInsightsReportForCategory_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForInsightsReportForCategory_Average", w);
                }
                int ScenarioCount = 0;
                string ReportDisplay = "Category";
                string DataCalculation = "Average";
                DataSet ReportFilters;
                bool spresult = true;
                bool AssertStatus = true;
                string InsightReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                string DrillDownReportSPName = "usp_drilldownreport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];

                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, InsightReportSPName + "_" + SPColor + " @Debug=1,");
                    DataSet DDResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, DrillDownReportSPName + "_" + SPColor);
                    spresult = CompareInsightAndDrillDownReportResults(DataCalculation, ResultSet, DDResultSet, ReportDisplay);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (spresult == false)
                    {
                        AssertStatus = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("\tFAILED", w);
                            ReportDataValidation.Log("\t" + InsightReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                            ReportDataValidation.Log("\t" + DrillDownReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                        }
                    }

                    loopcnt++;
                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateTestSPForInsightsReportForQuestion_Percentage()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForInsightsReportForQuestion_Percentage", w);
                }

                int ScenarioCount = 0;
                string ReportDisplay = "Question";
                string DataCalculation = "Percentage";
                DataSet ReportFilters;
                bool spresult = true;
                bool AssertStatus = true;
                string InsightReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                string DrillDownReportSPName = "usp_drilldownreport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];

                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, InsightReportSPName + "_" + SPColor + " @Debug=1,");
                    DataSet DDResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, DrillDownReportSPName + "_" + SPColor);
                    spresult = CompareInsightAndDrillDownReportResults(DataCalculation, ResultSet, DDResultSet, ReportDisplay);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (spresult == false)
                    {
                        AssertStatus = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("\tFAILED", w);
                            ReportDataValidation.Log("\t" + InsightReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                            ReportDataValidation.Log("\t" + DrillDownReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                        }
                    }
                    loopcnt++;

                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }


        [TestMethod]
        public void ValidateTestSPForInsightsReportForQuestion_Average()
        {
            try
            {
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("TestMethod : ValidateTestSPForInsightsReportForQuestion_Average", w);
                }
                int ScenarioCount = 0;
                string ReportDisplay = "Question";
                string DataCalculation = "Average";
                DataSet ReportFilters;
                bool spresult = true;
                bool AssertStatus = true;
                string InsightReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
                string DrillDownReportSPName = "usp_drilldownreport";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, ReportDisplay, DataCalculation);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];

                int loopcnt = 1;
                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    using (StreamWriter w = File.AppendText(LogFileName))
                    {
                        ReportDataValidation.Log("Scenario No : " + loopcnt + ";\tRemaining Scenarios: " + (dscount.Tables[1].Rows.Count - loopcnt), w);
                    }

                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, InsightReportSPName + "_" + SPColor + " @Debug=1,");
                    DataSet DDResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, DrillDownReportSPName + "_" + SPColor);
                    spresult = CompareInsightAndDrillDownReportResults(DataCalculation, ResultSet, DDResultSet, ReportDisplay);
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (spresult == false)
                    {
                        AssertStatus = false;
                        using (StreamWriter w = File.AppendText(LogFileName))
                        {
                            ReportDataValidation.Log("\tFAILED", w);
                            ReportDataValidation.Log("\t" + InsightReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                            ReportDataValidation.Log("\t" + DrillDownReportSPName + "_" + SPColor + " " + row[0].ToString() + ", " + row[1].ToString() + ", " + row[2].ToString(), w);
                        }
                    }

                    loopcnt++;
                }
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.IsTrue(AssertStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                    ReportDataValidation.Log("----------------------------------------------------------------------------------------------------", w);
                }
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateTestSPForDrillDown_Category_Percentage()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Percentage";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "category", "percentage");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "category");

                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    Console.WriteLine(UseranalyzeFilterID);
                    //spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);

                }


            }

        }

        [TestMethod]

        public void ValidateTestSPForDrillDown_Category_Average()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Average";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "category", "average");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "category");
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }


            }



        }
        [TestMethod]

        public void ValidateTestSPForDrillDown_Question_Percentage()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "Percentage";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "Item", "percentage");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "Item");
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }




            }


        }


        [TestMethod]

        public void ValidateTestSPForDrillDown_Question_Average()
        {

            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "Average";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters, "item", "average");
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "Item");
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    ReportDataValidation.WriteTestExecutionResult(DataCalculation, ReportSPName, UseranalyzeFilterID, spresult, DBConnectionParameters);
                }

            }


        }



    }
}
