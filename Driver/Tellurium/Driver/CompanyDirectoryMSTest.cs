﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

using Common;

namespace Driver
{
    [TestClass]
    public class CompanyDirectoryMSTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToCompanyDirectoy()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
                CompanyDirectoryActions.NavigateToCompanyDirectory(driver);
            }
            else if (UIActions.IsElementVisible(driver, "#directory-participants-title"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                CompanyDirectoryActions.NavigateToCompanyDirectory(driver);
            }
            return driver;
        }

        [TestMethod]
        public void ValidateCompanyDirectoryAddParticipants()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToCompanyDirectoy();
                Thread.Sleep(5000);
                CompanyDirectoryActions.UploadCompanyDirectoryParticipants(driver, "CompanyDirectoryParticipantImportTemplate.xlsx", "MM/DD/YYYY");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
                Assert.IsTrue(messageArea.Text.Trim().Contains("File uploaded successfully and will be processed soon."));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }

        }
        [TestMethod]

        public void Adddemo()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            CompanyDirectoryActions.NavigatetoSettings(driver);
            Thread.Sleep(8000);
            CompanyDirectoryActions.OpenDemographicstab(driver);
            Thread.Sleep(9000);
            var DemoName = "TestDemo1";
            CompanyDirectoryActions.AddDemographic(driver, DemoName, "Date");
        }

        [TestMethod]

        public void Editemographic()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            CompanyDirectoryActions.NavigatetoSettings(driver);
            Thread.Sleep(8000);
            CompanyDirectoryActions.OpenDemographicstab(driver);
            Thread.Sleep(4000);
            var Oldname = "RFE5demotest";
            var DemoName = "RFE5demotest1";
            CompanyDirectoryActions.EditDemographic(driver, Oldname, DemoName, null);

        }
        [TestMethod]
        public void Editemographicall()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            CompanyDirectoryActions.NavigatetoSettings(driver);
            Thread.Sleep(8000);
            CompanyDirectoryActions.OpenDemographicstab(driver);
            Thread.Sleep(4000);
            var Oldname = "RFE5demotest1";
            var DemoName = "RFE5demotest21";
            var datatype = "Email";
            CompanyDirectoryActions.EditDemographic(driver, Oldname, DemoName, datatype);
        }
        [TestMethod]
        public void Editdatatype()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            CompanyDirectoryActions.NavigatetoSettings(driver);
            Thread.Sleep(8000);
            CompanyDirectoryActions.OpenDemographicstab(driver);
            Thread.Sleep(4000);
            var Oldname = "RFE5demotest";
            var datatype = "Date";
            CompanyDirectoryActions.EditDemographic(driver, Oldname, null, datatype);
        }
        [TestMethod]
        public void Deletedemo()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            CompanyDirectoryActions.NavigatetoSettings(driver);
            Thread.Sleep(8000);
            CompanyDirectoryActions.OpenDemographicstab(driver);
            Thread.Sleep(4000);
            var Demoname = "TestDemo1";

            CompanyDirectoryActions.DeleteDemographic(driver, Demoname);
        }
    }
}

        
  

