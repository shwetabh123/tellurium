﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace Driver
{
    public class FileActionAttribute: Attribute, ITestAction
    {
        public static string FileName;
        public void BeforeTest(ITest test)
        {
        }

        public void AfterTest(ITest test)
        {
            //CommonActions.WriteTestResult(FileName,test.MethodName, TestContext.CurrentContext.Result.Outcome.Status.ToString());
        }

        public ActionTargets Targets
        {
            get { return ActionTargets.Test | ActionTargets.Suite; }
        }
    }
}
