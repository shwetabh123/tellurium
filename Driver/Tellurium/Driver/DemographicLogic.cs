﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Windows.Forms;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;


namespace Driver
{

    [TestClass]
    public class DemographicLogic
    {

        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        private static string FormName;
        public static string FileName;
        public static string distributionName = "Thunderbird";
        public static int runCheck = 0;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }


        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static void OpenOrCreateForm(RemoteWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }


        public static RemoteWebDriver SetDriverAndNavigateToForm(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                OpenOrCreateForm(driver);
            }
            else if (driver.Url.Contains("ManageForms"))
                OpenOrCreateForm(driver);
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);
            }
            else
            {
                OpenOrCreateForm(driver);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToHome(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            else
                NavigateToHome(driver);
            return driver;
        }

        public static void NavigateToHome(RemoteWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Home").Click();
        }

        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        }


        [TestMethod]
        public void ValidateCreateFormFromHome()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(10000);
            FormName = "SeleniumDemographicLogic" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Assert.AreEqual(true, formBuilder != null);
        }


        [TestMethod]
        public void ValidateAddSingleRatingScaleQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(1000);
            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            screenshot.SaveAsFile("test.png");
            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate this Automation framework?");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }



        [TestMethod]
        public void ValidateAddMultipleRatingScaleQuestionWithScaleRangeAndHeaders()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(5000);
            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            string[] scaleHeaders = { "One", "Two", "Three", "Four", "Five", "Six" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below new automation frameworks", questions, "1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [TestMethod]

        public void ValidateAddDemographicpagelogic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(1000);
            AuthorActions.DragAndDropLogic(driver, 1);
            Thread.Sleep(5000);
            var logicbuilder = driver.FindElement(By.Id("Add-Logic-modal"));
            if (logicbuilder != null)
            {
                Thread.Sleep(1000);
                AuthorActions.AddLogic(driver, "Hide", "2", new string[] {"Page 2"}, "Any of", new LogicCaseDto[]
                    {
                      new LogicCaseDto
                    {
                     SourceType = "2",
                        SourceItem =  "Country",
                        SourceCondition = "Equal To",
                        SourceLogic = "Any of",
                        SourceResponses = new string[] { "Russia" }
                       }
                    });
                Thread.Sleep(10000);

                logicbuilder = UIActions.GetElement(driver, "#Add-Logic-modal");
                Assert.IsFalse(logicbuilder != null && logicbuilder.Displayed);

            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod]

        public void ValidateEditpageName()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(3000);
            var ISLogic = UIActions.GetAttrForElementByCss(driver, ".ceb-icon_logic-circle", "title");
            if (ISLogic == "Has Logic")
            {
                UIActions.clickwithXapth(driver, "(//*[@class='panel panel-default page-list ui-droppable'])[1]");
                UIActions.clickwithXapth(driver, "(//*[@class='shl-icon_edit action-page-edit'])[2]");
                var PageName = "Page with demographic Logic";
                IWebElement element = UIActions.GetElementswithXpath(driver, "//input[@data-bind-type='formvalue']", 45);
                Actions action = new Actions(driver);
                action.MoveToElement(element).Click().Perform();
                Thread.Sleep(2000);
                UIActions.clickwithXapth(driver, "//input[@data-bind-type='formvalue']");
                element.Clear();
                element.SendKeys(PageName);
            }
            else
            {
                Assert.Fail();
            }


        }


        [TestMethod]

        public void ValidateAddDropDownSingleSelectQuestion()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            AuthorActions.AddPage(driver);
            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AuthorActions.AddDropDownQuestion(driver, "Which of the following best describes your level in the organization?", responseOptions);
            //Thread.Sleep(1000);

            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }


        [TestMethod]

        public void ValidateAddDemographicResponselogic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(1000);
            AuthorActions.DragAndDropLogic(driver, 1);
            Thread.Sleep(5000);
            var logicbuilder = driver.FindElement(By.Id("Add-Logic-modal"));
            if (logicbuilder != null)
            {
                Thread.Sleep(1000);
                AuthorActions.AddResponseLogic(driver, "Hide", "4", "Q2.Which of the following best describes your level in the organization?", new string[] { "Director", "Individual Contributor/Employee" }, "Any of", new LogicCaseDto[]
                    {
                      new LogicCaseDto
                    {
                     SourceType = "2",
                        SourceItem =  "Country",
                        SourceCondition = "Equal To",
                        SourceLogic = "Any of",
                        SourceResponses = new string[] { "Russia" }
                       }
                    });
                Thread.Sleep(10000);

                logicbuilder = UIActions.GetElement(driver, "#Add-Logic-modal");
                Assert.IsFalse(logicbuilder != null && logicbuilder.Displayed);

            }
            else
            {
                Assert.Fail();
            }
        }

        // validate page demographic logic with demo values as Russia(Positive case)
        [TestMethod]
        public void ValidateDemographicPageLogicForPositiveCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(5000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 5, "Russia");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var PageTitle = UIActions.GetValueByElement(driver, ".page-title");
            Console.WriteLine(PageTitle);
            Assert.AreNotEqual(PageTitle, "Automation Demographic logic | Demographic Logic Page");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            Thread.Sleep(10000);
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);

        }
        // validate page demographic logic with demo values as china(Negative case)

        [TestMethod]
        public void ValidateDemographicPageLogicForNegativeCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 5, "China");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var PageTitle = UIActions.GetValueByElement(driver, ".page-title");
            Console.WriteLine(PageTitle);
            Assert.AreEqual(PageTitle, "Automation Demographic logic | Demographic Logic Page");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            Thread.Sleep(5000);
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);

        }

        // validate Response based demographic logic with demo values as Russia(Positive Case)
        [TestMethod]
        public void ValidateResponseBasedDemographicLogicHidePositiveCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 5, "Russia");
            Thread.Sleep(5000);
            DistributeActions.EditParticipant(driver, 7, "Male");
            Thread.Sleep(5000);
            DistributeActions.EditParticipant(driver, 9, "MBA");
            Thread.Sleep(5000);
            DistributeActions.EditParticipant(driver, 11, "Manager");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            IWebElement Dropdown = driver.FindElement(By.XPath("(//*[@class='multiselect dropdown-toggle form-control ceb-dropdown'])[2]"));
            Dropdown.Click();
            IList<IWebElement> options = Dropdown.FindElements(By.XPath("//a[@class='dropdown-item']/label"));

            var Alloptions = options.Count;
            Console.WriteLine("count" + Alloptions);
            foreach (IWebElement option in options)
            {
                Console.WriteLine("Options:"+option.Text);

                if (option.Text.Equals("Manager/Supervisor"))
                   {

                    Console.WriteLine("The Repsonse is hide based on the question based logic");
                    }
                else if(option.Text.Equals("Individual Contributor/Employee"))
                   {
                    Assert.Fail();
                   }
                else if (option.Text.Equals("Director"))
                  {
                    Assert.Fail();
                  }


            }
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            Thread.Sleep(5000);
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);

          }
  

        // validate Response based demographic logic with demo values as China(Negative Case)
        [TestMethod]
        public void ValidateResponseBasedDemographicLogicHideNegativeCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 5, "China");
            Thread.Sleep(5000);
            DistributeActions.EditParticipant(driver, 9, "MBA");
            Thread.Sleep(3000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            IWebElement Dropdown = driver.FindElement(By.XPath("(//*[@class='multiselect dropdown-toggle form-control ceb-dropdown'])[2]"));
            Dropdown.Click();
            IList<IWebElement> options = Dropdown.FindElements(By.XPath("//a[@class='dropdown-item']/label"));

            var Alloptions = options.Count;
            Console.WriteLine("count" + Alloptions);
            foreach (IWebElement option in options)
            {
                Console.WriteLine("Options:" + option.Text);

                if (option.Text.Equals("Manager/Supervisor"))
                {

                    Console.WriteLine("The Repsonse is shown based on the Response based logic");
                }
                else if (option.Text.Equals("Individual Contributor/Employee"))
                {
                    Assert.AreEqual(option.Text, "Individual Contributor/Employee");
                }
                else if (option.Text.Equals("Director"))
                {
                    Assert.AreEqual(option.Text, "Director");
                }


            }
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            Thread.Sleep(5000);
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);

        }
        // validate Response based demographic logic with demo values as Male(Gender)

        [TestMethod]
        public void ValidateResponseBasedDemographicLogicShowPositiveCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 7, "Male");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            IWebElement ScaleRange3 = driver.FindElement(By.XPath("(//*[@data-bind='showResponseText'])[3]"));
            Assert.AreEqual(ScaleRange3.Text, "3");
            IWebElement ScaleRange4 = driver.FindElement(By.XPath("(//*[@data-bind='showResponseText'])[4]"));
            Assert.AreEqual(ScaleRange4.Text, "4");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);


        }
        // // validate Response based demographic logic with demo values as Female(Gender)

        [TestMethod]
        public void ValidateResponseBasedDemographicLogicShowNegativeCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 7, "Female");
            Thread.Sleep(10000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            IWebElement ScaleRange3 = driver.FindElement(By.XPath("(//*[@data-bind='showResponseText'])[3]"));
            Assert.AreNotEqual(ScaleRange3.Text, "3");
            IWebElement ScaleRange4 = driver.FindElement(By.XPath("(//*[@data-bind='showResponseText'])[3]"));
            Assert.AreEqual(ScaleRange4.Text, "5");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);


        }

        //validate Page based demographic logic with demo values as MBA(Degree)

        [TestMethod]
        public void ValidatePageBasedDemographicLogicShowPositiveCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 9, "MBA");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var PageTitle = UIActions.GetValueByElement(driver, ".page-title");
            Console.WriteLine(PageTitle);
            Assert.AreEqual(PageTitle, "Automation Demographic logic | Demographic Logic Page 3");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);


        }

        //validate Page based demographic logic with demo values as BBA(Degree)
        [TestMethod]
        public void ValidatePageBasedDemographicLogicShowNegativeCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 9, "BBA");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var PageTitle = UIActions.GetValueByElement(driver, ".page-title");
            Console.WriteLine(PageTitle);
            Assert.AreNotEqual(PageTitle, "Automation Demographic logic | Demographic Logic Page 3");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);


        }
        //validate Page based demographic logic with demo values as Tester(Domian)

        [TestMethod]
        public void ValidatePageBasedDemographiclogicwithNotEqualConditionPositiveFlow()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 11, "Tester");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var PageTitle = UIActions.GetValueByElement(driver, ".page-title");
            Console.WriteLine(PageTitle);
            Assert.AreNotEqual(PageTitle, "Automation Demographic logic | Demographic Logic Page 4");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);
        }

        //validate Page based demographic logic with demo values as Blank for Not Equal Condition(Domian)

        [TestMethod]
        public void ValidatePageBasedDemographiclogicwithNotEqualBlankValue()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 11, "");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(2000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var PageTitle = UIActions.GetValueByElement(driver, ".page-title");
            Console.WriteLine(PageTitle);
            Assert.AreNotEqual(PageTitle, "Automation Demographic logic | Demographic Logic Page 4");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);

        }


        //validate Page based demographic logic with demo values as Manager then it should Show(Domian)
        
        [TestMethod]
        public void ValidatePageBasedDemographiclogicwithNotEqualConditionNegativeFlow()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 11, "Manager");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var PageTitle = UIActions.GetValueByElement(driver, ".page-title");
            Console.WriteLine(PageTitle);
            Assert.AreEqual(PageTitle, "Automation Demographic logic | Demographic Logic page 4");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);
        }


        //validate Response based demographic logic with demo values as blank with Not Equal to condition(Domian)

        [TestMethod]
        public void ValidateResponsebasedDemologicforNotEqualtoBlank()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 11, "");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            IWebElement ScaleRange3 = driver.FindElement(By.XPath("(//*[@data-bind='showResponseText'])[2]"));
            Assert.AreNotEqual(ScaleRange3.Text, "2");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);
        }
        //validate Response based demographic logic with demo values with Not Equal positive condition(Domian)

        [TestMethod]
        public void ValidateResponsebasedDemologicforHideNotequalToPositiveFlow()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 11, "Tester");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            IWebElement ScaleRange3 = driver.FindElement(By.XPath("(//*[@data-bind='showResponseText'])[2]"));
            Assert.AreNotEqual(ScaleRange3.Text, "2");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);
        }


        //validate Response based demographic logic with demo values with Not Equal Negative condition(Domian)
        [TestMethod]
        public void ValidateResponsebasedDemologicforHideNotequalToNegativeFlow()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver, 11, "Tester");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            IWebElement ScaleRange3 = driver.FindElement(By.XPath("(//*[@data-bind='showResponseText'])[2]"));
            Assert.AreEqual(ScaleRange3.Text, "4");
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);
        }


        //validate Response based demographic logic with blank(Equalto) given in logic builder

        [TestMethod]
        public void ValidateResponsebasedDemologicforMoreThanOneCase()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            DistributeActions.EditParticipant(driver,7, "Male");
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-copy-participant-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            IList<IWebElement> ScaleOption = driver.FindElements(By.XPath("//*[@class='list-group-item simple-test-items']"));
            foreach (IWebElement option in ScaleOption)
            {
                if (option.Text.Equals("IT"))
                {
                    Assert.AreEqual(option.Text, "IT");
                }
                else if (option.Text.Equals("Finance"))
                {
                    Assert.Fail();
                }
                else if (option.Text.Equals("Sales"))
                {
                    Assert.Fail();
                }
            }
          

            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            UIActions.Click(driver, ".removeParticipantButton");
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnConfirmYes");
            Thread.Sleep(1000);
        }

     






    
        /*
         * 
         * Author   :   Abhishek Nath
         * 
         * 
         */

        [TestMethod]
        public void Validate_DemoLogic_Question_Show_AnyOf()
        {
            try
            {
                //string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 1.txt";
                string emailaddress = "mp_comm_africa_4@sdcomp.com";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;

                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Validate_DemoLogic_Question_Show_AllOf()
        {
            try
            {
                //string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 2.txt";
                string emailaddress = "mp_comm_africa_14@sdcomp.com";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;

                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Validate_DemoLogic_Question_Hide_AnyOf()
        {
            try
            {
                //string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 3.txt";
                string emailaddress = "mp_comm_africa_16@sdcomp.com";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;
                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Validate_DemoLogic_Question_Hide_AllOf()
        {
            try
            {
                string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 4.txt";
                string emailaddress = "andre@ceb.comm";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;

                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Validate_DemoLogic_Section_Show_AllOf()
        {
            try
            {
                string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 5.txt";
                string emailaddress = "cleveland@ceb.comm";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;

                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Validate_DemoLogic_Section_Show_AnyOf()
        {
            try
            {
                string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 6.txt";
                string emailaddress = "mp_comm_africa_199@sdcomp.com";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;

                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Validate_DemoLogic_Section_Hide_AnyOf()
        {
            try
            {
                string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 7.txt";
                string emailaddress = "geyer@ceb.comm";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;

                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Validate_DemoLogic_Section_Hide_AllOf()
        {
            try
            {
                string path = Directory.GetCurrentDirectory();
                string filepath = @"C:\Tellurium\DataSet\Question Set 8.txt";
                string emailaddress = "baird@ceb.comm";
                string[] requiredQuestions = File.ReadAllLines(filepath);

                bool isPresent = true;

                RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
                if (driver != null && driver.Title.Contains("Distribute Survey - Pulse"))
                {
                    DistributeActions.CopyUniqueLinkFromEditParticipantPage(driver, emailaddress);
                }
                else
                {
                    driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                    DistributeActions.SearchAndOpenDistribution(driver, distributionName);
                    Thread.Sleep(10000);
                    DistributeActions.CopyUniqueLinkBySearchParticipant(driver, emailaddress);
                }
                string participantURL = Clipboard.GetText();
                UIActions.OpenNewTab(driver, participantURL);

                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                while (nextButton.Displayed)
                {
                    string[] pageQuestions = PlayerActions.GetAllQuestionsFromPage(driver, ".question-text");
                    foreach (string question in pageQuestions)
                    {
                        if (!requiredQuestions.Contains(question))
                        {
                            isPresent = false;
                            break;
                        }
                    }
                    UIActions.Click(driver, "#btnNext");
                    Thread.Sleep(5000);
                }
                UIActions.Click(driver, "#btnFinish");
                Thread.Sleep(5000);
                Assert.IsTrue(isPresent && PlayerActions.GetThankYouPageContent(driver, ".content-inner"));
                driver.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }









        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
              // driver.Close();
              // driver.Dispose();
            }
        }


    }
}
