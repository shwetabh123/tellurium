﻿using System;
using System.Collections.Generic;

using System.Linq;

using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using OpenQA.Selenium.Interactions;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Forms;

namespace Common
{
    [TestClass]
    public class ThankUPageMSTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public ThankYouPageDto ThankYouPage = new ThankYouPageDto();
        private static string FormName;
        public static string FileName;
        static int _currentPage;
        string valValue = null;
        List<string> xmlList = new List<string>();
        public static string formNameToPublish;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static void OpenOrCreateForm(RemoteWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToForm(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                OpenOrCreateForm(driver);
            }
            else if (driver.Url.Contains("ManageForms"))
                OpenOrCreateForm(driver);
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);
            }
            else
            {
                OpenOrCreateForm(driver);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToManageForms(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
            }
            else
            {
                AuthorActions.NavigateToAuthor(driver);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToHome(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            else
                NavigateToHome(driver);
            return driver;
        }
        public static void NavigateToHome(RemoteWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Home").Click();
        }

        public static void NavigateToSupport(IWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Support").Click();
        }

        public static void NavigateToAccount(IWebDriver driver)
        {
            NavigateToSupport(driver);
            IReadOnlyCollection<IWebElement> dd = UIActions.FindElementsWithXpath(driver, "//li[@role='presentation']/a");
            dd.FirstOrDefault(d => d.Text == "Accounts").Click();
        }
        public static void NavigateToAccountAndSearch(IWebDriver driver, string _accName)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            NavigateToSupport(driver);
            IReadOnlyCollection<IWebElement> dd = UIActions.FindElementsWithXpath(driver, "//li[@role='presentation']/a");
            dd.FirstOrDefault(d => d.Text == "Accounts").Click();
            UIActions.GetElement(driver, "#txtSearch").Clear();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@type='button']")));
            UIActions.GetElement(driver, "#txtSearch").SendKeys("ModalAcc");
            UIActions.GetElement(driver, "#btnSearch").Click();
            IReadOnlyCollection<IWebElement> _accNames = UIActions.FindElementsWithXpath(driver, "//input[@type='button']");
            wait.Until(d => { return d.FindElement(By.XPath("//input[@type='button']")); });
            _accNames.FirstOrDefault(d => d.GetAttribute("value") == "Login").Click();
        }

        public static void CreateNewAccThruMigrationTool(IWebDriver driver)
        {
            NavigateToSupport(driver);
            string applicationPath = CommonActions.GetApplicationPath();
            string _filePath = applicationPath + @"DataSet\DVMigrationTemplate (7).xlsx";
            IReadOnlyCollection<IWebElement> dd = UIActions.FindElementsWithXpath(driver, "//li[@role='presentation']/a");
            dd.FirstOrDefault(d => d.Text == "Migrations").Click();
            UIActions.GetElement(driver, "#btnMigrationSelect").SendKeys(_filePath);
            UIActions.GetElement(driver, "#btnMigrationUpload").Click();
        }
        public static bool VerifyCustomThankUPageInPreview(IWebDriver driver, string surveyName, string thankyoupageContent = null)
        {
            bool report = false;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8000));
            wait.Until(d =>
            {
                return d.FindElement(By.XPath("//span[@data-bind='pageCount'][@class='font-18']"));
            });
            if (driver.FindElement(By.XPath("//span[@data-bind='pageCount'][@class='font-18']")).Displayed.Equals(false))
            {
                CommonActions.ScrollDown(driver);
                UIActions.GetElement(driver, "#btnFinish").Click();
                Thread.Sleep(3000);
                try
                {
                    var _thankyoupagecontent = UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']").Text;
                    report = (_thankyoupagecontent.Equals(thankyoupageContent) ? true : false);

                }
                catch (Exception)
                {
                    if (driver.Title == "PreviewFeedback - Pulse")
                    {
                        Console.WriteLine("Redirected to default Thank you page. Hence failing the case");
                        report = false;
                    }
                    else
                    {
                        Console.WriteLine("Error Message");
                        report = false;
                    }

                }

            }
            else
            {
                int _pageCount = Convert.ToInt32(UIActions.GetElementwithXpath(driver, "//span[@data-bind='pageCount'][@class='font-18']", 100).Text);
                int _currentPage = Convert.ToInt32(UIActions.GetElementwithXpath(driver, "//span[@data-bind='currentPage'][@class='font-18']", 60).Text);
                while (_currentPage <= _pageCount)
                {

                    if (driver.Title.Equals(surveyName) && (UIActions.GetElement(driver, "#btnFinish") != null) && (UIActions.GetElement(driver, "#btnFinish").Displayed))
                    {
                        wait.Until<IWebElement>(d =>
                        {
                            return d.FindElement(By.Id("btnFinish"));
                        });
                        Actions ac = new Actions(driver);
                        ac.MoveToElement(UIActions.GetElement(driver, "#btnFinish")).Build().Perform();
                        CommonActions.ScrollDown(driver);
                        UIActions.GetElement(driver, "#btnFinish").Click();
                        wait.Until<IWebElement>(d =>
                        {
                            return (UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']"));
                        });
                        Thread.Sleep(3000);
                        try
                        {
                            var _thankyoupagecontent = UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']").Text;
                            report = (_thankyoupagecontent.Equals(thankyoupageContent) ? true : false);

                        }
                        catch (Exception)
                        {
                            if (driver.Title == "PreviewFeedback - Pulse")
                            {
                                Console.WriteLine("Redirected to default Thank you page. Hence failing the case");
                                report = false;
                            }
                            else
                            {
                                Console.WriteLine("Error Message");
                                report = false;
                            }

                        }

                        break;
                    }
                    else if (driver.Title.Equals(surveyName) && (UIActions.GetElement(driver, "#btnNext") != null))
                    {
                        CommonActions.ScrollDown(driver);
                        UIActions.GetElement(driver, "#btnNext").Click();
                        Thread.Sleep(3000);
                    }
                }
            }
            return report;
        }
        [TestMethod]
        public void DummyPlayerCheck()
        {//single apage
         //RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/12c8860e83294261b3011fb0ec29c988");
         // AuthorActions.VerifyCustomThankUPageInPreview(driver, "https://qa-surveys.cebglobal.com/player/link/3fb57940c48e48cb83b7080777ad6f66");
         //multiplepage
         //RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://qa-surveys.cebglobal.com/player/link/188271e734274effbe2c7953b4772a4c");
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/de30d5cd935d4f4287ea82dfcd371a9c");
            //lastpagehidelogic
            // RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://qa-surveys.cebglobal.com/player/link/8f80ef00daa3471c8403708e2a555c1a");
            List<string> contenToWatch = readThankYouPageContent(driver, null);

            //bool report = VerifyCustomThankUPageInPreview(driver, driver.Title, "This is custom thank you page content");
            //Assert.IsTrue(report);
        }

        public static List<string> readThankYouPageContent(IWebDriver driver, string surveyName)
        {
            //declaring variable;
            string _thankyoupagecontent = null;
            List<string> resultList = new List<string>();
            string _pageTitle = null;
            Thread.Sleep(5000);

            //driver.FindElement(By.XPath("//span[@data-bind='pageCount'][@class='font-18']"));
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8000));
            //wait.Until(ExpectedConditions.ElementExists(By.XPath("//span[@data-bind='pageCount'][@class='font-18']")));
            wait.Until(d =>
            {
                return d.FindElement(By.XPath("//span[@data-bind='pageCount'][@class='font-18']"));
            });

            if (driver.FindElement(By.XPath("//span[@data-bind='pageCount'][@class='font-18']")).Displayed.Equals(false))
            {
                CommonActions.ScrollDown(driver);
                UIActions.GetElement(driver, "#btnFinish").Click();
                Thread.Sleep(5000);

                if (driver.Title == "PreviewFeedback - Pulse")
                {
                    _pageTitle = driver.Title;
                    resultList.Add(_pageTitle);

                }
                else if (driver.Title == "Feedback - Pulse")
                {
                    _pageTitle = driver.Title;
                    resultList.Add(_pageTitle);

                }
                else
                {
                    try
                    {
                        _thankyoupagecontent = UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']").Text;
                        resultList.Add(_thankyoupagecontent);
                    }

                    finally
                    {
                        _pageTitle = driver.Title;
                        resultList.Add(_pageTitle);
                    }
                }

            }
            else
            {
                int _pageCount = Convert.ToInt32(UIActions.GetElementwithXpath(driver, "//span[@data-bind='pageCount'][@class='font-18']", 100).Text);
                int _currentPage = Convert.ToInt32(UIActions.GetElementwithXpath(driver, "//span[@data-bind='currentPage'][@class='font-18']", 60).Text);
                while (_currentPage <= _pageCount)
                {

                    if (driver.Title.Contains(surveyName) && (UIActions.GetElement(driver, "#btnFinish") != null) && (UIActions.GetElement(driver, "#btnFinish").Displayed))
                    {
                        wait.Until<IWebElement>(d =>
                        {
                            return d.FindElement(By.Id("btnFinish"));
                        });
                        Actions ac = new Actions(driver);
                        ac.MoveToElement(UIActions.GetElement(driver, "#btnFinish")).Build().Perform();
                        CommonActions.ScrollDown(driver);
                        UIActions.GetElement(driver, "#btnFinish").Click();
                        wait.Until<IWebElement>(d =>
                        {
                            return (UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']"));
                        });
                        Thread.Sleep(10000);
                        try
                        {
                            _thankyoupagecontent = UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']").Text;
                            resultList.Add(_thankyoupagecontent);
                        }
                        catch (Exception)
                        {
                            break;
                        }
                        finally
                        {
                            _pageTitle = driver.Title;
                            resultList.Add(_pageTitle);
                        }



                        break;
                    }
                    //else if (driver.Title.Equals(surveyName) && (UIActions.GetElement(driver, "#btnNext") != null))
                        else if (driver.Title.Contains(surveyName) && (UIActions.GetElement(driver, "#btnNext") != null))
                    {
                        CommonActions.ScrollDown(driver);
                        UIActions.GetElement(driver, "#btnNext").Click();
                        Thread.Sleep(3000);
                    }
                }
            }
            return resultList;
        }

        [TestMethod]
        public void VerifyCustomSurveyExitAndContinueInPreview()
        {
            
            string surveyName = "New Survey", editType = "Customize Text";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Actions ac = new Actions(driver);
            AuthorActions.CopyForm(driver, "CustomThankYouPage");
            Thread.Sleep(3000);
            string _previewLink = AuthorActions.capturePreviewLink(driver);
            // AuthorActions.switchWindow(driver);
            Thread.Sleep(3000);
            if(UIActions.GetElement(driver, ".shl-icon_exit")!= null)
            UIActions.GetElement(driver, ".shl-icon_exit").Click();
            IReadOnlyCollection<IWebElement> _alert = UIActions.FindElementsWithXpath(driver, "//div[@class='modal-footer no-margin-top']/button");
            Thread.Sleep(3000);
            _alert.FirstOrDefault(d => d.Text == "OK").Click();
            Thread.Sleep(3000);
            string _readValue = UIActions.GetElement(driver, ".bolder").Text;
            bool _comparisonResult = _readValue.Contains("Thank you for your participation.") ? true : false;
            Assert.IsTrue(_comparisonResult);
        }

        [TestMethod]
        public void VerifyCustomSurveyExitAndContinueInPlayer()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            DistributeActions.CreateDistributionFromHomePage(driver,
                new DistributionParametersDTO()
                {
                    FormName = "CustomThankYouPage",
                    DistributionName = "CustomThankYouPage" + DateTime.Now,
                    StartDate = Convert.ToString(DateTime.Now.AddDays(2)),
                    StartTimeZone = "Chennai",
                    // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                    EndDate = null,
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Share Manually" }
                });
            Thread.Sleep(8000);
            
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']");
            Actions ac = new Actions(driver);
            ac.MoveToElement(surveyLink1).Build().Perform();
            Thread.Sleep(3000);
            surveyLink1.Click();
            Thread.Sleep(3000);
            driver.Navigate().GoToUrl(Clipboard.GetText());
            Thread.Sleep(6000);
            if (UIActions.GetElement(driver, ".shl-icon_exit") != null)
                UIActions.GetElement(driver, ".shl-icon_exit").Click();
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _alert = UIActions.FindElementsWithXpath(driver, "//div[@class='modal-footer no-margin-top']/button");
            _alert.FirstOrDefault(d => d.Text == "OK").Click();
            Thread.Sleep(10000);
            bool _copyUrlPresent = UIActions.GetElement(driver, "#copy-url").Displayed;
            Assert.IsTrue(_copyUrlPresent);
        }
        [TestMethod]
        public void VerifyCustomDisabledForNonEditableAccount()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            CreateNewAccThruMigrationTool(driver);
            NavigateToAccountAndSearch(driver, "ModalAcc");
            Thread.Sleep(8000);
            AuthorActions.NavigateToAuthor(driver);
            AuthorActions.SearchAndOpenForm(driver, "Shdnot have Custom");
            bool _isEditable = VerifyThankYouSectionIsEditable(driver);
            Assert.IsFalse(_isEditable);
        }
        public static bool VerifyThankYouSectionIsEditable(IWebDriver driver)
        {
            Actions ac = new Actions(driver);
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='Title']"));
            ac.MoveToElement(titleList.FirstOrDefault(d => d.Text == "Survey Thank You Page")).Build().Perform();
            Thread.Sleep(1000);
            bool _isEnabled = driver.FindElement(By.XPath("//span[@class='btn ceb-icon_home_class_edit action-Thank-you-edit secondary font-sz-23']")).Displayed;
            Assert.IsFalse(_isEnabled);
            return _isEnabled;
         }

        [TestMethod]
        public void VerifyEnableEditThankYouPage()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            AuthorActions.NavToAccountSettings(driver);
            AuthorActions.SelectAccountDefaults(driver);
            bool isSelected = AuthorActions.EnableThankUInAccSettings(driver);
            Assert.IsTrue(isSelected);
        }

        [TestMethod]
        public void VerifyThankYouPageIsEditableinEditor()
        {
            string surveyName = "New Survey", editType = "Customize Text";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, surveyName + DateTime.Now.ToString());
            AuthorActions.AddCommentQuestion(driver, "Test" + DateTime.Now);
            AuthorActions.SetThankUTypeInAtr(driver, editType, surveyName);
        }

        [TestMethod]
        public void VerifyCustomizedThankYouPageInPreview()
        {

            ThankYouPage._surveyName = "New Survey" + DateTime.Now.ToString();
            ThankYouPage._editType = "Customize Text";
            string applicationPath = CommonActions.GetApplicationPath();
            string _filePath = applicationPath + @"DataSet\CustomText.txt";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, ThankYouPage._surveyName);
            AuthorActions.AddCommentQuestion(driver, "Test" + DateTime.Now);
            ThankYouPage = AuthorActions.SetThankUTypeInAtr(driver, ThankYouPage._editType, ThankYouPage._surveyName);
            Thread.Sleep(2000);
            string _previewLink = AuthorActions.capturePreviewLink(driver);
            List<string> readContent = readThankYouPageContent(driver, ThankYouPage._surveyName);
            //string expectedContent = AuthorActions.ReadFromTextFile(@"C:\Automation_\Tellurium\Inputdata\CustomText.txt");
            Thread.Sleep(2000);
            string expectedContent = AuthorActions.ReadFromTextFile(_filePath);
            bool result = (readContent[0].Equals("This is custom thank you page content") ? true : false);
            Assert.IsTrue(result);
           // bool report = VerifyCustomThankUPageInPreview(driver, driver.Title, "This is custom thank you page content");
           // Assert.IsTrue(report);

        }

        [TestMethod]
        public void VerifyDefaultThankYouPageWithTranInPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            //to remove
            Thread.Sleep(2000);
            string applicationPath = CommonActions.GetApplicationPath();
            string _filePath = applicationPath + @"DataSet\CustomText.txt";
            AuthorActions.CopyForm(driver, "DefaultThankYouPageWithTran");
            Thread.Sleep(2000);
            string _previewLink = AuthorActions.capturePreviewLink(driver);
           // AuthorActions.switchWindow(driver);
            string _tranToChk = ReadXML("ThankUTransDefault");
            SelectTranslationInPlayer(Clipboard.GetText(), driver);
            List<string> readContent = readThankYouPageContent(driver, "DefaultThankYouPageWithTran");
            string expectedContent = AuthorActions.ReadFromTextFile(_filePath);
            bool result1 = (driver.FindElement(By.XPath("//div[@class='bolder']")).Text == _tranToChk) ? true : false;
            Assert.IsTrue(result1);
        }
        [TestMethod]
        public void VerifyDefaultThankYouPageWithTranInPlayer()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            DistributeActions.CreateDistributionFromHomePage(driver,
                new DistributionParametersDTO()
                {
                    FormName = "DefaultThankYouPageWithTran",
                    DistributionName = "DefaultThankYouPageWithTran" + DateTime.Now,
                    StartDate = Convert.ToString(DateTime.Now.AddDays(2)),
                    StartTimeZone = "Chennai",
                    // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                    EndDate = null,
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Share Manually" }
                });
            Thread.Sleep(5000);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']");
            Actions ac = new Actions(driver);
            ac.MoveToElement(surveyLink1).Build().Perform();
            Thread.Sleep(3000);
            surveyLink1.Click();
            driver.Navigate().GoToUrl(Clipboard.GetText());
            string _tranToChk = ReadXML("ThankUTransDefault");
            SelectTranslationInPlayer(Clipboard.GetText(), driver);
            Thread.Sleep(3000);
            List<string> readContent = readThankYouPageContent(driver, "WithTranslationWithDefault");
            //string expectedContent = AuthorActions.ReadFromTextFile(@"C:\Automation_C#\Tellurium\Inputdata\CustomText.txt");
            bool result1 =(driver.FindElement(By.XPath("//div[@class='bolder']")).Text == _tranToChk) ? true : false;
           Assert.IsTrue(result1);
        }

        [TestMethod]
        public void VerifyCustomizedThankYouPageInPlayer()
        {
            ThankYouPage._surveyName = "New Survey" + DateTime.Now.ToString();
            ThankYouPage._editType = "Customize Text";
            string applicationPath = CommonActions.GetApplicationPath();
            //C:\Tellurium_Artifacts\Tellurium_Tellurium_813\Tellurium_Artifacts\DataSet
            //bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\BenchmarkTemplate_verification.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            string _filePath = applicationPath + @"DataSet\CustomText.txt";
            string popupHandle = string.Empty;
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, ThankYouPage._surveyName);
            Thread.Sleep(3000);
            AuthorActions.AddCommentQuestion(driver, "Test" + DateTime.Now);
            ThankYouPage = AuthorActions.SetThankUTypeInAtr(driver, ThankYouPage._editType, ThankYouPage._surveyName);
            Thread.Sleep(8000);
            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(8000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "ThankYouPageCheck" + Guid.NewGuid(),
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });
            Thread.Sleep(4000);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']");
            Actions ac = new Actions(driver);
            ac.MoveToElement(surveyLink1).Build().Perform();
            Thread.Sleep(3000);
            surveyLink1.Click();
            driver.Navigate().GoToUrl(Clipboard.GetText());
            //bool report = VerifyCustomThankUPageInPreview(driver, driver.Title, "This is custom thank you page content");
            string expectedContent = AuthorActions.ReadFromTextFile(_filePath);
            List<string> readContent = readThankYouPageContent(driver, ThankYouPage._surveyName);
            bool result = (readContent[0].Equals(expectedContent) ? true : false);
            Assert.IsTrue(result);

        }

        public static bool VerifyThankYouContentPlayer(IWebDriver driver, string surveyName, string thankyoupageContent = null)
        {
            bool report = false;
            if (driver.FindElement(By.XPath("//span[@data-bind='pageCount'][@class='font-18']")).Displayed.Equals(false))
            {
                if (driver.Title.Equals(surveyName) && (UIActions.GetElement(driver, "#btnFinish") != null) && (UIActions.GetElement(driver, "#btnFinish").Displayed))
                {
                    UIActions.GetElement(driver, "#btnFinish").Click();
                    try
                    {
                        var _thankyoupagecontent = UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']").Text;
                        report = (_thankyoupagecontent.Equals(thankyoupageContent) ? true : false);

                    }
                    catch (Exception)
                    {
                        if (driver.Title == "PreviewFeedback - Pulse")
                        {
                            Console.WriteLine("Redirected to default Thank you page. Hence failing the case");
                            report = false;
                        }
                        else
                        {
                            Console.WriteLine("Error Message");
                            report = false;
                        }

                    }


                }
            }
            else
            {
                int _pageCount = Convert.ToInt32(UIActions.GetElementwithXpath(driver, "//span[@data-bind='pageCount'][@class='font-18']", 100).Text);
                int _currentPage = Convert.ToInt32(UIActions.GetElementwithXpath(driver, "//span[@data-bind='currentPage'][@class='font-18']", 60).Text);
                while (_currentPage <= _pageCount)
                {
                    if (driver.Title.Equals(surveyName) && (UIActions.GetElement(driver, "#btnFinish") != null) && (UIActions.GetElement(driver, "#btnFinish").Displayed))
                    {
                        UIActions.GetElement(driver, "#btnFinish").Click();
                        try
                        {
                            var _thankyoupagecontent = UIActions.FindElementWithXpath(driver, "//div[@data-bind='passageDTO.content.htmlContent']").Text;
                            report = (_thankyoupagecontent.Equals(thankyoupageContent) ? true : false);

                        }
                        catch (Exception)
                        {
                            if (driver.Title == "PreviewFeedback - Pulse")
                            {
                                Console.WriteLine("Redirected to default Thank you page. Hence failing the case");
                                report = false;
                            }
                            else
                            {
                                Console.WriteLine("Error Message");
                                report = false;
                            }

                        }

                        break;
                    }
                    else if (driver.Title.Equals(surveyName) && (UIActions.GetElement(driver, "#btnNext") != null))
                    {
                        UIActions.GetElement(driver, "#btnNext").Click();
                    }
                }
            }
            return report;
        }

        [TestMethod]
        public void VerifyThankYouPageInCopiedFormWithTranslationPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CopyForm(driver, "WithTranslation", "WithTranslation" + DateTime.Now);
            //AuthorActions.ClkPreviewBtn(driver);
            //AuthorActions.switchWindow(driver);

            string _previewLink = AuthorActions.capturePreviewLink(driver);
            SelectTranslationInPlayer(_previewLink, driver);
            string _tranToChk = ReadXML("ThankUTrans");
            Thread.Sleep(3000);
            List<string> readContent = readThankYouPageContent(driver, "Copy Of WithTranslationWithTranslation");
            bool result = (readContent[0].Equals(_tranToChk) ? true : false);
            //bool report = VerifyCustomThankUPageInPreview(driver, driver.Title, _tranToChk);
            Assert.IsTrue(result);
        }

        public void SelectTranslationInPlayer(string _previewLink, IWebDriver driver)

        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            wait.Until<bool>(d =>
            {
                return d.Url.ToLowerInvariant().Contains("https://stg-surveys.cebglobal.com/player/link/");
            });
            driver.Manage().Window.Maximize();
            UIActions.GetElementwithXpath(driver, "//button[@class='multiselect dropdown-toggle form-control ceb-dropdown']", 100).Click();
            IReadOnlyCollection<IWebElement> _languages = UIActions.FindElementsWithXpath(driver, "//ul[@class='multiselect-container dropdown-menu']/li");
            string _langToSelect = ReadXML("langToSelect");
            _languages.FirstOrDefault(d => (d.Text).Trim() == _langToSelect).Click();

        }

        [TestMethod]
        public void VerifyDefaultThankYouPage()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, "DefaultThankUPage" + DateTime.Now);
            AuthorActions.AddCommentQuestion(driver, "Comment QUestion" + DateTime.Now);
            string applicationPath = CommonActions.GetApplicationPath();
            string _filePath = applicationPath + @"DataSet\CustomText.txt";
            Thread.Sleep(3000);
            AuthorActions.ClkPreviewBtn(driver);
            AuthorActions.switchWindow(driver);
            List<string> readContent = readThankYouPageContent(driver, "DefaultThankUPage");
            string expectedContent = AuthorActions.ReadFromTextFile(_filePath);
            bool result = (readContent[0].Equals("PreviewFeedback - Pulse") ? true : false);
            Assert.IsTrue(result);
        }
        public string ReadXML(string readXML)
        {
            string applicationPath = CommonActions.GetApplicationPath();
            string _filePath = applicationPath + @"DataSet\PlayerTranslation.xml";
            IEnumerable<XElement> val = XElement.Load(_filePath).Elements("Parameter");
            //IEnumerable<XElement> val = XElement.Load(@"C:\Automation_C#\Tellurium\Inputdata\PlayerTranslation.xml").Elements("Parameter");
            var ele = val.FirstOrDefault(d => (d.Attribute("name").Value) == readXML);
            valValue = (ele.Attribute("value").Value).ToString();
            return valValue;
        }


        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }
    }
}
