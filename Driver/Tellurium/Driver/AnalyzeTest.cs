﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Driver.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using System.Data;

namespace Driver
{
    [TestClass]
    public class AnalyzeTest
    {
        public static bool IsChromeDriverAvailable;
        public static bool IsFirefoxDriverAvailable;
        public static bool IsIEDriverAvailable;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;
        private static string FormID;
        private static string BrowserURl = "http://localhost:3832/";
        private static SessionId SessionID = new SessionId( "cfb65526b4b3b53bcc96f6911f61d6fa");
        public static string GFESurveyformId;
        public static string surveyID;
        public static string surveyform = "DemoAnalyzeEdit1";
        public static DBConnectionStringDTO DBConnectionParameters;


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                 logFilePath = context.Properties["logFilePath"].ToString()
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

  
        public static RemoteWebDriver SetDriverAndNavigateToManageForms(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
            }
            else
            {
                AuthorActions.NavigateToAuthor(driver);
            }
            return driver;
        }
        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        }
        public static void GoToDemographicsTabInSurvey()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(4000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
            AuthorActions.EditPublishedForm(driver);
            Thread.Sleep(2000);
            string CommentQue = "AddCommentQuestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver,CommentQue);
            Thread.Sleep(2000);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            Thread.Sleep(3000);
            SurveyDemographicActions.OpenDemographicModal(driver);
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-add-demographic");
            string label = "Demolabel" + Guid.NewGuid();
            SurveyDemographicActions.AddEditDemographic(driver, label, "Number");
            Thread.Sleep(5000);
            var Closebutton = driver.FindElement(By.Id("demographic-close"));
            UIActions.clickwithID(driver, "demographic-close");
            Thread.Sleep(5000);
            //Actions action = new Actions(driver);
            //action.MoveToElement(Closebutton, 853, 745).Click().Perform();
            //Thread.Sleep(6000);
           }

        [TestMethod]
        public void ValidateViewAllLinkPresent()
        {
            CurrentDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            SetBrowserURl(CurrentDriver);
            //CurrentDriver = BindtoExistingWindow();
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AnalyzeAction.GoToSurveyAnalyzeScreen(CurrentDriver);
            Thread.Sleep(20000);
            AnalyzeAction.OpenAnalyzeForm(CurrentDriver, "Black Pearl 01");
            Thread.Sleep(60000);
            AnalyzeAction.ExpandDemoFilterUsingDemoLabel(CurrentDriver, "Education level");
            Thread.Sleep(10000);
            var viewAllLink = AnalyzeAction.GetViewAllLink(CurrentDriver, "Education level");
            Assert.IsTrue(viewAllLink != null);

        }

        [TestMethod]
        public void ValidateIfDemoPopupIsOpening()
        {
            //CurrentDriver = BindtoExistingWindow();
            Thread.Sleep(10000);
            AnalyzeAction.OpenViewAllModal(CurrentDriver,AnalyzeAction.GetViewAllLink(CurrentDriver, "Education level"));
            Thread.Sleep(10000);
       }

        [TestMethod]
        public void ApplyViewAllDemoFilters()
        {
            UIActions.SelectInMultiSelectCEBDropdownByValue(CurrentDriver, "#Filter-Demographic-Values", new []{ "Completed some college", "Completed some high school" });
            AnalyzeAction.ApplyViewAllFilter(CurrentDriver);
            List<IWebElement> selectedValues =  AnalyzeAction.GetSelectedDemoValues(CurrentDriver, "Education level");
            Assert.IsTrue(selectedValues.Count == 2);
        }

        private void SetBrowserURl(RemoteWebDriver driver)
        {
            BrowserURl = CommonActions.GetExecutorURLFromDriver(driver).ToString();
            SessionID = driver.SessionId;
            Settings.Default["BrowserURL"] = BrowserURl;
            Settings.Default["BrowserSessionID"] = SessionID.ToString();
            Settings.Default.Save();
        }

        private static RemoteWebDriver BindtoExistingWindow()
        {
           CurrentDriver =  new ReuseRemoteWebDriver( new Uri(Settings.Default["BrowserURL"].ToString()), Settings.Default["BrowserSessionID"].ToString());
           return CurrentDriver;
        }

        #region Raw data Export Otpions
        [TestMethod]
        public void ValidateRawDataPopupAppearsOnButtonClick()
        {
            CurrentDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            SetBrowserURl(CurrentDriver);
            //CurrentDriver = BindtoExistingWindow();
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AnalyzeAction.GoToSurveyAnalyzeScreen(CurrentDriver);
            AnalyzeAction.OpenAnalyzeForm(CurrentDriver, "Demo restriction logic s- Dist");
            Thread.Sleep(15000);
            UIActions.Click(CurrentDriver, ".export-btn .btn-primary");

            if (!UIActions.IsElementEnabledById(CurrentDriver, "btnExportData"))
            {
                Thread.Sleep(60000);
            }

            UIActions.Click(CurrentDriver, "#btnExportData");

            Thread.Sleep(3000);
            Assert.IsTrue(UIActions.IsElementVisibleById(CurrentDriver, "raw-data-export-option-modal"));

        }



        [TestMethod]
        public void ValidateDefaultOptionsSelected()
        {
            CurrentDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            SetBrowserURl(CurrentDriver);
            //CurrentDriver = BindtoExistingWindow();
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AnalyzeAction.GoToSurveyAnalyzeScreen(CurrentDriver);
            AnalyzeAction.OpenAnalyzeForm(CurrentDriver, "Demo restriction logic s- Dist");
            Thread.Sleep(15000);
            UIActions.Click(CurrentDriver, ".export-btn .btn-primary");

            if (!UIActions.IsElementEnabledById(CurrentDriver, "btnExportData"))
            {
                Thread.Sleep(60000);
            }

            UIActions.Click(CurrentDriver, "#btnExportData");

            Thread.Sleep(3000);

            IList<IWebElement> contentRadioBtn = CurrentDriver.FindElements(By.Name("opt-content"));
            IList<IWebElement> columnHeaderRadioBtn = CurrentDriver.FindElements(By.Name("opt-header"));

            //Check if default options are selected in both radio buttons
            Assert.IsTrue(contentRadioBtn.ElementAt(0).Selected && columnHeaderRadioBtn.ElementAt(0).Selected);
        }

        [TestMethod]
        public void ValidateDownloadWithDefaultOptions()
        {
            //CurrentDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
            SetBrowserURl(CurrentDriver);
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AnalyzeAction.GoToSurveyAnalyzeScreen(CurrentDriver);
            AnalyzeAction.OpenAnalyzeForm(CurrentDriver, "Demo restriction logic s- Dist");
            Thread.Sleep(15000);
            UIActions.Click(CurrentDriver, ".export-btn .btn-primary");

            if (!UIActions.IsElementEnabledById(CurrentDriver, "btnExportData"))
            {
                Thread.Sleep(60000);
            }

            UIActions.Click(CurrentDriver, "#btnExportData");

             Thread.Sleep(3000);

            UIActions.Click(CurrentDriver, "#btn-download-raw-data-export");
            Thread.Sleep(5000);

            Thread.Sleep(120000);

            //Assert.IsTrue(UIActions.IsElementVisibleByClass(CurrentDriver, "RawData-export-link"));
            UIActions.Click(CurrentDriver, ".RawData-export-link");
            Thread.Sleep(5000);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\RawData - QA - 27390 - Question Text and Scale Labels.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            Assert.IsTrue(areExcelsIdentical);


        }

        [TestMethod]
        public void ValidateDownloadWithContentValuesHeaderQuestionNumber()
        {
            //CurrentDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
            SetBrowserURl(CurrentDriver);
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AnalyzeAction.GoToSurveyAnalyzeScreen(CurrentDriver);
            AnalyzeAction.OpenAnalyzeForm(CurrentDriver, "Demo restriction logic s- Dist");
            Thread.Sleep(15000);
            UIActions.Click(CurrentDriver, ".export-btn .btn-primary");

            if (!UIActions.IsElementEnabledById(CurrentDriver, "btnExportData"))
            {
                Thread.Sleep(60000);
            }

            UIActions.Click(CurrentDriver, "#btnExportData");

            Thread.Sleep(3000);

            UIActions.Click(CurrentDriver, "#opt-content-radio label:nth-child(2)");
            UIActions.Click(CurrentDriver, "#opt-header-radio label:nth-child(2)");




            UIActions.Click(CurrentDriver, "#btn-download-raw-data-export");
            Thread.Sleep(120000);


            //Assert.IsTrue(UIActions.IsElementVisibleByClass(CurrentDriver, "RawData-export-link"));
            UIActions.Click(CurrentDriver, ".RawData-export-link");
            Thread.Sleep(5000);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\RawData - QA - 27390 - Question Number and Scale Values.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            Assert.IsTrue(areExcelsIdentical);


        }

        [TestMethod]
        public void ValidateDownloadWithContentValuesHeaderQuestionText()
        {
            //CurrentDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
            SetBrowserURl(CurrentDriver);
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AnalyzeAction.GoToSurveyAnalyzeScreen(CurrentDriver);
            AnalyzeAction.OpenAnalyzeForm(CurrentDriver, "Demo restriction logic s- Dist");
            Thread.Sleep(15000);
            UIActions.Click(CurrentDriver, ".export-btn .btn-primary");

            if (!UIActions.IsElementEnabledById(CurrentDriver, "btnExportData"))
            {
                Thread.Sleep(60000);
            }

            UIActions.Click(CurrentDriver, "#btnExportData");

            Thread.Sleep(3000);

            UIActions.Click(CurrentDriver, "#opt-content-radio label:nth-child(2)");




            UIActions.Click(CurrentDriver, "#btn-download-raw-data-export");
            Thread.Sleep(120000);


            //Assert.IsTrue(UIActions.IsElementVisibleByClass(CurrentDriver, "RawData-export-link"));
            UIActions.Click(CurrentDriver, ".RawData-export-link");
            Thread.Sleep(5000);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\RawData - QA - 27390 - Question Text and Scale Values.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            Assert.IsTrue(areExcelsIdentical);


        }

        [TestMethod]
        public void ValidateDownloadWithContentLabelsHeaderQuestionNumber()
        {
            //CurrentDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
            SetBrowserURl(CurrentDriver);
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            AnalyzeAction.GoToSurveyAnalyzeScreen(CurrentDriver);
            AnalyzeAction.OpenAnalyzeForm(CurrentDriver, "Demo restriction logic s- Dist");
            Thread.Sleep(15000);
            UIActions.Click(CurrentDriver, ".export-btn .btn-primary");

            if (!UIActions.IsElementEnabledById(CurrentDriver, "btnExportData"))
            {
                Thread.Sleep(60000);
            }

            UIActions.Click(CurrentDriver, "#btnExportData");

            Thread.Sleep(3000);

            UIActions.Click(CurrentDriver, "#opt-header-radio label:nth-child(2)");
            
            UIActions.Click(CurrentDriver, "#btn-download-raw-data-export");
            Thread.Sleep(120000);


            //Assert.IsTrue(UIActions.IsElementVisibleByClass(CurrentDriver, "RawData-export-link"));
            UIActions.Click(CurrentDriver, ".RawData-export-link");
            Thread.Sleep(5000);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\RawData - QA - 27390 - Question Number and Scale Labels.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            Assert.IsTrue(areExcelsIdentical);

        }

        #endregion

        [TestMethod]
        public void ValidateTrendEntireSurveyOnLoad()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            UIActions.ScrollToElement(driver, "#trend-chart-view");
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 12);
            var bars = driver.FindElements(By.CssSelector(".highcharts-series rect"));
            Assert.IsTrue(bars != null && bars.Count == 15);
            var xAxisLabel = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text tspan"));
            Assert.IsTrue(xAxisLabel != null && xAxisLabel.Any(x => x.Text.StartsWith("Q")));
            var benchmarkLine = driver.FindElement(By.CssSelector(".highcharts-plot-lines-5"));
            Assert.IsTrue(benchmarkLine != null);
        }


        [TestMethod]
        public void ValidateSaveUserAnalysisSettingsInCurrentSession()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Chrome";
            var appliedBenchMark = "Black Pearl Manufacturing";
                    IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)

            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Quartermaster")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Quartermaster");
                Thread.Sleep(30000);
            }
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(20000);
            AnalyzeAction.ApplyVsComparisionResults(driver, "chkCompPrev");
            AnalyzeAction.ApplyVsComparisionResults(driver, "chkCompBench");
            AnalyzeAction.ChangeBenchMarkValueInSettings(driver, appliedBenchMark);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(30000);
            AnalyzeAction.GoToHomeScreen(driver);
            Thread.Sleep(30000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(50000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Quartermaster");
            Thread.Sleep(80000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(50000);
            Assert.IsTrue(AnalyzeAction.IfComparisionApplied(driver, "chkCompPrev"));
            Assert.IsTrue(AnalyzeAction.IfComparisionApplied(driver, "chkCompBench"));
            var selectedBenchMark = UIActions.GetSelectedItemInCEBDropDown(driver, "#select-benchmark");
            Assert.AreEqual(appliedBenchMark, selectedBenchMark);

        }
        [TestMethod]
        public void ValidateSaveUserAnalysisSettingsInFutureSessions()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Chrome";
            var appliedBenchMark = "Black Pearl Manufacturing";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Quartermaster")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Quartermaster");
                Thread.Sleep(30000);
            }
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(20000);
            AnalyzeAction.ApplyVsComparisionResults(driver, "chkCompPrev");
            AnalyzeAction.ApplyVsComparisionResults(driver, "chkCompBench");
            AnalyzeAction.ChangeBenchMarkValueInSettings(driver, appliedBenchMark);
            AnalyzeAction.SaveUserAnalysisSettings(driver, "chkFuture");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(30000);
            WebDriverFactory.Logout(driver);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(5000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(60000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Quartermaster");
            Thread.Sleep(80000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(50000);
            Assert.IsTrue(AnalyzeAction.IfComparisionApplied(driver, "chkCompPrev"));
            Assert.IsTrue(AnalyzeAction.IfComparisionApplied(driver, "chkCompBench"));
            var selectedBenchMark = UIActions.GetSelectedItemInCEBDropDown(driver, "#select-benchmark");
            Assert.AreEqual(appliedBenchMark, selectedBenchMark);

        }

        [TestMethod]
        public void ValidateTrendEntireSurveyOnLoadDistribution()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver,"Bar Graph","Distribution","Rating Scale","Entire Survey (All Ratings)");
            Thread.Sleep(30000);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 6);
            var bars = driver.FindElements(By.CssSelector(".highcharts-series rect"));
            Assert.IsTrue(bars != null && bars.Count == 2);
            var benchmarkLine = driver.FindElement(By.CssSelector(".highcharts-plot-lines-5"));
            Assert.IsTrue(benchmarkLine != null);
            var responseRate = driver.FindElements(By.CssSelector(".highcharts-axis-title tspan")).Any(x => x.Text == "Response Rate");
            Assert.IsTrue(responseRate);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiCategoryBarChartDistribution()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver,"Bar Graph","Distribution", "Rating Scale","Categories","General,Medical Benefits, Retirement Benefits");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 6);
            var bars = driver.FindElements(By.CssSelector(".highcharts-series rect"));
            Assert.IsTrue(bars != null && bars.Count == 6);
            var responseRate = driver.FindElements(By.CssSelector(".highcharts-axis-title tspan")).Any(x => x.Text == "Response Rate");
            Assert.IsTrue(responseRate);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiCategoryLineChartDistribution()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Line Graph", "Distribution", "Rating Scale", "Categories", "General,Medical Benefits, Retirement Benefits");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 6);
            var bars = driver.FindElements(By.CssSelector(".highcharts-series-group .highcharts-line-series .highcharts-point"));
            Assert.IsTrue(bars != null && bars.Count == 6);
            var responseRate = driver.FindElements(By.CssSelector(".highcharts-axis-title tspan")).Any(x => x.Text == "Response Rate");
            Assert.IsTrue(responseRate);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiQuestionBarChartDistribution()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(5000);
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Bar Graph", "Distribution", "Rating Scale", "Questions", "Medical,Dental,Vision");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 6);
            var bars = driver.FindElements(By.CssSelector(".highcharts-series rect"));
            Assert.IsTrue(bars != null && bars.Count == 6);
            var responseRate = driver.FindElements(By.CssSelector(".highcharts-axis-title tspan")).Any(x => x.Text == "Response Rate");
            Assert.IsTrue(responseRate);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiQuestionLineChartDistribution()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Line Graph", "Distribution", "Rating Scale", "Questions", "Medical,Dental,Vision");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 6);
            var bars = driver.FindElements(By.CssSelector(".highcharts-series-group .highcharts-line-series .highcharts-point"));
            Assert.IsTrue(bars != null && bars.Count == 6);
            var responseRate = driver.FindElements(By.CssSelector(".highcharts-axis-title tspan")).Any(x => x.Text == "Response Rate");
            Assert.IsTrue(responseRate);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiQuestionBarChartMonth()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Bar Graph", "Month", "Rating Scale", "Questions", "Medical,Dental,Vision");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 12);
            var xAxisLabels = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text tspan"));
            DateTime dt;
            Assert.IsTrue(xAxisLabels != null && xAxisLabels.Any(x => DateTime.TryParseExact(x.Text, "MMM yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)));
            var bars = driver.FindElements(By.CssSelector(".highcharts-series rect"));
            Assert.IsTrue(bars != null && bars.Count == 123);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiQuestionLineChartMonth()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Line Graph", "Month", "Rating Scale", "Questions", "Medical,Dental,Vision");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 12);
            var xAxisLabels = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text tspan"));
            DateTime dt;
            Assert.IsTrue(xAxisLabels != null && xAxisLabels.Any(x => DateTime.TryParseExact(x.Text,"MMM yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)));
            var bars = driver.FindElements(By.CssSelector(".highcharts-series-group .highcharts-line-series .highcharts-point"));
            Assert.IsTrue(bars != null && bars.Count == 123);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiQuestionHeatMapMonth()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Heat Map", "Month", "Rating Scale", "Questions", "Medical,Dental,Vision");
            Thread.Sleep(30000);
            var columns = driver.FindElements(By.CssSelector("#trend-table tr#header td"));
            Assert.IsTrue(columns != null && columns.Count == 46);
            var columnsLabels = driver.FindElements(By.CssSelector("#trend-table tr#header td label"));
            DateTime dt;
            Assert.IsTrue(columnsLabels != null && columnsLabels.Any(x => DateTime.TryParseExact(x.Text, "MMM yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)));
            var rows = driver.FindElements(By.CssSelector("th.trend-view span.category-name"));
            Assert.IsTrue(rows != null && rows.Count == 3);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiCategoryBarChartQuarter()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Bar Graph", "Quarter", "Rating Scale", "Categories", "General,Medical Benefits, Retirement Benefits");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 12);
            var xAxisLabel = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text tspan"));
            Assert.IsTrue(xAxisLabel != null && xAxisLabel.Any(x => x.Text.StartsWith("Q")));
            var bars = driver.FindElements(By.CssSelector(".highcharts-series rect"));
            Assert.IsTrue(bars != null && bars.Count == 45);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiCategoryLineChartQuarter()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Line Graph", "Quarter", "Rating Scale", "Categories", "General,Medical Benefits, Retirement Benefits");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 12);
            var xAxisLabel = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text tspan"));
            Assert.IsTrue(xAxisLabel != null && xAxisLabel.Any(x => x.Text.StartsWith("Q")));
            var bars = driver.FindElements(By.CssSelector(".highcharts-series-group .highcharts-line-series .highcharts-point"));
            Assert.IsTrue(bars != null && bars.Count == 45);
        }

        [TestMethod]
        public void ValidateTrendRatingScaleMultiCategoryHeatMapQuarter()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Heat Map", "Quarter", "Rating Scale", "Categories", "General,Medical Benefits, Retirement Benefits");
            Thread.Sleep(30000);
            var columns = driver.FindElements(By.CssSelector("#trend-table tr#header td"));
            Assert.IsTrue(columns != null && columns.Count == 16);
            var columnsLabels = driver.FindElements(By.CssSelector("#trend-table tr#header td label"));
            Assert.IsTrue(columnsLabels != null && columnsLabels.Any(x => x.Text.StartsWith("Q")));
            var rows = driver.FindElements(By.CssSelector("th.trend-view span.pointer"));
            Assert.IsTrue(rows != null && rows.Count == 3);
        }

        [TestMethod]
        public void ValidateTrendNonRatingScaleMultiSelectBarChartQuarter()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Bar Graph", "Quarter", "Non-Rating Scale", " I read the benefits-related e-mails I re...", "25-50% of the time,50-75% of the time,Always");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 12);
            var xAxisLabel = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text tspan"));
            Assert.IsTrue(xAxisLabel != null && xAxisLabel.Any(x => x.Text.StartsWith("Q")));
            var bars = driver.FindElements(By.CssSelector(".highcharts-series rect"));
            Assert.IsTrue(bars != null && bars.Count == 44);
        }

        [TestMethod]
        public void ValidateTrendNonRatingScaleMultiSelectLineChartQuarter()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Line Graph", "Quarter", "Non-Rating Scale", " I read the benefits-related e-mails I re...", "25-50% of the time,50-75% of the time,Always");
            Thread.Sleep(30000);
            var legends = driver.FindElements(By.CssSelector(".highcharts-legend-item"));
            Assert.IsTrue(legends != null && legends.Count == 3);
            var xAxis = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text"));
            Assert.IsTrue(xAxis != null && xAxis.Count == 12);
            var xAxisLabel = driver.FindElements(By.CssSelector(".highcharts-xaxis-labels text tspan"));
            Assert.IsTrue(xAxisLabel != null && xAxisLabel.Any(x => x.Text.StartsWith("Q")));
            var bars = driver.FindElements(By.CssSelector(".highcharts-series-group .highcharts-line-series .highcharts-point"));
            Assert.IsTrue(bars != null && bars.Count == 44);
        }

        [TestMethod]
        public void ValidateTrendNonRatingScaleMultiSelectHeatMapDistribution()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Heat Map", "Distribution", "Non-Rating Scale", " I read the benefits-related e-mails I re...", "25-50% of the time,50-75% of the time,Always");
            Thread.Sleep(30000);
            var columns = driver.FindElements(By.CssSelector("#trend-table tr#header td"));
            Assert.IsTrue(columns != null && columns.Count == 3);
            var rows = driver.FindElements(By.CssSelector("th.trend-view span.category-name"));
            Assert.IsTrue(rows != null && rows.Count == 3);
        }

        [TestMethod]
        public void ValidateTrendNonRatingScaleSelectAllHeatMapYear()
        {
            DriverAndLogin.Account = "MI6";
            DriverAndLogin.Browser = "Firefox";
            IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            if (driver.Url.ToLower().Contains("analyze/reports/") && driver.FindElements(By.CssSelector("#dvanalyzeResultBanner .filteredItem span"))[0].Text == "Goldfinger")
            {
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, "Goldfinger");
                Thread.Sleep(30000);
            }
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.TrendActionMenuActions(driver, "Heat Map", "Year", "Non-Rating Scale", " I read the benefits-related e-mails I re...", "All");
            Thread.Sleep(30000);
            var columns = driver.FindElements(By.CssSelector("#trend-table tr#header td"));
            Assert.IsTrue(columns != null && columns.Count == 5);
            var columnsLabels = driver.FindElements(By.CssSelector("#trend-table tr#header td label"));
            DateTime dt;
            Assert.IsTrue(columnsLabels != null && columnsLabels.Any(x => DateTime.TryParseExact(x.Text, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt)));
            var rows = driver.FindElements(By.CssSelector("th.trend-view span.category-name"));
            Assert.IsTrue(rows != null && rows.Count == 7);
        }

        #region DemoEditValidateAnalyze

        [TestMethod]

        public void verifydemolabelleftfilter(RemoteWebDriver driver)
        {
            GoToDemographicsTabInSurvey();
            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "DemofilterAnalyzecheck" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });
            Thread.Sleep(5000);

            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID="+surveyID+" order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
            string query = @"select label from dimSurveyDemographicColumnLabel where GFESurveyFormID = " + GFESurveyformId + " and Status = 1";
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsTrue(AuthorActions.ValidateDemoQuestionInDB(ds));
        }

        [TestMethod]
        public void editdemolabelfilter()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(9000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
           // AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(5000);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            Thread.Sleep(9000);
            SurveyDemographicActions.OpenDemographicModal(driver);
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-add-demographic");
            string label = "Demolabel" + Guid.NewGuid();

            SurveyDemographicActions.AddEditDemographic(driver, label, "Number");
            Thread.Sleep(5000);
            IWebElement searchtext = driver.FindElement(By.XPath("//input[@type='search']"));
            Thread.Sleep(2000);
            searchtext.SendKeys(label);
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(label));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var gear = currenttr.FindElement(By.CssSelector("button.dropdown-toggle"));
            gear.Click();
            var editLink = currenttr.FindElement(By.CssSelector("ul.dropdown-menu li a.editDemographics"));
            editLink.Click();

            //IWebElement democolumn = driver.FindElement(By.XPath("//table[@id='add-edit-demographics']//label[text()=" + label +"]/tr[position()=last()]/../following-sibling::td[6]//button"));
            //IWebElement democolumn = driver.FindElement(By.XPath("//table[@id='add-edit-demographics']//tr[td[contains(.,"+ label + ")]][last()]//../following-sibling::td[6]//button"));
            //democolumn.Click();
      

            string label1 = "Editlabel" + Guid.NewGuid();
            var nameBox =driver.FindElements(By.CssSelector("#add-edit-demographics tbody input")).FirstOrDefault(x => x.Displayed);
            nameBox.Clear();
            nameBox.SendKeys(label1);
            Thread.Sleep(3000);
            var tick = driver.FindElement(By.XPath("//span[contains(@class,'ceb-icon_tick action-demo-edit-confirm')]"));
            tick.Click();
            //SurveyDemographicActions.EditDemographicsActions(driver, label);
            UIActions.clickwithID(driver, "demographic-close");
            Thread.Sleep(6000);

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "DemofilterAnalyzecheck" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });

            Thread.Sleep(9000);

            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
            string query = @"select label from dimSurveyDemographicColumnLabel where GFESurveyFormID = " + GFESurveyformId + " and Status = 1";
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsTrue(AuthorActions.ValidateDemoQuestionInDB(ds));
        }



        [TestMethod]

        public void demolabeldeletefilter()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(9000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
            // AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            AuthorActions.EditPublishedForm(driver);
            Thread.Sleep(5000);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            Thread.Sleep(9000);
            SurveyDemographicActions.OpenDemographicModal(driver);
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-add-demographic");
            string label = "Demolabel" + Guid.NewGuid();
            SurveyDemographicActions.AddEditDemographic(driver, label, "Number");
            Thread.Sleep(5000);
            Assert.IsTrue(UIActions.GetElementwithXpath(driver, "//span[@class='btn btn-primary btnNo']", 45).Displayed);
            var Closebutton = driver.FindElement(By.Id("demographic-close"));
            UIActions.clickwithID(driver, "demographic-close");
            Actions action = new Actions(driver);
            action.MoveToElement(Closebutton, 853, 745).Click().Perform();
            Thread.Sleep(6000);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(8000);
            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "DemofilterAnalyzecheck" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });

            Thread.Sleep(9000);

            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
            string query = @"select label from dimSurveyDemographicColumnLabel where GFESurveyFormID = " + GFESurveyformId + " and Status = 1";
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsTrue(AuthorActions.ValidateDeletedemo(ds));
        }

   
        [TestMethod]
        public void editdemovalueleftfilter()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            //RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            Thread.Sleep(2000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
            AuthorActions.EditPublishedForm(driver);
            Thread.Sleep(5000);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            Thread.Sleep(9000);
            SurveyDemographicActions.OpenDemographicModal(driver);
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-add-demographic");
            string label = "Demolabel" + Guid.NewGuid();
            SurveyDemographicActions.AddEditDemographic(driver, label, "Text");
            Thread.Sleep(5000);
            //Assert.IsTrue(UIActions.GetElementwithXpath(driver, "//span[@class='btn btn-primary btnNo']", 45).Displayed);
            Thread.Sleep(2000);
            var Closebutton = driver.FindElement(By.Id("demographic-close"));
            UIActions.clickwithID(driver, "demographic-close");
            Actions action = new Actions(driver);
            action.MoveToElement(Closebutton, 853, 745).Click().Perform();
            Thread.Sleep(6000);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(8000);
            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "DemofilterAnalyzecheck" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                //EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });

            Thread.Sleep(2000);

            /*DistributeActions.SearchAndOpenDistribution(driver, "DemofilterAnalyzecheck9/26/2018 4:20:23 PM");
            IWebElement element = UIActions.GetElementwithXpath(driver,
                   "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
            Actions action = new Actions(driver);
            action.MoveToElement(element).Click().Perform();
            IWebElement editButton = UIActions.GetElementWithWait(driver,
                "button[widget-data-event='click::SelectParticipantSectionForEdit']", 45);
            if (editButton != null)
                editButton.Click();*/

            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com","name1","5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();
            Thread.Sleep(1000);
           
            
            DistributeActions.EditParticipant(driver, 5, "Editvalue");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
            driver.Navigate().Refresh();
            //IWebElement element = UIActions.GetElement(driver,
            //       "#btn-next");
           
            //action.MoveToElement(element,34,22).Build().Perform();
            //element.Click();
            //Thread.Sleep(2000);
            //UIActions.Click(driver, "#btn-next");
            //Thread.Sleep(2000);
            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
            string query = string.Format("Select demo1 from DimParticipant where surveyID=" + surveyID + "");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, Query);
            Assert.IsFalse(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }

        [TestMethod]

        public void deletedemovaluefilter()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(5000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
            // AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(5000);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            Thread.Sleep(5000);
            SurveyDemographicActions.OpenDemographicModal(driver);
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-add-demographic");
            string label = "Demolabel" + Guid.NewGuid();
            SurveyDemographicActions.AddEditDemographic(driver, label, "Number");
            Thread.Sleep(5000);
            UIActions.clickwithID(driver, "demographic-close");
            Thread.Sleep(2000);
            // AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "DemofilterAnalyzecheck" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });
            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();
            Thread.Sleep(3000);
            
            DistributeActions.EditDemoValueDynamic(driver, label, "EditValue");
            //DistributeActions.EditParticipant(driver, 7, "Editvalue");
            UIActions.Click(driver, "#manage-participants-table td.select-checkbox");
            UIActions.Click(driver, "[widget-data-event='click::EditParticipant']");
            Thread.Sleep(4000);
            UIActions.clickwithXapth(driver, "//*[@class='btn btn-primary edit-fields-btn']");
            Console.WriteLine(label);
            IWebElement DemoValuefield = driver.FindElement(By.XPath("//*[@id='view-edit-participant-modal']//div[@widget-name='ParticipantDetailsWidget']//label[text()='"+label+"']/following::input[2]"));
            var nonmandatorySymbol = UIActions.GetAttrForElementByXpathCSSattribute(driver, "//*[@id='view-edit-participant-modal']//div[@widget-name='ParticipantDetailsWidget']//label[text()='"+label+"']/following-sibling::span", "display");
            if (nonmandatorySymbol.ToLower() == "none")
            {
                DemoValuefield.Clear();

            }

            UIActions.Click(driver, "[widget-data-event='click::SaveParticipantDetails']");
            driver.Navigate().Refresh();
            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
            string query = string.Format("Select demo1 from DimParticipant where surveyID=" + surveyID + "");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, Query);
            Assert.IsFalse(AuthorActions.ValidateDemoScaleoptionInDB(ds));

        }

        [TestMethod]
        public void DisableAvailableForFilter()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(5000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
            // AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(5000);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            Thread.Sleep(5000);
            SurveyDemographicActions.OpenDemographicModal(driver);
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-add-demographic");
            string label = "Demolabel" + Guid.NewGuid();
            SurveyDemographicActions.AddEditDemographic(driver, label, "Text");
            Thread.Sleep(5000);
            IWebElement searchtext = driver.FindElement(By.XPath("//input[@type='search']"));
            Thread.Sleep(2000);
            searchtext.SendKeys(label);
            SurveyDemographicActions.ToggleFiltering(driver, label);
            UIActions.clickwithID(driver, "demographic-close");
            Thread.Sleep(2000);
           // AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(5000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "DemofilterAnalyzecheck" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });
            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();
            Thread.Sleep(1000);
            driver.Navigate().Refresh();
            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
            string query = string.Format("Select IsAvailableForAnalyze from dimSurveyDemographicColumnLabel where surveyID=" + surveyID + "and label="+label+" and IsAvailableForAnalyze=0");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, Query);
        }

        [TestMethod]
        public void DemoquestionAvailableForFilter()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(5000);
           // AuthorActions.CreateForm(driver, "DemoQuestionAnlayze" + DateTime.Now);
            AuthorActions.SearchandCreateForm(driver, surveyform);
           string categoryque=AuthorActions.AddMultipleChoiceDemographicQuestion(driver, "MCQuestion1"+Guid.NewGuid(), new string[] { "MCR1", "MCR2", "MCR3" });
            Thread.Sleep(5000);
            
          
            AuthorActions.PublishForm(driver, surveyform);
            Thread.Sleep(7000);
            UIActions.clickwithXapth(driver, "//*[@id='publishPopUpModal']//button[@class='close']");
            Thread.Sleep(4000);
            AuthorActions.SearchAndOpenForm(driver, surveyform);
            Thread.Sleep(2000);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            SurveyDemographicActions.OpenDemographicModal(driver);
            Thread.Sleep(5000);
            UIActions.Click(driver, "#btn-add-demographic");
            IWebElement searchtext = driver.FindElement(By.XPath("//input[@type='search']"));
            searchtext.SendKeys(categoryque);
           
            SurveyDemographicActions.ToggleFiltering(driver,categoryque);
            UIActions.clickwithID(driver, "demographic-close");
            Thread.Sleep(5000);
            UIActions.clickwithID(driver, "btnFormDistribute");
           
            Thread.Sleep(5000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = categoryque,
                DistributionName = "DemofilterAnalyzecheck" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails" }
            });
            Thread.Sleep(2000);
            DistributeActions.OpenManualParticipantScreen(driver);
            DistributeActions.Addparticipantstandardlabel(driver, "name1@cebglobal.com", "name1", "5");
            DistributeActions.AddparticipantMandatoryField(driver);
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();
            Thread.Sleep(1000);
            driver.Navigate().Refresh();
         
            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
            string query = string.Format("Select IsAvailableForAnalyze from dimSurveyDemographicColumnLabel where surveyID=" + surveyID + "and label="+categoryque+" and IsAvailableForAnalyze=0");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, Query);
        }
        #endregion

        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }

    }

}
