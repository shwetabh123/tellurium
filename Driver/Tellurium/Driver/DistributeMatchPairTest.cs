﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;
using Driver.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;


namespace Driver
{
    [TestClass]
    public class DistributeMatchPairTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;

        public static string distributionName = "Enroute";


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
               
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = Convert.ToString(context.Properties["downloadFilepath"])
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName, string accountName  = null)
        {

            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            if (driver == null)
            {
                driver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
                Thread.Sleep(30000);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        
        }
        [TestMethod]
        public void ValidateCopyMatchPairDistributionValidations()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            //   DistributeActions.SearchAndOpenDistribution(driver, "testfullupdate");
            UIActions.GetElementWithWait(driver, "#distribute-survey-table", 30);
            IWebElement searchBox = driver.FindElement(By.Id("txtSearch"));
            searchBox.Clear();
            searchBox.SendKeys("testfullupdate");
            UIActions.Click(driver, "#btnSearch");
            Thread.Sleep(200000);
            IWebElement ActionGear = driver.FindElement(By.XPath("//*[@class='btn btn-link no-underline dropdown-toggle']"));
            ActionGear.Click();

            UIActions.clickwithXapth(driver, "//*[@widget-data-event='click::CopyDistributionClicked']");
            Thread.Sleep(1000);
            UIActions.Click(driver, "#chkCopyParticipants");
            Thread.Sleep(1000);
            UIActions.Click(driver, "#chkCopyEmails");
            UIActions.clickwithXapth(driver, "//*[@widget-data-event='click::CopyClicked']");
            Thread.Sleep(200000);
            IWebElement chkbox = driver.FindElement(By.Id("recurring-survey"));
            bool result = chkbox.Enabled;
            Assert.AreEqual(result, true);
        }
        [TestMethod]
        public void ValidateMatchPairValidations()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
          
            DistributeActions.SearchAndOpenDistribution(driver, "testfullupdate");
            Thread.Sleep(200000);

            DistributeActions.EditMatchPairParticipantForValidation(driver, "gnatarajamani@cebglobal.com", 5, 7, "gnatarajamani@cebglobal.com", false);
            Thread.Sleep(90000);
            string Message;
            Message = UIActions.GetValueByElement(driver, ".alert-danger #action-status");
            Assert.AreEqual("Parent and Child email addresses cannot be the same value.", Message.Trim());
            UIActions.clickwithID(driver, "btnClose");
            DistributeActions.EditMatchPairParticipantForValidation(driver, "", 1, 2, "gnatarajamani@cebglobal.com");
            Thread.Sleep(5000);
            Message = UIActions.GetValueByElement(driver, ".alert-danger #action-status");
            Assert.AreEqual("Please enter value for emailaddress", Message.Trim());
            UIActions.clickwithID(driver, "btnClose");

            DistributeActions.EditMatchPairParticipantForValidation(driver, "", 3, 2, "gnatarajamani@cebglobal.com");
            Thread.Sleep(5000);
            Message = UIActions.GetValueByElement(driver, ".alert-danger #action-status");
            Assert.AreEqual("Please enter value for Name", Message.Trim());
            UIActions.clickwithID(driver, "btnClose");

            DistributeActions.EditMatchPairParticipantForValidation(driver, "", 5, 2, "gnatarajamani@cebglobal.com");
            Thread.Sleep(5000);
            Message = UIActions.GetValueByElement(driver, ".alert-danger #action-status");
            Assert.AreEqual("Please enter a value for ParentEmail", Message.Trim());
            UIActions.clickwithID(driver, "btnClose");

            DistributeActions.EditMatchPairParticipantForValidation(driver, "", 9, 2, "gnatarajamani@cebglobal.com");
            Thread.Sleep(5000);
            Message = UIActions.GetValueByElement(driver, ".alert-danger #action-status");
            Assert.AreEqual("Please enter a value for ParentName", Message.Trim());



        }

        [TestMethod]
        public void checkIFImportResponseLinkIsEnabled()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser, "GN- Matched Pair");
            CurrentDriver = driver;
            SetBrowserURl(driver);
            DistributeActions.CreateOnetimeMatchPairUploadDistribution(driver, "Demo_MatchedPairSurvey", "06/11/2018 2:16 PM", null);
            Thread.Sleep(2000);
            //var driver = BindtoExistingWindow();
            Assert.IsTrue(DistributeActions.OpenImportResponseModal(driver));
            Thread.Sleep(14000);

        }

        [TestMethod]
        public void ValidateMatchedPairResponseUpload()
        {
            //CurrentDriver = BindtoExistingWindow();
            string applicationPath = CommonActions.GetApplicationPath();
            Assert.IsTrue(DistributeActions.UploadImportResponseTemplate(CurrentDriver, applicationPath + @"DataSet\Matchedpair_Resposne_Import.xlsx"));

        }

        [TestMethod]
        public void ValidateMatchedPairResponseUploadModalStatus()
        {
            //CurrentDriver = BindtoExistingWindow();
            Thread.Sleep(5000);
            string applicationPath = CommonActions.GetApplicationPath();
            DistributeActions.OpenImportResponseModal(CurrentDriver);
            Thread.Sleep(5000);
            if (DistributeActions.UploadImportResponseTemplate(CurrentDriver, applicationPath + @"DataSet\Matchedpair_Resposne_Import_Validation.xlsx"))
            {
                Thread.Sleep(1000);
                Assert.IsTrue(DistributeActions.CheckImportResponseStatus(CurrentDriver, "Process completed with errors"));
            }
            else
            {
                Assert.Fail("Upload is not successfull.");
            }
        }

        [TestMethod]
        public void VerifyErrorReportIsDownloaded()
        {
            Thread.Sleep(10000);
            //CurrentDriver = BindtoExistingWindow();
            string applicationPath = CommonActions.GetApplicationPath();
            Assert.IsTrue(DistributeActions.ValidateErrorReportIsDownloaded(CurrentDriver, "Process completed with errors", DriverAndLogin.downloadFilepath, applicationPath + @"DataSet\Matchedpair_Response_Error_Report.xlsx"));
        }

        [TestMethod]
        public void CheckIFTemplateGetsDownLoaded()
        {
            //Thread.Sleep(10000);
            //CurrentDriver.FindElement(By.Id("DownLoadImportResponseTemplate")).Click();
            Thread.Sleep(10000);
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Thread.Sleep(10000);
            DistributeActions.readExcel(CurrentDriver, DriverAndLogin.downloadFilepath, latestfile);
        }

        private static RemoteWebDriver BindtoExistingWindow()
        {
            var CurrentDriver = new ReuseRemoteWebDriver(new Uri(Settings.Default["BrowserURL"].ToString()), Settings.Default["BrowserSessionID"].ToString());
            return CurrentDriver;
        }

        private void SetBrowserURl(RemoteWebDriver driver)
        {
            var BrowserURl = CommonActions.GetExecutorURLFromDriver(driver).ToString();
            var SessionID = driver.SessionId;
            Settings.Default["BrowserURL"] = BrowserURl;
            Settings.Default["BrowserSessionID"] = SessionID.ToString();
            Settings.Default.Save();
        }


        [ClassCleanup()]
        public static void CleanUp()
        {
            //RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            //if (driver != null)
            //{
            //    driver.Close();
            //    driver.Dispose();
            //}
        }
    }
}
