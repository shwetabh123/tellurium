﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace Driver
{
    [TestClass]
    public class DemographicSuppressionMSTests
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString()
            };
        }
        //shwetabh srivastava----added capabilities for download file in specified location at run time//
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    driver = new FirefoxDriver(service);
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    //driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        //[TestMethod]
        //public void ValidateLeftsideDemoSelection()
        //{
        //    RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
        //    WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
        //    AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
        //    Thread.Sleep(1000);
        //    AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
        //    Thread.Sleep(130000);
        // //   SuppressionActions.Expandleftsidedemo(driver, "How long have you worked this company?");
        //    SuppressionActions.Expandleftsidedemo(driver, "How old are you?");
        //    Thread.Sleep(30000);
        //    //    SuppressionActions.Selectleftsidedemo(driver, "How long have you worked this company?", "0-4 months");
        //    SuppressionActions.Selectleftsidedemo(driver, "How old are you?", "26-35");
        //    Thread.Sleep(30000);
        //    SuppressionActions.DateRanges(driver, "01/01/20015", "06/01/2015");
        //    Thread.Sleep(130000);
        //    SuppressionActions.ExpandandSelectAdditionalFilters(driver, "city");
        //    Thread.Sleep(30000);
        //    SuppressionActions.ExpandandSelectAdditionalFilters(driver, "Which of the following best describes your marital status?");
        //    Thread.Sleep(30000);
        //    SuppressionActions.ExpandandSelectAdditionalFilters(driver, "Which of the following best describes your level in #the organization## ?");
        //    Thread.Sleep(30000);
        //    driver.Quit();
        //    Thread.Sleep(30000);
        //}


        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);

            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);


        //    DriverAndLogin.Account = "Automation Suppression";
      //      DriverAndLogin.Browser = "Chrome";
  
            //IWebDriver driver = GetDriver(DriverAndLogin.Browser);
            //if (driver == null)
            //{
            //    driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            //    WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            //    Thread.Sleep(5000);
            //}


            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);
      

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);

            
            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);
            
            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver,"country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


  


//*****************************************


            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);

            //******************************************

            //verify demovalue-country is disabled in  LeftsideDemo dropdown

           // IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");

            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);


        }



        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionSingleDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);
      




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            AnalyzeAction.Apply(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




   //         ****************************





            
            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);

//            ******************************


            //verify demovalue is disabled in  LeftsideDemo dropdown
         
       //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");

            
       //     Assert.IsTrue(demodropdownvalue.Displayed);


            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);


            Thread.Sleep(30000);
            driver.Quit();
            Thread.Sleep(30000);

        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);




            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(120000);

            //         ****************************



            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);


            //         ****************************



            //verify demovalue is disabled in  LeftsideDemo dropdown
            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");
            //Assert.IsTrue(demodropdownvalue.Displayed);

            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);





            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionMultiDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);



            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(120000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);



            //verify demovalue is disabled in  LeftsideDemo dropdown

            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");
            //Assert.IsTrue(demodropdownvalue.Displayed);


            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);


            Thread.Sleep(30000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver,"01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);






            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);



            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);



            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);




            //verify demovalue is disabled in  LeftsideDemo dropdown
            //      IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[contains(.,'country') and @class='disabled']");


            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");
            //Assert.IsTrue(demodropdownvalue.Displayed);


            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);

            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);







            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);


            //verify demovalue is disabled in  LeftsideDemo dropdown
            //      IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[contains(.,'country') and @class='disabled']");


            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");
            //Assert.IsTrue(demodropdownvalue.Displayed);



            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);
            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {
            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);



            //verify demovalue is disabled in  LeftsideDemo dropdown
            //      IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[contains(.,'country') and @class='disabled']");
            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");
            //Assert.IsTrue(demodropdownvalue.Displayed);



            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);
            Thread.Sleep(10000);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinLeftsideDemoSelectionwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);
  





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);




            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ExpandAdditionalFilters(driver);
            ////type additional filters
            //IWebElement aditionalfiltertype = UIActions.FindElementWithXpath(driver, "//*[@id='addColumnFilter']");
            //aditionalfiltertype.SendKeys("country");
            //Thread.Sleep(30000);
            ////search additional filters
            //IWebElement aditionalfilterssearch = UIActions.FindElementWithXpath(driver, "//*[@id='searchColumnFilter']");
            //aditionalfilterssearch.Click();
            //Thread.Sleep(30000);



            //verify demovalue is disabled in  LeftsideDemo dropdown
            //      IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[contains(.,'country') and @class='disabled']");
            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, "//*[@id='searchList-ul']//li[@class='disabled' and contains(.,'country')]");
            //Assert.IsTrue(demodropdownvalue.Displayed);

            IWebElement Chip = UIActions.FindElementWithXpath(driver, "//*[@class='mdc-chip mdc-chip-disabled']//*[text()='country']");


            Assert.IsTrue(Chip.Displayed);
            Thread.Sleep(10000);



            driver.Quit();
            Thread.Sleep(30000);
        }


        [TestMethod]
        public void ValidatedisabledDropdownDemographicinDemographicReportSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);

            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");

            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);





            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;

            //xpath of disabled demo

            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");


            //verify demovalue is disabled in dropdown


            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);

            //Assert.IsTrue(demodropdownvalue.Displayed);




            //verify demovalue is disabled in dropdown

    

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            //   var demodropdownvalue = (IWebElement)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");

            var demodropdownvalue =(bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);

            //        string a = demodropdownvalue.GetCssValue("style");


            //            Assert.AreEqual( "disabled",a);





            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);



        }



        [TestMethod]
        public void ValidatedisabledDropdownDemographicinDemographicReportSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);



            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;


            //xpath of disabled demo
            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");
            //verify demovalue is disabled in dropdown


            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            //Assert.IsTrue(demodropdownvalue.Displayed);

            //verify demovalue is disabled in dropdown


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demodropdownvalue = (bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinDemographicReportMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


       




            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);



            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;


            //xpath of disabled demo
            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");
            //verify demovalue is disabled in dropdown

            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            //Assert.IsTrue(demodropdownvalue.Displayed);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demodropdownvalue = (bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);

            driver.Quit();
            Thread.Sleep(10000);
        }

        [TestMethod]

        public void ValidatedisabledDropdownDemographicinDemographicReportMultiDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);

            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);








            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);
            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;

            //xpath of disabled demo
            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");


            //verify demovalue is disabled in dropdown

            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            //Assert.IsTrue(demodropdownvalue.Displayed);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demodropdownvalue = (bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);



            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);

            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //xpath of disabled demo
            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");
          
            //verify demovalue is disabled in dropdown

            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            //Assert.IsTrue(demodropdownvalue.Displayed);




            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demodropdownvalue = (bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }


        [TestMethod]
        public void ValidatedisabledDropdownDemographicinDemographicReportwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);


            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);


            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;









            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //xpath of disabled demo
            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");
            //verify demovalue is disabled in dropdown


            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            //Assert.IsTrue(demodropdownvalue.Displayed);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demodropdownvalue = (bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);

            driver.Quit();
            Thread.Sleep(10000);

        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);




            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");


            //Thread.Sleep(80000);


            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);


            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);

            //xpath of disabled demo
            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");


            //verify demovalue is disabled in dropdown

            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            //Assert.IsTrue(demodropdownvalue.Displayed);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demodropdownvalue = (bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);


            
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinDemographicReportwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);


            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");

            //seconddistribution.Click();
            //Thread.Sleep(180000);

            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);

            //SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);

            //string xpath1 = null;
            //string xpath2 = null;
            //string xpath3 = null;
            //string xpathgenerated = null;
            //xpath1 = " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'";
            //xpath2 = "country";
            //xpath3 = "')]//input[@disabled]";
            //xpathgenerated = xpath1 + xpath2 + xpath3;



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandDemographicDropdowninDemographicReport(driver);

            //xpath of disabled demo
            //   IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " (//*[contains(@class,'btn-group ceb-dropdown-container style-scope demographic-view')])[1]//li//label[contains(.,'country')]//input[@disabled]");

            //verify demovalue is disabled in dropdown

            //IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, xpathgenerated);
            //Assert.IsTrue(demodropdownvalue.Displayed);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var demodropdownvalue = (bool)js.ExecuteScript("return (document.querySelector('demographic-view').shadowRoot.querySelectorAll('input'))[28].hasAttribute('disabled') ");


            Assert.IsTrue(demodropdownvalue);

            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownSingleDistributionAnsweratleast1question()
        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(30000);


            //verify demovalue is disabled in Identifier dropdown
            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);




            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownSingleDistributionCompletedthesurvey()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(30000);
            //verify demovalue is disabled in Identifier dropdown
            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);
            Thread.Sleep(30000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(10000);
            //verify demovalue is disabled in Identifier dropdown

            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownMultiDistributionCompletedthesurvey()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);


            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(10000);
            //verify demovalue is disabled in Identifier dropdown
            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);



            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(10000);

            //verify demovalue is disabled in Identifier dropdown
            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");

            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(30000);
            //verify demovalue is disabled in Identifier dropdown
            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);
            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);


            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(10000);
            //verify demovalue is disabled in Identifier dropdown
            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinCommentReportIdentifierdropdownwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);





            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);


            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);

            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            SuppressionActions.ExpandIdentifierdropdowninCommentReport(driver);
            Thread.Sleep(30000);
            //verify demovalue is disabled in Identifier dropdown
            IWebElement demodropdownvalue = UIActions.FindElementWithXpath(driver, " //*[@id='analyse-identifier']/option[@disabled and contains(.,'country')]");
            Assert.IsTrue(demodropdownvalue.Displayed);
            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingsSingleDistributionAnsweratleast1question()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);

            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);



            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);




            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            IWebElement vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);



            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);





            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);





            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);



            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);





            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);




            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);





            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);







            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);



            Thread.Sleep(20000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingsSingleDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);


            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();


            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);


            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);




            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);




            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);






            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);




           

            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);



            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);





            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);



            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);
            Thread.Sleep(20000);



            driver.Quit();
            Thread.Sleep(30000);
        }



        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingsMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(220000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);


            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);


            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);







            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(50000);




            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);



            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);




            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);




            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);




            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);


            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);




            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);



            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);
            Thread.Sleep(20000);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingsMultiDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(220000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);


            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();




            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);


            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);


            
            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);



            

            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);


            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);



            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);


            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);



            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);
            Thread.Sleep(20000);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingswhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);


            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);


            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
        
            Thread.Sleep(10000);




            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);


            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);





            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);


            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);



            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);



            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);



            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);





            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);


            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);
            Thread.Sleep(20000);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingswhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();


            //Thread.Sleep(50000);
            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);


            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);





            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);




            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);


            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);





            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);




            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);


            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);
            Thread.Sleep(20000);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingswhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(220000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);






            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(50000);


            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);




            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);





            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);




            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);




            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);


            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);

            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);





            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);
            Thread.Sleep(20000);



            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinAnalysisSettingswhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(220000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);



            //IWebElement vssegmenetInsightStrengthssuppressed1 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed2 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetInsightStrengthssuppressed3 = UIActions.FindElementWithXpath(driver, "  (//*[@class='comparison-lbl text-center style-scope insights-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);
            //Thread.Sleep(20000);



            //IWebElement vssegmenetDrilldownsuppressed1 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[1]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed2 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[2]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);
            //Thread.Sleep(20000);
            //IWebElement vssegmenetDrilldownsuppressed3 = UIActions.FindElementWithXpath(driver, "(//*[@class='comparison-lbl drilldown-comparison-lbl text-align-center style-scope drilldown-view']//*[contains(.,'--')])[3]");
            //Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);
            //Thread.Sleep(20000);






            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var vssegmenetInsightStrengthssuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[3] ");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed1.Displayed);



            var vssegmenetInsightStrengthssuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[4]");

            Assert.IsTrue(vssegmenetInsightStrengthssuppressed2.Displayed);


            

            var vssegmenetInsightStrengthssuppressed3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-view').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#strength-data').querySelectorAll('.padding-left-5'))[5] ");


            Assert.IsTrue(vssegmenetInsightStrengthssuppressed3.Displayed);


            var vssegmenetDrilldownsuppressed1 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[5] ");

            Assert.IsTrue(vssegmenetDrilldownsuppressed1.Displayed);

            
            var vssegmenetDrilldownsuppressed2 = (IWebElement)js.ExecuteScript("return (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[7]");

            Assert.IsTrue(vssegmenetDrilldownsuppressed2.Displayed);




            var vssegmenetDrilldownsuppressed3 = (IWebElement)js.ExecuteScript("return  (document.querySelector('drilldown-view').shadowRoot.querySelector('paper-card').querySelectorAll('.padding-left-5'))[9]");


            Assert.IsTrue(vssegmenetDrilldownsuppressed3.Displayed);





            //      IWebElement segmenetinTrendreport = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@class,'highcharts-point style-scope trend-view style-scope trend-view style-scope trend-view style-scope trend-view style-scope')])[1]");
            //   Assert.IsFalse(segmenetinTrendreport.Displayed);
            Thread.Sleep(20000);
            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]

        public void ValidatedisabledDropdownDemographicinSegmenttoWatchSingleDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 26);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);



            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");

            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(20000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(20000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(20000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(20000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);



            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmenttoWatchSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(20000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(20000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(20000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(20000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);



            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmenttoWatchMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "65");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);

            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Previous");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");

            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(20000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(20000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(20000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(20000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(30000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmenttoWatchMultiDistributionCompletedthesurvey()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "65");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(220000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "Previous");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");

            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);






            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(20000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(20000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(20000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(20000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(20000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);

        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmenttoWatchwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {






            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);







            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);

            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            //IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement firstsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(firstsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);


            //IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement secondsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(secondsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //driver.Quit();
            //Thread.Sleep(30000);




            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(10000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(10000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(10000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(10000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(10000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);

        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmenttoWatchwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {




            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "34");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(180000);







            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            //IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement firstsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(firstsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);


            //IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement secondsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(secondsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //driver.Quit();
            //Thread.Sleep(30000);






            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(20000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(10000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(10000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(10000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);



        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmenttoWatchwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {



            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "65");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);








            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "Previous");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");

            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);







            //IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement firstsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(firstsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement secondsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(secondsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //driver.Quit();
            //Thread.Sleep(30000);








            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(20000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(10000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(10000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(10000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);


        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmenttoWatchwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {




            DriverAndLogin.Account = "UAT Data Checks";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "65");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            ////      AnalyzeAction.OpenAnalyzeForm(driver, "L&D survey form");
            ////    Thread.Sleep(180000);
            //UIActions.GetElementWithWait(driver, "#searchInputAnalyseResult", 30);
            //IWebElement searchBox = driver.FindElement(By.Id("searchInputAnalyseResult"));
            //searchBox.Clear();
            //searchBox.SendKeys("L&D survey form");
            //UIActions.Click(driver, "#searchAnalyseResult");
            //Thread.Sleep(30000);
            //IWebElement distributionLink = UIActions.FindElementWithXpath(driver, "(//*[contains(@class,'navigate-to-analyze')])[2]");
            //distributionLink.Click();
            //Thread.Sleep(120000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "L&D survey form");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Current Distribution");


            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);

            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);



            AnalyzeAction.searchsegmentcategory(driver, "Previous");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Previous");

            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            //IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement firstsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(firstsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //firstsegmenttowatchclick.Click();
            //Thread.Sleep(20000);




            //IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //IWebElement secondsegmenttowatch = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            //Assert.IsTrue(secondsegmenttowatch.Displayed);
            //Thread.Sleep(20000);
            //secondsegmenttowatchclick.Click();
            //Thread.Sleep(20000);
            //driver.Quit();
            //Thread.Sleep(30000);







            IWebElement firstsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            firstsegmenttowatchclick.Click();
            Thread.Sleep(20000);

            //zipcodes will be suppressed

            IWebElement firstsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(firstsegmenttowatch1.Displayed);
            Thread.Sleep(20000);


            //Country  will be suppressed

            IWebElement firstsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='first-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(firstsegmenttowatch2.Displayed);
            Thread.Sleep(10000);


            firstsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            IWebElement secondsegmenttowatchclick = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div[contains(@class,'btn-group ceb-dropdown-container')]");
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            //zipcodes will be suppressed

            IWebElement secondsegmenttowatch1 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,' zipcodes')]");
            Assert.IsTrue(secondsegmenttowatch1.Displayed);
            Thread.Sleep(10000);




            //Country  will be suppressed

            IWebElement secondsegmenttowatch2 = UIActions.FindElementWithXpath(driver, "//*[@id='second-segment']//following-sibling::div//ul/li[@class='disabled']//label[contains(.,'Country')]");
            Assert.IsTrue(secondsegmenttowatch2.Displayed);
            Thread.Sleep(10000);



            Thread.Sleep(10000);
            secondsegmenttowatchclick.Click();
            Thread.Sleep(10000);


            driver.Quit();
            Thread.Sleep(10000);

        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);





            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
   
            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());


            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());


        

            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());



            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());


 

            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());



            var insightopportunitiesarea7 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());





            var insightopportunitiesarea8 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());




            var insightopportunitiesarea9 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());




            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());

            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());


            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            //var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            //var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingSingleDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);




            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);









            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);


            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            //var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            //var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            //var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());
            //Thread.Sleep(10000);


            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());





            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());



            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            

            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());



            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());


      

            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());



            var insightopportunitiesarea7 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            var insightopportunitiesarea8 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());



            var insightopportunitiesarea9 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);



        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(10000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(10000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(10000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(10000);


            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);


            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            ////var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            ////var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            ////var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());





            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());

            
            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());

            

            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());

            

            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            

            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());

            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea7 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea8 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea9 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());




            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingMultiDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(220000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);



            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            //IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            //Favorability.Click();
            //Thread.Sleep(50000);
            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            //var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            //var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());





            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());



            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());


            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());



            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());

            

            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea7 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea8 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea9 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());



            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingwhentimeanddemographicfilterisappliedSingleDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";


            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);




            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);


            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            //var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            //var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            //var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());






            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());




            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());



            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());



            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());


            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            var insightopportunitiesarea7 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



       

            var insightopportunitiesarea8 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());


            var insightopportunitiesarea9 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingwhentimeanddemographicfilterisappliedSingleDistributionCompletedthesurvey()
        {

            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "24");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(80000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);

            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);







            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);


            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(10000);



            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            //var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            //var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            //var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());





            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());





            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());




            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());



            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


 

            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());



            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            var insightopportunitiesarea7 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            var insightopportunitiesarea8 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());




            var insightopportunitiesarea9 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingwhentimeanddemographicfilterisappliedMultiDistributionAnsweratleast1question()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(10000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            Thread.Sleep(10000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(10000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);

            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);






            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);








            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(50000);


            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            ////var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            ////var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            ////var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());



            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());




            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());




            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());





            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());


            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());




            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea7 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea8 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea9 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());

            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        [TestMethod]
        public void ValidatedisabledDropdownDemographicinSegmentReportingwhentimeanddemographicfilterisappliedMultiDistributionCompletedthesurvey()
        {


            DriverAndLogin.Account = "Automation Suppression";
            DriverAndLogin.Browser = "Chrome";
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            SuppressionActions.NavigatetoAccountSuppression(driver);
            Thread.Sleep(20000);
            SuppressionActions.SetGroupNSizetoReportDemographics(driver, "30");
            SuppressionActions.SetDemographicValueMinimum(driver, "2");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(220000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(20000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(20000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(20000);


            //AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            //Thread.Sleep(1000);
            //AnalyzeAction.OpenAnalyzeForm(driver, "Employee Engagement Survey");
            //Thread.Sleep(180000);
            //IWebElement seconddistribution = UIActions.FindElementWithXpath(driver, "   (//*[@widget-data-event='change::OnDistributionFilterChange'])[2]");
            //seconddistribution.Click();
            //Thread.Sleep(180000);
            //SuppressionActions.SelectDateRanges(driver, "01/01/2015", "02/10/2016");
            //Thread.Sleep(80000);
            //SuppressionActions.ClickAnalyzeSettings(driver);
            //Thread.Sleep(20000);
            //SuppressionActions.Selectsegmentfromanalysesettings(driver, "country", "India");
            //SuppressionActions.ClickApplyinanalysesettings(driver);
            //Thread.Sleep(20000);



            AnalyzeAction.OpenAnalyzeFromAuthor(driver, "Employee Engagement Survey");



            Thread.Sleep(10000);

            AnalyzeAction.ClickonFiltersComparisons(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "Distribution");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Distribution");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "Group N-Size to Report Demographics2");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "Group N-Size to Report Demographics2");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);




            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);




            AnalyzeAction.searchsegmentcategory(driver, "country");


            Thread.Sleep(10000);


            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "country");

            Thread.Sleep(10000);


            AnalyzeAction.searchsegmentcategory(driver, "India");


            Thread.Sleep(10000);

            AnalyzeAction.searchsegement(driver);


            Thread.Sleep(10000);


            AnalyzeAction.ClickChip(driver, "India");
            Thread.Sleep(10000);

            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);





            AnalyzeAction.ClickAddFilter(driver);


            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "All Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.ClickChip(driver, "Custom Time Period");
            Thread.Sleep(10000);



            AnalyzeAction.CustomTimePeriod(driver, "");

            Thread.Sleep(10000);


            AnalyzeAction.CustomTimePeriod(driver, "01/01/2015 - 02/10/2016");

            Thread.Sleep(10000);

            AnalyzeAction.OK(driver);
            Thread.Sleep(10000);


            AnalyzeAction.Apply(driver);

            Thread.Sleep(10000);



            IWebElement Favorability = UIActions.FindElementWithXpath(driver, " //*[@id='analyseTabs']/li/a[text()='Favorability']");
            Favorability.Click();
            Thread.Sleep(50000);


            //var insightopportunitiesarea1 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());
            //Thread.Sleep(20000);
            //var insightopportunitiesarea2 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());
            //Thread.Sleep(20000);
            //var insightopportunitiesarea3 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[1]");
            //Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());
            //Thread.Sleep(20000);
            //var insightopportunitiesarea4 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());
            //Thread.Sleep(20000);
            //var insightopportunitiesarea5 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());
            //Thread.Sleep(20000);
            //var insightopportunitiesarea6 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[2]");
            //Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());
            ////var insightopportunitiesarea7 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());
            ////var insightopportunitiesarea8 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-85 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());
            ////var insightopportunitiesarea9 = UIActions.FindElementWithXpath(driver, "(//*[@class='pointer wrap segmentvalue width-80 pull-right style-scope insights-opportunity tooltip-results style-scope insights-opportunity'])[3]");
            ////Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());





            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            var insightopportunitiesarea1 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[0]");




            Assert.AreNotEqual("country", insightopportunitiesarea1.Text.Trim());


            var insightopportunitiesarea2 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[1]");




            Assert.AreNotEqual("country", insightopportunitiesarea2.Text.Trim());



            var insightopportunitiesarea3 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[2]");




            Assert.AreNotEqual("country", insightopportunitiesarea3.Text.Trim());





            var insightopportunitiesarea4 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[3]");




            Assert.AreNotEqual("country", insightopportunitiesarea4.Text.Trim());



            var insightopportunitiesarea5 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[4]");




            Assert.AreNotEqual("country", insightopportunitiesarea5.Text.Trim());




            var insightopportunitiesarea6 = (IWebElement)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[5]");




            Assert.AreNotEqual("country", insightopportunitiesarea6.Text.Trim());


            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea7 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[6]");




            //Assert.AreNotEqual("country", insightopportunitiesarea7.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea8 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[7]");




            //Assert.AreNotEqual("country", insightopportunitiesarea8.Text.Trim());



            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            //var insightopportunitiesarea9 = (bool)js.ExecuteScript("return (document.querySelector('insights-opportunity').shadowRoot.querySelector('paper-card').querySelector('#insightStrengthContainer').querySelector('#opportunity-data').querySelectorAll('span[widget-data-event='click::OpenDrillDownViewFromSegment']'))[8]");




            //Assert.AreNotEqual("country", insightopportunitiesarea9.Text.Trim());


            Thread.Sleep(10000);
            driver.Quit();
            Thread.Sleep(10000);
        }
        //[TestMethod]
        //public void extractzipfile()
        //{
        //    IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
        //    Thread.Sleep(10000);
        //    WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
        //    Thread.Sleep(10000);
        //    AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
        //    Thread.Sleep(1000);
        //    AnalyzeAction.OpenAnalyzeForm(driver, "EI73SURVEY");
        //    Thread.Sleep(180000);
        //    IWebElement Export = UIActions.FindElementWithXpath(driver, "  //*[@class='btn btn-primary dropdown-toggle']");
        //    Export.Click();
        //    Thread.Sleep(20000);
        //    IWebElement RawData = UIActions.FindElementWithXpath(driver, " //*[@id='btnExportData']");
        //    RawData.Click();
        //    Thread.Sleep(180000);
        //    IWebElement RawDatalink = UIActions.FindElementWithXpath(driver, "//*[@class='RawData-export-link']");
        //    RawDatalink.Click();
        //    Thread.Sleep(50000);
        //    string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
        //    UIActions.extractZipFile(latestfile, DriverAndLogin.downloadFilepath);
        //}
        //[ClassCleanup()]
        //public static void CleanUp()
        //{
        //    RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
        //    if (driver != null)
        //    {
        //        driver.Close();
        //        driver.Dispose();
        //    }
        //}
    }
}