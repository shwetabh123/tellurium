﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using System.Data;
using System.Windows.Forms;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Text.RegularExpressions;

namespace Driver
{
    [TestClass]
    public class BenchMarkTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;
        private static string FormName;
        public static string FileName;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string SPColor;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),

            };

            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
            SPColor = context.Properties["SPColor"].ToString();

        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToHome(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            else
                AuthorMSTests.NavigateToHome(driver);
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToManageForms(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
            }
            else
            {
                AuthorActions.NavigateToAuthor(driver);
            }
            return driver;
        }

        //[TestMethod]
        //public void VerifyBenchMarkInSettings()
        //{
        //    string benchmarkName = null;
        //    RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
        //    AuthorActions.NavToAccountSettings(driver);
        //    Thread.Sleep(6000);
        //    AuthorActions.NavToLibAccess(driver);
        //    AuthorActions.NavToBenchMark(driver);
        //    List<string> benchmarkNames = new List<string>();
        //    IReadOnlyCollection<IWebElement> row = driver.FindElements(By.XPath("//div[@id='benchmarks-table_wrapper']//tr[@id='[object Object]']"));
        //    for(int i=1; i<= row.Count(); i++)
        //    {
        //        //bool isSelect = r.FindElement(By.CssSelector("#benchmarks-table tbody tr td")).Selected;

        //        bool isSelect1 = driver.FindElement(By.XPath("//div[@id='benchmarks-table_wrapper']//tr[@id='[object Object]']["+i+"]/td[1]/input")).Selected;
        //        benchmarkName = driver.FindElement(By.XPath("//div[@id='benchmarks-table_wrapper']//tr[@id='[object Object]'][" + i + "]/td[2]/span")).Text;
        //        benchmarkNames.Add(benchmarkName);
        //    }
        //    Console.WriteLine(benchmarkNames[1]);
        //    driver.Close();

        //}

        [TestMethod]
        public void VerifyVsBMInAnalysisSettings()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            AnalyzeAction.ChooseDistributionToAnalyze(driver, "Employee engagement survey results analysis");
            // AnalyzeAction.NavToResults(driver);
            AnalyzeAction.UserAnalysisSettings(driver);
            Thread.Sleep(3000);
            var dropdownValue = AnalyzeAction.AnalysisSettingsVsBM(driver);
            //var compareList =  AnalyzeAction.GetListOfBenchMark();
            List<string> list2 = new List<string>() { "BENCHMARK-EES-2017" };
            var list1 = dropdownValue.Except(list2).ToList();
            bool compareResults = (!list1.Any() ? true : false);
            //bool compareResults = dropdownValue.Equals("BENCHMARK-EES-2017");
            Console.WriteLine(compareResults);
        }

        [TestMethod]
        public void VerifyVsBMInAnalysisSettingsWithZeroItemMapping()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            AnalyzeAction.ChooseDistributionToAnalyze(driver, "Data Checks MH Group 1");
            AnalyzeAction.UserAnalysisSettings(driver);
            IWebElement chk = driver.FindElement(By.Id("chkBenchmark"));
            bool _vsBenchmarkDisplayed = chk.Displayed ? true : false;
            Assert.IsFalse(_vsBenchmarkDisplayed);
        }
          

        
        [TestMethod]
        public void validatebenchmarktempalte()
        {
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            BenchmarkActions.NavigateToBenchmark(CurrentDriver);
            Thread.Sleep(4000);
            String[] SurveyName = { " Automation Benchmark2" };
            BenchmarkActions.Downloadbenchmarktemplate(CurrentDriver, SurveyName);
            Thread.Sleep(5000);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Thread.Sleep(2000);
            bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\BenchmarkTemplate_verification.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            Assert.IsTrue(areExcelsIdentical);
            CurrentDriver.Quit();
        }

        [TestMethod]
        public void validateuploadbenchmark()
        {
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            // DistributeActions.UploadFileMthd();
            BenchmarkActions.NavigateToBenchmark(CurrentDriver);
            Thread.Sleep(2000);
            Assert.IsTrue(BenchmarkActions.uploadbenchmark(CurrentDriver, "BenchmarkTemplate_upload.xlsx", "BenchmarkTemplate_upload.xlsx"));
            CurrentDriver.Quit();
        }

        [TestMethod]

        public void ValidateBenchmarkuploaderrorsandedits()
        {

            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            BenchmarkActions.NavigateToBenchmark(CurrentDriver);
            Thread.Sleep(4000);
            BenchmarkActions.uploadbenchmark(CurrentDriver, "BenchmarkTemplate_errors.xlsx", "BenchmarkTemplate_errors.xlsx");
            Thread.Sleep(2000);
            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Thread.Sleep(2000);
            bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\benchmarkErrorsexport.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            Assert.IsTrue(areExcelsIdentical);
            CurrentDriver.Quit();
        }
        [TestMethod]
        public void Validatebenchmarkpresent()
        {
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            BenchmarkActions.NavigateToBenchmark(CurrentDriver);
            Thread.Sleep(4000);
            string[] BenchmarkName = { "India-398", "India-Sales", "India-IT", "Technology-India-IT", "Technology-Sales", "nonratingbenchadded", "ratingbenchmarkadded" };
            BenchmarkActions.Verifybenchmarkname(CurrentDriver, BenchmarkName);
            CurrentDriver.Quit();
        }

        [TestMethod]

        public void ValidateExportMultiplebenchmarkusingselectedbtn()
        {
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            BenchmarkActions.NavigateToBenchmark(CurrentDriver);

            var lastfile = DistributeActions.latestfile;
            BenchmarkActions.exportbenchmark(CurrentDriver, "Technology-India", "Multiple");

            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Thread.Sleep(2000);

            latestfile = DistributeActions.latestfile;
            Assert.AreNotEqual(lastfile, latestfile);
            CurrentDriver.Quit();
        }
        [TestMethod]
        public void Validatesingleexportbenchmarkusingexportbtn()
        {
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            BenchmarkActions.NavigateToBenchmark(CurrentDriver);

            var lastfile = DistributeActions.latestfile;
            BenchmarkActions.exportbenchmark(CurrentDriver, "Technology-India", null);

            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Thread.Sleep(2000);

            latestfile = DistributeActions.latestfile;
            Assert.AreNotEqual(lastfile, latestfile);
            CurrentDriver.Quit();


        }
        [TestMethod]

        public void ValidateBenchmarkExportwithUIedit()
        {

            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            BenchmarkActions.NavigateToBenchmark(CurrentDriver);
            BenchmarkActions.EditBenchmark(CurrentDriver, "Technology-India-IT", "Retail", null, "ITnew", null);


            Thread.Sleep(4000);

            UIActions.clickwithXapth(CurrentDriver, "//*[@id='btnSearch']");
            BenchmarkActions.exportbenchmark(CurrentDriver, "Technology-India-IT", null);
            Thread.Sleep(2000);

            string applicationPath = CommonActions.GetApplicationPath();
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            Thread.Sleep(2000);
            bool areExcelsIdentical = CommonActions.CompareExcels(applicationPath + @"DataSet\Benchmarkexportcompare.xlsx", DriverAndLogin.downloadFilepath + @"\" + latestfile, DriverAndLogin.downloadFilepath, true);
            Assert.IsTrue(areExcelsIdentical);
            CurrentDriver.Quit();
        }

        [TestMethod]

        public void ValidateBenchmarkeditname()
        {

            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);

            BenchmarkActions.NavigateToBenchmark(CurrentDriver);
            Thread.Sleep(2000);
            BenchmarkActions.EditBenchmark(CurrentDriver, "Technology-India-IT", "Retail", null, "ITnew", "benchmarkname");

            Thread.Sleep(4000);
            string[] BenchmarkName = { "benchmarkname" };
            BenchmarkActions.Verifybenchmarkname(CurrentDriver, BenchmarkName);
            Thread.Sleep(4000);

            BenchmarkActions.EditBenchmark(CurrentDriver, "benchmarkname", null, null, null, "Benchmarkname#@@%%");
            Thread.Sleep(4000);

            string[] BenchmarkName1 = { "Benchmarkname#@@%%" };
            BenchmarkActions.Verifybenchmarkname(CurrentDriver, BenchmarkName1);
            Thread.Sleep(4000);


            BenchmarkActions.EditBenchmark(CurrentDriver, "Benchmarkname#@@%%", "Technology", "India", "IT", "Technology-India-IT");
            Thread.Sleep(4000);
            CurrentDriver.Quit();
        }

        [TestMethod]
        public void createratingitemwithbenchmarkmap()
        {
            DriverAndLogin.Account = "Automation kkvp";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");
            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            string[] scaleHeaders = { "One", "Two", "Three", "Four", "Five", "Six" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below new automation frameworks", questions, "1-6", scaleHeaders);
            Thread.Sleep(5000);
            AuthorActions.AddBenchmarkMappingMultiRatingScale(driver, "Rate the below new automation frameworks", " ​​Retirement Benefits", new List<string> { " My retirement benefits (401(k) and pensi...", " My retirement savings plan includes mone...", " I have a good understanding of our pensi..." });
            Thread.Sleep(3000);
            driver.Quit();
        }
        [TestMethod]
        public void verifypopupconfirm_editratingitemtext_cancel()
        {

            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            AuthorActions.NavigateToAuthor(CurrentDriver);
            AuthorActions.SearchAndOpenForm(CurrentDriver, "BenchmarkAutomation");
            BenchmarkActions.Editratingitemaction(CurrentDriver, "Rate the below new automation frameworks", "new selenium java", null);
            UIActions.GetElementswithXpath(CurrentDriver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);

            UIActions.GetElementswithXpath(CurrentDriver, "//*[@id='benchmarkItemCancelMessage']", 45);

            UIActions.clickwithXapth(CurrentDriver, "//*[@id='btnBenchmarkQuestionConfirmCancel']");
            CurrentDriver.Quit();
        }
        [TestMethod]
        public void verifypopupconfirm_editratingscaleoption_cancel()
        {
            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);
            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            AuthorActions.NavigateToAuthor(CurrentDriver);
            AuthorActions.SearchAndOpenForm(CurrentDriver, "BenchmarkAutomation");
            BenchmarkActions.Editratingitemaction(CurrentDriver, "Rate the below new automation frameworks", "scalechange");
            UIActions.GetElementswithXpath(CurrentDriver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);

            UIActions.GetElementswithXpath(CurrentDriver, "//*[@id='benchmarkItemCancelMessage']", 45);

            UIActions.clickwithXapth(CurrentDriver, "//*[@id='btnBenchmarkQuestionConfirmCancel']");
            CurrentDriver.Quit();
        }



        [TestMethod]
        public void ratingbenchmarkrentention_edititem_Yesoption()
        {

            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            AuthorActions.NavigateToAuthor(CurrentDriver);
            AuthorActions.SearchAndOpenForm(CurrentDriver, "BenchmarkAutomation");
            BenchmarkActions.Editratingitemaction(CurrentDriver, "Rate the below new automation frameworks", "new selenium java", null);
            Thread.Sleep(3000);
            UIActions.GetElementswithXpath(CurrentDriver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);
            UIActions.clickwithXapth(CurrentDriver, "//*[@id='btnBenchmarkQuestionConfirmYes']");
            Thread.Sleep(8000);
            BenchmarkActions.checkbenchmarkmapping(CurrentDriver, "Rate the below new automation frameworks", "new selenium java", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.");
            CurrentDriver.Quit();
        }

        [TestMethod]

        public void benchmarkrentention_editscale_Yesoption()
        {

            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            AuthorActions.NavigateToAuthor(CurrentDriver);
            AuthorActions.SearchAndOpenForm(CurrentDriver, "BenchmarkAutomation");
            BenchmarkActions.Editratingitemaction(CurrentDriver, "Rate the below new automation frameworks", null, "test");
            Thread.Sleep(3000);
            UIActions.GetElementswithXpath(CurrentDriver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);
            UIActions.clickwithXapth(CurrentDriver, "//*[@id='btnBenchmarkQuestionConfirmYes']");
            Thread.Sleep(6000);
            UIActions.clickwithXapth(CurrentDriver, "(//button[@class='btn btn-secondary btnNo'])[6]");
            Thread.Sleep(6000);
            BenchmarkActions.checkbenchmarkmapping(CurrentDriver, "Rate the below new automation frameworks", "new selenium java", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.");
            CurrentDriver.Quit();
        }

        [TestMethod]
        public void verifyremovalofbenchmark_ratingitem_nooption()
        {

            CurrentDriver = WebDriverFactory.GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url, DriverAndLogin.downloadFilepath);

            WebDriverFactory.Login(CurrentDriver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(3000);
            AuthorActions.NavigateToAuthor(CurrentDriver);
            AuthorActions.SearchAndOpenForm(CurrentDriver, "BenchmarkAutomation");
            BenchmarkActions.Editratingitemaction(CurrentDriver, "Rate the below new automation frameworks", "new selenium", null);
            Thread.Sleep(3000);
            UIActions.GetElementswithXpath(CurrentDriver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);
            UIActions.clickwithXapth(CurrentDriver, "//*[@id='btnBenchmarkQuestionConfirmNo']");
            Thread.Sleep(6000);
            BenchmarkActions.checkbenchmarkmapping(CurrentDriver, "Rate the below new automation frameworks", "new selenium", "Please Select");
            CurrentDriver.Quit();
        }

        [TestMethod]
        public void verifyremovalofbenchmark_ratingscale_nooption()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);

            Thread.Sleep(3000);
            //AuthorActions.NavigateToAuthor(CurrentDriver);
            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");
            AuthorActions.AddBenchmarkMappingMultiRatingScale(driver, "Rate the below new automation frameworks", " ​​Retirement Benefits", new List<string> { " My retirement benefits (401(k) and pensi...", " My retirement savings plan includes mone...", " I have a good understanding of our pensi..." });
            //    createratingitemwithbenchmarkmap();
            Thread.Sleep(6000);
            BenchmarkActions.Editratingitemaction(driver, "Rate the below new automation frameworks", null, "test");
            Thread.Sleep(3000);
            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);
            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkQuestionConfirmNo']");
            Thread.Sleep(8000);
            BenchmarkActions.Nobenchmarkmapping(driver, "Rate the below new automation frameworks");
            Thread.Sleep(10000);
            UIActions.clickwithXapth(CurrentDriver, "(//button[@class='btn btn-secondary btnNo'])[6]");
            Thread.Sleep(30000);
            UIActions.clickwithXapth(driver, "//span[contains(.,'Rate the below new automation frameworks')]/..//following-sibling::*//span[@class='btn ceb-icon_delete2_knockout action-question-delete secondary font-sz-23']");
            Thread.Sleep(2000);
            UIActions.clickwithXapth(driver, "//*[@id='btnConfirmDeleteSectionClicked']");
            driver.Quit();
        }

        [TestMethod]
        public void createnonratingitemwithbenchmark()

        {
            DriverAndLogin.Account = "Automation kkvp";

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);

            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");

            Thread.Sleep(3000);
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions);
            Thread.Sleep(6000);
            AuthorActions.AddBenchmarkMappingSingleMultiChoice(driver, "Choose you Department", " How old are you?", new List<string> { "26-35 ", " 36-45", " 46-55", " 25 and under" });
            driver.Quit();
        }

        [TestMethod]
        public void verifybenchmarkretention_nonratingitemedit_canceloption()
        {
            DriverAndLogin.Account = "Automation kkvp";
            //click on cancel button in please confirm popup
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(9000);
            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");
            Thread.Sleep(5000);
            BenchmarkActions.Editnonratingitemaction(driver, "Choose you Department", "Department", null);
            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);

            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkItemCancelMessage']", 45);

            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkQuestionConfirmCancel']");
            Thread.Sleep(8000);

            BenchmarkActions.checkbenchmarkmapping(driver, "Choose you Department", "HR", "26-35");
            driver.Quit();
        }

        [TestMethod]
        public void verifybenchmarkrention_nonratingscaleedit_canceloption()
        {
            DriverAndLogin.Account = "Automation kkvp";
            //click on cancel button in please confirm popup
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(3000);

            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");


            BenchmarkActions.Editnonratingitemaction(driver, "Choose you Department", null, "HR");
            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);

            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkQuestionConfirmCancel']");
            Thread.Sleep(8000);

            BenchmarkActions.checkbenchmarkmapping(driver, "Choose you Department", "HR", "26-35");
            driver.Quit();
        }


        [TestMethod]
        public void verifybenchmarkrention_nonratingscaleedit_yesoption()
        {
            DriverAndLogin.Account = "Automation kkvp";
            //click on cancel button in please confirm popup
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(3000);

            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");


            BenchmarkActions.Editnonratingitemaction(driver, "Choose you Department", null, "test");
            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);

            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkQuestionConfirmYes']");
            Thread.Sleep(6000);
            UIActions.clickwithXapth(driver, "(//button[@class='btn btn-secondary btnNo'])[6]");
            Thread.Sleep(8000);
            BenchmarkActions.checkbenchmarkmapping(driver, "Choose you Department", "test", "26-35");
            driver.Quit();
        }




        [TestMethod]
        public void verifybenchmarkretention_nonratingitemedit_yesoption()
        {
            DriverAndLogin.Account = "Automation kkvp";
            //click on cancel button in please confirm popup
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(5000);

            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");

            BenchmarkActions.Editnonratingitemaction(driver, "Choose you Department", "Department", null,null);
            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);

            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkQuestionConfirmYes']");
            Thread.Sleep(8000);

            BenchmarkActions.checkbenchmarkmapping(driver, "Department", "test", "26-35");
            driver.Quit();
        }

        [TestMethod]
        public void verifyremovalofbenchmark_nonratingitem_nooption()
        {
            DriverAndLogin.Account = "Automation kkvp";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(3000);

            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");
            Thread.Sleep(7000);
            BenchmarkActions.Editnonratingitemaction(driver, "Department", "new department", null);
            Thread.Sleep(5000);
            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);
            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkQuestionConfirmNo']");
            Thread.Sleep(7000);
            BenchmarkActions.Nobenchmarkmapping(driver, "new department");
            driver.Quit();
        }

        [TestMethod]
        public void verifyremovalofbenchmark_nonratingscale_nooption()
        {
            DriverAndLogin.Account = "Automation kkvp";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);

            Thread.Sleep(3000);
            AuthorActions.SearchAndOpenForm(driver, "BenchmarkAutomation");
            AuthorActions.AddBenchmarkMappingSingleMultiChoice(driver, "new department", " How old are you?", new List<string> { "26-35 ", " 36-45", " 46-55", " 25 and under" });

            Thread.Sleep(5000);
            BenchmarkActions.Editnonratingitemaction(driver, "new department", null, "test","test2");
            Thread.Sleep(8000);
            UIActions.GetElementswithXpath(driver, "//*[@id='benchmarkedQuestionEditConfirmationModal']//h4[contains(.,'Please Confirm')]", 45);
            Thread.Sleep(8000);
            UIActions.clickwithXapth(driver, "//*[@id='btnBenchmarkQuestionConfirmNo']");
            Thread.Sleep(3000);
            BenchmarkActions.Nobenchmarkmapping(driver, "new department");
            Thread.Sleep(3000);
            UIActions.clickwithXapth(driver, "//span[contains(.,'new department')]/..//following-sibling::*//span[@class='btn ceb-icon_delete2_knockout action-question-delete secondary font-sz-23']");
            Thread.Sleep(5000);
            UIActions.clickwithXapth(driver, "//*[@id='btnConfirmDeleteSectionClicked']");
            Thread.Sleep(4000);
            createnonratingitemwithbenchmark();
        }

        [TestMethod]
        public  void executespotlight()
        {
            Spotlightreports.Spotlightfilters("5001", "DV", DBConnectionParameters,SPColor);

            

        }
        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }

        }

    }
}





