﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Driver
{


    [TestClass]
    public class TRReports
    {





        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DBConnectionStringDTO DBConnectionParameters1;


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString(),
            };
            //DBConnectionParameters = new DBConnectionStringDTO
            //{
            //    userName = "pulseqa",
            //    password = "password-1",
            //    TCESserverName = "USA-QIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-QIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            //DBConnectionParameters1 = new DBConnectionStringDTO
            //{
            //    userName = "pulsestg",
            //    password = "password-1",
            //    TCESserverName = "USA-SIN-PLSP-01,1605",
            //    TCESDB = "TCES",
            //    TCESReportingserverName = "USA-SIN-PLSR-01,1605",
            //    TCESReportingDB = "TCESReporting",
            //    IntegratedSecurity = "SSPI",
            //    PersistSecurityInfo = "False"
            //};
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString(),
                IntegratedSecurity = context.Properties["IntegratedSecurity"].ToString(),
                PersistSecurityInfo = context.Properties["PersistSecurityInfo"].ToString()
            };
        }
        //shwetabh srivastava--------added capabilities for download file in specified location at run time
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;
            switch (driverName)
            {
                case "Firefox":
                    // FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    //     service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    //   driver = new FirefoxDriver();
                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);
                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "Chrome":
                default:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    driver = new ChromeDriver(chromeOptions);
                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }



        //Scenario 1


        [TestMethod]
        public void Validate_TR_TargetGroup_Slide2()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5025", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "1");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 2---FilterNSize");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);



            driver.Quit();




        }


        //Scenario 1


        [TestMethod]
        public void Validate_TR_TargetGroup_Slide3()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5025", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "1");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();


        }




        //Scenario 1


        [TestMethod]
        public void Validate_TR_TargetGroup_Slide4()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5025", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "1");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();
        }


        //Scenario 1




        [TestMethod]
        public void Validate_TR_TargetGroup_Slide5()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5025", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "1");


            //Slide 5 Comparison//

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();
        }



        //Scenario 1

        [TestMethod]
        public void Validate_TR_TargetGroup_Slide6()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5025", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "1");


            //Slide 6 Comparison//

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();

        }


        //Scenario 1


        [TestMethod]
        public void Validate_TR_TargetGroup_Slide7()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 1, 1, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5025", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "1");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 1---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);

            driver.Quit();

        }

        //Scenario 2


        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Slide2()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5026", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "2");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 1, sheetname2);



            driver.Quit();




        }

        //Scenario 2


        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Slide3()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5026", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "2");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 19, 19, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();




        }



        //Scenario 2


        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Slide4()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5026", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "2");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();




        }


        //Scenario 2


        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Slide5()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5026", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "2");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();




        }

        //Scenario 2

        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Slide6()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5026", "TR", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "2");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();



        }

        //Scenario 2


        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Slide7()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 2, 2, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5026", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "2");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

     

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 29, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 47, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 2---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 29, 31, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 47, 49, 1, 3, sheetname2);





            driver.Quit();



        }




        //Scenario 3


        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Slide2()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5027", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "3");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);



            driver.Quit();




        }


        //Scenario 3


        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Slide3()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5027", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "3");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");





            //Physical Wellbeing 
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");





            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);


       
            //Financial Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 4, 4, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();



        }




        //Scenario 3


        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Slide4()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5027", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "3");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();

        }


        //Scenario 3


        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Slide5()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5027", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "3");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();




        }


        //Scenario 3

        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Slide6()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5027", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "3");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();
        }



        //Scenario 3

        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Slide7()


        {





            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 3, 3, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5027", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "3");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 3---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 1, 3, sheetname2);

            driver.Quit();

        }



        //Scenario 4


        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Slide2()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5028", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "4");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);



            driver.Quit();




        }


        //Scenario 4


        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Slide3()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5028", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "4");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Physical Wellbeing
   
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);

            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");





            //Physical Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);

            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 4, 4, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();





        }



        //Scenario 4


        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Slide4()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5028", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "4");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();
        }


        //Scenario 4



        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Slide5()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);


            string InputSPText1 = Spotlightreports.Spotlightfilters("5028", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "4");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4 --Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();

        }

        //Scenario 4


        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Slide6()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5028", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "4");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();




        }

        //Scenario 4


        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Slide7()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);

            string InputSPText1 = Spotlightreports.Spotlightfilters("5028", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "4");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 4---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 1, 3, sheetname2);

            driver.Quit();


        }


        //Scenario 5


        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Slide2()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5029", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "5");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);



            driver.Quit();




        }

        //Scenario 5

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Slide3()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5029", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "5");

            //Slide 3 Comparison//

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 9, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 21, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();




        }


        //Scenario 5


        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Slide4()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 4, 4, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5029", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "5");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();

        }

        //Scenario 5


        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Slide5()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5029", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "5");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5 --Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();
        }


        //Scenario 5
        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5029", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "5");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();

        }

        //Scenario 5

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Slide7()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 5, 5, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5029", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "5");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 5---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 1, 3, sheetname2);

            driver.Quit();

        }


        //Scenario 6


        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Slide2()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5030", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "6");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);



            driver.Quit();





        }



        //Scenario 6

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Slide3()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5030", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "6");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6--------Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 13, 13, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();




        }


        //Scenario 6




        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Slide4()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5030", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "6");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();



        }



        //Scenario 6

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Slide5()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5030", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "6");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6 --Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();

        }


        //Scenario 6

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5030", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "6");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();


        }


        //Scenario 6

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Slide7()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 6, 6, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5030", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "6");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 6---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 1, 3, sheetname2);

            driver.Quit();

        }



        //Scenario 7


        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Slide2()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5031", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "7");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 0, sheetname2);



            driver.Quit();




        }



        //Scenario 7

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Slide3()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5031", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "7");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();


        }


        //Scenario 7




        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Slide4()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5031", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "7");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();


        }



        //Scenario 7

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Slide5()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5031", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "7");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7 --Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();

        }


        //Scenario 7

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Slide6()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5031", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "7");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();
        }


        //Scenario 7

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Slide7()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 7, 7, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5031", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "7");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 7---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 1, 3, sheetname2);

            driver.Quit();

        }





        //Scenario 8


        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Slide2()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5032", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "8");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 1, sheetname2);



            driver.Quit();



        }



        //Scenario 8

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Slide3()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5032", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "8");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();



        }


        //Scenario 8




        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Slide4()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8 ,8, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5032", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "8");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();





        }



        //Scenario 8

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Slide5()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5032", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "8");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8 --Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();

        }


        //Scenario 8

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Slide6()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5032", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "8");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();
        }


        //Scenario 8

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Slide7()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 8, 8, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5032", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "8");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 8---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);

            driver.Quit();

        }



        //Scenario 9


        [TestMethod]
        public void Validate_TR_TargetGroup_Comparison_Benchmark_Slide2()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5033", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "9");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);



            driver.Quit();




        }



        //Scenario 9

        [TestMethod]
        public void Validate_TR_TargetGroup_Comparison_Benchmark_Slide3()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5033", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "9");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            //Physical Wellbeing



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);
            //Financial Wellbeing



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();


        }


        //Scenario 9




        [TestMethod]
        public void Validate_TR_TargetGroup_Comparison_Benchmark_Slide4()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5033", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "9");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();


        }



        //Scenario 9

        [TestMethod]
        public void Validate_TR_TargetGroup_Comparison_Benchmark_Slide5()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5033", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "9");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9 --Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();


        }


        //Scenario 9

        [TestMethod]
        public void Validate_TR_TargetGroup_Comparison_Benchmark_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5033", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "9");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();

        }


        //Scenario 9

        [TestMethod]
        public void Validate_TR_TargetGroup_Comparison_Benchmark_Slide7()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 9, 9, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5033", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "9");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 9---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);

            driver.Quit();



        }


        //Scenario 10


        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Comparison_Slide2()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5035", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "10");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 4, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 4, sheetname2);



            driver.Quit();





        }



        //Scenario 10

        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Comparison_Slide3()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5035", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "10");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            //Financial Wellbeing

   

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);
            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);
           

            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 9, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 21, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 9, 4, 5, sheetname2);




            driver.Quit();




        }


        //Scenario 10




        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Comparison_Slide4()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5035", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "10");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();



        }



        //Scenario 10

        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Comparison_Slide5()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5035", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "10");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10--Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();


        }


        //Scenario 10

        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Comparison_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10, 2, 2, sheetname1);








            string InputSPText1 = Spotlightreports.Spotlightfilters("5035", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "10");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();
        }


        //Scenario 10

        [TestMethod]
        public void Validate_TR_Targetgroup_demofilter_Comparison_Slide7()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 10, 10, 2, 2, sheetname1);






            string InputSPText1 = Spotlightreports.Spotlightfilters("5035", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "10");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 10---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);

            driver.Quit();



        }



        //Scenario 11


        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Comparison_Slide2()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);






            string InputSPText1 = Spotlightreports.Spotlightfilters("5037", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "11");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);



            driver.Quit();



        }



        //Scenario 11

        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Comparison_Slide3()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);






            string InputSPText1 = Spotlightreports.Spotlightfilters("5037", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "11");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            
            //Physical Wellbeing



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);
            //Financial Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);

            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 4, 4, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 24, 24, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 4, 5, sheetname2);


            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 23, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 4, 5, sheetname2);


            //Emotional Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 25, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 4, 5, sheetname2);




            driver.Quit();



        }


        //Scenario 11




        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Comparison_Slide4()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5037", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "11");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();




        }



        //Scenario 11

        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Comparison_Slide5()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5037", "TR", DBConnectionParameters, "Green");
            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "11");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11--Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();








        }


        //Scenario 11

        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Comparison_Slide6()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5037", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "11");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();



        }


        //Scenario 11

        [TestMethod]
        public void Validate_TR_Targetgroup_distributionfilter_Comparison_Slide7()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 11, 11, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5037", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "11");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 11---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);

            driver.Quit();


        }



        //Scenario 12


        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Comparison_Slide2()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5039", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "12");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "************************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);



            driver.Quit();




        }



        //Scenario 12

        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Comparison_Slide3()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5039", "TR", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "12");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Physical Wellbeing



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);
            //Financial Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);

            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 4, 4, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 24, 24, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 4, 5, sheetname2);


            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 23, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 4, 5, sheetname2);


            //Emotional Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 25, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 4, 5, sheetname2);




            driver.Quit();



        }


        //Scenario 12




        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Comparison_Slide4()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5039", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "12");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();




        }



        //Scenario 12

        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Comparison_Slide5()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5039", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "12");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12--Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();

        }


        //Scenario 12

        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Comparison_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5039", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "12");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();


        }


        //Scenario 12

        [TestMethod]
        public void Validate_TR_Targetgroup_customdaterange_Comparison_Slide7()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 12, 12, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5039", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "12");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 12---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 31, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 49, 1, 3, sheetname2);

            driver.Quit();


        }



        //Scenario 13


        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Comparison_Slide2()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5041", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "13");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "************************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 3, sheetname2);



            driver.Quit();





        }



        //Scenario 13

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Comparison_Slide3()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5041", "TR", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "13");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);
 


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

       

            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 4, 4, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);
            //Physical Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");


            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 23, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 4, 5, sheetname2);


            //Physical Wellbeing
    
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 24, 24, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 4, 5, sheetname2);


            //Emotional Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 25, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 4, 5, sheetname2);




            driver.Quit();

        }


        //Scenario 13




        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Comparison_Slide4()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5041", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "13");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();



        }



        //Scenario 13

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Comparison_Slide5()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5041", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "13");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13--Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();



        }


        //Scenario 13

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Comparison_Slide6()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5041", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "13");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();


        }


        //Scenario 13

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_customdaterange_Comparison_Slide7()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 13, 13, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5041", "TR", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "13");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 29, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 47, 0, 0, sheetname2);



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 31, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 49, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 13---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 29, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 47, 1, 3, sheetname2);

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 31, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 49, 49, 0, 0, sheetname2);




            driver.Quit();



        }




        //Scenario 14


        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide2()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5043", "TR", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "14");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "************************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 4, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 4, sheetname2);



            driver.Quit();





        }



        //Scenario 14

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide3()


        {





            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5043", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "14");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Financial Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);



            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 6, 6, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);
            
            //Physical Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 13, 13, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");


            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 23, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 4, 5, sheetname2);


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 24, 24, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 4, 5, sheetname2);


            //Emotional Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 25, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 4, 5, sheetname2);




            driver.Quit();




        }


        //Scenario 14




        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide4()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5043", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "14");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();

        }



        //Scenario 14

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide5()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);





            string InputSPText1 = Spotlightreports.Spotlightfilters("5043", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "14");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14--Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();

        }


        //Scenario 14

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5043", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "14");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();



        }


        //Scenario 14

        [TestMethod]
        public void Validate_TR_TargetGroup_demo_datedemo_customdaterange_Comparison_Slide7()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 14, 14, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5043", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "14");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 31, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 49, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 14---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 31, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 49, 49, 0, 0, sheetname2);




            driver.Quit();


        }




        //Scenario 15


        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide2()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5045", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "15");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "************************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 1, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 2, sheetname2);



            driver.Quit();










        }



        //Scenario 15

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide3()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5045", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "15");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");




            //Physical Wellbeing
 


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);

            //Financial Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");


            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 10, 10, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);

            //Financial Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 4, 4, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 20, 20, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 24, 24, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 4, 5, sheetname2);


            //Financial Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 23, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 4, 5, sheetname2);


            //Emotional Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 25, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 4, 5, sheetname2);




            driver.Quit();



        }


        //Scenario 15




        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide4()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5045", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "15");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();

        }



        //Scenario 15

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide5()


        {




            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5045", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "15");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15--Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();


        }


        //Scenario 15

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5045", "TR", DBConnectionParameters, "Green");

            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "15");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();



        }


        //Scenario 15

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_timeperiod_customdaterange_Comparison_Slide7()


        {
            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 15, 15, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5045", "TR", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "15");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 31, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 49, 49, 0, 0, sheetname2);



            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 15---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 31, 31, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 49, 49, 0, 0, sheetname2);




            driver.Quit();




        }





        //Scenario 16


        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide2()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5047", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "16");

            //Slide 2 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "************************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 1, 1, 0, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 1, 1, 0, 3, sheetname2);



            driver.Quit();






        }



        //Scenario 16

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide3()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5047", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "16");

            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 3---Metrics name & Metrics value");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");



            //Financial Wellbeing



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 33, 33, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 0, 1, sheetname2);


            //Physical Wellbeing
            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 34, 34, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 0, 1, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 35, 35, 1, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 0, 1, sheetname2);





            //Slide 3 Comparison



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 3---Benchmark Comparison	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            //Financial Wellbeing



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 5, 5, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 2, 3, sheetname2);

            //Physical Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 13, 13, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 2, 3, sheetname2);

            //Emotional Wellbeing


            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 21, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 2, 3, sheetname2);







            //Slide 3 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 3---Segment 1	Segment 2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");


            //Financial Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 23, 23, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 7, 7, 4, 5, sheetname2);
            //Physical Wellbeing



            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 24, 24, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 8, 8, 4, 5, sheetname2);


            //Emotional Wellbeing

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 25, 25, 2, 3, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 9, 9, 4, 5, sheetname2);




            driver.Quit();


        }


        //Scenario 16




        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide4()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5047", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "16");

            //Slide 4 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 4---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 4---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 4---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 3, 8, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 14, 19, 2, 3, sheetname2);



            driver.Quit();

        }



        //Scenario 16

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide5()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);



            string InputSPText1 = Spotlightreports.Spotlightfilters("5047", "TR", DBConnectionParameters, "Green");



            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "16");


            //Slide 5 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16-----Slide 5---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 0, 0, sheetname2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16------Slide 5---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16----Slide 5---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 9, 15, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 24, 30, 2, 3, sheetname2);




            driver.Quit();

        }


        //Scenario 16

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide6()


        {



            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5047", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "16");


            //Slide 6 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 6---Metrics name--	");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 6, 6, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 0, 0, sheetname2);





            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 6---Metrics value-");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 2, 2, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 1, 1, sheetname2);


            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16-----Slide 6---Benchmark Comparison--	Trendover time");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 16, 21, 4, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 35, 40, 2, 3, sheetname2);



            driver.Quit();



        }


        //Scenario 16

        [TestMethod]
        public void Validate_TR_TargetGroup_distribution_datedemo_customdaterange_Comparison_Slide7()


        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //string sheetname1 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", "1");



            //string InputSPText1 = DistributeActions.ReaddataonmultiplesheetExcelrowcolumnwise(driver, DriverAndLogin.logFilePath, "HRReports-SPDATA.xlsx", 16, 16, 2, 2, sheetname1);




            string InputSPText1 = Spotlightreports.Spotlightfilters("5047", "TR", DBConnectionParameters, "Green");


            DBOperations.executesqlandwritetocsvmultipletables(driver, DriverAndLogin.logFilePath, "TR-DEVresult", DBConnectionParameters, InputSPText1);

            string sheetname2 = UIActions.getExcelSheetName(driver, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", "16");

            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 7---Metrics name");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 1, 1, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 0, 0, sheetname2);



      


            //Slide 7 Comparison

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Comparing Scenario 16---Slide 7---Change of prior---	Segment1---	Segment2");
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "**********************");

            DBOperations.csvCompareExcel(driver, DriverAndLogin.logFilePath, "TR-DEVresult", 27, 30, 3, 5, DriverAndLogin.logFilePath, "TR_QA_Data set.xlsx", 45, 48, 1, 3, sheetname2);





            driver.Quit();

        }

        [TestMethod]
        public void date()


        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);



            //DateTime to String
            //DateTime MyDateTime = new DateTime(1999, 09, 01, 01, 34, 00);
            //String MyString;
            //MyString = MyDateTime.ToString("yyyy-MM-dd HH:mm tt");



            DateTime noon = new DateTime(2012, 12, 20, 00, 00, 00);
            String MyString1;
            String MyString2;
            String MyString3;
            String MyString4;
            String MyString5;
            String MyString6;

            MyString1 = noon.ToString("yyyy MMMM d hh:mm:ss tt");
            MyString2 = noon.ToString("yyyy MMMM d HH:mm:ss tt");

            MyString3 = noon.ToString("yyyy MMMM d hh:mm:ss ");
            MyString4 = noon.ToString("yyyy MMMM d HH:mm:ss ");
            


            MyString5 = noon.ToString("yyyy-MM-dd hh:mm:ss");

            MyString6 = noon.ToString("yyyy-MM-dd HH:mm:ss");




            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, MyString1);

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, MyString2);



            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, MyString3);

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, MyString4);

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, MyString5);

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, MyString6);
        }










    }


}