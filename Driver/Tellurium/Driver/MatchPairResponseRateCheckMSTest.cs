﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Driver
{
    [TestClass]
    public class MatchPairResponseRateCheckMSTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Tellurium\MatchPairResponse&ResponseRateCalculation.xlsx");

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver SetDriverAndNavigateToAccountDefaults()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            else if (UIActions.IsElementVisible(driver, "#account-defaults-title"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            return driver;
        }

        [TestMethod]
        public void ValidateResponseRateAnsAtleastOneQn_HomePage()
        {
            try
            {
                //RemoteWebDriver driver = SetDriverAndNavigateToAccountDefaults();
                //Thread.Sleep(3000);
                //SuppressionActions.ClickAnsweredatleast1question(driver);
                //SuppressionActions.SaveAnalysisdefaults(driver);
                //Thread.Sleep(3000);
                //IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                //js.ExecuteScript("window.scrollBy(0,950);");
                ////IWebElement we = driver.FindElement(By.CssSelector(".btn-primary.active"));
                //UIActions.Click(driver, "[name='defaults-completion-rule-setting'][value='1']");
                //UIActions.Click(driver, "#defaults-save");
                //Thread.Sleep(3000);
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Home Page");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Home Page"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateAnsAtleastOneQn_AnalyzePage()
        {
            try
            {
                //RemoteWebDriver driver = SetDriverAndNavigateToAccountDefaults();
                //UIActions.Click(driver, "[name='defaults-completion-rule-setting'][value='1']");
                //UIActions.Click(driver, "#defaults-save");
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Analyze Page");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Analyze Page"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateAnsAtleastOneQn_AnalyzePageSorting()
        {
            try
            {
                //RemoteWebDriver driver = SetDriverAndNavigateToAccountDefaults();
                //UIActions.Click(driver, "[name='defaults-completion-rule-setting'][value='1']");
                //UIActions.Click(driver, "#defaults-save");
                //DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Tellurium\MatchPairResponse&ResponseRateCalculation.xlsx");
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Analyze Page Sorting");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Analyze Page Sorting"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateAnsAtleastOneQn_ManageDistribute()
        {
            try
            {
                //RemoteWebDriver driver = SetDriverAndNavigateToAccountDefaults();
                //UIActions.Click(driver, "[name='defaults-completion-rule-setting'][value='1']");
                //UIActions.Click(driver, "#defaults-save");
                //DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Tellurium\MatchPairResponse&ResponseRateCalculation.xlsx");
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Manage Distribute");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Manage Distribute"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateAnsAtleastOneQn_DistributeSummary()
        {
            try
            {
                //RemoteWebDriver driver = SetDriverAndNavigateToAccountDefaults();
                //UIActions.Click(driver, "[name='defaults-completion-rule-setting'][value='1']");
                //UIActions.Click(driver, "#defaults-save");
                //DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Tellurium\MatchPairResponse&ResponseRateCalculation.xlsx");
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Distribute Summary");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Distribute Summary"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateAnsAtleastOneQn_ReportPage()
        {
            try
            {
                //RemoteWebDriver driver = SetDriverAndNavigateToAccountDefaults();
                //UIActions.Click(driver, "[name='defaults-completion-rule-setting'][value='1']");
                //UIActions.Click(driver, "#defaults-save");
                //DataTable dt = ExcelOperations.ReadFromExcelInput(@"C:\Tellurium\MatchPairResponse&ResponseRateCalculation.xlsx");
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Report");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponses_ResponseRate(dt, ds, "Report"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateCompletedTheSurvey_HomePage()
        {
            try
            {
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Home Page");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Home Page"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateCompletedTheSurvey_AnalyzePage()
        {
            try
            {
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Analyze Page");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Analyze Page"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateCompletedTheSurvey_AnalyzePageSorting()
        {
            try
            {
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Analyze Page Sorting");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Analyze Page Sorting"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateCompletedTheSurvey_ManageDistribute()
        {
            try
            {
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Manage Distribute");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Manage Distribute"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateCompletedTheSurvey_DistributeSummary()
        {
            try
            {
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Distribute Summary");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponsesCount(dt, ds, "Distribute Summary"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateResponseRateCompletedTheSurvey_ReportPage()
        {
            try
            {
                DataSet ds = MatchPairResponseRateCheck.GetSPResultToDataSet(DBConnectionParameters, dt, "Report");
                Assert.IsTrue(MatchPairResponseRateCheck.ValidateResponses_ResponseRate(dt, ds, "Report"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }
    }
}
