﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Data;
using Common;

namespace Driver
{
    [TestClass]
    public class ReportDataValidationMSTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string SPColor;


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
            };
            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
            SPColor = context.Properties["SPColor"].ToString();
        }
        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver SetDriverAndNavigateToAccountDefaults()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            else if (UIActions.IsElementVisible(driver, "#account-defaults-title"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                UIActions.GetElement(driver, "span.user-info").Click();
                UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click();
                Thread.Sleep(5000);
                UIActions.GetElement(driver, "#menu-account-defaults").Click();
            }
            return driver;
        }

        [TestMethod]
        public void ValidateTestSPForResultsReport_Percentage()
        {
             DataSet[] datasets = null;
             int ScenarioCount = 0;
            int i;
            DataSet ReportFilters;
            string DataCalculation = "Percentage";
            //bool[] SPResultArray;
            string ReportSPName = "USP_ResultsReport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for(i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetResultReportData(ReportFilters, DBConnectionParameters);
                }
            }
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
            
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters,ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters,ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters,ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters,ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
               

            }

        }


        [TestMethod]
        public void ValidateTestSPForResultsReport_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Average";
            //bool[] SPResultArray;
            string ReportSPName = "USP_ResultsReport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetResultReportData(ReportFilters, DBConnectionParameters);
                }
            }
         
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters,ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareResultReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              

            }

        }

        [TestMethod]
        public void ValidateTestSPForInsightsReportForCategory_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Category";
            string DataCalculation = "Percentage";
            DataSet ReportFilters;
            string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {

                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                }
            }
         
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }
        }


        [TestMethod]
        public void ValidateTestSPForInsightsReportForCategory_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Category";
            string DataCalculation = "Average";
            DataSet ReportFilters;
            string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                }
            }
  
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
               

            }
        }

        [TestMethod]
        public void ValidateTestSPForInsightsReportForQuestion_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Question";
            string DataCalculation = "Percentage";
            DataSet ReportFilters;
            string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                }
            }
        
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
            
                   

            }

        }


        [TestMethod]
        public void ValidateTestSPForInsightsReportForQuestion_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Question";
            string DataCalculation = "Average";
            DataSet ReportFilters;
            string ReportSPName = "usp_getstrengthandopportunitydetailwithsegment";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetInsightReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
                }
            }
    
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareInsightReportResults(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }

        }

        [TestMethod]

        public void ValidateTestSPForCommentReportForscoredandunscoredy()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string CategoryText = "General";
            int CategoryId = 38768;
            DataSet ReportFilters;
            string ReportSPName = "usp_getcommentdetailsforcategoryquestion";
            string Identifier = "city";
            int SurveyFormItemId = 82754;
            int PageNumber = 1;
            int PageSize = 20;

            //  @Identifier = 'Which of the following best describes your level in the organization?',@SortBy = '[Score] DESC',@ThenBy = '[CharCount] DESC',
            //@CategoryId = 17319,@SurveyFormItemId = 46717,@PageNumber = @p13 output,@PageSize = 20
            // @p13 = 1

            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetCommentReportData(ReportFilters, DBConnectionParameters, CategoryText);
            }

          foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
             
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + "," + ","
                    + "@Identifier=" + Identifier + "," + "@SortBy=" + "," + "@CategoryId='[Score] DESC'" + "," + "@ThenBy='[CharCount] DESC'" + "," + "@CategoryId=" + CategoryId +
                    ", " + "@SurveyFormItemId=" + SurveyFormItemId + "," + "@PageNumber=" + PageNumber + "," + "@PageSize="+PageSize);

                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;


                if (TestSPIDFromScenariosCount == 1)

                {
                   int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());

                    string UserNalyzeFilterID = "'" + TestSPIDFromScenarios.Tables[0].Rows[0][1].ToString() + "'" ;

                    string query = "SELECT SurveyFormID UserAnalyzeFilter WHERE UserAnalyzeFilter = "+ UserNalyzeFilterID ;
                    DataSet surveyform = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);

                                       DataSet rscount = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);
                    if (rscount.Tables[0].Rows[0][0].ToString() == "0")

                        spresult = ReportDataValidation.SwitchtocommentComparsion(surveyform, DBConnectionParameters, "Genereal", ResultSet, datasets[TargetIndex]);
                }

                ReportDataValidation.WriteTestExecutionResult();
            

            }

        }


       

        [TestMethod]

        public void ValidateTestSPForDrillDown_Category_Percentage()
        {
             
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Percentage";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i]=  ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "category");
                   
                }
            }
      
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }

                    }

        [TestMethod]

        public void ValidateTestSPForDrillDown_Category_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string DataCalculation = "Average";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "category");
                }
            }
           
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
           
            }



        }
        [TestMethod]

        public void ValidateTestSPForDrillDown_Question_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "Percentage";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "Item");
                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              

            }


        }


        [TestMethod]

        public void ValidateTestSPForDrillDown_Question_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "Average";
            string ReportSPName = "usp_drilldownreport";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetDrillDownReportData(ReportFilters, DBConnectionParameters, "Item");
                }
            }
  
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    int UseranalyzeFilterID = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][0].ToString());
                    spresult = ReportDataValidation.CompareDrillDownReportResults(DataCalculation, UseranalyzeFilterID, ReportSPName, DBConnectionParameters, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
            
            }


        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_EntireSurvey_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "Average";
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "survey", "1=1", DBConnectionParameters);
                }

            }
     
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor,"","","year");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "year");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex],datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1],datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }


        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_EntireSurvey_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            String DataCalculation = "percentage";
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "survey", "1=1", DBConnectionParameters);
                }

            }
     
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", "year");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "year");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex],datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }


        }
        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Item_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            String DataCalculation = "percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "item", "16822", DBConnectionParameters);
                }

            }
       
            foreach (DataRow row in dscount.Tables[1].Rows)
            { 
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "Item", "16822", "year");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "year");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
            if (TestSPIDFromScenariosCount == 1)
            {
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
             }
            else if (TestSPIDFromScenariosCount == 2)
            {
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
            else if (TestSPIDFromScenariosCount == 3)
            {
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
            else if (TestSPIDFromScenariosCount == 4)
            {
                int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
            ReportDataValidation.WriteTestExecutionResult();
         
        }

    }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Item_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            String DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {

                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "item", "16822", DBConnectionParameters);
                }

            }
   
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "Item", "16822", "year");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "year");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
           
            }

        }
        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Category_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            String DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "category", "general", DBConnectionParameters);
                }

            }
      
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "Category", "16822", "year");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "year");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex],datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
           
            }


        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Year_Category_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            String DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "year", "category", "general", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "Category", "16822", "year");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "year");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
            
            }


        }



        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_EntireSurvey_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            String DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "survey", "1=1", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", "quarter");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "quarter");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
               
            }

        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_EntireSurvey_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            String DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "survey", "1=1", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", "quarter");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "quarter");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                ReportDataValidation.WriteTestExecutionResult();
         
            }

        }
        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Item_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "item", "16822", DBConnectionParameters);
                }

            }
 
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "item", "16822", "quarter");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "quarter");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }


        }



        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Item_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "item", "16822", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "item", "16822", "quarter");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "quarter");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
             
            }


        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Category_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "category", "general", DBConnectionParameters);
                }

            }
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "category", "16822", "quarter");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "quarter");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
             
            }


        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Quarter_Category_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "quarter", "category", "general", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "category", "16822", "quarter");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "quarter");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
          
            }


        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_EntireSurvey_percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "survey", "1=1", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", "month");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
               
            }

        }



        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_EntireSurvey_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "survey", "1=1", DBConnectionParameters);

                }
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", "month");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
         
            }

        }
        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Item_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "item", "16822", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "Item", "16822", "month");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
            
            }


        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Item_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "item", "16822", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "item", "16822", "month");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
            
            }


        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Category_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "category", "general", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "category", "16822", "month");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex],datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
       
            }



        }

        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Month_Category_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "month", "category", "general", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "category", "16822", "month");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
            
            }



        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_EntireSurvey_Percentage()
        {
            try
            {
                int ScenarioCount = 0;
                DataSet ReportFilters;
                bool spresult = false;
                bool spresult1 = false;
                string ReportSPName = "Usp_TrendChartReportForRating";
                string HeapReportSPName = "Usp_TrendTableReportRatingScale";
                string DataCalculation = "Percentage";
                DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
                ScenarioCount = dscount.Tables[0].Rows.Count;
                DataSet[] datasets = new DataSet[ScenarioCount];
                for (int i = 0; i < ScenarioCount; i++)
                {
                    string scenario = dscount.Tables[0].Rows[i][1].ToString();
                    if (scenario.ToLower().Contains("benchmark"))
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                    }
                    else
                    {
                        ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                        datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "distribution", "survey", "1=1", DBConnectionParameters);
                    }

                }

                foreach (DataRow row in dscount.Tables[1].Rows)
                {
                    
                    DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                    DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", "Distribution");
                    DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "Distribution");
                    int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                    if (TestSPIDFromScenariosCount == 1)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                        spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                    }
                    else if (TestSPIDFromScenariosCount == 2)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                        spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                    }
                    else if (TestSPIDFromScenariosCount == 3)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                        spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    }
                    else if (TestSPIDFromScenariosCount == 4)
                    {
                        int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString()) - 1;
                        int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString()) - 1;
                        int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString()) - 1;
                        int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString()) - 1;
                        spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                        spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    }
                    ReportDataValidation.WriteTestExecutionResult();
                    if ((spresult && spresult1) == false)
                            break;
                }
                Assert.IsTrue(spresult && spresult1);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }

        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_EntireSurvey_Average()
        {
            
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "distribution", "survey", "1=1", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "", "", "Distribution");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "", "Distribution");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }

        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_Item_Percentage()
        {
            
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "distribution", "item", "16822", DBConnectionParameters);
                }

            }


            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "item", "16822", "Distribution");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "Distribution");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
              
            }


        }


        [TestMethod]

        public void ValidateTestSPForTrendRatingScaleReport_Distribution_Item_Average()
        {
            
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "distribution", "item", "16822", DBConnectionParameters);
                }

            }
      
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "item", "16822", "Distribution");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "Distribution");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
               
            }


        }

        [TestMethod]
         public void ValidateTestSPForTrendRatingScaleReport_Distribution_Category_Percentage()
        {
            
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Percentage";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            {
                string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "distribution", "category", "general", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "category", "16822", "Distribution");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "", "16822", "Distribution");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
                
            }

        }


        [TestMethod]
        public void ValidateTestSPForTrendRatingScaleReport_Distribution_Category_Average()
        {
            
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            string DataCalculation = "Average";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            DataSet[] datasets = new DataSet[ScenarioCount];
            for (int i = 0; i < ScenarioCount; i++)
            { string scenario = dscount.Tables[0].Rows[i][1].ToString();
                if (scenario.ToLower().Contains("benchmark"))
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetBenchmarkData(ReportFilters, DBConnectionParameters);
                }
                else
                {
                    ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                    datasets[i] = ReportDataValidation.GetTrendReportDataForRatingScale(ReportFilters, "distribution", "category", "general", DBConnectionParameters);
                }

            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "category", "16822", "Distribution");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "", "Distribution");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareRatingTrendReportResult(DataCalculation, ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapRatingTrendReportResult(DataCalculation, ResultSet1, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
                
            }

        }



        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Year()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "year", "16822",  DBConnectionParameters);

            }
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "16822", "-1", "Year");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "-1", "Year");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
               
            }


        }


        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Quarter()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "quarter", "16822", DBConnectionParameters);

            }
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "16822", "-1", "quarter");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "-1", "quarter");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult( ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult( ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
             
            }

        }

        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Month()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "month", "16822", DBConnectionParameters);

            }
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "16822", "-1", "Month");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "-1", "Month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult( ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult( ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult( ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
               
            }
        }

        [TestMethod]

        public void ValidateTestSPForTrendNonRatingScaleReport_Distribution()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            DataSet ReportFilters;
            string ReportSPName = "";
            string HeapReportSPName = "Usp_TrendTableReportRatingScale";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetTrendReportDataForNonRatingScale(ReportFilters, "distribution", "16822", DBConnectionParameters);

            }
            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                bool spresult1 = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor, "16822", "-1", "Distribution");
                DataSet ResultSet1 = ReportDataValidation.GetActualSPNonRatingTrendReportData(row, DBConnectionParameters, HeapReportSPName + "_" + SPColor, "16822", "-1", "Month");
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult( ResultSet, datasets[TargetIndex]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex]);
                }
                else if (TestSPIDFromScenariosCount == 2)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1]);
                }
                else if (TestSPIDFromScenariosCount == 3)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2]);
                }
                else if (TestSPIDFromScenariosCount == 4)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    int CompIndex1 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[1][3].ToString());
                    int CompIndex2 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[2][3].ToString());
                    int CompIndex3 = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[3][3].ToString());
                    spresult = ReportDataValidation.CompareNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                    spresult1 = ReportDataValidation.CompareHeapNonRatingTrendReportResult(ResultSet, datasets[TargetIndex], datasets[CompIndex1], datasets[CompIndex2], datasets[CompIndex3]);
                }
                ReportDataValidation.WriteTestExecutionResult();
                
            }

        }

        public void ValidateTestSPForDemographicReportForCategory_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Category";
            String DataCalculation = "Average";
            DataSet ReportFilters;
            string DemoLabelForSlice = "periodic activities";
            int DemographicGroupCount = 0;

  

            string ReportSPName = "usp_getdemoordistributionslice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor +"," + "@DemoLabelForSlice=" + DemoLabelForSlice + "@DemographicGroupCount=" + DemographicGroupCount);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                   spresult = ReportDataValidation.CompareDemographicReportResults_category(DataCalculation, ResultSet, datasets[TargetIndex]);
                }
               
                ReportDataValidation.WriteTestExecutionResult();
                
            }
        }
        [TestMethod]
        public void ValidateTestSPForDemographicReportForCategory_Percentage()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Category";
            String DataCalculation = "Percentage";
            DataSet ReportFilters;
            string DemoLabelForSlice = "periodic activities";
            int DemographicGroupCount = 0;



            string ReportSPName = "usp_getdemoordistributionslice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + "," + "@DemoLabelForSlice=" + DemoLabelForSlice + "@DemographicGroupCount=" + DemographicGroupCount);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareDemographicReportResults_category(DataCalculation, ResultSet, datasets[TargetIndex]);
                }

                ReportDataValidation.WriteTestExecutionResult();
              
            }
        }
       [TestMethod]
        public void ValidateTestSPForDemographicReportForQuestion_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Question";
            String DataCalculation = "Average";
            DataSet ReportFilters;
            string DemoLabelForSlice = "periodic activities";
            int DemographicGroupCount = 0;



            string ReportSPName = "usp_getdemoordistributionslice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + "," + "@DemoLabelForSlice=" + DemoLabelForSlice + "@DemographicGroupCount=" + DemographicGroupCount);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareDemographicReportResults_questions(DataCalculation, ResultSet, datasets[TargetIndex]);
                }

                ReportDataValidation.WriteTestExecutionResult();
     

            }
        }
        [TestMethod]

        public void ValidateTestSPForDemographicReportForQuestionItem_Average()
        {
            DataSet[] datasets = null;
            int ScenarioCount = 0;
            string ReportDisplay = "Question";
            String DataCalculation = "Percentage";
            DataSet ReportFilters;
            string DemoLabelForSlice = "periodic activities";
            int DemographicGroupCount = 0;



            string ReportSPName = "usp_getdemoordistributionslice";
            DataSet dscount = ReportDataValidation.GetTestScenarios(DBConnectionParameters);
            ScenarioCount = dscount.Tables[0].Rows.Count;
            for (int i = 0; i < ScenarioCount; i++)
            {
                ReportFilters = ReportDataValidation.GetReportFilter(dscount.Tables[0].Rows[i][0].ToString(), DBConnectionParameters);
                datasets[i] = ReportDataValidation.GetDemographicReportData(ReportFilters, DBConnectionParameters, ReportDisplay);
            }

            foreach (DataRow row in dscount.Tables[1].Rows)
            {
                bool spresult = false;
                DataSet TestSPIDFromScenarios = ReportDataValidation.GetTestSPIDFromScenarios(row, DBConnectionParameters);
                DataSet ResultSet = ReportDataValidation.GetActualSPReportData(row, DBConnectionParameters, ReportSPName + "_" + SPColor + "," + "@DemoLabelForSlice=" + DemoLabelForSlice + "@DemographicGroupCount=" + DemographicGroupCount);
                int TestSPIDFromScenariosCount = TestSPIDFromScenarios.Tables[0].Rows.Count;
                if (TestSPIDFromScenariosCount == 1)
                {
                    int TargetIndex = Int32.Parse(TestSPIDFromScenarios.Tables[0].Rows[0][3].ToString());
                    spresult = ReportDataValidation.CompareDemographicReportResults_questions(DataCalculation, ResultSet, datasets[TargetIndex]);
                }

                ReportDataValidation.WriteTestExecutionResult();
              

            }
        }
    }

        }

