﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System.Reflection;

using OpenQA.Selenium.Interactions;

namespace Driver
{
    [TestClass]
    public class DistributeMSTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static string downloadFilepath;
        public static string logFilePath;
        public static string latestfile = "";
        public static string distributionName = "Enroute";
        public static RemoteWebDriver currentDriver;



        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString()
            };
        }
        //shwetabh srivastava----added capabilities for download file in specified location at run time
        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;

            switch (driverName)
            {
                case "Firefox":
                    FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    driver = new FirefoxDriver(service);


                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);


                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);

                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);
                    driver = new FirefoxDriver(options);

                    break;

                case "IE":
                    driver = new InternetExplorerDriver();

                    break;
                case "Chrome":
                default:

                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    driver = new ChromeDriver(chromeOptions);

                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;

        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
        public static void NavigateToHome(RemoteWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Home").Click();
        }

        public static RemoteWebDriver SetDriverAndNavigateToHome(string driverName, string accountName = null)
        {
            if (accountName == null)
            {
                accountName = DriverAndLogin.Account;
            }
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, accountName);
            }
            else
                NavigateToHome(driver);
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        }

        [TestMethod]
        public void ValidateNavigationToMangeDistribution()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(10000);
            DistributeActions.NavigateToManageDistrbution(driver);
            var distributeGrid = UIActions.GetElementWithWait(driver, "#distribute-survey-table", 20);
            Assert.IsTrue(distributeGrid != null);
        }

        [TestMethod]
        public void ValidateSearchAndOpenDistribution()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            var distributepage = DistributeActions.SearchAndOpenDistribution(driver, distributionName);
            Assert.AreEqual(true, distributepage != null);
        }

        [TestMethod]
        public void ValidateCopyUniqueLink()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, distributionName);
            Thread.Sleep(10000);
            DistributeActions.CopyUniqueLink(driver);
            var messageArea = UIActions.GetElementWithWait(driver, "#action-status", 30);
            Assert.AreEqual("Link was successfully copied to your clipboard.", messageArea.Text.Trim());
        }

        //Gayathri Natarajamani-Multiple Demo Edit Field
        [TestMethod]
        public void ValidateEditparticipantWithMultipleDemo()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            var emailaddress = DistributeActions.AddParticipant(driver);
            Thread.Sleep(20000);
            string[] EditDemoValues = { "Manager", "MBA", "Male", "Russia" };
            int CountOfString = EditDemoValues.Count();
            Console.WriteLine(CountOfString);
            DistributeActions.EditParticipantWithCustomDemo(driver, emailaddress, 11, CountOfString, EditDemoValues);
            Thread.Sleep(2000);
            var EditParticipantPopUp = UIActions.GetElement(driver, "#view-edit-participant-modalLabel");
            Assert.IsFalse(EditParticipantPopUp != null && EditParticipantPopUp.Displayed);

        }

        //Gayathri Natarajamani - Delete Participant from Grid

        [TestMethod]
        public void ValidateDeleteParticipant()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Automation Demographic logic");
            Thread.Sleep(1000);
            var emailaddress = DistributeActions.AddParticipant(driver);
            Thread.Sleep(25000);
            DistributeActions.DeleteParticipant(driver, emailaddress);
            IWebElement textBox = UIActions.GetElement(driver, "[type='search']");
            textBox.Clear();
            textBox.SendKeys(emailaddress);
            Thread.Sleep(2000);
            var GetGridData = driver.FindElement(By.CssSelector(".dataTables_empty"));
            var GetText = GetGridData.Text;
            Assert.AreEqual("No matching records found", GetText);
        }

        //Gayathri Natarajamani- ValidateOnetimematchPairCreation
        [TestMethod]
        public void ValidateCreationOfOneTimeMatchPairDistribution()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.CreateOnetimeMatchPairUploadDistribution(driver, "Automation Match pair Logic", "06/11/2018 2:16 PM", null);
            Thread.Sleep(2000);
            var ChildTab = driver.FindElement(By.Id("child-view"));
            Assert.AreEqual("Child", ChildTab.Text);
        }

        //Gayathri Natarajamani- ValidateOnetimeMatchpairupload
        [TestMethod]
        public void Validate_1OneTimeMatchpairUpload()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.CreateOnetimeMatchPairUploadDistribution(driver, "Automation Match pair Logic", "06/11/2018 2:16 PM", null);
            Thread.Sleep(3000);
            IWebElement DistributionLink = driver.FindElement(By.XPath("//a[contains(@href,'DistributeSurvey')]"));
            DistributionLink.Click();
            Thread.Sleep(8000);
            DistributeActions.SearchAndOpenDistribution(driver, DistributeActions.DistributionDynamicName);
            Thread.Sleep(2000);
            DistributeActions.UploadOneTimeMatchPair(driver);
            //Thread.Sleep(150000);
            IWebElement UploadStatus = UIActions.GetElement(driver, "#pending-file-status");
            var UploadStatusText = UploadStatus.Text;
            while (UploadStatusText != "Upload process completed.")
            {
                Thread.Sleep(3000);
                UploadStatusText = UploadStatus.Text;
            }

            var Successlink = driver.FindElement(By.CssSelector(".import-complete-message"));
            Assert.AreEqual("Import completed. Click here to refresh participant list.", Successlink.Text);
        }

        //Gayathri Natarajamani- Validation of MatchpairLogic in Player for Child
        [TestMethod]

        public void Validate_2MatchpairLogicinPlayerForChild()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, DistributeActions.DistributionDynamicName);
            Thread.Sleep(2000);
            DistributeActions.CopyUniqueLinkBySearchParticipant(driver, "gnatarajamani@cebglobal.com");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(1000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(20000);
            var QuestionText = driver.FindElement(By.XPath("//*[@data-bind='displayText']")).Text;
            Assert.AreEqual("2. How would you rate your overall recruiting experience?", QuestionText);
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            Thread.Sleep(5000);
        }
        [TestMethod]
        public void Validate_3MatchPairLogicinPlayerForParent()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, DistributeActions.DistributionDynamicName);
            Thread.Sleep(2000);
            DistributeActions.CopyuniqueLinkBySelectingDefaultFirstManager(driver);
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(1000);
            UIActions.clickwithXapth(driver, "(//*[@id='btnNext'])[1]");
            Thread.Sleep(2000);
            UIActions.clickwithID(driver, "btnNext");
            Thread.Sleep(10000);
            var QuestionText = driver.FindElement(By.XPath("(//*[@data-bind='displayText'])[2]")).Text;
            Assert.AreEqual("3. Which Candidate Accepted the offer?", QuestionText);
            UIActions.SwitchWindows(driver, "Distribute Survey - Pulse");
            Thread.Sleep(5000);
        }

        /** Gayathri - Edit Demographic label **/
        [TestMethod]
        public void ValidateEditDemographicLabel()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "January - Trend");
            Thread.Sleep(2000);
            DistributeActions.EditDemographicLabel(driver);
            var SuccessMessage = driver.FindElement(By.Id("action-status"));
            Assert.AreEqual("Demographics saved successfully", SuccessMessage.Text);

        }



        [TestMethod]
        public void ValidateDemographicLogicInPlayer()
        {
            string playerURL;
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, distributionName);
            Thread.Sleep(100000);
            DistributeActions.CopyUniqueLink(driver);
            playerURL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, playerURL);
            Assert.IsTrue(driver.Title.Contains("| Pulse Survey"));

        }

        [TestMethod]
        public void ValidateCreateDistributionFromHomePage()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
                Thread.Sleep(10000);
                DistributeActions.CreateDistributionFromHomePage(driver, new DistributionParametersDTO
                {
                    FormName = "Thunderbird",
                    DistributionName = "SeleniumDistribution " + Guid.NewGuid(),
                    StartDate = "05/28/2018 10:00 PM",
                    StartTimeZone = "Chennai",
                    EndDate = "05/28/2022 10:00 PM",
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Schedule Emails" }
                });
                Thread.Sleep(5000);
                DistributeActions.UploadParticipantsWithDemoMapping(driver, "Template_AddParticipantFile.xlsx");
                Thread.Sleep(7000);
                UIActions.Click(driver, "#btn-next");
                Thread.Sleep(7000);
                UIActions.Click(driver, "#btn-next");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, "#action-status", 30);
                Assert.IsTrue(messageArea.Text.Trim().Contains("You have successfully created the Distribution"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }

        }

        [TestMethod]
        public void ValidateCreateDistributionFromDistributePage()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.CreateDistributionFromDistributePage(driver, new DistributionParametersDTO
                {
                    FormName = "Thunderbird",
                    DistributionName = "SeleniumDistribution " + Guid.NewGuid(),
                    StartDate = "05/28/2018 10:00 PM",
                    StartTimeZone = "Chennai",
                    EndDate = "05/28/2022 10:00 PM",
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Schedule Emails" }
                });
                Thread.Sleep(5000);
                DistributeActions.UploadParticipantsWithDemoMapping(driver, "Template_AddParticipantFile.xlsx");
                Thread.Sleep(7000);
                UIActions.Click(driver, "#btn-next");
                Thread.Sleep(7000);
                UIActions.Click(driver, "#btn-next");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, "#action-status", 30);
                Assert.IsTrue(messageArea.Text.Trim().Contains("You have successfully created the Distribution"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }

        }

        [TestMethod]
        public void ValidateAddParticipantToExistingDistribution()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                var distributepage = DistributeActions.SearchAndOpenDistribution(driver, "Selenium Auto 1");
                Thread.Sleep(7000);
                IWebElement editButton = UIActions.GetElement(driver, "[widget-data-event='click::SelectParticipantSectionForEdit']");
                editButton.Click();
                Thread.Sleep(7000);
                DistributeActions.UploadParticipantsWithDemoMapping(driver, "Template_AddParticipantFile.xlsx");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, ".alert-success", 30);
                Assert.IsTrue(messageArea.Text.Trim().Contains("File uploaded successfully and will be processed soon."));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateEditDistributionFromHomePage()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                UIActions.Click(driver, ".btn-xs.dropdown-toggle.btn-info.btn");
                UIActions.Click(driver, "[style='display: block; top: 410px; left: 210.063px;'] a[widget-data-event='click::redirectToEditDistFromAnalyse']");
                Thread.Sleep(8000);
                IWebElement editButton = UIActions.GetElement(driver, "[widget-data-event='click::SelectParticipantSectionForEdit']");
                editButton.Click();
                Thread.Sleep(7000);
                DistributeActions.UploadParticipantsWithDemoMapping(driver, "Template_AddParticipantFile.xlsx");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, ".alert-success", 30);
                Assert.IsTrue(messageArea.Text.Trim().Contains("File uploaded successfully and will be processed soon."));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }




        //Shwetabh Srivastava- Validate download sample Participant Template /getlatestdownloadedfile/Read contents of latestfile in log file
        [TestMethod]
        public void ValidateUploadParticipantfromDistribution()
        {

            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(10000);
            DistributeActions.downloadSampleParticipantUploadfile(driver, "AuthenticationLogicUn");
            Thread.Sleep(10000);
            //getlatestdownloadedfile

            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);

            Thread.Sleep(10000);

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Reading excel file ");
            DistributeActions.readExcel(driver, DriverAndLogin.downloadFilepath, latestfile);


            DistributeActions.readExcelrowcolumnwise(driver, DriverAndLogin.downloadFilepath, latestfile, 0, 0, 0, 8);


            DistributeActions.WriteLog(driver, DriverAndLogin.downloadFilepath, "Writing into excel file ");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 0, "shwetabh@gmail.com");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 1, "shwetabh");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 2, "32909");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 3, "mail@gmail.com");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 4, "Bangalore");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 5, "21000");


            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 6, "India");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 7, "6/19/2018");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 1, 8, "88888");



            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 0, "shwetabha@gmail.com");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 1, "shwetabha");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 2, "329090");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 3, "mailn@gmail.com");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 4, "Bangalorea");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 5, "21008");
            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 6, "china");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 7, "5/19/2018");

            DistributeActions.WriteToExcel(driver, DriverAndLogin.downloadFilepath, latestfile, 2, 8, "38888");
            //Select button
            IWebElement selectBtn = UIActions.FindElementWithXpath(driver, "//input[@type='file']");
            Thread.Sleep(10000);
            selectBtn.SendKeys(DriverAndLogin.downloadFilepath + "\\" + latestfile);
            Thread.Sleep(30000);
            //Upload button
            UIActions.FindElementWithXpath(driver, " //*[@id='btn-uploadParticipants']").Click();
            Thread.Sleep(30000);
            //dropdownselect
            //   UIActions.SelectInCEBDropdownByValue(driver, "#demodateFormat", "MM/DD/YYYY" );

            UIActions.FindElementWithXpath(driver, "//*[@id='demodateFormat']").Click();

            UIActions.FindElementWithXpath(driver, "  //*[@id='demodateFormat']//option[text()='MM/DD/YYYY']").Click();
            Thread.Sleep(30000);
            //proceed button
            UIActions.FindElementWithXpath(driver, "//*[@widget-data-event='click::UploadNewDemoParticipantFile']").Click();
            Thread.Sleep(30000);

            driver.Quit();

        }




        //Shwetabh Srivastava- Validate download sample Participant Template /getlatestdownloadedfile/Read contents of latestfile in log file
        [TestMethod]
        public void ValidatedownloadTemplatefromCompanyDirectory()
        {


            IWebDriver driver = GetWebDriverwithcapabilities(DriverAndLogin.Browser, DriverAndLogin.Url);
            Thread.Sleep(10000);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(10000);
            //click Arrow Down
            IWebElement Arrowdownfor_settings = UIActions.FindElementWithXpath(driver, "//*[@id='navbar-container']//ul/li/a[@href='#']");
            Arrowdownfor_settings.Click();
            Thread.Sleep(10000);
            //click Settings
            IWebElement SettingsClick = UIActions.FindElementWithXpath(driver, "//A[@href='/Pulse/AccountSettings']");
            SettingsClick.Click();
            Thread.Sleep(10000);
            //click Company Directory
            IWebElement CompanyDirectory = UIActions.FindElementWithXpath(driver, "//*[@id='directory-accordion']//DIV[@id='settings-heading']");
            CompanyDirectory.Click();

            Thread.Sleep(10000);
            //click participant
            IWebElement Participants = UIActions.FindElementWithXpath(driver, "//BUTTON[@id='menu-directory-participants'][text()='Participants']");
            Participants.Click();
            Thread.Sleep(10000);
            //click upload participant
            IWebElement Uploadparticipantsbutton = UIActions.FindElementWithXpath(driver, "//BUTTON[contains(., 'Upload Participants')]");
            Uploadparticipantsbutton.Click();
            Thread.Sleep(10000);
            //click sample import template
            UIActions.FindElementWithXpath(driver, "//a[text()='sample import template']").Click();
            Thread.Sleep(10000);
            //  getlatestdownloadedfile
            string latestfile = DistributeActions.getLastDownloadedFile(DriverAndLogin.downloadFilepath);
            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Latestdownloaded file is " + latestfile);

            Thread.Sleep(10000);

            DistributeActions.WriteLog(driver, DriverAndLogin.logFilePath, "Reading excel file ");
            DistributeActions.readExcel(driver, DriverAndLogin.downloadFilepath, latestfile);
            driver.Quit();

        }





        [TestMethod]
        public void ReopeningSurveyforDistributionAfterTakingResponses()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "SeleniumTestForm6ecdfde7-d2b1-4252-9240-bc9e5d4c582f");
            Thread.Sleep(15000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollBy(0,500)");
            IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
            EditParticipant.Click();
            Thread.Sleep(2000);
            IWebElement participant = driver.FindElement(By.XPath("(//*[@class='email']/../..//td[1])[1]"));
            participant.Click();
            driver.FindElement(By.Id("btn-copy-participant-url")).Click();
            String clipboardText = System.Windows.Forms.Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            IWebElement Page1Question1 = UIActions.GetElementswithXpath(driver, "(//*[text()='4'])[1]", 10);
            Page1Question1.Click();
            IWebElement Page1Question2 = UIActions.GetElementswithXpath(driver, "(//*[text()='5'])[2]", 10);
            Page1Question2.Click();
            IWebElement Page1Question3 = UIActions.GetElementswithXpath(driver, "(//*[text()='1'])[3]", 10);
            Page1Question3.Click();
            js.ExecuteScript("window.scrollBy(0,500)");
            IWebElement Page1Question4 = UIActions.GetElementswithXpath(driver, "(//*[@class='test-qs-answer rsTextDisplay'])[4]", 10);
            Page1Question4.Click();
            IWebElement Next1 = UIActions.GetElement(driver, "#btnNext");
            Next1.Click();
            String text = "Individual Contributor / Employee";
            UIActions.SelectInCEBDropdownByText(driver, ".dropdown-responses", text);
            IWebElement Next2 = UIActions.GetElement(driver, "#btnNext");
            Next2.Click();
            Thread.Sleep(3000);
            IWebElement Page4Question1 = UIActions.GetElementswithXpath(driver, "(//*[text()='3'])[2]", 10);
            Page4Question1.Click();
            IWebElement Finish = UIActions.GetElement(driver, "#btnFinish");
            Finish.Click();
            bool status = DistributeActions.ReopenSurvey(driver);
            Assert.IsTrue(status);

        }
        [TestMethod]
        public void ReopeningSurveyforDistribution()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "SeleniumTestForm6ecdfde7-d2b1-4252-9240-bc9e5d4c582f");
            Thread.Sleep(15000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollBy(0,500)");
            IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
            EditParticipant.Click();
            Thread.Sleep(2000);
            bool status = DistributeActions.ReopenSurvey(driver);
            Assert.IsTrue(status);
        }

        [TestMethod]
        public void Resetresponses()
        {
            try
            {
                RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
                DistributeActions.SearchAndOpenDistribution(driver, "SeleniumTestForm6ecdfde7-d2b1-4252-9240-bc9e5d4c582f");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(5000);
                String Reset = DistributeActions.resetresponses(driver);
                Assert.IsTrue(Reset.Equals("Not Started"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Assert.Fail();
            }
        }
        [TestMethod]
        public void ValidateCreateInviteEmailForMatchPair()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "MatchPair33adc1b7-fbda-45cc-8d0d-b27b8ae0fa00");
            DistributeActions.CreateInviteEmail(driver);
            Thread.Sleep(3000);
            DistributeActions.CreateEmailTemplate(driver, "ChildSurvey", "Automation", "Participant Group", "Survey URL");
            var status = UIActions.GetElement(driver, "#action-status");
            Assert.AreEqual("Email updated successfully", status.Text.TrimStart());

        }
        [TestMethod]
        public void ValidateCreatereminderEmailForMatchPair()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                DistributeActions.SearchAndOpenDistribution(driver, "MatchPair33adc1b7-fbda-45cc-8d0d-b27b8ae0fa00");
                DistributeActions.CreateReminderEmail(driver);
                Thread.Sleep(3000);
                DistributeActions.CreateEmailTemplate(driver, "ChildSurvey", "Automation-Reminder", "Participant Group", "Survey URL");
                /*var status = UIActions.GetElement(driver, "#action-status");
                Assert.AreEqual("Email created successfully", status.Text.TrimStart()); */
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [TestMethod]
        public void CheckStatusMatchpairDistribution()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "testfullupdate");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                UIActions.Click(driver, "#parent-view");
                Thread.Sleep(3000);
                IWebElement checkStatus = UIActions.FindElement(driver, "#manage-participants-table th.survey-status");
                if (checkStatus != null)
                    Assert.IsTrue(true);
                else
                    Assert.Fail();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }


        [TestMethod]
        public void CheckAuthenticationMatchpairDistribution()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "testfullupdate");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                UIActions.Click(driver, "#parent-view");
                Thread.Sleep(3000);
                IWebElement checkStatus = UIActions.FindElement(driver, "#manage-participants-table th.authentication-column");
                if (checkStatus != null)
                    Assert.IsTrue(true);
                else
                    Assert.Fail();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CheckAuthenticationMatchpairValueDistribution()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "testfullupdate");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                UIActions.Click(driver, "#parent-view");
                Thread.Sleep(3000);
                Assert.IsTrue(true);
                //IWebElement checkStatus = UIActions.FindElement(driver, "#manage-participants-table tbody tr:first td:nth-child(5) span");
                //if (checkStatus != null)
                //    Assert.IsTrue(true);
                //else
                //    Assert.IsTrue(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CheckstatusMatchpairValueDistribution()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "testfullupdate");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                UIActions.Click(driver, "#parent-view");
                Thread.Sleep(3000);

                Assert.IsTrue(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CheckSurveyurlMatchpairValueDistribution()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "testfullupdate");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                UIActions.Click(driver, "#parent-view");
                Thread.Sleep(3000);

                Assert.IsTrue(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CheckSurveyURLDistribution()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "testfullupdate");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                UIActions.Click(driver, "#parent-view");
                Thread.Sleep(3000);
                IWebElement checkStatus = UIActions.FindElement(driver, "#manage-participants-table th.survey-url");
                if (checkStatus != null)
                    Assert.IsTrue(true);
                else
                    Assert.Fail();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }



        [TestMethod]
        public void CheckSurveyURLDistributionTier4()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "tier4");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                try
                {
                    IWebElement checkStatus = UIActions.FindElement(driver, "#manage-participants-table th.survey-url");
                    if (checkStatus != null)
                        Assert.Fail();
                    else
                        Assert.IsTrue(true);
                }
                catch(Exception ex)
                {
                    Assert.IsTrue(true);
                }
               
              
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CheckSurveyURLDistributionTier2()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "tier2");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                try
                {
                    IWebElement checkStatus = UIActions.FindElement(driver, "#manage-participants-table th.survey-url");
                    if (checkStatus != null)
                        Assert.IsTrue(true); 
                    else
                        Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.Fail();
                }



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CheckSurveyURLDistributionTier3nonceb()
        {
            try
            {
                DriverAndLogin.Username = "viveknarang.86@gmail.com";
                DriverAndLogin.Password = "Welcom12345";
                 
                RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                Thread.Sleep(7000);
                DistributeActions.SearchAndLaunchDistributionSummary(driver, "tier3");
                Thread.Sleep(10000);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("window.scrollBy(0,500)");
                IWebElement EditParticipant = UIActions.GetElementswithXpath(driver, "(//button[@widget-data-event='click::SelectParticipantSectionForEdit'])", 45);
                EditParticipant.Click();
                Thread.Sleep(3000);
                try
                {
                    IWebElement checkStatus = UIActions.FindElement(driver, "#manage-participants-table th.survey-url");
                    if (checkStatus != null)
                        Assert.Fail(); 
                    else
                        Assert.IsTrue(true);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(true);
                }



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void A_CheckIfQuotaButtonExist()
        {
            try
            {
                RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser,"Mac");
                currentDriver = driver;
                Thread.Sleep(10000);
                DistributeActions.CreateDistributionFromHomePage(driver, new DistributionParametersDTO
                {
                    FormName = "Show test 6",
                    DistributionName = "QuotaTest" + DateTime.Now.Ticks,
                    StartDate = "07/28/2018 10:00 PM",
                    StartTimeZone = "Chennai",
                    EndDate = "05/28/2025 10:00 PM",
                    EndTimeZone = "Chennai",
                    Method = new string[] { "Share Manually" }
                });
                Thread.Sleep(10000);
                Assert.IsTrue(DistributeActions.CheckIfQuotaButtonExist(currentDriver));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void B_CheckIfQuotaModalOpens()
        {
            try
            {
                Assert.IsTrue(DistributeActions.CheckIfQuotaModelOpens(currentDriver));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void C_CheckIfNewQuotaModalOpens()
        {
            try
            {
                Assert.IsTrue(DistributeActions.CheckIfAddNewQuotaModalOpens(currentDriver));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void D_ValidateIfQuestionQuotaSaved()
        {
            try
            {
                Assert.IsTrue(DistributeActions.ValidateIfQuestionQuotaSaved(currentDriver));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void E_ValidateIfDemoGraphicQuotaSaved()
        {
            try
            {
                Assert.IsTrue(DistributeActions.ValidateIfDemoGraphicQuotaSaved(currentDriver));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void F_ValidateIfAuthenticationTypeQuotaSaved()
        {
            try
            {
                Assert.IsTrue(DistributeActions.ValidateIfAuthenticationTypeQuotaSaved(currentDriver));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void G_ValidateIfOverallResponseQuotaSaved()
        {
            try
            {
                Assert.IsTrue(DistributeActions.ValidateIfOverallResponseQuotaSaved(currentDriver));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Assert.Fail();
            }
        }
        [TestMethod]
        public void ValidateAddManualParticipantScreen()
        {
            DriverAndLogin.Account = "Chin";
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.CreateDistributionFromDistributePage(driver, new DistributionParametersDTO
            {
                FormName = "New Form V10Demo",
                DistributionName = "ManualParticipantAddTest_ " + Guid.NewGuid(),
                StartDate = "09/01/2018 10:00 AM",
                StartTimeZone = "Chennai",
                EndDate = "09/30/2018 10:00 PM",
                EndTimeZone = "Chennai",
                Method = new string[] { "Schedule Emails", "Share Manually" }
            });
            Thread.Sleep(5000);
            DistributeActions.OpenManualParticipantScreen(driver);
            var labels = driver.FindElements(By.CssSelector("#participants-manual label"));
            Assert.IsTrue(labels.Any(l => l.Text.Trim() == "Unique Link") && labels.Any(l => l.Text.Trim() == "Open Link"));
            Assert.IsTrue(labels.Any(l => l.Text.Trim() == "Name") && labels.Any(l => l.Text.Trim() == "Email*") && labels.Any(l => l.Text.Trim() == "Unique Link") && labels.Any(l => l.Text.Trim() == "Employee ID"));
            var mandatoryLabel = labels.FirstOrDefault(l => l.Text.Trim() == "Country1");
            var mandatorySymbol = mandatoryLabel.FindElement(By.XPath("..")).FindElement(By.CssSelector("span.danger"));
            Assert.IsTrue(mandatoryLabel != null && mandatorySymbol != null && mandatorySymbol.Displayed);
            var dateLabel1 = labels.FirstOrDefault(l => l.Text == "HireDate");
            var datePicker1 = DistributeActions.FindDatePickerInAddEditParticipant(driver, dateLabel1);
            var dateLabel2 = labels.FirstOrDefault(l => l.Text == "DOJ");
            var datePicker2 = DistributeActions.FindDatePickerInAddEditParticipant(driver, dateLabel2);
            Assert.IsTrue(datePicker1 != null && datePicker2 != null);
        }

        //[TestMethod]
        //public void ValidateAddManualParticipant_AllFields()
        //{
        //    DriverAndLogin.Account = "Chin";
        //    var driver = GetDriver(DriverAndLogin.Browser);
        //    if (driver == null)
        //    {
        //        driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
        //        DistributeActions.CreateDistributionFromDistributePage(driver, new DistributionParametersDTO
        //        {
        //            FormName = "New Form V10Demo",
        //            DistributionName = "ManualParticipantAddTest_ " + Guid.NewGuid(),
        //            StartDate = "09/01/2018 10:00 AM",
        //            StartTimeZone = "Chennai",
        //            EndDate = "09/30/2018 10:00 PM",
        //            EndTimeZone = "Chennai",
        //            Method = new string[] { "Schedule Emails", "Share Manually" }
        //        });
        //        Thread.Sleep(5000);
        //        DistributeActions.OpenManualParticipantScreen(driver);
        //    }
        //    var labels = driver.FindElements(By.CssSelector("#participants-manual label"));
        //    var participantLabels =
        //        labels.Where(l => l.Text.Trim() != "Authentication Type" && l.Text.Trim() != "Unique Link" && l.Text.Trim() != "Open Link").ToList();
        //    foreach (var label in participantLabels)
        //    {
        //        if (label.Text.Trim() == "Email*")
        //        {
        //            DistributeActions.PopulateStdFieldValueInManualParticipantScreen(driver, label, Guid.NewGuid() + "@cebglobal.com");
        //        }
        //        else if (label.Text.Trim() == "Name" || label.Text.Trim() == "Employee ID")
        //        {
        //            DistributeActions.PopulateStdFieldValueInManualParticipantScreen(driver, label, Guid.NewGuid().ToString());
        //        }
        //        else if (DistributeActions.FindDatePickerInAddEditParticipant(driver, label) != null)
        //        {
        //            DistributeActions.PopulateDateFieldValueInManualParticipantScreen(driver, label, new Random().Next(1,31), new Random().Next(1,12), 2018);
        //        }
        //        else
        //        {
        //            DistributeActions.PopulateDemoFieldValueInManualParticipantScreen(driver, label, Guid.NewGuid().ToString());
        //        }
        //    }
        //    var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
        //    addButton.Click();
        //    Thread.Sleep(10000);
        //    var message = UIActions.FindElement(driver, ".status-area #action-status");
        //    Assert.IsTrue(message.Text.Trim() == "Participant addition successful");
        //}

        [TestMethod]
        public void ValidateAddManualParticipant_MandatoryAndEmailValidation()
        {
            DriverAndLogin.Account = "Chin";
            var driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
                DistributeActions.CreateDistributionFromDistributePage(driver, new DistributionParametersDTO
                {
                    FormName = "New Form V10Demo",
                    DistributionName = "ManualParticipantAddTest_ " + Guid.NewGuid(),
                    StartDate = "09/01/2018 10:00 AM",
                    StartTimeZone = "Chennai",
                    EndDate = "09/30/2018 10:00 PM",
                    EndTimeZone = "Chennai",
                    Method = new string[] {"Schedule Emails", "Share Manually"}
                });
                Thread.Sleep(5000);
            }
            DistributeActions.OpenManualParticipantScreen(driver);
            var labels = driver.FindElements(By.CssSelector("#participants-manual label"));
            var addButton = driver.FindElements(By.CssSelector(".btn-save-participant-list")).FirstOrDefault(e => e.Displayed);
            addButton.Click();
            Thread.Sleep(1000);
            var status = UIActions.FindElement(driver, ".import-list-status-tabManual #action-status");
            Assert.IsTrue(status.Text.Trim() == "Mandatory fields must be completed.");
            var emailLabel = labels.FirstOrDefault(l => l.Text.Trim() == "Email*");
            var countryLabel = labels.FirstOrDefault(l => l.Text.Trim() == "Country1");
            DistributeActions.PopulateStdFieldValueInManualParticipantScreen(driver, emailLabel, Guid.NewGuid().ToString());
            DistributeActions.PopulateDemoFieldValueInManualParticipantScreen(driver, countryLabel, "India");
            addButton.Click();
            Thread.Sleep(1000);
            Assert.IsTrue(status.Text.Trim() == "Please enter a valid Email Address");
            IWebElement CancelButton = driver.FindElement(By.Id("btn-cancel"));
            Actions action = new Actions(driver);
            action.MoveToElement(CancelButton).Click().Perform();

        }

        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }


    }


}
