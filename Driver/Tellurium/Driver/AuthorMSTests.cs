﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using Microsoft.Office.Interop.Excel;
using OpenQA.Selenium.Interactions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Windows.Forms;
using System.IO;
using Aspose.Cells;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Driver
{
    [TestClass]
    public class AuthorMSTests
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        private static string FormName;
        public static string FileName;
        public static ExcelToDataTable xl;



        string highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"" });";



        //static AuthorMSTests()
        //{
        //    DriverAndLogin = new DriverAndLoginDto
        //    {
        //        Url = "https://qa-surveys.cebglobal.com/Pulse",
        //        Browser = "Chrome",
        //        Username = "avenkitaraman@cebglobal.com",
        //        Password = "Password@9",
        //        Account = "DirectoryDemoDelete"
        //    };
        //    //DriverAndLogin = CommonActions.ReadDriveAndLoginDetails();
        //    //FileName = CommonActions.CreateResultsFile("AuthorTestResuls_"+DateTime.Now.ToString("u").Replace(':','_'));
        //    //FileActionAttribute.FileName = FileName;
        //}

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static void OpenOrCreateForm(RemoteWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToForm(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                OpenOrCreateForm(driver);
            }
            else if (driver.Url.Contains("ManageForms"))
                OpenOrCreateForm(driver);
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);
            }
            else
            {
                OpenOrCreateForm(driver);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToManageForms(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
            }
            else
            {
                AuthorActions.NavigateToAuthor(driver);
            }
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToHome(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            }
            else
                NavigateToHome(driver);
            return driver;
        }

        public static void NavigateToHome(RemoteWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Home").Click();
        }

        public static void NavigateToAuthor(RemoteWebDriver driver)
        {
            var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
            navbuttons.FirstOrDefault(b => b.Text == "Author").Click();
        }

        [TestMethod]
        public void ValidateCreateFormFromHome()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(10000);
            FormName = "SeleniumTestForm" + Guid.NewGuid();
            var formBuilder = AuthorActions.CreateForm(driver, FormName);
            Assert.AreEqual(true, formBuilder != null);
        }

        //[TestMethod]
        //public void ValidateCopyFormFromHome()
        //{
        //    RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
        //    Thread.Sleep(10000);
        //    var formBuilder = AuthorActions.CopyForm(driver);
        //    Assert.AreEqual(true, formBuilder != null);
        //    driver.Close();
        //    driver.Dispose();
        //}

        //[TestMethod]
        //public void ValidateCreateFormFromManageForms()
        //{
        //    RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
        //    WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
        //    AuthorActions.NavigateToAuthor(driver);
        //    FormName = "SeleniumTestForm" + Guid.NewGuid();
        //    var formBuilder = AuthorActions.CreateForm(driver, FormName);
        //    Assert.AreEqual(true, formBuilder != null);
        //}

        //[TestMethod]
        //public void ValidateOpenFormFromManageForms()
        //{
        //    RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
        //    if (driver == null)
        //    {
        //        driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
        //        WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
        //    }
        //    AuthorActions.NavigateToAuthor(driver);
        //    var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
        //    Assert.AreEqual(true, formBuilder != null);
        //}

        [TestMethod]
        public void ValidateAddSingleRatingScaleQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(1000);
            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            screenshot.SaveAsFile("test.png");
            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate this Automation framework?");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [TestMethod]
        public void ValidateAddSingleRatingScaleQuestionWithScaleRangeAndHeaders()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] scaleHeaders = { "One", "Two", "Three", "Four", "Five", "Six" };
            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate the old Automation framework?", "1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [TestMethod]
        public void ValidateAddMultipleRatingScaleQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below automation frameworks", questions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }


        [TestMethod]
        public void ValidateAddPage()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            var pageList = UIActions.FindElements(driver, ".page-list");
            AuthorActions.AddPage(driver);
            var newPageList = UIActions.FindElements(driver, ".page-list");
            Assert.IsTrue(newPageList.Count > pageList.Count);
        }

        [TestMethod]
        public void ValidateAddMultipleRatingScaleQuestionWithScaleRangeAndHeaders()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] questions = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            string[] scaleHeaders = { "One", "Two", "Three", "Four", "Five", "Six" };
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, "Rate the below new automation frameworks", questions, "1-6", scaleHeaders);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [TestMethod]
        public void ValidateAddSection()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            var sectionList = UIActions.FindElements(driver, ".section-list");
            AuthorActions.AddSection(driver);
            var newSectionList = UIActions.FindElements(driver, ".section-list");
            Assert.IsTrue(newSectionList.Count > sectionList.Count);
        }

        [TestMethod]
        public void ValidateAddMultipleChoiceSingleItem()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceQuestion(driver, "Choose you Department", responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [TestMethod]
        public void ValidateAddMultipleChoiceMultiItem()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            string[] questions = { "First", "Second", "Third" };
            string[] responseOptions = { "HR", "IT", "Finance", "Sales" };
            AuthorActions.AddMultipleChoiceMultiItemQuestion(driver, "Chose your favorite Departments", questions, responseOptions);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }



        [TestMethod]

        public void ValidateAddDropDownSingleSelectQuestion()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            AuthorActions.AddPage(driver);
            Thread.Sleep(3000);
            string[] responseOptions = { "Director", "Individual Contributor/Employee", "Manager/Supervisor", "VP and Higher" };
            AuthorActions.AddDropDownQuestion(driver, "Which of the following best describes your level in the organization?", responseOptions);
            //Thread.Sleep(1000);

            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [TestMethod]
        public void ValidateAddCommentQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            AuthorActions.AddSection(driver);
            Thread.Sleep(3000);
            AuthorActions.AddCommentQuestion(driver, "Please take this time to let us know anything about your benefits that you have not already had a chance to express.");
            //Thread.Sleep(1000);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
        }

        [TestMethod]
        public void ValidateAddLogicWithTwoCases()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            AuthorActions.DragAndDropLogic(driver, 1);
            Thread.Sleep(5000);
            var logicbuilder = driver.FindElement(By.Id("Add-Logic-modal"));
            if (logicbuilder != null)
            {
                Thread.Sleep(1000);
                AuthorActions.AddLogic(driver, "Hide", "1", new string[] { "Q3.1.Selenium with Java" }, "All of", new LogicCaseDto[]
                {
                    new LogicCaseDto
                    {
                        SourceType = "1",
                        SourceItem =  "Q1. How do you rate this Automation framework?",
                        SourceCondition = "Equal To",
                        SourceLogic = "Any of",
                        SourceResponses = new string[] { "1","2"}
                    },
                    new LogicCaseDto
                    {
                        SourceType = "1",
                        SourceItem =  "Q2. How do you rate the old Automation framework?",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "1" }
                    }

                });
                Thread.Sleep(10000);


                logicbuilder = UIActions.GetElement(driver, "#Add-Logic-modal");
                Assert.IsFalse(logicbuilder != null && logicbuilder.Displayed);

            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateAddSectionLogic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(5000);
            AuthorActions.DragAndDropLogic(driver, 1);
            Thread.Sleep(5000);
            var logicbuilder = driver.FindElement(By.Id("Add-Logic-modal"));
            if (logicbuilder != null)
            {
                Thread.Sleep(1000);
                AuthorActions.AddLogic(driver, "Hide", "3", new string[] { "Section 3", "Section 4" }, "All of", new LogicCaseDto[]
                {
                    new LogicCaseDto
                    {
                        SourceType = "2",
                        SourceItem =  "Country ",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "Russia"}
                    },
                    new LogicCaseDto
                    {
                        SourceType = "1",
                        SourceItem =  "Q3.1. Selenium with Java ",
                        SourceCondition = "Less Than",
                        SourceLogic = "Any of",
                        SourceResponses = new string[] { "4","5" }
                    }

                });
                Thread.Sleep(10000);

                logicbuilder = UIActions.GetElement(driver, "#Add-Logic-modal");
                Assert.IsFalse(logicbuilder != null && logicbuilder.Displayed);

            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateAddPageLogic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(5000);
            AuthorActions.DragAndDropLogic(driver, 1);
            Thread.Sleep(5000);
            var logicbuilder = driver.FindElement(By.Id("Add-Logic-modal"));
            if (logicbuilder != null)
            {
                Thread.Sleep(1000);
                AuthorActions.AddLogic(driver, "Hide", "2", new string[] { "Page 3" }, "All of", new LogicCaseDto[]
                {
                    new LogicCaseDto
                    {
                        SourceType = "1",
                        SourceItem =  "Q3.2. Selenium with C# ",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "3"}
                    }

                });
                Thread.Sleep(10000);

                logicbuilder = UIActions.GetElement(driver, "#Add-Logic-modal");
                Assert.IsFalse(logicbuilder != null && logicbuilder.Displayed);

            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ValidateBenchmarkMappingForRatingScaleSingleItem()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.AddBenchmarkMappingSingleRatingScale(driver, "Rating Item New", " How meaningful is your work?");
            var element = UIActions.GetElementWithWait(driver, ".status-area-benchmark-mapping", 10);
            Assert.AreEqual(element.Text, "Benchmark Mapping successful.");
        }

        [TestMethod]
        public void ValidateBenchmarkMappingForMultipleChoiceSingleItem()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.AddBenchmarkMappingSingleMultiChoice(driver, "Multichoice Single New", " What is your education level?", new List<string> { " No High School", "High School Graduate", "Attended College", " 2 - year Degree", " 4 - year Degree" });
            var element = UIActions.GetElementWithWait(driver, ".status-area-benchmark-mapping", 10);
            Assert.AreEqual(element.Text, "Benchmark Mapping successful.");
        }

        [TestMethod]
        public void ValidateBenchmarkMappingForDropdown()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.AddBenchmarkMappingDropdown(driver, "New Dropdown", "How old are you?", new List<string> { "25 and under", "26-35", "36-45", "Over 65" });
            var element = UIActions.GetElementWithWait(driver, ".status-area-benchmark-mapping", 10);
            Assert.AreEqual(element.Text, "Benchmark Mapping successful.");
        }

        [TestMethod]
        public void ValidateBenchmarkMappingForRatingScaleMultiItem()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.AddBenchmarkMappingMultiRatingScale(driver, "​Multi Item Rating Scale New", "Medical and Retirement benefits", new List<string> { " My retirement benefits (401(k) and pensi...", " My retirement savings plan includes mone...", " I have a good understanding of our pensi..." });
            var element = UIActions.GetElementWithWait(driver, ".status-area-benchmark-mapping", 10);
            Assert.AreEqual(element.Text, "Benchmark Mapping successful.");
        }

        [TestMethod]
        public void ValidateBenchmarkMappingForMultipleChoiceMultiItem()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.AddBenchmarkMappingMultiChoiceMultiItem(driver, "MCQ Multi Item",
                "Transporation Details",
                new List<string> { " How did you travel to office with your p...", " What mode of transport would you prefer ..." }, new List<string> { "Office Bus_Updated", "Public Transport_Updated" });

            var element = UIActions.GetElementWithWait(driver, ".status-area-benchmark-mapping", 10);
            Assert.AreEqual(element.Text, "Benchmark Mapping successful.");
        }

        [TestMethod]
        public void ValidateBenchmarkMappingGridWithData()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.OpenBenchmarkMappingGridModal(driver);
            var benchmarkMappingGrid = UIActions.GetElement(driver, "#benchmark-map-grid");
            Assert.IsTrue(benchmarkMappingGrid.Displayed);
        }

        [TestMethod]
        public void ValidateEditBenchmarkMappingFromGrid()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.OpenBenchmarkMappingGridModal(driver);
            var benchmarkMappingGrid = UIActions.GetElement(driver, "#benchmark-map-grid");
            if (benchmarkMappingGrid.Displayed)
            {
                AuthorActions.ClickEditBenchmarkMappingOnGrid(driver, "New Dropdown");
                var element = UIActions.GetElement(driver, "#select-benchmark-question + .ceb-dropdown-container");
                Assert.IsTrue(element.Displayed);
            }
            else
                Assert.Fail();
        }

        [TestMethod]
        public void ValidateDeleteBenchmarkMappingFromGrid()
        {
            FormName = "Copy Of Benchmark Mapping Test";
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.OpenBenchmarkMappingGridModal(driver);
            var benchmarkMappingGrid = UIActions.GetElement(driver, "#benchmark-map-grid");
            if (benchmarkMappingGrid.Displayed)
            {
                AuthorActions.ClickDeleteBenchmarkMappingOnGrid(driver, "New Dropdown");
                var element = UIActions.GetElement(driver, "#divStatusForBenchmarkMappingGrid #action-status");
                Assert.IsTrue(element.Displayed && element.Text == "Benchmark Mapping removed successfully");
            }
            else
                Assert.Fail();
        }

        [TestMethod]
        public void ValidateBenchmarkMappingGridWithoutData()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            FormName = "";
            OpenOrCreateForm(driver);
            AuthorActions.OpenBenchmarkMappingGridModal(driver);
            var benchmarkMappingMessage = UIActions.GetElement(driver, "#nobenchmarkMappingMssg");
            Assert.IsTrue(benchmarkMappingMessage.Displayed && benchmarkMappingMessage.Text.Trim() == "No Benchmark Mapping available.");
        }

        [TestMethod]
        public void ValidateAddPageSectionWithInformationQuestion()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.AddInformationText(driver, "Welcome! Hi Please take survey");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());

        }

        [TestMethod]
        public void validateDragAndDropLogic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(2000);
            AuthorActions.DragAndDropLogic(driver, 1);
            var hasLogicBuilderOpened = Convert.ToBoolean(UIActions.GetAttrForElementById(driver, "Add-Logic-modal", "aria-hidden"));
            Assert.AreEqual(false, hasLogicBuilderOpened);

        }

        [TestMethod]
        public void ValidateDragAndDropPage()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var PageBeforeAdding = UIActions.FindElements(driver, ".page-list");
            int CountOfPageBeforeAdding = PageBeforeAdding.Count();
            Console.WriteLine(CountOfPageBeforeAdding);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(5000);
            var PageafterAdding = UIActions.FindElements(driver, ".page-list");
            int CountOfPageAfterAdding = PageafterAdding.Count();
            Console.WriteLine(CountOfPageAfterAdding);
            Assert.AreNotEqual(CountOfPageAfterAdding, CountOfPageBeforeAdding);


        }

        [TestMethod]
        public void ValidateDragAndDropSection()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var SectionBeforeAdding = UIActions.FindElements(driver, ".section-list");
            int CountOfsectionBeforeAdding = SectionBeforeAdding.Count();
            Console.WriteLine(CountOfsectionBeforeAdding);
            AuthorActions.DragAndDropSection(driver);
            Thread.Sleep(2000);
            var SectionafterAdding = UIActions.FindElements(driver, ".section-list");
            int CountOfSectionAfterAdding = SectionafterAdding.Count();
            Console.WriteLine(CountOfSectionAfterAdding);
            Assert.AreNotEqual(SectionBeforeAdding, CountOfSectionAfterAdding);
        }


        [TestMethod]
        public void ValidateDragAndDropInformationQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(2000);
            AuthorActions.DragAndDrop(driver, "#page-content-text", "question-list");
            Thread.Sleep(5000);
            var Questiontext = "Information" + Guid.NewGuid();
            var infoquestionbox = UIActions.GetElement(driver, "#PageContentTextIncludeContext + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(Questiontext);
            UIActions.clickwithID(driver, "btnTextContentSave");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Information"));
            Thread.Sleep(10000);
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            Console.WriteLine(TextToBeVerified);
            Assert.AreEqual(TextToBeVerified, Questiontext);
        }


        [TestMethod]
        public void ValidateDragAndDropRatingScaleQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(2000);
            AuthorActions.DragAndDropwithCSS(driver, "#add-rating-scale", ".section-list.ui-droppable.focus-component");
            var Questiontext = "RatingScale" + Guid.NewGuid();
            AuthorActions.AddRatingScaleQuestion(driver, Questiontext);
            Thread.Sleep(1000);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("RatingScale"));
            Thread.Sleep(10000);
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, Questiontext);

        }


        [TestMethod]
        public void ValidateDragAndDropRatingscaleMultiItem()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(4000);
            AuthorActions.DragAndDropwithCSS(driver, "#add-rating-scale", ".section-list.ui-droppable.focus-component");
            string[] questions = { "RRatingScaleQuestion1_" + Guid.NewGuid(), "RRatingScaleQuestion2_" + Guid.NewGuid(), "RRatingScaleQuestion3_" + Guid.NewGuid() };
            var instructiontext = "Rating Scale Multi item Instruction text" + Guid.NewGuid();
            AuthorActions.AddMultiItemRatingScaleQuestion(driver, instructiontext, questions);
            Thread.Sleep(1000);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Rating Scale Multi item Instruction text"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var VerifyInstructionAddition = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(VerifyInstructionAddition);
            Assert.AreEqual(VerifyInstructionAddition, instructiontext);
            for (int i = 0; i < questions.Length; i++)
            {
                var ElementvisibleForSubQuestion = driver.FindElements(By.CssSelector(".section-list.ui-droppable.focus-component span[data-bind='displayText']"));
                Thread.Sleep(2000);
                string ElementWithQuestionTextToBeVerified = ElementvisibleForSubQuestion.ElementAt(i).Text;
                Console.WriteLine(ElementWithQuestionTextToBeVerified);
                Assert.AreEqual(ElementWithQuestionTextToBeVerified, questions[i]);
            }

        }


        [TestMethod]
        public void ValidateDragAndDropMultichoice()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(4000);
            AuthorActions.DragAndDropwithCSS(driver, "#add-multiple-choice", ".section-list.ui-droppable.focus-component");
            var Questiontext = "Multichoice" + Guid.NewGuid();
            string[] Responses = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            AuthorActions.AddMultipleChoiceQuestion(driver, Questiontext, Responses);
            Thread.Sleep(1000);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Multichoice"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, Questiontext);
        }


        [TestMethod]
        public void ValidateDragAndDropMultichoiceMultiItem()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(4000);
            AuthorActions.DragAndDropwithCSS(driver, "#add-multiple-choice", ".section-list.ui-droppable.focus-component");
            string[] questions = { "MultichoiceMultiItem1_" + Guid.NewGuid(), "MultiChoiceMultiItem2" + Guid.NewGuid(), "MultiChoiceMultiItem3" + Guid.NewGuid() };
            var instructiontext = "Multi Choice Multi item Instruction text" + Guid.NewGuid();
            string[] Responses = { "Selenium with Java", "Selenium with C#", "Selenium with NUnit" };
            AuthorActions.AddMultipleChoiceMultiItemQuestion(driver, instructiontext, questions, Responses);
            Thread.Sleep(1000);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Multi Choice Multi item Instruction text"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var VerifyInstructionAddition = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(VerifyInstructionAddition);
            Assert.AreEqual(VerifyInstructionAddition, instructiontext);
            for (int i = 0; i < questions.Length; i++)
            {
                var ElementvisibleForSubQuestion = driver.FindElements(By.CssSelector(".section-list.ui-droppable.focus-component span[data-bind='displayText']"));
                Thread.Sleep(2000);
                string ElementWithQuestionTextToBeVerified = ElementvisibleForSubQuestion.ElementAt(i).Text;
                Console.WriteLine(ElementWithQuestionTextToBeVerified);
                Assert.AreEqual(ElementWithQuestionTextToBeVerified, questions[i]);
            }
        }


        [TestMethod]
        public void ValidateDragAndDropCommentQuestions()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(4000);
            AuthorActions.DragAndDropwithCSS(driver, "#add-Text", ".section-list.ui-droppable.focus-component");
            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Longcomment"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
            AuthorActions.DragAndDropwithCSS(driver, "#add-Text", ".section-list.ui-droppable.focus-component");
            var ShortQuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, ShortQuestionText);
            var shortmessageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement IsElementvisible = driver.FindElement(By.PartialLinkText("Shortcomment"));
            var ShortTextToBeVerified = IsElementvisible.Text.TrimStart();
            var ShortAddedQuestiontoSurvey = ShortTextToBeVerified.Substring(ShortTextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(ShortAddedQuestiontoSurvey);
            Assert.AreEqual(ShortAddedQuestiontoSurvey, ShortQuestionText);

        }

        [TestMethod]
        public void ValidateAddNumberQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatNumberQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "Number");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatNumberQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }


        [TestMethod]
        public void ValidateDragAndDropFixedFormatQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            AuthorActions.DragAndDropPage(driver);
            Thread.Sleep(4000);
            AuthorActions.DragAndDropwithCSS(driver, "#add-fixed-format", ".section-list.ui-droppable.focus-component");
            var QuestionText = "FixedFormatDATEQuestion" + Guid.NewGuid();
            String[] CategoryName = { "Demographic" };
            AuthorActions.AddFixedFormatDateQuestionAsDemo(driver, QuestionText, CategoryName);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatDATEQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }

        [TestMethod]
        public void ValidateAddLongCommentQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var QuestionText = "Longcommentquestion" + Guid.NewGuid();
            AuthorActions.AddLongCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Longcomment"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);

        }

        [TestMethod]
        public void ValidateAddShortCommentQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var QuestionText = "Shortcommentquestion" + Guid.NewGuid();
            AuthorActions.AddShortCommentQuestion(driver, QuestionText);
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("Shortcomment"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }

        [TestMethod]
        public void ValidateAddWebsiteURLQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatWebSiteURLQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_website_url");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatWebSiteURLQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }

        [TestMethod]
        public void ValidateAddEmailAddressQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            var QuestionText = "FixedFormatEmailAddressQuestion" + Guid.NewGuid();
            AuthorActions.AddFixedFormatQuestion(driver, QuestionText, "format_email_address");
            var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
            Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
            Thread.Sleep(2000);
            IWebElement Elementvisible = driver.FindElement(By.PartialLinkText("FixedFormatEmailAddressQuestion"));
            var TextToBeVerified = Elementvisible.Text.TrimStart();
            var AddedQuestiontoSurvey = TextToBeVerified.Substring(TextToBeVerified.IndexOf('.') + 1).TrimStart();
            Console.WriteLine(AddedQuestiontoSurvey);
            Assert.AreEqual(AddedQuestiontoSurvey, QuestionText);
        }

        [TestMethod]
        public void ValidateOpenNewtab()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            UIActions.OpenNewTab(driver, "https://www.google.co.in");
            UIActions.SwitchWindows(driver, "Home Page - Pulse");
            Thread.Sleep(2000);
            string PageTitle = driver.Title;
            Assert.AreEqual("Home Page - Pulse", PageTitle);
        }


        [TestMethod]
        public void ValidatePublishForm()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null && driver.Url.Contains("ManageForm/"))
            {
                Thread.Sleep(5000);
                UIActions.Click(driver, "#formHeader .pub");
                var pubConfirm = UIActions.GetElementWithWait(driver, "#publish-form-modal", 5);
                UIActions.Click(driver, "#btnConfirmPublishClicked");
                var pubSuccess = UIActions.GetElementWithWait(driver, "#publishPopUpModal", 45);
                if (pubSuccess != null)
                {
                    UIActions.Click(driver, "#publishPopUpModal button.close");
                    var editSurvey = UIActions.GetElementWithWait(driver, "#btnFormEdit", 10);
                    Assert.AreEqual(true, editSurvey != null);
                }
                else
                    Assert.Fail();
            }
            else
                Assert.Fail();
        }

        #region SurveySettings-Bulkmandatoryquestion

        [TestMethod]
        public void selectmandatorybulkquestions()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            //AuthorActions.NavigateToAuthor(driver);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            Thread.Sleep(3000);
            //AuthorActions.AddRatingScaleQuestion(driver, "AddSingleRatingScaleQuestion", "1-5", new string[] { "SH1", "SH2", "SH3", "SH4", "SH5" });
            //Thread.Sleep(3000);
            //AuthorActions.AddMultiItemRatingScaleQuestion(driver, "MultipleRatingScaleInstructions", new string[] { "AddMultipleQues1", "AddMultipleQues2", "AddMultipleQues3" }, "1-5", new string[] { "SH1", "SH2", "SH3", "SH4", "SH5" });
            //Thread.Sleep(3000);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            string _isMandatoryMsgDisplayed = AuthorActions.SetAsMandatory(driver);
            Assert.IsTrue(_isMandatoryMsgDisplayed.Contains("changed to Mandatory status."));
        }

       [TestMethod]
        public void VerifyMandatoryQuestionInFormBuilder()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(3000);
            Assert.IsTrue(UIActions.GetElementswithXpath(driver, "//span[@class='question-text true']", 45).Displayed);
        }

        [TestMethod]
        public void VerifyMandatoryQuestionInPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            //AuthorActions.AddRatingScaleQuestion(driver, "AddSingleRatingScaleQuestion", "1-5", new string[] { "SH1", "SH2", "SH3", "SH4", "SH5" });
            //Thread.Sleep(6000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(8000);
            AuthorActions.capturePreviewLink(driver);
            Thread.Sleep(3000);
            string _arrVal = UIActions.GetAttrForElementByXpath(driver, "//span[@class='question-text true']", "data-bind");
            Assert.IsTrue(_arrVal.Equals("isMandatory"));
            // Assert.IsTrue(UIActions.GetElementwithXpath(driver, "//span[@class='question-text true']", 45).Displayed);
        }

        [TestMethod]

        public void VerifyMandatoryQuestionInPlayer()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            //AuthorActions.AddRatingScaleQuestion(driver, "AddSingleRatingScaleQuestion", "1-5", new string[] { "SH1", "SH2", "SH3", "SH4", "SH5" });
            //Thread.Sleep(6000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(3000);
            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(4000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "BulkMandate" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });
            UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']").Click();
            driver.Navigate().GoToUrl(Clipboard.GetText());
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//span[@class='question-text true']")));
            string _arrVal = UIActions.GetAttrForElementByXpath(driver, "//span[@class='question-text true']", "data-bind");
            Assert.IsTrue(_arrVal.Equals("isMandatory"));
        }

        [TestMethod]
        public void deselectmandatoryquestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            //AuthorActions.AddRatingScaleQuestion(driver, "AddSingleRatingScaleQuestion", "1-5", new string[] { "SH1", "SH2", "SH3", "SH4", "SH5" });
            //Thread.Sleep(6000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(5000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.GetElementswithXpath(driver, "//div[@class='btn-group ceb-dropdown-container dropup']/button", 45).Click();
            IReadOnlyCollection<IWebElement> _ddOptions1 = driver.FindElements(By.XPath("//div[@class='btn-group ceb-dropdown-container dropup open']/ul//label"));
            _ddOptions1.FirstOrDefault(x => x.Text.Trim() == "All").Click();
            string _isMandatoryMsgDisplayed = UIActions.GetElement(driver, "#mandatory-question-types-warning").Text;
            Thread.Sleep(3000);
            Assert.IsTrue(_isMandatoryMsgDisplayed.Contains("changed to Non-Mandatory/Not Required status."));
        }

        [TestMethod]
        public void deselectmandatoryinquestionlevel()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool result = false;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(8000);
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
            if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
            {
                UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 45).Click();
            }
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _editorSections = UIActions.FindElementsWithXpath(driver, "//h4[@class='panel-title']/span");
            _editorSections.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(4000);
            if (UIActions.GetElement(driver, "#chkMandatory").Selected)
            {
                UIActions.GetElement(driver, "#chkMandatory").Click();
            }
            Thread.Sleep(4000);
            UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
            Thread.Sleep(4000);
            IReadOnlyCollection<IWebElement> titleList1 = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            OpenQA.Selenium.Interactions.Actions ac1 = new OpenQA.Selenium.Interactions.Actions(driver);
            ac1.MoveToElement(titleList1.First(x => x.Text.Contains("AddShortCommentQuestion"))).Build().Perform();
            result = UIActions.IsElementNotVisible(driver, "//span[@class='question-text true']");
            Assert.IsTrue(result);

        }

        [TestMethod]
        public void deselectmandatoryinquestionlevelInPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(7000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
            //UIActions.GetElementwithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 45).Click();
            if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
            {
                UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 45).Click();
            }
            
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _editorSections = UIActions.FindElementsWithXpath(driver, "//h4[@class='panel-title']/span");
            _editorSections.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(4000);
            if (UIActions.GetElement(driver, "#chkMandatory").Selected)
            {
                UIActions.GetElement(driver, "#chkMandatory").Click();
            }
            Thread.Sleep(4000);
            UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
            Thread.Sleep(4000);
            IReadOnlyCollection<IWebElement> titleList1 = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            OpenQA.Selenium.Interactions.Actions ac1 = new OpenQA.Selenium.Interactions.Actions(driver);
            Thread.Sleep(2000);
            ac1.MoveToElement(titleList1.First(x => x.Text.Contains("AddShortCommentQuestion"))).Build().Perform();
            Thread.Sleep(7000);
            UIActions.IsElementNotVisible(driver, "//span[@class='question-text true']");
            AuthorActions.capturePreviewLink(driver);
            //  RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/33c541ff9ea246b2bf08c959e7a82d95");
            Thread.Sleep(3000);
            try
            {
                IWebElement _mandatorySymbol = driver.FindElement(By.XPath("//span[@class='question-text false']"));
                _result = _mandatorySymbol.Displayed ? true : false;
            }
            catch (Exception)
            {

                IWebElement _mandatorySymbol = driver.FindElement(By.XPath("//span[@class='question-text true']"));
                _result = false;
            }
            Assert.IsTrue(_result);
        }

        [TestMethod]
        public void deselectmandatoryinquestionlevelInPlayer()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(8000);
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
            if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
            {
                UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 45).Click();
            }
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _editorSections = UIActions.FindElementsWithXpath(driver, "//h4[@class='panel-title']/span");
            _editorSections.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(4000);
            if (UIActions.GetElement(driver, "#chkMandatory").Selected)
            {
                UIActions.GetElement(driver, "#chkMandatory").Click();
            }
            Thread.Sleep(4000);
            UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
            Thread.Sleep(4000);
            IReadOnlyCollection<IWebElement> titleList1 = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            OpenQA.Selenium.Interactions.Actions ac1 = new OpenQA.Selenium.Interactions.Actions(driver);
            ac1.MoveToElement(titleList1.First(x => x.Text.Contains("AddShortCommentQuestion"))).Build().Perform();
            UIActions.IsElementNotVisible(driver, "//span[@class='question-text true']");

            string surveyForDistribution = DistributeActions.PublishForm(driver);
            Thread.Sleep(4000);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {
                FormName = surveyForDistribution,
                DistributionName = "BulkMandate" + DateTime.Now,
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });
            UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']").Click();
            driver.Navigate().GoToUrl(Clipboard.GetText());
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3000));
            Thread.Sleep(3000);
            try
            {
                IWebElement _mandatorySymbol = driver.FindElement(By.XPath("//span[@class='question-text false']"));
                _result = _mandatorySymbol.Displayed;
            }
            catch (Exception)
            {

                IWebElement _mandatorySymbol = driver.FindElement(By.XPath("//span[@class='question-text true']"));
                _result = false;
            }
            Assert.IsTrue(_result);

        }
        [TestMethod]

        public void copymandatoryquestionform()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            String formName = "BulkMandatory" + DateTime.Now;
             AuthorActions.CreateForm(driver,formName);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(7000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            AuthorActions.SetAsMandatory(driver);
            Thread.Sleep(3000);
            Assert.IsTrue(UIActions.GetElementswithXpath(driver, "//span[@class='question-text true']", 45).Displayed);
            Thread.Sleep(2000);
            string surveyForDistribution = driver.FindElement(By.XPath("//h2[@class='primary bolder surveyTitle-wrap']")).Text;
            var publishBtn = UIActions.FindElementsWithXpath(driver, "//li/button[@type='button']");
            Thread.Sleep(5000);
            if (driver != null && driver.Url.Contains("ManageForm/"))
            {
                Thread.Sleep(5000);
                UIActions.Click(driver, "#formHeader .pub");
                var pubConfirm = UIActions.GetElementWithWait(driver, "#publish-form-modal", 5);
                UIActions.Click(driver, "#btnConfirmPublishClicked");
                var pubSuccess = UIActions.GetElementWithWait(driver, "#publishPopUpModal", 45);
                if (pubSuccess != null)
                {
                    UIActions.Click(driver, "#publishPopUpModal button.close");
                    var editSurvey = UIActions.GetElementWithWait(driver, "#btnFormEdit", 10);

                    AuthorActions.NavigateToAuthor(driver);
                    Thread.Sleep(7000);

                    var newFormName = formName + " " + Guid.NewGuid();
                    
                    IWebElement formBuilder = AuthorActions.CopyForm(driver, formName, newFormName);
                    Thread.Sleep(5000);
                    Assert.IsTrue(UIActions.GetElementswithXpath(driver, "//span[@class='question-text true']", 45).Displayed);


                }


            }
        }
        #endregion

        #region Text Substitutions

        [TestMethod]
    public void ValidateAddNewTextSubstitution()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
        AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

        var addNewSubstitutionButton = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//a[@widget-data-event='click::AddNewTextSubstitution']", 5);
        addNewSubstitutionButton.Click();

        var labelElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-text'][last()]", 5);
        var valueElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-value'][last()]", 5);

        labelElement.Clear();
        labelElement.SendKeys("Label_" + Guid.NewGuid());

        valueElement.Clear();
        valueElement.SendKeys("Value_" + Guid.NewGuid());

        UIActions.Click(driver, "#btnFormSettingsSave");

        Thread.Sleep(5000);

        var statusArea = UIActions.GetElementswithXpath(driver, "(//span[@id='action-status'])[2]", 0);

        Assert.AreEqual("Text Substitutions updated successfully.", statusArea.Text);
    }

    [TestMethod]
    public void ValidateAddMultipleNewTextSubstitution()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);

        AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

        for (int i = 0; i < 3; i++)
        {
            var addNewSubstitutionButton = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//a[@widget-data-event='click::AddNewTextSubstitution']", 5);
            addNewSubstitutionButton.Click();

            var labelElement = UIActions.GetElementswithXpath(driver, "(//div[@id='text-substitution']//input[@class='form-control label-text'])[" + (i + 1) + "]", 1);
            var valueElement = UIActions.GetElementswithXpath(driver, "(//div[@id='text-substitution']//input[@class='form-control label-value'])[" + (i + 1) + "]", 1);

            labelElement.Clear();
            labelElement.SendKeys("Label_" + Guid.NewGuid());

            valueElement.Clear();
            valueElement.SendKeys("Value_" + Guid.NewGuid());
        }

        UIActions.Click(driver, "#btnFormSettingsSave");

        Thread.Sleep(5000);

            var statusArea = UIActions.GetElementswithXpath(driver, "(//span[@id='action-status'])[2]", 0);

        Assert.AreEqual("Text Substitutions updated successfully.", statusArea.Text);
    }

    [TestMethod]
    public void ValidateUpdateNewTextSubstitution()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
        if (driver != null && driver.Url.Contains("ManageForm/"))
        {
            Thread.Sleep(5000);
            AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

            var addNewSubstitutionButton = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//a[@widget-data-event='click::AddNewTextSubstitution']", 5);
            addNewSubstitutionButton.Click();

            var labelElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-text'][last()]", 5);
            var valueElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-value'][last()]", 5);

            labelElement.Clear();
            labelElement.SendKeys("Label_" + Guid.NewGuid());

            valueElement.Clear();
            valueElement.SendKeys("Value_" + Guid.NewGuid());

            UIActions.Click(driver, "#btnFormSettingsSave");

            Thread.Sleep(5000);

            AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

            labelElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-text'][last()]", 5);
            valueElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-value'][last()]", 5);

            labelElement.Clear();
            labelElement.SendKeys("Label_" + Guid.NewGuid());

            valueElement.Clear();
            valueElement.SendKeys("Value_" + Guid.NewGuid());

            UIActions.Click(driver, "#btnFormSettingsSave");

            Thread.Sleep(5000);

            var statusArea = UIActions.GetElementswithXpath(driver, "(//span[@id='action-status'])[2]", 0);

            Assert.AreEqual("Text Substitutions updated successfully.", statusArea.Text);
        }
        else
            Assert.Fail();
    }

    [TestMethod]
    public void ValidateTextSubstitutionSpecialCharacters()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
        if (driver != null && driver.Url.Contains("ManageForm/"))
        {
            Thread.Sleep(5000);
            AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

            var addNewSubstitutionButton = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//a[@widget-data-event='click::AddNewTextSubstitution']", 5);
            addNewSubstitutionButton.Click();

            var labelElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-text'][last()]", 5);
            var valueElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-value'][last()]", 5);

            labelElement.Clear();
            labelElement.SendKeys("Label_" + "~!@#$%^&*()_+=-`[]{};':<>?,.\\/");

            valueElement.Clear();
            valueElement.SendKeys("Value_" + "~!@#$%^&*()_+=-`[]{};':<>?,.\\/");

            UIActions.Click(driver, "#btnFormSettingsSave");

            Thread.Sleep(5000);

            var statusArea = UIActions.GetElementswithXpath(driver, "(//span[@id='action-status'])[2]", 0);

            Assert.AreEqual("Text Substitutions updated successfully.", statusArea.Text);
        }
        else
            Assert.Fail();
    }

    [TestMethod]
    public void ValidateTextSubstitutionDoubleQuote()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
        if (driver != null && driver.Url.Contains("ManageForm/"))
        {
            Thread.Sleep(5000);
            AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

            var addNewSubstitutionButton = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//a[@widget-data-event='click::AddNewTextSubstitution']", 5);
            addNewSubstitutionButton.Click();

            var labelElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-text'][last()]", 5);
            var valueElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-value'][last()]", 5);

            labelElement.Clear();
            labelElement.SendKeys("Label_" + "\"");

            valueElement.Clear();
            valueElement.SendKeys("Value_" + "\"");

            UIActions.Click(driver, "#btnFormSettingsSave");

            Thread.Sleep(5000);

            var statusArea = UIActions.GetElementswithXpath(driver, "(//span[@id='action-status'])[2]", 0);

            Assert.AreEqual("Text Substitutions updated successfully.", statusArea.Text);
        }
        else
            Assert.Fail();
    }

    [TestMethod]
    public void ValidateTextSubstitutionPipeHandlingForLabel()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
        if (driver != null && driver.Url.Contains("ManageForm/"))
        {
            Thread.Sleep(5000);
            AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

            var addNewSubstitutionButton = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//a[@widget-data-event='click::AddNewTextSubstitution']", 5);
            addNewSubstitutionButton.Click();

            var labelElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-text'][last()]", 5);
            var valueElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-value'][last()]", 5);

            labelElement.Clear();
            labelElement.SendKeys("Label_" + "|");

            valueElement.Clear();
            valueElement.SendKeys("Value_" + Guid.NewGuid());

            UIActions.Click(driver, "#btnFormSettingsSave");

            Thread.Sleep(5000);

            var statusArea = UIActions.GetElementswithXpath(driver, "(//span[@id='action-status'])[2]", 0);

            Assert.AreEqual("Text Substitutions updated successfully.", statusArea.Text);
        }
        else
            Assert.Fail();
    }

    [TestMethod]
    public void ValidateTextSubstitutionPipeHandlingForValue()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
        if (driver != null && driver.Url.Contains("ManageForm/"))
        {
            Thread.Sleep(5000);
            AuthorActions.OpenSettingsModalTab(driver, "Text Substitution");

            var addNewSubstitutionButton = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//a[@widget-data-event='click::AddNewTextSubstitution']", 5);
            addNewSubstitutionButton.Click();

            var labelElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-text'][last()]", 5);
            var valueElement = UIActions.GetElementswithXpath(driver, "//div[@id='text-substitution']//input[@class='form-control label-value'][last()]", 5);

            labelElement.Clear();
            labelElement.SendKeys("Label_" + Guid.NewGuid());

            valueElement.Clear();
            valueElement.SendKeys("Value_" + "|");

            UIActions.Click(driver, "#btnFormSettingsSave");

            Thread.Sleep(5000);

            var statusArea = UIActions.GetElementswithXpath(driver, "(//span[@id='action-status'])[2]", 0);

            Assert.AreEqual("Text Substitutions updated successfully.", statusArea.Text);
        }
        else
            Assert.Fail();
    }

    #endregion

        #region Demo restriction
    [TestMethod]
    public void ValidateDemoRestrictionAddSingleCaseWithQuestion()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
        if (driver != null)
        {
            Thread.Sleep(20000);

            //AuthorActions.SearchAndOpenPermissions(driver, "Demo Logic testing survey");

            var formName = "Demo Logic testing survey";
            var newFormName = formName + " " + Guid.NewGuid();

            var formBuilder = AuthorActions.CopyForm(driver, formName, newFormName);
            Thread.Sleep(20000);

            NavigateToAuthor(driver);
            Thread.Sleep(20000);

            AuthorActions.SearchAndOpenPermissions(driver, newFormName);

            Thread.Sleep(1000);
            AuthorActions.SearchUserAndSelect(driver, "sm_sanal@cebglobal.com");
            UIActions.Click(driver, "#rdAddAccess");
            UIActions.Click(driver, "#demodatarestriction-yeswithconditions");
            Thread.Sleep(5000);
            AuthorActions.AddDemoRestrictionLogic(driver, "Hide", "2", new List<LogicCaseDto>()
                {
                    new LogicCaseDto()
                    {
                        SourceType = "0",
                        SourceItem =  "What is your role in Pulse?",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "Automation" }
                    }
                });

            UIActions.Click(driver, "#access-save");
            Thread.Sleep(5000);
            var messageArea = UIActions.GetElementWithWait(driver, ".survey-access-modal-status-area", 30);
            Assert.AreEqual("Successfully saved survey permissions.", messageArea.Text.Trim());
        }
    }

        public void AddPage()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
        }
        [TestMethod]
        public void ValidateDemoRestrictionAddSingleCaseWithDemographic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            if (driver != null)
            {
                Thread.Sleep(20000);
                //AuthorActions.SearchAndOpenPermissions(driver, "Demo Logic testing survey");

                var formName = "Demo Logic testing survey-Tellurium";
                var newFormName = formName + " " + Guid.NewGuid();

            var formBuilder = AuthorActions.CopyForm(driver, formName, newFormName);
            Thread.Sleep(20000);

                NavigateToAuthor(driver);
                Thread.Sleep(20000);
                AuthorActions.SearchAndOpenPermissions(driver, newFormName);
                Thread.Sleep(3000);
                
                AuthorActions.SearchUserAndSelect(driver, "sm_sanal@cebglobal.com");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#rdAddAccess");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#demodatarestriction-yeswithconditions");
                Thread.Sleep(5000);
                AuthorActions.AddDemoRestrictionLogic(driver, "Hide", "2", new List<LogicCaseDto>()
                {
                    new LogicCaseDto()
                    {
                        SourceType = "1",
                        SourceItem =  "country",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "India" }
                    }
                });

            UIActions.Click(driver, "#access-save");
            Thread.Sleep(5000);
            var messageArea = UIActions.GetElementWithWait(driver, ".survey-access-modal-status-area", 30);
            Assert.AreEqual("Successfully saved survey permissions.", messageArea.Text.Trim());
        }
    }

    [TestMethod]
    public void ValidateDemoRestrictionAddMultipleCases()
    {
        RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
        if (driver != null)
        {
            Thread.Sleep(20000);
            //AuthorActions.SearchAndOpenPermissions(driver, "Demo Logic testing survey");


                var formName = "Demo Logic testing survey-Tellurium";
                var newFormName = formName + " " + Guid.NewGuid();

                var formBuilder = AuthorActions.CopyForm(driver, formName, newFormName);
                Thread.Sleep(20000);

                NavigateToAuthor(driver);
                Thread.Sleep(20000);

                AuthorActions.SearchAndOpenPermissions(driver, newFormName);
                Thread.Sleep(1000);
                AuthorActions.SearchUserAndSelect(driver, "sm_sanal@cebglobal.com");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#rdAddAccess");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#demodatarestriction-yeswithconditions");
                Thread.Sleep(5000);
                AuthorActions.AddDemoRestrictionLogic(driver, "Hide", "2", new List<LogicCaseDto>()
                {
                    new LogicCaseDto()
                    {
                        SourceType = "1",
                        SourceItem =  "Country",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "India" }
                    },
                    new LogicCaseDto()
                    {
                        SourceType = "0",
                        SourceItem =  "What is your role in Pulse?",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "Automation" }
                    }
                });

            UIActions.Click(driver, "#access-save");
            Thread.Sleep(5000);
            var messageArea = UIActions.GetElementWithWait(driver, ".survey-access-modal-status-area", 30);
            Assert.AreEqual("Successfully saved survey permissions.", messageArea.Text.Trim());
        }
    }

        [TestMethod]
        public void ValidateDemoRestrictionEdit()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            if (driver != null)
            {
                AuthorActions.SearchAndOpenPermissions(driver, "Demo Logic testing survey-Tellurium");
                Thread.Sleep(1000);
                AuthorActions.SearchUserAndSelect(driver, "sm_sanal@cebglobal.com");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#rdAddAccess");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#demodatarestriction-yes-with-conditions-button");
                Thread.Sleep(5000);

                UIActions.Click(driver, "#demodatarestriction-save-LogicBuilder");
                Thread.Sleep(5000);

                UIActions.Click(driver, "#access-save");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, ".survey-access-modal-status-area", 30);
                Assert.AreEqual("Successfully saved survey permissions.", messageArea.Text.Trim());
            }
        }

        [TestMethod]
        public void ValidateDemoRestrictionTurningOff()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            if (driver != null)
            {
                AuthorActions.SearchAndOpenPermissions(driver, "Demo Logic testing survey");
                Thread.Sleep(1000);
                AuthorActions.SearchUserAndSelect(driver, "sm_sanal@cebglobal.com");
                UIActions.Click(driver, "#rdAddAccess");
                UIActions.Click(driver, "#demodatarestriction-no");
                Thread.Sleep(5000);


            UIActions.Click(driver, "#access-save");
            Thread.Sleep(5000);
            var messageArea = UIActions.GetElementWithWait(driver, ".survey-access-modal-status-area", 30);
            Assert.AreEqual("Successfully saved survey permissions.", messageArea.Text.Trim());
        }
    }

        [TestMethod]
        public void ValidateDemoRestrictionTurningOnForUserWithLogic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            if (driver != null)
            {
                AuthorActions.SearchAndOpenPermissions(driver, "Demo Logic testing survey-Tellurium");
                Thread.Sleep(1000);
                AuthorActions.SearchUserAndSelect(driver, "sm_sanal@cebglobal.com");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#rdAddAccess");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#demodatarestriction-yeswithconditions");
                Thread.Sleep(5000);

                UIActions.Click(driver, "#demodatarestriction-save-LogicBuilder");
                Thread.Sleep(5000);

                UIActions.Click(driver, "#access-save");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, ".survey-access-modal-status-area", 30);
                Assert.AreEqual("Successfully saved survey permissions.", messageArea.Text.Trim());
            }
        }

        [TestMethod]
        public void ValidateDemoRestrictionEditWithSingleDemoQuestionSurvey()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            if (driver != null)
            {
                AuthorActions.SearchAndOpenPermissions(driver, "Demo Logic testing survey-Tellurium");
                Thread.Sleep(1000);
                AuthorActions.SearchUserAndSelect(driver, "sm2@cebglobal.com");
                UIActions.Click(driver, "#rdAddAccess");
                UIActions.Click(driver, "#demodatarestriction-yeswithconditions");
                Thread.Sleep(5000);
                AuthorActions.AddDemoRestrictionLogic(driver, "Hide", "2", new List<LogicCaseDto>()
                {
                    new LogicCaseDto()
                    {
                        SourceType = "0",
                        SourceItem =  "What is your role in Pulse?",
                        SourceCondition = "Equal To",
                        SourceLogic = "Exactly",
                        SourceResponses = new string[] { "Automation" }
                    }
                });

                UIActions.Click(driver, "#access-save");
                Thread.Sleep(5000);
                var messageArea = UIActions.GetElementWithWait(driver, ".survey-access-modal-status-area", 30);
                Assert.AreEqual("Successfully saved survey permissions.", messageArea.Text.Trim());
            }
        }
        #endregion

        #region PageName
        [TestMethod]
        public void VerifyPageName()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            string newSurveyName = AuthorActions.EnablePageName(driver, "Employee Benefits Survey");
            Thread.Sleep(10000);
            AuthorActions.SetPageName(driver, "firstPage");
            // AuthorActions.DisablePageName(driver, newSurveyName);
        }

        [TestMethod]
        public void VerifyDisplayNameInPreviewTitle()
        {
            RemoteWebDriver driver = new ChromeDriver();
            
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://stg-surveys.cebglobal.com/player/link/205d27d45ced48d585bd522eca0af535";
            driver.Navigate().GoToUrl(browserProperties.Url);
            driver.Manage().Window.Maximize();
            Thread.Sleep(15000);
            bool isPageNameDisplayed = driver.FindElement(By.Id("txtSurveyNameTitle")).Text.Contains("| firstPage");
            Assert.IsTrue(isPageNameDisplayed);
            driver.Close();
        }

        [TestMethod]
        public void VerifyDisplayNameInPreviewBrowserTitle()
        {
            RemoteWebDriver driver = new ChromeDriver();
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://stg-surveys.cebglobal.com/player/link/205d27d45ced48d585bd522eca0af535";
            driver.Navigate().GoToUrl(browserProperties.Url);
            driver.Manage().Window.Maximize();
            while (driver.Title == null)
            {
                Thread.Sleep(15000);
            }
            bool isPageNameDisplayed = driver.Title.Contains("firstPage");
            Console.WriteLine(isPageNameDisplayed);
            Assert.IsTrue(isPageNameDisplayed);
            driver.Close();
        }

        [TestMethod]
        public void VerifyDisplayNameInPlayerTitle()
        {
            RemoteWebDriver driver = new ChromeDriver();
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://stg-surveys.cebglobal.com/Pulse/Player/Start/22994/0/dca7fbd9-08b2-4687-8b0c-f71f1c2c1445";
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15000));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("txtSurveyNameTitle")));
            bool isPageNameDisplayed = driver.FindElement(By.Id("txtSurveyNameTitle")).Text.Contains("| firstPage");
            Assert.IsTrue(isPageNameDisplayed);
            driver.Close();
            driver.Dispose();
        }

        [TestMethod]
        public void VerifyDisplayNameInPlayerBrowserTitle()
        {
            RemoteWebDriver driver = new ChromeDriver();
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://stg-surveys.cebglobal.com/Pulse/Player/Start/22994/0/dca7fbd9-08b2-4687-8b0c-f71f1c2c1445";
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15000));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("txtSurveyNameTitle")));
            while (driver.Title == null)
            {
                Thread.Sleep(2000);
            }
            bool isPageNameDisplayed = driver.Title.Contains("firstPage");
            Console.WriteLine(isPageNameDisplayed);
            Assert.IsTrue(isPageNameDisplayed);
            driver.Close();
            driver.Dispose();
        }

        [TestMethod]
        public void VerifyDisplayNameNotInPreviewBrowserTitle()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            driver.Manage().Window.Maximize();
            string pageName = "firstPage";
            AuthorActions.EnablePageName(driver, "Employee Benefits Survey");
            AuthorActions.SetPageName(driver, pageName);
            driver.FindElement(By.Id("btnFullPreview")).Click();
            IReadOnlyCollection<String> handle = driver.WindowHandles;
            string lastHandle = handle.Last();
            driver.SwitchTo().Window(lastHandle);
            Thread.Sleep(15000);
            bool isPageNameDisplayed = driver.FindElement(By.Id("txtSurveyNameTitle")).Text.Contains(pageName);
            Assert.IsFalse(isPageNameDisplayed);
            driver.Close();
        }

        [TestMethod]
        public void VerifyDisplayNameNotInPlayerBrowserTitle()
        {
            RemoteWebDriver driver = new ChromeDriver();
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://stg-surveys.cebglobal.com/Pulse/Player/Start/22994/0/dca7fbd9-08b2-4687-8b0c-f71f1c2c1445";
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2000));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("txtSurveyNameTitle")));
            while (driver.Title == null)
            {
                Thread.Sleep(15000);
            }
            bool isPageNameDisplayed = driver.Title.Contains("firstPage");
            Console.WriteLine(isPageNameDisplayed);
            Assert.IsFalse(isPageNameDisplayed);
            driver.Close();
        }

        #endregion PageName
        [TestMethod]
        public void VerifyInfoTextLength()
        {
            string surveyName = null;
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool lenCheck = AuthorActions.AddInformationTextWithLimitCheckCase(driver, "InformationText Form - Do Not change", 3001);
            Assert.IsTrue(lenCheck);

        }

        [TestMethod]
        public void VerifyEnableEditThankYouPage()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            AuthorActions.NavToAccountSettings(driver);
            AuthorActions.EnableThankUInAccSettings(driver);
        }

        [TestMethod]
        public void mandatorybulkquestions()

        {

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);

            //AuthorActions.NavigateToAuthor(driver);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);

            Thread.Sleep(3000);

            AuthorActions.AddRatingScaleQuestion(driver, "AddSingleRatingScaleQuestion", "1-5", new string[] { "SH1", "SH2", "SH3", "SH4", "SH5" });


            Thread.Sleep(3000);

            AuthorActions.OpenSettingsModalTab(driver, "Options");

            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            IReadOnlyCollection<IWebElement> _ddOptions = driver.FindElements(By.XPath("//div[@class='btn-group ceb-dropdown-container dropup open']/ul//label"));
            _ddOptions.FirstOrDefault(x => x.Text.Trim() == "Rating scale").Click();
            bool _isMandatoryMsgDisplayed = UIActions.GetElement(driver, "#mandatory-question-types-warning").Displayed;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            ac.MoveToElement(UIActions.GetElement(driver, "#mandatory-question-types-warning")).Build().Perform();
            UIActions.GetElement(driver, "#btnFormSettingsSave").Click();
            Thread.Sleep(2000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.GetElementswithXpath(driver, "//div[@class='btn-group ceb-dropdown-container dropup']/button", 45).Click();
            IReadOnlyCollection<IWebElement> _ddOptions1 = driver.FindElements(By.XPath("//div[@class='btn-group ceb-dropdown-container dropup open']/ul//label"));
            _ddOptions1.FirstOrDefault(x => x.Text.Trim() == "Rating scale").Click();
            Assert.IsTrue(_isMandatoryMsgDisplayed);
        }
        [TestMethod]
        public void VerifyThankYouPageIsEditable()
        {
            string qText = null, surveyName = "New Survey", editType = "Customize Text";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, surveyName + DateTime.Now.ToString());
            AuthorActions.AddCommentQuestion(driver, "Test" + DateTime.Now);
            AuthorActions.SetThankUTypeInAtr(driver, editType, surveyName);
        }

        #region Alternate End Page

        [TestMethod]
        public void ValidateAddAlternateEndPageSurveyCompleted()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Completed");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '2']");

            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[1]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys("Survey Completed Text by Tellurium");
            emailBody.Click();
            Thread.Sleep(3000);

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(7000);
            var isModalClosed = UIActions.IsElementNotVisible(driver, "#thank-you-page-modal");

            Assert.AreEqual(isModalClosed, true);

        }

        [TestMethod]
        public void ValidateAddAlternateEndPageSurveyClosed()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Closed");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '12']");

            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[3]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys("Survey Closed Text by Tellurium");
            emailBody.Click();
            Thread.Sleep(3000);

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(7000);
            var isModalClosed = UIActions.IsElementNotVisible(driver, "#thank-you-page-modal");

            Assert.AreEqual(isModalClosed, true);

        }

        [TestMethod]
        public void ValidateAddAlternateEndPageOverQuota()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Over Quota");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '11']");
            
            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[2]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys("Over Quota Text by Tellurium");
            emailBody.Click();
            Thread.Sleep(3000);

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(7000);
            var isModalClosed = UIActions.IsElementNotVisible(driver, "#thank-you-page-modal");

            Assert.AreEqual(isModalClosed, true);

        }

        [TestMethod]
        public void ValidateAddAlternateEndPageSurveySubmitted()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Already Submitted");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '13']");

            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[4]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys("Survey Submitted Text by Tellurium");
            emailBody.Click();
            Thread.Sleep(3000);

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(7000);
            var isModalClosed = UIActions.IsElementNotVisible(driver, "#thank-you-page-modal");

            Assert.AreEqual(isModalClosed, true);
          



        }

        [TestMethod]
        public void ValidateEditSubmittedEndPageSurveyAndInPlayer()
        {
            FormName = "Edit_Tellurium_EndPageTest Survey 1";
            var customText = "Edited custom submitted text by Telurium " + Guid.NewGuid();


            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);

           
     
            //Edit Form

         
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(15000);


            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Already Submitted");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '13']");

            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[4]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys(customText);
            emailBody.Click();
            Thread.Sleep(3000);
            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(7000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Edit_Tellurium_EndPageTest Survey 1");
            /*UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(15000);

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/30807/1/bae8e69e-040f-4283-88cc-fd8d042e9a87";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(7000));
            var msgElem = By.XPath("//*[@id='main_content']/div/div/div");
            wait.Until(ExpectedConditions.ElementIsVisible(msgElem));
            bool isTextDisplayed = playerDriver.FindElement(msgElem).Text.Equals(customText);
            Assert.IsTrue(isTextDisplayed);

        }

        public static string SetAsMandatory(IWebDriver driver)
        {

            IReadOnlyCollection<IWebElement> ddOptions = driver.FindElements(By.XPath("//div[@class='btn-group ceb-dropdown-container dropup open']/ul//label"));
            ddOptions.FirstOrDefault(x => x.Text.Trim() == "All").Click();
            string _isMandatoryMsgDisplayed = UIActions.GetElement(driver, "#mandatory-question-types-warning").Text;

            //bool _isMandatoryMsgDisplayed = UIActions.GetElement(driver, "#mandatory-question-types-warning").Displayed;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            ac.MoveToElement(UIActions.GetElement(driver, "#mandatory-question-types-warning")).Build().Perform();
            UIActions.GetElement(driver, "#btnFormSettingsSave").Click();
            return _isMandatoryMsgDisplayed;
        }

        //[TestMethod]
        //public void deselectmandatoryinquestionlevel()
        //{
        //    RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
        //    bool result = false;
        //    OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
        //    AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
        //    AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
        //    Thread.Sleep(3000);
        //    AuthorActions.OpenSettingsModalTab(driver, "Options");
        //    UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
        //    SetAsMandatory(driver);
        //    Thread.Sleep(8000);
        //    IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
        //    ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
        //    if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
        //    {
        //        UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 45).Click();
        //    }
        //    Thread.Sleep(2000);
        //    IReadOnlyCollection<IWebElement> _editorSections = UIActions.FindElementsWithXpath(driver, "//h4[@class='panel-title']/span");
        //    _editorSections.FirstOrDefault(x => x.Text == "Additional Options").Click();
        //    Thread.Sleep(4000);
        //    if (UIActions.GetElement(driver, "#chkMandatory").Selected)
        //    {
        //        UIActions.GetElement(driver, "#chkMandatory").Click();
        //    }
        //    Thread.Sleep(4000);
        //    UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
        //    Thread.Sleep(4000);
        //    IReadOnlyCollection<IWebElement> titleList1 = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
        //    OpenQA.Selenium.Interactions.Actions ac1 = new OpenQA.Selenium.Interactions.Actions(driver);
        //    ac1.MoveToElement(titleList1.First(x => x.Text.Contains("AddShortCommentQuestion"))).Build().Perform();
        //    result = UIActions.IsElementNotVisible(driver, "//span[@class='question-text true']");
        //    Assert.IsTrue(result);

        //}
        [TestMethod]
        public void CheckNonMandatorySettingsInPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.AddShortCommentQuestion(driver, "AddShortCommentQuestion");
            Thread.Sleep(3000);
            AuthorActions.OpenSettingsModalTab(driver, "Options");
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            SetAsMandatory(driver);
            Thread.Sleep(8000);
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
            if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
            {
                UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 45).Click();
            }
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _editorSections = UIActions.FindElementsWithXpath(driver, "//h4[@class='panel-title']/span");
            _editorSections.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(4000);
            if (UIActions.GetElement(driver, "#chkMandatory").Selected)
            {
                UIActions.GetElement(driver, "#chkMandatory").Click();
            }
            Thread.Sleep(4000);
            UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
            Thread.Sleep(4000);
            IReadOnlyCollection<IWebElement> titleList1 = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            OpenQA.Selenium.Interactions.Actions ac1 = new OpenQA.Selenium.Interactions.Actions(driver);
            ac1.MoveToElement(titleList1.First(x => x.Text.Contains("AddShortCommentQuestion"))).Build().Perform();
            UIActions.IsElementNotVisible(driver, "//span[@class='question-text true']");
            AuthorActions.capturePreviewLink(driver);
            //  RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/33c541ff9ea246b2bf08c959e7a82d95");
            // RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/33c541ff9ea246b2bf08c959e7a82d95");
            try
            {
                IWebElement _mandatorySymbol = driver.FindElement(By.XPath("//span[@class='question-text false']"));
                _result = _mandatorySymbol.Displayed;
            }
            catch (Exception)
            {

                IWebElement _mandatorySymbol = driver.FindElement(By.XPath("//span[@class='question-text true']"));
                _result = false;
            }
            Assert.IsTrue(_result);
        }


        #region Embedded link
        [TestMethod]
        public void CheckSettingOfEmbeddedLink()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            string _emdebLnk = "www.google.com";
            string _qText = "AddShortCommentQuestion";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AddCommentQuestionWithEmbeddedLink(driver, _emdebLnk, _qText, ac);
            Thread.Sleep(2000);
            Console.WriteLine(UIActions.GetElementswithXpath(driver, "//a[@href='http://" + _emdebLnk + "']", 45).Text);
            Assert.IsTrue(UIActions.GetElementswithXpath(driver, "//a[@href='http://" + _emdebLnk + "']", 45).Text.Contains(_emdebLnk));


            //span[@class='cke_button_icon cke_button__link_icon']
        }

        [TestMethod]
        public void CheckCancelActionInEmbeddedLnkCKEditor()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            string _emdebLnk = "www.google.com";
            string _qText = "AddShortCommentQuestion";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            //AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            AuthorActions.SearchAndOpenForm(driver, "BulkMandatory");
            AddCommentQuestionWithEmbeddedLinkWithCancel(driver, _emdebLnk, _qText, ac);
            try
            {
                UIActions.FindElementWithXpath(driver, "//a[@href='" + _emdebLnk + "']");
                _result = true;
            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsFalse(_result);
            }
        }

        [TestMethod]
        public void CheckPresenceOfEmbLnkInForm()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            string _emdebLnk = "www.google.com";
            string _qText = "AddShortCommentQuestion";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            //AuthorActions.CreateForm(driver, "BulkMandatory" + DateTime.Now);
            //AddCommentQuestionWithEmbeddedLink(driver, _emdebLnk, _qText, ac);
            AuthorActions.SearchAndOpenForm(driver, "EmbedLink.SourceForm");
            Thread.Sleep(4000);
            UIActions.GetElementswithXpath(driver, "//span[@data-bind='BindQuestionTile']", 20).Click();
            Thread.Sleep(4000);
            try
            {
                UIActions.FindElementWithXpath(driver, "//a[@href='http://" + _emdebLnk + "']");
                _result = true;
            }
            catch (Exception)
            {
                _result = false;

            }
            finally
            {
                Assert.IsTrue(_result);
            }
        }
        [TestMethod]
        public void CheckUnlinkEmbLnkInForm()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            //AuthorActions.CopyForm(driver, "EmbedLink.SourceForm");
            AuthorActions.SearchAndOpenForm(driver, "Copy Of EmbedLink.SourceForm");
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            Thread.Sleep(8000);
            ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
            if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
            {
                UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 10).Click();
            }
            ClickCKEditorAndUnlinkEmb(driver, ac, executor);
            try
            {
                UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                _result = true;
            }
            catch (Exception)
            {

                _result = false;
            }
            finally
            {
                Assert.IsFalse(_result);
            }

            // UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
        }
        [TestMethod]
        public void CheckRemovalOfEmbLinkInForm()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            bool _result = false;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            AuthorActions.CopyForm(driver, "EmbedLink.SourceForm");
            Thread.Sleep(3000);
            //AuthorActions.SearchAndOpenForm(driver, "Copy Of EmbedLink.SourceForm");
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            Thread.Sleep(8000);
            ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
            //wait.Until(ExpectedConditions.ElementToBeClickable((By.XPath("//span[@title='Edit']"))));
            //if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
            //{
            Thread.Sleep(3000);
            UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 10).Click();
            //}
            ClickCKEditorAndUnlinkEmb(driver, ac, executor);
            Thread.Sleep(2000);
            UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
            Thread.Sleep(5000);
            UIActions.GetElementswithXpath(driver, "//span[@data-bind='BindQuestionTile']", 20).Click();
            Thread.Sleep(3000);
            try
            {
                UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                _result = false;
            }
            catch (Exception)
            {
                _result = true;

            }
            finally
            {
                Assert.IsTrue(_result);
            }
        }

        private static void ClickCKEditorAndUnlinkEmb(RemoteWebDriver driver, OpenQA.Selenium.Interactions.Actions ac, IJavaScriptExecutor executor)
        {
            Thread.Sleep(2000);
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Click();
            Thread.Sleep(2000); Thread.Sleep(2000); Thread.Sleep(2000); Thread.Sleep(2000);
            UIActions.GetElementswithXpath(driver, "//div[@class='cke_textarea_inline cke_editable cke_editable_inline cke_contents_ltr cke_show_borders']", 20).Click();
            Thread.Sleep(2000);
            IWebElement ckEditorUnlink = driver.FindElement(By.XPath("//a[@class='cke_button cke_button__unlink cke_button_off']"));
            string highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"" });";
            executor.ExecuteScript(highlightJavascript, new object[] { ckEditorUnlink });
            executor.ExecuteScript("arguments[0].click();", ckEditorUnlink);
            infoquestionbox.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
            executor.ExecuteScript("arguments[0].click();", ckEditorUnlink);
            ac.MoveToElement(ckEditorUnlink).Build().Perform();
            executor.ExecuteScript("arguments[0].click();", ckEditorUnlink);
        }

        private static void AddCommentQuestionWithEmbeddedLink(RemoteWebDriver driver, string _emdebLnk, string _qText, OpenQA.Selenium.Interactions.Actions ac)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(_qText);
            Thread.Sleep(3000);
            IWebElement _enteredText = UIActions.FindElementWithXpath(driver, "//div[@title='Rich Text Editor, item-question-FF']");
            _enteredText.Click();
            ClkOnEmbeddedLnkInCKEdr(driver, _emdebLnk);
            Thread.Sleep(7000);
            UIActions.GetElement(driver, "#btnFixedFormatSave").Click();
            Thread.Sleep(3000);
        }

        [TestMethod]
        public void CreateEmbLnkWith()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            string _emdebLnk = "www.google.com";
            string _qText = "AddShortCommentQuestion";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "BulkMandatory");
            //AuthorActions.SearchAndOpenForm(driver, "Embedded Form15");
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            try
            {
                infoquestionbox.Click();
                Thread.Sleep(3000);
                IWebElement ckEditorlink = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__link cke_button_off']"));
                Thread.Sleep(2000);

                ckEditorlink.Click();
                Console.WriteLine("Clicked on first" + ckEditorlink.Displayed);
                Thread.Sleep(2000);
                IReadOnlyCollection<IWebElement> _txtList = UIActions.FindElementsWithXpath(driver, "//table[@class='cke_dialog_contents'][@role='presentation']//input[@class='cke_dialog_ui_input_text']");
                Thread.Sleep(2000);
                _txtList.First().SendKeys("www.google.com");
                UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();
                IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                Assert.IsTrue(_embLnk.Displayed);
            }
            catch (Exception)
            {
                IWebElement ckEditor = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//span[@class='cke_button_icon cke_button__link_icon']"));
                Thread.Sleep(200);
                Console.WriteLine("Clicked on second" + ckEditor.Displayed);
                ckEditor.Click();
            }



        }
        private static void ClkOnEmbeddedLnkInCKEdr(RemoteWebDriver driver, string _emdebLnk)
        {
            Thread.Sleep(3000);
            IWebElement ckEditorlink = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__link cke_button_off']"));
            Thread.Sleep(2000);

            ckEditorlink.Click();
            Console.WriteLine("Clicked on first" + ckEditorlink.Displayed);
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _txtList = UIActions.FindElementsWithXpath(driver, "//table[@class='cke_dialog_contents'][@role='presentation']//input[@class='cke_dialog_ui_input_text']");
            Thread.Sleep(2000);
            _txtList.First().SendKeys("www.google.com");
            UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();
            Thread.Sleep(2000);
            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
            Assert.IsTrue(_embLnk.Displayed);
            /*
                        IWebElement ckEditor = driver.FindElement(By.XPath("//span[@class='cke_button_icon cke_button__link_icon']"));
                        IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
                        IWebElement ckEditorlink = driver.FindElement(By.XPath("//a[@class='cke_button cke_button__link cke_button_off']"));
                        executor.ExecuteScript("arguments[0].click();", ckEditorlink);
                       // ckEditorlink.Click();
                       // executor.ExecuteScript("arguments[0].click();", ckEditor);
                        IReadOnlyCollection<IWebElement> _txtList = UIActions.FindElementsWithXpath(driver, "//table[@class='cke_dialog_contents'][@role='presentation']//input[@class='cke_dialog_ui_input_text']");
                        _txtList.First().SendKeys(_emdebLnk);
                        _txtList.First().SendKeys(OpenQA.Selenium.Keys.Tab);
                        IReadOnlyCollection<IWebElement> _ckEditorBtn = UIActions.FindElementsWithXpath(driver, "//span[@class='cke_dialog_ui_button']");
                        executor.ExecuteScript("arguments[0].click();", _ckEditorBtn.FirstOrDefault(x => x.Text == "OK"));
                        OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
                        ac.MoveToElement(_ckEditorBtn.FirstOrDefault(x => x.Text == "OK"))
                            .SendKeys(OpenQA.Selenium.Keys.Enter)
                            .Build().Perform();
                        IReadOnlyCollection<IWebElement> _ckEditorBtn1 = UIActions.FindElementsWithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']");
                        UIActions.GetElementwithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();
                        UIActions.GetElementwithXpath(driver, "//td[@class='cke_dialog_ui_hbox_first']//span[@class='cke_dialog_ui_button']", 20).Click();
            */
        }

        private static void AddCommentQuestionWithEmbeddedLinkWithCancel(RemoteWebDriver driver, string _emdebLnk, string _qText, OpenQA.Selenium.Interactions.Actions ac)
        {
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys(_qText);
            Thread.Sleep(3000);
            IWebElement _enteredText = UIActions.FindElementWithXpath(driver, "//div[@title='Rich Text Editor, item-question-FF']");
            _enteredText.Click();
            OpenEmbeddedLnkInCKEdrAndCancel(driver, _emdebLnk);
            Thread.Sleep(4000);
            UIActions.GetElement(driver, "#btnFixedFormatSave").Click();

        }
        private static void OpenEmbeddedLnkInCKEdrAndCancel(RemoteWebDriver driver, string _emdebLnk)
        {
            IWebElement ckEditor = driver.FindElement(By.XPath("//span[@class='cke_button_icon cke_button__link_icon']"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            Thread.Sleep(5000);
            executor.ExecuteScript("arguments[0].click();", ckEditor);
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _txtList = UIActions.FindElementsWithXpath(driver, "//table[@class='cke_dialog_contents'][@role='presentation']//input[@class='cke_dialog_ui_input_text']");
            _txtList.FirstOrDefault().SendKeys(_emdebLnk);
            Thread.Sleep(2000);
            _txtList.First().SendKeys("www.google.com");
            IJavaScriptExecutor executor1 = (IJavaScriptExecutor)driver;
            executor1.ExecuteScript("arguments[0].click();", UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20));
            Thread.Sleep(2000);
            UIActions.FindElementsWithXpath(driver, "//span[@class='cke_dialog_ui_button']").FirstOrDefault(x => x.Text == "Cancel").Click();
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
        }
        [TestMethod]
        public void CheckClkOnEmbLinkFromFormBuilder()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            bool _result = false;
            string _setEmbLnk = "https://www.google.com";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            //AuthorActions.CopyForm(driver, "EmbedLink.SourceForm");
            AuthorActions.SearchAndOpenForm(driver, "Copy Of EmbedLink.SourceForm");
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            ac.MoveToElement(titleList.FirstOrDefault())
                .Click()
                .Build().Perform();
            try
            {
                IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                _embLnk.Click();
                string _currentWindowHandle = driver.CurrentWindowHandle;
                IReadOnlyCollection<string> handles = driver.WindowHandles;
                foreach (var _handle in handles)
                {
                    if (_handle != _currentWindowHandle)
                    {
                        driver.SwitchTo().Window(_handle);
                    }
                }
                _result = (driver.Url).Contains(_setEmbLnk) ? true : false;
            }
            catch (Exception)
            {
                Console.WriteLine("Embeddedlink not present");
                _result = false;
            }
            finally
            {
                Assert.IsTrue(_result);
            }
        }
        [TestMethod]
        public void CheckEmbLinkInPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));

            /*declare variable*/
            bool _result = false;
            string _setEmbLnk = "https://www.google.com";
            string _currentHandle;
            IReadOnlyCollection<string> _handles;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            //AuthorActions.CopyForm(driver, "EmbedLink.SourceForm");
            AuthorActions.SearchAndOpenForm(driver, "Copy Of EmbedLink.SourceForm");
            AuthorActions.capturePreviewLink(driver);
            _currentHandle = driver.CurrentWindowHandle;
            _handles = driver.WindowHandles;
            driver.SwitchTo().Window(_handles.Last());
            UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5).Click();
            _handles = driver.WindowHandles;
            driver.SwitchTo().Window(_handles.Last());
            _result = driver.Url.Contains(_setEmbLnk) ? true : false;
            Assert.IsTrue(_result);
        }
        [TestMethod]
        public void CheckEmbLinkInPlayer()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));

            /*declare variable*/
            bool _result = false;
            string _setEmbLnk = "https://www.google.com";
            string surveyForDistribution = "Emblnk" + DateTime.Now;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            string _currentHandle;
            IReadOnlyCollection<string> _handles;
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            //AuthorActions.CopyForm(driver, "EmbedLink.SourceForm");
            AuthorActions.SearchAndOpenForm(driver, "Copy Of EmbedLink.SourceForm");
            DistributeActions.PublishForm(driver);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {

                FormName = surveyForDistribution,
                DistributionName = "ThankYouPageCheck" + Guid.NewGuid(),
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });
            Thread.Sleep(4000);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']");

            ac.MoveToElement(surveyLink1).Build().Perform();
            Thread.Sleep(3000);
            surveyLink1.Click();
            driver.Navigate().GoToUrl(Clipboard.GetText());
            UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5).Click();
            _handles = driver.WindowHandles;
            driver.SwitchTo().Window(_handles.Last());
            _result = driver.Url.Contains(_setEmbLnk) ? true : false;
            Assert.IsTrue(_result);
        }
        [TestMethod]
        public void CheckCopiedSurveyWithEmbLnkIsCopiedIntoNewForm()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            bool _result = false;
            string _setEmbLnk = "https://www.google.com";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            AuthorActions.CopyForm(driver, "EmbedLink.SourceForm");
            //AuthorActions.SearchAndOpenForm(driver, "Copy Of EmbedLink.SourceForm");
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            ac.MoveToElement(titleList.FirstOrDefault())
                .Click()
                .Build().Perform();
            try
            {
                IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                _embLnk.Click();
                string _currentWindowHandle = driver.CurrentWindowHandle;
                IReadOnlyCollection<string> handles = driver.WindowHandles;
                foreach (var _handle in handles)
                {
                    if (_handle != _currentWindowHandle)
                    {
                        driver.SwitchTo().Window(_handle);
                    }
                }
                _result = (driver.Url).Contains(_setEmbLnk) ? true : false;
            }
            catch (Exception)
            {
                Console.WriteLine("Embeddedlink not present");
                _result = false;
            }
            finally
            {
                Assert.IsTrue(_result);
            }
        }

        [TestMethod]
        public void CheckCopyQuestionWithEmbInGFE()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            bool _result = false;
            string _surveyToCopyFrom = "EmbeddedLink CopySurvey"; //"Survey with Rating Scale as 0-10";
            string _questionToCopy = "WithEmbeddedLink";//"Alignment Category: Rating Scale Question with Scale Range 0-10";
            string _setEmbLnk = "https://www.google.com";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, "EmbbedForm" + DateTime.Now);
            // AuthorActions.SearchAndOpenForm(driver, "EmbbedForm11");
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").Click();
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys("rating");
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(OpenQA.Selenium.Keys.Enter);
            UIActions.GetElementswithXpath(driver, "//a[@aria-controls='CopySurveyPanel']", 20).Click();
            UIActions.GetElementswithXpath(driver, "//button[@class='btn btn-xs btn-info existing-form-btn']", 5).Click();
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _qBundle = UIActions.FindElementsWithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[@class='row no-margin quadrant-focus']");
            for (int i = 1; i <= _qBundle.Count(); i++)
            {
                IWebElement _questionText = UIActions.FindElementWithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[@data-bind-type='sanitizedhtml']");
                string highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"" });";
                if (_questionText.Text == _questionToCopy)
                {
                    IWebElement e1 = UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5);
                    IWebElement e = UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//div[@class='quadrant-container']", 5);
                    try
                    {
                        executor.ExecuteScript(highlightJavascript, new object[] { UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5) });
                        ac.MoveToElement(UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5)).Build().Perform();
                        executor.ExecuteScript("arguments[0].click();", e1);
                        UIActions.GetElement(driver, "#btnAddToFormClicked").Click();
                        IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
                        Thread.Sleep(8000);
                        ac.MoveToElement(titleList.FirstOrDefault()).Build().Perform();
                        if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
                        {
                            UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 10).Click();
                            try
                            {
                                UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                                _result = true;
                            }
                            catch (Exception)
                            {

                                _result = false;
                            }
                        }
                    }
                    finally
                    {
                        Assert.IsTrue(_result);
                    }
                }
            }
        }

        [TestMethod]
        public void CheckCopyQuestionWithEmbInSurveyForm()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            bool _result = false;
            string _surveyToCopyFrom = "EmbeddedLink CopySurvey"; //"Survey with Rating Scale as 0-10";
            string _questionToCopy = "WithEmbeddedLink";//"Alignment Category: Rating Scale Question with Scale Range 0-10";
            string _setEmbLnk = "https://www.google.com";
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, "EmbbedForm" + DateTime.Now);
            // AuthorActions.SearchAndOpenForm(driver, "EmbbedForm11");
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").Click();
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys("rating");
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(OpenQA.Selenium.Keys.Enter);
            UIActions.GetElementswithXpath(driver, "//a[@aria-controls='CopySurveyPanel']", 20).Click();
            UIActions.GetElementswithXpath(driver, "//button[@class='btn btn-xs btn-info existing-form-btn']", 5).Click();
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _qBundle = UIActions.FindElementsWithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[@class='row no-margin quadrant-focus']");
            for (int i = 1; i <= _qBundle.Count(); i++)
            {
                IWebElement _questionText = UIActions.FindElementWithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[@data-bind-type='sanitizedhtml']");
                string highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"" });";
                if (_questionText.Text == _questionToCopy)
                {
                    IWebElement e1 = UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5);
                    IWebElement e = UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//div[@class='quadrant-container']", 5);
                    try
                    {
                        executor.ExecuteScript(highlightJavascript, new object[] { UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5) });
                        ac.MoveToElement(UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5)).Build().Perform();
                        executor.ExecuteScript("arguments[0].click();", e1);
                        UIActions.GetElement(driver, "#btnAddToFormClicked").Click();
                        IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
                        ac.MoveToElement(titleList.FirstOrDefault())
                            .Click()
                            .Build().Perform();
                        try
                        {
                            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 15);
                            _result = true;
                        }
                        catch (Exception)
                        {
                            _result = false;
                            throw;
                        }
                    }
                    finally
                    {
                        Assert.IsTrue(_result);
                    }
                }
            }
        }

        [TestMethod]
        public void CheckCopyQuestionWithEmbInPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            string _setEmbLnk = "https://www.google.com";
            string _currentHandle;
            IReadOnlyCollection<string> _handles;
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            bool _result = false;
            string _surveyToCopyFrom = "EmbeddedLink CopySurvey"; //"Survey with Rating Scale as 0-10";
            string _questionToCopy = "WithEmbeddedLink";//"Alignment Category: Rating Scale Question with Scale Range 0-10";
            AuthorActions.CreateForm(driver, "EmbbedForm" + DateTime.Now);
            // AuthorActions.SearchAndOpenForm(driver, "EmbbedForm11");
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").Click();
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(_surveyToCopyFrom);
            UIActions.GetElement(driver, "#txtSearchCopyQuestionsSurveys").SendKeys(OpenQA.Selenium.Keys.Enter);
            UIActions.GetElementswithXpath(driver, "//a[@aria-controls='CopySurveyPanel']", 20).Click();
            UIActions.GetElementswithXpath(driver, "//button[@class='btn btn-xs btn-info existing-form-btn']", 5).Click();
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _qBundle = UIActions.FindElementsWithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[@class='row no-margin quadrant-focus']");
            for (int i = 1; i <= _qBundle.Count(); i++)
            {
                IWebElement _questionText = UIActions.FindElementWithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[@data-bind-type='sanitizedhtml']");
                string highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"" });";
                if (_questionText.Text == _questionToCopy)
                {
                    IWebElement e1 = UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5);
                    IWebElement e = UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//div[@class='quadrant-container']", 5);
                    try
                    {
                        executor.ExecuteScript(highlightJavascript, new object[] { UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5) });
                        ac.MoveToElement(UIActions.GetElementswithXpath(driver, "//div[@class='content-section-panel panel-body']/div//div[" + i + "][@class='row no-margin quadrant-focus']//span[1][@class='lbl']", 5)).Build().Perform();
                        executor.ExecuteScript("arguments[0].click();", e1);
                        UIActions.GetElement(driver, "#btnAddToFormClicked").Click();
                        Thread.Sleep(5000);
                        AuthorActions.capturePreviewLink(driver);
                        _currentHandle = driver.CurrentWindowHandle;
                        _handles = driver.WindowHandles;
                        driver.SwitchTo().Window(_handles.Last());
                        Thread.Sleep(3000);
                        UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5).Click();
                        _handles = driver.WindowHandles;
                        driver.SwitchTo().Window(_handles.Last());
                        _result = driver.Url.Contains(_setEmbLnk) ? true : false;
                    }
                    finally
                    {
                        Assert.IsTrue(_result);
                    }
                }
            }
        }

        [TestMethod]
        public void CheckEmbLinkInDownloadTemplate()
        {

            string _Folder = @"C:\Users\ja.m\Downloads\";
            DateTime _currentdate = DateTime.Now;
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            string _setEmbLnk = "https://www.google.com";
            string _templateLnk = "www.google.com";
            string _currentHandle;
            IReadOnlyCollection<string> _handles;

            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            //AuthorActions.SearchAndOpenForm(driver, "EmbbedForm11");
            AuthorActions.SearchAndOpenForm(driver, "EmbeddedLink DownloadTemplate");
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> settingButton = UIActions.FindElements(driver, "#btnlangSettings");
            Thread.Sleep(3000);
            settingButton.FirstOrDefault(x => x.Text == "Languages").Click();
            Thread.Sleep(3000);
            UIActions.clickwithXapth(driver, "//button[@title='Please Select...']");
            Thread.Sleep(7000);

            wait.Until(d => { return d.FindElement(By.XPath("//label[@class='checkbox']/input")); });
            IReadOnlyCollection<IWebElement> _language = UIActions.FindElementsWithXpath(driver, "//label[@class='checkbox']/input");
            foreach (IWebElement lang in _language)
                lang.Click();
            Thread.Sleep(3000);
            executor.ExecuteScript(highlightJavascript, new object[] { UIActions.GetElementswithXpath(driver, "//button[@title='English as spoken in United States']", 5) });
            Thread.Sleep(2000);
            UIActions.GetElementswithXpath(driver, "//button[@title='English as spoken in United States']", 5).Click();
            UIActions.FindElementWithXpath(driver, "//button[@widget-data-event='click::downloadTemplateClick']").Click();
            _currentHandle = driver.CurrentWindowHandle;
            _handles = driver.WindowHandles;
            int _handlesCnt = _currentHandle.Count();
            //DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(driver);
            //fluentWait.Timeout = TimeSpan.FromSeconds(60);
            //fluentWait.PollingInterval = TimeSpan.FromSeconds(5);
            while (_handlesCnt > 1)
            {
                //fluentWait.Until((x => _handlesCnt == 1));
                Thread.Sleep(50);
                _handlesCnt = driver.WindowHandles.Count();
            }

            var files = new DirectoryInfo(_Folder).GetFiles("FormContentInformation*.*");


            foreach (var f in files)
            {
                if (f.CreationTime > _currentdate)
                {
                    string filename = _Folder + f.Name;
                    filename = filename.ToString().Substring(0, filename.Length - 11);
                    Aspose.Cells.Workbook wb = new Aspose.Cells.Workbook(filename);
                    Aspose.Cells.Worksheet sh = wb.Worksheets[0];
                    Cells cells = sh.Cells;
                    FindOptions findOptions = new FindOptions();
                    findOptions.CaseSensitive = false;
                    findOptions.LookInType = LookInType.Values;
                    Cell foundCells = cells.Find(_templateLnk, null, findOptions);
                    Console.WriteLine("${0}", foundCells);
                    Assert.IsTrue(foundCells != null ? true : false);


                    break;
                }
                else
                    Console.WriteLine("No file downloaded");
            }

            /*
                string filename = @"C:\Users\ja.m\Downloads\FormContentInformation_1500295751 (1).xlsx.crdownload";
                filename = filename.ToString().Substring(0, filename.Length - 11);
                Console.WriteLine(filename);
                Aspose.Cells.Workbook wb = new Aspose.Cells.Workbook(filename);
                     Aspose.Cells.Worksheet sh = wb.Worksheets[0];
                     Cells cells = sh.Cells;
                     FindOptions findOptions = new FindOptions();
                     findOptions.CaseSensitive = false;
                     findOptions.LookInType = LookInType.Values;
                     Cell foundCells = cells.Find("www.google.com", null, findOptions);
                     Console.WriteLine("${0}", foundCells);
                     Assert.IsTrue(foundCells != null ? true : false); 
             */
        }

        [TestMethod]
        public void CheckEmbLinkInDownloadTemplateWithTtranslation()
        {
            string _Folder = @"C:\Users\ja.m\Downloads\";
            DateTime _currentdate = DateTime.Now;
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            string _templateLnk = "www.google.com";
            string _currentHandle;
            IReadOnlyCollection<string> _handles;

            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "EmbeddedLink DownloadTemplate");
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> settingButton = UIActions.FindElements(driver, "#btnlangSettings");
            Thread.Sleep(3000);
            settingButton.FirstOrDefault(x => x.Text == "Languages").Click();
            Thread.Sleep(3000);
            UIActions.FindElementWithXpath(driver, "//button[@widget-data-event='click::downloadTemplateClick']").Click();
            _currentHandle = driver.CurrentWindowHandle;
            _handles = driver.WindowHandles;
            int _handlesCnt = _currentHandle.Count();
            while (_handlesCnt > 1)
            {
                Thread.Sleep(50);
                _handlesCnt = driver.WindowHandles.Count();
            }
            var files = new DirectoryInfo(_Folder).GetFiles("FormContentInformation*.*");
            int _occurance = 0;
            string filename = @"C:\Users\ja.m\Downloads\";
            Aspose.Cells.Workbook wb;
            Aspose.Cells.Worksheet sh;
            Cells cells;
            FindOptions findOptions = new FindOptions();
            findOptions.CaseSensitive = false;
            findOptions.LookInType = LookInType.Values;
            // Cell foundCells = null;
            foreach (var f in files)
            {
                if (f.CreationTime > _currentdate)
                {
                    filename = _Folder + f.Name;
                    filename = filename.ToString().Substring(0, filename.Length - 11);
                    wb = new Aspose.Cells.Workbook(filename);
                    sh = wb.Worksheets[0];
                    cells = sh.Cells;
                    int _maxColumnCnt = cells.MaxDataColumn;
                    for (short j = 0; j <= _maxColumnCnt; j++)
                    {
                        object _cellValue = cells.EndCellInColumn(j).Value;
                        if (_cellValue.ToString().Contains(_templateLnk) == true)
                            _occurance++;
                    }

                    //  foundCells = cells.Find(_templateLnk, null, findOptions);


                    //Console.WriteLine("${0}", foundCells);
                    Console.WriteLine(_occurance);
                    break;
                }
                else
                    Console.WriteLine("No file downloaded");
            }
            //      Assert.IsTrue((foundCells != null && _occurance == 45) ? true : false);
            Assert.IsTrue((_occurance == 45) ? true : false);
        }

        [TestMethod]
        public void CheckColorOfEmbLnkInGFE()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "EmbeddedLink DownloadTemplate");
            string _expectedColor = "rgba(0, 174, 239, 1)";
            string _color = "";
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> titleList = driver.FindElements(By.XPath("//span[@data-bind='BindQuestionTile']"));
            ac.MoveToElement(titleList.FirstOrDefault())
                .Click()
                .Build().Perform();
            if (driver.FindElement(By.XPath("//span[@title='Edit']")).Enabled)
            {
                UIActions.GetElementswithXpath(driver, "//span[@class='btn ceb-icon_home_class_edit action-question-edit secondary font-sz-23']", 45).Click();
                try
                {
                    IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                    _color = _embLnk.GetCssValue("color");
                    Console.WriteLine(_embLnk.GetCssValue("color"));
                }
                finally
                {
                    Assert.AreEqual(_expectedColor, _color);
                }

            }
            Thread.Sleep(2000);

        }

        [TestMethod]
        public void CheckColorOfEmbLnkInPreview()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "EmbeddedLink DownloadTemplate");
            if (UIActions.GetElement(driver, "#btnFormEdit") != null)
            {
                UIActions.GetElement(driver, "#btnFormEdit").Click();
                UIActions.GetElement(driver, "#edit-form-button").Click();
            }
            wait.Until(x =>
            {
                return
  x.FindElement(By.XPath("//li/button[@type='button']"));
            });
            string _expectedColor = "rgba(0, 174, 239, 1)";
            string _color = "";
            Thread.Sleep(3000);
            AuthorActions.capturePreviewLink(driver);
            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
            _color = _embLnk.GetCssValue("color");
            Assert.AreEqual(_expectedColor, _color);
        }

        [TestMethod]
        public void CheckColorOfEmbLnkInPlayer()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "EmbeddedLink Published");
            string _expectedColor = "rgba(0, 174, 239, 1)";
            string _color = "";
            Thread.Sleep(3000);
            if (UIActions.GetElement(driver, "#btnFormEdit") != null)
            {
                UIActions.GetElement(driver, "#btnFormEdit").Click();
                UIActions.GetElement(driver, "#edit-form-button").Click();
            }
            wait.Until(x =>
            {
                return
  x.FindElement(By.XPath("//li/button[@type='button']"));
            });
            DistributeActions.PublishForm(driver);
            DistributeActions.PublishAndCreateDistribution(driver, new DistributionParametersDTO
            {

                FormName = "EmbeddedLink Published",
                DistributionName = "EmbeddedLink Published" + Guid.NewGuid(),
                StartDate = Convert.ToString(DateTime.UtcNow),
                StartTimeZone = "Chennai",
                // EndDate = Convert.ToString(DateTime.UtcNow.AddDays(1200)),
                EndDate = null,
                EndTimeZone = "Chennai",
                Method = new string[] { "Share Manually" }
            });
            Thread.Sleep(4000);
            IWebElement surveyLink1 = UIActions.FindElementWithXpath(driver, "//*[@id='btn-copy-url']");

            ac.MoveToElement(surveyLink1).Build().Perform();
            Thread.Sleep(3000);
            surveyLink1.Click();
            driver.Navigate().GoToUrl(Clipboard.GetText());
            IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
            _color = _embLnk.GetCssValue("color");
            Assert.AreEqual(_expectedColor, _color);
        }

        [TestMethod]
        public void CheckColorAfterUnlinkEmbLnkInForm_25372()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            bool _result = false;
            string _expectedColor = "rgba(0, 174, 239, 1)";
            string _color = "";

            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            //AuthorActions.CopyForm(driver, "EmbeddedLink Published", "EmbeddedLink Published" + Guid.NewGuid());
            AuthorActions.SearchAndOpenForm(driver, "BulkMandatory");
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            infoquestionbox.SendKeys("Question Text");
            Thread.Sleep(3000);
            IWebElement _enteredText = UIActions.FindElementWithXpath(driver, "//div[@title='Rich Text Editor, item-question-FF']");
            _enteredText.Click();
            try
            {
                IWebElement ckEditorlink = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__link cke_button_off']"));
                Thread.Sleep(2000);

                ckEditorlink.Click();
                infoquestionbox.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
                Console.WriteLine("Clicked on first" + ckEditorlink.Displayed);
                Thread.Sleep(2000);
                IReadOnlyCollection<IWebElement> _txtList = UIActions.FindElementsWithXpath(driver, "//table[@class='cke_dialog_contents'][@role='presentation']//input[@class='cke_dialog_ui_input_text']");
                Thread.Sleep(2000);
                _txtList.First().SendKeys("www.google.com");
                UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();
                IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                _color = _embLnk.GetCssValue("color");
                /* Unlink the text and check color */
                infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
                infoquestionbox.Click();
                infoquestionbox.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
                Thread.Sleep(2000);
                IWebElement ckEditorUnlink = driver.FindElement(By.XPath("//a[@class='cke_button cke_button__unlink cke_button_off']"));
                string highlightJavascript = @"$(arguments[0]).css({ ""border-width"" : ""2px"", ""border-style"" : ""solid"", ""border-color"" : ""red"" });";
                executor.ExecuteScript(highlightJavascript, new object[] { ckEditorUnlink });
                executor.ExecuteScript("arguments[0].click();", ckEditorUnlink);
                infoquestionbox.SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control);
                executor.ExecuteScript("arguments[0].click();", ckEditorUnlink);
                ac.MoveToElement(ckEditorUnlink).Build().Perform();
                executor.ExecuteScript("arguments[0].click();", ckEditorUnlink);
                string _colorAfterUnlink = UIActions.GetElementswithXpath(driver, "//span[@data-bind='isMandatory']/span[@data-bind-type='sanitizedhtml']", 20).GetCssValue("color");

                Assert.AreNotEqual(_color, _colorAfterUnlink);
                Console.WriteLine(_colorAfterUnlink);
            }
            catch (Exception)
            {

                _result = false;
            }
            finally
            {
                Assert.AreEqual(_expectedColor, _color);
            }

        }

        [TestMethod]
        public void CheckUnLinkOnLinkedtext_25344()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "BulkMandatory");
            //AuthorActions.SearchAndOpenForm(driver, "Embedded Form15");
            UIActions.clickwithID(driver, "add-Text");
            Thread.Sleep(5000);
            UIActions.SelectInCEBDropdownByValue(driver, "#text-format-type", "shortText");
            var infoquestionbox = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
            infoquestionbox.Clear();
            try
            {
                infoquestionbox.Click();
                Thread.Sleep(3000);
                /*Check if unlink is enabled before settings embedded link to the question */
                IWebElement ckEditorUnlink = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__unlink cke_button_off']"));
                bool _isUnlinkVisible = ckEditorUnlink.Displayed;
                Thread.Sleep(2000);
                IWebElement ckEditorlink = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__link cke_button_off']"));
                Thread.Sleep(2000);

                ckEditorlink.Click();
                Console.WriteLine("Clicked on first" + ckEditorlink.Displayed);
                Thread.Sleep(2000);
                IReadOnlyCollection<IWebElement> _txtList = UIActions.FindElementsWithXpath(driver, "//table[@class='cke_dialog_contents'][@role='presentation']//input[@class='cke_dialog_ui_input_text']");
                Thread.Sleep(2000);
                _txtList.First().SendKeys("www.google.com");
                UIActions.GetElementswithXpath(driver, "//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']/span[@class='cke_dialog_ui_button']", 20).Click();
                IWebElement _embLnk = UIActions.GetElementswithXpath(driver, "//span[@data-bind='displayText']/a", 5);
                Thread.Sleep(2000);
                /* click on unlink button */
                //IWebElement ckEditorUnlink = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//a[@class='cke_button cke_button__unlink cke_button_off']"));
                bool _isUnlinkVisible1 = ckEditorUnlink.Displayed;
                Assert.IsTrue(_isUnlinkVisible1 && _isUnlinkVisible);
            }
            catch (Exception)
            {
                IWebElement ckEditor = driver.FindElement(By.XPath("//div[@id='cke_item-question-FF']//span[@class='cke_button_icon cke_button__link_icon']"));
                Thread.Sleep(200);
                Console.WriteLine("Clicked on second" + ckEditor.Displayed);
                ckEditor.Click();
            }
        }


        #endregion Embedded link

        #region HoverText

        [TestMethod]
        public void CheckEnablingOfHoverText()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "EmbbedForm_05");

            var message = UIActions.GetElementWithWait(driver, "#add-Text", 60);
            if (message.Text.Trim() == "Comment")
            {
                UIActions.Click(driver, "#add-Text");
                Thread.Sleep(5000);
                IWebElement questionTextEditor = UIActions.GetElement(driver, "#item-question-FF + .cke_textarea_inline");
                questionTextEditor.Clear();
                questionTextEditor.SendKeys("This is comment question");
            }
            EnableInstructionText(driver);
            EnterInstructionText(driver, "this is instruction text for comment question");
            AuthorActions.SetHoverText(driver, "ho for comment question");
            Thread.Sleep(2000);
            string _hoTxt = UIActions.GetAttrForElementByXpath(driver, "//div[@id='fixedFormat-preview-pane']//div[@class='row previewPane']//div[@class='row previewWrapper']//div[@class='instruction-inner']//span", "title");
            Assert.AreEqual(_hoTxt, "ho for comment question");
            Console.WriteLine(_hoTxt);
        }

        [TestMethod]
        public void CheckHOForRatingScaleQ()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "EmbbedForm_05");
            AuthorActions.AddRatingScaleWithHoverText(driver, "How do you rate the training", new string[] { "Very interesting", "Not so interesting" }, "hover text for rs");
            Thread.Sleep(5000);
            string _hoTxt = UIActions.GetAttrForElementByXpath(driver, "//span[@data-bind='isMandatory']/span/span", "title");
            Assert.AreEqual("hover text for rs", _hoTxt);
        }

        [TestMethod]
        public void CheckHOForCommentQuestion()
        {
            string questionTxt = "3.Please let us know you feedback";
            string hoverTxt = "hover text for comment question";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "EmbbedForm_05");
            string _gfeHo = AuthorActions.AddCommentQuestionWithHoverText(driver, questionTxt, hoverTxt);
            //IReadOnlyCollection<IWebElement> _questionCollection = UIActions.FindElementsWithXpath(driver, "//span[@data-bind='BindQuestionTile']");
            //_questionCollection.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title"); 
            Thread.Sleep(5000);
            IReadOnlyCollection<IWebElement> _questionWithHOCollection = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            string _titleInSurveyForm = _questionWithHOCollection.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            //Console.WriteLine(_gfeHo);
            //Console.WriteLine(_questionWithHOCollection.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title"));
            AuthorActions.capturePreviewLink(driver);
            Thread.Sleep(5000);
            IReadOnlyCollection<IWebElement> _questionWithHOCollectionInPreview = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            Thread.Sleep(2000);
            string _titleInPreview = _questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            //Console.WriteLine(_questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title"));
            Array.TrueForAll(new string[] { _gfeHo, _titleInSurveyForm, _titleInPreview }, x => x.Equals(hoverTxt));
        }

        [TestMethod]
        public void CheckHOForDDQuestion()
        {
            string questionTxt = "Which country do you belong to?";
            string hoverTxt = "Country demographics";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "EmbbedForm_05");
            string _gfeHo = AuthorActions.AddDDtQuestionWithHoverText(driver, questionTxt, hoverTxt, new string[] { "India", "Germany" });
            //IReadOnlyCollection<IWebElement> _questionCollection = UIActions.FindElementsWithXpath(driver, "//span[@data-bind='BindQuestionTile']");
            //_questionCollection.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            Thread.Sleep(5000);
            IReadOnlyCollection<IWebElement> _questionWithHOCollection = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            string _titleInSurveyForm = _questionWithHOCollection.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            //Console.WriteLine(_gfeHo);
            //Console.WriteLine(_questionWithHOCollection.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title"));
            AuthorActions.capturePreviewLink(driver);
            Thread.Sleep(5000);
            IReadOnlyCollection<IWebElement> _questionWithHOCollectionInPreview = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            string _titleInPreview = _questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            //Console.WriteLine(_questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title"));
            Array.TrueForAll(new string[] { _gfeHo, _titleInSurveyForm, _titleInPreview }, x => x.Equals(hoverTxt));
        }

        [TestMethod]
        public void CheckHOForMCQ()
        {
            string questionTxt = "What the training duration sufficient";
            string hoverTxt = "Country demographics";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "EmbbedForm_05");
            string _gfeHo = AuthorActions.AddMCQQuestionWithHoverText(driver, 
                new string[] { "What the training duration sufficient","What is the training room convinient"}, hoverTxt, new string[] { "Yes", "No" });
            Thread.Sleep(5000);
            IReadOnlyCollection<IWebElement> _questionCollection = UIActions.FindElementsWithXpath(driver, "//span[@data-bind='isMandatory'][@data-bind-type='multi-class']/span/span");
            Thread.Sleep(5000);
            string _Title = _questionCollection.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            Console.WriteLine(_gfeHo);
            AuthorActions.capturePreviewLink(driver);
            Thread.Sleep(5000);
            IReadOnlyCollection<IWebElement> _questionWithHOCollectionInPreview = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            Thread.Sleep(5000);
            _questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            Console.WriteLine(_questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title"));
        }
        
        [TestMethod]
        public void CheckHOForFF()
        {
            string questionTxt = "Enter you Date of joining";
            string hoverTxt = "doj";
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "EmbbedForm_05");
            string _gfeHo = AuthorActions.AddFixedFormatDateQuestionAsDemoWithHO(driver, questionTxt, hoverTxt);
            //Console.WriteLine(_gfeHo);
            //IReadOnlyCollection<IWebElement> _questionCollection = UIActions.FindElementsWithXpath(driver, "//span[@data-bind='BindQuestionTile']");
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> _questionInExpandedView = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            string _titleInSurveyForm = _questionInExpandedView.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            //Console.WriteLine(_titleInSurveyFomr);
            AuthorActions.capturePreviewLink(driver);
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _questionWithHOCollectionInPreview = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            
            string _titleInPreview = _questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title");
            //Console.WriteLine(_questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionTxt)).GetAttribute("title"));
            Array.TrueForAll(new string[] { _gfeHo, _titleInSurveyForm, _titleInPreview }, x => x == hoverTxt);
        }

        //public void CheckHOInRepublishFlow(IWebDriver driver, string questiontext, string hovertext, string saveButtonElement)
        //{
        //    OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
        //    IReadOnlyCollection<IWebElement> _questionCollection = UIActions.FindElementsWithXpath(driver, "//span[@data-bind='BindQuestionTile']");
        //    _questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questiontext)).Click();
        //    ac.MoveToElement(_questionCollection.FirstOrDefault(x => x.GetAttribute("title").Contains(questiontext))).Build().Perform();
        //    IReadOnlyCollection<IWebElement> _questions = UIActions.FindElementsWithXpath(driver, "//div[@class='panel panel-default question-list ui-droppable']//span[@title='Edit']");
        //    for (int i = 1; i <= _questions.Count(); i++)
        //    {
        //        try
        //        {
        //            bool v = UIActions.GetElementswithXpath(driver, "//div[@class='panel panel-default question-list ui-droppable'][" + i + "]//span[@title='Edit']", 5).Displayed;
        //            UIActions.GetElementswithXpath(driver, "//div[@class='panel panel-default question-list ui-droppable'][" + i + "]//span[@title='Edit']", 2).Click();
        //        }
        //        catch (Exception)
        //        {

        //        }
        //    }
        //    Thread.Sleep(2000);
        //   // AuthorActions.EditHoverText(driver, questiontext, hovertext);
        //    UIActions.clickwithID(driver, saveButtonElement);
        //}

        [TestMethod]
        public void CheckHOTextInRepublishFF()
        {
            RemoteWebDriver driver = null;
            string parentWindowHandle = null;
            /* Question & Hover to edit */
            xl = new ExcelToDataTable();
            string[] questionType = {  "FixedFormat", "DropDown"};
            foreach(string q in questionType)
            {
               
                string surveyToCopy = xl.readXl(q).formName;
                string destinationFormName = "HO";
                string questionText = xl.readXl(q).questionTxt;
                string hoverText = xl.readXl(q).alterHoverTxt;
                string saveBtn = xl.readXl(q).saveBtn;
                string QuestionTextEditorElement = xl.readXl(q).QuestionTextEditorElement;
                string hoverElement = xl.readXl(q).HoverElement;
                string hoverTextEditorElement = xl.readXl(q).HoverTextEditorElement;
                string okBtn = xl.readXl(q).okBtn;
                /* using existing surveyform 'HoverTextWithAllQuestions' to perform this */
                if (driver == null)
                {
                    driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
                    
                    OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
                    AuthorActions.CopyForm(driver, surveyToCopy, destinationFormName + DateTime.Now);
                    parentWindowHandle = driver.CurrentWindowHandle;
                }
               
                AuthorActions.EditHoverTextForSingleQuestion(driver, questionText, hoverText, saveBtn, QuestionTextEditorElement, hoverElement, hoverTextEditorElement, okBtn);
                IReadOnlyCollection<IWebElement> _questionWithHOCollection1 = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
                Thread.Sleep(6000);
                string _titleInSurveyForm = _questionWithHOCollection1.FirstOrDefault(x => x.Text.Contains(questionText)).GetAttribute("title");
                Thread.Sleep(5000);
                AuthorActions.capturePreviewLink(driver);
                Thread.Sleep(5000);
                IReadOnlyCollection<IWebElement> _questionWithHOCollectionInPreview = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
                Thread.Sleep(2000);
                string _titleInPreview = _questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(xl.readXl(q).questionTxt)).GetAttribute("title");
                Array.TrueForAll(new string[] { _titleInSurveyForm, _titleInPreview }, x => x.Equals(xl.readXl(q).alterHoverTxt));
                xl.writeXl(_titleInSurveyForm, _titleInPreview, Array.TrueForAll(new string[] { _titleInSurveyForm, _titleInPreview }, x => x.Equals(xl.readXl(q).alterHoverTxt)), q);
                driver.Close();
                driver.SwitchTo().Window(parentWindowHandle);
               
            }
            
        }

        [TestMethod]
        public void CheckHOTextInRepublishFFDT(HoverTxtDto hoverTxt)
        {
            /* Question & Hover to edit */
            xl = new ExcelToDataTable();
            string questionTypo = "RatingScale";
            xl.readXl(questionTypo);

            string formNameToCopy = "HoverTextWithAllQuestions";
            formNameToCopy = hoverTxt.formName;
            string destinationFormName = "HO";
            string questionText = "Please enter the date of joining";
            questionText = hoverTxt.questionType;
            string hoverText = "Joining Date";
            string hoverElement = hoverTxt.HoverElement;
            string hoverTextEditorElement = hoverTxt.HoverTextEditorElement;
            string okBtn = hoverTxt.okBtn;
            ExcelToDataTable.xl.readXl(questionTypo);

            /* using existing surveyform 'HoverTextWithAllQuestions' to perform this */
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CopyForm(driver, formNameToCopy, destinationFormName + DateTime.Now);
            AuthorActions.EditHoverTextForSingleQuestion(driver, questionText, hoverText, "btnFixedFormatSave", "#txtDropDownQuestion", hoverElement, hoverTextEditorElement, okBtn);
            IReadOnlyCollection<IWebElement> _questionWithHOCollection1 = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            string _titleInSurveyForm = _questionWithHOCollection1.FirstOrDefault(x => x.Text.Contains(questionText)).GetAttribute("title");
            Thread.Sleep(5000);
            AuthorActions.capturePreviewLink(driver);
            Thread.Sleep(5000);
            IReadOnlyCollection<IWebElement> _questionWithHOCollectionInPreview = UIActions.FindElementsWithXpath(driver, "//span[@class='source-info']");
            Thread.Sleep(2000);
            string _titleInPreview = _questionWithHOCollectionInPreview.FirstOrDefault(x => x.Text.Contains(questionText)).GetAttribute("title");
            Array.TrueForAll(new string[] { _titleInSurveyForm, _titleInPreview }, x => x.Equals(hoverText));
            new HoverTxtDto
            {
                result1 = _titleInSurveyForm,
                result2 = _titleInPreview,
                result3 = Array.TrueForAll(new string[] { _titleInSurveyForm, _titleInPreview }, x => x.Equals(hoverText))
            };
            
        }

        private static void EnterInstructionText(RemoteWebDriver driver, string text)
        {
            /* Click on instruction text */
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            Thread.Sleep(2000);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            IReadOnlyCollection<IWebElement> cont = driver.FindElements(By.XPath("//*[@id='fixedFormat-response']/div/div[1]/div[2]/div"));
            foreach (var item in cont)
            {
                executor.ExecuteScript("arguments[0].click();", item);
                item.Click();
                item.SendKeys("Instruction text for comment question");
            }
        }

        private static void EnableInstructionText(RemoteWebDriver driver)
        {
            /* Enable instruction text */
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _headers = UIActions.FindElements(driver, ".margin-left-5");
            Thread.Sleep(5000);
            _headers.FirstOrDefault(x => x.Text == "Additional Options").Click();
            Thread.Sleep(2000);
            UIActions.GetElement(driver, "#chkInclude").Click();
        }
       
        #endregion HoverText

      

        [TestMethod]
        public void ValidateEditClosedEndPageSurveyAndInPlayer()
        {

            FormName = "Edit_Tellurium_EndPageTest Survey 1";
            var customText = "Edited custom closed text by Telurium " + Guid.NewGuid();

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);

            IWebElement Publishedicon = driver.FindElement(By.XPath("//span[@class='surveyTitle-statusicon published-success display-inline-block margin-top-5']"));
            var publishedText = Publishedicon.Text;
            //Edit Form

            if (publishedText == "PUBLISHED")
            {
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(15000);
            }

            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Closed");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '12']");

            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[3]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys(customText);
            emailBody.Click();
            Thread.Sleep(3000);

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(7000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Edit_Tellurium_EndPageTest Survey 1");
            /*UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(15000);

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/30806/0/f59966b4-0da3-403c-a108-5d6753d79141";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(2000));
            var msgElem = By.XPath("//*[@id='main_content']/div/div/div");
            wait.Until(ExpectedConditions.ElementIsVisible(msgElem));
            bool isTextDisplayed = playerDriver.FindElement(msgElem).Text.Equals(customText);
            Assert.IsTrue(isTextDisplayed);

        }

        [TestMethod]
        public void ValidateEditOverQuotaEndPageSurveyAndInPlayer()
        {
            FormName = "Edit_Tellurium_EndPageTest Survey 1";
            var customText = "Edited custom over quota text by Telurium " + Guid.NewGuid();

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(10000);

            IWebElement Publishedicon = driver.FindElement(By.XPath("//span[@class='surveyTitle-statusicon published-success display-inline-block margin-top-5']"));
            var publishedText = Publishedicon.Text;
            //Edit Form

            if (publishedText == "PUBLISHED")
            {
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(17000);
            }
            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Over Quota");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '11']");

            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[2]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys(customText);
            emailBody.Click();
            Thread.Sleep(3000);

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(10000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Edit_Tellurium_EndPageTest Survey 1");
            /* UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
             Thread.Sleep(2000);
             UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(10000);

            var isModalClosed = UIActions.IsElementNotVisible(driver, "#thank-you-page-modal");

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/31092/0/e0857968-a91c-4113-98ef-88e096a7a34f";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(2000));
            Thread.Sleep(2000);
            driver.FindElement(By.Id("btnFinish")).Click();
            /*var msgElem = By.XPath("//*[@id='main_content']/div/div/div");
            wait.Until(ExpectedConditions.ElementIsVisible(msgElem));*/
            IWebElement EditOverQuota = driver.FindElement(By.XPath("//*[@data-bind='passageDTO.content.htmlContent']"));
            bool isTextDisplayed = EditOverQuota.Text.Trim().Equals(customText);
           /* bool isTextDisplayed1 = playerDriver.FindElement(msgElem).Text.Equals(customText);*/
            
            Assert.IsTrue(isTextDisplayed);

        }


        [TestMethod]
        public void ValidateEditCompletedEndPageSurveyAndInPlayer()
        {

            FormName = "Edit_Tellurium_EndPageTest Survey 1";
            var customText = "Edited custom completed text by Telurium " + Guid.NewGuid();

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(15000);

            IWebElement Publishedicon = driver.FindElement(By.XPath("//span[@class='surveyTitle-statusicon published-success display-inline-block margin-top-5']"));
            var publishedText = Publishedicon.Text;
            //Edit Form

            if (publishedText == "PUBLISHED")
            {
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(15000);
            }


            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Completed");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '2']");

            IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='thank-you-page-context']/div/div[1]/div[4]/div[1]/div/div/div"));
            emailBody.Clear();
            emailBody.SendKeys(customText);
            emailBody.Click();
            Thread.Sleep(3000);

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(10000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Edit_Tellurium_EndPageTest Survey 1");
            /* UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
             Thread.Sleep(2000);
             UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(15000);

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/30807/0/2c26bd12-0c22-4e44-9866-b81b40e8f174";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(2000));

            Thread.Sleep(5000);
            UIActions.Click(playerDriver, "#btnFinish");
            Thread.Sleep(5000);


            var msgElem = By.XPath("//*[@id='main_content']/div/div[2]/div[2]/div/div/div/div[4]/div/div/div/div/div/div[1]/div/div/div/div[1]/div[2]/div[2]/div/div");
            wait.Until(ExpectedConditions.ElementIsVisible(msgElem));
            bool isTextDisplayed = driver.FindElement(msgElem).Text.Equals(customText);
            Assert.IsTrue(isTextDisplayed);

        }


        [TestMethod]
        public void ValidateDefaultSubmittedEndPageSurveyAndInPlayer()
        {
            FormName = "Survey 2_Tellurium_EndPageTest";
            var text = "You have already submitted a response to this survey. To exit this survey, please close your browser.";


            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(15000);

            IWebElement Publishedicon = driver.FindElement(By.XPath("//span[@class='surveyTitle-statusicon published-success display-inline-block margin-top-5']"));
            var publishedText = Publishedicon.Text;
            //Edit Form

            if (publishedText == "PUBLISHED")
            {
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(17000);
            }

            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Already Submitted");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '10']");

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(10000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Survey 2_Tellurium_EndPageTest");
            /*UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(15000);

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/30859/1/5bc90896-38c5-4675-96b6-f70b4bb1d462";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(7000));
            var msgElem = By.XPath("//*[@id='main_content']/div/div/div");
            wait.Until(ExpectedConditions.ElementIsVisible(msgElem));
            bool isTextDisplayed = playerDriver.FindElement(msgElem).Text.Equals(text);
            Assert.IsTrue(isTextDisplayed);

        }

        [TestMethod]
        public void ValidateDefaultClosedEndPageSurveyAndInPlayer()
        {

            FormName = "Survey 2_Tellurium_EndPageTest";
            var text = "This survey is now closed. Thank you for your interest. To exit this survey, please close your browser.";

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(15000);

            IWebElement Publishedicon = driver.FindElement(By.XPath("//span[@class='surveyTitle-statusicon published-success display-inline-block margin-top-5']"));
            var publishedText = Publishedicon.Text;
            //Edit Form

            if (publishedText == "PUBLISHED")
            {
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(15000);
            }

            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Closed");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '9']");

            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(10000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Survey 2_Tellurium_EndPageTest");
            /*UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(15000);

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/30856/0/0e3b6262-cc6e-4747-902f-d848004952d7";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(2000));
            var msgElem = By.XPath("//*[@id='main_content']/div/div/div");
            wait.Until(ExpectedConditions.ElementIsVisible(msgElem));
            bool isTextDisplayed = playerDriver.FindElement(msgElem).Text.Equals(text);
            Assert.IsTrue(isTextDisplayed);

        }

        [TestMethod]
        public void ValidateDefaultOverQuotaEndPageSurveyAndInPlayer()
        {
            FormName = "Survey 2_Tellurium_EndPageTest";
            var text = "Thank you for your participation. The quota for this survey based on your demographics has already been met. Thank you for your interest. To exit this survey, please close your browser.";

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(15000);

            IWebElement Publishedicon = driver.FindElement(By.XPath("//span[@class='surveyTitle-statusicon published-success display-inline-block margin-top-5']"));
            var publishedText = Publishedicon.Text;
            //Edit Form

            if (publishedText == "PUBLISHED")
            {
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(15000);
            }

            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Over Quota");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '8']");
            
            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(7000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Survey 2_Tellurium_EndPageTest");
           /* UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(15000);

            var isModalClosed = UIActions.IsElementNotVisible(driver, "#thank-you-page-modal");

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/30857/0/9d1dd8d0-e2ae-42a8-afed-6f4cdf440341";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            Thread.Sleep(2000);
            driver.FindElement(By.Id("btnFinish")).Click();
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(2000));
            var msgElem = By.XPath("//*[@id='main_content']/div/div/div");
            wait.Until(ExpectedConditions.ElementIsVisible(msgElem));
            bool isTextDisplayed = playerDriver.FindElement(msgElem).Text.Equals(text);
            Assert.IsTrue(isTextDisplayed);

        }


        [TestMethod]
        public void ValidateDefaultCompletedEndPageSurveyAndInPlayer()
        {

            FormName = "Survey 2_Tellurium_EndPageTest";
            var text = "Thank you for your participation.";

            RemoteWebDriver driver = SetDriverAndNavigateToForm(DriverAndLogin.Browser);
            Thread.Sleep(15000);

            //Edit Form
            IWebElement Publishedicon = driver.FindElement(By.XPath("//span[@class='surveyTitle-statusicon published-success display-inline-block margin-top-5']"));
            var publishedText = Publishedicon.Text;
            //Edit Form

            if (publishedText == "PUBLISHED")
            {
                UIActions.Click(driver, "#btnFormEdit");
                Thread.Sleep(2000);
                UIActions.Click(driver, "#edit-form-button");
                Thread.Sleep(17000);
            }

            var firstPage = UIActions.FindElements(driver, ".page-list").FirstOrDefault();
            firstPage.Click();
            AuthorActions.OpenEndPage(driver);
            Thread.Sleep(3000);
            UIActions.SelectInCEBDropdownByText(driver, "#select-end-page-type", "Survey Completed");

            UIActions.Click(driver, "input[type = 'radio'][name = 'destination-type-radio'][value = '1']");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#btnThankYouPageSave");
            Thread.Sleep(10000);

            //Publish Form
            AuthorActions.PublishForm(driver, "Survey 2_Tellurium_EndPageTest");
           /* UIActions.Click(driver, "#formHeader>div.col-sm-12.col-md-9>div>div.col-xs-10.align-right>ul>li:nth-child(7)>button");
            Thread.Sleep(2000);
            UIActions.Click(driver, "#btnConfirmPublishClicked");*/
            Thread.Sleep(15000);

            RemoteWebDriver playerDriver = GetDriver(DriverAndLogin.Browser);
            Common.DriverAndLoginDto browserProperties = new Common.DriverAndLoginDto();
            browserProperties.Url = "https://qa-surveys.cebglobal.com/Pulse/Player/Start/30859/0/fa0190cb-b5a8-4cda-b0b0-ac9bae8e4435";
            playerDriver.Manage().Window.Maximize();
            playerDriver.Navigate().GoToUrl(browserProperties.Url);
            WebDriverWait wait = new WebDriverWait(playerDriver, TimeSpan.FromSeconds(2000));

            Thread.Sleep(5000);
            UIActions.Click(playerDriver, "#btnFinish");
            Thread.Sleep(5000);


            //var msgElem = By.XPath("//*[@id='main_content']/div/div[2]/div[2]/div/div/div/div[4]/div/div/div/div/div/div[1]/div/div/div/div[1]/div[2]/div[2]/div/div");
            // var msgElem = driver.FindElement(By.CssSelector(".content-inner"));
            //wait.Until(ExpectedConditions.ElementIsVisible(msgElem));
            var msgElem = driver.FindElement(By.XPath("//*[@class='bolder']"));
            bool isTextDisplayed = msgElem.Text.Equals(text);
            Assert.IsTrue(isTextDisplayed);
            

        }

        #endregion

        #region ScaleHeaders
        [TestMethod]
        public void VerifyPresenceOfScaleHeaders()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            var ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.NavToAccountSettings(driver);
            AuthorActions.SelectAccountDefaults(driver);
            //AccountSettings.selectScaleRange(driver, "1 - 5");
            Thread.Sleep(2000);
            AccountSettings.SetScaleHeaders(driver, new string[] { "One", "two", "three", "four", "five" });
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#defaults-save"))).Build().Perform();
            driver.FindElement(By.CssSelector("button#defaults-save")).Click();
            Thread.Sleep(2000);
            bool isAlertPresent = driver.FindElement(By.XPath("//span[@id='action-status']")).Text.Equals("Account Defaults updated.");
            Assert.IsTrue(isAlertPresent);
        }

        [TestMethod]
        public void VerifyAccountSettingsReflectInSurveyForm()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            AuthorActions.NavToAccountSettings(driver);
            AuthorActions.SelectAccountDefaults(driver);
            List<string> scaleRangeListInSettings = AccountSettings.GetScaleRangeInAccountSettings(driver);
            AuthorActions.NavigateToAuthor(driver);
            AuthorActions.CreateForm(driver, "ScaleRange");
            AuthorActions.OpenSettingsModalTab(driver);
            List<string> scaleRangeListInForm = AccountSettings.GetScaleRangeInForm(driver);
           // Console.WriteLine(scaleRangeListInSettings[0], scaleRangeListInSettings[1], scaleRangeListInForm[0], scaleRangeListInForm[1]);
            Assert.IsTrue(scaleRangeListInSettings.SequenceEqual(scaleRangeListInForm));
        }

        [TestMethod]
        public void VerifyIfScaleRangeISEditable()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.CreateForm(driver, DateTime.Now.ToString());
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(1000);
            string newSetScaleRange = AccountSettings.SetScaleRangeInForm(driver, "1 - 3");
            Assert.IsTrue(newSetScaleRange.Equals("1 - 3"));
        }

        [TestMethod]
        public void VerifyIfScaleHeaderIsEditable()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            List<string> setVal = new List<string> { "One", "Two", "Three" };
            List<string> headerLabel = new List<string>();
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.CreateForm(driver, DateTime.Now.ToString());
            Thread.Sleep(4000);
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(4000);
            AccountSettings.SetScaleRangeInForm(driver, "1 - 3");
            AccountSettings.SetScaleHeaderInForm(driver, setVal);
            Thread.Sleep(4000); ;
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#btnFormSettingsSave"))).Build().Perform();
            Thread.Sleep(4000);
            driver.FindElement(By.CssSelector("button#btnFormSettingsSave")).Click();
            Thread.Sleep(4000);
            driver.FindElement(By.CssSelector(".scale-header-confirmation button#btnConfirmYes")).Click();
            Thread.Sleep(4000);
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(4000);
            List<string> headers = AccountSettings.GetScaleHeaderInForm(driver);
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> afterSave = driver.FindElements(By.CssSelector("div input.defaultResponseTextAddRatingScale"));
            foreach (IWebElement savedHeader in afterSave)
            {
                string val = savedHeader.GetAttribute("value");
                headerLabel.Add(val);
            }
            Assert.IsTrue(headers.Any(x => headerLabel.Contains(x)));
            Assert.IsTrue(headers.Any(x => headers.Contains(x)));
            //var list1_in_list2 = from first in setVal
            //                     join second in headers
            //                     on first.GetType() equals second.GetType()
            //                     select first;
            // bool isMatch = setVal.Except(headers);
        }

        [TestMethod]
        public void VerifyOverrideAccLevelWithFormLevel()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.NavToAccountSettings(driver);
            AuthorActions.SelectAccountDefaults(driver);
            Thread.Sleep(2000);
            AccountSettings.ClearScaleHeadersInAccountSettings(driver);
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#defaults-save"))).Build().Perform();
            driver.FindElement(By.CssSelector("button#defaults-save")).Click();
            AuthorActions.NavigateToAuthor(driver);
            AuthorActions.SearchAndOpenForm(driver, "09/26/2018 2:28:32 PM");
            AuthorActions.AddRatingScaleQuestion(driver, "How do you rate the employee benefit of this company"+DateTime.Now, "0-5", new string[] { "VeryEfficient", "Efficient", "Moderate", "Not Useful","Unresourceful","Junk"});
            //driver.FindElement(By.CssSelector("button#btnRatingScale"));
            Thread.Sleep(2000);
            int scaleCnt = driver.FindElements(By.XPath("//div[@class='panel panel-default question-list ui-droppable']//div[@class='qs-section pulse-ce-player-tabular-single-item']//th")).Count();
            Thread.Sleep(2000);
            Console.WriteLine(scaleCnt);
            Assert.IsTrue(scaleCnt.ToString().Equals("6"));
        }

        [TestMethod]
        public void CheckIfFormSettingsIsAppliedOnNewRS()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "09/26/2018 2:28:32 PM");
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(2000);
            List<string> scaleRange_Header = AccountSettings.GetScaleRangeInForm(driver);
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#btnFormSettingsSave"))).Build().Perform();
            driver.FindElement(By.CssSelector("button#btnFormSettingsSave")).Click();
            Thread.Sleep(4000);
            AuthorActions.AddRatingScaleQuestion(driver, "Would you recommend this policy to other employees?");
            Thread.Sleep(2000);
            int scaleCnt = driver.FindElements(By.XPath("//div[@class='panel panel-default question-list ui-droppable']//div[@class='qs-section pulse-ce-player-tabular-single-item']//th")).Count();
            Assert.IsTrue(scaleRange_Header[1].Equals(scaleCnt.ToString()));
        }

        [TestMethod]
        public void CheckIsAlertAppearsOnScaleChange()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "09/26/2018 2:28:32 PM");
            AuthorActions.OpenSettingsModalTab(driver);
            AccountSettings.SetScaleRangeInForm(driver, "1 - 3");
            AccountSettings.SetScaleHeaderInForm(driver, new List<string> { "VeryEfficient", "Efficient", "Moderate"});
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#btnFormSettingsSave"))).Build().Perform();
            driver.FindElement(By.CssSelector("button#btnFormSettingsSave")).Click();
            Array.TrueForAll(new string[] { driver.FindElement(By.CssSelector(".scale-header-confirmation #btnConfirmYes")).Displayed.ToString(), driver.FindElement(By.CssSelector(".scale-header-confirmation #btnConfirmNo")).Displayed.ToString() }, x => x.Equals(true));
        }

        [TestMethod]
        public void AcceptChangeForExistingQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            List<string> newHeaders = new List<string> { "Beneficial", "Useful", "Irrelevant" };
            AuthorActions.SearchAndOpenForm(driver, "09/26/2018 2:28:32 PM");
            AuthorActions.AddRatingScaleQuestion(driver, "To validate the header change");
            Thread.Sleep(4000);
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(4000);
            AccountSettings.ChangeScaleHeaderInForm(driver, newHeaders);
            Thread.Sleep(4000);
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#btnFormSettingsSave"))).Build().Perform();
            driver.FindElement(By.CssSelector("button#btnFormSettingsSave")).Click();
            Thread.Sleep(4000);
            driver.FindElement(By.CssSelector(".scale-header-confirmation #btnConfirmYes")).Click();
            Thread.Sleep(4000);
            IReadOnlyCollection<IWebElement> rsLabels =  driver.FindElements(By.XPath("//div[@class='panel panel-default question-list ui-droppable']//div[@class='qs-section pulse-ce-player-tabular-single-item']//th/span[@data-bind-type='sanitizedhtml']"));
            List<string> labelHeaders1 = new List<string> (rsLabels.Select(x => x.Text));
            Assert.IsTrue(labelHeaders1.Any(x => newHeaders.Contains(x)));
        }

        [TestMethod]
        public void AcceptChangeForNewRSAndCheckNewLablesInOldRS()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            List<string> newHeaders = new List<string> { "Beneficial", "Useful", "Irrelevant" };
            AuthorActions.SearchAndOpenForm(driver, "09/26/2018 2:28:32 PM");
            AuthorActions.AddRatingScaleQuestion(driver, "To validate the header change");
            Thread.Sleep(3000);
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(4000);
            AccountSettings.ChangeScaleHeaderInForm(driver, newHeaders);
            Thread.Sleep(3000);
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#btnFormSettingsSave"))).Build().Perform();
            driver.FindElement(By.CssSelector("button#btnFormSettingsSave")).Click();
            Thread.Sleep(1000);
            driver.FindElement(By.CssSelector(".scale-header-confirmation #btnConfirmNo")).Click();
            Thread.Sleep(2000);
            IReadOnlyCollection<IWebElement> rsLabels = driver.FindElements(By.XPath("//div[@class='panel panel-default question-list ui-droppable']//div[@class='qs-section pulse-ce-player-tabular-single-item']//th/span[@data-bind-type='sanitizedhtml']"));
            List<string> labelHeaders1 = new List<string>(rsLabels.Select(x => x.Text));
            Assert.IsFalse(labelHeaders1.Any(x => newHeaders.Contains(x)));
        }

        [TestMethod]
        public void CheckScaleHeadersInCopiedQuestion()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.SearchAndOpenForm(driver, "09/26/2018 2:28:32 PM");
            Thread.Sleep(3000);
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(3000);
            List<string> formHeader = AccountSettings.GetScaleRangeInForm(driver);
            ac.MoveToElement(driver.FindElement(By.CssSelector("button#btnFormSettingsSave"))).Build().Perform();
            driver.FindElement(By.CssSelector("button#btnFormSettingsSave")).Click();
            Thread.Sleep(3000);
            AuthorActions.CopyQuestionFromQuestionsTab(driver, "Rating Scale Question -Data Transfer");
            Thread.Sleep(1000);
            IReadOnlyCollection<IWebElement> rsLabels = driver.FindElements(By.XPath("//div[@class='panel panel-default question-list ui-droppable']//div[@class='qs-section pulse-ce-player-tabular-single-item']//th/span[@data-bind-type='sanitizedhtml']"));
            List<string> labelHeaders1 = new List<string>(rsLabels.Select(x => x.Text));
            Assert.IsFalse(formHeader[1].Equals(labelHeaders1.Count().ToString()));
        }

        [TestMethod]
        public void CheckRatingScaleInCopiedSurvey()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToHome(DriverAndLogin.Browser);
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            AuthorActions.NavToAccountSettings(driver);
            AuthorActions.SelectAccountDefaults(driver);
            List<string> scaleRangeListInSettings = AccountSettings.GetScaleRangeInAccountSettings(driver);
            AuthorActions.NavigateToAuthor(driver);
            Thread.Sleep(5000);
            AuthorActions.CopyForm(driver, "ScaleHeader.SourceForm", DateTime.Now.ToString());
            AuthorActions.OpenSettingsModalTab(driver);
            Thread.Sleep(1000);
            List<string> accountHeaders = AccountSettings.GetScaleRangeInForm(driver);
            Assert.IsFalse(accountHeaders[1].Equals(scaleRangeListInSettings[1]));
        }

        [TestMethod]
        public void CleanQuestionsInForm()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            AuthorActions.SearchAndOpenForm(driver, "EmbbedForm_05");
            AccountSettings.CleanCreatedQuestionsFromForm(driver);
        }
        #endregion
        
        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
               driver.Close();
               driver.Dispose();
            }
        }
    }
}


