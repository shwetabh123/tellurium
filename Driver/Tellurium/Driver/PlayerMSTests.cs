﻿using System;
using System.Windows.Forms;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions.Internal;


using Common;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Driver
{
    [TestClass]
    public class PlayerMSTests
    {
        public static string clipboardText;
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string playerURL;
        private static string FormName;
        public static string FileName;
        public static IWebDriver Driver;

        public static string distributionName = "Thunderbird";


        //public static int SurveyID = 13014;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };

            //DBConnectionParameters = new DBConnectionStringDTO
            //{
            //    userName = context.Properties["DBUserName"].ToString(),
            //    password = context.Properties["DBUserPassword"].ToString(),
            //    serverName = context.Properties["DBServerName"].ToString()
            //};
        }


        public static void GetPlayerURL(int SurveyID)
        {
            try
            {


                playerURL = DBOperations.getSurveyURLSByDistribution(SurveyID, DBConnectionParameters.userName, DBConnectionParameters.password, DBConnectionParameters.TCESserverName);

            }
            catch (TypeInitializationException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        }

        [TestMethod]
        public void ValidateDemographicLogicPlayer()
        {
            GetPlayerURL(26010);

            RemoteWebDriver plDriver = InitializeAndGetDriver(true, DriverAndLogin.Browser, playerURL);

            Thread.Sleep(10000);


            Assert.AreEqual(true, plDriver != null);
        }





        [TestMethod]
        public void ValidateQuestionLogicPlayerAnyOffPosScenario()

        {


            //Hide--Question --Positive scenario---any off ----(1or2 or 1) 
            //SeleniumTestFormc76fddec-8280-4653-931e-03387c4f39c4

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "Player-Tellu1");
            Thread.Sleep(20000);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            Thread.Sleep(30000);
            IWebElement Questionone = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[2]");
            Questionone.Click();
            Thread.Sleep(10000);
            IWebElement nextbutton = UIActions.FindElementWithXpath(driver, " //*[@id='btnNext']/div");
            nextbutton.Click();
            Thread.Sleep(10000);
            UIActions.IsElementNotVisible(driver, "(//*[contains(@id,'qgroup_simple')])[1]");//---working
            Thread.Sleep(10000);
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(40000);
            driver.Quit();
        }


        [TestMethod]
        public void ValidateQuestionLogicPlayerAllOffPosScenario()

        {


            //Hide-- Question-- Positive scenario---All off ( 1or 2 and 1)
            //Copy Of SeleniumTestFormc76fddec-8280-4653-931e-03387c4f39c4
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "Player-Tellu2");
            Thread.Sleep(20000);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            IWebElement Questionone = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[2]");
            Questionone.Click();
            Thread.Sleep(10000);
            IWebElement Questiontwo = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[4]");
            Questiontwo.Click();
            Thread.Sleep(10000);
            IWebElement nextbutton = UIActions.FindElementWithXpath(driver, " //*[@id='btnNext']/div");
            nextbutton.Click();
            // UIActions.mouseHoverToElementAndClick(driver, " //*[@id='btnNext']/div");
            Thread.Sleep(10000);
            IWebElement sixthquestion = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'qgroup_simple')])[1]");
            UIActions.IsElementNotVisible(driver, "(//*[contains(@id,'qgroup_simple')])[1]");
            Thread.Sleep(10000);
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(40000);
            driver.Quit();
        }







        [TestMethod]
        public void ValidateQuestionHideLogicPlayerAllOffNegaSce()

        {
            // Hide--Question ---Negative scenario  --All off ( 1or 2 and 2)

            //Copy Of SeleniumTestFormc76fddec-8280-4653-931e-03387c4f39c4
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "Player-Tellu2");
            Thread.Sleep(20000);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            Thread.Sleep(30000);
            IWebElement Questionone = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[2]");
            Questionone.Click();
            Thread.Sleep(10000);
            IWebElement Questiontwo = UIActions.FindElementWithXpath(driver, "(//*[text()='2'])[4]");
            Questiontwo.Click();
            Thread.Sleep(10000);
            UIActions.MouseHoverToElementAndClick(driver, " //*[@id='btnNext']/div");
            Thread.Sleep(10000);
            IWebElement sixthquestion = UIActions.FindElementWithXpath(driver, "(//*[contains(@id,'qgroup_simple')])[1]");
            // Assert.IsTrue(sixthquestion != null && sixthquestion.Displayed && sixthquestion.Enabled);---working
            UIActions.IsElementVisible(driver, "(//*[contains(@id,'qgroup_simple')])[1]");
            Thread.Sleep(10000);
            UIActions.getScreenshot(driver);
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(40000);

            driver.Quit();

        }

        [TestMethod]
        public void ValidateQuestionResponseHideLogicPlayerAnyOff()

        {

            //Question Response

            //Hide--Question Response-----Q6---Director --Any  off
            //Copy Of Copy Of SeleniumTestFormc76fddec-8280-4653-931e-03387c4f39c4

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "Player-Tellu3");
            Thread.Sleep(20000);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            Thread.Sleep(30000);
            IWebElement Questionone = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[2]");
            Questionone.Click();
            Thread.Sleep(10000);
            IWebElement nextbutton = UIActions.FindElementWithXpath(driver, " //*[@id='btnNext']/div");
            nextbutton.Click();
            Thread.Sleep(10000);
            IWebElement sixthquestiondropdown = UIActions.FindElementWithXpath(driver, "//*[@class='btn-group ceb-dropdown-container']");
            sixthquestiondropdown.Click();
            Thread.Sleep(40000);
            UIActions.IsElementNotVisible(driver, "//ul[@class='multiselect-container dropdown-menu'])[2]//label[text()=' Director']");
            Thread.Sleep(20000);
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(40000);
            driver.Quit();
        }


        [TestMethod]
        public void ValidateQuestionResponseHideLogicPlayerAllOff()

        {

            //Question Response

            //Hide--Question Response---------Q6---Director --All  off
            //Copy Of Copy Of Copy Of SeleniumTestFormc76fddec-8280-4653-931e-03387c4f39c4

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "Player-Tellu4");
            Thread.Sleep(50000);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            Thread.Sleep(30000);
            IWebElement Questionone = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[2]");
            Questionone.Click();
            Thread.Sleep(10000);
            IWebElement Questiontwo = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[4]");
            Questiontwo.Click();
            Thread.Sleep(10000);
            IWebElement nextbutton = UIActions.FindElementWithXpath(driver, " //*[@id='btnNext']/div");
            nextbutton.Click();
            Thread.Sleep(10000);
            IWebElement sixthquestiondropdown = UIActions.FindElementWithXpath(driver, "//*[@class='btn-group ceb-dropdown-container']");
            sixthquestiondropdown.Click();
            UIActions.IsElementNotVisible(driver, "//ul[@class='multiselect-container dropdown-menu'])[2]//label[text()=' Director']");
            Thread.Sleep(10000);
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(40000);
            driver.Quit();

        }





        [TestMethod]
        public void ValidateQuestionResponseShowLogicPlayerAllOff()

        {

            //Question Response
            //Show--Question Response---------Q6---Director --All  off  ----------if Q1 -any of 1,2 and Q2 Exactly =1
            //Copy of Copy Of Copy Of Copy Of SeleniumTestFormc76fddec-8280-4653-931e-03387c4f39c4


            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "Player-Tellu5");
            Thread.Sleep(50000);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            Thread.Sleep(30000);
            IWebElement Questionone = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[2]");
            Questionone.Click();
            Thread.Sleep(10000);
            IWebElement Questiontwo = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[4]");
            Questiontwo.Click();
            Thread.Sleep(10000);
            IWebElement nextbutton = UIActions.FindElementWithXpath(driver, " //*[@id='btnNext']/div");
            nextbutton.Click();
            Thread.Sleep(10000);
            IWebElement sixthquestiondropdown = UIActions.FindElementWithXpath(driver, "//*[@class='btn-group ceb-dropdown-container']");
            sixthquestiondropdown.Click();
            UIActions.IsElementVisible(driver, "//ul[@class='multiselect-container dropdown-menu'])[2]//label[text()=' Director']");
            Thread.Sleep(10000);
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(40000);
            driver.Quit();

        }




        [TestMethod]
        public void ValidateQuestionResponseShowLogicPlayerAnyOff()

        {

            //Question Response
            //Show--Question Response---------Q6---Director --Any  off  ----------if Q1 -any of 1,2 or Q2 Exactly =1
            //Copy of Copy of Copy Of Copy Of Copy Of SeleniumTestFormc76fddec-8280-4653-931e-03387c4f39c4

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "Player-Tellu6");
            Thread.Sleep(20000);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            Thread.Sleep(10000);
            IWebElement Questiontwo = UIActions.FindElementWithXpath(driver, "(//*[text()='1'])[4]");
            Questiontwo.Click();
            Thread.Sleep(10000);
            IWebElement nextbutton = UIActions.FindElementWithXpath(driver, " //*[@id='btnNext']/div");
            nextbutton.Click();
            Thread.Sleep(10000);
            IWebElement sixthquestiondropdown = UIActions.FindElementWithXpath(driver, "//*[@class='btn-group ceb-dropdown-container']");
            sixthquestiondropdown.Click();
            UIActions.IsElementVisible(driver, "//ul[@class='multiselect-container dropdown-menu'])[2]//label[text()=' Director']");
            Thread.Sleep(10000);
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(40000);
            driver.Quit();

        }


        [TestMethod]

        public void questionlogic()
        //questionlogictestcase
        {
            string playerURL;
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "tellurimkkvp");
            Thread.Sleep(20000);
            // UIActions.clickwithXapth(driver, "//*[@id='btn-copy-url']");
            playerURL = Clipboard.GetText();
            //hide logic for both any of questions not equal to and less than or equal to
            UIActions.OpenNewTab(driver, playerURL);
            Thread.Sleep(20000);
            UIActions.clickwithXapth(driver, "(//div[@data-bind='showResponseText']//*[text()='1'])[1]");
            Thread.Sleep(10000);
            UIActions.clickwithXapth(driver, "//*[@id='btnNext']/div");
            Thread.Sleep(20000);
            UIActions.IsElementVisible(driver, "//*[text()=('3-1. Selenium with Java')]");
            Thread.Sleep(20000);
            UIActions.clickwithXapth(driver, "//*[text()='Previous']");
            Thread.Sleep(20000);
            UIActions.clickwithXapth(driver, "(//div[@data-bind='showResponseText']//*[text()='3'])[1]");
            UIActions.clickwithXapth(driver, "//*[@id='btnNext']/div");
            Thread.Sleep(20000);
            UIActions.IsElementNotVisible(driver, "//*[text()=('3-1. Selenium with Java')]");
            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(20000);

            //show action logic for all of condition

            playerURL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, playerURL);
            UIActions.clickwithXapth(driver, "(//div[@data-bind='showResponseText']//*[text()='2'])[1]");
            UIActions.clickwithXapth(driver, "(//div[@data-bind='showResponseText']//*[text()='1'])[2]");
            Thread.Sleep(20000);
            UIActions.clickwithXapth(driver, "//*[@id='btnNext']/div");
            Thread.Sleep(20000);
            UIActions.IsElementVisible(driver, "//*[text()=('3-3. Selenium with NUnit')]");
            Thread.Sleep(20000);
            UIActions.clickwithXapth(driver, "//*[text()='Previous']");
            Thread.Sleep(20000);

            UIActions.clickwithXapth(driver, "(//div[@data-bind='showResponseText']//*[text()='1'])[1]");
            UIActions.clickwithXapth(driver, "(//div[@data-bind='showResponseText']//*[text()='1'])[2]");
            UIActions.clickwithXapth(driver, "//*[@id='btnNext']/div");
            Thread.Sleep(20000);
            UIActions.IsElementNotVisible(driver, "//*[text()=('3-3. Selenium with NUnit')]");

            UIActions.SwitchToWindowWithTitle("Summary - Pulse", driver);
            Thread.Sleep(10000);


        }

        [TestMethod]
        //To check if player restricts/allows copy/paste through mouse action
        public void VerifyCopyPaste()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/f0155f91c5d0465fb31c3d8ac5f9dc47");
            // RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://www.wikipedia.org/");
            Clipboard.Clear();
            Actions ac = new Actions(driver);
            Thread.Sleep(5000);
            IWebElement ele = driver.FindElement(By.CssSelector("#txtSurveyNameTitle"));
            //IWebElement ele = driver.FindElement(By.CssSelector(".localized-slogan"));
            ac.MoveToElement(ele).Click()
            .SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control)
            .SendKeys(OpenQA.Selenium.Keys.Control + "c" + OpenQA.Selenium.Keys.Control).Build().Perform();

            bool res = Clipboard.ContainsText();
            Console.WriteLine(Clipboard.GetText());
            Assert.IsTrue(res);
        }

        public void VerifyAbilityToCopyPageName()
        {
            Driver.Navigate().GoToUrl("https://stg-surveys.cebglobal.com/player/link/324dd4b7b14b4ed5bbfbb5958b57e739");
            //RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://www.wikipedia.org/");

        }
        public static IWebDriver CopyAll()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/66c7ab7bea524eeb913d48e5ce15ebf6");
            Actions ac = new Actions(driver);
            Clipboard.Clear();
            Thread.Sleep(3000);
            IReadOnlyCollection<IWebElement> _collectionOfQues = UIActions.FindElementsWithXpath(driver, "//span[@data-bind='displayText']");
            string e = @"//div[@data-bind='passageDTO.content.htmlContent']";
            IWebElement ele = driver.FindElement(By.XPath(e));
            ac.MoveToElement(ele).Click(ele)
               .SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control)
               .SendKeys(OpenQA.Selenium.Keys.Control + "c" + OpenQA.Selenium.Keys.Control).Build().Perform();

            return driver;
        }

        [TestMethod]
        public void VerifyStyleDefaultInPlayer()
        {
            var _chromeOptions = new ChromeOptions();
            _chromeOptions.AddArguments("headless");
            var browser = new ChromeDriver(_chromeOptions);
            browser.Navigate().GoToUrl("https://stg-surveys.cebglobal.com/player/link/66c7ab7bea524eeb913d48e5ce15ebf6");
            Actions ac = new Actions(browser);
            Clipboard.Clear();
            Thread.Sleep(3000);

            //string e = @"//div[@data-bind='passageDTO.content.htmlContent']";
            string e = @"//*[@id='main_content']/div/div[3]/div[2]/div/div/div[2]/div[4]/div/div[1]/div/div/div/div[1]/div/div/div/div[1]/div[2]/div[3]/div/div/span[2]/span/em";
            IWebElement ele = browser.FindElement(By.XPath(e));
            var bgstyle = ele.GetCssValue("font-style");
            var colorc = ele.GetCssValue("color");
            Console.WriteLine(bgstyle);
            Console.WriteLine(colorc);
            ac.MoveToElement(ele).Click(ele)
               .SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control)
               .SendKeys(OpenQA.Selenium.Keys.Control + "c" + OpenQA.Selenium.Keys.Control).Build().Perform();

            IWebElement ele1 = browser.FindElement(By.XPath("//*[@id='qgroup_simple_140919']/div[2]/div/div[2]/div/textarea"));
            Thread.Sleep(2000);
            Clipboard.GetText();
            ac.MoveToElement(ele1).Click()
                .SendKeys(OpenQA.Selenium.Keys.Control + "v" + OpenQA.Selenium.Keys.Control)
                .Build().Perform();
            ac.MoveToElement(ele).Click(ele1)
               .SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control)
               .SendKeys(OpenQA.Selenium.Keys.Control + "c" + OpenQA.Selenium.Keys.Control).Build().Perform();
            Thread.Sleep(2000);
            string _remainingTxt = UIActions.GetElementwithXpath(browser, "//div[@class='col-xs-12']/label", 45).Text;
            var ftStyle = ele1.GetCssValue("font-style");
            var bgcolor = ele1.GetCssValue("background-color");
            var color = ele1.GetCssValue("color");
            Console.WriteLine(ftStyle);
            Console.WriteLine(color);
            bool compareStyle = (!bgstyle.Equals(ftStyle) && !colorc.Equals(color)) ? true : false;
            Assert.IsTrue(compareStyle);
        }
        public void ResponseHeaders()
        {

        }
        public void PageNumbers()
        {

        }

        public void SectionName()
        {

        }
        public void Footer()
        {

        }
        public void Instructiontext()
        {

        }
        [TestMethod]
        public void VerifyAbilityToPasteThruKeyBoardAction()
        {

            IWebDriver driver = CopyAll();
            IReadOnlyCollection<IWebElement> _collectionOfQues = UIActions.FindElementsWithXpath(driver, "//input[@data-bind-type='formvalue']");
            Actions ac = new Actions(driver);
            IWebElement ele = driver.FindElement(By.XPath("//*[@id='qgroup_simple_140919']/div[2]/div/div[2]/div/textarea"));
            //ele.SendKeys(Clipboard.GetText());
            Thread.Sleep(2000);
            Clipboard.GetText();
            //ac.MoveToElement(_collectionOfQues.FirstOrDefault(d => d.Text == "To contain pasted text")).Click()
            ac.MoveToElement(ele).Click()
                .SendKeys(OpenQA.Selenium.Keys.Control + "v" + OpenQA.Selenium.Keys.Control)
                .Build().Perform();
            ac.MoveToElement(ele).Click(ele)
               .SendKeys(OpenQA.Selenium.Keys.Control + "a" + OpenQA.Selenium.Keys.Control)
               .SendKeys(OpenQA.Selenium.Keys.Control + "c" + OpenQA.Selenium.Keys.Control).Build().Perform();
            Thread.Sleep(2000);
            string _remainingTxt = UIActions.GetElementwithXpath(driver, "//div[@class='col-xs-12']/label", 45).Text;
            Console.WriteLine(_remainingTxt);
            Assert.IsTrue(_remainingTxt.Contains("0"));
        }

        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver plDriver = GetDriver(DriverAndLogin.Browser);
            if (plDriver != null)
            {
                plDriver.Close();
                plDriver.Dispose();
            }
        }

    }
}
