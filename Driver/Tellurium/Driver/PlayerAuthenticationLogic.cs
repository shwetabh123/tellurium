﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
//using Microsoft.Office.Interop.Excel;

namespace Driver
{
    [TestClass]
    public class PlayerAuthenticationLogic
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;

        public static string distributionName = "Enroute";
        private string clipboardText;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
      
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        }

        [TestMethod]
        public void ValidatePageHideWithAuthLogic()
        {

            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndLaunchDistributionSummary(driver, "AuthenticationLogicUn");//Provide the distribution name

            //Edit participants grid
            DistributeActions.CopyUniqueLink(driver);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            String expectedText = "What is the age group";
            String qText = driver.FindElement(By.XPath("//*[@id='qgroup_simple_87971']/span/p[1]/span/span ")).Text; //*[@id="qgroup_simple_87971"]/span/p[1]/span/span
            bool val = qText.Contains(expectedText) ? true : false;
           
            Assert.IsTrue(val);
            driver.Dispose();
            }

        [TestMethod]
        public void ValidatePageHideWithAuthLogic_DisplayPageAndQues_Openlink()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndCopyLink(driver, "AuthenticationLogicUn");//Provide the distribution name
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            String expectedText = "How do you rate the question";
          
                String qText = driver.FindElement(By.XPath("//*[@id='qgroup_tabular_87970']/td/p/span/span")).Text;
                bool val = qText.Contains(expectedText) ? true : false;
                Assert.IsTrue(val);
                driver.Dispose();
            }

        [TestMethod]
        public void ValidatePageHideWithAuthLogic_Positive_Openlink()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "AuthenticationLogicUn");//Provide the distribution name
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            UIActions.Click(driver, "#btnNext");
            bool val = false;
            try
            {
                if (UIActions.FindElementWithXpath(driver, "//*[@id='qgroup_simple_67923']/span/p[1]/span/span").Displayed || UIActions.FindElementWithXpath(driver, "//*[@id='qgroup_simple_67923']/span/p[1]/span/span").Enabled)
                {
                    val = true;
                    Assert.IsFalse(val);
                }
            }
           catch(NoSuchElementException e)
            {
                Assert.IsFalse(val);
            }
              driver.Dispose();
        }


        [TestMethod]
        public void ValidatePageHideWithAuthLogic_Negative_Openlink()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "AuthenticationLogicUn");//Provide the distribution name
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            UIActions.Click(driver, "#btnNext");
            //String exceptedText = "2. What is the age group";
            bool val = false;
            try
            {
                if(UIActions.FindElementWithXpath(driver, "//*[@id='qgroup_simple_87971']/span/p[1]/span/span").Displayed)
                {
                    val = true;
                    Assert.IsFalse(val);
                }
            }

            catch(NoSuchElementException e)
            {
                Assert.IsFalse(val);
            }
            //String qText = UIActions.FindElementWithXpath(driver, "//*[@id='qgroup_simple_87971']/span/p[1]/span/span").Text;
            //bool val = !exceptedText.Contains(qText) ? false : true;
            //Assert.IsFalse(val);
            driver.Dispose();
        }
        

       [TestMethod]
        public void ValidateAuthResponseHide_Positive_UniqueLink()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
           // DistributeActions.SearchAndOpenDistribution(driver, "AuthenticationLogicUn");//Provide the distribution name
            DistributeActions.SearchAndLaunchDistributionSummary(driver, "AuthenticationLogicUn");//Provide the distribution name

            //Edit participants grid
            DistributeActions.CopyUniqueLink(driver);
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            //UIActions.Click(driver, "#btnNext");
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/button")).Click(); 
            IReadOnlyCollection<IWebElement> dd = driver.FindElements(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/ul/li")); 
            bool val = false;

            for (int i = 1; i <= dd.Count(); i++)
            {
                IWebElement d = driver.FindElement(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/ul/li[" + i + "]/a/label"));
                val = d.Text.Contains("Analyst") ? true : false;
                if (val == true)
                {
                    break;
                }

            }
            Assert.IsFalse(val);
            driver.Dispose();
        }

       [TestMethod]
        public void ValidateAuthResponseHide_Negative_UniqueLink()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndCopyLink(driver, "AuthenticationLogicUn");//Provide the distribution name
            clipboardText = Clipboard.GetText();
            UIActions.OpenNewTab(driver, clipboardText);
            UIActions.Click(driver, "#btnNext");
            Thread.Sleep(10000);
            driver.FindElement(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/button")).Click();
            IReadOnlyCollection<IWebElement> dd = driver.FindElements(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/ul/li"));
            bool val = false;
            Thread.Sleep(10000);
            for (int i = 1; i <= dd.Count(); i++)
            {
                IWebElement d = driver.FindElement(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/ul/li[" + i + "]/a/label"));
                val = d.Text.Contains("Analyst") ? false : true;
                if (val == true)
                {
                    break;
                }

            }
            Assert.IsTrue(val);
            driver.Dispose();
        }
       [TestMethod]
        public void ValidateHideResponse()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, "https://stg-surveys.cebglobal.com/player/link/5220fe33473e48d0809185d9ba4391ba");
            Thread.Sleep(3000);
            UIActions.Click(driver, "#btnNext");
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/button")).Click();
            IReadOnlyCollection<IWebElement> dd = driver.FindElements(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/ul/li"));
            bool val = false;
            
           for (int i=1; i <= dd.Count(); i++)
            {
                IWebElement d = driver.FindElement(By.XPath("//*[@id='qgroup_simple_88081']/div[2]/div/ul/li[" + i + "]/a/label"));
                val = d.Text.Contains("Analyst") ? true : false;
                if(val == true)
                {
                    break;
                }
                
            }
            Assert.IsFalse(val);
        }

     
        public void ClsBrowser()
        {
            NewChromeDriver.Dispose();
        }
     
    }
}
