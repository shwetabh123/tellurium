﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using System.Data;
using System.Windows.Forms;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Text.RegularExpressions;

namespace Driver
{

    [TestClass]
    public class DemographicQuestionMSTest
    {

        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static RemoteWebDriver CurrentDriver;
        private static string FormName;
        public static int fileImportQueueRowCount;
        public static string surveyform = "Automation Demographic ADD/EDIT";
        public static string surveyform1 = "Demographic Addition and deletion";
        public static string surveyID;
        public static string GFESurveyformId;


        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString()

            };

            DBConnectionParameters = new DBConnectionStringDTO
            {
                userName = context.Properties["DBUserName"].ToString(),
                password = context.Properties["DBUserPassword"].ToString(),
                TCESserverName = context.Properties["DBServerName"].ToString(),
                TCESDB = context.Properties["TCESDB"].ToString(),
                TCESReportingserverName = context.Properties["DBServerName_Reporting"].ToString(),
                TCESReportingDB = context.Properties["TCESReportingDB"].ToString()
            };
        }


        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;

            switch (driverName)
            {
                case "Firefox":
                    FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    driver = new FirefoxDriver(service);


                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);


                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);

                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);


                    //driver = new FirefoxDriver(options);



                    break;

                case "IE":
                    driver = new InternetExplorerDriver();

                    break;
                case "Chrome":
                default:

                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    driver = new ChromeDriver(chromeOptions);

                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;

        }


        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }


        public static RemoteWebDriver SetDriverAndNavigateToManageForms(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
            }
            else
            {
                AuthorActions.NavigateToAuthor(driver);
            }
            return driver;
        }

        public static void OpenOrCreateForm(RemoteWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToForm(string SurveyName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, SurveyName);
                AuthorActions.EditPublishedForm(driver);

            }
            else if (driver.Url.Contains("ManageForms"))

            {
                AuthorActions.SearchAndOpenForm(driver, SurveyName);
                AuthorActions.EditPublishedForm(driver);
            }
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);
                
            }
           
            return driver;
        }

        public static RemoteWebDriver SetDriverAndNavigateToParticularForm(string SurveyName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AuthorActions.NavigateToAuthor(driver);
                AuthorActions.SearchAndOpenForm(driver, SurveyName);
                AuthorActions.EditPublishedForm(driver);

            }
            else if (driver.Url.Contains("ManageForms"))

            {
                AuthorActions.SearchandCreateForm(driver, SurveyName);
               
            }
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);

            }

            return driver;
        }


        public static RemoteWebDriver SetDriverAndNavigateToSupport()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                var navbuttons = UIActions.FindElements(driver, "#navbar-menu a span");
                navbuttons.FirstOrDefault(b => b.Text == "Support").Click();

            }
            else if(driver.Url.Contains("Support Users - Pulse"))
            {
                Thread.Sleep(2000);
            }
            return driver;
        }


        public static RemoteWebDriver SetDriverAndNavigateToDistribute(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            else if (driver.Title.Contains("Distribute - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                DistributeActions.NavigateToManageDistrbution(driver);
            }
            return driver;
        }


        /// <summary>
        /// Demo Question Add in 
        /// </summary>

        [TestMethod]

        public void ValidateCEBUserAdd_EditDemographicQuestioninPublishedSurvey()
        {


            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(3000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);

            string QuestionText = "MultiChoiceDemoGraphicQuestion" + Guid.NewGuid();
                string[] responseText = { "One", "two", "Three" };
                AuthorActions.AddMultipleChoiceDemographicQuestion(driver, QuestionText, responseText);
                var messageArea = UIActions.GetElementWithWait(driver, ".status-area", 30);
                Assert.AreEqual("Question added successfully.", messageArea.Text.Trim());
                Thread.Sleep(5000);
                AuthorActions.PublishForm(driver, "Demographic Addition and deletion");
          
       }
        /// <summary>
        /// Demo Response addition in publised survey
        /// </summary>

        [TestMethod]

        public void AllDemoresponseAdditioninPublishedSurvey()
        {
            ValidateCEBUserAdd_DemoResponseOptioninPublishedSurvey();
            ValidateCEBUserAddResponse_Demographic_ScaleOption();
            ValidateNewDemographicScaleoptioninDatatransferQueue();
            ValidateNewDemoResponseOptioninDimItem();
          }

        [TestMethod]

        public void ValidateCEBUserAdd_DemoResponseOptioninPublishedSurvey()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(10000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);
            Thread.Sleep(5000);
            string ResponseText = "UpdateResponseOption" + Guid.NewGuid();
            string OptionPresent = null;
            AuthorActions.EditMCQSingleAddResponse(driver, " 1. MultiChoiceDemoGraphicQuestion", null, ResponseText,5);
            Thread.Sleep(5000);
            IList<IWebElement> UpdatedResponseOptions = driver.FindElements(By.XPath("//*[@class='test-qs-answer rsTextDisplay']"));
            UpdatedResponseOptions.ToList()
                .ForEach((element) =>
                {
                    if (element.Text == ResponseText)
                    {
                        OptionPresent = "Pass";
                    }

                });

            Assert.AreEqual("Pass", OptionPresent);
            AuthorActions.PublishForm(driver, surveyform1);
            Thread.Sleep(20000);
            driver.FindElement(By.XPath("//*[@class='btn btn-secondary btn-lg']")).Click();
            Thread.Sleep(20000);

        }

        [TestMethod]
        public void ValidateCEBUserAddResponse_Demographic_ScaleOption()

        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(5000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            string Query = string.Format("Select SO.ScaleOptionText from surveyform SF INNER JOIN surveyformitem SFI on SFI.surveyformID = SF.surveyformID inner join scaleoption SO on SO.ScaleID = SFI.PrimaryScaleID where SF.GFESurveyFormId = "+ GFESurveyformId + "and SFI.ShortItemText = 'MultiChoiceDemoGraphicQuestion' ");

            DataSet ds = AuthorActions.DBValidationWithQuery(DBConnectionParameters, Query);

            Assert.IsTrue(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }
        [TestMethod]

        public void ValidateNonCEBUserAddResponse_DemographicScaleOption()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToSupport();
            AuthorActions.NavigateToSupportandImpersonate(driver, "t2_user@cebglobal.com", 1);
            DemographicQuestionMSTest.SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(20000);
            AuthorActions.SearchandCreateForm(driver, "Demographic Addition and deletion");
        
            string ResponseText = "UpdateResponseOption" + Guid.NewGuid();
            AuthorActions.EditMCQSingleAddResponse(driver, "1. MultiChoiceDemoGraphicQuestion", null, ResponseText, 1);
            IWebElement ActionStatus = driver.FindElement(By.Id("action-status"));
            string Validation = ActionStatus.Text;
            Thread.Sleep(5000);
            Assert.AreEqual("Category of published items cannot be changed to demographic", Validation);
            driver.Quit();

        }

        [TestMethod]
         public void ValidateNewDemographicScaleoptioninDatatransferQueue()

        {
            string query = @"Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyformID in(

                        select surveyformID from surveyform where GFESurveyFormId=" + GFESurveyformId +
                        ")order by DataTransferQueueId desc";
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, query));
        }

        [TestMethod]

        public void ValidateNewDemoResponseOptioninDimItem()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Demographic Addition and deletion");
            Thread.Sleep(5000);
            string DistributionURL = driver.Url;
            char[] ID = new char[] { '/', '\t' };
            string[] SubValue = DistributionURL.Split(ID);
            for (int i = 0; i < SubValue.Length; i++)
            {
                if ((Regex.Match(SubValue[i], @"(?=[0-9])").Success))
                {
                    surveyID = SubValue[i];
                }
            }
            string query = @"Select ScaleOptiontext from dimitem where surveyID=" + surveyID + " and ItemText='MultiChoiceDemoGraphicQuestion'";
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsTrue(AuthorActions.ValidateDemoScaleoptionInDB(ds));

        }


     

        
        /// <summary>
        /// Demo Response Edit
        /// </summary>
        /// 
        [TestMethod]

        public void AllDemoResponseEdit()
        {
            ValidateDemoResponseEdit_01_NO();
            ValidateDemoResponseEdit_02_Yes();
            ValidateDemoResponseEdit_03_ScaleOptionTable();
            ValidateDemoResponseEdit_04_Logic();
            ValidateDemoResponseEdit_05_SurveywaveParticipantDemo();
            ValidateDemoResponseEdit_07_DataTransferForItem();
            ValidateDemoResponseEdit_08_DimItem();
            ValidateDemoResponseEdit_09_DimParticipant();
        }

        [TestMethod]
        public void ValidateDemoResponseEdit_01_NO()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(8000);
            AuthorActions.SearchandCreateForm(driver,surveyform1);
            //AuthorActions.EditPublishedForm(driver);
            Thread.Sleep(5000);
            string ResponseText = "UpdateResponseOption" + Guid.NewGuid();
            AuthorActions.EditMCQSingleEditResponse(driver, "1. MultiChoiceDemoGraphicQuestion", null, ResponseText, 1);
            Thread.Sleep(3000);
            AuthorActions.DemographicEditConfirmationPoPup(driver, "No");
            Thread.Sleep(3000);
            var UpdatedResponseOption = driver.FindElement(By.XPath("(//*[@class='test-qs-answer rsTextDisplay'])[2]"));
            Assert.AreNotEqual(AuthorActions.DemoResponseText, UpdatedResponseOption.Text);
        }

        [TestMethod]
        public void ValidateDemoResponseEdit_02_Yes()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm("Demographic Addition and deletion");
            Thread.Sleep(10000);
            string ResponseText = "UpdateResponseOption" + Guid.NewGuid();
            AuthorActions.EditMCQSingleEditResponse(driver, "1. MultiChoiceDemoGraphicQuestion", null, ResponseText, 1);
            Thread.Sleep(2000);
            AuthorActions.DemographicEditConfirmationPoPup(driver, "Yes");
            Thread.Sleep(8000);
            var UpdatedResponseOption = driver.FindElement(By.XPath("(//*[@class='test-qs-answer rsTextDisplay'])[2]"));
            Assert.AreEqual(AuthorActions.DemoResponseText, UpdatedResponseOption.Text);
            Thread.Sleep(5000);
            AuthorActions.PublishForm(driver, "Demographic Addition and deletion");
            Thread.Sleep(20000);
            driver.FindElement(By.XPath("//*[@class='btn btn-secondary btn-lg']")).Click();
            Thread.Sleep(20000);
         
        }



        [TestMethod]
         public void ValidateDemoResponseEdit_03_ScaleOptionTable()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(5000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            string Query =   string.Format("Select SO.ScaleOptionText from surveyform SF INNER JOIN surveyformitem SFI on SFI.surveyformID = SF.surveyformID inner join scaleoption SO on SO.ScaleID = SFI.PrimaryScaleID where SF.GFESurveyFormId = "+GFESurveyformId+" and SFI.ShortItemText = 'MultiChoiceDemoGraphicQuestion' ");

            DataSet ds= AuthorActions.DBValidationWithQuery(DBConnectionParameters, Query);

            Assert.IsTrue(AuthorActions.ValidateDemoScaleoptionInDB(ds));

          
        }

        [TestMethod]

        public void ValidateDemoResponseEdit_04_Logic()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToForm("Demographic Addition and deletion");
            AuthorActions.EditPublishedForm(driver);
            Thread.Sleep(5000);
            AuthorActions.DragAndDropLogic(driver, 1);
            Thread.Sleep(2000);
            UIActions.SelectInCEBDropdownByValue(driver, "#select-sourceQuestion", " Q1.MultiChoiceDemoGraphicQuestion");
            Thread.Sleep(2000);
            String Updatedvalue = null;
            IList<IWebElement> Opions = UIActions.GetallValuesFromCEBDropdown(driver, "#select-Response", 0);
            Opions.ToList()
               .ForEach((element) =>
                 {
                     if (element.Text == AuthorActions.DemoResponseText)
                     {
                         Updatedvalue = element.Text;


                      }
                                      
                   });
            Assert.AreEqual(Updatedvalue, AuthorActions.DemoResponseText);
            driver.FindElement(By.Id("btnLogicBuilderClose")).Click();
        }



        [TestMethod]

        public void ValidateDemoResponseEdit_05_SurveywaveParticipantDemo()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Demographic Addition and deletion");
            Thread.Sleep(5000);
            string DistributionURL = driver.Url;
            char[] ID = new char[] { '/', '\t' };
            string[] SubValue = DistributionURL.Split(ID);
            for (int i = 0; i < SubValue.Length; i++)
            {
                if ((Regex.Match(SubValue[i], @"(?=[0-9])").Success))
                {
                    surveyID = SubValue[i];
                }
            }
            UIActions.Click(driver, "#copy-url");
            string URL = Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            Thread.Sleep(5000);
            IWebElement ResposneOption = driver.FindElement(By.XPath("(//*[@class='test-qs-answer rsTextDisplay'])[2]"));
            Actions action = new Actions(driver);
            action.MoveToElement(ResposneOption).Perform();
            ResposneOption.Click();
            Thread.Sleep(2000);
            IWebElement NextButton=driver.FindElement(By.Id("btnNext"));
            Actions action1 = new Actions(driver);
            action1.MoveToElement(NextButton).Perform();
            NextButton.Click();
            Thread.Sleep(2000);
            string Query = string.Format("Select demo170 from  Surveywaveparticipantdemo where surveyID=" + surveyID + "");
            DataSet ds = AuthorActions.DBValidationWithQuery(DBConnectionParameters, Query);
            Assert.IsTrue(AuthorActions.ValidateDemoScaleoptionInDB(ds));
            driver.Quit();
        }


        /*[TestMethod]

        public void ValidateDemoResponseEdit_06_ParticipantQueue()
        {
            string Query_Before = string.Format("select * from ParticipantQueue where surveyID = 30802");
            DataSet ds_Before= AuthorActions.DBValidationWithQuery(DBConnectionParameters, Query_Before);
            int Row_count_Before = AuthorActions.ValidateQueryEntriesCount(ds_Before);
            
            RemoteWebDriver driver = SetDriverAndNavigateToForm("Demographic Addition and deletion");
            
        
            string ResponseText = "UpdateResponseOption" + Guid.NewGuid();
            Thread.Sleep(5000);
            AuthorActions.EditMCQSingleEditResponse(driver, "1. MultiChoiceDemoGraphicQuestion", null, ResponseText, 1);
            AuthorActions.DemographicEditConfirmationPoPup(driver, "Yes");
            Thread.Sleep(5000);
            var UpdatedResponseOption = driver.FindElement(By.XPath("(//*[@class='test-qs-answer rsTextDisplay'])[2]"));
            Assert.AreEqual(AuthorActions.DemoResponseText, UpdatedResponseOption.Text);
            Thread.Sleep(3000);
            AuthorActions.PublishForm(driver, "Demographic Addition and deletion");
            Thread.Sleep(30000);
            string Query_After = string.Format("select * from ParticipantQueue where surveyID = 30802");
            DataSet ds_After = AuthorActions.DBValidationWithQuery(DBConnectionParameters, Query_After);
            int Row_count_after = AuthorActions.ValidateQueryEntriesCount(ds_After);
            Assert.AreNotEqual(Row_count_Before, Row_count_after);

        }*/

        [TestMethod]

        public void ValidateDemoResponseEdit_07_DataTransferForItem()

        {
           string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID="+surveyID+" order by DataTransferQueueID desc");
     
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
        }

        [TestMethod]
        public void ValidateDemoResponseEdit_08_DimItem()
        {
            string query = @"Select ScaleOptiontext from dimitem where surveyID=" + surveyID + " and ItemText='MultiChoiceDemoGraphicQuestion'";
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsTrue(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }

        [TestMethod]
        public void ValidateDemoResponseEdit_09_DimParticipant()
        {
            string Query = string.Format("Select demo170 from DimParticipant where surveyID="+surveyID+"");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, Query);
            Assert.IsTrue(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }

        [TestMethod]

        public void ValidateNonCEBUserEditResponse_DemographicScaleOption()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToSupport();
            AuthorActions.NavigateToSupportandImpersonate(driver, "t2_user@cebglobal.com", 1);
            DemographicQuestionMSTest.SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(20000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);
            string ResponseText = "UpdateResponseOption" + Guid.NewGuid();
            AuthorActions.EditMCQSingleEditResponse(driver, "1. MultiChoiceDemoGraphicQuestion", null, ResponseText, 1);
            IWebElement ActionStatus = driver.FindElement(By.Id("action-status"));
            string Validation = ActionStatus.Text;
            Thread.Sleep(5000);
            Assert.AreEqual("Category of published items cannot be changed to demographic", Validation);
            driver.Quit();

        }

        /// <summary>
        /// Demo Question Edition Scenarios
        /// </summary>
        /// 
        [TestMethod]

        public void AllDemoQuestionEditTest()
        {
            A_ValidateEditedDemoQuestion_01();
            B_ValidateEditedDemoQuestion_Yes_02();
            C_ValidateEditedDemoQuestion_surveydemographicColumnlabel_03();
            D_ValidateEditedDemoQuestion_SurveyformItem_04();
            E_ValidateEditedDemoQuestion_DataTransfer_05();
            F_ValidateEditedDemoQuestion_DimItem_06();
            G_ValidateEditedDemoQuestion_DimSurveyDemographicColumnLabel_07();

        }

        [TestMethod]

        public void A_ValidateEditedDemoQuestion_01()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(3000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
            string QuestionText = "MCQDemoQuestionEdit" + Guid.NewGuid();
            string[] responseText = { "One", "two", "Three" };
            AuthorActions.AddMultipleChoiceDemographicQuestion(driver, QuestionText, responseText);
            Thread.Sleep(8000);
            AuthorActions.PublishForm(driver, surveyform);
            Thread.Sleep(5000);
            driver.Navigate().Refresh();
            Thread.Sleep(5000);
            AuthorActions.SearchandCreateForm(driver, surveyform);
            string UpdateQuestionText = "MCQDemoQuestionEdit" + Guid.NewGuid();
            AuthorActions.EditMCQSingleEditResponse(driver, QuestionText, UpdateQuestionText, null, 1);
            Thread.Sleep(2000);
            AuthorActions.DemographicEditConfirmationPoPup(driver, "No");
            Thread.Sleep(5000);
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            string SuppressedText = driver.FindElement(By.XPath("//*[@class='inline-block']")).Text;
            char[] whitespace = new char[] { ' ', '\t' };
            string[] ValidateText = SuppressedText.Split(whitespace);

            string isdisplayed = "true";
            foreach (var text in ValidateText)
            {
                if (text == UpdateQuestionText)

                {
                    Assert.Fail();
                }

            }
            Assert.AreEqual("true", isdisplayed);


        }

        [TestMethod]
        public void B_ValidateEditedDemoQuestion_Yes_02()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToParticularForm(surveyform);
            
            string QuestionText = "MCQDemoQuestionEdit" + Guid.NewGuid();
            AuthorActions.EditMCQSingleEditResponse(driver, "MCQDemoQuestionEdit", QuestionText, null, 1);
            Thread.Sleep(10000);
            AuthorActions.DemographicEditConfirmationPoPup(driver, "Yes");
            Thread.Sleep(10000);
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            string SuppressedText = driver.FindElement(By.XPath("//*[@class='inline-block']")).Text;
            char[] whitespace = new char[] { ' ', '\t' };
            string[] ValidateText = SuppressedText.Split(whitespace);
            string isdisplayed = null;
            foreach (var text in ValidateText)
            {
                if(text== QuestionText)

                {
                    isdisplayed = "true";
                }
            }
            Assert.AreEqual("true", isdisplayed);
            Thread.Sleep(3000);
            AuthorActions.PublishForm(driver, surveyform);
            Thread.Sleep(20000);
            driver.FindElement(By.XPath("//*[@class='btn btn-secondary btn-lg']")).Click();
            Thread.Sleep(20000);
        }

        //ValidateEditedDemoQuestioninSurveyDemographicColumnLabel
        [TestMethod]

        public void C_ValidateEditedDemoQuestion_surveydemographicColumnlabel_03()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToParticularForm(surveyform);
            
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            string Query = string.Format("select label from SurveyDemographicColumnLabel where GFESurveyFormID =" + GFESurveyformId + "");
            DataSet ds = AuthorActions.DBValidationWithQuery(DBConnectionParameters, Query);
            Assert.IsTrue(AuthorActions.ValidateDemoQuestionInDB(ds));
        }
    
        // Surveyformitem -demoquestionEdition
       
        [TestMethod]

        public void D_ValidateEditedDemoQuestion_SurveyformItem_04()
        {
            string Query = @"Select ShortItemText from surveyformitem SFI
                            inner join  surveyform SF
                            on SFI.SurveyFormID= SF.SurveyFormId
                            where SF.GFESurveyFormId="+ GFESurveyformId +
                            "and SFI.Status=1";
            DataSet ds = AuthorActions.DBValidationWithQuery(DBConnectionParameters, Query);
            Assert.IsTrue(AuthorActions.ValidateDemoQuestionInDB(ds));

        }
  

        //Datatransfer
        [TestMethod]

        public void E_ValidateEditedDemoQuestion_DataTransfer_05()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "DemoAdd/EDIT");
            Thread.Sleep(5000);
            string DistributionURL = driver.Url;
            char[] ID = new char[] { '/', '\t' };
            string[] SubValue = DistributionURL.Split(ID);
            for (int i = 0; i < SubValue.Length; i++)
            {
                if ((Regex.Match(SubValue[i], @"(?=[0-9])").Success))
                {
                    surveyID = SubValue[i];
                }
            }

            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");
            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
        }


        //Dimitem

        [TestMethod]

        public void F_ValidateEditedDemoQuestion_DimItem_06()
        {
            string query = string.Format("Select itemText from dimitem where surveyID = " + surveyID + " and  CategoryText = 'Demographic'group by CategoryID, itemText");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsTrue(AuthorActions.ValidateDemoQuestionInDB(ds));
        }
        //DimsurveydemographicColumnlabel
        [TestMethod]

        public void G_ValidateEditedDemoQuestion_DimSurveyDemographicColumnLabel_07()
        {
            string query = string.Format("select label from dimSurveyDemographicColumnLabel where GFESurveyFormID = "+ GFESurveyformId + " and Status = 1");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsTrue(AuthorActions.ValidateDemoQuestionInDB(ds));
        }

      
        /// <summary>
        /// Demo Response Option Removal
        /// </summary>
        /// 

        [TestMethod]

        public void AllDemoResponseRemoval()
        {
            ValidateDemoResponseEdit_02_Yes();
            ValidateRemoveDemoResponse_No();
            ValidateRemoveDemoResponse_Yes();
            ValidateRemovedDemoResponse_ScaleOption();
            ValidateRemovedDemoResponse_Draft_ScaleOption();
            ValidateRemovedDemoResponse_SurveywaveparticipantDemo();
            ValidateRemovedDemoResponse_DatatransferQueueforParticipant();
            ValidateRemovedDemoResponse_Dimitem();
            ValidateRemovedDemoResponse_DimParticipant();
        }


        [TestMethod]
        public void ValidateRemoveDemoResponse_No()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(8000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);
            //AuthorActions.EditPublishedForm(driver);
            Thread.Sleep(5000);
            AuthorActions.DeleteResponseOption(driver, "1. MultiChoiceDemoGraphicQuestion", AuthorActions.DemoResponseText);
            Thread.Sleep(5000);
            AuthorActions.DemographicEditConfirmationPoPup(driver, "No");
            Thread.Sleep(1000);
            driver.Navigate().Refresh();
            Thread.Sleep(15000);
            AuthorActions.GotoQuestionTileAndExpand(driver, "1. MultiChoiceDemoGraphicQuestion");
            Thread.Sleep(8000);
            string ResponseAvailable= "false";
            IList<IWebElement> RemovedResponse = driver.FindElements(By.XPath("//*[@class='test-qs-answer rsTextDisplay']"));
            
            RemovedResponse.ToList().
                ForEach((element)=>
                {
                    if(element.Text == AuthorActions.DemoResponseText)
                    {
                        ResponseAvailable = "true";
                    }
                  
                });
            bool isdisplayed = ResponseAvailable.Equals("true") ? true : false;
            Assert.IsTrue(isdisplayed);
            AuthorActions.PublishForm(driver, surveyform1);
            Thread.Sleep(20000);
            driver.FindElement(By.XPath("//*[@class='btn btn-secondary btn-lg']")).Click();
            Thread.Sleep(20000);


        }

        [TestMethod]
        public void ValidateRemoveDemoResponse_Yes()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(8000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);
            //AuthorActions.EditPublishedForm(driver);
            Thread.Sleep(5000);
            AuthorActions.DeleteResponseOption(driver, "1. MultiChoiceDemoGraphicQuestion", AuthorActions.DemoResponseText);
            Thread.Sleep(5000);
            AuthorActions.DemographicEditConfirmationPoPup(driver, "Yes");
            Thread.Sleep(10000);

            string ResponseAvailable = "false";
            IList<IWebElement> RemovedResponse = driver.FindElements(By.XPath("//*[@class='test-qs-answer rsTextDisplay']"));

            RemovedResponse.ToList().
                ForEach((element) =>
                {
                    if (element.Text == AuthorActions.DemoResponseText)
                    {
                        ResponseAvailable = "true";
                    }

                });
            bool isdisplayed = ResponseAvailable.Equals("false") ? true : false;
            Assert.IsTrue(isdisplayed);
            AuthorActions.PublishForm(driver, surveyform1);
            Thread.Sleep(20000);
            driver.FindElement(By.XPath("//*[@class='btn btn-secondary btn-lg']")).Click();
            Thread.Sleep(20000);


        }

        [TestMethod]

        public void ValidateRemovedDemoResponse_ScaleOption()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToManageForms(DriverAndLogin.Browser);
            Thread.Sleep(5000);
            AuthorActions.SearchandCreateForm(driver, surveyform1);
            string SurveyFormURL = driver.Url;
            char[] ID = new char[] { '#', '\t' };
            GFESurveyformId = SurveyFormURL.Split(ID).Last();
            string query = @"
                           Select SO.ScaleOptionText from surveyform SF 
                           INNER JOIN surveyformitem SFI on SFI.surveyformID = SF.surveyformID 
                           inner join scaleoption SO on SO.ScaleID = SFI.PrimaryScaleID 
                           where SF.GFESurveyFormId = " + GFESurveyformId + " and SFI.ShortItemText = 'MultiChoiceDemoGraphicQuestion' and SO.status=0";
            DataSet ds = AuthorActions.DBValidationWithQuery(DBConnectionParameters, query);

            Assert.IsTrue(AuthorActions.ValidateDemoScaleoptionInDB(ds));

        }

        [TestMethod]

        public void  ValidateRemovedDemoResponse_Draft_ScaleOption()
        {
            string query = @"
                          Select SO.ScaleOptionText from draft.surveyformdraft SF 
                          INNER JOIN draft.surveyformitemdraft SFI on SFI.surveyformID = SF.surveyformID 
                          inner join draft.scaleoptiondraft SO on SO.ScaleID = SFI.PrimaryScaleID 
                          where SF.GFESurveyFormId = 199956 and SFI.ShortItemText = 'MultiChoiceDemoGraphicQuestion'";
            DataSet ds = AuthorActions.DBValidationWithQuery(DBConnectionParameters, query);
            Assert.IsFalse(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }
        [TestMethod]

        public void  ValidateRemovedDemoResponse_SurveywaveparticipantDemo()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToDistribute(DriverAndLogin.Browser);
            DistributeActions.SearchAndOpenDistribution(driver, "Demographic Addition and deletion");
            Thread.Sleep(5000);
            string DistributionURL = driver.Url;
            char[] ID = new char[] { '/', '\t' };
            string[] SubValue = DistributionURL.Split(ID);
            for (int i = 0; i < SubValue.Length; i++)
            {
                if ((Regex.Match(SubValue[i], @"(?=[0-9])").Success))
                {
                    surveyID = SubValue[i];
                }
            }
            string query = string.Format("Select demo170 from  Surveywaveparticipantdemo where surveyID=" + surveyID + "");
            DataSet ds = AuthorActions.DBValidationWithQuery(DBConnectionParameters, query);
            Assert.IsFalse(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }

        [TestMethod]


        public void ValidateRemovedDemoResponse_DatatransferQueueforParticipant()
        {
            string Query = string.Format("Select top 1 DataTransferStatus from DataTransferQueueForItem where surveyID=" + surveyID + " order by DataTransferQueueID desc");

            Assert.IsTrue(AuthorActions.ValidateDataTransferItem_Queue(DBConnectionParameters, Query));
        }


        [TestMethod]


        public void ValidateRemovedDemoResponse_Dimitem()
        {
            string query = @"Select ScaleOptiontext from dimitem where surveyID = " + surveyID + " and ItemText = 'MultiChoiceDemoGraphicQuestion'";
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            Assert.IsFalse(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }

        [TestMethod]


        public void ValidateRemovedDemoResponse_DimParticipant()
        {

            string Query = string.Format("Select demo170 from DimParticipant where surveyID=" + surveyID + "");
            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, Query);
            Assert.IsFalse(AuthorActions.ValidateDemoScaleoptionInDB(ds));
        }


    }


}
