﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace Driver
{
    [TestClass]
    public class PlayerLogicTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        private static string FormName;
        // public static string FileName;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        public static void OpenOrCreateForm(RemoteWebDriver driver)
        {
            if (!string.IsNullOrEmpty(FormName))
            {
                AuthorActions.NavigateToAuthor(driver);
                var formBuilder = AuthorActions.SearchAndOpenForm(driver, FormName);
            }
            else
            {
                FormName = "SeleniumTestForm" + Guid.NewGuid();
                Thread.Sleep(10000);
                var formBuilder = AuthorActions.CreateForm(driver, FormName);
            }
        }

        public static RemoteWebDriver SetDriverAndNavigateToForm(string driverName)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                OpenOrCreateForm(driver);
            }
            else if (driver.Url.Contains("ManageForms"))
                OpenOrCreateForm(driver);
            else if (driver.Url.Contains("ManageForm"))
            {
                Thread.Sleep(2000);
            }
            else
            {
                OpenOrCreateForm(driver);
            }
            return driver;
        }



        [TestMethod]
        public void ValidateHidePageLogicAnyof()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "SeleniumTestForm6ecdfde7-d2b1-4252-9240-bc9e5d4c582f");
            Thread.Sleep(15000);
                DistributeActions.AddParticipant(driver);
                Thread.Sleep(7000);
             IWebElement participant = driver.FindElementByXPath("(//*[@class='email']/../..//td[1])[1]");
                participant.Click();
                driver.FindElementById("btn-copy-participant-url").Click();
                String URL = System.Windows.Forms.Clipboard.GetText();
                UIActions.OpenNewTab(driver, URL);
                IWebElement critieria1 = driver.FindElementByXPath("(//*[text()='2'])[1]");
                critieria1.Click();
        
            driver.FindElementById("btnNext").Click();
            Thread.Sleep(5000);
            if (!(UIActions.IsElementNotVisible(driver, "//*[text()='2']")))
                driver.FindElementByXPath("//*[contains(text(),'Previous')]").Click();
            Thread.Sleep(5000);
            IWebElement criteria2 = driver.FindElementByXPath("(//*[text()='3'])[2]");
                criteria2.Click();
                driver.FindElementById("btnNext").Click();
                Thread.Sleep(5000);
                if (!(UIActions.IsElementNotVisible(driver, "//*[text()='2']")))
                    UIActions.SwitchToWindowWithTitle("Distribute Survey - Pulse", driver);
                driver.Quit();
            
          
        }

        [TestMethod]
        public void ValidateShowPageLogicAllof()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, "SeleniumTestForm6ecdfde7-d2b1-4252-9240-bc9e5d4c582f");
            DistributeActions.AddParticipant(driver);
            Thread.Sleep(10000);
            IWebElement participant = driver.FindElementByXPath("(//*[@class='email']/../..//td[1])[1]");
            participant.Click();
            driver.FindElementById("btn-copy-participant-url").Click();
            String URL = System.Windows.Forms.Clipboard.GetText();
            UIActions.OpenNewTab(driver, URL);
            IWebElement critieria1 = driver.FindElementByXPath("(//*[text()='6'])[3]");
            critieria1.Click();
            driver.FindElementById("btnNext").Click();
            Thread.Sleep(5000);
            driver.FindElementById("btnNext").Click();
            Thread.Sleep(5000);
            if (!(UIActions.IsElementNotVisible(driver, "//*[text()='3']")))
                driver.FindElementByXPath("//*[contains(text(),'Previous')]").Click();
            Thread.Sleep(5000);
            String text = "Director";
            UIActions.SelectInCEBDropdownByText(driver, ".dropdown-responses", text);
            driver.FindElementById("btnNext").Click();
            if (!(UIActions.IsElementNotVisible(driver, "//*[text()='3']")))
                UIActions.SwitchToWindowWithTitle("Distribute Survey - Pulse", driver);
            driver.Quit();
            //sample
        }

        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver plDriver = GetDriver(DriverAndLogin.Browser);
            if (plDriver != null)
            {
                plDriver.Close();
                plDriver.Dispose();
            }
        }
    }
}