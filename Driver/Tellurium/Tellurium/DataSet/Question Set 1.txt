1. Please select your age group
2. What is your race?
3. What is your gender?
4. Please select the choice that most closely describes your level in the organization you're departing (or have already left).
5. I would like to be working at Alpha one year from now.
6. I am proud to work for alpha
7. How would you rate your overall financial health.
8. There is a high degree of alignment across different teams at Alpha towards achieving common goals.
9. What was your motivation for choosing or accepting this position?
10. The length of time between submission of my application and when I received a job offer was reasonable
11. What should be your manager's top priority for improving recruitment, hiring, and orientation process?
12. The job/vacancy announcement was clear and understandable
13. Before I applied for this job, I was familiar with this company.
14. Please share your views on how Alpha can improve its employee engagement?
15. Before I applied for this job, I was familiar with Alpha's values.
16. Before I applied for this job, I was familiar with Alpha's line of work.
17. Someone from my work unit contacted me in advance of my first day and made me feel welcome.
18. I had a helpful, knowledgeable point of contact for my questions before I reported to work.
19. How well are you paid for the work you do?
20. How competitive is your salary compared to jobs you might find elsewhere?
21. How satisfied are you with your salary?
22. How well do you understand your benefits package?
23. How competitive is your benefits package compared to those offered by other companies?
24. How satisfied are you with your benefits package?
25. Which mode of transport do you use to convey to your workplace?
26. Please enter your email
27. I am satisfied with the overall orientation that I have received.
28. Please share any additional feedback or recommendations you may have to improve the company's orientation processes.
29. How likely is it that you would recommend [Organization Name] to a friend or colleague as a great place to work? (0 being not at all likely and 10 being extremely likely)
30. If you have any suggestions on improving your experience in commuting to office, or if you have any feedback on the existing mode of transport, please feel to express them in the space below.
31. How did you hear about this vacancy?
32. My team (people I work with) has a climate in which diverse perspectives are valued.
33. Please indicate how long you have been working for Alpha:
34. Human resources contact was