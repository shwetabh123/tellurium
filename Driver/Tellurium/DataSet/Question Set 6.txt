1. Please select your age group
2. What is your race?
3. What is your gender?
4. Please select the choice that most closely describes your level in the organization you're departing (or have already left).
5. I would like to be working at Alpha one year from now.
6. I am proud to work for alpha
7. How would you rate your overall financial health.
8. There is a high degree of alignment across different teams at Alpha towards achieving common goals.
9. What was your motivation for choosing or accepting this position?
10. What should be your manager's top priority for improving recruitment, hiring, and orientation process?
11. Please share your views on how Alpha can improve its employee engagement?
12. How well do you understand your benefits package?
13. How competitive is your benefits package compared to those offered by other companies?
14. How satisfied are you with your benefits package?
15. Which mode of transport do you use to convey to your workplace?
16. Please enter your email
17. I am satisfied with the overall orientation that I have received.
18. Please share any additional feedback or recommendations you may have to improve the company's orientation processes.
19. How likely is it that you would recommend [Organization Name] to a friend or colleague as a great place to work? (0 being not at all likely and 10 being extremely likely)
20. If you have any suggestions on improving your experience in commuting to office, or if you have any feedback on the existing mode of transport, please feel to express them in the space below.
21. How did you hear about this vacancy?
22. My team (people I work with) has a climate in which diverse perspectives are valued.
23. Please indicate how long you have been working for Alpha:
24. Human resources contact was