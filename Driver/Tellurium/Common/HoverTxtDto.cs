﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class HoverTxtDto
    {
        public string formName { get; set; }
        public string questionType { get; set; }
        public string questionTxt { get; set; }
        public string hoverTxt { get; set; }
        public string alterHoverTxt { get; set; }
        public string saveBtn { get; set; }
        public string result1 { get; set; }
        public string result2 { get; set; }
        public bool result3 { get; set; }
        public string QuestionTextEditorElement { get; set; }
        public string HoverElement { get; set; }
        public string HoverTextEditorElement { get; set; }
        public string okBtn { get; set; }

    }
}
