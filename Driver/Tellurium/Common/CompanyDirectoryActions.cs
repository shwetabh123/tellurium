﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;

namespace Common
{
    public class CompanyDirectoryActions
    {
        public static void NavigateToCompanyDirectory(IWebDriver driver)
        {
            UIActions.GetElement(driver, "span.user-info").Click(); 
            UIActions.GetElement(driver, "a[href='/Pulse/AccountSettings']").Click(); 
            Thread.Sleep(5000);
            UIActions.GetElement(driver, "[href='#directory-section']").Click();
            UIActions.GetElement(driver, "#menu-directory-participants").Click();
        }

        public static void UploadCompanyDirectoryParticipants(IWebDriver driver, string FileName, string DateFormat)
        {
            try
            {
                UIActions.Click(driver, "[widget-data-event='click::uploadPartcipants']");
                Thread.Sleep(5000);
                var element = UIActions.GetElement(driver, "#dateFormat");
                SelectElement dropdown = new SelectElement(element);
                dropdown.SelectByText(DateFormat);
                IWebElement SelectFileButton = UIActions.GetElement(driver, "#select-participant-upload-file");
                SelectFileButton.SendKeys(@"C:\Tellurium\" + FileName);
                UIActions.Click(driver, "#uploadImportTemplate");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
                    }
        public static void NavigatetoSettings(IWebDriver driver)
        {
            UIActions.clickwithXapth(driver, "//i[@class='shl-icon_arrow_small_down']");
            UIActions.clickwithXapth(driver, "//a[@href='/Pulse/AccountSettings']");
        }

        public static void OpenDemographicstab(IWebDriver driver)
        {
            UIActions.clickwithXapth(driver, "(//div[@id='directory-accordion']//a[@href='#directory-section'])");
            Thread.Sleep(2000);
            UIActions.clickwithXapth(driver, "(//*[@id='menu-directory-demographics'])");
        }


        public static IWebElement AddDemographic(IWebDriver driver, string DemoName, string datatype)
        {
            UIActions.clickwithXapth(driver, "//*[@id='add-demo']");
            var demopage = UIActions.GetElementWithWait(driver, "#demographics-modal-title", 45);
            IWebElement DemoNameTextBox = driver.FindElement(By.Id("txt-demo-name"));
            DemoNameTextBox.Clear();
            DemoNameTextBox.SendKeys(DemoName);
            UIActions.SelectInCEBDropdownByText(driver, "#select-data-type", datatype);
            Thread.Sleep(1000);
            UIActions.clickwithXapth(driver, "//*[@id='btn-demographics-save']");
            return demopage;
        }

        public static IWebElement EditDemographic(IWebDriver driver, string Oldname, string DemoName = null, string datatype = null)
        {
            var dempgraphicpage = UIActions.GetElementWithWait(driver, "#directory-demographics-title", 45);
            IWebElement SearchBox = UIActions.GetElementswithXpath(driver, "//*[@id='directory-demographics-table_filter']/label/input", 45);
            SearchBox.Clear();
            SearchBox.SendKeys(Oldname);
            Thread.Sleep(7000);
            UIActions.clickwithXapth(driver, "//*[@id='[object Object]']/td[4]/div/button/i");
            Thread.Sleep(3000);
            UIActions.clickwithXapth(driver, "//a[@widget-data-event='click::EditDemographics']");
            Thread.Sleep(3000);
            var demopage = UIActions.GetElementWithWait(driver, "#demographics-modal-title", 45);
            Thread.Sleep(3000);
            IWebElement textBox = UIActions.FindElementWithXpath(driver, "//*[@id='txt-demo-name']");

            if (DemoName != null)
            {
                textBox.Clear();
                textBox.SendKeys(DemoName);
                Thread.Sleep(3000);
            }

            if
                (datatype != null)
            {
                UIActions.SelectInCEBDropdownByText(driver, "#select-data-type", datatype);
            }
            UIActions.clickwithXapth(driver, "//*[@id='btn-demographics-save']");

            return dempgraphicpage;
        }
        
        public static IWebElement DeleteDemographic(IWebDriver driver, string Demoname)
        {

            var dempgraphicpage = UIActions.GetElementWithWait(driver, "#directory-demographics-title", 45);
            IWebElement SearchBox = UIActions.GetElementswithXpath(driver, "//*[@id='directory-demographics-table_filter']/label/input", 45);
            SearchBox.Clear();
            SearchBox.SendKeys(Demoname);
            Thread.Sleep(7000);
            UIActions.clickwithXapth(driver, "//*[@id='[object Object]']/td[4]/div/button/i");
            Thread.Sleep(3000);
            UIActions.clickwithXapth(driver, "//a[@widget-data-event='click::DeleteDemographics']");
            Thread.Sleep(4000);
            UIActions.clickwithXapth(driver, "//*[@id='btnConfirmYes']");

            return dempgraphicpage;
        }
    }
    
}
