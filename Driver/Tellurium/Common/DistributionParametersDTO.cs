﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DistributionParametersDTO
    {
        public string FormName { get; set; }
        public string DistributionName { get; set; }
        public string StartDate { get; set; }
        public string StartTimeZone { get; set; }
        public string EndDate { get; set; }
        public string EndTimeZone { get; set; }
        public string[] Method { get; set; } 

    }
}
