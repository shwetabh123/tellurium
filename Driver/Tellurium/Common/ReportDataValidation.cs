﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace Common
{
    public class ReportDataValidation
    {
        public static DataSet ReportFilters { get; set; }

        public static DataSet GetReportFilter(string UserAnalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {
            string InputQuery = "EXEC dbo.QAPROC_GetFilterResults " + UserAnalyzeFilterID;
            DataSet ds = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ds;
        }
        public static string GetTestSPName(string ReportName, string ItemOrCategory)
        {
            string TestSPName = "";
            if (ReportName.ToLower() == "results")
            {
                TestSPName = "dbo.Parent_SP_QAPROC_ResultsReport_V11";
            }
            else if (ReportName.ToLower() == "insights" || ReportName.ToLower() == "drilldown")
            {
                if (ItemOrCategory == "2")
                    TestSPName = "dbo.QAPROC_ReportCalculationQuestions_V11";
                else
                    TestSPName = "dbo.Parent_SP_QAPROC_ReportCalculationCategory_V11";
            }
            else if (ReportName.ToLower() == "demographic")
            {
                if (ItemOrCategory == "2")
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcCategory_V11";
                else
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcQuestions_V11";
            }
            else if (ReportName.ToLower() == "comment")
            {
                if (ItemOrCategory == "2")
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcCategory_V11";
                else
                    TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcQuestions_V11";
            }
            return TestSPName;
        }
        public static DataSet GetTestScenarios(DBConnectionStringDTO DBConnectionParameters)
        {
            string InputQuery = "EXEC dbo.QAPROC_GetTestScenarios_V11";
            DataSet ds = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ds;
        }
        public static DataSet GetTestSPIDFromScenarios(DataRow row, DBConnectionStringDTO DBConnectionParameters)
        {
            string UserID = row[0].ToString() + ", ";
            string ClientID = row[1].ToString() + ", ";
            string SurveyFormID = row[2].ToString();
            string InputQuery = "EXEC dbo.QAPROC_GetTestSPIDFromScenarios_V11 " + UserID + ClientID + SurveyFormID;
            DataSet ds = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ds;
        }

        public static DataSet GetBenchmarkData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters)
        {
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'";
            string BenchmarkId = "'" + ReportFilters.Tables[0].Rows[0][6].ToString() + "'" + ", ";
            string TestSpQuery = "Exec dbo.BenchMarkCalculation_V11 " + BenchmarkId + SurveyIDs;
            DataSet BenchmarkData = DBOperations.ExecuteSPwithInputParameters(TestSpQuery, DBConnectionParameters);
            return BenchmarkData;
        }

        public static DataSet GetBenchmarkDataForTrend(DataSet ReportFilters, string ItemID, String CategoryID, DBConnectionStringDTO DBConnectionParameters)
        {
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string BenchmarkId = "'" + ReportFilters.Tables[0].Rows[0][4].ToString() + "'" + ", ";
            string SurveyformItemID = ItemID + ",";
            string SurveyCategoryID = CategoryID + ",";
            string TestSpQuery = "Exec BenchMarkCalculationTrend_V11 " + BenchmarkId + SurveyIDs + SurveyformItemID + SurveyCategoryID;
            DataSet BenchmarkData = DBOperations.ExecuteSPwithInputParameters(TestSpQuery, DBConnectionParameters);
            return BenchmarkData;
        }
        public static DataSet GetResultReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters)
        {
            string SurveyFormID = ReportFilters.Tables[0].Rows[0][0].ToString() + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'";
            string TestSPQuery = "EXEC dbo.Parent_SP_QAPROC_ResultsReport_V11 " + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            return ResultData;
        }
        public static DataSet GetInsightReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, string ReportDisplay)
        {
            string TestSPName;
            string SurveyFormID = ReportFilters.Tables[0].Rows[0][0].ToString() + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'";
            if (ReportDisplay.ToLower() == "category")
                TestSPName = "dbo.Parent_SP_QAPROC_ReportCalculationCategory_V11 ";
            else
                TestSPName = "dbo.QAPROC_ReportCalculationQuestions_V11 ";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            return ResultData;
        }
        public static DataSet GetDemographicReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, string ReportDisplay)
        {
            string TestSPName;
            string SurveyFormID = ReportFilters.Tables[0].Rows[0][0].ToString() + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'";
            if (ReportDisplay.ToLower() == "category")
                TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcCategory_V11 ";
            else
                TestSPName = "dbo.Parent_SP_QAPROC_DemographicReportCalcQuestions_V11 ";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            return ResultData;
        }
        public static DataSet GetCommentReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, string CategoryText)
        {
            int SurveyFormItemId = 82754;
            string TestSPName;
            string SurveyFormID = ReportFilters.Tables[0].Rows[0][0].ToString() + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'" + ", ";
            string query = "SELECT COUNT(*) FROM DimItem_V11 WHERE SurveyFormID = '" + SurveyFormID + "' and CategoryText = '" + CategoryText + "' AND GroupName = 'RatingScale'";
            DataSet rscount = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);
            if (rscount.Tables[0].Rows[0][0].ToString() == "0")
                TestSPName = "dbo.QAPROC_CommentReportCalc_UnScored_V11 ";
            else
                TestSPName = "dbo.QAPROC_CommentReportCalc_V11 ";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery + CategoryText + "@SurveyFormItemId = " + SurveyFormItemId;
            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
            return ResultData;
        }
        public static DataSet GetDrillDownReportData(DataSet ReportFilters, DBConnectionStringDTO DBConnectionParameters, String ReportDisplay)
        {
            string TestSPName;
            string SurveyFormID = "'" + ReportFilters.Tables[0].Rows[0][0].ToString() + "'" + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0][1].ToString() + "'" + ", ";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0][2].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0][3].ToString() + "'";
            if (ReportDisplay.ToLower() == "category")
                TestSPName = "dbo.Parent_SP_QAPROC_ReportCalculationCategory_V11";
            else
                TestSPName = "dbo.QAPROC_ReportCalculationQuestions_V11";
            string TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyIDs + DemoFilterQuery + TimePeriodQuery;

            DataSet ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);

            return ResultData;
        }
        public static DataSet GetTrendReportDataForRatingScale(DataSet ReportFilters, String GroupBy, String Show, String ItemorCategory, DBConnectionStringDTO DBConnectionParameters)
        {
            string TestSPName;
            DataSet ResultData;
            string TestSPQuery;
            string SurveyFormID = "'" + ReportFilters.Tables[0].Rows[0]["SurveyFormID"].ToString() + "'" + ", ";
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0]["SurveyIDs"].ToString() + "'";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0]["DemoFilter"].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0]["TimePeriodFilter"].ToString() + "'" + ", ";
            string ReportDisplay = ReportFilters.Tables[0].Rows[0]["ResultDisplay"].ToString();
            string ShowItemorCategory;
            if (Show.ToLower() == "item")
            {
                ShowItemorCategory = "SurveyFormItemID IN(" + ItemorCategory + ") , ";
            }
            else if (Show.ToLower() == "category")
            {
                ShowItemorCategory = "SurveyCategoryID IN(" + ItemorCategory + ") ,";
            }
            else
            {
                ShowItemorCategory = "'1 = 1', ";
            }
            switch (GroupBy.ToLower())
            {
                case "year":
                    TestSPName = "dbo.TrendSPExecution_Year ";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + DemoFilterQuery + TimePeriodQuery + ShowItemorCategory + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
                case "quarter":
                    TestSPName = "[dbo].[TrendSPExecution_Quarter] ";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + DemoFilterQuery + TimePeriodQuery + ShowItemorCategory + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
                case "month":
                    TestSPName = "[dbo].[TrendSPExecution_Month] ";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + DemoFilterQuery + TimePeriodQuery + ShowItemorCategory + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
                default:
                    TestSPName = "[dbo].[TrendSPExecution_Distribution] ";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + DemoFilterQuery + TimePeriodQuery + ShowItemorCategory + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
            }
            return ResultData;
        }
        public static DataSet GetTrendReportDataForNonRatingScale(DataSet ReportFilters, String GroupBy, String SurveyFormItemID, DBConnectionStringDTO DBConnectionParameters, String Show = null)
        {
            string TestSPName;
            DataSet ResultData;
            string TestSPQuery;
            string SurveyFormID = "'" + ReportFilters.Tables[0].Rows[0]["SurveyFormID"].ToString();
            string SurveyIDs = "'" + ReportFilters.Tables[0].Rows[0]["SurveyIDs"].ToString() + "'";
            string DemoFilterQuery = "'" + ReportFilters.Tables[0].Rows[0]["DemoFilter"].ToString() + "'" + ", ";
            string TimePeriodQuery = "'" + ReportFilters.Tables[0].Rows[0]["TimePeriodFilter"].ToString() + "'" + ", ";
            string ReportDisplay = ReportFilters.Tables[0].Rows[0]["ResultDisplay"].ToString();

            switch (GroupBy.ToLower())
            {
                case "year":
                    TestSPName = "dbo.TrendSPExecution_NR_Year ";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyFormItemID + ", " + DemoFilterQuery + TimePeriodQuery + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
                case "quarter":
                    TestSPName = "[dbo].[TrendSPExecution_NR_Quarter] ";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyFormItemID + ", " + DemoFilterQuery + TimePeriodQuery + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
                case "month":
                    TestSPName = "[dbo].[TrendSPExecution_NR_Month]";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyFormItemID + ", " + DemoFilterQuery + TimePeriodQuery + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
                default:
                    TestSPName = "[dbo].[TrendSPExecution_NR_Distribution]";
                    TestSPQuery = "EXEC " + TestSPName + SurveyFormID + SurveyFormItemID + ", " + DemoFilterQuery + TimePeriodQuery + SurveyIDs;
                    ResultData = DBOperations.ExecuteSPwithInputParameters(TestSPQuery, DBConnectionParameters);
                    break;
            }
            return ResultData;
        }
        public static DataSet GetActualSPReportData(DataRow row, DBConnectionStringDTO DBConnectionParameters, string ReportSPName)
        {
            string UserFilter = "";
            string UserID = row[0].ToString() + ", ";
            string ClientID = row[1].ToString() + ", ";
            string SurveyFormID = row[2].ToString();
            string InputQuery = "EXEC dbo." + ReportSPName + " " + "@UserID=" + UserID + "@ClientID=" + ClientID + "@SurveyFormID=" + SurveyFormID;
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ResultSet;
        }
        public static DataSet GetActualSPTrendReportData(DataRow row, DBConnectionStringDTO DBConnectionParameters, string ReportSPName, string show, string ItemorCategory, string GroupBy)
        {
            string UserID = "@userID = " + row[0].ToString() + ", ";
            string ClientID = "@clientID = " + row[1].ToString() + ", ";
            string SurveyFormID = "@surveyFormID = " + row[2].ToString() + ", ";
            string ShowCategory = "";
            string ShowItem = "";
            if (show.ToLower() == "category")
            {
                ShowCategory = "@surveycategoryfilter=" + ItemorCategory + ", ";
            }
            else if (show.ToLower() == "item")
            {
                ShowItem = "@surveyformitemfilter=" + ItemorCategory + ", ";

            }
            else
            {
                ShowCategory = "@surveycategoryfilter='', ";
                ShowItem = "@surveyformitemfilter='', ";
            }
            string GroupByFilter = "@GroupByFilter = '" + GroupBy + "'";

            string InputQuery = "EXEC dbo." + ReportSPName + " " + UserID + ClientID + SurveyFormID + ShowCategory + ShowItem + GroupByFilter;
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ResultSet;
        }

        public static DataSet GetActualSPNonRatingTrendReportData(DataRow row, DBConnectionStringDTO DBConnectionParameters, string ReportSPName, string SurveyformItem, string scaleRange, string GroupBy)
        {
            string UserID = row[0].ToString() + ", ";
            string ClientID = row[1].ToString() + ", ";
            string SurveyFormID = row[2].ToString();
            string SurveyFormItem = "@SurveyFormItemId=" + SurveyformItem + " ,";
            string ScaleOptions = "@ScaleOptionIds=" + scaleRange + " ,";
            string InputQuery = "EXEC dbo." + ReportSPName + " " + UserID + ClientID + SurveyFormItem + ScaleOptions + GroupBy;
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
            return ResultSet;
        }


        public static void WriteTestExecutionResult()
        {

        }
        public static void WriteFailedData(string Datacalcualtion, string reportname, int UseranalyzeFilterID, DBConnectionStringDTO DBConnectionParameters)
        {

            string InputQuery = "Insert into Writelog values " + Datacalcualtion + "," + reportname + "," + UseranalyzeFilterID;
            DataSet ResultSet = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);

        }

        public static bool CompareResultReportBenchmarkResults(string DataCalculation, DataSet ActualSP, DataSet CompGrp)
        {
            bool RSbbflag = true;
            bool NRbbflag = true;

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN(1,2) AND ScaleoptionID IS NULL";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrpRatingScaleTable;
            DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
            DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[0]);
            Compdv.Sort = "SurveyFormItemID ASC";
            CompGrpRatingScaleTable = Compdv.ToTable();

            if (DataCalculation.ToLower() == "average")
            {
                var ActualSPBenchmarkAverageCol = ActualSPRatingScaleTable.Columns["BenchMarkAverageScore"];
                var ActualSPBenchmarkCountCol = ActualSPRatingScaleTable.Columns["BenchMarkTotalResponse"];
                var CompGrpBenchmarkAverageCol = CompGrpRatingScaleTable.Columns["Mean"];
                var CompGrpvBenchmarkCountCol = CompGrpRatingScaleTable.Columns["RespondentCount"];

                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[ActualSPBenchmarkAverageCol] != RowTbl2[CompGrpBenchmarkAverageCol] || RowTbl1[ActualSPBenchmarkCountCol] != RowTbl2[CompGrpvBenchmarkCountCol])
                        {
                            RSbbflag = false;

                            break;
                        }
                    }
                }
            }
            else
            {
                var ActualSPBenchmarkFavCol = ActualSPRatingScaleTable.Columns["BenchMarkFavPercent"];
                var ActualSPBenchmarkNeuCol = ActualSPRatingScaleTable.Columns["BenchMarkNeutralPercent"];
                var ActualSPBenchmarkUnFavCol = ActualSPRatingScaleTable.Columns["BenchMarkUnFavPercent"];
                var ActualSPBenchmarkCountCol = ActualSPRatingScaleTable.Columns["BenchTotalResponse"];

                var CompGrpBenchmarkFavCol = CompGrpRatingScaleTable.Columns["BenchmarkFavperc"];
                var CompGrpBenchmarkNeuCol = CompGrpRatingScaleTable.Columns["Neuperc"];
                var CompGrpBenchmarkUnFavCol = CompGrpRatingScaleTable.Columns["UnFavperc"];
                var CompGrpBenchmarkCountCol = CompGrpRatingScaleTable.Columns["RespondentCount"];

                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrpRatingScaleTable.Rows)
                    {
                        if ((RowTbl1[ActualSPBenchmarkFavCol] != RowTbl2[CompGrpBenchmarkFavCol]) || (RowTbl1[ActualSPBenchmarkNeuCol] != RowTbl2[CompGrpBenchmarkNeuCol]) || (RowTbl1[ActualSPBenchmarkUnFavCol] != RowTbl2[CompGrpBenchmarkUnFavCol]) || (RowTbl1[ActualSPBenchmarkCountCol] != RowTbl2[CompGrpBenchmarkCountCol]))
                        {
                            RSbbflag = false;
                            break;
                        }
                    }
                }

            }

            DataTable ActualSPNonRatingScaleTable;
            DataViewManager ActualNRdvm = ActualSP.DefaultViewManager;
            DataView ActualNRDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualNRDv.RowFilter = "ItemType IN(3,4)";
            ActualNRDv.Sort = "ItemID,ScaleOptionID ASC";
            ActualSPNonRatingScaleTable = ActualNRDv.ToTable();

            DataTable CompNonRatingScaleTable;
            DataViewManager CompNRdvm = CompGrp.DefaultViewManager;
            DataView CompNRDv = CompNRdvm.CreateDataView(CompGrp.Tables[1]);
            CompNRDv.Sort = "SurveyFormItemID, BenchmarkScaleOptionId ASC";
            CompNonRatingScaleTable = CompNRDv.ToTable();

            var ActualNRScaleOptionIDCol = ActualSPNonRatingScaleTable.Columns["ScaleOptionID"];
            var ActualNRBenchmarkValueCol = ActualSPNonRatingScaleTable.Columns["BenchMarkAverageScore"];

            var CompNRScaleOptionIDCol = CompNonRatingScaleTable.Columns["BenchmarkScaleOptionId"];
            var CompNRBenchmarkValueCol = CompNonRatingScaleTable.Columns["RespPerc"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
            {
                foreach (DataRow RowTbl2 in CompNonRatingScaleTable.Rows)
                {
                    if ((RowTbl1[ActualNRScaleOptionIDCol].ToString()) == (RowTbl2[CompNRScaleOptionIDCol].ToString()))
                    {
                        if (RowTbl1[ActualNRBenchmarkValueCol] != RowTbl2[CompNRBenchmarkValueCol])
                        {
                            NRbbflag = false;
                            break;
                        }
                    }
                }
            }

            return RSbbflag && NRbbflag;
        }



        public static bool CompareInsightReportBenchmarkResults(string DataCalculation, DataSet ActualSP, DataSet CompGrp)
        {
            bool Strbbflag = true;
            bool Oppbbflag = true;

            DataTable ActualSPRatingScaleStrTable;
            DataViewManager ActualStrdvm = ActualSP.DefaultViewManager;
            DataView ActualStrDv = ActualStrdvm.CreateDataView(ActualSP.Tables[0]);
            ActualStrDv.Sort = "ObjectId ASC";
            ActualSPRatingScaleStrTable = ActualStrDv.ToTable();
            var objecttype = ActualStrDv.ToTable().Rows[0][0].ToString();

            DataTable ActualSPRatingScaleOppTable;
            DataViewManager ActualOppdvm = ActualSP.DefaultViewManager;
            DataView ActualOppDv = ActualOppdvm.CreateDataView(ActualSP.Tables[1]);
            ActualOppDv.Sort = "ObjectId ASC";
            ActualSPRatingScaleOppTable = ActualOppDv.ToTable();

            if (objecttype.ToLower() == "item")
            {
                DataTable CompGrpRatingScaleItemTable;
                DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[0]);
                Compdv.Sort = "SurveyFormItemID ASC";
                CompGrpRatingScaleItemTable = Compdv.ToTable();

                if (DataCalculation.ToLower() == "average")
                {
                    var ActualSPBenchmarkAverageCol = ActualSPRatingScaleStrTable.Columns["Comp_Bench_Mean_Diff"];
                    var ActualSPObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                    var ActualSPMeanCol = ActualSPRatingScaleStrTable.Columns["MeanScore"];
                    var ActualSPBenchmarkOppAverageCol = ActualSPRatingScaleOppTable.Columns["Comp_Bench_Mean_Diff"];
                    var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                    var ActualSPOppMeanCol = ActualSPRatingScaleOppTable.Columns["MeanScore"];

                    var CompSPBenchmarkMeanCol = CompGrpRatingScaleItemTable.Columns["Mean"];
                    var CompSPSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleStrTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleItemTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkAverageCol]) != Diff_benchmark)
                                {
                                    Strbbflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleOppTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleItemTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPOppObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppAverageCol]) != Diff_benchmark)
                                {
                                    Oppbbflag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var ActualSPBenchmarkFavCol = ActualSPRatingScaleStrTable.Columns["Comp_Bench_Favperc_Diff"];
                    var ActualSPObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                    var ActualSPFavCol = ActualSPRatingScaleStrTable.Columns["FavPerc"];
                    var ActualSPBenchmarkOppFavCol = ActualSPRatingScaleOppTable.Columns["Comp_Bench_Favperc_Diff"];
                    var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                    var ActualSPOppFavCol = ActualSPRatingScaleOppTable.Columns["FavPerc"];

                    var CompSPBenchmarkFavCol = CompGrpRatingScaleItemTable.Columns["BenchmarkFavPerc"];
                    var CompSPSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleStrTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleItemTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkFavCol]) != Diff_benchmark)
                                {
                                    Strbbflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleOppTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleItemTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPOppObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppFavCol]) != Diff_benchmark)
                                {
                                    Oppbbflag = false;
                                    break;
                                }
                            }
                        }
                    }

                }

            }
            else
            {
                DataTable CompGrpRatingScaleCategoryTable;
                DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[2]);
                Compdv.Sort = "ObjectID ASC";
                CompGrpRatingScaleCategoryTable = Compdv.ToTable();


                if (DataCalculation.ToLower() == "average")
                {
                    var ActualSPBenchmarkAverageCol = ActualSPRatingScaleStrTable.Columns["Comp_Bench_Mean_Diff"];
                    var ActualSPObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                    var ActualSPMeanCol = ActualSPRatingScaleStrTable.Columns["MeanScore"];
                    var ActualSPBenchmarkOppAverageCol = ActualSPRatingScaleOppTable.Columns["Comp_Bench_Mean_Diff"];
                    var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                    var ActualSPOppMeanCol = ActualSPRatingScaleOppTable.Columns["MeanScore"];

                    var CompSPBenchmarkMeanCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkMean"];
                    var CompSPSurveyformItemIDCol = CompGrpRatingScaleCategoryTable.Columns["ObjectId"];
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleStrTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleCategoryTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                if (Rowtbl2[CompSPBenchmarkMeanCol].ToString() == null)
                                {
                                    if ((Rowtbl1[ActualSPBenchmarkAverageCol].ToString()) != (Rowtbl2[CompSPBenchmarkMeanCol].ToString()))
                                    {
                                        Strbbflag = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                    if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkAverageCol]) != Diff_benchmark)
                                    {
                                        Strbbflag = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleOppTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleCategoryTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPOppObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                if (Rowtbl2[CompSPBenchmarkMeanCol].ToString() == null)
                                {
                                    if ((Rowtbl1[ActualSPBenchmarkOppAverageCol].ToString()) != (Rowtbl2[CompSPBenchmarkMeanCol].ToString()))
                                    {
                                        Oppbbflag = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppMeanCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkMeanCol]);
                                    if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppAverageCol]) != Diff_benchmark)
                                    {
                                        Oppbbflag = false;
                                        break;

                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var ActualSPBenchmarkFavCol = ActualSPRatingScaleStrTable.Columns["Comp_Bench_Favperc_Diff"];
                    var ActualSPObjectIdCol = ActualSPRatingScaleStrTable.Columns["ObjectId"];
                    var ActualSPFavCol = ActualSPRatingScaleStrTable.Columns["FavPerc"];
                    var ActualSPBenchmarkOppFavCol = ActualSPRatingScaleOppTable.Columns["Comp_Bench_Favperc_Diff"];
                    var ActualSPOppObjectIdCol = ActualSPRatingScaleOppTable.Columns["ObjectId"];
                    var ActualSPOppFavCol = ActualSPRatingScaleOppTable.Columns["FavPerc"];

                    var CompSPBenchmarkFavCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkPerc"];
                    var CompSPSurveyformItemIDCol = CompGrpRatingScaleCategoryTable.Columns["ObjectID"];
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleStrTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleCategoryTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                if (Rowtbl2[CompSPBenchmarkFavCol].ToString() == null)
                                {
                                    if ((Rowtbl1[ActualSPBenchmarkFavCol].ToString()) != (Rowtbl2[CompSPBenchmarkFavCol].ToString()))
                                    {
                                        Strbbflag = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                    if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkFavCol]) != Diff_benchmark)
                                    {
                                        Strbbflag = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    foreach (DataRow Rowtbl1 in ActualSPRatingScaleOppTable.Rows)
                    {
                        foreach (DataRow Rowtbl2 in CompGrpRatingScaleCategoryTable.Rows)
                        {
                            if ((Rowtbl1[ActualSPOppObjectIdCol].ToString()) == (Rowtbl2[CompSPSurveyformItemIDCol].ToString()))
                            {
                                if (Rowtbl2[CompSPBenchmarkFavCol].ToString() == null)
                                {
                                    if ((Rowtbl1[ActualSPBenchmarkOppFavCol].ToString()) != (Rowtbl2[CompSPBenchmarkFavCol].ToString()))
                                    {
                                        Oppbbflag = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_benchmark = Convert.ToInt32(Rowtbl1[ActualSPOppFavCol]) - Convert.ToInt32(Rowtbl2[CompSPBenchmarkFavCol]);
                                    if (Convert.ToInt32(Rowtbl1[ActualSPBenchmarkOppFavCol]) != Diff_benchmark)
                                    {
                                        Oppbbflag = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }


                }

            }

            return Oppbbflag && Strbbflag;
        }

        public static bool CompareDrillDownReportBenchmarkResults(string DataCalculation, DataSet ActualSP, DataSet CompGrp)
        {
            bool bbflag = true;
            string Columnname;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
            {
                Columnname = "SurveyFormItemID";
            }
            else
            {
                Columnname = "CategoryID";
            }

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[0]);
            ActualDv.Sort = Columnname + "ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            if (Columnname.ToLower() == "surveyformitemID")
            {

                DataTable CompGrpRatingScaleItemTable;
                DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[0]);
                Compdv.Sort = "SurveyFormItemID ASC";
                CompGrpRatingScaleItemTable = Compdv.ToTable();
                if (DataCalculation.ToLower() == "average")
                {
                    var ActualSPBenchmarkAverageValueCol = ActualSPRatingScaleTable.Columns["BenchMarkDiffAverageScore"];
                    var ActualSpSurveyformItemIDCol = ActualSPRatingScaleTable.Columns["SurveyFormItemID"];
                    var ActualSPAverageScoreCol = ActualSPRatingScaleTable.Columns["AverageScore"];

                    var CompSPBenchmarkAverageValueCol = CompGrpRatingScaleItemTable.Columns["Mean"];
                    var CompSpSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in ActualSPRatingScaleTable.Rows)
                        {
                            if ((RowTbl1[ActualSpSurveyformItemIDCol].ToString()) == (RowTbl2[CompSpSurveyformItemIDCol].ToString()))
                            {

                                var Diff_benchmark = Convert.ToDecimal(RowTbl1[ActualSPAverageScoreCol]) - Convert.ToDecimal(RowTbl2[CompSPBenchmarkAverageValueCol]);
                                if (Convert.ToDecimal(RowTbl1[ActualSPBenchmarkAverageValueCol]) != Convert.ToDecimal(Diff_benchmark))
                                {
                                    bbflag = false;
                                    break;
                                }
                            }
                        }

                    }


                }
                else
                {
                    var ActualSPBenchmarkFavPercCol = ActualSPRatingScaleTable.Columns["BenchMarkDiffFavScore"];
                    var ActualSpSurveyformItemIDCol = ActualSPRatingScaleTable.Columns["SurveyFormItemID"];
                    var ActualSPFavPercCol = ActualSPRatingScaleTable.Columns["FavPercent"];

                    var CompSPBenchmarkFavPercCol = CompGrpRatingScaleItemTable.Columns["BenchmarkFavPerc"];
                    var CompSpSurveyformItemIDCol = CompGrpRatingScaleItemTable.Columns["SurveyFormItemID"];

                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in ActualSPRatingScaleTable.Rows)
                        {
                            if ((RowTbl1[ActualSpSurveyformItemIDCol].ToString()) == (RowTbl2[CompSpSurveyformItemIDCol].ToString()))
                            {
                                var Diff_benchmark = Convert.ToInt32(RowTbl1[ActualSPFavPercCol]) - Convert.ToInt32(RowTbl2[CompSPBenchmarkFavPercCol]);
                                if (Convert.ToInt32(RowTbl1[ActualSPBenchmarkFavPercCol]) != Convert.ToInt32(Diff_benchmark))
                                {
                                    bbflag = false;
                                    break;
                                }
                            }
                        }
                    }

                }

            }
            else
            {
                DataTable CompGrpRatingScaleCategoryTable;
                DataViewManager CompGrpdvm = CompGrp.DefaultViewManager;
                DataView Compdv = CompGrpdvm.CreateDataView(CompGrp.Tables[2]);
                Compdv.Sort = "ObjectID ASC";
                CompGrpRatingScaleCategoryTable = Compdv.ToTable();

                if (DataCalculation.ToLower() == "average")
                {
                    var ActualSPBenchmarkAverageValueCol = ActualSPRatingScaleTable.Columns["BenchMarkDiffAverageScore"];
                    var ActualSpCategoryIDCol = ActualSPRatingScaleTable.Columns["CategoryID"];
                    var ActualSPAverageScoreCol = ActualSPRatingScaleTable.Columns["AverageScore"];

                    var CompSPBenchmarkAverageValueCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkMean"];
                    var CompSpCategoryIDCol = CompGrpRatingScaleCategoryTable.Columns["ObjectID"];

                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in ActualSPRatingScaleTable.Rows)
                        {
                            if ((RowTbl1[ActualSpCategoryIDCol].ToString()) == (RowTbl2[CompSpCategoryIDCol].ToString()))
                            {
                                if (RowTbl2[CompSPBenchmarkAverageValueCol].ToString() == null)
                                {
                                    if ((RowTbl1[ActualSPBenchmarkAverageValueCol].ToString()) != (RowTbl2[CompSPBenchmarkAverageValueCol].ToString()))
                                    {
                                        bbflag = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    var Diff_benchmark = Convert.ToDecimal(RowTbl1[ActualSPAverageScoreCol]) - Convert.ToDecimal(RowTbl2[CompSPBenchmarkAverageValueCol]);
                                    if (Convert.ToDecimal(RowTbl1[ActualSPBenchmarkAverageValueCol]) != Convert.ToDecimal(Diff_benchmark))
                                    {
                                        bbflag = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var ActualSPBenchmarkFavPercCol = ActualSPRatingScaleTable.Columns["BenchMarkDiffFavScore"];
                    var ActualSpCategoryIDCol = ActualSPRatingScaleTable.Columns["CategoryID"];
                    var ActualSPFavPercCol = ActualSPRatingScaleTable.Columns["FavPercent"];

                    var CompSPBenchmarkFavPercCol = CompGrpRatingScaleCategoryTable.Columns["BenchmarkFavPerc"];
                    var CompSpCategoryCol = CompGrpRatingScaleCategoryTable.Columns["ObjectID"];

                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in ActualSPRatingScaleTable.Rows)
                        {
                            if ((RowTbl1[ActualSpCategoryIDCol].ToString()) == (RowTbl2[CompSpCategoryCol].ToString()))
                            {
                                if (RowTbl2[CompSPBenchmarkFavPercCol].ToString() == null)
                                {
                                    if ((RowTbl1[ActualSPBenchmarkFavPercCol].ToString()) != (RowTbl2[CompSPBenchmarkFavPercCol].ToString()))
                                    {
                                        bbflag = false;
                                        break;
                                    }
                                }

                                else
                                {
                                    var Diff_benchmark = Convert.ToInt32(RowTbl1[ActualSPFavPercCol]) - Convert.ToInt32(RowTbl2[CompSPBenchmarkFavPercCol]);
                                    if (Convert.ToInt32(RowTbl1[ActualSPBenchmarkFavPercCol]) != Convert.ToInt32(Diff_benchmark))
                                    {
                                        bbflag = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }
            }

            return bbflag;

        }

        public static bool CompareTrendReportBenchmarkResult(string DataCalculation, DataSet ActualSP, DataSet CompGrp)
        {
            bool Tbbflag = true;
            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[0]);
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompSPRatingScaleTable;
            DataViewManager Compdvm = CompGrp.DefaultViewManager;
            DataView CompDv = Actualdvm.CreateDataView(CompGrp.Tables[0]);
            CompSPRatingScaleTable = CompDv.ToTable();

            if (DataCalculation.ToLower() == "average")
            {
                var ActualSpMeanvalue = ActualSPRatingScaleTable.Rows[0][1].ToString();
                var CompGrpMeanValue = CompSPRatingScaleTable.Rows[0][1].ToString();
                var ActualSpRespondentCount = ActualSPRatingScaleTable.Rows[0][3].ToString();
                var CompGrpRespondentCount = CompSPRatingScaleTable.Rows[0][2].ToString();
                if ((ActualSpMeanvalue != CompGrpMeanValue) || (ActualSpRespondentCount != CompGrpRespondentCount))
                {
                    Tbbflag = false;

                }
            }
            else
            {
                var ActualSpFavPercvalue = ActualSPRatingScaleTable.Rows[0][0].ToString();
                var CompGrpFavpercValue = CompSPRatingScaleTable.Rows[0][0].ToString();
                var ActualSpRespondentCount = ActualSPRatingScaleTable.Rows[0][3].ToString();
                var CompGrpRespondentCount = CompSPRatingScaleTable.Rows[0][2].ToString();
                if ((ActualSpFavPercvalue != CompGrpFavpercValue) || (ActualSpRespondentCount != CompGrpRespondentCount))
                {
                    Tbbflag = false;

                }
            }

            return Tbbflag;
        }

        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp)
        {
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            int avgORfav = 0;

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable TargetGrpRatingScaleTable;
            DataViewManager TGrpDvm = TargetGrp.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView TGrpDv = TGrpDvm.CreateDataView(TargetGrp.Tables[avgORfav]);
            TGrpDv.Sort = "ItemID ASC";
            TargetGrpRatingScaleTable = TGrpDv.ToTable();

            if (DataCalculation.ToLower() == "average")
            {
                var AvgScoreCol = ActualSPRatingScaleTable.Columns["AverageScore"];
                var MeanCol = TargetGrpRatingScaleTable.Columns["Mean"];
                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[AvgScoreCol] != RowTbl2[MeanCol])
                        {
                            rsflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }
            }
            else
            {
                var FavScoreCol = ActualSPRatingScaleTable.Columns["FavPercent"];
                var NeuScoreCol = ActualSPRatingScaleTable.Columns["NeutralPercent"];
                var UnFavScoreCol = ActualSPRatingScaleTable.Columns["UnFavPercent"];

                var FavorableCol = TargetGrpRatingScaleTable.Columns["Favorable"];
                var NeutralCol = TargetGrpRatingScaleTable.Columns["Neutral"];
                var UnfavorableCol = TargetGrpRatingScaleTable.Columns["Unfavorable"];
                foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {
                        if ((RowTbl1[FavScoreCol] != RowTbl2[FavorableCol]) || (RowTbl1[NeuScoreCol] != RowTbl2[NeutralCol]) || (RowTbl1[UnFavScoreCol] != RowTbl2[UnfavorableCol]))
                        {
                            rsflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }
            }

            DataTable ActualSPNonRatingScaleTable;
            DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
            DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
            ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";

            ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
            ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

            DataTable TargetNonRatingGrpTable;
            DataViewManager TGrpNonRatingDvm = TargetGrp.DefaultViewManager;
            DataView TGrpNonRatingDv = TGrpNonRatingDvm.CreateDataView(TargetGrp.Tables[2]);
            TGrpNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
            TargetNonRatingGrpTable = TGrpNonRatingDv.ToTable();

            var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["AverageScore"];
            var TrgNonRatingscore = TargetNonRatingGrpTable.Columns["ResponseRate"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetNonRatingGrpTable.Rows)
                {
                    if (RowTbl1[ActNonRatingScore] != RowTbl2[TrgNonRatingscore])
                    {
                        nonrsflag = false;
                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                        break;
                    }
                }
            }

            DataTable ActualSPCommentTable;
            DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
            DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
            ActualCommentDv.RowFilter = "ItemType IN(6)";
            ActualCommentDv.Sort = "ItemID ASC";
            ActualSPCommentTable = ActualCommentDv.ToTable();

            DataTable TargetCommentTable;
            DataViewManager TargetCommentTabledvm = TargetGrp.DefaultViewManager;
            DataView TargetCommentDv = TargetCommentTabledvm.CreateDataView(TargetGrp.Tables[3]);
            TargetCommentDv.Sort = "SurveyformItemID ASC";
            TargetCommentTable = TargetCommentDv.ToTable();

            var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
            var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
            var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];

            var TrgCommentWordID = TargetCommentTable.Columns["WordID"];
            var TrgCommentText = TargetCommentTable.Columns["Word"];
            var TrgCommentWordFrequency = TargetCommentTable.Columns["WordFrequency"];

            foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetCommentTable.Rows)
                {
                    if ((RowTbl1[ActCommentWordID] != RowTbl2[TrgCommentWordID]) || (RowTbl1[ActCommentText] != RowTbl2[TrgCommentText]) || (RowTbl1[ActWordFrequency] != RowTbl2[TrgCommentWordFrequency]))

                    {
                        wcflag = false;
                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                        break;
                    }
                }
            }

            DataTable ActualSPFFTable;
            DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
            DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
            ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
            ActualFFDv.Sort = "ItemID ASC";
            ActualSPFFTable = ActualFFDv.ToTable();

            DataTable TargetFFTable;
            DataViewManager TargetFFTabledvm = TargetGrp.DefaultViewManager;
            DataView TargetFFDv = TargetFFTabledvm.CreateDataView(TargetGrp.Tables[4]);
            TargetFFDv.Sort = "ItemID ASC";
            TargetFFTable = TargetFFDv.ToTable();
            var ActFFItemID = ActualSPCommentTable.Columns["ItemID"];
            var ActFFText = ActualSPCommentTable.Columns["CommentText"];
            var TrgFFItemID = TargetCommentTable.Columns["ItemID"];
            var TrgFFText = TargetCommentTable.Columns["Comments"];

            foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetFFTable.Rows)
                {
                    if ((RowTbl1[ActFFItemID] != RowTbl2[TrgFFItemID]) || (RowTbl1[ActFFText] != RowTbl2[TrgFFText]))

                    {
                        ffflag = false;
                        WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                        break;
                    }
                }
            }

            return rsflag && nonrsflag && wcflag && ffflag;
        }
        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool firstlevelcomparison = CompareResultReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp);
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            bool bbflag = true;
            int avgORfav = 0;

            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrp1RatingScaleTable;
            DataViewManager TGrpDvm = CompGrp1.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView TGrpDv = TGrpDvm.CreateDataView(CompGrp1.Tables[avgORfav]);
            TGrpDv.Sort = "ItemID ASC";
            CompGrp1RatingScaleTable = TGrpDv.ToTable();
            DataColumnCollection Columns = CompGrp1RatingScaleTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp1);

            }
            else
            {

                if (DataCalculation.ToLower() == "average")
                {
                    var AvgScoreCol = ActualSPRatingScaleTable.Columns["Comp1AverageScore"];
                    var MeanCol = CompGrp1RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if (RowTbl1[AvgScoreCol] != RowTbl2[MeanCol])
                            {
                                rsflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    var FavScoreCol = ActualSPRatingScaleTable.Columns["CompGrp1TableFavPercent"];
                    var NeuScoreCol = ActualSPRatingScaleTable.Columns["CompGrp1TableNeutralPercent"];
                    var UnFavScoreCol = ActualSPRatingScaleTable.Columns["CompGrp1TableUnFavPercent"];

                    var FavorableCol = CompGrp1RatingScaleTable.Columns["Favorable"];
                    var NeutralCol = CompGrp1RatingScaleTable.Columns["Neutral"];
                    var UnfavorableCol = CompGrp1RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if ((RowTbl1[FavScoreCol] != RowTbl2[FavorableCol]) || (RowTbl1[NeuScoreCol] != RowTbl2[NeutralCol]) || (RowTbl1[UnFavScoreCol] != RowTbl2[UnfavorableCol]))
                            {
                                rsflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }


                DataTable ActualSPNonRatingScaleTable;
                DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
                DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
                ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";
                ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

                DataTable Comp1NonRatingGrpTable;
                DataViewManager comp1NonRatingDvm = CompGrp1.DefaultViewManager;
                DataView Comp1NonRatingDv = comp1NonRatingDvm.CreateDataView(CompGrp1.Tables[2]);
                Comp1NonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                Comp1NonRatingGrpTable = Comp1NonRatingDv.ToTable();

                var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["Comp1AverageScore"];
                var TrgNonRatingscore = Comp1NonRatingGrpTable.Columns["ResponseRate"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in Comp1NonRatingGrpTable.Rows)
                    {
                        if (RowTbl1[ActNonRatingScore] != RowTbl2[TrgNonRatingscore])
                        {
                            nonrsflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }

                DataTable ActualSPCommentTable;
                DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
                DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualCommentDv.RowFilter = "ItemType IN(6)";
                ActualCommentDv.Sort = "ItemID ASC";
                ActualSPCommentTable = ActualCommentDv.ToTable();

                DataTable CompGrp1TargetCommentTable;
                DataViewManager CompGrp1TargetCommentTabledvm = CompGrp1.DefaultViewManager;
                DataView CompGrp1TargetCommentDv = CompGrp1TargetCommentTabledvm.CreateDataView(CompGrp1.Tables[3]);
                CompGrp1TargetCommentDv.Sort = "SurveyformItemID ASC";
                CompGrp1TargetCommentTable = CompGrp1TargetCommentDv.ToTable();

                var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
                var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
                var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];

                var TrgCommentWordID = CompGrp1TargetCommentTable.Columns["WordID"];
                var TrgCommentText = CompGrp1TargetCommentTable.Columns["Word"];
                var TrgCommentWordFrequency = CompGrp1TargetCommentTable.Columns["WordFrequency"];

                foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp1TargetCommentTable.Rows)
                    {
                        if ((RowTbl1[ActCommentWordID] != RowTbl2[TrgCommentWordID]) || (RowTbl1[ActCommentText] != RowTbl2[TrgCommentText]) || (RowTbl1[ActWordFrequency] != RowTbl2[TrgCommentWordFrequency]))

                        {
                            wcflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }

                DataTable ActualSPFFTable;
                DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
                DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
                ActualFFDv.Sort = "ItemID ASC";
                ActualSPFFTable = ActualFFDv.ToTable();

                DataTable CompGrp1TargetFFTable;
                DataViewManager CompGrp1TargetFFTabledvm = CompGrp1.DefaultViewManager;
                DataView CompGrp1TargetFFDv = CompGrp1TargetFFTabledvm.CreateDataView(CompGrp1.Tables[4]);
                CompGrp1TargetFFDv.Sort = "ItemID ASC";
                CompGrp1TargetFFTable = CompGrp1TargetFFDv.ToTable();
                var ActFFItemID = ActualSPCommentTable.Columns["ItemID"];
                var ActFFText = ActualSPCommentTable.Columns["CommentText"];
                var TrgFFItemID = CompGrp1TargetCommentTable.Columns["ItemID"];
                var TrgFFText = CompGrp1TargetCommentTable.Columns["Comments"];

                foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp1TargetFFTable.Rows)
                    {
                        if ((RowTbl1[ActFFItemID] != RowTbl2[TrgFFItemID]) || (RowTbl1[ActFFText] != RowTbl2[TrgFFText]))

                        {
                            ffflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }

            }


            return firstlevelcomparison && rsflag && nonrsflag && wcflag && ffflag && bbflag;
        }

        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool secondlevelcomparison = CompareResultReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1);
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrp2RatingScaleTable;
            DataViewManager TGrpDvm = CompGrp2.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView TGrpDv = TGrpDvm.CreateDataView(CompGrp2.Tables[avgORfav]);
            TGrpDv.Sort = "ItemID ASC";
            CompGrp2RatingScaleTable = TGrpDv.ToTable();
            DataColumnCollection Columns = CompGrp2RatingScaleTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp2);

            }
            else
            {
                if (DataCalculation.ToLower() == "average")
                {
                    var AvgScoreCol = ActualSPRatingScaleTable.Columns["Comp2AverageScore"];
                    var MeanCol = CompGrp2RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if (RowTbl1[AvgScoreCol] != RowTbl2[MeanCol])
                            {
                                rsflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    var FavScoreCol = ActualSPRatingScaleTable.Columns["CompGrp2TableFavPercent"];
                    var NeuScoreCol = ActualSPRatingScaleTable.Columns["CompGrp2TableNeutralPercent"];
                    var UnFavScoreCol = ActualSPRatingScaleTable.Columns["CompGrp2TableUnFavPercent"];

                    var FavorableCol = CompGrp2RatingScaleTable.Columns["Favorable"];
                    var NeutralCol = CompGrp2RatingScaleTable.Columns["Neutral"];
                    var UnfavorableCol = CompGrp2RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if ((RowTbl1[FavScoreCol] != RowTbl2[FavorableCol]) || (RowTbl1[NeuScoreCol] != RowTbl2[NeutralCol]) || (RowTbl1[UnFavScoreCol] != RowTbl2[UnfavorableCol]))
                            {
                                rsflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }



                DataTable ActualSPNonRatingScaleTable;
                DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
                DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
                ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";
                ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

                DataTable Comp2NonRatingGrpTable;
                DataViewManager comp2NonRatingDvm = CompGrp2.DefaultViewManager;
                DataView Comp2NonRatingDv = comp2NonRatingDvm.CreateDataView(CompGrp2.Tables[2]);
                Comp2NonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                Comp2NonRatingGrpTable = Comp2NonRatingDv.ToTable();

                var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["Comp2AverageScore"];
                var TrgNonRatingscore = Comp2NonRatingGrpTable.Columns["ResponseRate"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in Comp2NonRatingGrpTable.Rows)
                    {
                        if (RowTbl1[ActNonRatingScore] != RowTbl2[TrgNonRatingscore])
                        {
                            nonrsflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }

                DataTable ActualSPCommentTable;
                DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
                DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualCommentDv.RowFilter = "ItemType IN(6)";
                ActualCommentDv.Sort = "ItemID ASC";
                ActualSPCommentTable = ActualCommentDv.ToTable();

                DataTable CompGrp2TargetCommentTable;
                DataViewManager CompGrp2TargetCommentTabledvm = CompGrp2.DefaultViewManager;
                DataView CompGrp2TargetCommentDv = CompGrp2TargetCommentTabledvm.CreateDataView(CompGrp2.Tables[3]);
                CompGrp2TargetCommentDv.Sort = "SurveyformItemID ASC";
                CompGrp2TargetCommentTable = CompGrp2TargetCommentDv.ToTable();

                var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
                var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
                var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];

                var TrgCommentWordID = CompGrp2TargetCommentTable.Columns["WordID"];
                var TrgCommentText = CompGrp2TargetCommentTable.Columns["Word"];
                var TrgCommentWordFrequency = CompGrp2TargetCommentTable.Columns["WordFrequency"];

                foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp2TargetCommentTable.Rows)
                    {
                        if ((RowTbl1[ActCommentWordID] != RowTbl2[TrgCommentWordID]) || (RowTbl1[ActCommentText] != RowTbl2[TrgCommentText]) || (RowTbl1[ActWordFrequency] != RowTbl2[TrgCommentWordFrequency]))

                        {
                            wcflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }


                DataTable ActualSPFFTable;
                DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
                DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
                ActualFFDv.Sort = "ItemID ASC";
                ActualSPFFTable = ActualFFDv.ToTable();

                DataTable CompGrp2TargetFFTable;
                DataViewManager CompGrp2TargetFFTabledvm = CompGrp2.DefaultViewManager;
                DataView CompGrp2TargetFFDv = CompGrp2TargetFFTabledvm.CreateDataView(CompGrp2.Tables[4]);
                CompGrp2TargetFFDv.Sort = "ItemID ASC";
                CompGrp2TargetFFTable = CompGrp2TargetFFDv.ToTable();
                var ActFFItemID = ActualSPCommentTable.Columns["ItemID"];
                var ActFFText = ActualSPCommentTable.Columns["CommentText"];
                var TrgFFItemID = CompGrp2TargetCommentTable.Columns["ItemID"];
                var TrgFFText = CompGrp2TargetCommentTable.Columns["Comments"];

                foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp2TargetFFTable.Rows)
                    {
                        if ((RowTbl1[ActFFItemID] != RowTbl2[TrgFFItemID]) || (RowTbl1[ActFFText] != RowTbl2[TrgFFText]))

                        {
                            ffflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }
            }


            return secondlevelcomparison && rsflag && nonrsflag && wcflag && ffflag && bbflag;
        }
        public static bool CompareResultReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool thirdlevelcomparison = CompareResultReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1, CompGrp2);
            bool rsflag = true;
            bool nonrsflag = true;
            bool wcflag = true;
            bool ffflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            DataTable ActualSPRatingScaleTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[2]);
            ActualDv.RowFilter = "ItemType IN (1,2)";
            ActualDv.Sort = "ItemID ASC";
            ActualSPRatingScaleTable = ActualDv.ToTable();

            DataTable CompGrp3RatingScaleTable;
            DataViewManager TGrpDvm = CompGrp3.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView TGrpDv = TGrpDvm.CreateDataView(CompGrp3.Tables[avgORfav]);
            TGrpDv.Sort = "ItemID ASC";
            CompGrp3RatingScaleTable = TGrpDv.ToTable();
            DataColumnCollection Columns = CompGrp3RatingScaleTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp3);

            }
            else
            {

                if (DataCalculation.ToLower() == "average")
                {
                    var AvgScoreCol = ActualSPRatingScaleTable.Columns["Comp3AverageScore"];
                    var MeanCol = CompGrp3RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if (RowTbl1[AvgScoreCol] != RowTbl2[MeanCol])
                            {
                                rsflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    var FavScoreCol = ActualSPRatingScaleTable.Columns["CompGrp3TableFavPercent"];
                    var NeuScoreCol = ActualSPRatingScaleTable.Columns["CompGrp3TableNeutralPercent"];
                    var UnFavScoreCol = ActualSPRatingScaleTable.Columns["CompGrp3TableUnFavPercent"];

                    var FavorableCol = CompGrp3RatingScaleTable.Columns["Favorable"];
                    var NeutralCol = CompGrp3RatingScaleTable.Columns["Neutral"];
                    var UnfavorableCol = CompGrp3RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPRatingScaleTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if ((RowTbl1[FavScoreCol] != RowTbl2[FavorableCol]) || (RowTbl1[NeuScoreCol] != RowTbl2[NeutralCol]) || (RowTbl1[UnFavScoreCol] != RowTbl2[UnfavorableCol]))
                            {
                                rsflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }



                DataTable ActualSPNonRatingScaleTable;
                DataViewManager ActualNonRatingdvm = ActualSP.DefaultViewManager;
                DataView ActualNonRatingDv = ActualNonRatingdvm.CreateDataView(ActualSP.Tables[2]);
                ActualNonRatingDv.RowFilter = "ItemType IN(3,4)";
                ActualNonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                ActualSPNonRatingScaleTable = ActualNonRatingDv.ToTable();

                DataTable Comp3NonRatingGrpTable;
                DataViewManager comp3NonRatingDvm = CompGrp3.DefaultViewManager;
                DataView Comp3NonRatingDv = comp3NonRatingDvm.CreateDataView(CompGrp3.Tables[2]);
                Comp3NonRatingDv.Sort = "ItemID,ScaleOptionID ASC";
                Comp3NonRatingGrpTable = Comp3NonRatingDv.ToTable();

                var ActNonRatingScore = ActualSPNonRatingScaleTable.Columns["Comp3AverageScore"];
                var TrgNonRatingscore = Comp3NonRatingGrpTable.Columns["ResponseRate"];

                foreach (DataRow RowTbl1 in ActualSPNonRatingScaleTable.Rows)
                {
                    foreach (DataRow RowTbl2 in Comp3NonRatingGrpTable.Rows)
                    {
                        if (RowTbl1[ActNonRatingScore] != RowTbl2[TrgNonRatingscore])
                        {
                            nonrsflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }


                DataTable ActualSPCommentTable;
                DataViewManager ActualCommentTabledvm = ActualSP.DefaultViewManager;
                DataView ActualCommentDv = ActualCommentTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualCommentDv.RowFilter = "ItemType IN(6)";
                ActualCommentDv.Sort = "ItemID ASC";
                ActualSPCommentTable = ActualCommentDv.ToTable();

                DataTable CompGrp3TargetCommentTable;
                DataViewManager CompGrp3TargetCommentTabledvm = CompGrp3.DefaultViewManager;
                DataView CompGrp3TargetCommentDv = CompGrp3TargetCommentTabledvm.CreateDataView(CompGrp3.Tables[3]);
                CompGrp3TargetCommentDv.Sort = "SurveyformItemID ASC";
                CompGrp3TargetCommentTable = CompGrp3TargetCommentDv.ToTable();

                var ActCommentWordID = ActualSPCommentTable.Columns["WordID"];
                var ActCommentText = ActualSPCommentTable.Columns["CommentText"];
                var ActWordFrequency = ActualSPCommentTable.Columns["WordFrequency"];

                var TrgCommentWordID = CompGrp3TargetCommentTable.Columns["WordID"];
                var TrgCommentText = CompGrp3TargetCommentTable.Columns["Word"];
                var TrgCommentWordFrequency = CompGrp3TargetCommentTable.Columns["WordFrequency"];

                foreach (DataRow RowTbl1 in ActualSPCommentTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp3TargetCommentTable.Rows)
                    {
                        if ((RowTbl1[ActCommentWordID] != RowTbl2[TrgCommentWordID]) || (RowTbl1[ActCommentText] != RowTbl2[TrgCommentText]) || (RowTbl1[ActWordFrequency] != RowTbl2[TrgCommentWordFrequency]))

                        {
                            wcflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }


                DataTable ActualSPFFTable;
                DataViewManager ActualFFTabledvm = ActualSP.DefaultViewManager;
                DataView ActualFFDv = ActualFFTabledvm.CreateDataView(ActualSP.Tables[2]);
                ActualFFDv.RowFilter = "ItemType IN(8,9,10,11)";
                ActualFFDv.Sort = "ItemID ASC";
                ActualSPFFTable = ActualFFDv.ToTable();

                DataTable CompGrp3TargetFFTable;
                DataViewManager CompGrp3TargetFFTabledvm = CompGrp3.DefaultViewManager;
                DataView CompGrp3TargetFFDv = CompGrp3TargetFFTabledvm.CreateDataView(CompGrp2.Tables[4]);
                CompGrp3TargetFFDv.Sort = "ItemID ASC";
                CompGrp3TargetFFTable = CompGrp3TargetFFDv.ToTable();
                var ActFFItemID = ActualSPCommentTable.Columns["ItemID"];
                var ActFFText = ActualSPCommentTable.Columns["CommentText"];
                var TrgFFItemID = CompGrp3TargetCommentTable.Columns["ItemID"];
                var TrgFFText = CompGrp3TargetCommentTable.Columns["Comments"];

                foreach (DataRow RowTbl1 in ActualSPFFTable.Rows)
                {
                    foreach (DataRow RowTbl2 in CompGrp3TargetFFTable.Rows)
                    {
                        if ((RowTbl1[ActFFItemID] != RowTbl2[TrgFFItemID]) || (RowTbl1[ActFFText] != RowTbl2[TrgFFText]))

                        {
                            ffflag = false;
                            WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                            break;
                        }
                    }
                }

            }

            return thirdlevelcomparison && rsflag && nonrsflag && wcflag && ffflag && bbflag;
        }
        /// <summary>
        /// Comparing the results of Insight SP and test SP with only target group
        /// </summary>
        /// <param name="DataCalculation"></param>
        /// <param name="ActualSP"></param>
        /// <param name="TargetGrp"></param>
        /// <returns></returns>
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp)
        {
            bool strflag = true;
            bool oppflag = true;
            int avgORfav = 0;

            DataTable ActualSPStrengthTable;
            DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
            DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPStrengthTable = StrengthDv.ToTable();

            DataTable ActualSPOpportunitiesTable;
            DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
            DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

            DataTable TargetGrpRatingScaleTable;
            DataViewManager TGrpDvm = TargetGrp.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView TGrpDv = TGrpDvm.CreateDataView(TargetGrp.Tables[avgORfav]);
            TGrpDv.Sort = "ObjectID ASC";
            TargetGrpRatingScaleTable = TGrpDv.ToTable();

            if (DataCalculation.ToLower() == "average")
            {
                var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                var TargetItemIDCol = TargetGrpRatingScaleTable.Columns["ObjectID"];

                var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                var TargetGrpMeanCol = TargetGrpRatingScaleTable.Columns["Mean"];
                foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if (RowTbl1[StrAvgScoreCol] != RowTbl2[TargetGrpMeanCol])
                            {
                                strflag = false;
                                break;
                            }
                        }
                    }
                }
                foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if (RowTbl1[OppAvgScoreCol] != RowTbl2[TargetGrpMeanCol])
                            {
                                oppflag = false;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                var TargetItemIDCol = TargetGrpRatingScaleTable.Columns["ObjectID"];

                var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                var StrNeuScoreCol = ActualSPStrengthTable.Columns["NeutralPerc"];
                var StrUnFavScoreCol = ActualSPStrengthTable.Columns["UnFavPerc"];
                var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                var OppNeuScoreCol = ActualSPOpportunitiesTable.Columns["NeutralPerc"];
                var OppUnFavScoreCol = ActualSPOpportunitiesTable.Columns["UnFavPerc"];

                var TargetGrpFavorableCol = TargetGrpRatingScaleTable.Columns["Favorable"];
                var TargetGrpNeutralCol = TargetGrpRatingScaleTable.Columns["Neutral"];
                var TargetGrpUnfavorableCol = TargetGrpRatingScaleTable.Columns["Unfavorable"];
                foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if ((RowTbl1[StrFavScoreCol] != RowTbl2[TargetGrpFavorableCol]) || (RowTbl1[StrNeuScoreCol] != RowTbl2[TargetGrpNeutralCol])
                                || (RowTbl1[StrUnFavScoreCol] != RowTbl2[TargetGrpUnfavorableCol]))
                            {
                                strflag = false;
                                break;
                            }
                        }
                    }
                }
                foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingScaleTable.Rows)
                    {
                        if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if ((RowTbl1[OppFavScoreCol] != RowTbl2[TargetGrpFavorableCol]) || (RowTbl1[OppNeuScoreCol] != RowTbl2[TargetGrpNeutralCol])
                                || (RowTbl1[OppUnFavScoreCol] != RowTbl2[TargetGrpUnfavorableCol]))
                            {
                                oppflag = false;
                                break;
                            }
                        }
                    }
                }
            }

            return strflag && oppflag;
        }
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool firstlevelcomparison = CompareInsightReportResults(DataCalculation, ActualSP, TargetGrp, CompGrp1);
            bool strflag = true;
            bool oppflag = true;
            bool bbflag = true;
            int avgORfav = 0;

            DataTable ActualSPStrengthTable;
            DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
            DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPStrengthTable = StrengthDv.ToTable();

            DataTable ActualSPOpportunitiesTable;
            DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
            DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[0]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

            DataTable CompGrp1RatingScaleTable;
            DataViewManager CompGrp1Dvm = CompGrp1.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView CompGrp1Dv = CompGrp1Dvm.CreateDataView(CompGrp1.Tables[avgORfav]);
            CompGrp1Dv.Sort = "ObjectID ASC";
            CompGrp1RatingScaleTable = CompGrp1Dv.ToTable();
            DataColumnCollection Columns = CompGrp1RatingScaleTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareInsightReportBenchmarkResults(DataCalculation, ActualSP, CompGrp1);

            }
            else
            {
                if (DataCalculation.ToLower() == "average")
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var CompGrp1ItemIDCol = CompGrp1RatingScaleTable.Columns["ObjectID"];

                    var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                    var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                    var CompGrp1MeanCol = CompGrp1RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if (RowTbl1[StrAvgScoreCol] != RowTbl2[CompGrp1MeanCol])
                                {
                                    strflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if (RowTbl1[OppAvgScoreCol] != RowTbl2[CompGrp1MeanCol])
                                {
                                    oppflag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var CompGrp1ItemIDCol = CompGrp1RatingScaleTable.Columns["ObjectID"];

                    var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                    var StrNeuScoreCol = ActualSPStrengthTable.Columns["NeutralPerc"];
                    var StrUnFavScoreCol = ActualSPStrengthTable.Columns["UnFavPerc"];
                    var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                    var OppNeuScoreCol = ActualSPOpportunitiesTable.Columns["NeutralPerc"];
                    var OppUnFavScoreCol = ActualSPOpportunitiesTable.Columns["UnFavPerc"];

                    var CompGrp1FavorableCol = CompGrp1RatingScaleTable.Columns["Favorable"];
                    var CompGrp1NeutralCol = CompGrp1RatingScaleTable.Columns["Neutral"];
                    var CompGrp1UnfavorableCol = CompGrp1RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if ((RowTbl1[StrFavScoreCol] != RowTbl2[CompGrp1FavorableCol]) || (RowTbl1[StrNeuScoreCol] != RowTbl2[CompGrp1NeutralCol])
                                    || (RowTbl1[StrUnFavScoreCol] != RowTbl2[CompGrp1UnfavorableCol]))
                                {
                                    strflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1RatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                if ((RowTbl1[OppFavScoreCol] != RowTbl2[CompGrp1FavorableCol]) || (RowTbl1[OppNeuScoreCol] != RowTbl2[CompGrp1NeutralCol])
                                    || (RowTbl1[OppUnFavScoreCol] != RowTbl2[CompGrp1UnfavorableCol]))
                                {
                                    oppflag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return firstlevelcomparison && strflag && oppflag && bbflag;
        }
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool secondlevelcomparison = CompareInsightReportResults(DataCalculation, ActualSP, TargetGrp, CompGrp1);
            bool strflag = true;
            bool oppflag = true;
            bool bbflag = true;
            int avgORfav = 0;

            DataTable ActualSPStrengthTable;
            DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
            DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPStrengthTable = StrengthDv.ToTable();

            DataTable ActualSPOpportunitiesTable;
            DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
            DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[0]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

            DataTable CompGrp2RatingScaleTable;
            DataViewManager CompGrp2Dvm = CompGrp2.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView CompGrp2Dv = CompGrp2Dvm.CreateDataView(CompGrp2.Tables[avgORfav]);
            CompGrp2Dv.Sort = "ObjectID ASC";
            CompGrp2RatingScaleTable = CompGrp2Dv.ToTable();
            DataColumnCollection Columns = CompGrp2RatingScaleTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareInsightReportBenchmarkResults(DataCalculation, ActualSP, CompGrp2);

            }
            else
            {
                if (DataCalculation.ToLower() == "average")
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var CompGrp2ItemIDCol = CompGrp2RatingScaleTable.Columns["ObjectID"];

                    var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                    var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                    var CompGrp2MeanCol = CompGrp2RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                if (RowTbl1[StrAvgScoreCol] != RowTbl2[CompGrp2MeanCol])
                                {
                                    strflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                if (RowTbl1[OppAvgScoreCol] != RowTbl2[CompGrp2MeanCol])
                                {
                                    oppflag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var CompGrp2ItemIDCol = CompGrp2RatingScaleTable.Columns["ObjectID"];

                    var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                    var StrNeuScoreCol = ActualSPStrengthTable.Columns["NeutralPerc"];
                    var StrUnFavScoreCol = ActualSPStrengthTable.Columns["UnFavPerc"];
                    var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                    var OppNeuScoreCol = ActualSPOpportunitiesTable.Columns["NeutralPerc"];
                    var OppUnFavScoreCol = ActualSPOpportunitiesTable.Columns["UnFavPerc"];

                    var CompGrp2FavorableCol = CompGrp2RatingScaleTable.Columns["Favorable"];
                    var CompGrp2NeutralCol = CompGrp2RatingScaleTable.Columns["Neutral"];
                    var CompGrp2UnfavorableCol = CompGrp2RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                if ((RowTbl1[StrFavScoreCol] != RowTbl2[CompGrp2FavorableCol]) || (RowTbl1[StrNeuScoreCol] != RowTbl2[CompGrp2NeutralCol])
                                    || (RowTbl1[StrUnFavScoreCol] != RowTbl2[CompGrp2UnfavorableCol]))
                                {
                                    strflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2RatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                if ((RowTbl1[OppFavScoreCol] != RowTbl2[CompGrp2FavorableCol]) || (RowTbl1[OppNeuScoreCol] != RowTbl2[CompGrp2NeutralCol])
                                    || (RowTbl1[OppUnFavScoreCol] != RowTbl2[CompGrp2UnfavorableCol]))
                                {
                                    oppflag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return secondlevelcomparison && strflag && oppflag && bbflag;
        }
        public static bool CompareInsightReportResults(string DataCalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool thirdlevelcomparison = CompareInsightReportResults(DataCalculation, ActualSP, TargetGrp, CompGrp1, CompGrp2);
            bool strflag = true;
            bool oppflag = true;
            bool bbflag = true;
            int avgORfav = 0;

            DataTable ActualSPStrengthTable;
            DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
            DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPStrengthTable = StrengthDv.ToTable();

            DataTable ActualSPOpportunitiesTable;
            DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
            DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[0]);
            StrengthDv.Sort = "objectId ASC";
            ActualSPOpportunitiesTable = OpportunitiesDv.ToTable();

            DataTable CompGrp3RatingScaleTable;
            DataViewManager CompGrp3Dvm = CompGrp3.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView CompGrp3Dv = CompGrp3Dvm.CreateDataView(CompGrp3.Tables[avgORfav]);
            CompGrp3Dv.Sort = "ObjectID ASC";
            CompGrp3RatingScaleTable = CompGrp3Dv.ToTable();
            DataColumnCollection Columns = CompGrp3RatingScaleTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareInsightReportBenchmarkResults(DataCalculation, ActualSP, CompGrp3);

            }
            else
            {
                if (DataCalculation.ToLower() == "average")
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var CompGrp3ItemIDCol = CompGrp3RatingScaleTable.Columns["ObjectID"];

                    var StrAvgScoreCol = ActualSPStrengthTable.Columns["MeanScore"];
                    var OppAvgScoreCol = ActualSPOpportunitiesTable.Columns["MeanScore"];
                    var CompGrp3MeanCol = CompGrp3RatingScaleTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                            {
                                if (RowTbl1[StrAvgScoreCol] != RowTbl2[CompGrp3MeanCol])
                                {
                                    strflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                            {
                                if (RowTbl1[OppAvgScoreCol] != RowTbl2[CompGrp3MeanCol])
                                {
                                    oppflag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var StrObjectIDCol = ActualSPStrengthTable.Columns["objectId"];
                    var OppObjectIDCol = ActualSPOpportunitiesTable.Columns["objectId"];
                    var CompGrp3ItemIDCol = CompGrp3RatingScaleTable.Columns["ObjectID"];

                    var StrFavScoreCol = ActualSPStrengthTable.Columns["FavPerc"];
                    var StrNeuScoreCol = ActualSPStrengthTable.Columns["NeutralPerc"];
                    var StrUnFavScoreCol = ActualSPStrengthTable.Columns["UnFavPerc"];
                    var OppFavScoreCol = ActualSPOpportunitiesTable.Columns["FavPerc"];
                    var OppNeuScoreCol = ActualSPOpportunitiesTable.Columns["NeutralPerc"];
                    var OppUnFavScoreCol = ActualSPOpportunitiesTable.Columns["UnFavPerc"];

                    var CompGrp3FavorableCol = CompGrp3RatingScaleTable.Columns["Favorable"];
                    var CompGrp3NeutralCol = CompGrp3RatingScaleTable.Columns["Neutral"];
                    var CompGrp3UnfavorableCol = CompGrp3RatingScaleTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPStrengthTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if (RowTbl1[StrObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                            {
                                if ((RowTbl1[StrFavScoreCol] != RowTbl2[CompGrp3FavorableCol]) || (RowTbl1[StrNeuScoreCol] != RowTbl2[CompGrp3NeutralCol])
                                    || (RowTbl1[StrUnFavScoreCol] != RowTbl2[CompGrp3UnfavorableCol]))
                                {
                                    strflag = false;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow RowTbl1 in ActualSPOpportunitiesTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3RatingScaleTable.Rows)
                        {
                            if (RowTbl1[OppObjectIDCol].ToString() == RowTbl2[CompGrp3ItemIDCol].ToString())
                            {
                                if ((RowTbl1[OppFavScoreCol] != RowTbl2[CompGrp3FavorableCol]) || (RowTbl1[OppNeuScoreCol] != RowTbl2[CompGrp3NeutralCol])
                                    || (RowTbl1[OppUnFavScoreCol] != RowTbl2[CompGrp3UnfavorableCol]))
                                {
                                    oppflag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return thirdlevelcomparison && strflag && oppflag && bbflag;
        }
        /// <summary>
        /// Comparing the results of Drill Down SP and test SP with only target group
        /// </summary>
        /// <param name="DataCalculation"></param>
        /// <param name="ActualSP"></param>
        /// <param name="TargetGrp"></param>
        /// <returns></returns>
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp)
        {
            bool ddflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable TargetGrpDrillDownTable;
            DataViewManager TGrpDvm = TargetGrp.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView TGrpDv = TGrpDvm.CreateDataView(TargetGrp.Tables[avgORfav]);
            TGrpDv.Sort = "ObjectID ASC";
            TargetGrpDrillDownTable = TGrpDv.ToTable();

            if (DataCalculation.ToLower() == "average")
            {
                var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                var TargetItemIDCol = TargetGrpDrillDownTable.Columns["ObjectID"];

                var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];
                var TargetGrpMeanCol = TargetGrpDrillDownTable.Columns["Mean"];
                foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDrillDownTable.Rows)
                    {
                        if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if (RowTbl1[DrillDownAvgScoreCol] != RowTbl2[TargetGrpMeanCol])
                            {
                                ddflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                var TargetItemIDCol = TargetGrpDrillDownTable.Columns["ObjectID"];

                var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                var DrillDownNeuScoreCol = ActualSPDrillDownTable.Columns["NeutralPercent"];
                var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                var TargetGrpFavorableCol = TargetGrpDrillDownTable.Columns["Favorable"];
                var TargetGrpNeutralCol = TargetGrpDrillDownTable.Columns["Neutral"];
                var TargetGrpUnfavorableCol = TargetGrpDrillDownTable.Columns["Unfavorable"];
                foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDrillDownTable.Rows)
                    {
                        if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[TargetItemIDCol].ToString())
                        {
                            if ((RowTbl1[DrillDownFavScoreCol] != RowTbl2[TargetGrpFavorableCol]) || (RowTbl1[DrillDownNeuScoreCol] != RowTbl2[TargetGrpNeutralCol])
                                || (RowTbl1[DrillDownUnFavScoreCol] != RowTbl2[TargetGrpUnfavorableCol]))
                            {
                                ddflag = false;
                                WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                break;
                            }
                        }
                    }
                }
            }
            return ddflag;
        }
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool firstlevelcomparison = CompareDrillDownReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp);
            bool ddflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable CompGrp1DrillDownTable;
            DataViewManager CompGrp1Dvm = CompGrp1.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView CompGrp1Dv = CompGrp1Dvm.CreateDataView(CompGrp1.Tables[avgORfav]);
            CompGrp1Dv.Sort = "ObjectID ASC";
            CompGrp1DrillDownTable = CompGrp1Dv.ToTable();
            DataColumnCollection Columns = CompGrp1DrillDownTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp1);

            }
            else
            {
                if (DataCalculation.ToLower() == "average")
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp1DrillDownTable.Columns["ObjectID"];

                    var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];
                    var DrillDownAvgDiffScoreCol = ActualSPDrillDownTable.Columns["Comp1DiffAverageScore"];
                    var CompGrp1MeanCol = CompGrp1DrillDownTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                var Diff_Comp1 = (Convert.ToInt32(DrillDownAvgScoreCol) - Convert.ToInt32(CompGrp1MeanCol));
                                if (Convert.ToInt32(RowTbl1[DrillDownAvgScoreCol]) != Diff_Comp1)
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp1DrillDownTable.Columns["ObjectID"];

                    var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                    var DrillDownDiffFavScoreCol = ActualSPDrillDownTable.Columns["Comp1DiffFavPercent"];
                    // var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                    var CompGrp1FavorableCol = CompGrp1DrillDownTable.Columns["Favorable"];
                    var CompGrp1NeutralCol = CompGrp1DrillDownTable.Columns["Neutral"];
                    var CompGrp1UnfavorableCol = CompGrp1DrillDownTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp1DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                var Diff_comp1 = (Convert.ToInt32(DrillDownFavScoreCol) - Convert.ToInt32(CompGrp1FavorableCol));
                                if (Convert.ToInt32(RowTbl1[DrillDownDiffFavScoreCol]) != Diff_comp1)
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return firstlevelcomparison && ddflag && bbflag;
        }
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool secondlevelcomparison = CompareDrillDownReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1);
            bool ddflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable CompGrp2DrillDownTable;
            DataViewManager CompGrp2Dvm = CompGrp2.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView CompGrp2Dv = CompGrp2Dvm.CreateDataView(CompGrp2.Tables[avgORfav]);
            CompGrp2Dv.Sort = "ObjectID ASC";
            CompGrp2DrillDownTable = CompGrp2Dv.ToTable();
            DataColumnCollection Columns = CompGrp2DrillDownTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp2);

            }
            else
            {

                if (DataCalculation.ToLower() == "average")
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp2ItemIDCol = CompGrp2DrillDownTable.Columns["ObjectID"];

                    var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];
                    var DrillDownAvgDiffScoreCol = ActualSPDrillDownTable.Columns["Comp2DiffAverageScore"];
                    var CompGrp2MeanCol = CompGrp2DrillDownTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                var Diff_Comp2 = (Convert.ToInt32(DrillDownAvgScoreCol) - Convert.ToInt32(CompGrp2MeanCol));
                                if (Convert.ToInt32(RowTbl1[DrillDownAvgDiffScoreCol]) != Diff_Comp2)
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp2ItemIDCol = CompGrp2DrillDownTable.Columns["ObjectID"];

                    var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                    var DrillDownDiffFavScoreCol = ActualSPDrillDownTable.Columns["Comp2DiffFavPercent"];
                    var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                    var CompGrp2FavorableCol = CompGrp2DrillDownTable.Columns["Favorable"];
                    var CompGrp2NeutralCol = CompGrp2DrillDownTable.Columns["Neutral"];
                    var CompGrp2UnfavorableCol = CompGrp2DrillDownTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp2DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp2ItemIDCol].ToString())
                            {
                                var Diff_comp2 = (Convert.ToInt32(DrillDownFavScoreCol) - Convert.ToInt32(CompGrp2FavorableCol));
                                if (Convert.ToInt32(RowTbl1[DrillDownDiffFavScoreCol]) != Diff_comp2)
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return secondlevelcomparison && ddflag && bbflag;
        }
        public static bool CompareDrillDownReportResults(string DataCalculation, int UserAnalyzeFilterId, string ReportName, DBConnectionStringDTO DBConnectionParameters, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool thirdlevelcomparison = CompareDrillDownReportResults(DataCalculation, UserAnalyzeFilterId, ReportName, DBConnectionParameters, ActualSP, TargetGrp, CompGrp1, CompGrp2);
            bool ddflag = true;
            bool bbflag = true;
            int avgORfav = 0;
            string ColumnName;

            DataColumnCollection columns = ActualSP.Tables[0].Columns;
            if (columns.Contains("SurveyFormItemID"))
                ColumnName = "SurveyFormItemID";
            else
                ColumnName = "CategoryID";

            DataTable ActualSPDrillDownTable;
            DataViewManager DrillDownDvm = ActualSP.DefaultViewManager;
            DataView DrillDownDv = DrillDownDvm.CreateDataView(ActualSP.Tables[0]);
            DrillDownDv.Sort = ColumnName + " ASC";
            ActualSPDrillDownTable = DrillDownDv.ToTable();

            DataTable CompGrp3DrillDownTable;
            DataViewManager CompGrp3Dvm = CompGrp3.DefaultViewManager;
            if (DataCalculation.ToLower() == "average")
                avgORfav = 1;
            else
                avgORfav = 0;
            DataView CompGrp3Dv = CompGrp3Dvm.CreateDataView(CompGrp3.Tables[avgORfav]);
            CompGrp3Dv.Sort = "ObjectID ASC";
            CompGrp3DrillDownTable = CompGrp3Dv.ToTable();
            DataColumnCollection Columns = CompGrp3DrillDownTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareResultReportBenchmarkResults(DataCalculation, ActualSP, CompGrp3);

            }
            else
            {

                if (DataCalculation.ToLower() == "average")
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp3DrillDownTable.Columns["ObjectID"];
                    var DrillDownAvgDiffScoreCol = ActualSPDrillDownTable.Columns["Comp3DiffAverageScore"];
                    var DrillDownAvgScoreCol = ActualSPDrillDownTable.Columns["AverageScore"];

                    var CompGr3MeanCol = CompGrp3DrillDownTable.Columns["Mean"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                var Diff_Comp2 = (Convert.ToInt32(DrillDownAvgScoreCol) - Convert.ToInt32(CompGr3MeanCol));
                                if (Convert.ToInt32(RowTbl1[DrillDownAvgDiffScoreCol]) != Diff_Comp2)
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var DrillDownObjectIDCol = ActualSPDrillDownTable.Columns[ColumnName];
                    var CompGrp1ItemIDCol = CompGrp3DrillDownTable.Columns["ObjectID"];

                    var DrillDownFavScoreCol = ActualSPDrillDownTable.Columns["FavPercent"];
                    var DrillDownDiffFavScoreCol = ActualSPDrillDownTable.Columns["Comp3DiffFavPercent"];
                    var DrillDownUnFavScoreCol = ActualSPDrillDownTable.Columns["UnFavPercent"];

                    var CompGrp3FavorableCol = CompGrp3DrillDownTable.Columns["Favorable"];
                    var CompGrp3NeutralCol = CompGrp3DrillDownTable.Columns["Neutral"];
                    var CompGrp3UnfavorableCol = CompGrp3DrillDownTable.Columns["Unfavorable"];
                    foreach (DataRow RowTbl1 in ActualSPDrillDownTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in CompGrp3DrillDownTable.Rows)
                        {
                            if (RowTbl1[DrillDownObjectIDCol].ToString() == RowTbl2[CompGrp1ItemIDCol].ToString())
                            {
                                var Diff_comp2 = (Convert.ToInt32(DrillDownFavScoreCol) - Convert.ToInt32(CompGrp3FavorableCol));
                                if (Convert.ToInt32(RowTbl1[DrillDownDiffFavScoreCol]) != Diff_comp2)
                                {
                                    ddflag = false;
                                    WriteFailedData(DataCalculation, ReportName, UserAnalyzeFilterId, DBConnectionParameters);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return thirdlevelcomparison && ddflag && bbflag;
        }

        /// <summary>
        /// Comparing the results of trend Sp and Test Sp with only target group
        /// </summary>
        /// <param name  ="DataCalculation"></param>
        /// <param name  ="ActualSP"></param>
        /// <param name  ="TargetGrp"></param>
        /// <returns></returns>

        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp)
        {
            try
            {
                bool RSFlag = true;

                DataTable ActualSPRatingTrendTable;
                DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                ActualDv.Sort = "GroupBy ASC";
                ActualSPRatingTrendTable = ActualDv.ToTable();

                DataTable TargetGrpRatingTrendTable;
                DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
                DataView TargetGrpDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
                TargetGrpDv.Sort = "GroupBy ASC";
                TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                if (Datacalculation.ToLower() == "average")
                {
                    var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["MeanScore"];
                    var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["TotalDistinctRespCount"];
                    var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                            {
                                if (RowTbl1[TrendOverallMeanCol] != RowTbl2[TargetOverallMeanCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }
                        }
                    }

                }
                else
                {
                    var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    var TrendFavCol = ActualSPRatingTrendTable.Columns["FavPerc"];
                    var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["TotalDistinctRespCount"];
                    var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                            {
                                if (RowTbl1[TrendFavCol] != RowTbl2[TargetFavCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }

                        }

                    }
                }
                return RSFlag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            try
            {
                bool FirstLevelComparison = CompareRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp1.Tables[0].Columns;
                
                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp1);
                }
                else
                {
                    DataTable ActualSPRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();

                    DataTable TargetGrpRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
                    DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
                    TargetGrpDv.Sort = "GroupBy ASC";
                    TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    if (Datacalculation.ToLower() == "average")
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["Comp_One_Mean"];
                        var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_One_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendOverallMeanCol] != RowTbl2[TargetOverallMeanCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                    {
                                        RSFlag = false;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendFavCol = ActualSPRatingTrendTable.Columns["Comp_One_FavPerc"];
                        var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_One_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendFavCol] != RowTbl2[TargetFavCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                    {
                                        RSFlag = false;
                                        break;
                                    }
                                }

                            }

                        }
                    }
                }

                return FirstLevelComparison && RSFlag & bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            try
            {
                bool SecondLevelComparison = CompareRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp, CompGrp1);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp2.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp2);
                }
                else
                {
                    DataTable ActualSPRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();

                    DataTable TargetGrpRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
                    DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
                    TargetGrpDv.Sort = "GroupBy ASC";
                    TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    if (Datacalculation.ToLower() == "average")
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["Comp_Two_Mean"];
                        var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Two_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendOverallMeanCol] != RowTbl2[TargetOverallMeanCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                    {
                                        RSFlag = false;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendFavCol = ActualSPRatingTrendTable.Columns["Comp_Two_FavPerc"];
                        var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Two_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendFavCol] != RowTbl2[TargetFavCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                    {
                                        RSFlag = false;
                                        break;
                                    }
                                }

                            }

                        }
                    }
                }
                return SecondLevelComparison && RSFlag && bbflag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CompareRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            try
            {
                bool ThirdLevelComparison = CompareRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp, CompGrp1, CompGrp2);
                bool RSFlag = true;
                bool bbflag = true;

                DataColumnCollection Columns = CompGrp3.Tables[0].Columns;

                if (Columns.Contains("BenchmarkFavPerc") == true)
                {
                    bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp3);
                }
                else
                {
                    DataTable ActualSPRatingTrendTable;
                    DataViewManager Actualdvm = ActualSP.DefaultViewManager;
                    DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
                    ActualDv.Sort = "GroupBy ASC";
                    ActualSPRatingTrendTable = ActualDv.ToTable();

                    DataTable TargetGrpRatingTrendTable;
                    DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
                    DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
                    TargetGrpDv.Sort = "GroupBy ASC";
                    TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

                    if (Datacalculation.ToLower() == "average")
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendOverallMeanCol = ActualSPRatingTrendTable.Columns["Comp_Three_Mean"];
                        var TargetOverallMeanCol = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Three_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];

                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendOverallMeanCol] != RowTbl2[TargetOverallMeanCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                    {
                                        RSFlag = false;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        var TrendGroupByCol = ActualSPRatingTrendTable.Columns["GroupBy"];
                        var TargetGroupByCol = TargetGrpRatingTrendTable.Columns["GroupBy"];

                        var TrendFavCol = ActualSPRatingTrendTable.Columns["Comp_Three_FavPerc"];
                        var TargetFavCol = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                        var TrendResponseCountCol = ActualSPRatingTrendTable.Columns["Comp_Three_TotalDistinctRespCount"];
                        var TargetResponseCountCol = TargetGrpRatingTrendTable.Columns["Responses"];
                        foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                        {
                            foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                            {
                                if (RowTbl1[TrendGroupByCol].ToString() == RowTbl2[TargetGroupByCol].ToString())
                                {
                                    if (RowTbl1[TrendFavCol] != RowTbl2[TargetFavCol] || RowTbl1[TrendResponseCountCol] != RowTbl2[TargetResponseCountCol])
                                    {
                                        RSFlag = false;
                                        break;
                                    }
                                }

                            }

                        }
                    }
                }
                return ThirdLevelComparison && RSFlag;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp)
        {
            bool NRFlag = true;
            String ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
            DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
            ActualSPNonRatingTrendTable = ActualDV.ToTable();

            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["MeanScore"];
            var TrendTotalRespCount = ActualSPNonRatingTrendTable.Columns["TotalRespCount"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];
            var TargetGrpTotalRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol] || RowTbl1[TrendTotalRespCount] != RowTbl2[TargetGrpTotalRespCount])
                        {
                            NRFlag = false;
                            break;
                        }
                    }
                }
            }
            return NRFlag;
        }


        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool NRFlag = true;
            bool FirstLevelComparison = CompareNonRatingTrendReportResult(ActualSP, TargetGrp);
            string ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
            DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
            ActualSPNonRatingTrendTable = ActualDV.ToTable();

            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["MeanScore"];
            var TrendTotalRespCount = ActualSPNonRatingTrendTable.Columns["TotalRespCount"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];
            var TargetGrpTotalRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol] || RowTbl1[TrendTotalRespCount] != RowTbl2[TargetGrpTotalRespCount])
                        {
                            NRFlag = false;
                            break;
                        }
                    }
                }
            }
            return NRFlag && FirstLevelComparison;
        }


        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool NRFlag = true;
            bool SecondLevelComparison = CompareNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1);
            string ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
            DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
            ActualSPNonRatingTrendTable = ActualDV.ToTable();

            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["MeanScore"];
            var TrendTotalRespCount = ActualSPNonRatingTrendTable.Columns["TotalRespCount"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];
            var TargetGrpTotalRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol] || RowTbl1[TrendTotalRespCount] != RowTbl2[TargetGrpTotalRespCount])
                        {
                            NRFlag = false;
                            break;
                        }
                    }
                }
            }
            return NRFlag && SecondLevelComparison;
        }


        public static bool CompareNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool NRFlag = true;
            bool ThirdLevelComparison = CompareNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1, CompGrp2);
            string ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = CompGrp3.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
            DataView ActualDV = ActualSPDVM.CreateDataView(ActualSP.Tables[1]);
            ActualSPNonRatingTrendTable = ActualDV.ToTable();

            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ScaleOptionId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["MeanScore"];
            var TrendTotalRespCount = ActualSPNonRatingTrendTable.Columns["TotalRespCount"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];
            var TargetGrpTotalRespCount = TargetGrpSPRatingTrendTable.Columns["ScaleResponses"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol] || RowTbl1[TrendTotalRespCount] != RowTbl2[TargetGrpTotalRespCount])
                        {
                            NRFlag = false;
                            break;
                        }
                    }
                }
            }
            return NRFlag && ThirdLevelComparison;
        }


        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp)
        {
            bool RSFlag = true;

            DataTable ActualSPRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(4)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPRatingTrendTable = ActualDv.ToTable();

            DataTable TargetGrpRatingTrendTable;
            DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
            DataView TargetGrpDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
            TargetGrpDv.Sort = "GroupBy ASC";
            TargetGrpRatingTrendTable = TargetGrpDv.ToTable();

            if (Datacalculation.ToLower() == "average")
            {
                var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    {
                        if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                        {
                            if (RowTbl1[ActualSPAverageValue] != RowTbl2[TargetGrpGroupBy])
                            {
                                RSFlag = false;
                                break;
                            }
                        }
                    }
                }


            }
            else
            {
                var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                    {
                        if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                        {
                            if (RowTbl1[ActualSPPercentageValue] != RowTbl2[TargetGrpPerCentageValue])
                            {
                                RSFlag = false;
                                break;
                            }
                        }
                    }
                }
            }

            return RSFlag;

        }

        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool FirstLevelComparison = CompareHeapRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp);
            bool RSFlag = true;
            bool bbflag = true;

            DataTable ActualSPRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(4)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPRatingTrendTable = ActualDv.ToTable();

            DataTable TargetGrpRatingTrendTable;
            DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
            DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
            TargetGrpDv.Sort = "GroupBy ASC";
            TargetGrpRatingTrendTable = TargetGrpDv.ToTable();
            DataColumnCollection Columns = TargetGrpRatingTrendTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp1);

            }
            else
            {

                if (Datacalculation.ToLower() == "average")
                {
                    var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (RowTbl1[ActualSPAverageValue] != RowTbl2[TargetGrpGroupBy])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }
                        }
                    }


                }
                else
                {
                    var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (RowTbl1[ActualSPPercentageValue] != RowTbl2[TargetGrpPerCentageValue])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return RSFlag && FirstLevelComparison && bbflag;

        }

        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool SecondLevelComparison = CompareHeapRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp, CompGrp1);
            bool RSFlag = true;
            bool bbflag = true;
            DataTable ActualSPRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(4)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPRatingTrendTable = ActualDv.ToTable();

            DataTable TargetGrpRatingTrendTable;
            DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
            DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
            TargetGrpDv.Sort = "GroupBy ASC";
            TargetGrpRatingTrendTable = TargetGrpDv.ToTable();
            DataColumnCollection Columns = TargetGrpRatingTrendTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp2);

            }
            else
            {
                if (Datacalculation.ToLower() == "average")
                {
                    var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (RowTbl1[ActualSPAverageValue] != RowTbl2[TargetGrpGroupBy])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }
                        }
                    }


                }
                else
                {
                    var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (RowTbl1[ActualSPPercentageValue] != RowTbl2[TargetGrpPerCentageValue])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return RSFlag && SecondLevelComparison && bbflag;

        }

        public static bool CompareHeapRatingTrendReportResult(string Datacalculation, DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool ThirdLevelComparison = CompareHeapRatingTrendReportResult(Datacalculation, ActualSP, TargetGrp, CompGrp1, CompGrp2);
            bool RSFlag = true;
            bool bbflag = true;

            DataTable ActualSPRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(4)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPRatingTrendTable = ActualDv.ToTable();

            DataTable TargetGrpRatingTrendTable;
            DataViewManager Targetdvm = CompGrp3.DefaultViewManager;
            DataView TargetGrpDv = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
            TargetGrpDv.Sort = "GroupBy ASC";
            TargetGrpRatingTrendTable = TargetGrpDv.ToTable();
            DataColumnCollection Columns = TargetGrpRatingTrendTable.Columns;
            if (Columns.Contains("Benchmark") == true)
            {
                bbflag = CompareTrendReportBenchmarkResult(Datacalculation, ActualSP, CompGrp2);

            }
            else
            {
                if (Datacalculation.ToLower() == "average")
                {
                    var ActualSPAverageValue = ActualSPRatingTrendTable.Columns["Data2"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpAverageValue = TargetGrpRatingTrendTable.Columns["Overall Mean"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (RowTbl1[ActualSPAverageValue] != RowTbl2[TargetGrpGroupBy])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }
                        }
                    }


                }
                else
                {
                    var ActualSPPercentageValue = ActualSPRatingTrendTable.Columns["Data1"];
                    var ActualSPGroupBy = ActualSPRatingTrendTable.Columns["GroupBy"];
                    var TargetGrpPerCentageValue = TargetGrpRatingTrendTable.Columns["Overall Favorability"];
                    var TargetGrpGroupBy = TargetGrpRatingTrendTable.Columns["GroupBy"];

                    foreach (DataRow RowTbl1 in ActualSPRatingTrendTable.Rows)
                    {
                        foreach (DataRow RowTbl2 in TargetGrpRatingTrendTable.Rows)
                        {
                            if ((RowTbl1[ActualSPGroupBy].ToString()) == (RowTbl2[TargetGrpGroupBy].ToString()))
                            {
                                if (RowTbl1[ActualSPPercentageValue] != RowTbl2[TargetGrpPerCentageValue])
                                {
                                    RSFlag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return RSFlag && ThirdLevelComparison && bbflag;

        }

        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp)
        {
            bool NRFlag = true;

            String ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = TargetGrp.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(TargetGrp.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(5)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPNonRatingTrendTable = ActualDv.ToTable();


            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol])
                        {
                            NRFlag = false;
                            break;

                        }
                    }
                }
            }

            return NRFlag;

        }

        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1)
        {
            bool NRFlag = true;
            bool FirstLevelComparison = CompareHeapNonRatingTrendReportResult(ActualSP, TargetGrp);
            String ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = CompGrp1.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(CompGrp1.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(5)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPNonRatingTrendTable = ActualDv.ToTable();

            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol])
                        {
                            NRFlag = false;
                            break;

                        }
                    }
                }
            }

            return NRFlag && FirstLevelComparison;

        }
        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2)
        {
            bool NRFlag = true;
            bool SecondLevelComparison = CompareHeapNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1);
            String ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = CompGrp2.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(CompGrp2.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(5)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPNonRatingTrendTable = ActualDv.ToTable();


            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol])
                        {
                            NRFlag = false;
                            break;

                        }
                    }
                }
            }

            return NRFlag && SecondLevelComparison;

        }
        public static bool CompareHeapNonRatingTrendReportResult(DataSet ActualSP, DataSet TargetGrp, DataSet CompGrp1, DataSet CompGrp2, DataSet CompGrp3)
        {
            bool NRFlag = true;
            bool thirdLevelComparison = CompareHeapNonRatingTrendReportResult(ActualSP, TargetGrp, CompGrp1, CompGrp2);
            String ScaleOption = "";
            DataTable TargetGrpSPRatingTrendTable;
            DataViewManager Targetdvm = CompGrp3.DefaultViewManager;
            DataView TargetDv = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
            TargetDv.Sort = "GroupBy DESC";
            var RecentGroup = TargetDv.ToTable().Rows[0][0].ToString();
            DataView ActualTop5 = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
            ActualTop5.RowFilter = "GroupBy=" + RecentGroup;
            ActualTop5.Sort = "ScaleResponse DESC";
            var Top5ScaleOption = ActualTop5.ToTable().Select("Select Top 5 ScaleOptionID");
            foreach (var row in Top5ScaleOption)
            {
                var ScaleoptionID = row[2];
                ScaleOption = ScaleoptionID + ",";
            }
            DataView TargetGroupDV = Targetdvm.CreateDataView(CompGrp3.Tables[0]);
            TargetGroupDV.RowFilter = "ScaleoptionID IN (" + ScaleOption.TrimEnd(',') + ")";
            TargetGrpSPRatingTrendTable = TargetGroupDV.ToTable();

            DataTable ActualSPNonRatingTrendTable;
            DataViewManager Actualdvm = ActualSP.DefaultViewManager;
            DataView ActualDv = Actualdvm.CreateDataView(ActualSP.Tables[1]);
            ActualDv.RowFilter = "ObjectTypeId IN(5)";
            ActualDv.RowFilter = "GroupBY NOT IN(All)";
            ActualDv.Sort = "GroupBy ASC";
            ActualSPNonRatingTrendTable = ActualDv.ToTable();


            var TrendNRGroupByCol = ActualSPNonRatingTrendTable.Columns["GroupBy"];
            var TrendScaleOptionCol = ActualSPNonRatingTrendTable.Columns["ObjectId"];
            var TrendMeanScoreCol = ActualSPNonRatingTrendTable.Columns["Data"];

            var TargetGrpNRGroupByCol = TargetGrpSPRatingTrendTable.Columns["GroupBy"];
            var TargetGrpScaleOptionCol = TargetGrpSPRatingTrendTable.Columns["ScaleOptionID"];
            var TargetGrpMeanScoreCol = TargetGrpSPRatingTrendTable.Columns["ResponseRate"];

            foreach (DataRow RowTbl1 in ActualSPNonRatingTrendTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpSPRatingTrendTable.Rows)
                {
                    if ((RowTbl1[TrendNRGroupByCol].ToString() == RowTbl2[TargetGrpNRGroupByCol].ToString()) && (RowTbl1[TrendScaleOptionCol].ToString() == RowTbl2[TargetGrpScaleOptionCol].ToString()))
                    {
                        if (RowTbl1[TrendMeanScoreCol] != RowTbl2[TargetGrpMeanScoreCol])
                        {
                            NRFlag = false;
                            break;

                        }
                    }
                }
            }

            return NRFlag && thirdLevelComparison;

        }
        public static bool CompareDemographicReportResults_category(string DataCalculation, DataSet ActualSP, DataSet TargetGrp)
        {
            bool ddflag = true;
            DataTable ActualSPDemographicTable;
            DataViewManager DemographicDVM = ActualSP.DefaultViewManager;
            DataView DemographicDV = DemographicDVM.CreateDataView(ActualSP.Tables[1]);
            DemographicDV.RowFilter = "objectTypeId in(3)";
            DemographicDV.Sort = "ObjectID, Demovalue ASC";
            ActualSPDemographicTable = DemographicDV.ToTable();


            DataTable TargetGrpDemographicTable;
            DataViewManager TGrpDemographic = TargetGrp.DefaultViewManager;
            DataView TGrpDemo = TGrpDemographic.CreateDataView(TargetGrp.Tables[0]);
            TGrpDemo.Sort = "CategoryID ASC";
            TargetGrpDemographicTable = TGrpDemo.ToTable();

            if (DataCalculation.ToLower() == "average")

            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["ObjectID"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["CategoryID"];

                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data2"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Mean"];
                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {
                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString())
                        {
                            if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                            {
                                ddflag = false;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["ObjectID"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["CategoryID"];

                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data1"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Favorable"];

                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {
                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString())
                        {
                            if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                            {
                                ddflag = false;
                                break;
                            }
                        }
                    }
                }
            }

            return ddflag;
        }

        public static bool CompareDemographicReportResults_questions(string DataCalculation, DataSet ActualSP, DataSet TargetGrp)
        {
            bool ddflag = true;
            DataTable ActualSPDemographicTable;
            DataViewManager DemographicDVM = ActualSP.DefaultViewManager;
            DataView DemographicDV = DemographicDVM.CreateDataView(ActualSP.Tables[1]);
            DemographicDV.RowFilter = "objectTypeId in(4)";
            DemographicDV.Sort = "ObjectID, Demovalue ASC";
            ActualSPDemographicTable = DemographicDV.ToTable();


            DataTable TargetGrpDemographicTable;
            DataViewManager TGrpDemographic = TargetGrp.DefaultViewManager;
            DataView TGrpDemo = TGrpDemographic.CreateDataView(TargetGrp.Tables[0]);
            TGrpDemo.Sort = "QuestionID ASC";
            TargetGrpDemographicTable = TGrpDemo.ToTable();

            if (DataCalculation.ToLower() == "average")

            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["ObjectID"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["QuestionID"];

                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data2"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Mean"];
                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {
                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString())
                        {
                            if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                            {
                                ddflag = false;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                var DemographicDownObjectIDCol = ActualSPDemographicTable.Columns["ObjectID"];
                var TargetdemopercentageIDCol = TargetGrpDemographicTable.Columns["QuestionID"];

                var DemographicpercentagevalueCol = ActualSPDemographicTable.Columns["Data1"];
                var TargetGrpdemographicpercentagevalueCol = TargetGrpDemographicTable.Columns["Favorable"];

                foreach (DataRow RowTbl1 in ActualSPDemographicTable.Rows)
                {
                    foreach (DataRow RowTbl2 in TargetGrpDemographicTable.Rows)
                    {
                        if (RowTbl1[DemographicDownObjectIDCol].ToString() == RowTbl2[TargetdemopercentageIDCol].ToString())
                        {
                            if (RowTbl1[DemographicpercentagevalueCol] != RowTbl2[TargetGrpdemographicpercentagevalueCol])
                            {
                                ddflag = false;
                                break;
                            }
                        }
                    }
                }
            }

            return ddflag;
        }
        public static bool SwitchtocommentComparsion(DataSet surveyform, DBConnectionStringDTO DBConnectionParameters, string CategoryText, DataSet TargertGrp, DataSet ActualSp)
        {

            bool ddflag = true;

            string SurveyFormID = surveyform.Tables[0].Rows[0][0].ToString() + ", ";
            string query = "SELECT COUNT(*) FROM DimItem_V11 WHERE SurveyFormID = '" + SurveyFormID + "' and CategoryText = '" + CategoryText + "' AND GroupName = 'RatingScale'";
            DataSet rscount = DBOperations.ExecuteSPwithInputParameters(query, DBConnectionParameters);
            if (rscount.Tables[0].Rows[0][0].ToString() == "0")
            {
                ddflag = CompareCommentResults_unscoredcategory(ActualSp, TargertGrp);

            }
            else
            {
                ddflag = CompareCommentResults_scoredcategory(ActualSp, TargertGrp);


            }
            return ddflag;
        }
        public static bool CompareCommentResults_unscoredcategory(DataSet ActualSP, DataSet TargetGrp)
        {
            bool unscorecommentsflag = true;

            //unscoredcategory  comments dev output

            DataTable ActualSPCommentUnscoreTable;
            DataViewManager UnsocreCommentDVM = ActualSP.DefaultViewManager;
            DataView UnscoreCommentDV = UnsocreCommentDVM.CreateDataView(ActualSP.Tables[0]);
            UnscoreCommentDV.RowFilter = "RespondentType in(0)";

            ActualSPCommentUnscoreTable = UnscoreCommentDV.ToTable();

            // unscored category comments testsp output	        

            DataTable TargetGrpCommentUnscoreTable;
            DataViewManager TGrpUnscoreComments = TargetGrp.DefaultViewManager;

            DataView TGrpUnscoreCm = TGrpUnscoreComments.CreateDataView(TargetGrp.Tables[0]);
            TargetGrpCommentUnscoreTable = TGrpUnscoreCm.ToTable();

            //defining the columns for comparision 

            var CommentObjectTextCol = ActualSPCommentUnscoreTable.Columns["Comments"];
            var TargetCommentsTextCol = TargetGrpCommentUnscoreTable.Columns["Comments"];

            var itemObjectTextCol = ActualSPCommentUnscoreTable.Columns["ItemText"];
            var TargetitemtextCol = TargetGrpCommentUnscoreTable.Columns["ReportItemText"];


            foreach (DataRow RowTbl1 in ActualSPCommentUnscoreTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpCommentUnscoreTable.Rows)
                {

                    if (RowTbl1[CommentObjectTextCol] != RowTbl2[TargetCommentsTextCol])
                    {
                        unscorecommentsflag = false;
                        break;
                    }
                }

            }


            return unscorecommentsflag;


        }




        public static bool CompareCommentResults_scoredcategory(DataSet ActualSP, DataSet TargetGrp)
        {
            bool abovecommentsflag = true;
            bool belowcommentsflag = true;

            //scored category and above average comments dev output

            DataTable ActualSPCommentAboveAvgTable;
            DataViewManager AboveCommentDVM = ActualSP.DefaultViewManager;
            DataView AboveCommentDV = AboveCommentDVM.CreateDataView(ActualSP.Tables[0]);
            AboveCommentDV.RowFilter = "RespondentType in(1)";

            ActualSPCommentAboveAvgTable = AboveCommentDV.ToTable();

            //scored category and above belowaverage comments dev output

            DataTable ActualSPCommentBelowAvgTable;
            DataViewManager BelowCommentDVM = ActualSP.DefaultViewManager;
            DataView BelowCommentDV = BelowCommentDVM.CreateDataView(ActualSP.Tables[0]);
            BelowCommentDV.RowFilter = "RespondentType in(-1)";

            ActualSPCommentBelowAvgTable = BelowCommentDV.ToTable();

            //scored category and above average comments testsp output	        

            DataTable TargetGrpCommentTable;
            DataViewManager TGrpComments = TargetGrp.DefaultViewManager;

            DataView TGrpCm = TGrpComments.CreateDataView(TargetGrp.Tables[0]);
            TargetGrpCommentTable = TGrpCm.ToTable();

            //scored category and above below comments test output    
            DataTable TargetGrpbelowCommentTable;
            DataViewManager TGrpbelowComments = TargetGrp.DefaultViewManager;

            DataView TGrpbelowCm = TGrpbelowComments.CreateDataView(TargetGrp.Tables[1]);
            TargetGrpbelowCommentTable = TGrpbelowCm.ToTable();

            //defining columns for comparision 


            var CommentObjectTextCol = ActualSPCommentAboveAvgTable.Columns["Comments"];
            var TargetCommentsTextCol = TargetGrpCommentTable.Columns["Comments"];

            var itemObjectTextCol = ActualSPCommentAboveAvgTable.Columns["ItemText"];
            var TargetitemtextCol = TargetGrpCommentTable.Columns["ReportItemText"];


            var BelowCommentObjecttextCol = ActualSPCommentBelowAvgTable.Columns["Comments"];
            var TargetBelowCommentstextCol = TargetGrpbelowCommentTable.Columns["Comments"];

            var itembelowObjecttextCol = ActualSPCommentBelowAvgTable.Columns["ItemText"];
            var TargetitemtextbelowCol = TargetGrpbelowCommentTable.Columns["ReportItemText"];


            //comparision process  for above comments

            foreach (DataRow RowTbl1 in ActualSPCommentAboveAvgTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpCommentTable.Rows)
                {


                    if (RowTbl1[CommentObjectTextCol] != RowTbl2[TargetCommentsTextCol])
                    {
                        abovecommentsflag = false;
                        break;
                    }

                }
            }



            //comparision process  for below comments

            foreach (DataRow RowTbl1 in ActualSPCommentBelowAvgTable.Rows)
            {
                foreach (DataRow RowTbl2 in TargetGrpbelowCommentTable.Rows)
                {

                    if (RowTbl1[BelowCommentObjecttextCol] != RowTbl2[TargetBelowCommentstextCol])
                    {
                        belowcommentsflag = false;
                        break;
                    }

                }
            }

            return abovecommentsflag && belowcommentsflag;


        }


    }
}

