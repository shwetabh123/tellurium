﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class LogicCaseDto
    {
        public string SourceType { get; set; }
        public string SourceItem { get; set; }
        public string SourceCondition { get; set; }
        public string SourceLogic { get; set; }
        public string[] SourceResponses { get; set; }
    }
}
