﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using System.Data;
namespace Common
{
  public   class Spotlightreports
    {

        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;
        private static string FormName;
        public static DBConnectionStringDTO DBConnectionParameters;
        public static string FileName;
        public static string SPColor;




        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }
    public static string Getspotlightsp(string ReportName)
        {

            string TestSPName = "";
            if (ReportName.ToLower() == "dv")
            {
                TestSPName = "exec Usp_DepartureViewReport";
            }

            else if (ReportName.ToLower() == "tr")
            {
                TestSPName = "exec Usp_TotalRewardsReport";

            }
            else if (ReportName.ToLower() == "ld")
            {
                TestSPName = "exec USP_LearningNDevelopmentReport";

            }


            else if (ReportName.ToLower() == "red")
            {
                TestSPName = "exec USP_RED_Spotlight_Report";

            }


            return TestSPName;
           
        }

        public static string  Spotlightfilters(string UserAnalyzeFilterID,string ReportName, DBConnectionStringDTO DBConnectionParameters,string SPColor)

        {
            string spotsp = Getspotlightsp(ReportName) + "_";
            string PPT = "1"+",";
            string UserFilters = "NULL";
             string query = "select *  from Automationspotlight where UserAnalyzeFilterID = " + UserAnalyzeFilterID + "";
    

            DataSet ds = AuthorActions.DBValidationWithQuery_TCESReporting(DBConnectionParameters, query);
            
            string SurveyFormID = ds.Tables[0].Rows[0][3].ToString()  + ", ";
            string ClientID = ds.Tables[0].Rows[0][2].ToString() + ",";
            string UserID = ds.Tables[0].Rows[0][1].ToString() + ",";
           // string Segment1 = ds.Tables[0].Rows[0][4].ToString() + ",";
           // string Segment2 = ds.Tables[0].Rows[0][5].ToString() + ",";
            string Segment1 = ds.Tables[0].Rows[0][4].ToString() ;
            string Segment2 = ds.Tables[0].Rows[0][5].ToString() ;


            string date1 = ds.Tables[0].Rows[0][6].ToString()+",";

            string date2 = ds.Tables[0].Rows[0][7].ToString() + ",";

            

            DateTime tmpDate1 = Convert.ToDateTime(date1, System.Globalization.CultureInfo.InvariantCulture);

            
            DateTime tmpDate2= Convert.ToDateTime(date2, System.Globalization.CultureInfo.InvariantCulture);
            

            string PreviousStartDate = tmpDate1.ToString("yyyy-MM-dd HH:mm:ss");
            string PreviousEndDate = tmpDate2.ToString("yyyy-MM-dd HH:mm:ss");
            

            string InputQuery = spotsp + SPColor + " " + "@UserFilters=default" + "," + "@PreviousStartDate='"+ PreviousStartDate +"'," +
"@PreviousEndDate = '"   + PreviousEndDate + "'," + "@SegmentDemoGraphic = default" + "," + "@UserID=" + UserID + "@ClientID=" + ClientID +
"@Segment1ToWatch = N'" + Segment1 + "'" + "," + "@Segment2ToWatch= N'" + Segment2 + "'" + "," + "@IsPPTNeeded=" + PPT + "@SurveyFormID=" + SurveyFormID +
 "@SegmentDemoGraphicValue = default";




            // DistributeActions.WriteLog(driver, filepath, InputQuery);
            return InputQuery;
            //exec Usp_DepartureViewReport_auto_Green
            // @UserFilters = default,
            //@PreviousStartDate = '2017-01-09 00:00:00',
            //@PreviousEndDate = '2017-11-01 00:00:00',
            //  @SegmentDemoGraphic = default,
            // @SegmentDemoGraphicValue = default,
            // @Segment1ToWatch = N'What is your department ?',
            //  @Segment2ToWatch = N' Organization Name',
            //@IsPPTNeeded = 1 ,
            // @SurveyFormId = 18031,
            //  @ClientId = 16,
            //  @UserId = 2572515





        } 
          
            }
            
        }   
    

