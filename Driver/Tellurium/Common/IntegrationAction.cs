﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class IntegrationAction
    {

        public static void NavigatetoSettings(IWebDriver driver)
        {
            UIActions.clickwithXapth(driver, "//i[@class='shl-icon_arrow_small_down']");
            UIActions.clickwithXapth(driver, "//a[@href='/Pulse/AccountSettings']");
        }

        public static void OpenIntegrationtab(IWebDriver driver)
        {
         
            UIActions.clickwithXapth(driver, "(//*[@id='menu-integration-features'])");
        }
     
    }
}
