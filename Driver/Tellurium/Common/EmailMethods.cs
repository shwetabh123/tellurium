﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using System.Drawing;
using OpenQA.Selenium.Support.UI;

namespace Common
{

    public class EmailMethods
    {
        
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;

        //  public static string distributionName = "Enroute";
        //private string clipboardText;

   //     [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        //Gets driver, create email button element, surveyname, testmethod name, metatag as parameters
        public static IWebDriver CreateEmailThruEditDistribution(RemoteWebDriver driver, String emailTypeEle, String surveyName, String methodName, string metaTag)
        {

            //String pathProject = Environment.CurrentDirectory;
            //String filePath = pathProject + @"\Inputdata\InviteSubject.txt";
            String filePath = @"C:\Tellurium\Dataset\InviteSubject.txt";
            DistributeActions.NavigateToManageDistrbution(driver);
            DistributeActions.SearchAndOpenDistribution(driver, surveyName);


            IWebElement ele = driver.FindElement(By.CssSelector("button[widget-data-event='click::SelectEmailSectionForEdit"));
            Thread.Sleep(3000);
            Actions ac = new Actions(driver);
            ac.MoveToElement(ele).Build().Perform();
            UIActions.ClickWithWait(driver, "button[widget-data-event='click::SelectEmailSectionForEdit']", 1000);
            Thread.Sleep(3000);
            IWebElement delEmailBtn;
            bool isPresent;

            switch (methodName)
            {
                case "AddInvitation":
                    delEmailBtn = driver.FindElement(By.XPath("//div[@class='panel-body invite-section']//i[@widget-data-event='click::RemoveEmail']"));
                    isPresent = DistributeActions.IfElementExists(driver, "//div[@class='panel-body invite-section']//i[@widget-data-event='click::RemoveEmail']");

                    if (isPresent == true)
                        delEmailBtn.Click();
                    break;

                case "AddReminder":

                    delEmailBtn = driver.FindElement(By.XPath("//div[@class='panel-body reminder-section']//i[@widget-data-event='click::RemoveEmail']"));
                    isPresent = DistributeActions.IfElementExists(driver, "//div[@class='panel-body reminder-section']//i[@widget-data-event='click::RemoveEmail']");
                    if (isPresent == true)

                        delEmailBtn.Click();
                    break;

                case "AddCustomEmail":
                    try
                    {
                        delEmailBtn = driver.FindElement(By.XPath("//div[@class='panel-body custom-section']//i[@widget-data-event='click::RemoveEmail']"));
                        isPresent = DistributeActions.IfElementExists(driver, "//div[@class='panel-body custom-section']//i[@widget-data-event='click::RemoveEmail']");
                        if (isPresent == true)
                        {
                            ac.MoveToElement(delEmailBtn).Build().Perform();
                            delEmailBtn.Click();
                        }
                            
                        IWebElement customBtn = UIActions.FindElementWithXpath(driver, "//button[@widget-data-event='click::AddCustomEmail']");
                         ac.MoveToElement(customBtn).Build().Perform();
                    }
                    catch (NoSuchElementException )
                    {
                        IWebElement customBtn = UIActions.FindElementWithXpath(driver, "//button[@widget-data-event='click::AddCustomEmail']");
                        ac.MoveToElement(customBtn).Build().Perform();
                        break;
                    }
                  
                    break;
            }
            (new WebDriverWait(driver, TimeSpan.FromSeconds(10000))).Until(ExpectedConditions.ElementIsVisible(By.XPath(emailTypeEle)));
            UIActions.clickwithXapth(driver, emailTypeEle);
            Thread.Sleep(10000);
            IWebElement nameField = driver.FindElement(By.CssSelector("input[class='form-control email-name-text']"));
            nameField.SendKeys(surveyName + methodName + DateTime.Now);
            driver.FindElement(By.XPath("//*[@id='add-email-modal']/div[2]/div/div[3]/div/div[1]/div[2]/div/div/div[3]/div[2]/div")).SendKeys(surveyName + methodName + "email subject - Automation");
            try
            {
                StreamReader sr = new StreamReader(filePath, false);
                var txtContent = sr.ReadToEnd().ToString();
                IWebElement emailBody = driver.FindElement(By.XPath("//*[@id='add-email-modal']/div[2]/div/div[3]/div/div[1]/div[2]/div/div/div[6]/div[2]/div"));
                emailBody.Clear();
                emailBody.SendKeys(txtContent);
                emailBody.Click();
                IWebElement ckEditor = driver.FindElement(By.XPath("//span[@class='cke_button_icon cke_button__metatag_icon']"));
                ckEditor.Click();
                driver.FindElement(By.XPath("//input[@class='metaTags ui-autocomplete-input']")).Click();
                IWebElement e = driver.FindElement(By.XPath("//ul[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all']"));
                Actions a = new Actions(driver);
                a.ClickAndHold(e).Build().Perform();
                IReadOnlyCollection<IWebElement> liCnt = driver.FindElements(By.XPath("//ul[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all']/li/a"));
                //Reading the list of metatags
                foreach (IWebElement li in liCnt)
                {
                    string metaTagToEnter = li.Text;
                    if (metaTagToEnter.Equals(metaTag, StringComparison.CurrentCultureIgnoreCase))
                    {
                        li.Click();
                    }
                }
                IWebElement saveBtn = driver.FindElement(By.CssSelector("button[widget-data-event='click::SaveEmailForm']"));
                WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(3000));
                wait.Until(ExpectedConditions.ElementExists(By.CssSelector("button[widget-data-event='click::SaveEmailForm']")));
                ac.MoveToElement(saveBtn).Click().Build().Perform();
                Thread.Sleep(4000);

            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
            }

            return driver;
        }







    }
}

