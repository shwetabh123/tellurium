﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;
using System.IO;
using OpenQA.Selenium.Interactions;


namespace Common
{
    public class PlayerActions
    {
        public static string thankYouPageContent = "Thank you for your participation.We greatly appreciate your participation in the survey. Your survey responses have been received and saved, and no further changes can be made.To exit this survey now, please close your browser.";
        public static string[] GetAllQuestionsFromPage(IWebDriver driver, string selector)
        {
            var questions = driver.FindElements(By.CssSelector(selector));
            int noOfQuestions = questions.Count();
            string[] questionArray = new string[noOfQuestions];
            for(int index = 0; index<noOfQuestions; index++)
            {
                questionArray[index] = questions[index].Text;
            }
            return questionArray;
        }

        public static bool GetThankYouPageContent(IWebDriver driver, string selector)
        {
            try
            {
                var questions = driver.FindElements(By.CssSelector(selector));
                var content = questions.FirstOrDefault(e => e.Displayed && e.Text.Trim() == thankYouPageContent);
                if (content != null)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static void NextorFinishButtonCheckinPrevieworPlayer(IWebDriver driver)
        {
            try
            {
                IWebElement nextButton = UIActions.GetElement(driver, "#btnNext");
                IWebElement finishbutton = UIActions.GetElement(driver, "#btnFinish");
                if (nextButton.Displayed)
                {
                    while (nextButton.Displayed)
                    {
                        if (finishbutton.Displayed)
                        {
                            Thread.Sleep(4000);
                            finishbutton.Click();
                            break;
                        }
                        else
                            if (nextButton.Displayed)
                            driver.FindElement(By.Id("btnNext")).Click();
                        Thread.Sleep(8000);
                    }
                }
                if (finishbutton.Displayed)
                {
                    finishbutton.Click();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static bool CompareThankYouPageContent(IWebDriver driver, string selector,string ThankYouPageAuthor)
        {
            try
            {
                var questions = driver.FindElements(By.CssSelector(selector));
                var content = questions.FirstOrDefault(e => e.Displayed && e.Text.Trim() == ThankYouPageAuthor);
                if (content != null)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static bool CheckMetaTagReplacementinPlayerorPreview(IWebDriver driver, string selector,string metatag)
        {
            try
            {
                var questions = driver.FindElements(By.CssSelector(selector));
                var content = questions.FirstOrDefault(e => e.Displayed && e.Text.Trim().Contains(metatag));
                if (content != null)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public static void TranslationLanguageClick(IWebDriver driver,String language)
        {
            IWebElement Translation = UIActions.FindElementWithXpath(driver, "//*[@class='multiselect dropdown-toggle form-control ceb-dropdown']");
            Translation.Click();
            IWebElement Languageoption = UIActions.FindElementWithXpath(driver, "//*[contains(text(),'"+ language + "')]");
            Languageoption.Click();
        }
    }
}
