﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Data.SqlClient;


namespace Common
{
    public class MatchPairDBOperations
    {
        //public static DBConnectionStringDTO DBConnectionParameters;
        private string connectionString;

       
       
        public List<MatchPairParticipants> GetDistributionParticipants(int  distributionid, string userName, string password, string serverName)
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;
            List<MatchPairParticipants> participants = new List<MatchPairParticipants>();

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    // 1. create a command object identifying
                    // the stored procedure
                    SqlCommand cmd = new SqlCommand(
                        "usp_GetDistributionParticipants_Blue", sqlConn);

                    // 2. set the command object so it knows
                    // to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;
                 
                    // 3. add parameter to command, which
                    // will be passed to the stored procedure
                    cmd.Parameters.Add(
                        new SqlParameter("@DistributionId", distributionid));
                    cmd.Parameters.Add(
                       new SqlParameter("@PageNumber", page));
                    cmd.Parameters.Add(
                      new SqlParameter("@PageSize", 10));
                    cmd.Parameters.Add(
                      new SqlParameter("@OrderByCol", string.Empty));

                    cmd.Parameters.Add(
                    new SqlParameter("@Order", string.Empty));

                   cmd.Parameters.Add(
                   new SqlParameter("@SearchString", string.Empty));

                    cmd.Parameters.Add(
                    new SqlParameter("@ExcludeDemoColumns", 1));

                    cmd.Parameters.Add(
                    new SqlParameter("@RequiredDemoColumns", string.Empty));
                    // execute the command
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                       
                        // iterate through results, printing each to console
                        while (rdr.Read())
                        {
                            var Matchpairdto = new MatchPairParticipants();
                            Matchpairdto.SourceType = Convert.ToString(rdr["SourceType"]);
                            Matchpairdto.EmailAddress = Convert.ToString(rdr["EmailAddress"]);
                            participants.Add(Matchpairdto);
                        }
                    }
                }


            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }
            return participants;
        }
        public List<MatchPairParticipants> GetDistributionParticipantsForChildTab(int distributionid, string userName, string password, string serverName)
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;
            List<MatchPairParticipants> participants = new List<MatchPairParticipants>();

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                int excludedemo = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    // 1. create a command object identifying
                    // the stored procedure
                    SqlCommand cmd = new SqlCommand(
                        "usp_GetDistributionParticipants_Blue", sqlConn);

                    // 2. set the command object so it knows
                    // to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // 3. add parameter to command, which
                    // will be passed to the stored procedure
                    cmd.Parameters.Add(
                        new SqlParameter("@DistributionId", distributionid));
                    cmd.Parameters.Add(
                       new SqlParameter("@PageNumber", page));
                    cmd.Parameters.Add(
                      new SqlParameter("@PageSize", 10));
                    cmd.Parameters.Add(
                      new SqlParameter("@OrderByCol", string.Empty));

                    cmd.Parameters.Add(
                    new SqlParameter("@Order", string.Empty));

                    cmd.Parameters.Add(
                    new SqlParameter("@SearchString", string.Empty));

                    cmd.Parameters.Add(
                    new SqlParameter("@ExcludeDemoColumns", excludedemo));

                    cmd.Parameters.Add(
                    new SqlParameter("@RequiredDemoColumns", string.Empty));
                    // execute the command
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {

                        // iterate through results, printing each to console
                        while (rdr.Read())
                        {
                            var Matchpairdto = new MatchPairParticipants();
                            Matchpairdto.SourceType = Convert.ToString(rdr["SourceType"]);
                            Matchpairdto.SurveyWaveParticipantID = Convert.ToInt32(rdr["SurveyWaveParticipantID"]);
                            Matchpairdto.LastEmailSentDate = Convert.ToDateTime(rdr["LastEmailSentDate"]);

                            participants.Add(Matchpairdto);
                        }
                    }
                }


            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }
            return participants;
        }
        public int  ValidateDistributionParticipants(int distributionid, string userName, string password, string serverName, List<MatchPairParticipants> participants)
        {
            SqlConnection conn = null;
            SqlDataReader reader = null;
            int errorcode = 0;

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    // 1. create a command object identifying
                    // the stored procedure
                    SqlCommand command = new SqlCommand();
                    command.Connection = sqlConn;
                    command.CommandText = @"select  LastEmailSentDate from SurveyWaveParticipant where SurveyWaveParticipantID = @RecordID";
                    //command.CommandText = "SELECT TOP 1 SurveyWaveID FROM SurveyWaveParticipant WHERE SurveyID = @SurveyID AND [Status] = 1 AND IsSystemGenerated = 0 AND SurveyStatusFlag = 0";
                    SqlParameter _SurveypartID = command.Parameters.Add("@RecordID", SqlDbType.Int);
                    _SurveypartID.Direction = ParameterDirection.Input;
                    
                    for (int i = 0; i < participants.Count; i++)
                    {
                        _SurveypartID.Value = participants[i].SurveyWaveParticipantID;
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var date = reader.GetDateTime(0);
                            if (participants[i].LastEmailSentDate != date)
                            {
                                errorcode = 1;
                                break;
                            }

                        }
                        reader.Close();
                        if (errorcode == 1)
                            break;
                    }
                    
                    sqlConn.Close();
               

                   
                   
                }


            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (reader != null)
                {
                    reader.Dispose();
                }
            }
            return errorcode;
        }
        public List<MatchPairParticipants> GetDistributionParticipantsForParentTab(int distributionid, string userName, string password, string serverName)
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;
            List<MatchPairParticipants> participants = new List<MatchPairParticipants>();

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                int excludedemo = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    // 1. create a command object identifying
                    // the stored procedure
                    SqlCommand cmd = new SqlCommand(
                        "USP_GETMATCHEDPAIRPARTICIPANTS_blue", sqlConn);

                    // 2. set the command object so it knows
                    // to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // 3. add parameter to command, which
                    // will be passed to the stored procedure
                    cmd.Parameters.Add(
                        new SqlParameter("@DistributionId", distributionid));
                    cmd.Parameters.Add(
                       new SqlParameter("@PageNumber", page));
                    cmd.Parameters.Add(
                      new SqlParameter("@PageSize", 10));
                    cmd.Parameters.Add(
                      new SqlParameter("@OrderByCol", string.Empty));

                    cmd.Parameters.Add(
                    new SqlParameter("@Order", string.Empty));

                    cmd.Parameters.Add(
                    new SqlParameter("@SearchString", string.Empty));

                  
                    // execute the command
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {

                        // iterate through results, printing each to console
                        while (rdr.Read())
                        {
                            var Matchpairdto = new MatchPairParticipants();
                            Matchpairdto.SourceType = Convert.ToString(rdr["SourceType"]);
                            Matchpairdto.SurveyWaveParticipantID = Convert.ToInt32(rdr["SurveyWaveParticipantID"]);
                            object obj = rdr["LastEmailSentDate"];
                            if (obj != null)
                                Matchpairdto.LastEmailSentDate = Convert.ToDateTime(rdr["LastEmailSentDate"]);
                            Matchpairdto.ParentEmail = Convert.ToString(rdr["ParentEmail"]);
                            Matchpairdto.ChildEmail = Convert.ToString(rdr["ChildEmail"]);
                            participants.Add(Matchpairdto);
                        }
                    }
                }


            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (rdr != null)
                {
                    rdr.Dispose();
                }
            }
            return participants;
        }
        public int ValidateDestinationDistributionParticipants(int distributionid, string userName, string password, string serverName, List<MatchPairParticipants> participants)
        {
            SqlConnection conn = null;
            SqlDataReader reader = null;
            int errorcode = 0;

            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    // 1. create a command object identifying
                    // the stored procedure
                    SqlCommand command = new SqlCommand();
                    command.Connection = sqlConn;
                    command.CommandText = @"select  Count(*) from SurveyWaveParticipant where surveyid = 30794 and Sourceid = @RecordID ";
                    //command.CommandText = "SELECT TOP 1 SurveyWaveID FROM SurveyWaveParticipant WHERE SurveyID = @SurveyID AND [Status] = 1 AND IsSystemGenerated = 0 AND SurveyStatusFlag = 0";
                    SqlParameter _SurveypartID = command.Parameters.Add("@RecordID", SqlDbType.Int);
                    _SurveypartID.Direction = ParameterDirection.Input;

                    for (int i = 0; i < participants.Count; i++)
                    {
                        _SurveypartID.Value = participants[i].SurveyWaveParticipantID;
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var val = reader.GetInt32(0);
                            if (val != 1)
                            {
                                errorcode = 1;
                                break;
                            }

                        }
                        reader.Close();
                        if (errorcode == 1)
                            break;
                    }

                    sqlConn.Close();




                }


            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (reader != null)
                {
                    reader.Dispose();
                }
            }
            return errorcode;
        }
        public List<MatchPairParticipants> GetDistributionParentParticipants(int distributionid, string userName, string password, string serverName)
        {
            SqlConnection conn = null;
            SqlDataReader reader = null;
            int errorcode = 0;
            List<MatchPairParticipants> participants = new List<MatchPairParticipants>();
            try
            {
                connectionString = "data source =" + serverName + ";Initial Catalog=TCES;;MultipleActiveResultSets=True;User ID=" + userName +
                ";Password=" + password;
                int page = 0;
                // string connection = ConfigurationManager.AppSettings["DataModel"];
                // create and open a connection object
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    // 1. create a command object identifying
                    // the stored procedure
                    SqlCommand command = new SqlCommand();
                    command.Connection = sqlConn;
                    command.CommandText = @"select  LastEmailSentDate,emailaddress from SurveyWaveParticipant where SurveyWaveParticipantID in (select surveywaveparticipantid from surveymatchedpairmapping where surveyid = 26333) and LastEmailSentDate is not null and surveyid =26333 ";
                    //command.CommandText = "SELECT TOP 1 SurveyWaveID FROM SurveyWaveParticipant WHERE SurveyID = @SurveyID AND [Status] = 1 AND IsSystemGenerated = 0 AND SurveyStatusFlag = 0";
   

               
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var Matchpairdto = new MatchPairParticipants();
                            Matchpairdto.LastEmailSentDate = reader.GetDateTime(0);
                            Matchpairdto.EmailAddress = reader.GetString(1);
                            participants.Add(Matchpairdto);
                        }
                        
                       
                 }

            }

            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (reader != null)
                {
                    reader.Dispose();
                }
            }
            return participants;
        }
    }
}
