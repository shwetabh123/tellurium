﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Common
{
    public class SurveyDemographicActions
    {
        public static void OpenDemographicModal(IWebDriver driver)
        {
            UIActions.Click(driver, "#btnDemographics");    
        }

        public static void AddEditDemographic(IWebDriver driver, string demoLabel, string datatype)
        {
            var nameBox =
              driver.FindElements(By.CssSelector("#add-edit-demographics tbody input")).FirstOrDefault(x => x.Displayed);
            nameBox.Clear();
            nameBox.SendKeys(demoLabel);
            var selectElement =
                driver.FindElements(By.CssSelector("#add-edit-demographics tbody select"))
                    .FirstOrDefault(x => x.Displayed);
            SelectElement dataTypeSelect = new SelectElement(selectElement);
            dataTypeSelect.SelectByText(datatype);
            var editConfirm = driver.FindElements(By.CssSelector(".action-demo-edit-confirm")).FirstOrDefault(x => x.Displayed);
           editConfirm.Click();
            Thread.Sleep(5000);
        }

        public static void AddEditCancel(IWebDriver driver)
        {
            var editConfirm = driver.FindElements(By.CssSelector(".action-demo-edit-cancel")).FirstOrDefault(x => x.Displayed);
            editConfirm.Click();
        }

        public static void CopyDirectoryDemographics(IWebDriver driver, List<string> demoLabels)
        {
            UIActions.Click(driver, "#btn-copy-directory");
            Thread.Sleep(2000);
            var demoElements = driver.FindElements(By.CssSelector("input.edit-column-checkbox"));
            foreach (var demo in demoElements)
            {
                var label = demo.FindElement(By.XPath("..")).FindElement(By.TagName("label"));
                if (demoLabels.Contains(label.Text))
                    demo.Click();
            }
            UIActions.Click(driver, "#copy-demographics");
            Thread.Sleep(5000);
        }

        public static void CopySurveyDemographics(IWebDriver driver, string surveyName, List<string> demoLabels)
        {
            UIActions.Click(driver, "#btn-copy-survey");
            Thread.Sleep(10000);
            UIActions.SelectInCEBDropdown(driver,"#select-survey",surveyName);
            Thread.Sleep(10000);
            var demoElements = driver.FindElements(By.CssSelector("input.edit-column-checkbox"));
            foreach (var demo in demoElements)
            {
                var label = demo.FindElement(By.XPath("..")).FindElement(By.TagName("label"));
                if (demoLabels.Contains(label.Text))
                    demo.Click();
            }
            UIActions.Click(driver, "#copy-demographics");
            Thread.Sleep(5000);
        }

        public static void DeleteDemographics(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var gear = currenttr.FindElement(By.CssSelector("button.dropdown-toggle"));
            gear.Click();
            var deleteLink = currenttr.FindElements(By.CssSelector("ul.dropdown-menu li a")).FirstOrDefault(x => x.Text == "Delete");
            deleteLink.Click();
            Thread.Sleep(1000);
            var confirmButton = UIActions.GetElement(driver, "#pulseconfirmationModal #btnConfirmYes");
            confirmButton.Click();
            Thread.Sleep(2000);
        }

        public static void EditDemographicsActions(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var gear = currenttr.FindElement(By.CssSelector("button.dropdown-toggle"));
            gear.Click();
            var editLink = currenttr.FindElement(By.CssSelector("ul.dropdown-menu li a.editDemographics"));
            editLink.Click();
        }

        public static void ReorderDemographics(IWebDriver driver, string demoLabel, string toPosition)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var gear = currenttr.FindElement(By.CssSelector("button.dropdown-toggle"));
            gear.Click();
            var editLink = currenttr.FindElement(By.CssSelector("ul.dropdown-menu li a#btn-reorder-demo"));
            editLink.Click();
            if (toPosition == "-1")
            {
                var radioElement = driver.FindElements(By.CssSelector("#reorder-demographics-modal input"))[1];
                radioElement.Click();
            }
            else
            {
                UIActions.SelectInCEBDropdown(driver, "#reorder-demographics-modal #select-sortorder",toPosition);
            }
            UIActions.Click(driver, "#reorder-demo-save");
            Thread.Sleep(5000);
        }

        public static void ToggleFiltering(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-useForAnalyze .bootstrap-switch-container label.bootstrap-switch-label"));
            if (toggleFinder.GetAttribute("class").Contains("switchOn"))
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-useForAnalyze .bootstrap-switch-container span.bootstrap-switch-handle-on"));
                toggle.Click();
            }
            else
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-useForAnalyze .bootstrap-switch-container span.bootstrap-switch-handle-off"));
                toggle.Click();
            }
            Thread.Sleep(5000);
        }
        public static void TogglePII(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-personalIdentifier .bootstrap-switch-container label.bootstrap-switch-label"));
            if (toggleFinder.GetAttribute("class").Contains("switchOn"))
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-personalIdentifier .bootstrap-switch-container span.bootstrap-switch-handle-on"));
                toggle.Click();
            }
            else
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-personalIdentifier .bootstrap-switch-container span.bootstrap-switch-handle-off"));
                toggle.Click();
            }
            Thread.Sleep(5000);
        }

        public static void ToggleUpload(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-availableInUpload .bootstrap-switch-container label.bootstrap-switch-label"));
            if (toggleFinder.GetAttribute("class").Contains("switchOn"))
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-availableInUpload .bootstrap-switch-container span.bootstrap-switch-handle-on"));
                toggle.Click();
            }
            else
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-availableInUpload .bootstrap-switch-container span.bootstrap-switch-handle-off"));
                toggle.Click();
            }
            Thread.Sleep(5000);
        }

        public static void ToggleMandatory(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-mandatory .bootstrap-switch-container label.bootstrap-switch-label"));
            if (toggleFinder.GetAttribute("class").Contains("switchOn"))
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-mandatory .bootstrap-switch-container span.bootstrap-switch-handle-on"));
                toggle.Click();
            }
            else
            {
                var toggle = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-mandatory .bootstrap-switch-container+ span.bootstrap-switch-handle-off"));

                toggle.Click();
            }
            Thread.Sleep(5000);
        }

        public static bool GetToggleFiltering(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-useForAnalyze .bootstrap-switch-container label.bootstrap-switch-label"));
            return toggleFinder.GetAttribute("class").Contains("switchOn"); ;
        }
        public static bool GetTogglePII(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-personalIdentifier .bootstrap-switch-container label.bootstrap-switch-label"));
            return toggleFinder.GetAttribute("class").Contains("switchOn");
        }

        public static bool GetToggleUpload(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-availableInUpload .bootstrap-switch-container label.bootstrap-switch-label"));
            return toggleFinder.GetAttribute("class").Contains("switchOn");
        }

        public static bool GetToggleMandatory(IWebDriver driver, string demoLabel)
        {
            var columns = driver.FindElements(By.CssSelector("#add-edit-demographics tbody tr td label"));
            var demolabel = columns.FirstOrDefault(l => l.Text.Contains(demoLabel));
            var currenttr = demolabel.FindElement(By.XPath("..")).FindElement(By.XPath(".."));
            var toggleFinder = currenttr.FindElement(By.CssSelector(".bootstrap-switch-id-chk-mandatory .bootstrap-switch-container label.bootstrap-switch-label"));
            return toggleFinder.GetAttribute("class").Contains("switchOn");
        }
    }
}
