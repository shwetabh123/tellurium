﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class StyleSheetParametersDTO
    {
        public string BackgroundColor { get; set; }
        public string SecondaryBackgroundColor { get; set; }
        public string TitleBarColor { get; set; }
        public string TitleFontColor { get; set; }
        public string SectionHeadingFontColor { get; set; }
        public string SelectionColor { get; set; }
        public string SelectionOutlineColor { get; set; }
        public string ScaleLabelFontColor { get; set; }
    }
}
