﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class MatchPairParticipants
    {
        public long SurveyWaveParticipantID { get; set; }

        public int SurveyId { get; set; }

        public int SurveyWaveId { get; set; }

        public int ClientId { get; set; }

        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string EmployeeID { get; set; }

        public byte? AuthenticationType { get; set; }
        public string SourceType { get; set; }

        public byte? SurveyStatusFlag { get; set; }

        public byte? Status { get; set; }

        public long? LastSentEmailSurveyWaveID { get; set; }

        public DateTime? LastEmailSentDate { get; set; }

        public int? LastSentEmailTypeId { get; set; }

        public byte? LastEmailErrorCode { get; set; }

        public string ParentEmail { get; set; }

        public string ChildEmail { get; set; }
    }
}
