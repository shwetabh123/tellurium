﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace Common
{
    public class AccountSettings
    {
        public static void NavigateToUserProfile(IWebDriver driver)
        {
            IWebElement profile = driver.FindElement(By.CssSelector(".shl-icon_arrow_small_down"));
            if (profile.Displayed)
                profile.Click();
        }

        public static void selectScaleRange(IWebDriver driver, string scaleRange)
        {
            IReadOnlyCollection<IWebElement> scaleRangeDropdown = driver.FindElements(By.CssSelector("div li a label"));
            scaleRangeDropdown.FirstOrDefault(x => x.Text.Contains(scaleRange)).Click();
        }
        public static void SetScaleHeaders(IWebDriver driver, string[] headerLabel)
        {
            IReadOnlyCollection<IWebElement> scaleHeader = driver.FindElements(By.CssSelector("div input.responseTextAddRatingScale"));
            IReadOnlyCollection<IWebElement> checkBoxes = driver.FindElements(By.CssSelector("div.checkbox span.lbl"));
            //IReadOnlyCollection<IWebElement> checkBoxes = driver.FindElements(By.XPath("//div[@class='checkbox']//span[@class='lbl'] "));
            int headerLabelLen = headerLabel.Count();
            for (int i = 0; i < headerLabel.Length; i++)
            {
                scaleHeader.ElementAt(i).Click();
                scaleHeader.ElementAt(i).Clear();
                scaleHeader.ElementAt(i).SendKeys(headerLabel[i]);
                checkBoxes.ElementAt(i).Click();

            }
        }

        public static List<string> GetScaleRangeInAccountSettings(IWebDriver driver)
        {
            List<string> scaleRange = new List<string>();
            Thread.Sleep(2000);
            IWebElement scaleRangeDropdown = driver.FindElement(By.XPath("//select[@id='defaults-scale']/ancestor::*[1]/div/./button[@type='button']"));
            string scaleRangeSet = scaleRangeDropdown.Text;
            IReadOnlyCollection<IWebElement> scaleHeader = driver.FindElements(By.CssSelector("div input.responseTextAddRatingScale"));
            IReadOnlyCollection<IWebElement> checkBoxes = driver.FindElements(By.CssSelector("div.checkbox span.lbl"));
            scaleRange.Add(scaleRangeSet);
            scaleRange.Add(scaleHeader.Count().ToString());
            return scaleRange;
        }

        public static List<string> GetScaleRangeInForm(IWebDriver driver)
        {
            OpenQA.Selenium.Interactions.Actions ac = new OpenQA.Selenium.Interactions.Actions(driver);
            List<string> scaleRangeSetCollection = new List<string>();
            IWebElement scaleRangeInForm = driver.FindElement(By.XPath("//select[@id='default-rating-scales']/ancestor::*[1]/div/./button[@type='button']"));
            string scaleRangeSet = scaleRangeInForm.Text;
          
            IReadOnlyCollection<IWebElement> scaleLabelInForm = driver.FindElements(By.CssSelector("div.checkbox span.lbl"));
            ac.MoveToElement(driver.FindElement(By.CssSelector("div.checkbox span.lbl"))).Build().Perform();
            scaleRangeSetCollection.Add(scaleRangeSet);
            scaleRangeSetCollection.Add(scaleLabelInForm.Count().ToString());
            return scaleRangeSetCollection;
        }

        public static List<string> GetScaleHeaderInForm(IWebDriver driver)
        {
            List<string> getHeaders = new List<string>();
            IReadOnlyCollection<IWebElement> scaleHeaderFields = driver.FindElements(By.CssSelector("div input.defaultResponseTextAddRatingScale"));
            IReadOnlyCollection<IWebElement> checkBoxes = driver.FindElements(By.CssSelector("div.checkbox span.lbl"));
            foreach(IWebElement header in scaleHeaderFields)
            {
                header.GetAttribute("value").ToList();
                getHeaders.Add(header.GetAttribute("value"));
            }
           
            getHeaders.Add(checkBoxes.Count().ToString());
            return getHeaders;
        }

        public static string SetScaleRangeInForm(IWebDriver driver, string scaleRange)
        {
            Thread.Sleep(2000);
            IWebElement scaleRangeInForm = driver.FindElement(By.XPath("//select[@id='default-rating-scales']/ancestor::*[1]/div/./button[@type='button']"));
            scaleRangeInForm.Click();
            driver.FindElement(By.CssSelector("input.form-control.multiselect-search")).Click();
            driver.FindElement(By.CssSelector("input.form-control.multiselect-search")).Clear();
            driver.FindElement(By.CssSelector("input.form-control.multiselect-search")).SendKeys(scaleRange);
            //driver.FindElement(By.XPath("//select[@id='default-rating-scales']/ancestor::*[1]//li[@style='display: list-item;']/a/label")).Click();
            IReadOnlyCollection<IWebElement> scaleRadio = driver.FindElements(By.XPath("//select[@id='default-rating-scales']/ancestor::*[1]//li/a/label"));
            scaleRadio.FirstOrDefault(x => x.Text.Contains(scaleRange)).Click();
            IWebElement newscaleRangeInForm = driver.FindElement(By.XPath("//select[@id='default-rating-scales']/ancestor::*[1]/div/./button[@type='button']"));
            return newscaleRangeInForm.Text;
        }

        public static void SetScaleHeaderInForm(IWebDriver driver, List<string> scaleHeaders)
        {
            IReadOnlyCollection<IWebElement> scaleHeaderFields = driver.FindElements(By.CssSelector("div input.defaultResponseTextAddRatingScale"));
            IReadOnlyCollection<IWebElement> checkBoxes = driver.FindElements(By.CssSelector("div.checkbox span.lbl"));
            for (int i = 0; i < scaleHeaders.Count; i++)
            {
                scaleHeaderFields.ElementAt(i).Click();
                scaleHeaderFields.ElementAt(i).Clear();
                scaleHeaderFields.ElementAt(i).SendKeys(scaleHeaders[i]);
                checkBoxes.ElementAt(i).Click();
            }
        }

        public static void ClearScaleHeadersInAccountSettings(IWebDriver driver)
        {
            IReadOnlyCollection<IWebElement> scaleHeader = driver.FindElements(By.CssSelector("div input.responseTextAddRatingScale"));
            IReadOnlyCollection<IWebElement> checkBoxes = driver.FindElements(By.CssSelector("div.checkbox span.lbl"));
            for (int i = 0; i< scaleHeader.Count(); i++)
            {
                if(!string.IsNullOrEmpty(scaleHeader.ElementAt(i).GetAttribute("value")))
                {
                    scaleHeader.ElementAt(i).Clear();
                    checkBoxes.ElementAt(i).Click();
                }
            }
        }

        public static void ChangeScaleHeaderInForm(IWebDriver driver, List<string> scaleHeaders)
        {
            Actions ac = new Actions(driver);
            IReadOnlyCollection<IWebElement> scaleHeaderFields = driver.FindElements(By.CssSelector("div input.defaultResponseTextAddRatingScale"));
            IReadOnlyCollection<IWebElement> checkBoxes = driver.FindElements(By.CssSelector("div.checkbox span.lbl"));
            for (int i = 0; i < scaleHeaders.Count; i++)
            {
                ac.MoveToElement(scaleHeaderFields.ElementAt(i)).Build().Perform();
                scaleHeaderFields.ElementAt(i).Click();
                scaleHeaderFields.ElementAt(i).Clear();
                scaleHeaderFields.ElementAt(i).SendKeys(scaleHeaders[i]);
            }
        }

        public static void CleanCreatedQuestionsFromForm(IWebDriver driver)
        {
            
            IReadOnlyCollection<IWebElement> questionsTile = driver.FindElements(By.XPath("//div[@data-bind='SectionGuid']//div//div[@data-bind-transform='expression::#{questionGuid}']"));
            int i = questionsTile.Count();
           while (i > 0)
            {
                Thread.Sleep(1000);
                
                retryingFindClick(By.XPath("//div[@data-bind='SectionGuid']//div[" + i + "]//div[@data-bind-transform='expression::#{questionGuid}']//span[@title='Delete']"), driver, i);
                Thread.Sleep(4000);
                i--;
               
                questionsTile = driver.FindElements(By.XPath("//div[@data-bind='SectionGuid']//div//div[@data-bind-transform='expression::#{questionGuid}']"));
                i = questionsTile.Count();
            }
        }
        public static bool retryingFindClick(By by, IWebDriver driver, int i)
        {
            Actions ac = new Actions(driver);
            ac.MoveToElement(driver.FindElement(By.XPath("//div[@data-bind='SectionGuid']//div[" + i + "]//div[@data-bind-transform='expression::#{questionGuid}']"))).Build().Perform();
            bool result = false;
            int attempts = 0;
            while (attempts < 2)
            {
                try
                {
                    driver.FindElement(by).Click();
                    Thread.Sleep(1000);
                    driver.FindElement(By.CssSelector("#btnConfirmDeleteSectionClicked")).Click();
                    result = true;
                    break;
                }
                catch (StaleElementReferenceException e)
                {
                }
                attempts++;
            }
            return result;
        }
    }
}
