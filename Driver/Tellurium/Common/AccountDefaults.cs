﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium.Remote;
using System.Drawing;

namespace Common
{
    public class AccountDefaults
    {
        public static void NavigateToAccountDefaults(IWebDriver driver)
        {
            try
            {
                UIActions.Click(driver, ".user-info");
                UIActions.Click(driver, "[href='/Pulse/AccountSettings']");
                Thread.Sleep(5000);
                UIActions.Click(driver, "#menu-account-defaults");
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void CreateNewStyleSheet(IWebDriver driver, string StyleName, StyleSheetParametersDTO styleSheetParameters)
        {
            try
            {
                UIActions.Click(driver, "#defaults-create-new-stylesheet");
                Thread.Sleep(3000);
                IWebElement StyleSheetName = UIActions.FindElement(driver, "#stylesheet-name");
                IWebElement PrimaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-primary-background-text");
                IWebElement SecondaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-secondary-background-text");
                IWebElement TitleBarBckText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-background-text");
                IWebElement TitleBarText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-text-text");
                IWebElement SectionHeadingText = UIActions.FindElement(driver, "#stylesheet-color-picker-section-heading-text-text");
                IWebElement SelectionOptionText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-option-text");
                IWebElement SelectionOutlineText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-outline-text");
                IWebElement ScaleLabelText = UIActions.FindElement(driver, "#stylesheet-color-picker-scale-label-text-text");

                StyleSheetName.Clear();
                StyleSheetName.SendKeys(StyleName);
                PrimaryBckColor.Clear();
                PrimaryBckColor.SendKeys(styleSheetParameters.BackgroundColor);
                SecondaryBckColor.Clear();
                SecondaryBckColor.SendKeys(styleSheetParameters.SecondaryBackgroundColor);
                TitleBarBckText.Clear();
                TitleBarBckText.SendKeys(styleSheetParameters.TitleBarColor);
                TitleBarText.Clear();
                TitleBarText.SendKeys(styleSheetParameters.TitleFontColor);
                SectionHeadingText.Clear();
                SectionHeadingText.SendKeys(styleSheetParameters.SectionHeadingFontColor);
                SelectionOptionText.Clear();
                SelectionOptionText.SendKeys(styleSheetParameters.SelectionColor);
                SelectionOutlineText.Clear();
                SelectionOutlineText.SendKeys(styleSheetParameters.SelectionOutlineColor);
                ScaleLabelText.Clear();
                ScaleLabelText.SendKeys(styleSheetParameters.ScaleLabelFontColor);

                UIActions.Click(driver, "#btn-stylesheets-save");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void EditStyleSheet(IWebDriver driver, string StyleName, StyleSheetParametersDTO styleSheetParameters, string NewName = null)
        {
            try
            {
                UIActions.Click(driver, "#defaults-edit-stylesheets");
                Thread.Sleep(3000);
                IWebElement StyleSheetName = UIActions.FindElement(driver, "#stylesheet-name");
                IWebElement PrimaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-primary-background-text");
                IWebElement SecondaryBckColor = UIActions.FindElement(driver, "#stylesheet-color-picker-secondary-background-text");
                IWebElement TitleBarBckText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-background-text");
                IWebElement TitleBarText = UIActions.FindElement(driver, "#stylesheet-color-picker-title-bar-text-text");
                IWebElement SectionHeadingText = UIActions.FindElement(driver, "#stylesheet-color-picker-section-heading-text-text");
                IWebElement SelectionOptionText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-option-text");
                IWebElement SelectionOutlineText = UIActions.FindElement(driver, "#stylesheet-color-picker-selection-outline-text");
                IWebElement ScaleLabelText = UIActions.FindElement(driver, "#stylesheet-color-picker-scale-label-text-text");

                UIActions.SelectInCEBDropdownByText(driver, "#stylesheets-modal .ceb-dropdown-container", StyleName);
                if (NewName != null)
                {
                    StyleSheetName.Clear();
                    StyleSheetName.SendKeys(StyleName);
                }
                if (styleSheetParameters.BackgroundColor != null)
                {
                    PrimaryBckColor.Clear();
                    PrimaryBckColor.SendKeys(styleSheetParameters.BackgroundColor);
                }
                if (styleSheetParameters.SecondaryBackgroundColor != null)
                {
                    SecondaryBckColor.Clear();
                    SecondaryBckColor.SendKeys(styleSheetParameters.SecondaryBackgroundColor);
                }
                if (styleSheetParameters.TitleBarColor != null)
                {
                    TitleBarBckText.Clear();
                    TitleBarBckText.SendKeys(styleSheetParameters.TitleBarColor);
                }
                if (styleSheetParameters.TitleFontColor != null)
                {
                    TitleBarText.Clear();
                    TitleBarText.SendKeys(styleSheetParameters.TitleFontColor);
                }
                if (styleSheetParameters.SectionHeadingFontColor != null)
                {
                    SectionHeadingText.Clear();
                    SectionHeadingText.SendKeys(styleSheetParameters.SectionHeadingFontColor);
                }
                if (styleSheetParameters.SelectionColor != null)
                {
                    SelectionOptionText.Clear();
                    SelectionOptionText.SendKeys(styleSheetParameters.SelectionColor);
                }
                if (styleSheetParameters.SelectionOutlineColor != null)
                {
                    SelectionOutlineText.Clear();
                    SelectionOutlineText.SendKeys(styleSheetParameters.SelectionOutlineColor);
                }
                if (styleSheetParameters.ScaleLabelFontColor != null)
                {
                    ScaleLabelText.Clear();
                    ScaleLabelText.SendKeys(styleSheetParameters.ScaleLabelFontColor);
                }

                UIActions.Click(driver, "#btn-stylesheets-save");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void DeleteStyleSheet(IWebDriver driver, string StyleSheetName)
        {
            try
            {
                UIActions.Click(driver, "#defaults-edit-stylesheets");
                Thread.Sleep(3000);
                UIActions.SelectInCEBDropdownByText(driver, "#stylesheets-modal .ceb-dropdown-container", StyleSheetName);
                UIActions.Click(driver, "#btn-stylesheet-delete");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
