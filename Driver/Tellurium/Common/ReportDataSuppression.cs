﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Common
{
    public class ReportDataSuppression
    {
        public static string LogFileName = @"C:\Automation\AnalyzeAutomationLog.txt";
        public static DataSet CategoryItemCount;

        public static void Log(string logMessage, TextWriter w)
        {
            //w.Write("\r\nLog Entry : ");
            w.Write("\r\n ");
            w.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.Write("\t:\t");
            w.Write("{0}", logMessage);
            //w.WriteLine("-------------------------------");
        }
        public static void UpdateGroupMinimumSize(string NSize, string ClientID, DBConnectionStringDTO DBConnectionParameters)
        {
            string TCESInputQuery = "UPDATE AuthoringSetting SET GroupMinimumSize = " + NSize + " WHERE ClientId = " + ClientID;
            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET GroupMinimumSize = " + NSize + " WHERE ClientId = " + ClientID;
            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);
            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }
        public static void UpdateGroupNSizeToReportDemo(string NSize, string ClientID, DBConnectionStringDTO DBConnectionParameters)
        {
            string TCESInputQuery = "UPDATE AuthoringSetting SET GroupMinimumSize = " + NSize + " WHERE ClientId = " + ClientID;
            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET GroupMinimumSize = " + NSize + " WHERE ClientId = " + ClientID;
            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);
            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }


        public static void UpdateDemoValueMinimum(string NSize, string ClientID, DBConnectionStringDTO DBConnectionParameters)
        {
            string TCESInputQuery = "UPDATE AuthoringSetting SET DemoValueMinimum = " + NSize + " WHERE ClientId = " + ClientID;
            string TCESReportingInputQuery = "UPDATE DimSurveySetting SET DemoValueMinimum = " + NSize + " WHERE ClientId = " + ClientID;
            DataSet t_ds = DBOperations.ExecuteSPwithInputParameters_TCES(TCESInputQuery, DBConnectionParameters);
            DataSet tr_ds = DBOperations.ExecuteSPwithInputParameters(TCESReportingInputQuery, DBConnectionParameters);
        }



        public static void GetCategoryItemCount(string SurveyFormID, DBConnectionStringDTO DBConnectionParameters)
        {
            string InputQuery = "SELECT SurveyCategoryId, COUNT(DISTINCT SurveyFormItemID) [ItemCount] FROM Dimitem_V11 WHERE SurveyFormID = " + SurveyFormID + " AND GroupName IS NOT NULL GROUP BY SurveyCategoryId";
            CategoryItemCount = DBOperations.ExecuteSPwithInputParameters(InputQuery, DBConnectionParameters);
        }
        public static bool CompareSuppressionForInsightReport(DataSet ActualSP, DataSet TestSP, string ResultDisplay, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {
                //DataTable DT_ActualSPStrength;
                //DataTable DT_ActualSPOpportunities;
                DataTable DT_ActualSP = null;
                DataTable DT_TestSP = null;

                bool SuppStatus = true;
                //string ActualSPColumnName;
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_One_FavPerc_Diff" : "Comp_One_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_One_TotalRespCount_Diff";
                        break;
                    case 2:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Two_FavPerc_Diff" : "Comp_Two_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_Two_TotalRespCount_Diff";
                        break;
                    case 3:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp_Three_FavPerc_Diff" : "Comp_Three_Mean_Diff";
                        ActualSPCompColumnResp = "Comp_Three_TotalRespCount_Diff";
                        break;
                    default:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "FavPerc" : "MeanScore"; ;
                        ActualSPCompColumnResp = "TotalRespCount";
                        break;
                }
                if (TestSP.Tables[0].Columns.Contains("BenchmarkFavPerc") == false)
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[0]);
                    TestSPDv.Sort = "ObjectID ASC";
                    DT_TestSP = TestSPDv.ToTable(false, "ObjectID", "Total Responses", "IsSuppressed");
                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        if (ResultDisplay.ToLower() == "category")
                        {
                            DataViewManager Dvm = CategoryItemCount.DefaultViewManager;
                            DataView Dv = Dvm.CreateDataView(CategoryItemCount.Tables[0]);
                            Dv.RowFilter = "SurveyCategoryId = " + row["ObjectID"].ToString();
                            int ItemCount = Int32.Parse(Dv.ToTable(false, "ItemCount").ToString());
                            decimal Cat_Supp = Math.Round(Decimal.Parse(row["Total Responses"].ToString()), 0) / ItemCount;
                            row["IsSuppressed"] = (Cat_Supp < NSize) ? 0 : 1;
                        }
                        else
                        {
                            row["IsSuppressed"] = (Int32.Parse(row["Total Responses"].ToString()) < NSize) ? 0 : 1;
                        }
                    }
                }
                else
                {
                    DataViewManager TestSPDvm = TestSP.DefaultViewManager;
                    DataView TestSPDv = TestSPDvm.CreateDataView(TestSP.Tables[2]);
                    if (ResultDisplay.ToLower() == "category")
                    {
                        TestSPDv.Sort = "ObjectID ASC";
                        DT_TestSP = TestSPDv.ToTable(false, "ObjectID", "RespondentCount", "IsSuppressed");
                    }
                    else
                    {
                        TestSPDv.Sort = "SurveyFormItemID ASC";
                        DT_TestSP = TestSPDv.ToTable(false, "SurveyFormItemID", "RespondentCount", "IsSuppressed");
                    }
                    foreach (DataRow row in DT_TestSP.Rows)
                    {
                        if (ResultDisplay.ToLower() == "category")
                        {
                            DataViewManager Dvm = CategoryItemCount.DefaultViewManager;
                            DataView Dv = Dvm.CreateDataView(CategoryItemCount.Tables[0]);
                            Dv.RowFilter = "ObjectID = " + row["ObjectID"].ToString();
                            int ItemCount = Int32.Parse(Dv.ToTable(false, "ItemCount").ToString());
                            decimal Cat_Supp = Math.Round(Decimal.Parse(row["RespondentCount"].ToString()), 0) / ItemCount;
                            row["IsSuppressed"] = (Cat_Supp < NSize) ? 0 : 1;
                        }
                        else
                        {
                            row["IsSuppressed"] = (Int32.Parse(row["RespondentCount"].ToString()) < NSize) ? 0 : 1;
                        }
                    }
                }

                if (GroupOrder == 0)
                {
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    DT_ActualSP = StrengthDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp);

                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    //DT_ActualSP = OpportunitiesDv.ToTable(false, "objectId");
                    DT_ActualSP.Merge(OpportunitiesDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp));
                    DT_ActualSP.DefaultView.Sort = "objectId ASC";

                    //DataViewManager Dvm = DT_TestSP.DefaultView;
                    DataView Dv = DT_TestSP.DefaultView;
                    Dv.RowFilter = "IsSuppressed = 0";
                    DataTable DT = Dv.ToTable();

                    for (int i = 0, j = 0; i < DT.Rows.Count && j < DT_ActualSP.Rows.Count; i++, j++)
                    {
                        if (DT.Rows[i]["ObjectID"].ToString() != DT_ActualSP.Rows[j]["objectId"].ToString())
                            SuppStatus = false;
                    }
                }
                else
                {
                    DataViewManager StrengthDvm = ActualSP.DefaultViewManager;
                    DataView StrengthDv = StrengthDvm.CreateDataView(ActualSP.Tables[0]);
                    DT_ActualSP = StrengthDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp);

                    DataViewManager OpportunitiesDvm = ActualSP.DefaultViewManager;
                    DataView OpportunitiesDv = OpportunitiesDvm.CreateDataView(ActualSP.Tables[1]);
                    //DT_ActualSP = OpportunitiesDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp);
                    DT_ActualSP.Merge(OpportunitiesDv.ToTable(false, "objectId", ActualSPCompColumnName, ActualSPCompColumnResp));
                    DT_ActualSP.DefaultView.Sort = "objectId ASC";

                    //DataViewManager Dvm = TestSP.DefaultViewManager;
                    DataView Dv = DT_TestSP.DefaultView;
                    Dv.RowFilter = "IsSuppressed = 0";
                    DataTable DT = Dv.ToTable();

                    foreach (DataRow test_row in DT.Rows)
                    {
                        foreach (DataRow act_row in DT_ActualSP.Rows)
                        {
                            if (test_row["ObjectID"].ToString() == act_row["objectId"].ToString())
                            {
                                if (act_row[ActualSPCompColumnName] == DBNull.Value)
                                    SuppStatus = false;
                            }
                        }
                    }
                }
                return SuppStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }

        public static bool CompareSuppressionForDrillDownReport(DataSet ActualSP, DataSet TestSP, string ResultDisplay, string DataCalculation, int NSize, int GroupOrder)
        {
            try
            {
                DataTable DT_ActualSP;
                DataTable DT_TestSP;

                bool SuppStatus = true;
                //string ActualSPFavColumnName;
                string ActualSPCompColumnName;
                string ActualSPCompColumnResp;
                string ObjectID;

                switch (GroupOrder)
                {
                    case 1:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp1DiffFavPercent" : "Comp1DiffAverageScore";
                        ActualSPCompColumnResp = "Comp_One_TotalRespCount_Diff";
                        break;
                    case 2:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp2DiffFavPercent" : "Comp2DiffAverageScore";
                        ActualSPCompColumnResp = "Comp_Two_TotalRespCount_Diff";
                        break;
                    case 3:
                        ActualSPCompColumnName = (DataCalculation.ToLower() == "percentage") ? "Comp3DiffFavPercent" : "Comp3DiffAverageScore";
                        ActualSPCompColumnResp = "Comp_Three_TotalRespCount_Diff";
                        break;
                    default:
                        ActualSPCompColumnName = "IsSuppressed";
                        ActualSPCompColumnResp = "";
                        break;
                }
                DataViewManager ActualSPDVM = ActualSP.DefaultViewManager;
                DataView ActualSPDV = ActualSPDVM.CreateDataView(ActualSP.Tables[0]);
                if (ResultDisplay.ToLower() == "category")
                {
                    ActualSPDV.RowFilter = "Order by CategoryID Asc";
                    ObjectID = "CategoryID";
                }
                else
                {
                    ActualSPDV.RowFilter = "Order by SurveyFormItemID Asc";
                    ObjectID = "SurveyFormItemID";
                }
                DT_ActualSP = ActualSPDV.ToTable();

                DataViewManager TestSPDVM = TestSP.DefaultViewManager;
                DataView Dv = TestSPDVM.CreateDataView(TestSP.Tables[0]);
                Dv.RowFilter = "Order by ObjectID Asc";
                DT_TestSP = Dv.ToTable();

                if (GroupOrder == 0)
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            int Test_IsSuppressed;
                            Test_IsSuppressed = (Int32.Parse(RowTbl2["TotalResponses"].ToString()) > NSize) ? 0 : 1;
                            if (Int32.Parse(RowTbl1["IsSuppressed"].ToString()) != Test_IsSuppressed)
                            {
                                SuppStatus = false;
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    Log("RowTbl1[" + ObjectID + "] = " + RowTbl1[ObjectID].ToString(), w);
                                    Log("RowTbl2[" + ObjectID + "] = " + RowTbl2[ObjectID].ToString(), w);
                                    Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                    Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                    Log("RowTbl2[" + "TotalResponses" + "] = " + RowTbl2["TotalResponses"].ToString(), w);
                                }
                                break;
                            }
                        }
                    }
                }
                if (GroupOrder != 0)
                {
                    foreach (DataRow RowTbl1 in DT_ActualSP.Rows)
                    {
                        foreach (DataRow RowTbl2 in DT_TestSP.Rows)
                        {
                            int Test_IsSuppressed;
                            Test_IsSuppressed = (Int32.Parse(RowTbl2["TotalResponses"].ToString()) > NSize) ? 0 : 1;
                            int Actual_IsSuppressed;
                            Actual_IsSuppressed = (RowTbl1[ActualSPCompColumnName] == DBNull.Value) ? 1 : 0;
                            if (Actual_IsSuppressed != Test_IsSuppressed)
                            {
                                SuppStatus = false;
                                using (StreamWriter w = File.AppendText(LogFileName))
                                {
                                    Log("RowTbl1[" + ObjectID + "] = " + RowTbl1[ObjectID].ToString(), w);
                                    Log("RowTbl2[" + ObjectID + "] = " + RowTbl2[ObjectID].ToString(), w);
                                    Log("RowTbl1[" + ActualSPCompColumnName + "] = " + RowTbl1[ActualSPCompColumnName].ToString(), w);
                                    Log("TestSP_Suppression" + Test_IsSuppressed, w);
                                    Log("RowTbl2[" + "TotalResponses" + "] = " + RowTbl2["TotalResponses"].ToString(), w);
                                }
                                break;
                            }
                        }
                    }
                }
                return SuppStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    ReportDataValidation.Log("ERROR : " + e.Message, w);
                }
                return false;
            }
        }
    }
}
