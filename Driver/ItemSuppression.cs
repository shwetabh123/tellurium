﻿using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;

namespace Driver
{

    [TestClass]
    public class ItemSuppression
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString(),
                downloadFilepath = context.Properties["downloadFilepath"].ToString(),
                logFilePath = context.Properties["logFilePath"].ToString()

            };
        }


        public static IWebDriver GetWebDriverwithcapabilities(string driverName, string url)
        {
            IWebDriver driver = null;

            switch (driverName)
            {
                case "Firefox":
                    FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Automation\Driver");
                    service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    driver = new FirefoxDriver(service);


                    FirefoxOptions options = new FirefoxOptions();
                    options.SetPreference("browser.download.folderList", 2);

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.dir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.downloadDir", DriverAndLogin.downloadFilepath);
                    options.SetPreference("browser.download.defaultFolder", DriverAndLogin.downloadFilepath);


                    options.SetPreference("pref.downloads.disable_button.edit_actions", false);

                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk",
                          "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/xls;text/csv");

                    options.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");

                    options.SetPreference("browser.download.manager.showWhenStarting", false);
                    options.SetPreference("browser.download.manager.focusWhenStarting", false);
                    options.SetPreference("browser.download.useDownloadDir", true);
                    options.SetPreference("browser.helperApps.alwaysAsk.force", false);
                    options.SetPreference("browser.download.manager.alertOnEXEOpen", false);
                    options.SetPreference("browser.download.manager.closeWhenDone", true);
                    options.SetPreference("browser.download.manager.showAlertOnComplete", false);
                    options.SetPreference("browser.download.manager.useWindow", false);
                    options.SetPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                    options.SetPreference("pdfjs.disabled", true);


                    //driver = new FirefoxDriver(options);



                    break;

                case "IE":
                    driver = new InternetExplorerDriver();

                    break;
                case "Chrome":
                default:

                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DriverAndLogin.downloadFilepath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "false");
                    chromeOptions.AddUserProfilePreference("browser.helperApps.neverAsk.saveToDisk", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    driver = new ChromeDriver(chromeOptions);

                    break;
            }
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;

        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }

        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }




        public static RemoteWebDriver SetDriverAndNavigateToAnalyze()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            }
            else if (driver.Title.Contains("Analyze - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            }
            return driver;
        }


        public static RemoteWebDriver SetDriverAndNavigateToAnalyzeReport(string Distribution)
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, Distribution);
            }
            else if (driver.Title.Contains("Analyze - Reports - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
                Thread.Sleep(10000);
                AnalyzeAction.OpenAnalyzeForm(driver, Distribution);
            }
            return driver;
        }


        public static RemoteWebDriver SetDriverAndNavigatetoAccountSetting()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                SuppressionActions.NavigatetoAccountSuppression(driver);
            }
            else if (driver.Title.Contains("Account Settings - Pulse"))
            {
                Thread.Sleep(3000);
            }
            else
            {
                SuppressionActions.NavigatetoAccountSuppression(driver);
            }
            return driver;
        }

        [TestMethod]

        public void A_CS_ValidateResultItemSuppressionwithNoFilterapplied_percentage()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "848", "848", "848", "848", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            string[] QuestionText = { "I have a good understanding of our pension plan.", "Provider website" };
            QuestionText.ToList()
                .ForEach((element) =>
                {
                    AnalyzeAction.SearchQuestion(driver, element);
                    Thread.Sleep(10000);
                    IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                    var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                    Assert.AreEqual("Not enough responses", SuppressedText.Text);
                });


        }

        [TestMethod]

        public void B_CS_ValidateResultItemSuppressionwithNoFilterapplied_Average()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyze();
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            string[] QuestionText = { "I have a good understanding of our pension plan.", "Provider website" };
            QuestionText.ToList()
                .ForEach((element) =>
                {
                    AnalyzeAction.SearchQuestion(driver, element);
                    Thread.Sleep(10000);
                    IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                    var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                    Assert.AreEqual("Not enough responses", SuppressedText.Text);
                });


        }

        [TestMethod]
        public void C_CS_ValidateInsightItemSuppressionWithNoFilterapplied_percentage()
        {
            bool flag = false;
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void D_CS_ValidateInsightItemSuppressionWithNoFilterapplied_Average()
        {
            bool flag = false;
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }



        [TestMethod]

        public void E_CS_ValidateDrilldownItemSuppressionwithNoFilterapplied_Percentage()
        {

            bool flag = false;
            string[] questionTexts = { "I have a good understanding of our pension plan.", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void F_CS_ValidateDrilldownItemSuppressionwithNoFilterapplied_Average()
        {


            bool flag = false;
            string[] questionTexts = { "I have a good understanding of our pension plan.", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Average");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void G_CS_ValidateDemographicItemSuppressionwithNoFilterapplied_Percentage()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "I have a good understanding of our pension plan.");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }


        [TestMethod]

        public void H_CS_ValidateDemographicItemSuppressionwithNoFilterapplied_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "I have a good understanding of our pension plan.");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }



        [TestMethod]
        public void A_CS_ValidateResultItemSuppressionWithDemoFilterapplied_Country_India_Percentage()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "133", "133", "133", "133", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            SuppressionActions.ExpandAdditionalFilters(driver);
            SuppressionActions.SelectAdditionalFilters(driver, "country");
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "India");
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "I have a good work/life balance." };
            QuestionText.ToList()
          .ForEach((element) =>
          {
              AnalyzeAction.SearchQuestion(driver, element);
              Thread.Sleep(7000);
              IJavaScriptExecutor js = driver as IJavaScriptExecutor;
              var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
              Assert.AreEqual("Not enough responses", SuppressedText.Text);
          });


        }
        [TestMethod]
        public void B_CS_ValidateResultItemSuppressionWithDemoFilterapplied_Country_India_Average()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(12000);
            /*SuppressionActions.ExpandAdditionalFilters(driver);
            SuppressionActions.SelectAdditionalFilters(driver, "country");
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "India");*/
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "I have a good work/life balance." };
            QuestionText.ToList()
          .ForEach((element) =>
          {
              AnalyzeAction.SearchQuestion(driver, element);
              Thread.Sleep(12000);
              IJavaScriptExecutor js = driver as IJavaScriptExecutor;
              var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
              Assert.AreEqual("Not enough responses", SuppressedText.Text);
          });


        }

        [TestMethod]


        public void C_CS_ValidateInsightItemSuppressionwithDemoFilter_Country_India_Percentage()
        {
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "I am satisfied with my medical plan’s hospital network.", "401(k) Savings Plan", "Auto &amp; Home Insurance", "I am satisfied with the quantity of the benefits communications I receive." };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }



        [TestMethod]
        public void D_CS_ValidateInsightItemSuppressionwithDemoFilter_Country_India_Average()
        {
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "I am satisfied with my medical plan’s hospital network.", "401(k) Savings Plan", "Auto & amp; Home Insurance", "I am satisfied with the quantity of the benefits communications I receive." };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void E_CS_ValidateDrillDownItemSuppressionwithDemoFilter_Country_India_Percentage()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Stock Purchase Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }


        [TestMethod]

        public void F_CS_ValidateDrillDownItemSuppressionwithDemoFilter_Country_India_Average()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Stock Purchase Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void G_CS_ValidateDemographicItemSuppressionwithDemoFilter_Country_India_percentage()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "401(k) Savings Plan");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }

        [TestMethod]

        public void H_CS_ValidateDemographicItemSuppressionwithDemoFilter_Country_India_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");

            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "401(k) Savings Plan");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }


        [TestMethod]
        public void A_CS_ValidateResultItemSuppressionWithFilter_TimePeriod_Percentage()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "355", "355", "355", "355", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "I have a good understanding of our pension plan." };
            QuestionText.ToList()
          .ForEach((element) =>
          {
              AnalyzeAction.SearchQuestion(driver, element);
              Thread.Sleep(10000);
              IJavaScriptExecutor js = driver as IJavaScriptExecutor;
              var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
              Assert.AreEqual("Not enough responses", SuppressedText.Text);
          });


        }

        [TestMethod]
        public void B_CS_ValidateResultItemSuppressionWithFilter_TimePeriod_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            //AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "I have a good understanding of our pension plan." };
            QuestionText.ToList()
          .ForEach((element) =>
          {
              AnalyzeAction.SearchQuestion(driver, element);
              Thread.Sleep(10000);
              IJavaScriptExecutor js = driver as IJavaScriptExecutor;
              var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
              Assert.AreEqual("Not enough responses", SuppressedText.Text);
          });


        }

        [TestMethod]

        public void C_CS_ValidateInsightItemSuppressionWithFilter_TimePeriod_Percentage()
        {
            string[] questionTexts = { "Which of the following best describes your level in the organization?", "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "401(k) Savings Plan" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void D_CS_ValidateInsightItemSuppressionWithFilter_TimePeriod_Average()
        {
            string[] questionTexts = { "Which of the following best describes your level in the organization?", "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "401(k) Savings Plan" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]

        public void E_CS_ValidateDrillDownItemSuppressionWithFilter_TimePeriod_Percentage()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Stock Purchase Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }


        [TestMethod]

        public void F_CS_ValidateDrillDownItemSuppressionWithFilter_TimePeriod_Average()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Stock Purchase Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void G_CS_ValidateDemographicItemSuppressionWithFilter_TimePeriod_percentage()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "401(k) Savings Plan");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }

        [TestMethod]

        public void H_CS_ValidateDemographicItemSuppressionWithFilterapplied_Timeperiod_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");

            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "401(k) Savings Plan");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }


        [TestMethod]

        public void A_CS_ValidateResultItemSuppressionWithDemo_TimePeriodFilterapplied_Percentage()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "14", "14", "14", "14", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(3000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(20000);
            SuppressionActions.ExpandAdditionalFilters(driver);
            SuppressionActions.SelectAdditionalFilters(driver, "country");
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "India");
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            string[] QuestionText = { "Stock Purchase Plan", "Provider website" };
            QuestionText.ToList()
              .ForEach((element) =>
              {
                  AnalyzeAction.SearchQuestion(driver, element);
                  Thread.Sleep(10000);
                  IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                  var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                  Assert.AreEqual("Not enough responses", SuppressedText.Text);
              });

        }



        [TestMethod]
        // Issue in Lower Environment
        public void B_CS_ValidateResultItemSuppressionWithDemo_TimePeriodFilterapplied_Average()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            SuppressionActions.ExpandAdditionalFilters(driver);
            SuppressionActions.SelectAdditionalFilters(driver, "country");
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "India");
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            string[] QuestionText = { "Stock Purchase Plan", "Provider website" };
            QuestionText.ToList()
              .ForEach((element) =>
              {
                  AnalyzeAction.SearchQuestion(driver, element);
                  Thread.Sleep(10000);
                  IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                  var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                  Assert.AreEqual("Not enough responses", SuppressedText.Text);
              });

        }

        [TestMethod]

        public void C_CS_ValidateInsightItemSuppressionWithDemo_TimePeriodFilterapplied_Percentage()
        {
            string[] questionTexts = { "Stock Purchase Plan", "Auto &amp; Home Insurance" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void D_CS_ValidateInsightItemSuppressionWithDemo_TimePeriodFilterapplied_Average()
        {
            string[] questionTexts = { "Stock Purchase Plan", "Auto &amp; Home Insurance" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]

        public void E_CS_ValidateDrillDownItemSuppressionWithDemo_TimePeriodFilterapplied_Percentage()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void F_CS_ValidateDrillDownItemSuppressionWithDemo_TimePeriodFilterapplied_Average()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void G_CS_ValidateDemographicItemSuppressionWithDemo_TimePeriodFilterapplied_Percentage()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "Stock Purchase Plan");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }

        [TestMethod]

        public void H_CS_ValidateDemographicItemSuppressionWithDemo_TimePeriodFilterapplied_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "Stock Purchase Plan");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }



        [TestMethod]

        public void A_CS_ValidateResultItemSuppressionWithDateAsDemoFilter_Percentage()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "275", "275", "275", "275", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            SuppressionActions.ExpandAdditionalFilters(driver);
            SuppressionActions.SelectAdditionalFilters(driver, "retirement eligible date");
            AnalyzeAction.SelectLeftSideDateDemoValues(driver, "retirement eligible date", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            string[] QuestionText = { "I feel my Company has good benefits.", "Provider website", "I find the Benefits Newsletter useful.", "Provider Coverage meeting", "I am satisfied with my medical plan’s hospital network." };
            QuestionText.ToList()
              .ForEach((element) =>
              {
                  AnalyzeAction.SearchQuestion(driver, element);
                  Thread.Sleep(10000);
                  IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                  var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                  Assert.AreEqual("Not enough responses", SuppressedText.Text);
              });

        }


        [TestMethod]

        public void B_CS_ValidateResultItemSuppressionWithDateAsDemoFilter_Average()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            /* SuppressionActions.ExpandAdditionalFilters(driver);
             SuppressionActions.SelectAdditionalFilters(driver, "retirement eligible date");
             AnalyzeAction.SelectLeftSideDateDemoValues(driver, "retirement eligible date", "01/01/2015", "01/01/2016");
             Thread.Sleep(10000);*/
            string[] QuestionText = { "I feel my Company has good benefits.", "Provider website", "I find the Benefits Newsletter useful.", "Provider Coverage meeting", "I am satisfied with my medical plan’s hospital network." };
            QuestionText.ToList()
              .ForEach((element) =>
              {
                  AnalyzeAction.SearchQuestion(driver, element);
                  Thread.Sleep(10000);
                  IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                  var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                  Assert.AreEqual("Not enough responses", SuppressedText.Text);
              });

        }

        [TestMethod]

        public void C_CS_ValidateInsightItemSuppressionWithDateAsDemoFilter_Percentage()
        {
            string[] questionTexts = { "Which of the following best describes your level in the organization ? ", "I feel my Company has good benefits.", " The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", " I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", " I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & amp; Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "How helpful was the Employee Assistance Program?", "Over the phone Advisor Service", "The 401(k) plan website" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void D_CS_ValidateInsightItemSuppressionWithDateAsDemoFilter_Average()
        {
            string[] questionTexts = { "Which of the following best describes your level in the organization ? ", "I feel my Company has good benefits.", " The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", " I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", " I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & amp; Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "How helpful was the Employee Assistance Program?", "Over the phone Advisor Service", "The 401(k) plan website" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }



        [TestMethod]

        public void E_CS_ValidateDrillDownItemSuppressionWithDateAsDemoFilter_Percentage()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }


        [TestMethod]

        public void F_CS_ValidateDrillDownItemSuppressionWithDateAsDemoFilter_Average()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void G_CS_ValidateDemographicItemSuppressionWithDateAsDemoFilter_Percentage()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "I feel my Company has good benefits.");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }

        [TestMethod]

        public void H_CS_ValidateDemographicItemSuppressionWithDateAsDemoFilter_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "I feel my Company has good benefits.");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void A_CS_ValidateResultItemSuppressionWithTimeperiodInAnalysis_Percentage()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "275", "275", "275", "275", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(2000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.SetTimePeriodAnalysisSettings(driver, "retirement eligible date");
            Thread.Sleep(1000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            /*AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);*/
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.SetTimePeriodAnalysisSettings(driver, "retirement eligible date");
            Thread.Sleep(1000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "I have a good understanding of our pension plan.", "The 401(k) plan website", "401(k) Savings Plan" };
            QuestionText.ToList()
              .ForEach((element) =>
              {
                  AnalyzeAction.SearchQuestion(driver, element);
                  Thread.Sleep(10000);
                  IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                  var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                  Assert.AreEqual("Not enough responses", SuppressedText.Text);
              });



        }

        [TestMethod]
        public void B_CS_ValidateResultItemSuppressionWithTimeperiodInAnalysis_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);

            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "I have a good understanding of our pension plan.", "The 401(k) plan website", "401(k) Savings Plan" };
            QuestionText.ToList()
              .ForEach((element) =>
              {
                  AnalyzeAction.SearchQuestion(driver, element);
                  Thread.Sleep(10000);
                  IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                  var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                  Assert.AreEqual("Not enough responses", SuppressedText.Text);
              });



        }

        [TestMethod]

        public void C_CS_ValidateInsightSuppressionWithTimeperiodInAnalysis_Percentage()
        {
            string[] questionTexts = { "Which of the following best describes your level in the organization ? ", "I feel my Company has good benefits.", " The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", " I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", " I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & amp; Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "How helpful was the Employee Assistance Program?", "Over the phone Advisor Service", "The 401(k) plan website" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void D_CS_ValidateInsightSuppressionWithTimeperiodInAnalysis_Average()
        {
            string[] questionTexts = { "Which of the following best describes your level in the organization ? ", "I feel my Company has good benefits.", " The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", " I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", " I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & amp; Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "How helpful was the Employee Assistance Program?", "Over the phone Advisor Service", "The 401(k) plan website" };
            bool flag = false;
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]

        public void E_CS_ValidateDrillDownSuppressionWithTimePeriodInAnalysis_Percentage()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]

        public void F_CS_ValidateDrillDownSuppressionWithTimePeriodInAnalysis_Average()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void G_CS_ValidateDemographicItemSuppressionWithTimePeriodInAnalysis_Percentage()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "I feel my Company has good benefits.");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }


        }

        [TestMethod]

        public void H_CS_ValidateDemographicItemSuppressionWithTimePeriodInAnalysis_Average()
        {

            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            var sup_symbol = SuppressionActions.CheckDemographicSuppression(driver, "I feel my Company has good benefits.");
            if (sup_symbol != null)
            {
                Assert.AreEqual("--", sup_symbol);
            }
            else
            {
                Assert.Fail();
            }

        }
        

        [TestMethod]
        public void CS_ValidateResultItemSuppressionWithPreviousComparisongroup_NoFilterapplied_Percentage_01()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "855", "855", "855", "855", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(2000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(15000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            Thread.Sleep(2000);
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            string[] QuestionText = { "I feel my Company has good benefits.", "Dental" };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 SuppressionActions.ClickMenubarinResultTab(driver);
                 Thread.Sleep(2000);
                 SuppressionActions.SelectChartTypeDatatableInResultTab(driver);
                 Thread.Sleep(2000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (string)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('question-table').shadowRoot.querySelector('.table-striped tr td:last-child').textContent ");
                 Assert.AreEqual("--", SuppressedText);

             });


        }


        [TestMethod]
        public void CS_ValidateResultItemSuppressionWithPreviousComparisongroup_NoFilterapplied_Average_02()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            /*AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            Thread.Sleep(2000);*/
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            string[] QuestionText = { "I feel my Company has good benefits.", "Dental" };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 SuppressionActions.ClickMenubarinResultTab(driver);
                 Thread.Sleep(2000);
                 SuppressionActions.SelectChartTypeDatatableInResultTab(driver);
                 Thread.Sleep(2000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (string)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('question-table').shadowRoot.querySelector('.table-striped tr td:last-child').textContent ");
                 Assert.AreEqual("--", SuppressedText);

             });


        }

        [TestMethod]

        public void CS_ValidateInsightItemSuppressionWithPreviousComparisongroup_NoFilterapplied_Percentage_03()
        {

            bool flag = false;
            string[] questionTexts = { "Medical" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

       [TestMethod]

       public void CS_ValidateInsightItemSuppressionWithPreviousComparisongroup_NoFilterapplied_Average_04()
        {
            bool flag = false;
            string[] questionTexts = {"Medical"};
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightComparisonAvg(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]



        public void CS_ValidateDrillDownItemSuppressionWithPreviousComparisongroup_NoFilterapplied_Percentage_05()
        {
            bool flag = false;
            string[] questionTexts = { "Medical" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void CS_ValidateDrillDownItemSuppressionWithPreviousComparisongroup_NoFilterapplied_Average_06()
        {
            bool flag = false;
            string[] questionTexts = { "Medical" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod] 
        public void CS_ValidateResultItemSuppressionWithPreviousComparisongroup_DemoFilter_Percentage_01()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "136", "136", "136", "136", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(2000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(15000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            SuppressionActions.ExpandAdditionalFilters(driver);
            SuppressionActions.SelectAdditionalFilters(driver, "country");
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "India");
            Thread.Sleep(10000);
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "401(k) Savings Plan" };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(7000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                 Assert.AreEqual("Not enough responses", SuppressedText.Text);

             });

        }



        [TestMethod]
        public void CS_ValidateResultItemSuppressionWithPreviousComparisongroup_DemoFilter_Average_02()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            /*AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            Thread.Sleep(2000);*/
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            string[] QuestionText = { "The Company benefits package was an important factor in my decision to join the Company.", "401(k) Savings Plan" };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(7000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('.suppresed')");
                 Assert.AreEqual("Not enough responses", SuppressedText.Text);

             });

        }

        [TestMethod]

        public void CS_ValidateInsightItemSuppressionWithPreviousComparisongroup_Demoapplied_Percentage_03()
        {

            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the Benefits Website?" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }


        [TestMethod]

        public void CS_ValidateInsightItemSuppressionWithPreviousComparisongroup_Demoapplied_Average_04()
        {

            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the Benefits Website?" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightComparisonAvg(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]



        public void CS_ValidateDrillDownItemSuppressionWithPreviousComparisongroup_Demoapplied_Percentage_05()
        {
            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the Benefits Website?" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]



        public void CS_ValidateDrillDownItemSuppressionWithPreviousComparisongroup_Demoapplied_Average_06()
        {
            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the Benefits Website?" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }



        [TestMethod]
        public void A_CS_ValidateResultItemSuppressionWithPreviousComparisongroup_DateDemoFilter_Percentage_01()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "275", "275",null, null, null);
            SuppressionActions.ApplyDemographicSuppressionToggle(driver, "//*[@class='bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-off bootstrap-switch-small bootstrap-switch-animate bootstrap-switch-id-groupNSizeDemoActive']");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(2000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(15000);
            SuppressionActions.ExpandAdditionalFilters(driver);
            SuppressionActions.SelectAdditionalFilters(driver, "retirement eligible date");
            AnalyzeAction.SelectLeftSideDateDemoValues(driver, "retirement eligible date", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            
            string[] QuestionText = { "I feel my Company has good benefits." };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 SuppressionActions.ClickMenubarinResultTab(driver);
                 Thread.Sleep(2000);
                 SuppressionActions.SelectChartTypeDatatableInResultTab(driver);
                 Thread.Sleep(2000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (string)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('question-table').shadowRoot.querySelector('.table-striped tr td:last-child').textContent ");
                 Assert.AreEqual("--", SuppressedText);

             });

        }

        [TestMethod]
        public void B_CS_ValidateResultItemSuppressionWithPreviousComparisongroup_DateDemoFilter_Average_02()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            string[] QuestionText = { "I feel my Company has good benefits." };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 SuppressionActions.ClickMenubarinResultTab(driver);
                 Thread.Sleep(2000);
                 SuppressionActions.SelectChartTypeDatatableInResultTab(driver);
                 Thread.Sleep(2000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (string)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('question-table').shadowRoot.querySelector('.table-striped tr td:last-child').textContent ");
                 Assert.AreEqual("--", SuppressedText);

             });

        }


        [TestMethod]

        public void CS_ValidateInsightItemSuppressionWithPreviousComparisongroup_DateDemoapplied_Percentage_03()
        {

            bool flag = false;
            string[] questionTexts = { "Medical" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }


        [TestMethod]

        public void CS_ValidateInsightItemSuppressionWithPreviousComparisongroup_DateDemoapplied_Average_04()
        {

            bool flag = false;
            string[] questionTexts = { "Medical" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightComparisonAvg(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void CS_ValidateDrillDownItemSuppressionWithPreviousComparisongroup_DateDemoapplied_Percentage_05()
        {
            bool flag = false;
            string[] questionTexts = { "Medical" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void CS_ValidateDrillDownItemSuppressionWithPreviousComparisongroup_DateDemoapplied_Average_06()
        {
            bool flag = false;
            string[] questionTexts = { "Medical" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]
        public void CS_ValidateResultItemSuppressionWithSegmentComparisongroup_NoFilter_Percentage()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "220", "220", "220", "220", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(3000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(2000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(15000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            Thread.Sleep(2000);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Segment", null, null, null, "marital status", "Single (never married)");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);

            Thread.Sleep(15000);
            string[] QuestionText = { "I feel my Company has good benefits.", "Dental" };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 SuppressionActions.ClickMenubarinResultTab(driver);
                 Thread.Sleep(2000);
                 SuppressionActions.SelectChartTypeDatatableInResultTab(driver);
                 Thread.Sleep(2000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (string)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('question-table').shadowRoot.querySelector('.table-striped tr td:last-child').textContent ");
                 Assert.AreEqual("--", SuppressedText);

             });

        }

        [TestMethod]
        public void CS_ValidateResultItemSuppressionWithSegmentComparisongroup_NoFilter_Average()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyze();
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(15000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
             AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            string[] QuestionText = { "I feel my Company has good benefits.", "Dental" };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 SuppressionActions.ClickMenubarinResultTab(driver);
                 Thread.Sleep(2000);
                 SuppressionActions.SelectChartTypeDatatableInResultTab(driver);
                 Thread.Sleep(2000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (string)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('question-table').shadowRoot.querySelector('.table-striped tr td:last-child').textContent ");
                 Assert.AreEqual("--", SuppressedText);

             });
        }

        [TestMethod]

        public void CS_ValidateResultItemSuppressionWithBenchmarkComparisongroup_NoFilter_Percentage_01()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(2000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "480", "480", "480", "480", "10");
            SuppressionActions.Saveaccountsuppression(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.Clickcompletedthesurvey(driver);
            Thread.Sleep(2000);
            SuppressionActions.SaveAnalysisdefaults(driver);
            Thread.Sleep(50000);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(15000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            SuppressionActions.UncheckbenchmarkfromResultTab(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Benchmark", null, null, "Black Pearl Technology", null, null);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);

            Thread.Sleep(15000);
            string[] QuestionText = { "I feel my Company has good benefits." };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('.comparison-1-unfav') ");
                 if (SuppressedText == null)
                 {
                     var verificationText = "Element not present";
                     Assert.AreEqual("Element not present", verificationText);
                 }
                 else
                 {
                     Assert.Fail();
                 }
             });

        }


        [TestMethod]

        public void CS_ValidateResultItemSuppressionWithBenchmarkComparisongroup_NoFilter_Average_02()
        {
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyze();
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(15000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(2000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            Thread.Sleep(2000);
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(15000);
            string[] QuestionText = { "I feel my Company has good benefits." };
            QuestionText.ToList()
             .ForEach((element) =>
             {
                 AnalyzeAction.SearchQuestion(driver, element);
                 Thread.Sleep(10000);
                 IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                 var SuppressedText = (IWebElement)js.ExecuteScript("return document.querySelector('question-cards').shadowRoot.querySelector('chart-selector').shadowRoot.querySelector('rating-scale-chart').shadowRoot.querySelector('.comparison-1-unfav') ");
                 if (SuppressedText == null)
                 {
                     var verificationText = "Element not present";
                     Assert.AreEqual("Element not present", verificationText);
                 }
                 else
                 {
                     Assert.Fail();
                 }
             });

        }

        [TestMethod]
        public void CS_ValidateInsightItemSuppressionWithBenchmarkComparisongroup_NoFilterapplied_Percentage_03()
        {

            bool flag = false;
            string[] questionTexts = { "Overall, I am satisfied with the medical benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ChangeBenchMarkValueInSettings(driver, "Black Pearl Technology");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightBenchmark(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }


        [TestMethod]
        public void CS_ValidateInsightItemSuppressionWithBenchmarkComparisongroup_NoFilterapplied_Average_04()
        {

            bool flag = false;
            string[] questionTexts = { "Overall, I am satisfied with the medical benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ChangeBenchMarkValueInSettings(driver, "Black Pearl Technology");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInIsightBenchmark(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);

        }

        [TestMethod]

        public void CS_ValidateDrillDownItemSuppressionWithBenchmarkComparisongroup_NoFilterapplied_Percentage_05()
        {
            bool flag = false;
            string[] questionTexts = { "Overall, I am satisfied with the medical benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownBenchmark(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }


        [TestMethod]

        public void CS_ValidateDrillDownItemSuppressionWithBenchmarkComparisongroup_NoFilterapplied_Average_06()
        {
            bool flag = false;
            string[] questionTexts = { "Overall, I am satisfied with the medical benefits." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(1000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(20000);
            foreach (string question in questionTexts)
            {
                flag = SuppressionActions.CheckForSuppressionInDrillDownBenchmark(driver, questionTexts);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }



        /// <summary>
        /// Alteast One Rule Flag by Abhishek
        /// </summary>

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   None
        /// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavNoFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "983", "983", "983", "983", "983");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            /// <summary>
            /// Waiting for the Data transfer job to process
            /// </summary>
            Thread.Sleep(60000);
            /// <summary>
            /// Waiting ends
            /// </summary>
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits", "The Company benefits package was an important factor in my decision to join the Company", "The Company benefits package is an important factor in my decision to continue to work here", "I am satisfied with my medical plan’s hospital network", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations", "I have a good understanding of our pension plan", "My retirement benefits (401(k) and pens ion combined) are a significant component of my retirement planning", "Medical", "Dental", "Vision", "Disability", "401(k) Savings Plan", "Retiree Medical", "Stock Purchase Plan", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive", "I find the Benefits Newsletter useful", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "I have a good work/life balance", "The Company promotes wellness", "The Company provides adequate mental health resources for myself and my family", "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Over the phone Advisor Service", "The 401(k) plan website", "Provider Coverage meeting", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDemoReportNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Demo Filter
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavDemoFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "151", "151", "151", "151", "151");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            /// <summary>
            /// Waiting for the Data transfer job to process
            /// </summary>
            Thread.Sleep(60000);
            /// <summary>
            /// Waiting ends
            /// </summary>
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "india");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "401(k) Savings Plan", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "I have a good work/life balance.", "The Company provides adequate mental health resources for myself and my family.", "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDemoReportDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Time Period Filter
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavTimeFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "406", "406", "406", "406", "406");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            /// <summary>
            /// Waiting for the Data transfer job to process
            /// </summary>
            Thread.Sleep(60000);
            /// <summary>
            /// Waiting ends
            /// </summary>
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "1/1/2015", "1/1/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "401(k) Savings Plan", "Retiree Medical", "Auto & Home Insurance", "I have a good work / life balance.", "The Company provides adequate mental health resources for myself and my family.", "The Pension Plan website", "Provider Coverage meeting" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Retiree Medical", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Retiree Medical", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDemoReportTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Retiree Medical", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Demo + Time Period Filter
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavDemoNTimeFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "15", "15", "15", "15", "15");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            /// <summary>
            /// Waiting for the Data transfer job to process
            /// </summary>
            Thread.Sleep(60000);
            /// <summary>
            /// Waiting ends
            /// </summary>
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "1/1/2015", "1/1/2016");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "india");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDemoReportDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Date as Time Period Filter
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavDateAsTimeFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "303", "303", "303", "303", "303");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            /// <summary>
            /// Waiting for the Data transfer job to process
            /// </summary>
            Thread.Sleep(60000);
            /// <summary>
            /// Waiting ends
            /// </summary>
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SetTimePeriodAnalysisSettings(driver, "retirement eligible date");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "1/1/2015", "1/1/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Over the phone Advisor Service", "The 401(k) plan website", "Provider Coverage meeting", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDemoReportDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Date as Demo Filter
        /// Data Calculation	=	Percentage
        /// </summary>

        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavDateAsDemoFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "303", "303", "303", "303", "303");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            /// <summary>
            /// Waiting for the Data transfer job to process
            /// </summary>
            Thread.Sleep(60000);
            /// <summary>
            /// Waiting ends
            /// </summary>
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDateDemoValues(driver, "retirement eligible date", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Over the phone Advisor Service", "The 401(k) plan website", "Provider Coverage meeting", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDemoReportDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   None
        /// Comparison      	=   Previous
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavNoFilterScenarioVsPrev()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "983", "983", "983", "983", "983");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabNoFilterVsPrev()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "401(k) Savings Plan", "Dental", "Disability", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "How satisfied are you with the Pension Plan website?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.", "Retiree Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Vision" };
            string[] questionTextsNonRating = { "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Over the phone Advisor Service", "Provider Coverage meeting", "Provider website", "The 401(k) plan website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsComparisonRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsComparisonNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightNoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Disability", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "How satisfied are you with the Pension Plan website?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.", "Retiree Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownNoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Disability", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "How satisfied are you with the Pension Plan website?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.", "Retiree Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Demo Filter
        /// Comparison      	=   Previous
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavDemoFilterScenarioVsPrev()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "151", "151", "151", "151", "151");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "india");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabDemoFilterVsPrev()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "401(k) Savings Plan", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            string[] questionTextsNonRating = { "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsComparisonRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsComparisonNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Date as Demo Filter
        /// Comparison      	=   Previous
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavDateAsDemoFilterScenarioVsPrev()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "303", "303", "303", "303", "303");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDateDemoValues(driver, "retirement eligible date", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabDateAsDemoFilterVsPrev()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "401(k) Savings Plan", "Dental", "Employee Assistance Program (EAP)", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            string[] questionTextsNonRating = { "Over the phone Advisor Service", "Provider Coverage meeting", "Provider website", "The 401(k) plan website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsComparisonRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsComparisonNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightDateAsDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Employee Assistance Program (EAP)", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownDateAsDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Employee Assistance Program (EAP)", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   None
        /// Comparison      	=   Benchmark
		/// Data Calculation	=	Percentage
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForFavNoFilterScenarioVsBenchmark()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "400", "400", "400", "400", "400");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Percentage");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Benchmark", null, "Black Pearl Technology", null, null, null);
            AnalyzeAction.ChangeBenchMarkValueInSettings(driver, "Black Pearl Technology");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavResultTabNoFilterVsBenchmark()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "How satisfied are you with the 401(k) website?", "How satisfied are you with the Pension Plan website?", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Flexible Spending Accounts", "I am satisfied with my medical plan’s physician network.", "Overall, I am satisfied with the medical benefits.", "I am satisfied with the quality of the benefits communications I receive.", "The amount I pay out of my paycheck for my medical plan coverage is reasonable.", "My retirement savings plan includes money set aside for healthcare expenses in retirement.", "401(k) Savings Plan", "How much does your job give you a sense of personal satisfaction?", "How satisfied are you with the Benefits Service Center?", "Stock Purchase Plan", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the way my medical plan covers prescription drugs.", "I think the contribution rate to cover an employee should be less than the contribution rate to cover an employee’s spouse.", "Life Insurance", "Retiree Medical", "The Company benefits package was an important factor in my decision to join the Company." };
            string[] questionTextsNonRating = { "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Provider website", "The Pension Plan website", "Why did you choose not to enroll in a High Deductible Health Plan? (check all that apply)", "Have you considered enrolling in one of the High Deductible Health Plans (HDHPs)?", "Over the phone Advisor Service", "Please check the issues that distract you during the work day and/or reduce your productivity (check all that apply)" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsBenchmarkRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsBenchmarkNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavInsightNoFilterVsBenchmark()
        {
            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the 401(k) website?", "How satisfied are you with the Pension Plan website?", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Flexible Spending Accounts", "I am satisfied with my medical plan’s physician network.", "Overall, I am satisfied with the medical benefits.", "I am satisfied with the quality of the benefits communications I receive.", "The amount I pay out of my paycheck for my medical plan coverage is reasonable.", "My retirement savings plan includes money set aside for healthcare expenses in retirement.", "401(k) Savings Plan", "How much does your job give you a sense of personal satisfaction?", "How satisfied are you with the Benefits Service Center?", "Stock Purchase Plan", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the way my medical plan covers prescription drugs.", "I think the contribution rate to cover an employee should be less than the contribution rate to cover an employee’s spouse.", "Life Insurance", "Retiree Medical", "The Company benefits package was an important factor in my decision to join the Company." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightBenchmark(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneFavDrillDownNoFilterVsBenchmark()
        {
            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the 401(k) website?", "How satisfied are you with the Pension Plan website?", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Flexible Spending Accounts", "I am satisfied with my medical plan’s physician network.", "Overall, I am satisfied with the medical benefits.", "I am satisfied with the quality of the benefits communications I receive.", "The amount I pay out of my paycheck for my medical plan coverage is reasonable.", "My retirement savings plan includes money set aside for healthcare expenses in retirement.", "401(k) Savings Plan", "How much does your job give you a sense of personal satisfaction?", "How satisfied are you with the Benefits Service Center?", "Stock Purchase Plan", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the way my medical plan covers prescription drugs.", "I think the contribution rate to cover an employee should be less than the contribution rate to cover an employee’s spouse.", "Life Insurance", "Retiree Medical", "The Company benefits package was an important factor in my decision to join the Company." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownBenchmark(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   None
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgNoFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "983", "983", "983", "983", "983");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits", "The Company benefits package was an important factor in my decision to join the Company", "The Company benefits package is an important factor in my decision to continue to work here", "I am satisfied with my medical plan’s hospital network", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations", "I have a good understanding of our pension plan", "My retirement benefits (401(k) and pens ion combined) are a significant component of my retirement planning", "Medical", "Dental", "Vision", "Disability", "401(k) Savings Plan", "Retiree Medical", "Stock Purchase Plan", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive", "I find the Benefits Newsletter useful", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "I have a good work/life balance", "The Company promotes wellness", "The Company provides adequate mental health resources for myself and my family", "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Over the phone Advisor Service", "The 401(k) plan website", "Provider Coverage meeting", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDemoReportNoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Demo Filter
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgDemoFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "151", "151", "151", "151", "151");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "india");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "401(k) Savings Plan", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "I have a good work/life balance.", "The Company provides adequate mental health resources for myself and my family.", "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDemoReportDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "Retiree Medical", "Stock Purchase Plan", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Time Period Filter
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgTimeFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "406", "406", "406", "406", "406");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "1/1/2015", "1/1/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "401(k) Savings Plan", "Retiree Medical", "Auto & Home Insurance", "I have a good work / life balance.", "The Company provides adequate mental health resources for myself and my family.", "The Pension Plan website", "Provider Coverage meeting" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Retiree Medical", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Retiree Medical", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDemoReportTimePeriodFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Retiree Medical", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule		=   Answer at least one Question
        /// Filter          	=   Demo + Time Period Filter
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgDemoNTimeFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "15", "15", "15", "15", "15");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "1/1/2015", "1/1/2016");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "india");
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDemoReportDemoNTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "Stock Purchase Plan", "Auto & Home Insurance" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Date as Time Period Filter
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgDateAsTimeFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "303", "303", "303", "303", "303");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SetTimePeriodAnalysisSettings(driver, "retirement eligible date");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
            AnalyzeAction.ApplyLeftSideTimePeriod(driver, "date ranges", "1/1/2015", "1/1/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Over the phone Advisor Service", "The 401(k) plan website", "Provider Coverage meeting", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDemoReportDateAsTimeFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Date as Demo Filter
        /// Data Calculation	=	Average
        /// </summary>

        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgDateAsDemoFilterScenario()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "303", "303", "303", "303", "303");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDateDemoValues(driver, "retirement eligible date", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "I feel my Company has good benefits.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company benefits package is an important factor in my decision to continue to work here.", "I am satisfied with my medical plan’s hospital network.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I have a good understanding of our pension plan.", "Medical", "Dental", "401(k) Savings Plan", "Stock Purchase Plan", "Employee Assistance Program (EAP)", "Auto & Home Insurance", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Over the phone Advisor Service", "The 401(k) plan website", "Provider Coverage meeting", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTexts)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flag = SuppressionActions.CheckForSuppressionInResults(driver);
                if (flag == false)
                    break;
            }
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsight(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDown(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDemoReportDateAsDemoFilter()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Auto & Home Insurance", "Dental", "Employee Assistance Program (EAP)", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression2");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDemographic(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   None
        /// Comparison      	=   Previous
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgNoFilterScenarioVsPrev()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "983", "983", "983", "983", "983");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabNoFilterVsPrev()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "401(k) Savings Plan", "Dental", "Disability", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "How satisfied are you with the Pension Plan website?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.", "Retiree Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Vision" };
            string[] questionTextsNonRating = { "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Over the phone Advisor Service", "Provider Coverage meeting", "Provider website", "The 401(k) plan website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsComparisonRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsComparisonNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightNoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Disability", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "How satisfied are you with the Pension Plan website?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.", "Retiree Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownNoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Disability", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "How satisfied are you with the Pension Plan website?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Medical", "My retirement benefits (401(k) and pension combined) are a significant component of my retirement planning.", "Retiree Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family.", "Vision" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Demo Filter
        /// Comparison      	=   Previous
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgDemoFilterScenarioVsPrev()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "151", "151", "151", "151", "151");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDemoValues(driver, "country", "india");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabDemoFilterVsPrev()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "401(k) Savings Plan", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            string[] questionTextsNonRating = { "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Provider website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsComparisonRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsComparisonNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "I am satisfied with the quality and quantity of the information I received about my benefits when I first started with the Company.", "I am satisfied with the quantity of the benefits communications I receive.", "I find the Benefits Newsletter useful.", "I have a good work/life balance.", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "The Company benefits package was an important factor in my decision to join the Company.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   Date as Demo Filter
        /// Comparison      	=   Previous
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgDateAsDemoFilterScenarioVsPrev()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "303", "303", "303", "303", "303");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SelectLeftSideDateDemoValues(driver, "retirement eligible date", "01/01/2015", "01/01/2016");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Previous", null, "Item suppression2", null, null, null);
            AnalyzeAction.ApplyFavTabVsPreviousComparison(driver, "Item suppression2");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabDateAsDemoFilterVsPrev()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "401(k) Savings Plan", "Dental", "Employee Assistance Program (EAP)", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            string[] questionTextsNonRating = { "Over the phone Advisor Service", "Provider Coverage meeting", "Provider website", "The 401(k) plan website" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsComparisonRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsComparisonNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightDateAsDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Employee Assistance Program (EAP)", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownDateAsDemoFilterVsPrev()
        {
            bool flag = false;
            string[] questionTexts = { "401(k) Savings Plan", "Dental", "Employee Assistance Program (EAP)", "How able are you to satisfy both your work and family responsibilities?", "How helpful was the Employee Assistance Program?", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the quantity of the benefits communications I receive.", "I feel my 401(k) savings plan (SIP) is as good as those offered by other organizations.", "I feel my Company has good benefits.", "I find the Benefits Newsletter useful.", "I have a good understanding of our pension plan.", "I have a good work/life balance.", "Medical", "Stock Purchase Plan", "The Company benefits package is an important factor in my decision to continue to work here.", "The Company benefits package was an important factor in my decision to join the Company.", "The Company promotes wellness.", "The Company provides adequate mental health resources for myself and my family." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownComparison(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        /// <summary>
        /// Completion Rule 	=   Answer at least one Question
        /// Filter          	=   None
        /// Comparison      	=   Benchmark
		/// Data Calculation	=	Average
        /// </summary>
        [TestMethod]
        public void UpdateSuppValuesComplRuleForAvgNoFilterScenarioVsBenchmark()
        {
            RemoteWebDriver driver = SetDriverAndNavigatetoAccountSetting();
            Thread.Sleep(10000);
            SuppressionActions.SetSupprersionNAccountSetting(driver, "400", "400", "400", "400", "400");
            SuppressionActions.Saveaccountsuppression(driver);
            SuppressionActions.ClickAccountDefaults(driver);
            Thread.Sleep(2000);
            SuppressionActions.ClickAnsweredatleast1question(driver);
            SuppressionActions.SaveAnalysisdefaults(driver);
            AnalyzeAction.GoToSurveyAnalyzeScreen(driver);
            Thread.Sleep(5000);
            AnalyzeAction.OpenAnalyzeForm(driver, "Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.OpenAnalysisSettings(driver);
            Thread.Sleep(5000);
            AnalyzeAction.DataCalculationSelection(driver, "Average");
            AnalyzeAction.GroupingInAnalysisSetting(driver, "Question");
            AnalyzeAction.UnSelectFavComparisons(driver, "All", "Previous", "Benchmark", "Segment");
            AnalyzeAction.UnSelectResultTabComparisonGroup(driver);
            AnalyzeAction.SelectResultTabComparisonGroup(driver, "Benchmark", null, "Black Pearl Technology", null, null, null);
            AnalyzeAction.ChangeBenchMarkValueInSettings(driver, "Black Pearl Technology");
            AnalyzeAction.ApplyAnalyseSettings(driver);
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgResultTabNoFilterVsBenchmark()
        {
            bool flagrating = false;
            bool flagnonrating = false;
            string[] questionTextsRating = { "How satisfied are you with the 401(k) website?", "How satisfied are you with the Pension Plan website?", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Flexible Spending Accounts", "I am satisfied with my medical plan’s physician network.", "Overall, I am satisfied with the medical benefits.", "I am satisfied with the quality of the benefits communications I receive.", "The amount I pay out of my paycheck for my medical plan coverage is reasonable.", "My retirement savings plan includes money set aside for healthcare expenses in retirement.", "401(k) Savings Plan", "How much does your job give you a sense of personal satisfaction?", "How satisfied are you with the Benefits Service Center?", "Stock Purchase Plan", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the way my medical plan covers prescription drugs.", "I think the contribution rate to cover an employee should be less than the contribution rate to cover an employee’s spouse.", "Life Insurance", "Retiree Medical", "The Company benefits package was an important factor in my decision to join the Company." };
            string[] questionTextsNonRating = { "For services such as office visits, do you commonly ask what the doctor charges in advance of the visit (not your copay/coinsurance, but the full, actual cost)?", "Provider website", "The Pension Plan website", "Why did you choose not to enroll in a High Deductible Health Plan? (check all that apply)", "Have you considered enrolling in one of the High Deductible Health Plans (HDHPs)?", "Over the phone Advisor Service", "Please check the issues that distract you during the work day and/or reduce your productivity (check all that apply)" };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Results");
            foreach (string question in questionTextsRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagrating = SuppressionActions.CheckForSuppressionInResultsBenchmarkRating(driver);
                if (flagrating == false)
                    break;
            }
            foreach (string question in questionTextsNonRating)
            {
                AnalyzeAction.SearchQuestion(driver, question);
                flagnonrating = SuppressionActions.CheckForSuppressionInResultsBenchmarkNonRating(driver);
                if (flagnonrating == false)
                    break;
            }
            Assert.IsTrue(flagrating && flagnonrating);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgInsightNoFilterVsBenchmark()
        {
            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the 401(k) website?", "How satisfied are you with the Pension Plan website?", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Flexible Spending Accounts", "I am satisfied with my medical plan’s physician network.", "Overall, I am satisfied with the medical benefits.", "I am satisfied with the quality of the benefits communications I receive.", "The amount I pay out of my paycheck for my medical plan coverage is reasonable.", "My retirement savings plan includes money set aside for healthcare expenses in retirement.", "401(k) Savings Plan", "How much does your job give you a sense of personal satisfaction?", "How satisfied are you with the Benefits Service Center?", "Stock Purchase Plan", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the way my medical plan covers prescription drugs.", "I think the contribution rate to cover an employee should be less than the contribution rate to cover an employee’s spouse.", "Life Insurance", "Retiree Medical", "The Company benefits package was an important factor in my decision to join the Company." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInIsightBenchmark(driver, questionTexts);
            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void ItemSuppAtleastOneAvgDrillDownNoFilterVsBenchmark()
        {
            bool flag = false;
            string[] questionTexts = { "How satisfied are you with the 401(k) website?", "How satisfied are you with the Pension Plan website?", "Disability", "I would like to have the option of receiving information in new communication formats (e.g., podcasts, wikis, etc.)", "Flexible Spending Accounts", "I am satisfied with my medical plan’s physician network.", "Overall, I am satisfied with the medical benefits.", "I am satisfied with the quality of the benefits communications I receive.", "The amount I pay out of my paycheck for my medical plan coverage is reasonable.", "My retirement savings plan includes money set aside for healthcare expenses in retirement.", "401(k) Savings Plan", "How much does your job give you a sense of personal satisfaction?", "How satisfied are you with the Benefits Service Center?", "Stock Purchase Plan", "I am satisfied with my medical plan’s hospital network.", "I am satisfied with the way my medical plan covers prescription drugs.", "I think the contribution rate to cover an employee should be less than the contribution rate to cover an employee’s spouse.", "Life Insurance", "Retiree Medical", "The Company benefits package was an important factor in my decision to join the Company." };
            RemoteWebDriver driver = SetDriverAndNavigateToAnalyzeReport("Item suppression1");
            Thread.Sleep(10000);
            AnalyzeAction.SwitchToReportTab(driver, "Favorability");
            flag = SuppressionActions.CheckForSuppressionInDrillDownBenchmark(driver, questionTexts);
            Assert.IsTrue(flag);
        }
    }
}
