﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Remote;

namespace Driver
{
    [TestClass]
    public class HomePageAnalyzeCardsTest
    {
        public static bool IsChromeDriverAvailable = false;
        public static bool IsFirefoxDriverAvailable = false;
        public static bool IsIEDriverAvailable = false;
        public static ChromeDriver NewChromeDriver;
        public static FirefoxDriver NewFirefoxDriver;
        public static InternetExplorerDriver NewIEDriver;
        public static DriverAndLoginDto DriverAndLogin;
        public static RemoteWebDriver CurrentDriver;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            DriverAndLogin = new DriverAndLoginDto
            {
                Url = context.Properties["Url"].ToString(),
                Browser = context.Properties["Browser"].ToString(),
                Username = context.Properties["Username"].ToString(),
                Password = context.Properties["Password"].ToString(),
                Account = context.Properties["Account"].ToString()
            };
        }

        public static RemoteWebDriver GetDriver(string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (IsFirefoxDriverAvailable && !string.IsNullOrEmpty(url))
                        NewFirefoxDriver.Navigate().GoToUrl(url);
                    return NewFirefoxDriver;
                case "IE":
                    if (IsIEDriverAvailable && !string.IsNullOrEmpty(url))
                        NewIEDriver.Navigate().GoToUrl(url);
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (IsChromeDriverAvailable && !string.IsNullOrEmpty(url))
                        NewChromeDriver.Navigate().GoToUrl(url);
                    return NewChromeDriver;
            }
        }
        public static RemoteWebDriver InitializeAndGetDriver(bool newDriver, string driverName, string url = null)
        {
            switch (driverName)
            {
                case "Firefox":
                    if (!newDriver && IsFirefoxDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewFirefoxDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewFirefoxDriver = (FirefoxDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsFirefoxDriverAvailable = true;
                    }
                    return NewFirefoxDriver;
                case "IE":
                    if (!newDriver && IsIEDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewIEDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewIEDriver = (InternetExplorerDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsIEDriverAvailable = true;
                    }
                    return NewIEDriver;
                case "Chrome":
                default:
                    if (!newDriver && IsChromeDriverAvailable)
                    {
                        if (!string.IsNullOrEmpty(url))
                            NewChromeDriver.Navigate().GoToUrl(url);
                    }
                    else
                    {
                        NewChromeDriver = (ChromeDriver)WebDriverFactory.GetWebDriver(driverName, url);
                        IsChromeDriverAvailable = true;
                    }
                    return NewChromeDriver;
            }
        }

        [TestMethod]
        public void ValidateNewHomePageLayout()
        {
            RemoteWebDriver driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
            WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
            Thread.Sleep(5000);
            var cardArea = UIActions.FindElement(driver, "#homecards");
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var editSruveys = UIActions.FindElement(driver, ".editSurveyHomePage.mdc-card");
            Assert.IsTrue(cardArea != null);
            Assert.IsTrue(cards != null && cards.Count > 0);
            Assert.IsTrue(editSruveys != null);
        }

        [TestMethod]
        public void ValidateAnalyzeLandgingPageLayout()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var sideBarAnalyze = UIActions.FindElement(driver, ".sidebar ul.sidebar-menu li a.show-analyze");
            sideBarAnalyze.Click();
            Thread.Sleep(10000);
            var cardArea = UIActions.FindElement(driver, "#homecards");
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            Assert.IsTrue(cardArea != null);
            Assert.IsTrue(cards != null && cards.Count > 0);
        }

        [TestMethod]
        public void ValidateCardLayout()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var background = firstCard.FindElement(By.CssSelector(".mdc-card__media"));
            var bgIcon = background.FindElement(By.CssSelector(".card-infography-icon"));
            Assert.IsTrue(background != null && bgIcon != null);
            var surveyLink = firstCard.FindElement(By.CssSelector("a.card-title"));
            Assert.IsTrue(surveyLink != null);
            var lastResponseText = firstCard.FindElement(By.CssSelector(".card-info"));
            Assert.IsTrue(lastResponseText != null);
            var chipsReg = firstCard.FindElements(By.CssSelector(".mdc-chip-set .mdc-chip-light"));
            Assert.IsTrue(chipsReg != null && chipsReg.Count > 0);
            var analyzeIcon = firstCard.FindElement(By.CssSelector("a.show-analyze"));
            var expandIcon = firstCard.FindElement(By.CssSelector(".card-footer a.card-expand"));
            var menuIcon = firstCard.FindElement(By.CssSelector(".card-footer a.dropdown-toggle"));
            Assert.IsTrue(analyzeIcon != null && expandIcon != null && menuIcon != null);
        }

        [TestMethod]
        public void ValidateCardActions_ExpandCard()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var expandIcon = firstCard.FindElement(By.CssSelector(".card-footer a.card-expand"));
            expandIcon.Click();
            var distTable = firstCard.FindElement(By.CssSelector(".pulse-dist-card table.card-table"));
            Assert.IsTrue(distTable != null);
            var rows = distTable.FindElements(By.CssSelector("tbody tr"));
            Assert.IsTrue(rows != null && rows.Count > 0);
        }

        [TestMethod]
        public void ValidateCardActions_DistributeNavigation_Edit()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var expandIcon = firstCard.FindElement(By.CssSelector(".card-footer a.card-expand"));
            expandIcon.Click();
            var distTable = firstCard.FindElement(By.CssSelector(".pulse-dist-card table.card-table"));
            Assert.IsTrue(distTable != null);
            var rows = distTable.FindElements(By.CssSelector("tbody tr"));
            var dist = rows.FirstOrDefault();
            string distName = dist.FindElements(By.TagName("td")).FirstOrDefault().Text;
            var distMenuArea = dist.FindElement(By.CssSelector("td div.btn-group"));
            var distMenuIcon = distMenuArea.FindElement(By.CssSelector("a.dropdown-toggle"));
            distMenuIcon.Click();
            var distMenu = distMenuArea.FindElements(By.CssSelector("ul.dropdown-menu li a"));
            var edit = distMenu.FirstOrDefault(x => x.Text == "Edit Distribution");
            edit.Click();
            Thread.Sleep(10000);
            var header = UIActions.FindElement(driver, "#distribute-summary-page-header");
            Assert.IsTrue(header != null && header.Text == "Summary");
        }

        [TestMethod]
        public void ValidateCardActions_DistributeNavigation_AddParticipants()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var expandIcon = firstCard.FindElement(By.CssSelector(".card-footer a.card-expand"));
            expandIcon.Click();
            var distTable = firstCard.FindElement(By.CssSelector(".pulse-dist-card table.card-table"));
            Assert.IsTrue(distTable != null);
            var rows = distTable.FindElements(By.CssSelector("tbody tr"));
            var dist = rows.FirstOrDefault();
            string distName = dist.FindElements(By.TagName("td")).FirstOrDefault().Text;
            var distMenuArea = dist.FindElement(By.CssSelector("td div.btn-group"));
            var distMenuIcon = distMenuArea.FindElement(By.CssSelector("a.dropdown-toggle"));
            distMenuIcon.Click();
            var distMenu = distMenuArea.FindElements(By.CssSelector("ul.dropdown-menu li a"));
            var edit = distMenu.FirstOrDefault(x => x.Text == "Add Participants");
            edit.Click();
            Thread.Sleep(10000);
            var header = UIActions.FindElement(driver, "#wizard-distribution-name");
            Assert.IsTrue(header != null && header.Text == distName);
            var participantGrid = UIActions.FindElement(driver, "#manage-participants-table");
            Assert.IsTrue(participantGrid != null);
        }

        [TestMethod]
        public void ValidateCardActions_DistributeNavigation_AnalyzeDistribution()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var surveyLink = firstCard.FindElement(By.CssSelector("a.card-title"));
            var surveyName = surveyLink.Text;
            var expandIcon = firstCard.FindElement(By.CssSelector(".card-footer a.card-expand"));
            expandIcon.Click();
            var distTable = firstCard.FindElement(By.CssSelector(".pulse-dist-card table.card-table"));
            Assert.IsTrue(distTable != null);
            var rows = distTable.FindElements(By.CssSelector("tbody tr"));
            var dist = rows.FirstOrDefault();
            string distName = dist.FindElements(By.TagName("td")).FirstOrDefault().Text;
            var distMenuArea = dist.FindElement(By.CssSelector("td div.btn-group"));
            var distMenuIcon = distMenuArea.FindElement(By.CssSelector("a.dropdown-toggle"));
            distMenuIcon.Click();
            var distMenu = distMenuArea.FindElements(By.CssSelector("ul.dropdown-menu li a"));
            var analyze = distMenu.FirstOrDefault(x => x.Text == "Analyze Distribution");
            analyze.Click();
            Thread.Sleep(15000);
            var filterAccordiion = UIActions.FindElement(driver, ".filter-accordion #accordion h4.panel-title a.accordion-toggle span");
            filterAccordiion.Click();
            Thread.Sleep(1000);
            var filterchips = UIActions.FindElements(driver, "#Filter .panel-body .mdc-chip__text");
            Assert.IsTrue(filterchips.Any(x => x.Text.Contains(distName)));
        }

        [TestMethod]
        public void ValidateCardActions_ClickOnSurveyName()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var surveyLink = firstCard.FindElement(By.CssSelector("a.card-title"));
            var surveyName = surveyLink.Text;
            surveyLink.Click();
            Thread.Sleep(5000);
            var analyzeTitle = UIActions.FindElement(driver, "#analyze-report-title");
            Assert.IsTrue(analyzeTitle != null && analyzeTitle.GetAttribute("value") == surveyName);
        }

        [TestMethod]
        public void ValidateCardActions_ClickOnAnalyzeIcon()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var surveyLink = firstCard.FindElement(By.CssSelector("a.card-title"));
            var surveyName = surveyLink.Text;
            var analyzeIcon = firstCard.FindElement(By.CssSelector("a.show-analyze"));
            analyzeIcon.Click();
            Thread.Sleep(5000);
            var analyzeTitle = UIActions.FindElement(driver, "#analyze-report-title");
            Assert.IsTrue(analyzeTitle != null && analyzeTitle.GetAttribute("value") == surveyName);
        }

        [TestMethod]
        public void ValidateCardActions_EditSurvey()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var surveyLink = firstCard.FindElement(By.CssSelector("a.card-title"));
            var surveyName = surveyLink.Text;
            var menuToggle = firstCard.FindElement(By.CssSelector(".card-footer .btn-group"));
            var menuIcon = menuToggle.FindElement(By.CssSelector(".card-footer a.dropdown-toggle"));
            menuIcon.Click();
            var menu = menuToggle.FindElements(By.CssSelector("ul.dropdown-menu li a"));
            var editSurvey = menu.FirstOrDefault(x => x.Text == "Edit Survey");
            editSurvey.Click();
            Thread.Sleep(15000);
            var authorSurveyTitle = UIActions.FindElement(driver, ".mainPage .surveyTitle-wrap");
            Assert.IsTrue(authorSurveyTitle != null && authorSurveyTitle.Text == surveyName);
        }

        [TestMethod]
        public void ValidateCardActions_PreviewSurvey()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver == null)
            {
                driver = InitializeAndGetDriver(true, DriverAndLogin.Browser, DriverAndLogin.Url);
                WebDriverFactory.Login(driver, DriverAndLogin.Username, DriverAndLogin.Password, DriverAndLogin.Account);
                Thread.Sleep(5000);
            }
            var cards = UIActions.FindElements(driver, "#homecards .pulse-card");
            var firstCard = cards.FirstOrDefault();
            var surveyLink = firstCard.FindElement(By.CssSelector("a.card-title"));
            var surveyName = surveyLink.Text;
            var menuToggle = firstCard.FindElement(By.CssSelector(".card-footer .btn-group"));
            var menuIcon = menuToggle.FindElement(By.CssSelector(".card-footer a.dropdown-toggle"));
            menuIcon.Click();
            var menu = menuToggle.FindElements(By.CssSelector("ul.dropdown-menu li a"));
            var previewSurvey = menu.FirstOrDefault(x => x.Text == "Preview Survey");
            previewSurvey.Click();
            Thread.Sleep(1000);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(5000);
            var playerSurveyTitle = UIActions.FindElement(driver, "#txtSurveyNameTitle");
            Assert.IsTrue(playerSurveyTitle != null && playerSurveyTitle.Text == surveyName);
        }

        [ClassCleanup()]
        public static void CleanUp()
        {
            RemoteWebDriver driver = GetDriver(DriverAndLogin.Browser);
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }
    }
}
